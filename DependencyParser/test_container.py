from unittest import TestCase

from DependencyParser.Container import Container


class TestContainer(TestCase):
    container = Container("./resources/GOLD_tagged/GOLD_5SE1.txt")

    def test_get_program_name(self):
        self.assertEqual("GOLD", self.container.get_program_name())

    def test_get_file_name(self):
        self.assertEqual("GOLD_5SE1.txt", self.container.get_file_name())
        pass


def main():
    TestCase.main()


if __name__ == '__main__':
    main()

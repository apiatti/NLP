from unittest import TestCase

from DependencyParser.Analyzer import Analyzer
from DependencyParser.Container import Container
from DependencyParser.FileWriter import FileWriter


class TestFileWriter(TestCase):
    analyzer = Analyzer(Container("./resources/GOLD_tagged/GOLD_5SE10.txt"), "GOLD")
    fileWriter = FileWriter("TestUnit.txt", "SYN", analyzer)

    def test_write_results(self):
        self.fileWriter.write_results()


def main():
    TestCase.main()


if __name__ == '__main__':
    main()

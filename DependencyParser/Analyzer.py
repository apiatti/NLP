import re

from Sentence import Sentence

ROOT = "root"


class Analyzer:
    def __init__(self, container, prog_name):
        self.container = container
        self.sentences = []
        self.prog_name = prog_name
        self.output = ""
        pass

    def populate_sentences(self):
        '''
        Search commas and populate the sentences (tokenizing them)
        @return: None
        '''
        self.search_commas()
        j = 0
        while j < len(self.container.ID):
            is_not_finished = True
            start = j

            while is_not_finished:
                j += 1
                is_not_finished = self.sentence_ends(j)
            self.create_sentence(start, j)

    def create_sentence(self, start, end):
        '''
        Populates a list of sentences with a Sentence object
        @param start: where the elements to add begin
        @param end: whete the elements to add end
        @return: None
        '''
        self.sentences.append(Sentence(self.container.ID[start: end], self.container.words[start: end],
                                       self.container.POS_dep[start: end], self.container.ID_dep[start: end],
                                       self.container.dep[start: end], None))

    def is_a_subj(self, dep):
        '''
        Checks whether the dependecy is a subject
        @param dep:
        @return: True or False
        '''
        if dep == "nsubj":
            return True
        return False

    def search_commas(self):
        for i in range(len(self.sentences)):
            self.check_dependency(self.sentences[i])
        print self.output

    def check_dependency(self, sentence):
        '''
        Corpus of the dependency analyzer, apply different steps
        @param sentence: the sentence to analyze
        @return: None
        '''
        root_id = self.get_ROOT_ID(sentence)
        for i in range(len(sentence.IDs_dep)):
            if self.is_a_subj(sentence.deps[i]):
                if root_id == sentence.IDs_dep[i]:
                    root_pos = self.get_ROOT_position(sentence, root_id)
                    n_commas = self.get_n_commas(sentence, i, root_pos)
                    if n_commas == 1 or n_commas >= 3:
                        self.get_sentences_matched(sentence, i, root_pos)

    def get_sentences_matched(self, sentence, i, root_pos):
        '''
        Constructs the sentence from token to token
        @param sentence: the sentence to analyze
        @param i: the position inside the sentence
        @param root_pos: the root position
        @return: None
        '''
        if root_pos > i:
            for j in range(len(sentence.words)):
                self.output += sentence.words[j] + " "
            self.output += "\n"
            for i in range(i, root_pos + 1):
                self.output += sentence.words[i] + " "
            self.output += "\n"

        else:
            for j in range(len(sentence.words)):
                self.output += sentence.words[j] + " "
            self.output += "\n"
            for i in range(root_pos, i + 1):
                self.output += sentence.words[i] + " "
            self.output += "\n"

        self.output += "\n"

    def get_ROOT_position(self, sentence, root_id):
        '''
        Returns the root position given and id and a sentence
        @param sentence: the sentence to analyze
        @param root_id: the root's id
        @return: the root position, -1 if no match for the given id is found
        '''
        for i in range(len(sentence.IDs)):
            if sentence.IDs[i] == root_id:
                return i
        return -1

    def get_ROOT_ID(self, sentence):
        '''
        Returns the root's id given a sentence
        @param sentence: the sentence to analyze
        @return: None
        '''
        for i in range(len(sentence.deps)):
            if sentence.deps[i].lower() == ROOT:
                return sentence.IDs[i]
        return -1

    # 0id, 1word, 2POS,3ID_DEP,4DEP
    def get_n_commas(self, sentence, i, root_pos):
        '''
        Returns the numbers of commas found in the sentence
        @param sentence: the sentence to analyze
        @param i: the pointer inside the sentence
        @param root_pos: the root position
        @return: number of commas found
        '''
        counter = 0
        if root_pos > i:
            for j in range(i, root_pos):
                if sentence.words[j] == ",":
                    counter += 1
        else:
            for j in range(root_pos, i):
                if sentence.words[j] == ",":
                    counter += 1
        return counter

    def print_sentences(self):
        '''
        Prints the sentences' words
        @return: None
        '''
        for i in range(len(self.sentences)):
            print self.sentences[i].words

    def sentence_ends(self, j):
        '''
        Returns True or False (switched) if a sentence is finished
        @param j: the word position
        @return: True or False
        '''
        if self.prog_name == "TINT":
            if j < len(self.container.words):
                word = self.container.words[j]
                if str(word[0]).isupper():
                    return False
                else:
                    return True
            return False
        else:
            if j < len(self.container.ID):
                if self.container.ID[j] == '1' or (
                            re.match("\d-\d", str(self.container.ID[j])) and self.container.words[j] == "-"):
                    return False
                else:
                    return True
            return False

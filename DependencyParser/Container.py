import ntpath
import re
import codecs

CONV_PATH = "./resources/tagset_conversion.txt"


class Container:
    def __init__(self, file_path):
        self.file_path = file_path
        self.IDs = []
        self.words = []
        self.POSs_dep = []
        self.IDs_dep = []
        self.deps = []
        self.TINT_TT = {}
        pass

    def populate(self, file_list):
        '''
        Populates the attributes given the program name
        @param file_list: the lines read
        @return: None
        '''
        prog_name = self.get_program_name()

        if prog_name == "TINT":
            for i in range(len(file_list)):
                if len(file_list[i][0]) >= 1:
                    self.populate_attrbutes(i, file_list)

            self.print_infos()

        if prog_name == "SYN" or prog_name == "SYND":
            for i in range(len(file_list)):
                #  Escape comments,sentences number
                if len(file_list[i][0]) >= 1 and not str(file_list[i][0]).startswith("#"):
                    self.populate_attrbutes(i, file_list)

            self.print_infos()

        elif prog_name == "UDP":
            i = 0
            while i < (len(file_list)):
                # Escape comments,sentences numbers and UDP strange lines (number - number ....)
                if len(file_list[i][0]) >= 1 and not str(file_list[i][0]).startswith("#"):
                    if re.match("\d+-\d+", str(file_list[i][0])):
                        i += 2
                    self.populate_attrbutes(i, file_list)

                i += 1
            self.print_infos()

    def read_conversion(self):
        '''
        Reads the conversions from file, populate a dictionary
        @return: None
        '''
        with codecs.open(CONV_PATH, "r") as f:
            for line in f:
                k = line.split("\t")[0]
                v = line.split("\t")[1].split(",")
                v = [value.replace("\n", "").replace("\r", "") for value in v]
                self.TINT_TT[k] = v

    def populate_attrbutes(self, i, file_list):
        '''
        Populates the useful attributes field of this class
        @param i: sentence's number
        @param file_list: the lines read
        @return: None
        '''
        self.IDs.append(file_list[i][0])
        self.words.append(file_list[i][1])
        self.POSs_dep.append(file_list[i][4])
        self.IDs_dep.append(file_list[i][6])
        self.deps.append(file_list[i][7])

    def print_infos(self):
        '''
        Prints some info  to the console, such as: file name and program name
        @return:
        '''
        print ">> %s file read: %s" % (self.get_program_name(), self.get_file_name())

    def print_words(self):
        '''
        Prints the words to the console formatted as a list

        @return: NOne
        '''
        print self.words

    def print_POSs(self):
        '''
        Prints the POS to the console formatted as a list
        @return: None
        '''
        print self.POSs_dep

    def get_program_name(self):
        '''
        Returns the program's name as a string
        @return:  program's name
        '''
        return str(ntpath.basename(self.file_path)).split("_")[0]

    def get_file_name(self):
        '''
        Returns the file name
        @return:  file name
        '''
        return str(ntpath.basename(self.file_path))

    def get_conversions(self):
        '''
        Returns a conversion dictionary from TT to Eagles tagset
        @return:  dictionary
        '''
        return self.TINT_TT

from Container import Container
from Analyzer import Analyzer
from FileReader import FileReader
from FileWriter import FileWriter

FILE_PATH = "../resources/Dependencies/"
FILE = "_10_testi_5SE.txt"

def analyze(prog_name):
    PATH = FILE_PATH + prog_name + FILE

    file_reader = FileReader(PATH)
    container = Container(PATH)

    container.populate(file_reader.parse_file())
    analyzer = Analyzer(container, prog_name)
    analyzer.populate_sentences()
    analyzer.search_commas()

    f_writer = FileWriter(FILE,prog_name,analyzer)
    f_writer.write_results()

################################################

analyze("UDP")
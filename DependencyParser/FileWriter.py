class FileWriter:
    SEPARATOR = "\n\n=======================================================\n\n"
    PATH = "../resources/Results/"

    def __init__(self, file_name, program_name, analyzer):
        self.file_name = file_name
        self.program_name = program_name
        self.analyzer = analyzer
        pass

    def write_results(self):
        '''
        Writes the results to a file given  the path
        @return: None
        '''
        with open(self.PATH + "Dependencies" + self.file_name, "a") as f:
            f.write(">>" + self.program_name + "\n")
            f.write(self.SEPARATOR)
            f.write(self.analyzer.output)
            f.write(self.SEPARATOR)

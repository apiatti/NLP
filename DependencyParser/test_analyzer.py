from unittest import TestCase

from DependencyParser.Analyzer import Analyzer
from DependencyParser.Container import Container
from DependencyParser.Sentence import Sentence


class TestAnalyzer(TestCase):
    analyzer = Analyzer(Container("./resources/GOLD_tagged/GOLD_5SE10.txt"), "GOLD")
    ids = [1, 2, 3, 4, 5]
    words = ["Ciao", ",", "come", "stai", "?"]
    pos = ["INTJ", "PUNCT", "SCONJ", "VERB", "PUNCT"]
    ids_dep = [0, 1, 4, 1, 1]
    dep = ["root", "punct", "mark", "advcl", "punct"]
    sentence = Sentence(ids, words, pos, ids_dep, dep, None)

    def test_is_a_subj(self):
        self.assertTrue(True, self.analyzer.is_a_subj("nsubj"))
        self.assertFalse(False, self.analyzer.is_a_subj("subj"))

    def test_get_n_commas(self):
        self.assertEquals(1, self.analyzer.get_n_commas(self.sentence, 5, 1))


def main():
    TestCase.main()


if __name__ == '__main__':
    main()

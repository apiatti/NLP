from unittest import TestCase

from TxtCleaner.FileReader import FileReader


class TestFileReader(TestCase):
    fileReader = FileReader("../resources/GOLD_tagged/GOLD_5SE10.txt")
    fileReader2 = FileReader("../resources/GOLD_tagged/noExists.txt")

    def test_parse_file(self):
        self.fileReader.parse_file()

        with self.assertRaises(IOError):
            self.fileReader2.parse_file()

    def test_remove_newlines(self):
        self.fileReader.remove_newlines()
        for i in range(len(self.fileReader.file_list)):
            for j in range(len(self.fileReader.file_list[i])):
                self.assertFalse("\n" in self.fileReader.file_list[i][j])


def main():
    TestCase.main()


if __name__ == '__main__':
    main()

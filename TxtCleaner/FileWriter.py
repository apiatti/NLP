import ntpath

OUTPUT_PATH = "../resources/ScripSit/ScripSitCleaned/"


class FileWriter:
    def __init__(self, filepath, cleaner):
        self.filepath = filepath
        self.cleaner = cleaner
        pass

    def write_results(self):
        with open(self.return_outputpath(), "w+") as f:
            for i in range(len(self.cleaner.sentences)):
                if self.cleaner.sentences[i] is not None:
                    f.write(self.cleaner.sentences[i].text+"\n")


    def get_file_name(self):
        return ntpath.basename(self.filepath)

    def return_outputpath(self):
        return OUTPUT_PATH + self.get_file_name()

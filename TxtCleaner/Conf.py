# -*- coding: utf-8 -*-
#
# Chars useful to clean a sentence
#
PUNCT_CHARS = ".,"
SPECIAL_CHARS = "!(){};:=|<>\"@#/_-'*"

#
# Factors, lengths, ...
#
PUNCT_FACTOR = 2
SPECIAL_FACTOR = 10
SEN_LENGTH = 4
EQUALITY_PERC = 0.7

#
# Patterns used to clean a sentence
#
PATTERNS = ["(\d\\.)", "Figura", "Indice", "Index", "Capitolo", "Abstract", "Tabella", "Sommario",
            "Elenco delle figure", "Elenco", "Numero", "Obiettivi", "Scopo", "Requisiti", "Listato", "Descrizione",
            "Riassunto", "Ringraziamenti", "•", "Apple", "Android", "Java", "Tecnologie", ""]
ROOT_TYPES = ["VERB", "NOUN", "ADJ"]

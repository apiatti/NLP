import os

from Cleaner import Cleaner
from Container import Container
from FileReader import FileReader
from FileWriter import FileWriter

DIR = "../resources/ScripSit/ScripSitTexts"


def analyze():
    for filename in os.listdir(DIR):
        filepath = os.path.join(DIR, filename)
        print "Cleaning: " + filepath

        file_reader = FileReader(filepath)
        container = Container(filepath)

        container.populate(file_reader.parse_file())
        cleaner = Cleaner(container)
        cleaner.populate_sentences()
        cleaner.clean_text()

        fwriter = FileWriter(filepath, cleaner)
        fwriter.write_results()


################################################

analyze()

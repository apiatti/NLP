import codecs


class FileReader:
    def __init__(self, path):
        self.path = path
        self.file_list = []
        pass

    def parse_file(self):
        '''
        Reads the file splitting by tab
        @return:
        '''
        with codecs.open(self.path, "r") as f:
            self.file_list = [line.split("\t") for line in f]
        self.remove_newlines()
        return self.file_list

    def remove_newlines(self):
        '''
        Removes new lines from read text
        @return:
        '''
        for i in range(len(self.file_list)):
            for j in range(len(self.file_list[i])):
                self.file_list[i][j] = self.file_list[i][j].replace("\n", "")

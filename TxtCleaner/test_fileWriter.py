from unittest import TestCase

from TxtCleaner.Cleaner import Cleaner
from TxtCleaner.Container import Container
from TxtCleaner.FileWriter import FileWriter

OUTPUT_PATH = "../resources/ScripSitCleaned/"


class TestFileWriter(TestCase):
    cleaner = Cleaner(Container("./resources/GOLD_tagged/GOLD_5SE10.txt"))
    fileWriter = FileWriter("TestUnit.txt", cleaner)

    def test_write_results(self):
        self.fileWriter.write_results()

    def test_get_file_name(self):
        self.assertEqual("TestUnit.txt", self.fileWriter.get_file_name())

    def test_return_outputpath(self):
        self.assertEquals("../resources/ScripSitCleaned/TestUnit.txt", self.fileWriter.return_outputpath())


def main():
    TestCase.main()


if __name__ == '__main__':
    main()

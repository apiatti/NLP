import re
import enchant
import Conf

from Sentence import Sentence

ROOT = "root"


class Cleaner:
    def __init__(self, container):
        self.container = container
        self.sentences = []
        pass

    def populate_sentences(self):
        '''
        Populate the list of sentences given a Container object
        @return: None
        '''
        j = 0
        z = 0
        while j < len(self.container.IDs):
            is_not_finished = True
            start = j

            while is_not_finished:
                j += 1
                is_not_finished = self.sentence_ends(j)
            self.create_sentence(start, j, z)
            z += 1

    def clean_text(self):
        '''
        Apply criterias to clean the whole text
        @return: None
        '''
        for i in range(len(self.sentences)):
            self.remove_has_noroots(self.sentences[i], i)
            self.remove_nosense_roots(self.sentences[i], i)
            self.remove_strange_sentences(self.sentences[i], i, Conf.PUNCT_CHARS, Conf.PUNCT_FACTOR)
            self.remove_strange_sentences(self.sentences[i], i, Conf.SPECIAL_CHARS, Conf.SPECIAL_FACTOR)
            self.remove_short_sentences(self.sentences[i], i)
            self.remove_english_sentences(self.sentences[i], i)
            self.remove_matches(self.sentences[i], i)

    def create_sentence(self, start, end, z):
        """
        Append a Sentence object in the list of sentences
        @param start: where the elements  to add begin
        @param end: where the elements to add end
        @param z: the sentence's number
        @return: None
        """
        self.sentences.append(
            Sentence(self.container.IDs[start: end], self.container.lemmas[start:end], self.container.words[start: end],
                     self.container.POSs_dep[start: end], self.container.IDs_dep[start: end],
                     self.container.deps[start: end],
                     self.remove_header(self.container.sentences[z])))

    def remove_has_noroots(self, sentence, i):
        """
        Removes the sentence if no root is found
        @param sentence: the sentence to analyze
        @param i: the sentence's number
        @return: None
        """
        if sentence is not None:
            if "root" not in sentence.deps:
                print sentence.text
                self.sentences[i] = None

    def remove_nosense_roots(self, sentence, i):
        '''
        Removes a sentence if has no root matched against a list of possibles roots
        @param sentence: the sentence to analyze
        @param i: the sentence's number
        @return: None
        '''
        if sentence is not None:
            root_pos = self.get_ROOT_position(sentence)
            if sentence.POSs_dep[root_pos] not in Conf.ROOT_TYPES:
                print sentence.text
                self.sentences[i] = None

    def remove_strange_sentences(self, sentence, j, chars_list, factor):
        '''
        Removes sentences with too much strange symbols or punctation
        @param sentence: the sentence to analyze
        @param j: sentence's position
        @param chars_list: the symbol list used
        @param factor: the divIDser factor
        @return: None
        '''
        if sentence is not None:
            counts = tuple(sentence.text.count(c) for c in chars_list)
            for i in range(len(counts)):
                if counts[i] >= (len(sentence.words) - counts[i]) / factor:
                        print sentence.text
                        self.sentences[j] = None
                        break

    def remove_short_sentences(self, sentence, i):
        '''
        Removes the short sentences according to the constant SEN_LENGTH
        @param sentence: the sentence to analyze
        @param i: the sentence's position
        @return: None
        '''
        if sentence is not None:
            if len(sentence.words) <= Conf.SEN_LENGTH:
                print sentence.text

                self.sentences[i] = None

    def remove_english_sentences(self, sentence, j):
        '''
        Removes the sentence if english text is found. This is achieved by comparing lemma and word with an equality factor.
        Although this technique removes english and foreign text it also removes other types of sentences.
        @param sentence: the sentence to clean
        @param j: the sentence's number
        @return: None
        '''
        if sentence is not None:
            n_words = len(sentence.words)
            counter = 0
            for i in range(n_words):
                if sentence.words[i] == sentence.lemmas[i]:
                    counter += 1
            if counter >= n_words * Conf.EQUALITY_PERC:
                print sentence.text

                self.sentences[j] = None


    def get_ROOT_position(self, sentence):
        '''
        Returns the root position
        @param sentence: the sentence where to find the root
        @return: the root position, -1 if no root has been found
        '''
        if sentence is not None:
            for i in range(len(sentence.deps)):
                if sentence.deps[i].lower() == ROOT:
                    return i
            return -1

    def sentence_ends(self, j):
        '''
        Checks if the sentence is finished
        @param j: the sentence number
        @return: True or False
        '''
        if j < len(self.container.IDs):
            if self.container.IDs[j] == '1' or (
                        re.match("\d-\d", str(self.container.IDs[j])) and self.container.words[j] == "-"):
                return False
            else:
                return True
        return False

    def remove_header(self, text):
        '''
        Removes comments from text
        @param text: the text to clean
        @return: None
        '''
        return str(text).replace("# text = ", "")

    def remove_matches(self, sentence, z):
        '''
        Removes matches found in Conf.PATTERNS from the sentences
        @param sentence: the sentence to apply the regex
        @param z: the sentence number
        @return: None
        '''
        if sentence is not None:
            for i in range(len(Conf.PATTERNS)):
                if re.match("\d+", sentence.words[0]):
                    print sentence.text
                    self.sentences[z] = None
                    break

                for j in range(len(sentence.words)):
                    if re.match(Conf.PATTERNS[i], sentence.words[0]):
                        print sentence.text

                        self.sentences[z] = None
                        break

    # PRINTS section
    ############################################
    def print_sentences(self):
        '''
        Prints the sentence's text to the console
        @return: None
        '''
        for i in range(len(self.sentences)):
            if self.sentences[i] is not None:
                print self.sentences[i].text

    def print_remaining_roots(self):
        '''
        Prints the roots to the console
        @return: None
        '''
        for i in range(len(self.sentences)):
            if self.sentences[i] is not None:
                for j in range(len(self.sentences[i].deps)):
                    if self.sentences[i].deps[j] == "root":
                        print self.sentences[i].words[j], self.sentences[i].POSs_dep[j]

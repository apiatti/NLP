SUBSTRING = "text"


class Container:
    def __init__(self, file_path):
        self.file_path = file_path
        self.IDs = []
        self.lemmas = []
        self.sentences = []
        self.words = []
        self.POSs_dep = []
        self.IDs_dep = []
        self.deps = []
        pass

    def populate(self, file_list):
        '''
        Populates the attributes paying attention to comments
        @param file_list: the lines read
        @return: None
        '''

        i = 0
        while i < (len(file_list)):
            if len(file_list[i][0]) >= 1:
                if str(file_list[i][0]).startswith("#"):
                    if SUBSTRING in str(file_list[i][0]):
                        self.sentences.append(file_list[i][0])
                elif not str(file_list[i][0]).startswith("#"):
                    self.populate_attrbutes(i, file_list)

            i += 1

    def populate_attrbutes(self, i, file_list):
        '''
        Populates each class field (useful read attributes)
        @param i: line's read number
        @param file_list: the lines read
        @return: None
        '''
        self.IDs.append(file_list[i][0])
        self.words.append(file_list[i][1])
        self.lemmas.append(file_list[i][2])
        self.POSs_dep.append(file_list[i][3])
        self.IDs_dep.append(file_list[i][6])
        self.deps.append(file_list[i][7])

    def print_words(self):
        '''
        Prints the words read, formatted as a list
        @return: None
        '''
        print self.words

    def print_POSs(self):
        '''
        Prints the POS read, formatted as a list
        @return: None
        '''
        print self.POSs_dep

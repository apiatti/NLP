#
# Program's names
#
UDP = "UDP"
TINT = "TINT"
TT = "TT"
SYN = "SYN"
SYND = "SYND"

#
# Spell checked or not
#
CHECK = "spell_checked"
UNCHECK = "unchecked"

#
# Tags' indexes used for matrixes
#
TAGS_INDEXES = {"A+D": 0, "B": 1, "C": 2, "E+R": 3, "P": 4, "S": 5, "Verbs": 6}
TAGS_INDEXES_EXT = {"A+D": 0, "B": 1, "C": 2, "E+R": 3, "P": 4, "PC": 5, "PRO-R": 6, "S": 7, "Verbs": 8}
VERBS_INDEXES = {"V": 0, "VA": 1, "VM": 2}

#
# Almost all tags
#
TAGS_DICT = {
    "A+D": ["A", "AP", "DE", "DI", "DQ", "DR", "DD", "CHE:adj", "DET:poss", "ADJ", "DET:indef", "DET:wh", "DET:demo"],
    "S": ["S", "SP", "NOUN", "NPR"],
    "P": ["PC", "PD", "PE", "PI", "PP", "PQ", "PR", "CLI", "PRO:demo", "PRO:pers", "PRO:indef", "PRO:poss",
          "PRO:rel", "CHE:rel", "WH"],
    "B": ["B", "BN", "ADV", "ADV:mente", "NEG"],
    "C": ["CC", "CS", "CON"],
    "E+R": ["RD", "RI", "ART", "E", "EA", "PRE", "ARTPRE", "E+RD"],
    "Verbs": ["VA", "VM", "V", "AUX:fin", "AUX:fin:cli", "AUX:infi", "AUX:infi:cli", "AUX:geru", "AUX:geru:cli",
              "AUX:ppre",
              "AUX:ppast",
              "VER2:fin", "VER2:fin:cli", "VER2:infi", "VER2:infi:cli", "VER2:geru", "VER2:geru:cli", "VER2:ppre",
              "VER2:ppast", "VER:fin", "VER:fin:cli", "VER:infi", "VER:infi:cli", "VER:geru", "VER:geru:cli",
              "VER:ppre",
              "VER:ppast",
              "VER:ppast:cli"]
}

TAGGED_TAGS = ["A+D", "B", "C", "E+R", "P", "S", "Verbs"]

#
# Nouns and pronouns
#
TAGGED_CLI_PRO = ["PC", "PRO-R"]
NOUNS_PRO_DICT = {"Nouns": ["NOUN", "S", "SP", "NPR"],
                  "PRO": ["CLI", "PRO:demo", "PRO:pers", "PRO:indef", "PRO:poss", "WH", "PC", "PD", "PE", "PI", "PP",
                          "PQ"],
                  "PRO-R": ["PRO:rel", "CHE:rel", "PR"]}

#
# VERBS tags
#
VERBS_DICT = {
    "V": ["V", "VER:fin", "VER:fin:cli", "VER:infi", "VER:infi:cli", "VER:geru", "VER:geru:cli", "VER:ppre",
          "VER:ppast",
          "VER:ppast:cli"],
    "VA": ["VA", "AUX:fin", "AUX:fin:cli", "AUX:infi", "AUX:infi:cli", "AUX:geru", "AUX:ger:cli", "AUX:ppre",
           "AUX:ppast"],
    "VM": ["VM", "VER2:fin", "VER2:fin:cli", "VER2:infi", "VER2:infii:cli", "VER2:geru", "VER2:geru:cli", "VER2:ppre",
           "VER2:ppast"]
}

TAGGED_VERBS = ["V", "VA", "VM"]

#
# Verbs forms and tenses
#
VERBS_FORM_DICT = {"Fin": ["fin"], "Inf": ["infi"], "Ger": ["geru"], "Part": ["ppast", "ppres"],
                   "Past": ["ppast", "ppres"],
                   "Pres": ["ppast", "ppres"]}

VERBS_FORM_LIST = ["Fin", "Inf", "Ger", "Part", "Past", "Pres"]
TAGGED_TENSE = ["Past", "Pres"]
TAGGED_TENSE_DICT = {"Past": ["Past", "ppast"], "Pres": ["Pres", "ppres"]}
INDEF_LIST = ["Inf", "Ger", "Part", "Past", "Pres", "fin", "infi", "geru", "ppast", "ppres"]

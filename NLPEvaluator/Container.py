import ntpath
import re
import codecs

from Morpho import Morpho

CONV_PATH = "./resources/Utils/tagset_conversion.txt"


class Container:
    def __init__(self, file_path):
        self.file_path = file_path
        self.words = []
        self.POS_dip = []
        self.POS_indip = []
        self.lemmas = []
        self.morpho = []
        self.morpho_splitted = []
        self.TINT_TT = {}
        pass

    def populate_attributes(self, file_list):
        '''
        Populate each attribute given the program's name
        @param file_list: lines read
        @return: None
        '''
        prog_name = self.get_program_name()

        self.read_conversion()
        if prog_name == "TT":
            for i in range(len(file_list)):
                self.words.append(file_list[i][0])
                self.POS_dip.append(file_list[i][1])
                self.lemmas.append(file_list[i][2])
                self.morpho.append(file_list[i][1])
            self.populate_TT_morpho()

            self.print_infos()

        elif prog_name == "GOLD" or prog_name == "150":
            for i in range(len(file_list)):
                if len(file_list[i]) >= 3:
                    self.words.append(file_list[i][0])
                    self.POS_dip.append(file_list[i][1])
                    self.lemmas.append(file_list[i][2])
                # elif len(file_list[i]) == 2 or len(file_list[i]) == 1:
                #     self.words.append("$")
                #     self.POS_dip.append("$")
                #     self.lemmas.append("$")

            self.print_infos()

        elif prog_name == "TINT":
            for i in range(len(file_list)):
                if len(file_list[i][0]) >= 1:
                    self.words.append(file_list[i][1])
                    self.lemmas.append(file_list[i][2])
                    self.POS_dip.append(file_list[i][4])
                    self.morpho.append("")
            self.populate_morpho()
            self.print_infos()

        elif prog_name == "SYN" or prog_name == "SYND":
            for i in range(len(file_list)):
                if len(file_list[i][0]) >= 1 and not str(file_list[i][0]).startswith("#"):
                    self.words.append(file_list[i][1])
                    self.lemmas.append(None)
                    self.POS_indip.append(file_list[i][3])
                    self.POS_dip.append(file_list[i][4])
                    self.morpho.append(file_list[i][5])
            self.populate_morpho()
            self.print_infos()

        elif prog_name == "UDP":
            i = 0
            while i < (len(file_list)):
                # Escape comments
                if len(file_list[i][0]) >= 1 and not str(file_list[i][0]).startswith("#"):
                    if re.match("\d+-\d+", str(file_list[i][0])):
                        i += 2
                    print file_list[i][1]
                    self.words.append(file_list[i][1])
                    self.lemmas.append(file_list[i][2])
                    self.POS_indip.append(file_list[i][3])
                    self.POS_dip.append(file_list[i][4])
                    self.morpho.append(file_list[i][5])

                i += 1
            self.populate_morpho()
            self.print_infos()

    def read_conversion(self):
        '''
         Reads the conversions from file, populate a dictionary
         @return: None
         '''
        with codecs.open(CONV_PATH, "r") as f:
            for line in f:
                k = line.split("\t")[0]
                v = line.split("\t")[1].split(",")
                v = [value.replace("\n", "").replace("\r", "") for value in v]
                self.TINT_TT[k] = v

    def populate_morpho(self):
        '''
        Populate the Eagle's morphology list by double-splitting the tag
        @return:
        '''
        for i in range(len(self.POS_dip)):
            morpho = Morpho("", "", "", "", "", "")
            temp = [item.split("=") for item in re.split("\|", self.morpho[i])]
            for j in range(len(temp)):
                if temp[j][0] == "Mood":
                    morpho.set_mood(temp[j][1])
                elif temp[j][0] == "Number":
                    morpho.set_number(temp[j][1])
                elif temp[j][0] == "Person":
                    morpho.set_person(temp[j][1])
                elif temp[j][0] == "Tense":
                    morpho.set_tense(temp[j][1])
                elif temp[j][0] == "VerbForm":
                    morpho.set_form(temp[j][1])
                elif temp[j][0] == "fPOS" and self.get_program_name() == "SYN":
                    morpho.set_fPOS(temp[j][1])
            self.morpho_splitted.append(morpho)

    def populate_TT_morpho(self):
        '''
        Populate the TT's morphology list by splitting the tag
        @return: None
        '''
        for i in range(len(self.POS_dip)):
            morpho = Morpho("", "", "", "", "", "")
            temp = re.split(":", self.morpho[i])
            if len(temp) > 1:
                morpho.set_tense(temp[1])
                morpho.set_form(temp[1])
            self.morpho_splitted.append(morpho)

    def print_words(self):
        '''
        Prints the words as list to the console
        @return:
        '''
        print self.words

    def print_POSs(self):
        '''
        Prints the POS tags as list to the console
        @return:
        '''
        print self.POS_dip

    def get_program_name(self):
        '''
        Returns the program's name as a string
        @return:  program's name
        '''
        return str(ntpath.basename(self.file_path)).split("_")[0]

    def get_file_name(self):
        '''
        Returns the file name
        @return:  file name
        '''
        return str(ntpath.basename(self.file_path))

    def print_infos(self):
        '''
        Prints some info  to the console, such as: file name and program name
        @return: None
        '''
        print ">> %s file read: %s" % (self.get_program_name(), self.get_file_name())

    def get_conversions(self):
        '''
        Returns a conversion dictionary from TT to Eagles tagset
        @return: dictionary
        '''
        return self.TINT_TT

    def get_splitted_gold_morpho(self, gold_tagged):
        '''
        Splits verbs morphology (TT)
        @param gold_tagged: the TAG to split
        @return: list
        '''
        return gold_tagged.split(":")

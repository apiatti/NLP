import ntpath
import codecs


class TaggedReader:
    def __init__(self, path):
        self.path = path
        self.file_list = []
        pass

    def parse_file(self):
        '''
        Reads and splits by tab the file
        @return: list lines read
        '''
        with codecs.open(self.path, "r") as f:
            self.file_list = [line.split("\t") for line in f]
        self.remove_newlines()
        self.print_file_name()
        return self.file_list

    def remove_newlines(self):
        '''
        Removes new lines \n from read lines
        @return: None
        '''
        for i in range(len(self.file_list)):
            for j in range(len(self.file_list[i])):
                self.file_list[i][j] = self.file_list[i][j].replace("\n", "")
                # self.print_list()


    def print_file_name(self):
        '''
        Prints file's name
        @return: None
        '''
        print "list loaded from file: " + ntpath.basename(self.path).upper()

import Conf


class FileWriter:
    SEPARATOR = "\n\n=======================================================\n\n"
    PATH = "./resources/Results/10_testi_5SE_"

    def __init__(self, macro_stats, stat_cs, micro_stats, header, program_name):
        self.macro_stats = macro_stats
        self.stat_cs = stat_cs
        self.header = header
        self.micro_stats = micro_stats
        self.program_name = program_name
        pass

    def write_statistics(self):
        '''
        Writes all the detailed-statiscs to the corresponding Results.txt file
        @return: None
        '''
        with open(self.PATH + self.program_name + "_Results.txt", "a") as f:
            f.write(" ".join(map(str, self.header)))
            f.write(self.SEPARATOR)
            f.write(self.macro_stats.return_unequals_tags())
            #f.write(self.macro_stats.return_compound_words())
            f.write(self.micro_stats.return_nouns_prons_accuracy())
            f.write(self.micro_stats.return_verbs_accuracy("TAGS", Conf.TAGGED_VERBS))
            f.write(self.micro_stats.return_verbs_accuracy("MORPHOLOGY", Conf.VERBS_FORM_LIST))
            f.write(self.macro_stats.return_stats())
            if self.stat_cs is not None:
                f.write("\n->Delta (spell-unchecked vs spell-spell_checked): %.2f%% " % (
                    self.stat_cs.get_accuracy() - self.macro_stats.get_accuracy()))

            f.write(self.SEPARATOR)

    def write_overall_accuracies(self, output):
        '''
        Writes the general-statistcs to the corresponding Results.txt file
        @param output: the output to be written
        @return: None
        '''
        with open(self.PATH + self.program_name + "_Results.txt", "a") as f:
            f.write(output)

    def write_confusion_matrixes(self, output):
        '''
        Writes the confusion-matrixes to the corresponding Results.txt file
        @param output: the output to be written
        @return: None
        '''
        with open(self.PATH + self.program_name + "_Results.txt", "a") as f:
            f.write(output)

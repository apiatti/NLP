import re
import Conf
from Matrix import Matrix
import copy


class MicroStatistics:
    #
    # Useful static costants
    #
    total_fPOS = 0
    matrix_list = []
    overall_counter = {"VA": 0, "VM": 0, "V": 0, "Fin": 0, "Inf": 0, "Ger": 0, "Part": 0, "Past": 0, "Pres": 0,
                       "Nouns": 0, "PRO": 0, "PRO-R": 0, "PC": 0, "A+D": 0, "B": 0, "C": 0, "E+R": 0, "P": 0,
                       "S": 0, "Verbs": 0}
    counter = 0

    @staticmethod
    def create_matrixes():
        '''
        Create all the needed confusion matrixes
        @return: None
        '''
        matrixes = MicroStatistics.matrix_list
        matrixes.append(Matrix(Conf.TAGGED_VERBS, Conf.TAGGED_VERBS, 3, 3, "Verbs Confusion Matrix"))
        matrixes.append(Matrix(Conf.TAGGED_TAGS, Conf.TAGGED_TAGS, 7, 7, "Tags Confusion Matrix"))
        matrixes.append(
            Matrix(Conf.VERBS_FORM_LIST[0:2], Conf.VERBS_FORM_LIST[0:2], 2, 2, "Morphology Confusion Matrix"))
        tmp_list = Conf.TAGGED_TAGS[:]
        tmp_list.insert(5, "PC")
        tmp_list.insert(6, "PRO-R")
        matrixes.append(Matrix(Conf.TAGGED_CLI_PRO, tmp_list, 2, 9, "Pronouns Confusion Matrix"))

    def __init__(self, truth_container, tagged_container):
        if not MicroStatistics.matrix_list:
            MicroStatistics.create_matrixes()
        MicroStatistics.total_fPOS += len(truth_container.words)
        self.truth_container = truth_container
        self.tagged_container = tagged_container
        self.nouns_prons_tagged = {"Nouns": 0, "PRO": 0, "PRO-R": 0}
        self.nouns_prons_total = {"Nouns": 0, "PRO": 0, "PRO-R": 0}
        self.__fPOS_counter = 0
        self.tags_association = {"Nouns": [], "PRO": [], "PRO-R": [], "fPOS": []}
        self.verbs_associations = {"VA": [], "VM": [], "V": [], "Fin": [], "Inf": [], "Ger": [], "Part": [], "Past": [],
                                   "Pres": []}
        self.overall_cnt_temp = copy.deepcopy(MicroStatistics.overall_counter)

        self.verbs_total = {"VA": 0, "VM": 0, "V": 0, "Fin": 0, "Inf": 0, "Ger": 0, "Part": 0, "Past": 0, "Pres": 0}
        self.verbs_tagged = {"VA": 0, "VM": 0, "V": 0, "Fin": 0, "Inf": 0, "Ger": 0, "Part": 0, "Past": 0, "Pres": 0}
        self.compare_fPOS()
        pass

    def count_tags(self, truth):
        '''
        Counts all the tags, by invoking dedicated counting functions
        @param truth: the GOLD tag
        @return: None
        '''
        self.nouns_prons_counter(truth)
        self.count_other_tags(truth)
        if len(self.get_splitted_gold_morpho(truth)) > 1:
            self.count_verbs_form(self.get_splitted_gold_morpho(truth)[1])  # form

    def substract_counter(self):
        """
        Substract bestscore, in order to not count a file's score if this one is corrupted
        @return:
        """
        keys = self.overall_cnt_temp.keys()
        for i in range(len(keys)):
            MicroStatistics.overall_counter[keys[i]] -= self.overall_cnt_temp[keys[i]]





    def nouns_prons_counter(self, truth):
        '''
        Counts all the GOLD nouns/pronouns tags
        @param truth: the GOLD tag
        @return: None
        '''
        for i in range(len(Conf.NOUNS_PRO_DICT.keys())):
            if truth in Conf.NOUNS_PRO_DICT[Conf.NOUNS_PRO_DICT.keys()[i]]:
                self.nouns_prons_total[Conf.NOUNS_PRO_DICT.keys()[i]] += 1
                MicroStatistics.overall_counter[Conf.NOUNS_PRO_DICT.keys()[i]] += 1
                self.overall_cnt_temp[Conf.NOUNS_PRO_DICT.keys()[i]] += 1

        if truth == "CLI":
            MicroStatistics.counter += 1
            MicroStatistics.overall_counter["PC"] += 1
            self.overall_cnt_temp["PC"] += 1

    def count_other_tags(self, truth):
        '''
               Counts all the GOLD remaining tags
               @param truth: the GOLD tag
               @param tags_list: the corresponding taglist
               @param tags_dict: the dictionary used to store the corresponding counters
               @return: None
               '''
        for i in range(len(Conf.TAGGED_TAGS)):
            if truth in Conf.TAGS_DICT[Conf.TAGGED_TAGS[i]]:
                MicroStatistics.overall_counter[Conf.TAGGED_TAGS[i]] += 1
                self.overall_cnt_temp[Conf.TAGGED_TAGS[i]] += 1

        self.tags_counter(truth, Conf.TAGGED_VERBS, Conf.VERBS_DICT)

    def count_verbs_form(self, morpho):
        '''
        Counts all the GOLD verbs morphology
        @param truth: the GOLD tag
        @param tags_list: the corresponding taglist
        @param tags_dict: the dictionary used to store the corresponding counters
        @return: None
        '''
        for i in range(len(Conf.VERBS_FORM_LIST)):
            if morpho in Conf.VERBS_FORM_DICT[Conf.VERBS_FORM_LIST[i]]:
                self.verbs_total[Conf.VERBS_FORM_LIST[i]] += 1
                MicroStatistics.overall_counter[Conf.VERBS_FORM_LIST[i]] += 1
                self.overall_cnt_temp[Conf.VERBS_FORM_LIST[i]] += 1

    def tags_counter(self, truth, tags_list, tags_dict):
        '''
        Counts all the GOLD tags
        @param truth: the GOLD tag
        @param tags_list: the corresponding taglist
        @param tags_dict: the dictionary used to store the corresponding counters
        @return: None
        '''
        for i in range(len(tags_list)):
            if truth in tags_dict[tags_list[i]]:
                self.verbs_total[tags_list[i]] += 1
                MicroStatistics.overall_counter[tags_list[i]] += 1
                self.overall_cnt_temp[tags_list[i]] += 1

    def get_splitted_gold_morpho(self, gold_tagged):
        # 0 type 1form 2tense
        '''
        Splits the GOLD tags morphology
        @param gold_tagged: the GOLD tag's morphology
        @return: list words
        '''
        return gold_tagged.split(":")

    def compare_micro_morpho(self, truth_morpho, tagged_morpho, word):
        '''
        Compares the morphology of the verbs
        @param truth_morpho: the GOLD morphology
        @param tagged_morpho: the tagged morphology
        @param word: the corresponding words
        @return: None
        '''
        truth = ""
        tagged_tense = ""
        tagged_form = ""

        if len(truth_morpho) > 1:
            truth = truth_morpho[1]
            tagged_form = tagged_morpho.get_form()
            tagged_tense = tagged_morpho.get_tense()

        for i in range(len(Conf.TAGGED_TENSE)):
            if truth in Conf.TAGGED_TENSE_DICT[Conf.TAGGED_TENSE[i]] and (
                        tagged_tense in Conf.TAGGED_TENSE_DICT[
                        Conf.TAGGED_TENSE[i]]):
                self.verbs_tagged[Conf.TAGGED_TENSE[i]] += 1

            elif truth in Conf.TAGGED_TENSE_DICT[Conf.TAGGED_TENSE[i]] and (
                        tagged_tense not in Conf.TAGGED_TENSE_DICT[
                        Conf.TAGGED_TENSE[i]]):
                tmp = [word, truth, tagged_tense]
                self.verbs_associations[Conf.TAGGED_TENSE[i]].append(tmp)

        # check the form
        if truth == "fin" and tagged_form.lower() == "fin":
            self.verbs_tagged["Fin"] += 1
            MicroStatistics.matrix_list[2].increment_diagonal(0)
        elif truth == "fin" and tagged_form.lower() != "Fin":
            tmp = [word, truth, tagged_form]
            self.verbs_associations["Fin"].append(tmp)
            self.add_to_confusion_morpho(0, tagged_form)

        if truth == "infi" and (tagged_form == "Inf" or tagged_form == "infi"):
            self.verbs_tagged["Inf"] += 1
            MicroStatistics.matrix_list[2].increment_diagonal(1)

        elif truth == "infi" and (tagged_form != "Inf" or tagged_form != "infi"):
            tmp = [word, truth, tagged_form]
            self.verbs_associations["Inf"].append(tmp)
            self.add_to_confusion_morpho(1, tagged_form)

        if truth == "geru" and (tagged_form == "Ger" or tagged_form == "geru"):
            self.verbs_tagged["Ger"] += 1
            MicroStatistics.matrix_list[2].increment_diagonal(1)

        elif truth == "geru" and (tagged_form != "Ger" or tagged_form != "geru"):
            tmp = [word, truth, tagged_form]
            self.verbs_associations["Ger"].append(tmp)
            self.add_to_confusion_morpho(1, tagged_form)

        if (truth == "ppast" or truth == "ppres") and (
                            tagged_form == "Part" or tagged_form == "ppast" or tagged_form == "ppres"):
            tmp = self.verbs_tagged.get("Part")
            self.verbs_tagged["Part"] += 1
            MicroStatistics.matrix_list[2].increment_diagonal(1)


        elif (truth == "ppast" or truth == "ppres") and (
                            tagged_form != "Part" or tagged_form != "ppast" or tagged_form != "ppres"):
            tmp = [word, truth, tagged_form]
            self.verbs_associations["Part"].append(tmp)
            self.add_to_confusion_morpho(1, tagged_form)

    def compare_verbs(self, truth_tag, tagged_tag, word):
        '''
        Compares the verbs tags
        @param truth_tag: the TAG tagged
        @param tagged_tag: the GOLD tag
        @param word: the corresponding word
        @return: None
        '''
        for i in range(len(Conf.TAGGED_VERBS)):
            if truth_tag in Conf.VERBS_DICT[Conf.TAGGED_VERBS[i]] and (
                        tagged_tag in Conf.VERBS_DICT[Conf.TAGGED_VERBS[i]]):
                self.verbs_tagged[Conf.TAGGED_VERBS[i]] += 1
                MicroStatistics.matrix_list[0].increment_diagonal(Conf.VERBS_INDEXES[Conf.TAGGED_VERBS[i]])


            elif truth_tag in Conf.VERBS_DICT[Conf.TAGGED_VERBS[i]] and (tagged_tag not in Conf.VERBS_DICT[
                Conf.TAGGED_VERBS[i]]):

                tmp = [word, truth_tag, tagged_tag]
                self.verbs_associations[Conf.TAGGED_VERBS[i]].append(tmp)
                self.add_to_confusion_verbs(Conf.VERBS_INDEXES[Conf.TAGGED_VERBS[i]], tagged_tag)

    def compare_micro_tags(self, truth_tag, tagged_tag, word):
        '''
        Compares the detailed tags
        @param truth_tag: the TAG tagged
        @param tagged_tag: the GOLD tag
        @param word: the corresponding word
        @return: None
        '''
        self.populate_confusion_tags_matrix(tagged_tag, truth_tag)
        self.populate_cli_pror_matrix(tagged_tag, truth_tag)
        self.compare_verbs(truth_tag, tagged_tag, word)

        for i in range(len(Conf.NOUNS_PRO_DICT.keys())):
            if truth_tag in Conf.NOUNS_PRO_DICT[Conf.NOUNS_PRO_DICT.keys()[i]] and tagged_tag in Conf.NOUNS_PRO_DICT[
                Conf.NOUNS_PRO_DICT.keys()[i]]:
                self.nouns_prons_tagged[Conf.NOUNS_PRO_DICT.keys()[i]] += 1
            elif truth_tag in Conf.NOUNS_PRO_DICT[Conf.NOUNS_PRO_DICT.keys()[i]] and tagged_tag not in \
                    Conf.NOUNS_PRO_DICT[
                        Conf.NOUNS_PRO_DICT.keys()[i]]:
                tmp = [word, truth_tag, tagged_tag]
                self.tags_association[Conf.NOUNS_PRO_DICT.keys()[i]].append(tmp)

    def compare_fPOS(self):
        '''
        Compares the fPOS tag with the corresponding tag
        @return: None
        '''
        if self.tagged_container.get_program_name() == "SYN":
            for i in range(len(self.tagged_container.POS_dip)):
                fPOS_indip = self.split_fPOS(self.tagged_container.morpho_splitted[i].get_fPOS())[0]
                fPOS_dip = self.split_fPOS(self.tagged_container.morpho_splitted[i].get_fPOS())[2]
                POS_dip = self.tagged_container.POS_dip[i]
                POS_indip = self.tagged_container.POS_indip[i]

                if POS_dip == fPOS_dip and POS_indip == fPOS_indip:
                    self.__fPOS_counter += 1
                else:
                    tmp = [self.tagged_container.words[i], POS_indip + " " + POS_dip, fPOS_indip + " " + fPOS_dip]
                    self.tags_association["fPOS"].append(tmp)

    def split_fPOS(self, fPOS):
        '''
        Splits the fPOS tag
        @param fPOS: the fPOS tag
        @return: list words
        '''
        return re.split("\+?", fPOS)

    def get_nouns_prons_accuracy(self, tag):
        '''
         Returns the nouns/pronouns' accuracy, 0.0 if no nouns/pronouns' tags  has been found
         @return: float accuracy
         '''
        if self.nouns_prons_total[tag] > 0:
            return float(self.nouns_prons_tagged[tag]) / self.nouns_prons_total[tag]
        else:
            return 0.0

    def get_verb_accuracy(self, TAG):
        '''
         Returns the verbs' accuracy, 0.0 if no verbs' tags  has been found
         @return: float accuracy
         '''
        if self.verbs_total[TAG] > 0:
            return float(self.verbs_tagged[TAG]) / self.verbs_total[TAG]
        else:
            return 0.0

    def get_fPOS_accuracy(self):
        '''
        Returns the fPOS' accuracy, 0.0 if no fPOS' tags has been found
        @return: float accuracy
        '''
        if self.__fPOS_counter > 0:
            return float(self.__fPOS_counter) / len(self.tagged_container.POS_dip)
        else:
            return 0.0

    def return_micro_accuracies(self):
        '''
        Prints all the micro-accuracies
        @return: None
        '''
        print self.return_nouns_prons_accuracy()
        print self.return_verbs_accuracy("TAGS", Conf.TAGGED_VERBS)
        print self.return_verbs_accuracy("MORPHOLOGY", Conf.VERBS_FORM_LIST)
        print self.return_fPOS_accuracy()

    def return_nouns_prons_accuracy(self):
        '''
        Returns the formatted nouns/pronouns accuracy
        @return: string output
        '''
        output = ""
        for i in range(len(Conf.NOUNS_PRO_DICT.keys())):
            output += "\n>>" + Conf.NOUNS_PRO_DICT.keys()[i] + ":\n================\nAccuracy % .2f%% " % (
                self.get_nouns_prons_accuracy(Conf.NOUNS_PRO_DICT.keys()[i]) * 100) + " - "
            output += str(self.nouns_prons_tagged[Conf.NOUNS_PRO_DICT.keys()[i]]) + " / " + str(
                self.nouns_prons_total[Conf.NOUNS_PRO_DICT.keys()[i]]) + " correctly tagged\n"
            output += self.return_associations(self.tags_association[Conf.NOUNS_PRO_DICT.keys()[i]])

        return output

    def return_verbs_accuracy(self, what, tagset):
        '''
        Returns the formatted verbs' accuracy
        @return: string output
        '''
        output = ""
        output += "\n>>VERBS [" + str(what.capitalize()) + "]:\n================"
        for i in range(len(tagset)):
            tag = tagset[i]
            output += "\n\n[" + tag + "] % .2f%% " % (self.get_verb_accuracy(tag) * 100) + " - "
            output += str(self.verbs_tagged[tag]) + " / " + str(self.verbs_total[tag]) + " correctly tagged" + "\n"
            output += self.return_associations(self.verbs_associations[tag])

        return output

    def return_fPOS_accuracy(self):
        '''
        Returns the formatted fPOS' accuracy
        @return: string output
        '''
        output = ""
        output += "\n>>fPOS:\n================"
        if self.tagged_container.get_program_name() != "SYN":
            output += "\n\n Not available"
            return output
        else:
            output += "\n\n% .2f%% " % (self.get_fPOS_accuracy() * 100) + " - "
            output += str(self.__fPOS_counter) + " / " + str(len(
                self.tagged_container.POS_dip)) + " correctly tagged" + "\n"
            output += self.return_associations(self.tags_association["fPOS"])
        return output

    def return_associations(self, association):
        '''
        Returns the association's list of tags (mistakes with conversions)
        @param association: list of associations (word, TT, EAGLE)
        @return: string output
        '''
        output = "\n"
        for i in range(len(association)):
            if str(association[i][2]) == "":
                association[i][2] = "Not Given"
            output += str(association[i][0]) + "\n" + str(association[i][1]) + " <-> " + str(
                association[i][2])
            conversions = self.tagged_container.get_conversions()
            if association[i][2] in conversions:
                v_list = conversions[association[i][2]]
                output += "-> " + str(v_list) + "\n"

            else:
                output += "\n"
        output += "\n----------------\n" + ""

        return output

    def add_to_confusion_verbs(self, z, tag):
        '''
        Increment the verbs-matrix element accuracy
        @param z: matrix row
        @param tag: the corresponding tag to increment
        @return: None
        '''
        for i in range(len(Conf.TAGGED_VERBS)):
            if tag in Conf.VERBS_DICT[Conf.TAGGED_VERBS[i]]:
                MicroStatistics.matrix_list[0].add_accuracy(z, Conf.VERBS_INDEXES[Conf.TAGGED_VERBS[i]])

    def add_to_confusion_morpho(self, z, tag):
        '''
        Increment the morphology-matrix element accuracy
        @param z: matrix row
        @param tag: the corresponding tag to increment
        @return: None
        '''
        if str(tag).lower() == "fin":
            MicroStatistics.matrix_list[2].add_accuracy(z, 0)

        elif tag in Conf.INDEF_LIST:
            MicroStatistics.matrix_list[2].add_accuracy(z, 1)

    def add_to_confusion_tags(self, z, tagged, tagged_list, matrix):
        '''
        Increment a matrix element accuracy
        @param z: matrix row
        @param tagged: the TAG tagged
        @param tagged_list: a tag list (matrix legend)
        @param matrix: the numpy matrix's object to use
        @return: None
        '''
        if len(tagged_list) == 9:
            new_matrix_tags = copy.deepcopy(Conf.TAGS_DICT)
            new_matrix_tags["PC"] = ["PC", "CLI"]
            new_matrix_tags["PRO-R"] = ["CHE:rel", "PR"]
            # new_matrix_tags["P"] = [x for x in new_matrix_tags["P"] if x not in ["CHE:rel", "PR", "PC", "CLI"]]
            for i in range(len(tagged_list)):
                k = tagged_list[i]
                if tagged in new_matrix_tags[k]:
                    matrix[z][Conf.TAGS_INDEXES_EXT[k]] += 1
        else:
            for i in range(len(tagged_list)):
                k = tagged_list[i]
                if tagged in Conf.TAGS_DICT[k]:
                    matrix[z][Conf.TAGS_INDEXES[k]] += 1

    @property
    def produce_confusion_matrixes(self):
        '''
        Calculates all the confusions matrixes
        @return: a formatted view of all the matrixes
        '''
        output = ""
        MicroStatistics.matrix_list[0].calculate_confusion_percentage()
        MicroStatistics.matrix_list[1].calculate_confusion_percentage()
        MicroStatistics.matrix_list[2].calculate_confusion_morpho_percen()
        MicroStatistics.matrix_list[3].calculate_confusion_percentage()

        self.correct_matrixes_sum_overflow()

        for i in range(len(MicroStatistics.matrix_list)):
            output += MicroStatistics.matrix_list[i].return_formatted_matrixes()

        return output

    def correct_matrixes_sum_overflow(self):
        '''
        Invokes a function to adjust the row's sum overflow
        @return: None
        '''
        # excludes Pronouns Confusion matrix
        for i in range(len(MicroStatistics.matrix_list) - 1):
            MicroStatistics.matrix_list[i].correct_sum_overflow()

    def populate_confusion_tags_matrix(self, tagged, gold):
        '''
        Populates the confusion matrixes, given two tags: TAG tagged and GOLD tag
        @param tagged: the TAG tagged
        @param gold: the GOLD tag
        @return: None
        '''
        for i in range(len(Conf.TAGGED_TAGS)):
            if gold in Conf.TAGS_DICT[Conf.TAGGED_TAGS[i]]:
                self.add_to_confusion_tags(i, tagged, Conf.TAGGED_TAGS,
                                           MicroStatistics.matrix_list[1].get_matrix())

    def populate_cli_pror_matrix(self, tagged, gold):
        '''
        Populate the clitics/PROR matrix given the two tags: TAG tagged and GOLD tag
        @param tagged: the TAG tagged
        @param gold: the GOLD tag
        @return: None
        '''
        tmp_list = Conf.TAGGED_TAGS[:]
        tmp_list.insert(5, "PC")
        tmp_list.insert(6, "PRO-R")

        if gold == "CLI":
            self.add_to_confusion_tags(0, tagged, tmp_list, MicroStatistics.matrix_list[3].get_matrix())

        elif gold == "CHE:rel" or gold == "PRO:rel":
            self.add_to_confusion_tags(1, tagged, tmp_list, MicroStatistics.matrix_list[3].get_matrix())

    def remove_end_space(self, word):
        '''
        Removes the end-whitespace from a word
        @param word: the word to be cleaned
        @return: None
        '''
        if len(word) > 1:
            if word[1] == "":
                word.pop()

    def delete_end_chars(self, words):
        '''
        Removes strange characters from compound words, such as: \s and "'"
        @param words: the compound word to clean
        @return: None
        '''
        if words[len(words) - 1] == '':
            del words[len(words) - 1]
        if words[len(words) - 1] == '\xe2\x80\x99':
            del words[len(words) - 1]

    def get_nouns_prons_tagged(self, tag):
        '''
        Returns the specific noun/pronoun counter given its tag
        @param tag: the noun/pronoun's tag
        @return: the specific noun/pronoun counter
        '''
        return self.nouns_prons_tagged[tag]

    def get_verb_tagged(self, tag):
        '''
        Returns the specific verbs counter given its tag
        @param tag: the verb's tag
        @return: the specific verb's counter
        '''
        return self.verbs_tagged[tag]

    def get_fPOS_tagged(self):
        '''
        Returns fPOS list
        @return: list
        '''
        return self.__fPOS_counter

import numpy


class Matrix:
    def __init__(self, rows_tagset, columns_tagset, rows, columns, title):
        self.rows_tagset = rows_tagset
        self.columns_tagset = columns_tagset
        self.rows = rows
        self.columns = columns
        self.matrix = numpy.zeros(shape=(self.rows, self.columns))
        self.title = title
        pass

    def increment_diagonal(self, i):
        '''
        Increments a matrix's diagonal
        @param i: element's position
        @return: None
        '''
        self.matrix[i, i] += 1

    def add_accuracy(self, i, j):
        '''
        Increments a matrix's element counter
        @param i: row index
        @param j: column index
        @return: None
        '''
        self.matrix[i, j] += 1

    def calculate_confusion_percentage(self):
        '''
        Calculate the percentages in all the matrixes (except the morphology one)
        @return: None
        '''
        from MicroStatistics import MicroStatistics
        for i in range(self.rows):
            for j in range(self.columns):
                self.matrix[i][j] = "%.2f" % (
                    self.matrix[i][j] / float(MicroStatistics.overall_counter[self.rows_tagset[i]]))

    def calculate_confusion_morpho_percen(self):
        '''
        Calculate the percentages in the morphology-matrix
        @return: None
        '''
        from MicroStatistics import MicroStatistics
        tagset = MicroStatistics.overall_counter
        for j in range(self.columns):
            self.matrix[0][j] = "%.2f" % (self.matrix[0][j] / float(tagset["Fin"]))
            self.matrix[1][j] = "%.2f" % (self.matrix[1][j] / float(tagset["Inf"] + tagset["Ger"] + tagset["Part"]))

    def correct_sum_overflow(self):
        '''
        Computes and corrects the row sum overflow, if row's sum >1.0
        @return: None
        '''
        for j in range(0, self.rows):
            if sum(map(float, self.matrix[j][0:len(self.matrix[j])])) > 1.0:
                max_index = numpy.argmax(self.matrix[j][0:len(self.matrix[j])])
                self.matrix[j][max_index] = float(self.matrix[j][max_index]) - 0.01
                self.matrix[j][max_index] = "%.2f" % self.matrix[j][max_index]

    def get_matrix(self):
        '''
        Returns a matrix object
        @return: numpy matrix
        '''
        return self.matrix

    def return_formatted_matrixes(self):
        '''
        Returns a formatted view of the computed matrixes
        @return: string output
        '''
        output = ""
        output += "\n>>" + self.title + "\n================\n"
        for i in range(len(self.columns_tagset)):
            output += "   " + (self.enclose_inbrackets(self.columns_tagset[i]))
        output += "\n"
        for i in range(self.rows):
            output += self.enclose_inbrackets(self.rows_tagset[i])
            for j in range(self.columns):
                output += self.enclose_inbrackets(self.matrix[i][j]) + "\t"
            output += "\n"

        return output

    def enclose_inbrackets(self, element):
        '''
        Encloses string in brackets
        @param element: element to enclose in brackets
        @return: string enclosed in brackets
        '''
        return "[" + str(element) + "]"

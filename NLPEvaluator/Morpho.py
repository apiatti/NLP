class Morpho:
    def __init__(self, mood, number, person, tense, form, fPOS):
        self.mood = mood
        self.number = number
        self.person = person
        self.tense = tense
        self.form = form
        self.fPOS = fPOS
        pass

    def set_mood(self, mood):
        self.mood = mood

    def set_number(self, number):
        self.number = number

    def set_person(self, person):
        self.person = person

    def set_form(self, form):
        self.form = form

    def set_tense(self, tense):
        self.tense = tense

    def set_fPOS(self, fPOS):
        self.fPOS = fPOS

    def get_mood(self):
        return self.mood

    def get_number(self):
        return self.number

    def get_person(self):
        return self.person

    def get_form(self):
        return self.form

    def get_tense(self):
        return self.tense

    def get_fPOS(self):
        return self.fPOS

    def print_attributes(self):
        print self.mood,
        print self.number,
        print self.person,
        print self.tense,
        print self.form

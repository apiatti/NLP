import codecs
import copy
import ntpath

import os
import re

DIR = "../resources/GOLD_tagged"
OUTPUT_PATH = "../resources/GOLD_tagged"


class ReaderParser:
    def __init__(self, path):
        self.path = path
        self.file_list = []
        self.file_list_cp=[]
        pass

    def parse_file(self):
        with codecs.open(self.path, "r") as f:
            for line in f:
                self.file_list.append(line)
                print line,
        self.remove_whitespaces()
        return self.file_list

    def remove_whitespaces(self):
        for i in range(len(self.file_list)):
            self.file_list[i]=(self.file_list[i].replace(" ", "\t"))

    def get_file_name(self):

        return ntpath.basename(self.path)



    def write_results(self):
        print OUTPUT_PATH
        with open(self.get_file_name(), "w+") as f:
            for i in range(len(self.file_list)):
                f.write(self.file_list[i])


def analyze():
    for filename in os.listdir(DIR):
        filepath = os.path.join(DIR, filename)
        print "Formatting...: " + filepath

        file_reader = ReaderParser(filepath)

        file_reader.parse_file()
        file_reader.write_results()


##################################

analyze()

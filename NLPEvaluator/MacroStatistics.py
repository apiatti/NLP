import re
import Conf


class MacroStatistics:
    counter = 0
    total_tags = 0
    __PATTERN = "\xe2\x80\x99| "

    def __init__(self, tagged_container, truth_container, micro_stats):
        global total_tags
        self.tagged_container = tagged_container
        self.truth_container = truth_container
        self.best_score = len(truth_container.words)
        MacroStatistics.total_tags += len(truth_container.words)
        self.__compound_words = []
        self.__unequals_words_tag = []
        self.__unequals_words_truth = []
        self.__compound_tags = []
        self.__unequals_tags_tag = []
        self.__unequals_tags_truth = []
        self.__conversion_tags = []
        self.__sentences_ptrs = []
        self.__accuracy = 0.0
        self.micro_stats = micro_stats
        self.status = ""

    pass

    def substract_bestscore(self):
        """
        Substract bestscore, in order to not count a file's score if this one is corrupted
        @return:
        """
        self.best_score -= len(self.truth_container.words)
        MacroStatistics.total_tags -= len(self.truth_container.words)

    def analyze(self):
        '''
        Analyze the two files by aligning pointers, comparing tags and populate different data structures to compute all the statistics
        @return: None
        '''
        print ""
        print "Comparing words..."
        print "------------------------------------------------------------------------------"

        # TODO: rimuovere correzioni compound-words
        j = 0
        i = 0
        print self.truth_container.words
        while i < (len(self.truth_container.words) or len(self.tagged_container.words)):

            self.truth_container.words[i] = re.sub("(_.*)", "", str(self.truth_container.words[i]))

            words = re.split(self.__PATTERN, str(self.truth_container.words[i]))
            words2 = re.split(self.__PATTERN, str(self.tagged_container.words[j]))

            print words, words2


            if len(words) > 1:
                self.delete_end_chars(words)

            if len(words2) > 1:
                self.delete_end_chars(words2)

            # ground truth or tagged contains 2+ words in a line
            if len(words) > len(words2):
                if self.nx_word_is_longer(i, j):
                    j += 2
                    i += 2
                else:
                    self.__compound_words.append(words)

                    j += len(words)
                    i += 1

            elif len(words2) > len(words):
                if self.nx_word_is_longer(i, j):
                    j += 2
                    i += 2

                else:
                    self.__compound_words.append(words2)
                    i += len(words2)
                    j += 1

            # ground truth's word not equal to tagged word
            if self.truth_container.words[i] != self.tagged_container.words[j]:
                i += 1
                j += 1


            # ground and tagged words are equal
            elif self.truth_container.words[i] == self.tagged_container.words[j]:
                self.micro_stats.count_tags(self.truth_container.POS_dip[i])
                self.compare_tags(self.truth_container.POS_dip[i], self.tagged_container.POS_dip[j], j)
                j += 1
                i += 1

        self.print_statistics()
        self.pointers_are_aligned(words, words2)

    def print_statistics(self):
        '''
        Prints different statisics and information
        @return: None
        '''
        # print self.return_compound_words()
        print self.return_unequals_tags()
        print self.return_stats()
        self.micro_stats.return_micro_accuracies()

    def delete_end_chars(self, words):
        '''
        Clean special characters from a compound word such as: \s and "'"
        @param words: the splitted word
        @return: None
        '''
        if words[len(words) - 1] == '':
            del words[len(words) - 1]
        if words[len(words) - 1] == '\xe2\x80\x99':
            del words[len(words) - 1]

    def nx_word_is_longer(self, i, j):
        '''
        Apply little 'algorithm' to determine if comparing-pointer have to move forward during comparison, return True or False
        @param i: pointer 1 inside the words
        @param j: pointer 2 inside the words
        @return: True or False
        '''
        j1_s = re.split(self.__PATTERN, str(self.tagged_container.words[j + 1]))
        i_s = re.split(self.__PATTERN, str(self.truth_container.words[i]))
        i1_s = re.split(self.__PATTERN, str(self.truth_container.words[i + 1]))

        if len(i_s) == len(i1_s) and len(i_s) == len(j1_s):
            return True
        else:
            return False

    def compare_tags(self, truth, tagged_tag, j):
        '''
        Compares the tags and perform various calculations detailed and general statistics
        @param truth: GOLD TAG
        @param tagged_tag: tagged TAG
        @param j: pointer inside the sentence
        @return: None
        '''

        prog_name = self.tagged_container.get_program_name()

        self.micro_stats.compare_micro_tags(truth, tagged_tag,
                                            self.tagged_container.words[j])  # compare names,pronouns,...

        self.micro_stats.compare_micro_morpho(self.get_splitted_gold_morpho(truth),
                                              self.tagged_container.morpho_splitted[j], self.tagged_container.words[j])

        if prog_name != "TT":
            if self.is_a_verb(truth) and prog_name != "TINT":
                gold_form = self.get_splitted_gold_morpho(truth)[1]
                tag_form = self.tagged_container.morpho_splitted[j].get_form()
                forms = Conf.VERBS_FORM_DICT.get(tag_form)

                # Individuo la categoria del verbo
                for i in range(len(Conf.TAGGED_VERBS)):
                    if truth in Conf.VERBS_DICT[Conf.TAGGED_VERBS[i]] and (
                                (tagged_tag == Conf.TAGGED_VERBS[i]) or tagged_tag in Conf.VERBS_DICT[
                                Conf.TAGGED_VERBS[i]]):
                        if forms is not None and gold_form in forms:
                            self.__accuracy += 1
                        else:
                            self.populate_unequals_structures(tagged_tag, truth, "[:" + tag_form.lower() + "]", j)

            else:
                conversions = self.tagged_container.get_conversions()
                if tagged_tag in conversions:
                    v_list = conversions[tagged_tag]
                    if truth in v_list:
                        self.__accuracy += 1
                    else:
                        self.populate_unequals_structures(tagged_tag, truth, v_list, j)


        elif truth == tagged_tag:
            self.__accuracy += 1

        else:
            self.__unequals_tags_tag.append(tagged_tag)
            self.__unequals_tags_truth.append(truth)
            self.__unequals_words_tag.append(self.tagged_container.words[j])
            self.__sentences_ptrs.append(j)

    def populate_unequals_structures(self, tagged, gold_tagged, unequal_tag, j):
        '''
        Populates differents data structures useful for prints, calculations, etc..
        @param tagged: tagged TAG
        @param gold_tagged: GOLD TAG
        @param unequal_tag: the wrong tagged TAG
        @param j: pointer inside the sentence
        @return: None
        '''
        self.__unequals_tags_tag.append(unequal_tag)
        self.__unequals_tags_truth.append(gold_tagged)
        self.__conversion_tags.append(tagged)
        self.__unequals_words_tag.append(self.tagged_container.words[j])
        self.__sentences_ptrs.append(j)

    def is_a_verb(self, gold_tagged):
        '''
        Returns True o False whether a tag is a verb or not
        @param gold_tagged: TT/GOLD's TAG
        @return: True or False
        '''
        if gold_tagged in Conf.TAGS_DICT["Verbs"]:
            return True
        else:
            return False

    def get_splitted_gold_morpho(self, gold_tagged):
        '''
        Splits  and returns the GOLD verbs' morphology
        @param gold_tagged: TT/GOLD's TAG
        @return: list of words
        '''
        return gold_tagged.split(":")

    def get_accuracy(self):
        '''
        Returns the general statistics
        @return: returns the accuracy as float, 0 if no tags were found
        '''
        if self.best_score > 0:
            return (float(self.__accuracy) / self.best_score)
        else:
            return 0.0

    def get_single_accuracy(self):
        return self.__accuracy

    def print_both_words_read(self, i, j):
        '''
        Debug purpose: prints both words read
        @param i: pointer 1 in the sentence
        @param j: pointer 2 in the sentence
        @return: None
        '''
        print self.truth_container.words[i],
        print self.tagged_container.words[j]
        print ""

    def pointers_are_aligned(self, words, words2):
        '''
        Print if files are rightly compared or not
        @param words: the last word from the GOLD/tagged file
        @param words2: the last word from the GOLD/tagged file
        @return: None
        '''
        if words == words2:
            self.status = "OK"
        else:
            self.status = "KO"

    def return_stats(self):
        '''
        Returns in a formatted view the general results
        @return: string output
        '''
        return "\n\n" \
               + ">>>>RESULTS: " + "\n ------------------" \
               + "\nN words parsed: %d" % len(self.truth_container.words) \
               + "\nN compound words found: %d" % len(self.__compound_words) \
               + "\nN correctly words tagged: %d" % self.__accuracy \
               + "\n->Overall accuracy: %.2f%%" % (self.get_accuracy() * 100) \
               + "\n"

    def return_compound_words(self):
        '''
        Returns in a formatted view the compound words  previously found

        @return: string output
        '''
        output = "\n" + ">>Compound words found" + "\n"

        if not self.__compound_words:
            return "None"
        else:
            for i in range(len(self.__compound_words)):
                for j in range(len(self.__compound_words[0])):
                    output += ''.join(self.__compound_words[i][j]) + " "
                output += "  "

        output += "\n"
        return output

    def return_unequals_tags(self):
        '''
        Returns in a formatted view the unequals tags previously found
        @return: string output
        '''
        title = ">>Unequals tags\n-----------------\nTRUTH <-> NLPEvaluator -> Converted\n-----------------\n"
        output = "\n" + title + "\n"
        if not self.__unequals_tags_tag:
            return output + "None"
        else:
            for i in range(len(self.__unequals_tags_tag)):
                if len(self.__conversion_tags) > 0:  # if not self.__conversion_tags NOT WORKING WTF?!
                    output += str(self.__unequals_words_tag[i]) + "\n" + str(
                        self.return_sentence(self.__sentences_ptrs[i])) + "\n" + str(
                        self.__unequals_tags_truth[i]) + " <-> " + str(
                        self.__conversion_tags[i]) + " ->" + str(self.__unequals_tags_tag[i]) + "\n\n"
                else:
                    output += str(self.__unequals_words_tag[i]) + "\n" + str(
                        self.return_sentence(self.__sentences_ptrs[i])) + "\n" + str(
                        self.__unequals_tags_truth[i]) + " <-> " + str(self.__unequals_tags_tag[i]) + "\n\n"
        return output

    def return_compound_tags(self):
        '''
        Returns in a formatted view the compound tags previously found
        @return: string output
        '''
        title = ">>Compound tags errors (from compound words)"
        output = "\n" + title + "\n"

        if not self.__compound_tags:
            return output + " None"
        else:
            for i in range(len(self.__compound_tags)):
                for j in range(len(self.__compound_words[0])):
                    output += str(self.__compound_words[i][j]) + " "
                output += " -> " + str(self.__compound_tags[i]) + "\n"

        return output

    def return_sentence(self, j):
        '''
        Splits a sentence and returns it
        @param j: the sentence's number
        @return: string output
        '''
        output = ""
        while self.tagged_container.words[j] not in ".!?" or j == 0:
            j -= 1

        if j != len(self.tagged_container.words) - 1:
            j += 1

        while self.tagged_container.words[j] not in ".!?":
            output += self.tagged_container.words[j] + " "
            j += 1
            if j == len(self.tagged_container.words) - 1:
                break
        output += self.tagged_container.words[j] + " "
        return output

    def return_unequals_words(self):
        '''
        Returns in a formatted view the unequals words previously found comparing the two sources of texts

        @return:
        '''
        output = ''
        output += ">>Unequals words\n"

        if not self.__unequals_words_tag:
            output += "None\n"
        else:
            for i in range(len(self.__unequals_words_tag)):
                output += self.__unequals_words_truth[i], " <-> ", self.__unequals_words_tag[i] + "\n"

        output += "\n"
        return output

    def get_ptrs_status(self):
        """
        Returns pointers status, OK or KO
        @return: str
        """
        return self.status

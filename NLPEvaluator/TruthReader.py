import codecs


class TruthReader:
    def __init__(self, path):
        self.path = path
        self.file_list = []
        self.file_header = []
        pass

    def parse_file(self):
        '''
        Parses the file
        @return:
        '''
        self.parse_header()
        return self.split_by_tab()

    def parse_header(self):
        '''
        Parses the header
        @return: None
        '''
        f = file(self.path)
        self.file_header = f.readline().split(" *")
        self.remove_newlines()

    def split_by_tab(self):
        '''
        Reads and splits file
        @return: list lines read
        '''
        with codecs.open(self.path, "r") as f:
            next(f)
            self.file_list = [line.split("\t") for line in f]
        self.remove_newlines()
        return self.file_list

    def remove_newlines(self):
        '''
        Removes the \n and **** from read lines
        @return: None
        '''
        for i in range(len(self.file_header)):
            self.file_header = [w.replace("\r\n", "").replace("****", "") for w in self.file_header]


    def print_header(self):
        '''
        Prints the file's header
        @return: None
        '''
        print self.get_header()

    def print_list(self):
        '''
        Prints the lines read
        @return: None
        '''
        print self.file_list

    def get_header(self):
        '''
        Returns the file header
        @return: None
        '''
        return self.file_header

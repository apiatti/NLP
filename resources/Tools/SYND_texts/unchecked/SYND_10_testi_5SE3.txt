# L’aquila e il leone  Un giorno guardai un documentario che parlava degli animali. 
1	L’	_	DET	RD	Definite=Def|Number=Sing|PronType=Art	2	det	_	_
2	aquila	_	NOUN	S	Gender=Fem|Number=Sing	0	ROOT	_	_
3	e	_	CONJ	CC	fPOS=CONJ++CC	2	cc	_	_
4	il	_	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	5	det	_	_
5	leone	_	NOUN	S	Gender=Masc|Number=Sing	2	conj	_	_
6	.	_	PUNCT	FS	-	2	punct	_	_

# Un giorno guardai un documentario che parlava degli animali.
6	Un	_	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	7	det	_	_
7	giorno	_	NOUN	S	Gender=Masc|Number=Sing	8	obl	_	_
8	guardai	_	VERB	V	Mood=Ind|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	0	root	_	_
9	un	_	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	10	det	_	_
10	documentario	_	NOUN	S	Gender=Masc|Number=Sing	8	obj	_	_
11	che	_	PRON	PR	PronType=Rel	12	nsubj	_	_
12	parlava	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	10	acl:relcl	_	_
13	degli	_	DET	DI	Gender=Masc|Number=Plur|PronType=Ind	14	det	_	_
14	animali	_	NOUN	S	Gender=Masc|Number=Plur	12	obj	_	_
15	.	_	PUNCT	FS	_	8	punct	_	_


# Il documentario che stavo guardando parlava di una leonessa, che viveva in Africa, e di un’aquila reale, che viveva in Gran Bretagna. 
1	Il	_	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	documentario	_	NOUN	S	Gender=Masc|Number=Sing	6	nsubj	_	_
3	che	_	PRON	PR	PronType=Rel	5	nsubj	_	_
4	stavo	_	AUX	VA	Mood=Sub|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	guardando	_	VERB	V	VerbForm=Ger	2	acl:relcl	_	_
6	parlava	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
7	di	_	ADP	E	_	9	case	_	_
8	una	_	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	9	det	_	_
9	leonessa	_	NOUN	S	Gender=Fem|Number=Sing	6	obl	_	_
10	,	_	PUNCT	FF	_	9	punct	_	_
11	che	_	PRON	PR	PronType=Rel	12	nsubj	_	_
12	viveva	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	9	acl:relcl	_	_
13	in	_	ADP	E	_	14	case	_	_
14	Africa	_	PROPN	SP	_	12	obl	_	_
15	,	_	PUNCT	FF	_	19	punct	_	_
16	e	_	CCONJ	CC	_	19	cc	_	_
17	di	_	ADP	E	_	19	case	_	_
18	un’	_	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	19	det	_	_
19	aquila	_	NOUN	S	Gender=Fem|Number=Sing	14	conj	_	_
20	reale	_	ADJ	A	Number=Sing	19	amod	_	_
21	,	_	PUNCT	FF	_	19	punct	_	_
22	che	_	PRON	PR	PronType=Rel	23	nsubj	_	_
23	viveva	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	19	acl:relcl	_	_
24	in	_	ADP	E	_	25	case	_	_
25	Gran	_	PROPN	SP	_	23	obl	_	_
26	Bretagna	_	PROPN	SP	_	25	flat:name	_	_
27	.	_	PUNCT	FS	_	6	punct	_	_


# L’aquila e la leonessa sono stati sottomessi ad una sfida, vinceva chi si nutriva per primo. 
1	L’	_	DET	RD	Definite=Def|Number=Sing|PronType=Art	2	det	_	_
2	aquila	_	NOUN	S	Gender=Fem|Number=Sing	8	nsubj:pass	_	_
3	e	_	CCONJ	CC	_	5	cc	_	_
4	la	_	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	5	det	_	_
5	leonessa	_	NOUN	S	Gender=Fem|Number=Sing	2	conj	_	_
6	sono	_	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	8	aux	_	_
7	stati	_	AUX	VA	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	8	aux:pass	_	_
8	sottomessi	_	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	0	root	_	_
9	ad	_	ADP	E	_	11	case	_	_
10	una	_	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	11	det	_	_
11	sfida	_	NOUN	S	Gender=Fem|Number=Sing	8	obl	_	_
12	,	_	PUNCT	FF	_	13	punct	_	_
13	vinceva	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	8	conj	_	_
14	chi	_	PRON	PR	Number=Sing|PronType=Rel	13	obj	_	_
15	si	_	PRON	PC	Clitic=Yes|Person=3|PronType=Prs	16	expl:impers	_	_
16	nutriva	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	14	acl:relcl	_	_
17	per	_	ADP	E	_	18	case	_	_
18	primo	_	ADJ	NO	Gender=Masc|Number=Sing|NumType=Ord	16	obl	_	_
19	.	_	PUNCT	FS	_	8	punct	_	_


# La leonessa trovò subito una preda, che era una zebra. 
1	La	_	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	2	det	_	_
2	leonessa	_	NOUN	S	Gender=Fem|Number=Sing	3	nsubj	_	_
3	trovò	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
4	subito	_	ADV	B	_	3	advmod	_	_
5	una	_	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	6	det	_	_
6	preda	_	NOUN	S	Gender=Fem|Number=Sing	3	obj	_	_
7	,	_	PUNCT	FF	_	6	punct	_	_
8	che	_	PRON	PR	PronType=Rel	11	nsubj	_	_
9	era	_	AUX	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	11	cop	_	_
10	una	_	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	11	det	_	_
11	zebra	_	NOUN	S	Gender=Fem|Number=Sing	6	acl:relcl	_	_
12	.	_	PUNCT	FS	_	3	punct	_	_


# La leonessa allora la attaccò ma fu respinta subito, da una zoccolata; la leonessa si arrese subito, senza un premio. 
1	La	_	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	2	det	_	_
2	leonessa	_	NOUN	S	Gender=Fem|Number=Sing	5	nsubj	_	_
3	allora	_	ADV	B	_	5	advmod	_	_
4	la	_	PRON	PC	Clitic=Yes|Gender=Fem|Number=Sing|Person=3|PronType=Prs	5	obj	_	_
5	attaccò	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
6	ma	_	CCONJ	CC	_	8	cc	_	_
7	fu	_	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	8	aux:pass	_	_
8	respinta	_	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	5	conj	_	_
9	subito	_	ADV	B	_	8	advmod	_	_
10	,	_	PUNCT	FF	_	8	punct	_	_
11	da	_	ADP	E	_	13	case	_	_
12	una	_	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	13	det	_	_
13	zoccolata	_	NOUN	S	Gender=Fem|Number=Sing	8	obl:agent	_	_
14	;	_	PUNCT	FC	_	5	punct	_	_
15	la	_	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	16	det	_	_
16	leonessa	_	NOUN	S	Gender=Fem|Number=Sing	18	nsubj	_	_
17	si	_	PRON	PC	Clitic=Yes|Person=3|PronType=Prs	18	expl	_	_
18	arrese	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	5	parataxis	_	_
19	subito	_	ADV	B	_	18	advmod	_	_
20	,	_	PUNCT	FF	_	18	punct	_	_
21	senza	_	ADP	E	_	23	case	_	_
22	un	_	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	23	det	_	_
23	premio	_	NOUN	S	Gender=Masc|Number=Sing	18	obl	_	_
24	.	_	PUNCT	FS	_	5	punct	_	_


# L’aquila dopo due, tre ore di volo avvistò una lepre giovane. 
1	L’	_	DET	RD	Definite=Def|Number=Sing|PronType=Art	2	det	_	_
2	aquila	_	NOUN	S	Gender=Fem|Number=Sing	10	nsubj	_	_
3	dopo	_	ADP	E	_	7	case	_	_
4	due	_	NUM	N	NumType=Card	7	nummod	_	_
5	,	_	PUNCT	FF	_	4	punct	_	_
6	tre	_	NUM	N	NumType=Card	7	nummod	_	_
7	ore	_	NOUN	S	Gender=Fem|Number=Plur	2	nmod	_	_
8	di	_	ADP	E	_	9	case	_	_
9	volo	_	NOUN	S	Gender=Masc|Number=Sing	7	nmod	_	_
10	avvistò	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
11	una	_	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	12	det	_	_
12	lepre	_	NOUN	S	Gender=Fem|Number=Sing	10	obj	_	_
13	giovane	_	ADJ	A	Number=Sing	12	amod	_	_
14	.	_	PUNCT	FS	_	10	punct	_	_


# L’aquila allora attaccò per la prima volta, ma la lepre schivò l’attacco con agilità. 
1	L’	_	DET	RD	Definite=Def|Number=Sing|PronType=Art	2	det	_	_
2	aquila	_	NOUN	S	Gender=Fem|Number=Sing	4	nsubj	_	_
3	allora	_	ADV	B	_	4	advmod	_	_
4	attaccò	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
5	per	_	ADP	E	_	8	case	_	_
6	la	_	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	8	det	_	_
7	prima	_	ADJ	NO	Gender=Fem|Number=Sing|NumType=Ord	8	amod	_	_
8	volta	_	NOUN	S	Gender=Fem|Number=Sing	4	obl	_	_
9	,	_	PUNCT	FF	_	13	punct	_	_
10	ma	_	CCONJ	CC	_	13	cc	_	_
11	la	_	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	12	det	_	_
12	lepre	_	NOUN	S	Gender=Fem|Number=Sing	13	nsubj	_	_
13	schivò	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	4	conj	_	_
14	l’	_	DET	RD	Definite=Def|Number=Sing|PronType=Art	15	det	_	_
15	attacco	_	NOUN	S	Gender=Masc|Number=Sing	13	obj	_	_
16	con	_	ADP	E	_	17	case	_	_
17	agilità	_	NOUN	S	Gender=Fem	13	obl	_	_
18	.	_	PUNCT	FS	_	4	punct	_	_


# L’aquila allora attaccò un’altra volta, ma di nuovo la lepre schivò l’attacco. 
1	L’	_	DET	RD	Definite=Def|Number=Sing|PronType=Art	2	det	_	_
2	aquila	_	NOUN	S	Gender=Fem|Number=Sing	4	nsubj	_	_
3	allora	_	ADV	B	_	4	advmod	_	_
4	attaccò	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
5	un’	_	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	7	det	_	_
6	altra	_	ADJ	A	Gender=Fem|Number=Sing	7	amod	_	_
7	volta	_	NOUN	S	Gender=Fem|Number=Sing	4	obl	_	_
8	,	_	PUNCT	FF	_	14	punct	_	_
9	ma	_	CCONJ	CC	_	14	cc	_	_
10	di	_	ADP	E	_	11	case	_	_
11	nuovo	_	ADJ	A	Gender=Masc|Number=Sing	14	obl	_	_
12	la	_	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	13	det	_	_
13	lepre	_	NOUN	S	Gender=Fem|Number=Sing	14	nsubj	_	_
14	schivò	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	4	conj	_	_
15	l’	_	DET	RD	Definite=Def|Number=Sing|PronType=Art	16	det	_	_
16	attacco	_	NOUN	S	Gender=Masc|Number=Sing	14	obj	_	_
17	.	_	PUNCT	FS	_	4	punct	_	_


# L’aquila non si arrese comunque e fece un terzo attacco, questa volta avendo successo. 
1	L’	_	DET	RD	Definite=Def|Number=Sing|PronType=Art	2	det	_	_
2	aquila	_	NOUN	S	Gender=Fem|Number=Sing	5	nsubj	_	_
3	non	_	ADV	BN	PronType=Neg	5	advmod	_	_
4	si	_	PRON	PC	Clitic=Yes|Person=3|PronType=Prs	5	expl	_	_
5	arrese	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
6	comunque	_	ADV	B	_	5	advmod	_	_
7	e	_	CCONJ	CC	_	8	cc	_	_
8	fece	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	5	conj	_	_
9	un	_	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	11	det	_	_
10	terzo	_	ADJ	NO	Gender=Masc|Number=Sing|NumType=Ord	11	amod	_	_
11	attacco	_	NOUN	S	Gender=Masc|Number=Sing	8	obj	_	_
12	,	_	PUNCT	FF	_	5	punct	_	_
13	questa	_	DET	DD	Gender=Fem|Number=Sing|PronType=Dem	14	det	_	_
14	volta	_	NOUN	S	Gender=Fem|Number=Sing	5	obl	_	_
15	avendo	_	VERB	V	VerbForm=Ger	14	acl	_	_
16	successo	_	NOUN	S	Gender=Masc|Number=Sing	15	obj	_	_
17	.	_	PUNCT	FS	_	5	punct	_	_


# L’aquila, quindi, vinse la sfida. 
1	L’	_	DET	RD	Definite=Def|Number=Sing|PronType=Art	2	det	_	_
2	aquila	_	NOUN	S	Gender=Fem|Number=Sing	6	nsubj	_	_
3	,	_	PUNCT	FF	_	2	punct	_	_
4	quindi	_	ADV	B	_	6	advmod	_	_
5	,	_	PUNCT	FF	_	4	punct	_	_
6	vinse	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
7	la	_	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	8	det	_	_
8	sfida	_	NOUN	S	Gender=Fem|Number=Sing	6	obj	_	_
9	.	_	PUNCT	FS	_	6	punct	_	_


# Il documentario disse che l’aquila non si arrese mai, invece la leonessa si arrese subito. 
1	Il	_	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	documentario	_	NOUN	S	Gender=Masc|Number=Sing	3	nsubj	_	_
3	disse	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
4	che	_	SCONJ	CS	_	9	mark	_	_
5	l’	_	DET	RD	Definite=Def|Number=Sing|PronType=Art	6	det	_	_
6	aquila	_	NOUN	S	Gender=Fem|Number=Sing	9	nsubj	_	_
7	non	_	ADV	BN	PronType=Neg	9	advmod	_	_
8	si	_	PRON	PC	Clitic=Yes|Person=3|PronType=Prs	9	expl:impers	_	_
9	arrese	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	16	advcl	_	_
10	mai	_	ADV	B	_	9	advmod	_	_
11	,	_	PUNCT	FF	_	9	punct	_	_
12	invece	_	ADV	B	_	16	advmod	_	_
13	la	_	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	14	det	_	_
14	leonessa	_	NOUN	S	Gender=Fem|Number=Sing	16	nsubj	_	_
15	si	_	PRON	PC	Clitic=Yes|Person=3|PronType=Prs	16	expl	_	_
16	arrese	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	3	ccomp	_	_
17	subito	_	ADV	B	_	16	advmod	_	_
18	.	_	PUNCT	FS	_	3	punct	_	_


# Alla fine del documentario pensai che l’aquila era più valorosa della leonessa, e quindi si meritò la vittoria. 
1	Alla	_	DET	DD	Gender=Fem|Number=Sing|PronType=Dem	2	det	_	_
2	fine	_	NOUN	S	Gender=Fem|Number=Sing	5	nsubj	_	_
3	del	_	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	4	det	_	_
4	documentario	_	NOUN	S	Gender=Masc|Number=Sing	5	nsubj	_	_
5	pensai	_	VERB	V	Mood=Ind|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	0	root	_	_
6	che	_	SCONJ	CS	_	11	mark	_	_
7	l’	_	DET	RD	Definite=Def|Number=Sing|PronType=Art	8	det	_	_
8	aquila	_	NOUN	S	Gender=Fem|Number=Sing	11	nsubj	_	_
9	era	_	AUX	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	11	cop	_	_
10	più	_	ADV	B	_	11	advmod	_	_
11	valorosa	_	ADJ	A	Gender=Fem|Number=Sing	5	ccomp	_	_
12	della	_	DET	DI	Gender=Fem|Number=Sing|PronType=Ind	13	det	_	_
13	leonessa	_	NOUN	S	Gender=Fem|Number=Sing	11	obj	_	_
14	,	_	PUNCT	FF	_	18	punct	_	_
15	e	_	CCONJ	CC	_	18	cc	_	_
16	quindi	_	ADV	B	_	18	advmod	_	_
17	si	_	PRON	PC	Clitic=Yes|Person=3|PronType=Prs	18	expl:pass	_	_
18	meritò	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	5	conj	_	_
19	la	_	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	20	det	_	_
20	vittoria	_	NOUN	S	Gender=Fem|Number=Sing	18	obj	_	_
21	.	_	PUNCT	FS	_	5	punct	_	_


# Questo documentario mi ha insegnato di non arrendersi mai, perché se non ti arrendi ottieni quel che vuoi.
1	Questo	_	DET	DD	Gender=Masc|Number=Sing|PronType=Dem	2	det	_	_
2	documentario	_	NOUN	S	Gender=Masc|Number=Sing	5	nsubj	_	_
3	mi	_	PRON	PC	Clitic=Yes|Number=Sing|Person=1|PronType=Prs	5	iobj	_	_
4	ha	_	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	insegnato	_	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
6	di	_	ADP	E	_	8	mark	_	_
7	non	_	ADV	BN	PronType=Neg	8	advmod	_	_
8	arrendersi	_	ADJ	A	Gender=Masc|Number=Plur	5	xcomp	_	_
9	mai	_	ADV	B	_	8	advmod	_	_
10	,	_	PUNCT	FF	_	8	punct	_	_
11	perché	_	SCONJ	CS	_	8	advmod	_	_
12	se	_	SCONJ	CS	_	15	mark	_	_
13	non	_	ADV	BN	PronType=Neg	15	advmod	_	_
14	ti	_	PRON	PC	Clitic=Yes|Number=Sing|Person=2|PronType=Prs	15	iobj	_	_
15	arrendi	_	VERB	V	Mood=Ind|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	11	advcl	_	_
16	ottieni	_	VERB	V	Mood=Ind|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	15	xcomp	_	_
17	quel	_	PRON	PD	Gender=Masc|Number=Sing|PronType=Dem	16	obj	_	_
18	che	_	PRON	PR	PronType=Rel	19	nsubj	_	_
19	vuoi	_	VERB	V	Mood=Ind|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	17	acl:relcl	_	_
20	.	_	PUNCT	FS	_	5	punct	_	_
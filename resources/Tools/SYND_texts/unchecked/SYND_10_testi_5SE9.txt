# Una volta un bambino mi ha chiesto di fare un gioco, lui penzava di essere più bravo di mé ma non era così. 
1	Una	_	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	2	det	_	_
2	volta	_	NOUN	S	Gender=Fem|Number=Sing	7	obl	_	_
3	un	_	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	4	det	_	_
4	bambino	_	NOUN	S	Gender=Masc|Number=Sing	7	nsubj	_	_
5	mi	_	PRON	PC	Clitic=Yes|Number=Sing|Person=1|PronType=Prs	7	iobj	_	_
6	ha	_	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	7	aux	_	_
7	chiesto	_	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
8	di	_	ADP	E	_	9	mark	_	_
9	fare	_	VERB	V	VerbForm=Inf	7	xcomp	_	_
10	un	_	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	11	det	_	_
11	gioco	_	NOUN	S	Gender=Masc|Number=Sing	9	obj	_	_
12	,	_	PUNCT	FF	_	14	punct	_	_
13	lui	_	PRON	PE	Gender=Masc|Number=Sing|Person=3|PronType=Prs	14	nsubj	_	_
14	penzava	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	7	conj	_	_
15	di	_	ADP	E	_	18	mark	_	_
16	essere	_	AUX	V	VerbForm=Inf	18	cop	_	_
17	più	_	ADV	B	_	18	advmod	_	_
18	bravo	_	ADJ	A	Gender=Masc|Number=Sing	14	xcomp	_	_
19	di	_	ADP	E	_	20	case	_	_
20	mé	_	PRON	PE	Number=Sing|Person=1|PronType=Prs	18	obl	_	_
21	ma	_	CCONJ	CC	_	23	cc	_	_
22	non	_	ADV	BN	PronType=Neg	23	advmod	_	_
23	era	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	18	conj	_	_
24	così	_	ADV	B	_	23	advmod	_	_
25	.	_	PUNCT	FS	_	7	punct	_	_


# Il giorno dopo abbiamo giocato a quel gioco lui era molto avanti ma io dopo lo superato e ho vinto il gioco. 
1	Il	_	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	giorno	_	NOUN	S	Gender=Masc|Number=Sing	5	obl	_	_
3	dopo	_	ADV	B	_	2	advmod	_	_
4	abbiamo	_	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	giocato	_	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
6	a	_	ADP	E	_	8	case	_	_
7	quel	_	DET	DD	Gender=Masc|Number=Sing|PronType=Dem	8	det	_	_
8	gioco	_	NOUN	S	Gender=Masc|Number=Sing	5	obl	_	_
9	lui	_	PRON	PE	Gender=Masc|Number=Sing|Person=3|PronType=Prs	12	nsubj	_	_
10	era	_	AUX	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	12	cop	_	_
11	molto	_	ADV	B	_	12	advmod	_	_
12	avanti	_	ADV	B	_	5	advmod	_	_
13	ma	_	CCONJ	CC	_	17	cc	_	_
14	io	_	PRON	PE	Number=Sing|Person=1|PronType=Prs	17	nsubj	_	_
15	dopo	_	ADP	E	_	17	case	_	_
16	lo	_	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	17	det	_	_
17	superato	_	NOUN	S	Gender=Masc|Number=Sing	12	conj	_	_
18	e	_	CCONJ	CC	_	20	cc	_	_
19	ho	_	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	20	aux	_	_
20	vinto	_	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	12	conj	_	_
21	il	_	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	22	det	_	_
22	gioco	_	NOUN	S	Gender=Masc|Number=Sing	20	obj	_	_
23	.	_	PUNCT	FS	_	5	punct	_	_


# Mi ha insegnato che si deve essere bravi in quel gioco e non dire che si e sicuri divincere il gioco. 
1	Mi	_	PRON	PC	Clitic=Yes|Number=Sing|Person=1|PronType=Prs	3	iobj	_	_
2	ha	_	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	3	aux	_	_
3	insegnato	_	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
4	che	_	SCONJ	CS	_	8	mark	_	_
5	si	_	PRON	PC	Clitic=Yes|Person=3|PronType=Prs	8	expl:impers	_	_
6	deve	_	AUX	VM	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	8	aux	_	_
7	essere	_	AUX	V	VerbForm=Inf	8	cop	_	_
8	bravi	_	ADJ	A	Gender=Masc|Number=Plur	3	ccomp	_	_
9	in	_	ADP	E	_	11	case	_	_
10	quel	_	DET	DD	Gender=Masc|Number=Sing|PronType=Dem	11	det	_	_
11	gioco	_	NOUN	S	Gender=Masc|Number=Sing	8	obl	_	_
12	e	_	CCONJ	CC	_	14	cc	_	_
13	non	_	ADV	BN	PronType=Neg	14	advmod	_	_
14	dire	_	VERB	V	VerbForm=Inf	8	conj	_	_
15	che	_	SCONJ	CS	_	16	mark	_	_
16	si	_	PRON	PC	Clitic=Yes|Person=3|PronType=Prs	14	ccomp	_	_
17	e	_	CCONJ	CC	_	18	cc	_	_
18	sicuri	_	ADJ	A	Gender=Masc|Number=Plur	16	conj	_	_
19	divincere	_	VERB	V	VerbForm=Inf	18	advcl	_	_
20	il	_	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	21	det	_	_
21	gioco	_	NOUN	S	Gender=Masc|Number=Sing	19	obj	_	_
22	.	_	PUNCT	FS	_	3	punct	_	_


# Lui che penzava di vìncere, ma quando io l’avevo battuto si era arrabbiato perciò voleva fare la rivincita, ma stavolto a un altro gioco. 
1	Lui	_	PRON	PE	Gender=Masc|Number=Sing|Person=3|PronType=Prs	0	root	_	_
2	che	_	PRON	PR	PronType=Rel	3	nsubj	_	_
3	penzava	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	1	acl:relcl	_	_
4	di	_	ADP	E	_	5	mark	_	_
5	vìncere	_	VERB	V	VerbForm=Inf	3	xcomp	_	_
6	,	_	PUNCT	FF	_	15	punct	_	_
7	ma	_	CCONJ	CC	_	15	cc	_	_
8	quando	_	SCONJ	CS	_	12	mark	_	_
9	io	_	PRON	PE	Number=Sing|Person=1|PronType=Prs	12	nsubj	_	_
10	l’	_	PRON	PC	Clitic=Yes|Number=Sing|Person=3|PronType=Prs	12	obj	_	_
11	avevo	_	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	12	aux	_	_
12	battuto	_	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	15	advcl	_	_
13	si	_	PRON	PC	Clitic=Yes|Person=3|PronType=Prs	15	expl	_	_
14	era	_	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	15	aux	_	_
15	arrabbiato	_	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	1	conj	_	_
16	perciò	_	ADV	B	_	18	mark	_	_
17	voleva	_	AUX	VM	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	18	aux	_	_
18	fare	_	VERB	V	VerbForm=Inf	15	advcl	_	_
19	la	_	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	20	det	_	_
20	rivincita	_	NOUN	S	Gender=Fem|Number=Sing	18	obj	_	_
21	,	_	PUNCT	FF	_	27	punct	_	_
22	ma	_	CCONJ	CC	_	27	cc	_	_
23	stavolto	_	ADV	B	_	27	advmod	_	_
24	a	_	ADP	E	_	27	case	_	_
25	un	_	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	27	det	_	_
26	altro	_	ADJ	A	Gender=Masc|Number=Sing	27	amod	_	_
27	gioco	_	NOUN	S	Gender=Masc|Number=Sing	20	conj	_	_
28	.	_	PUNCT	FS	_	1	punct	_	_


# Ma stavolto ero in vantaggio io ma al ultimo momento mi supero e vinse lui il gioco, cosi stavolto era contento. 
1	Ma	_	CCONJ	CC	_	5	cc	_	_
2	stavolto	_	ADV	B	_	5	advmod	_	_
3	ero	_	AUX	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	5	cop	_	_
4	in	_	ADP	E	_	5	case	_	_
5	vantaggio	_	NOUN	S	Gender=Masc|Number=Sing	22	obl	_	_
6	io	_	PRON	PE	Number=Sing|Person=1|PronType=Prs	5	amod	_	_
7	ma	_	CCONJ	CC	_	12	cc	_	_
8	al	_	DET	RD	Definite=Def|Number=Sing|PronType=Art	10	det	_	_
9	ultimo	_	ADJ	NO	Gender=Masc|Number=Sing|NumType=Ord	10	amod	_	_
10	momento	_	NOUN	S	Gender=Masc|Number=Sing	12	nsubj	_	_
11	mi	_	PRON	PC	Clitic=Yes|Number=Sing|Person=1|PronType=Prs	12	iobj	_	_
12	supero	_	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	5	conj	_	_
13	e	_	CCONJ	CC	_	14	cc	_	_
14	vinse	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	5	conj	_	_
15	lui	_	PRON	PE	Gender=Masc|Number=Sing|Person=3|PronType=Prs	14	obj	_	_
16	il	_	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	17	det	_	_
17	gioco	_	NOUN	S	Gender=Masc|Number=Sing	14	nsubj	_	_
18	,	_	PUNCT	FF	_	5	punct	_	_
19	cosi	_	PROPN	SP	_	22	nsubj	_	_
20	stavolto	_	ADV	B	_	22	advmod	_	_
21	era	_	AUX	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	22	cop	_	_
22	contento	_	ADJ	A	Gender=Masc|Number=Sing	0	root	_	_
23	.	_	PUNCT	FS	_	22	punct	_	_


# Ho imparato che non si deve fare la battalia di un gioco ma la miglior coso in un gioco si dive divertirsi,
1	Ho	_	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	2	aux	_	_
2	imparato	_	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
3	che	_	SCONJ	CS	_	7	mark	_	_
4	non	_	ADV	BN	PronType=Neg	7	advmod	_	_
5	si	_	PRON	PC	Clitic=Yes|Person=3|PronType=Prs	7	expl:pass	_	_
6	deve	_	AUX	VM	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	7	aux	_	_
7	fare	_	VERB	V	VerbForm=Inf	2	ccomp	_	_
8	la	_	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	9	det	_	_
9	battalia	_	NOUN	S	Gender=Fem|Number=Sing	7	nsubj:pass	_	_
10	di	_	ADP	E	_	12	case	_	_
11	un	_	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	12	det	_	_
12	gioco	_	NOUN	S	Gender=Masc|Number=Sing	9	nmod	_	_
13	ma	_	CCONJ	CC	_	16	cc	_	_
14	la	_	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	16	det	_	_
15	miglior	_	ADJ	A	Degree=Cmp|Number=Sing	16	amod	_	_
16	coso	_	NOUN	S	Gender=Masc|Number=Sing	7	conj	_	_
17	in	_	ADP	E	_	19	case	_	_
18	un	_	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	19	det	_	_
19	gioco	_	NOUN	S	Gender=Masc|Number=Sing	21	obl	_	_
20	si	_	PRON	PC	Clitic=Yes|Person=3|PronType=Prs	21	expl:impers	_	_
21	dive	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	16	acl:relcl	_	_
22	divertirsi	_	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	21	xcomp	_	_
23	,	_	PUNCT	FF	_	16	punct	_	_
# Quella partita memorabile.  A luglio dell’anno scorso ero in vacanza dai miei nonni.
1	Quella	_	DET	DD	Gender=Fem|Number=Sing|PronType=Dem	2	det	_	_
2	partita	_	NOUN	S	Gender=Fem|Number=Sing	0	ROOT	_	_
3	memorabile	_	ADJ	A	Number=Sing	2	amod	_	_
4	.	_	PUNCT	FS	-	2	punct	_	_

# A luglio dell'anno scorso ero in vacanza dai miei nonni.
1	A	_	ADP	E	-	2	case	_	_
2	luglio	_	NOUN	S	Gender=Masc|Number=Sing	6	nmod	_	_
3	dell’	_	DET	DI	Definite=Def|Number=Sing|PronType=Art	4	det	_	_
4	anno	_	NOUN	S	Gender=Masc|Number=Sing	2	nmod	_	_
5	scorso	_	ADJ	A	Gender=Masc|Number=Sing	4	amod	_	_
6	ero	_	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	0	ROOT	_	_
7	in	_	ADP	E	-	8	case	_	_
8	vacanza	_	NOUN	S	Gender=Fem|Number=Sing	6	nmod	_	_
9	dai	_	VERB	V	Gender=Masc|Number=Plur|Poss=Yes|PronType=Prs	6	punct	_	_
10	miei	_	DET	AP	Gender=Masc|Number=Plur|Poss=Yes|PronType=Prs	11	det:poss	_	_
11	nonni	_	NOUN	S	Gender=Masc|Number=Plur	6	nsubj	_	_
12	.	_	PUNCT	FS	-	6	punct	_	_

# Là vivono anche i miei cugini. 
1	Là	_	ADV	B	_	2	obj	_	_
2	vivono	_	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
3	anche	_	ADV	B	_	6	advmod	_	_
4	i	_	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	6	det	_	_
5	miei	_	DET	AP	Gender=Masc|Number=Plur|Poss=Yes|PronType=Prs	6	det:poss	_	_
6	cugini	_	NOUN	S	Gender=Masc|Number=Plur	2	nsubj	_	_
7	.	_	PUNCT	FS	_	2	punct	_	_


# Anche essendo più grandi di me gioco con loro e mi diverto tanto. 
1	Anche	_	ADV	B	_	4	advmod	_	_
2	essendo	_	AUX	V	VerbForm=Ger	4	cop	_	_
3	più	_	ADV	B	_	4	advmod	_	_
4	grandi	_	ADJ	A	Number=Plur	0	root	_	_
5	di	_	ADP	E	_	6	case	_	_
6	me	_	PRON	PE	Number=Sing|Person=1|PronType=Prs	4	obl	_	_
7	gioco	_	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	4	csubj	_	_
8	con	_	ADP	E	_	9	case	_	_
9	loro	_	PRON	PE	Number=Plur|Person=3|PronType=Prs	7	obl	_	_
10	e	_	CCONJ	CC	_	12	cc	_	_
11	mi	_	PRON	PC	Clitic=Yes|Number=Sing|Person=1|PronType=Prs	12	expl	_	_
12	diverto	_	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	7	conj	_	_
13	tanto	_	ADV	B	_	12	advmod	_	_
14	.	_	PUNCT	FS	_	4	punct	_	_


# Vicino alla loro casa c’è un campo da calcio e noi giochiamo spesso. 
1	Vicino	_	ADV	B	_	6	advmod	_	_
2	alla	_	DET	DI	Gender=Fem|Number=Sing|PronType=Ind	4	det	_	_
3	loro	_	DET	AP	Poss=Yes|PronType=Prs	4	det:poss	_	_
4	casa	_	NOUN	S	Gender=Fem|Number=Sing	6	nsubj	_	_
5	c’	_	PRON	PC	Clitic=Yes|PronType=Prs	6	expl	_	_
6	è	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
7	un	_	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	8	det	_	_
8	campo	_	NOUN	S	Gender=Masc|Number=Sing	6	nsubj	_	_
9	da	_	ADP	E	_	10	case	_	_
10	calcio	_	NOUN	S	Gender=Masc|Number=Sing	8	nmod	_	_
11	e	_	CCONJ	CC	_	13	cc	_	_
12	noi	_	PRON	PE	Number=Plur|Person=1|PronType=Prs	13	nsubj	_	_
13	giochiamo	_	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	6	conj	_	_
14	spesso	_	ADV	B	_	13	advmod	_	_
15	.	_	PUNCT	FS	_	6	punct	_	_


# Un giorno dei ragazzi, certi più grandi, ci hanno chiesto di fare una partita perché dicevano che ci stracceranno, ma noi abbiamo accettato solo perché ci hanno detto così. 
1	Un	_	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	giorno	_	NOUN	S	Gender=Masc|Number=Sing	12	obl	_	_
3	dei	_	DET	DI	Gender=Masc|Number=Plur|PronType=Ind	4	det	_	_
4	ragazzi	_	NOUN	S	Gender=Masc|Number=Plur	12	nsubj	_	_
5	,	_	PUNCT	FF	_	4	punct	_	_
6	certi	_	PRON	PI	Gender=Masc|Number=Plur|PronType=Ind	4	nmod	_	_
7	più	_	ADV	B	_	8	advmod	_	_
8	grandi	_	ADJ	A	Number=Plur	6	amod	_	_
9	,	_	PUNCT	FF	_	4	punct	_	_
10	ci	_	PRON	PC	Clitic=Yes|Number=Plur|Person=1|PronType=Prs	12	iobj	_	_
11	hanno	_	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	12	aux	_	_
12	chiesto	_	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
13	di	_	ADP	E	_	14	mark	_	_
14	fare	_	VERB	V	VerbForm=Inf	12	xcomp	_	_
15	una	_	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	16	det	_	_
16	partita	_	NOUN	S	Gender=Fem|Number=Sing	14	obj	_	_
17	perché	_	SCONJ	CS	_	18	mark	_	_
18	dicevano	_	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	14	advcl	_	_
19	che	_	SCONJ	CS	_	21	mark	_	_
20	ci	_	PRON	PC	Clitic=Yes|Number=Plur|Person=1|PronType=Prs	21	expl	_	_
21	stracceranno	_	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Fut|VerbForm=Fin	18	ccomp	_	_
22	,	_	PUNCT	FF	_	26	punct	_	_
23	ma	_	CCONJ	CC	_	26	cc	_	_
24	noi	_	PRON	PE	Number=Plur|Person=1|PronType=Prs	26	nsubj	_	_
25	abbiamo	_	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	26	aux	_	_
26	accettato	_	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	21	conj	_	_
27	solo	_	ADV	B	_	31	advmod	_	_
28	perché	_	SCONJ	CS	_	31	mark	_	_
29	ci	_	PRON	PC	Clitic=Yes|Number=Plur|Person=1|PronType=Prs	31	expl	_	_
30	hanno	_	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	31	aux	_	_
31	detto	_	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	26	advcl	_	_
32	così	_	ADV	B	_	31	advmod	_	_
33	.	_	PUNCT	FS	_	12	punct	_	_


# Quando abbiamo iniziato a giocare stavamo perdendo ma poi abbiamo rimontato e siamo andati ai rigori. 
1	Quando	_	SCONJ	CS	_	3	mark	_	_
2	abbiamo	_	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	3	aux	_	_
3	iniziato	_	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	7	advcl	_	_
4	a	_	ADP	E	_	5	mark	_	_
5	giocare	_	VERB	V	VerbForm=Inf	3	xcomp	_	_
6	stavamo	_	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	7	aux	_	_
7	perdendo	_	VERB	V	VerbForm=Ger	0	root	_	_
8	ma	_	CCONJ	CC	_	11	cc	_	_
9	poi	_	ADV	B	_	11	advmod	_	_
10	abbiamo	_	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	11	aux	_	_
11	rimontato	_	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	7	conj	_	_
12	e	_	CCONJ	CC	_	14	cc	_	_
13	siamo	_	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	14	aux	_	_
14	andati	_	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	11	conj	_	_
15	ai	_	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	16	det	_	_
16	rigori	_	NOUN	S	Gender=Masc|Number=Plur	14	nsubj	_	_
17	.	_	PUNCT	FS	_	7	punct	_	_


# Ai rigori abbiamo vinto, e loro si sono arrabbiati talmente tanto che ci inseguivano per picchiarci. 
1	Ai	_	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	2	det	_	_
2	rigori	_	NOUN	S	Gender=Masc|Number=Plur	4	nsubj	_	_
3	abbiamo	_	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	4	aux	_	_
4	vinto	_	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
5	,	_	PUNCT	FF	_	10	punct	_	_
6	e	_	CCONJ	CC	_	10	cc	_	_
7	loro	_	PRON	PE	Number=Plur|Person=3|PronType=Prs	10	nsubj	_	_
8	si	_	PRON	PC	Clitic=Yes|Person=3|PronType=Prs	10	expl	_	_
9	sono	_	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	10	aux	_	_
10	arrabbiati	_	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	4	conj	_	_
11	talmente	_	ADV	B	_	10	advmod	_	_
12	tanto	_	ADV	B	_	15	advmod	_	_
13	che	_	SCONJ	CS	_	12	fixed	_	_
14	ci	_	PRON	PC	Clitic=Yes|Number=Plur|Person=1|PronType=Prs	15	expl	_	_
15	inseguivano	_	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	10	advcl	_	_
16	per	_	ADP	E	_	17	case	_	_
17	picchiarci	_	NOUN	S	Gender=Masc|Number=Plur	15	obl	_	_
18	.	_	PUNCT	FS	_	4	punct	_	_


# Le cose che ho imparato sono due: la prima è che non si sottovalutano gli avversari più o meno grandi di te, e la seconda cosa è di non arrabbiarsi per cose minime.
1	Le	_	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	2	det	_	_
2	cose	_	NOUN	S	Gender=Fem|Number=Plur	7	nsubj	_	_
3	che	_	PRON	PR	PronType=Rel	5	nsubj	_	_
4	ho	_	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	imparato	_	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	2	acl:relcl	_	_
6	sono	_	AUX	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	7	cop	_	_
7	due	_	NUM	N	NumType=Card	0	root	_	_
8	:	_	PUNCT	FC	_	7	punct	_	_
9	la	_	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	10	det	_	_
10	prima	_	ADJ	NO	Gender=Fem|Number=Sing|NumType=Ord	7	amod	_	_
11	è	_	AUX	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	10	acl:relcl	_	_
12	che	_	SCONJ	CS	_	15	mark	_	_
13	non	_	ADV	BN	PronType=Neg	15	advmod	_	_
14	si	_	PRON	PC	Clitic=Yes|Person=3|PronType=Prs	15	expl:pass	_	_
15	sottovalutano	_	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	11	advcl	_	_
16	gli	_	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	17	det	_	_
17	avversari	_	NOUN	S	Gender=Masc|Number=Plur	15	nsubj:pass	_	_
18	più	_	ADV	B	_	17	amod	_	_
19	o	_	CCONJ	CC	_	21	cc	_	_
20	meno	_	ADV	B	_	21	advmod	_	_
21	grandi	_	ADJ	A	Number=Plur	18	conj	_	_
22	di	_	ADP	E	_	23	case	_	_
23	te	_	PRON	PE	Gender=Fem|Number=Plur|Person=3|PronType=Prs	18	obl	_	_
24	,	_	PUNCT	FF	_	32	punct	_	_
25	e	_	CCONJ	CC	_	32	cc	_	_
26	la	_	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	28	det	_	_
27	seconda	_	ADJ	NO	Gender=Fem|Number=Sing|NumType=Ord	28	amod	_	_
28	cosa	_	NOUN	S	Gender=Fem|Number=Sing	32	nsubj	_	_
29	è	_	AUX	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	32	cop	_	_
30	di	_	ADP	E	_	32	case	_	_
31	non	_	ADV	BN	PronType=Neg	32	advmod	_	_
32	arrabbiarsi	_	ADJ	A	Gender=Masc|Number=Plur	15	conj	_	_
33	per	_	ADP	E	_	34	case	_	_
34	cose	_	NOUN	S	Gender=Fem|Number=Plur	32	obl	_	_
35	minime	_	ADJ	A	Gender=Fem|Number=Plur	34	amod	_	_
36	.	_	PUNCT	FS	_	7	punct	_	_
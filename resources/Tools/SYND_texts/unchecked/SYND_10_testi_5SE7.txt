# Allo spettacolo di pattinaggio  Un giorno sono andata a Lugano, per partecipare allo spettacolo di pattinaggio. 
1	Allo	_	ADJ	A	Gender=Masc|Number=Sing	2	amod	_	_
2	spettacolo	_	NOUN	S	Gender=Masc|Number=Sing	0	ROOT	_	_
3	di	_	ADP	E	-	4	case	_	_
4	pattinaggio	_	NOUN	S	Gender=Masc|Number=Sing	2	nmod	_	_
5	.	_	PUNCT	FS	-	2	punct	_	_

# Un gionro sono andato a Lugano, per partecipare allo spettacolo di pattinaggio.
1	Un	_	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	giorno	_	NOUN	S	Gender=Masc|Number=Sing	4	nmod	_	_
3	sono	_	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	4	aux	_	_
4	andata	_	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	0	ROOT	_	_
5	a	_	ADP	E	-	6	case	_	_
6	Lugano	_	PROPN	SP	-	4	nmod	_	_
7	,	_	PUNCT	FF	-	4	punct	_	_
8	per	_	ADP	E	fPOS=ADP++E	9	mark	_	_
9	partecipare	_	VERB	V	VerbForm=Inf	4	advcl	_	_
10	allo	_	ADJ	A	Gender=Masc|Number=Sing	11	amod	_	_
11	spettacolo	_	NOUN	S	Gender=Masc|Number=Sing	9	dobj	_	_
12	di	_	ADP	E	-	13	case	_	_
13	pattinaggio	_	NOUN	S	Gender=Masc|Number=Sing	11	nmod	_	_
14	.	_	PUNCT	FS	-	4	punct	_	_


# C’era una ragazza molto presuntuosa, che aveva fatto il suo spettacolo di fretta, per vantarsi, ma alla fine aveva perso. 
1	C’	_	PRON	PC	Clitic=Yes|PronType=Prs	2	expl	_	_
2	era	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
3	una	_	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	4	det	_	_
4	ragazza	_	NOUN	S	Gender=Fem|Number=Sing	2	nsubj	_	_
5	molto	_	ADV	B	_	6	advmod	_	_
6	presuntuosa	_	ADJ	A	Gender=Fem|Number=Sing	4	amod	_	_
7	,	_	PUNCT	FF	_	4	punct	_	_
8	che	_	PRON	PR	PronType=Rel	10	nsubj	_	_
9	aveva	_	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	10	aux	_	_
10	fatto	_	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	4	acl:relcl	_	_
11	il	_	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	13	det	_	_
12	suo	_	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	13	det:poss	_	_
13	spettacolo	_	NOUN	S	Gender=Masc|Number=Sing	10	obj	_	_
14	di	_	ADP	E	_	15	case	_	_
15	fretta	_	NOUN	S	Gender=Fem|Number=Sing	13	nmod	_	_
16	,	_	PUNCT	FF	_	10	punct	_	_
17	per	_	ADP	E	_	18	case	_	_
18	vantarsi	_	NOUN	S	Gender=Masc|Number=Plur	10	obl	_	_
19	,	_	PUNCT	FF	_	24	punct	_	_
20	ma	_	CCONJ	CC	_	24	cc	_	_
21	alla	_	DET	DI	Gender=Fem|Number=Sing|PronType=Ind	22	det	_	_
22	fine	_	NOUN	S	Gender=Fem|Number=Sing	24	nsubj	_	_
23	aveva	_	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	24	aux	_	_
24	perso	_	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	2	conj	_	_
25	.	_	PUNCT	FS	_	2	punct	_	_


# Io, dato che avevo visto che farlo veloce non andava bene, l’ho fatto con calma e ho vinto il primo posto. 
1	Io	_	PRON	PE	Number=Sing|Person=1|PronType=Prs	16	nsubj	_	_
2	,	_	PUNCT	FF	_	1	punct	_	_
3	dato	_	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	16	advcl	_	_
4	che	_	SCONJ	CS	_	6	mark	_	_
5	avevo	_	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	6	aux	_	_
6	visto	_	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	3	advcl	_	_
7	che	_	SCONJ	CS	_	11	mark	_	_
8	farlo	_	NOUN	S	Gender=Masc|Number=Sing	11	nsubj	_	_
9	veloce	_	ADJ	A	Number=Sing	8	amod	_	_
10	non	_	ADV	BN	PronType=Neg	11	advmod	_	_
11	andava	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	6	ccomp	_	_
12	bene	_	ADV	B	_	11	advmod	_	_
13	,	_	PUNCT	FF	_	3	punct	_	_
14	l’	_	PRON	PC	Clitic=Yes|Number=Sing|Person=3|PronType=Prs	16	obj	_	_
15	ho	_	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	16	aux	_	_
16	fatto	_	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
17	con	_	ADP	E	_	18	case	_	_
18	calma	_	NOUN	S	Gender=Fem|Number=Sing	16	obl	_	_
19	e	_	CCONJ	CC	_	21	cc	_	_
20	ho	_	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	21	aux	_	_
21	vinto	_	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	16	conj	_	_
22	il	_	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	24	det	_	_
23	primo	_	ADJ	NO	Gender=Masc|Number=Sing|NumType=Ord	24	amod	_	_
24	posto	_	NOUN	S	Gender=Masc|Number=Sing	21	obj	_	_
25	.	_	PUNCT	FS	_	16	punct	_	_


# MORALE: La morale è che l’egoismo e la vanità non portano niente. 
1	MORALE	_	PROPN	SP	_	5	obl	_	_
2	:	_	PUNCT	FC	_	1	punct	_	_
3	La	_	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	4	det	_	_
4	morale	_	NOUN	S	Number=Sing	5	nsubj	_	_
5	è	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
6	che	_	SCONJ	CS	_	13	mark	_	_
7	l’	_	DET	RD	Definite=Def|Number=Sing|PronType=Art	8	det	_	_
8	egoismo	_	NOUN	S	Gender=Masc|Number=Sing	13	nsubj	_	_
9	e	_	CCONJ	CC	_	11	cc	_	_
10	la	_	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	11	det	_	_
11	vanità	_	NOUN	S	Gender=Fem	8	conj	_	_
12	non	_	ADV	BN	PronType=Neg	13	advmod	_	_
13	portano	_	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	5	ccomp	_	_
14	niente	_	PRON	PI	Gender=Masc|Number=Sing|PronType=Ind	13	obj	_	_
15	.	_	PUNCT	FS	_	5	punct	_	_


# Quindi chi va piano va sano e lontano! 
1	Quindi	_	ADV	B	_	5	advmod	_	_
2	chi	_	PRON	PR	Number=Sing|PronType=Rel	5	nsubj	_	_
3	va	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	2	acl:relcl	_	_
4	piano	_	NOUN	S	Gender=Masc|Number=Sing	3	obj	_	_
5	va	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
6	sano	_	ADJ	A	Gender=Masc|Number=Sing	5	xcomp	_	_
7	e	_	CCONJ	CC	_	8	cc	_	_
8	lontano	_	ADJ	A	Gender=Masc|Number=Sing	6	conj	_	_
9	!	_	PUNCT	FS	_	5	punct	_	_


# THE END Poi fare gli esercizzi con calma mi è  servito in altre occasioni per esempio quando mi stavo allenando per dare il meglio di me. 
1	THE	_	DET	DD	Gender=Fem|Number=Plur|PronType=Dem	2	det	_	_
2	END	_	PROPN	SP	_	4	nsubj	_	_
3	Poi	_	PROPN	SP	_	2	flat:name	_	_
4	fare	_	VERB	V	VerbForm=Inf	0	root	_	_
5	gli	_	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	6	det	_	_
6	esercizzi	_	NOUN	S	Gender=Masc|Number=Plur	4	obj	_	_
7	con	_	ADP	E	_	8	case	_	_
8	calma	_	NOUN	S	Gender=Fem|Number=Sing	6	nmod	_	_
9	mi	_	PRON	PC	Clitic=Yes|Number=Sing|Person=1|PronType=Prs	11	iobj	_	_
10	è	_	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	11	aux	_	_
11	servito	_	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	4	parataxis	_	_
12	in	_	ADP	E	_	14	case	_	_
13	altre	_	DET	DI	Gender=Fem|Number=Plur|PronType=Ind	14	amod	_	_
14	occasioni	_	NOUN	S	Gender=Fem|Number=Plur	11	obl	_	_
15	per	_	ADP	E	_	16	case	_	_
16	esempio	_	NOUN	S	Gender=Masc|Number=Sing	14	nmod	_	_
17	quando	_	SCONJ	CS	_	20	mark	_	_
18	mi	_	PRON	PC	Clitic=Yes|Number=Sing|Person=1|PronType=Prs	20	expl	_	_
19	stavo	_	AUX	VA	Mood=Sub|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	20	aux	_	_
20	allenando	_	VERB	V	VerbForm=Ger	11	advcl	_	_
21	per	_	ADP	E	_	22	mark	_	_
22	dare	_	VERB	V	VerbForm=Inf	20	advcl	_	_
23	il	_	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	24	det	_	_
24	meglio	_	NOUN	S	Gender=Masc|Number=Sing	22	obj	_	_
25	di	_	ADP	E	_	26	case	_	_
26	me	_	PRON	PE	Number=Sing|Person=1|PronType=Prs	24	nmod	_	_
27	.	_	PUNCT	FS	_	4	punct	_	_


# Dando il meglio miglioro.
1	Dando	_	VERB	V	VerbForm=Ger	0	root	_	_
2	il	_	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	3	det	_	_
3	meglio	_	NOUN	S	Gender=Masc|Number=Sing	1	obj	_	_
4	miglioro	_	ADJ	A	Degree=Cmp|Number=Sing	3	amod	_	_
5	.	_	PUNCT	FS	_	1	punct	_	_
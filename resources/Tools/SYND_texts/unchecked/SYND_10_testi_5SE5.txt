# Io l’altra volta, volevo giocare a calcio con il mio fratellino, e un suo amico. 
1	Io	_	PRON	PE	Number=Sing|Person=1|PronType=Prs	7	nsubj	_	_
2	l’	_	DET	RD	Definite=Def|Number=Sing|PronType=Art	4	det	_	_
3	altra	_	ADJ	A	Gender=Fem|Number=Sing	4	amod	_	_
4	volta	_	NOUN	S	Gender=Fem|Number=Sing	1	nmod	_	_
5	,	_	PUNCT	FF	_	1	punct	_	_
6	volevo	_	AUX	VM	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	7	aux	_	_
7	giocare	_	VERB	V	VerbForm=Inf	0	root	_	_
8	a	_	ADP	E	_	9	case	_	_
9	calcio	_	NOUN	S	Gender=Masc|Number=Sing	7	obl	_	_
10	con	_	ADP	E	_	13	case	_	_
11	il	_	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	13	det	_	_
12	mio	_	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	13	det:poss	_	_
13	fratellino	_	NOUN	S	Gender=Masc|Number=Sing	7	obl	_	_
14	,	_	PUNCT	FF	_	18	punct	_	_
15	e	_	CCONJ	CC	_	18	cc	_	_
16	un	_	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	18	det	_	_
17	suo	_	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	18	det:poss	_	_
18	amico	_	NOUN	S	Gender=Masc|Number=Sing	13	conj	_	_
19	.	_	PUNCT	FS	_	7	punct	_	_


# Quando è iniziata li avevo sottovalutati dandoli 4 punti di vantaggio e quindi vinsero. 
1	Quando	_	SCONJ	CS	_	3	mark	_	_
2	è	_	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	3	aux	_	_
3	iniziata	_	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	6	advcl	_	_
4	li	_	PRON	PC	Clitic=Yes|Gender=Masc|Number=Plur|Person=3|PronType=Prs	6	obj	_	_
5	avevo	_	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	6	aux	_	_
6	sottovalutati	_	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	0	root	_	_
7	dandoli	_	NOUN	S	Gender=Masc|Number=Plur	9	amod	_	_
8	4	_	NUM	N	NumType=Card	9	nummod	_	_
9	punti	_	NOUN	S	Gender=Masc|Number=Plur	6	obl	_	_
10	di	_	ADP	E	_	11	case	_	_
11	vantaggio	_	NOUN	S	Gender=Masc|Number=Sing	9	nmod	_	_
12	e	_	CCONJ	CC	_	14	cc	_	_
13	quindi	_	ADV	B	_	14	advmod	_	_
14	vinsero	_	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Past|VerbForm=Fin	6	conj	_	_
15	.	_	PUNCT	FS	_	6	punct	_	_


# Poi quando vinsero hanno cominciato a figheggiarsi, e quindi ho voluto la rivincita. 
1	Poi	_	ADV	B	_	5	advmod	_	_
2	quando	_	SCONJ	CS	_	3	mark	_	_
3	vinsero	_	NOUN	S	Gender=Masc|Number=Sing	5	advcl	_	_
4	hanno	_	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	cominciato	_	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
6	a	_	ADP	E	_	7	case	_	_
7	figheggiarsi	_	NOUN	S	Gender=Masc|Number=Plur	5	obl	_	_
8	,	_	PUNCT	FF	_	12	punct	_	_
9	e	_	CCONJ	CC	_	12	cc	_	_
10	quindi	_	ADV	B	_	12	advmod	_	_
11	ho	_	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	12	aux	_	_
12	voluto	_	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	5	conj	_	_
13	la	_	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	14	det	_	_
14	rivincita	_	NOUN	S	Gender=Fem|Number=Sing	12	obj	_	_
15	.	_	PUNCT	FS	_	5	punct	_	_


# E la vinsi senza sottovalutare l’avversario. 
1	E	_	CCONJ	CC	_	3	cc	_	_
2	la	_	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	3	det	_	_
3	vinsi	_	NOUN	S	Gender=Fem	0	root	_	_
4	senza	_	ADP	E	_	5	mark	_	_
5	sottovalutare	_	VERB	V	VerbForm=Inf	3	acl	_	_
6	l’	_	DET	RD	Definite=Def|Number=Sing|PronType=Art	7	det	_	_
7	avversario	_	NOUN	S	Gender=Masc|Number=Sing	5	obj	_	_
8	.	_	PUNCT	FS	_	3	punct	_	_


# Riflessioni: Meglio non dare punti di vantaggio senò è sicuro o quasi sicuro che perdi. 
1	Riflessioni	_	NOUN	S	Gender=Fem|Number=Plur	11	nsubj	_	_
2	:	_	PUNCT	FC	_	1	punct	_	_
3	Meglio	_	PROPN	SP	_	5	nsubj	_	_
4	non	_	ADV	BN	PronType=Neg	5	advmod	_	_
5	dare	_	VERB	V	VerbForm=Inf	11	csubj	_	_
6	punti	_	NOUN	S	Gender=Masc|Number=Plur	5	obj	_	_
7	di	_	ADP	E	_	8	case	_	_
8	vantaggio	_	NOUN	S	Gender=Masc|Number=Sing	6	nmod	_	_
9	senò	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	5	obl	_	_
10	è	_	AUX	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	11	cop	_	_
11	sicuro	_	ADJ	A	Gender=Masc|Number=Sing	0	root	_	_
12	o	_	CCONJ	CC	_	14	cc	_	_
13	quasi	_	ADV	B	_	14	advmod	_	_
14	sicuro	_	ADJ	A	Gender=Masc|Number=Sing	11	conj	_	_
15	che	_	PRON	PR	PronType=Rel	16	mark	_	_
16	perdi	_	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	11	advcl	_	_
17	.	_	PUNCT	FS	_	11	punct	_	_


# Insegnamento: Mi ha insegnato a non sottovalutare l’avversario, dandoli punti di vantaggio.
1	Insegnamento	_	NOUN	S	Gender=Masc|Number=Sing	0	root	_	_
2	:	_	PUNCT	FC	_	1	punct	_	_
3	Mi	_	PRON	PC	Clitic=Yes|Number=Sing|Person=1|PronType=Prs	5	nsubj	_	_
4	ha	_	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	insegnato	_	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	1	parataxis	_	_
6	a	_	ADP	E	_	8	mark	_	_
7	non	_	ADV	BN	PronType=Neg	8	advmod	_	_
8	sottovalutare	_	VERB	V	VerbForm=Inf	5	xcomp	_	_
9	l’	_	DET	RD	Definite=Def|Number=Sing|PronType=Art	10	det	_	_
10	avversario	_	NOUN	S	Gender=Masc|Number=Sing	8	obj	_	_
11	,	_	PUNCT	FF	_	8	punct	_	_
12	dandoli	_	ADJ	A	Gender=Masc|Number=Plur	13	amod	_	_
13	punti	_	NOUN	S	Gender=Masc|Number=Plur	8	obj	_	_
14	di	_	ADP	E	_	15	case	_	_
15	vantaggio	_	NOUN	S	Gender=Masc|Number=Sing	13	nmod	_	_
16	.	_	PUNCT	FS	_	1	punct	_	_
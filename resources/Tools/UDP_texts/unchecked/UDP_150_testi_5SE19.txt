# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE19.txt
# newpar
# sent_id = 1
# text = Mercoledi venticinque Aprile, ho imparato un insegnamento molto importante; quello di: come fidarsi: ecco come è andata.
1	Mercoledi	Mercolede	VERB	V	Mood=Imp|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	0	root	_	_
2	venticinque	venticinque	ADV	B	_	3	advmod	_	_
3	Aprile	aprile	ADJ	A	Number=Sing	1	xcomp	_	SpaceAfter=No
4	,	,	PUNCT	FF	_	1	punct	_	_
5	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	6	aux	_	_
6	imparato	imparare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	1	conj	_	_
7	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	8	det	_	_
8	insegnamento	insegnamento	NOUN	S	Gender=Masc|Number=Sing	6	dobj	_	_
9	molto	molto	ADV	B	_	10	advmod	_	_
10	importante	importante	ADJ	A	Number=Sing	8	amod	_	SpaceAfter=No
11	;	;	PUNCT	FC	_	1	punct	_	_
12	quello	quello	PRON	PD	Gender=Masc|Number=Sing|PronType=Dem	1	dobj	_	_
13	di	di	ADP	E	_	16	mark	_	SpaceAfter=No
14	:	:	PUNCT	FC	_	16	punct	_	SpacesAfter=\s\s
15	come	come	ADP	E	_	16	mark	_	_
16-17	fidarsi	_	_	_	_	_	_	_	SpaceAfter=No
16	fidar	fidare	VERB	V	VerbForm=Inf	12	acl	_	_
17	si	si	PRON	PC	Person=3|PronType=Clit	16	expl	_	_
18	:	:	PUNCT	FC	_	16	punct	_	_
19	ecco	ecco	ADV	B	_	22	advmod	_	_
20	come	come	SCONJ	CS	_	22	mark	_	_
21	è	essere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	22	aux	_	_
22	andata	andare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	16	advcl	_	SpaceAfter=No
23	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 2
# text = qel giorno, ero antata (come tutti i mercoledì) a mangiare dalle mie due cugine, Nina (10 anni) e lisa (7 anni).
1	qel	qel	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	5	nmod	_	SpaceAfter=No
3	,	,	PUNCT	FF	_	2	punct	_	_
4	ero	essere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	5	aux	_	_
5	antata	antare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
6	(	(	PUNCT	FB	_	10	punct	_	SpaceAfter=No
7	come	come	ADP	E	_	10	case	_	_
8	tutti	tutto	DET	T	Gender=Masc|Number=Plur	10	det:predet	_	_
9	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	10	det	_	_
10	mercoledì	mercoledì	NOUN	S	Gender=Masc	5	nmod	_	SpaceAfter=No
11	)	)	PUNCT	FB	_	5	punct	_	_
12	a	a	ADP	E	_	13	mark	_	_
13	mangiare	mangiare	VERB	V	VerbForm=Inf	5	advcl	_	_
14-15	dalle	_	_	_	_	_	_	_	_
14	da	da	ADP	E	_	18	case	_	_
15	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	18	det	_	_
16	mie	mio	DET	AP	Gender=Fem|Number=Plur|Poss=Yes|PronType=Prs	18	det:poss	_	_
17	due	due	NUM	N	NumType=Card	18	nummod	_	_
18	cugine	cugine	NOUN	S	Gender=Fem|Number=Plur	13	nmod	_	SpaceAfter=No
19	,	,	PUNCT	FF	_	18	punct	_	_
20	Nina	Nina	PROPN	SP	_	18	conj	_	_
21	(	(	PUNCT	FB	_	23	punct	_	SpaceAfter=No
22	10	10	NUM	N	NumType=Card	23	nummod	_	_
23	anni	anno	NOUN	S	Gender=Masc|Number=Plur	20	nmod	_	SpaceAfter=No
24	)	)	PUNCT	FB	_	23	punct	_	_
25	e	e	CONJ	CC	_	18	cc	_	_
26	lisa	liso	NOUN	S	Gender=Fem|Number=Sing	18	conj	_	_
27	(	(	PUNCT	FB	_	29	punct	_	SpaceAfter=No
28	7	7	NUM	N	NumType=Card	29	nummod	_	_
29	anni	anno	NOUN	S	Gender=Masc|Number=Plur	26	nmod	_	SpaceAfter=No
30	)	)	PUNCT	FB	_	29	punct	_	SpaceAfter=No
31	.	.	PUNCT	FS	_	5	punct	_	_

# sent_id = 3
# text = Le mie cugine hanno tre cani, Briciola (la mamma), Muffin (il cucciolo piu grande) e Toffie (la più piccola).
1	Le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	3	det	_	_
2	mie	mio	DET	AP	Gender=Fem|Number=Plur|Poss=Yes|PronType=Prs	3	det:poss	_	_
3	cugine	cugine	NOUN	S	Gender=Fem|Number=Plur	4	nsubj	_	_
4	hanno	avere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
5	tre	tre	NUM	N	NumType=Card	6	nummod	_	_
6	cani	cane	NOUN	S	Gender=Masc|Number=Plur	4	dobj	_	SpaceAfter=No
7	,	,	PUNCT	FF	_	6	punct	_	_
8	Briciola	Briciola	PROPN	SP	_	6	conj	_	_
9	(	(	PUNCT	FB	_	11	punct	_	SpaceAfter=No
10	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	11	det	_	_
11	mamma	mamma	NOUN	S	Gender=Fem|Number=Sing	8	appos	_	SpaceAfter=No
12	)	)	PUNCT	FB	_	11	punct	_	SpaceAfter=No
13	,	,	PUNCT	FF	_	6	punct	_	_
14	Muffin	Muffin	PROPN	SP	_	6	conj	_	_
15	(	(	PUNCT	FB	_	17	punct	_	SpaceAfter=No
16	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	17	det	_	_
17	cucciolo	cucciolo	NOUN	S	Gender=Masc|Number=Sing	6	appos	_	_
18	piu	piu	ADJ	A	_	17	amod	_	_
19	grande	grande	ADJ	A	Number=Sing	17	amod	_	SpaceAfter=No
20	)	)	PUNCT	FB	_	17	punct	_	_
21	e	e	CONJ	CC	_	6	cc	_	_
22	Toffie	Toffie	PROPN	SP	_	6	conj	_	_
23	(	(	PUNCT	FB	_	26	punct	_	SpaceAfter=No
24	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	26	det	_	_
25	più	più	ADV	B	_	26	advmod	_	_
26	piccola	piccolo	ADJ	A	Gender=Fem|Number=Sing	22	amod	_	SpaceAfter=No
27	)	)	PUNCT	FB	_	26	punct	_	SpaceAfter=No
28	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 4
# text = Mia madre e Nina sono andati a fare una passeggiata con gli animali, mentre io e Lisa siamo state a casa a giocare.
1	Mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	2	det:poss	_	_
2	madre	madre	NOUN	S	Gender=Fem|Number=Sing	6	nsubj	_	_
3	e	e	CONJ	CC	_	2	cc	_	_
4	Nina	Nina	PROPN	SP	_	2	conj	_	_
5	sono	essere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	6	aux	_	_
6	andati	andare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	0	root	_	_
7	a	a	ADP	E	_	8	mark	_	_
8	fare	fare	VERB	V	VerbForm=Inf	6	xcomp	_	_
9	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	10	det	_	_
10	passeggiata	passeggiata	NOUN	S	Gender=Fem|Number=Sing	8	dobj	_	_
11	con	con	ADP	E	_	13	case	_	_
12	gli	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	13	det	_	_
13	animali	animale	NOUN	S	Gender=Masc|Number=Plur	10	nmod	_	SpaceAfter=No
14	,	,	PUNCT	FF	_	6	punct	_	_
15	mentre	mentre	SCONJ	CS	_	20	mark	_	_
16	io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	20	nsubj	_	_
17	e	e	CONJ	CC	_	16	cc	_	_
18	Lisa	Lisa	PROPN	SP	_	16	conj	_	_
19	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	20	aux	_	_
20	state	essere	VERB	V	Gender=Fem|Number=Plur|Tense=Past|VerbForm=Part	6	advcl	_	_
21	a	a	ADP	E	_	22	case	_	_
22	casa	casa	NOUN	S	Gender=Fem|Number=Sing	20	nmod	_	_
23	a	a	ADP	E	_	24	mark	_	_
24	giocare	giocare	VERB	V	VerbForm=Inf	20	xcomp	_	SpaceAfter=No
25	.	.	PUNCT	FS	_	6	punct	_	_

# sent_id = 5
# text = stavamo cercando in una borsa il sotto di una scatoletta cm 6x6.
1	stavamo	stare	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	2	aux	_	_
2	cercando	cercare	VERB	V	VerbForm=Ger	0	root	_	_
3	in	in	ADP	E	_	5	case	_	_
4	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	5	det	_	_
5	borsa	borsa	NOUN	S	Gender=Fem|Number=Sing	2	nmod	_	_
6	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	7	det	_	_
7	sotto	sotto	ADP	E	_	10	case	_	_
8	di	di	ADP	E	_	7	mwe	_	_
9	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	10	det	_	_
10	scatoletta	scatoletto	NOUN	S	Gender=Fem|Number=Sing	2	nmod	_	_
11	cm	cm	NOUN	S	_	10	nmod	_	_
12	6x6	6x6	NUM	N	NumType=Card	11	nummod	_	SpaceAfter=No
13	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 6
# text = La borsa era molto piena e non si riusciva bene a cercare;
1	La	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	2	det	_	_
2	borsa	borsa	NOUN	S	Gender=Fem|Number=Sing	5	nsubj	_	_
3	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	5	cop	_	_
4	molto	molto	ADV	B	_	5	advmod	_	_
5	piena	pieno	ADJ	A	Gender=Fem|Number=Sing	0	root	_	_
6	e	e	CONJ	CC	_	5	cc	_	_
7	non	non	ADV	BN	PronType=Neg	9	neg	_	_
8	si	si	PRON	PC	Person=3|PronType=Clit	9	expl:impers	_	_
9	riusciva	riuscire	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	5	conj	_	_
10	bene	bene	ADV	B	_	9	advmod	_	_
11	a	a	ADP	E	_	12	mark	_	_
12	cercare	cercare	VERB	V	VerbForm=Inf	9	xcomp	_	SpaceAfter=No
13	;	;	PUNCT	FC	_	5	punct	_	_

# sent_id = 7
# text = Lisa decise di rovescare il tutto in una scatola più grande, ma quando ebbe finito di rovesciarla, un sasso grande così:
1	Lisa	Lisa	PROPN	SP	_	2	nsubj	_	_
2	decise	decidere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
3	di	di	ADP	E	_	4	mark	_	_
4	rovescare	rovescare	VERB	V	VerbForm=Inf	2	xcomp	_	_
5	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	6	det	_	_
6	tutto	tutto	PRON	PI	Gender=Masc|Number=Sing|PronType=Ind	4	dobj	_	_
7	in	in	ADP	E	_	9	case	_	_
8	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	9	det	_	_
9	scatola	scatola	NOUN	S	Gender=Fem|Number=Sing	4	nmod	_	_
10	più	più	ADV	B	_	11	advmod	_	_
11	grande	grande	ADJ	A	Number=Sing	9	amod	_	SpaceAfter=No
12	,	,	PUNCT	FF	_	2	punct	_	_
13	ma	ma	CONJ	CC	_	2	cc	_	_
14	quando	quando	SCONJ	CS	_	15	mark	_	_
15	ebbe	avere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	2	conj	_	_
16	finito	finire	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	15	xcomp	_	_
17	di	di	ADP	E	_	18	case	_	_
18	rovesciarla	rovesciarla	NOUN	S	Gender=Masc	16	nmod	_	SpaceAfter=No
19	,	,	PUNCT	FF	_	15	punct	_	_
20	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	21	det	_	_
21	sasso	sasso	NOUN	S	Gender=Masc|Number=Sing	15	dobj	_	_
22	grande	grande	ADJ	A	Number=Sing	21	amod	_	_
23	così	così	ADV	B	_	21	advmod	_	SpaceAfter=No
24	:	:	PUNCT	FC	_	2	punct	_	SpacesAfter=\s\s

# sent_id = 8
# text = mi cadde sul piede e mi feci molto male.
1	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	2	dobj	_	_
2	cadde	caddere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
3-4	sul	_	_	_	_	_	_	_	_
3	su	su	ADP	E	_	5	case	_	_
4	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	5	det	_	_
5	piede	piede	NOUN	S	Gender=Masc|Number=Sing	2	nmod	_	_
6	e	e	CONJ	CC	_	5	cc	_	_
7	mi	mi	NOUN	S	Gender=Masc|Number=Plur	5	conj	_	_
8	feci	feci	ADJ	A	Number=Plur	7	amod	_	_
9	molto	molto	ADV	B	_	10	advmod	_	_
10	male	male	ADV	B	_	2	advmod	_	SpaceAfter=No
11	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 9
# text = Il mio dito era diventato tutto nero e usciva sangue da lì ho imparato che non ci si deve mai fidare troppo delle persone, neanche quelle più strette
1	Il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	3	det	_	_
2	mio	mio	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	3	det:poss	_	_
3	dito	dito	NOUN	S	Gender=Masc|Number=Sing	5	nsubj	_	_
4	era	essere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	5	aux	_	_
5	diventato	diventare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
6	tutto	tutto	PRON	PI	Gender=Masc|Number=Sing|PronType=Ind	7	advmod	_	_
7	nero	nero	ADJ	A	Gender=Masc|Number=Sing	5	xcomp	_	_
8	e	e	CONJ	CC	_	7	cc	_	_
9	usciva	uscivo	ADJ	A	Gender=Fem|Number=Sing	10	amod	_	_
10	sangue	sanguire	NOUN	S	Gender=Fem|Number=Sing	7	conj	_	_
11	da	da	ADP	E	_	12	case	_	_
12	lì	lì	ADV	B	_	10	advmod	_	_
13	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	14	aux	_	_
14	imparato	imparare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	5	advcl	_	_
15	che	che	SCONJ	CS	_	21	mark	_	_
16	non	non	ADV	BN	PronType=Neg	21	neg	_	_
17	ci	ci	PRON	PC	PronType=Clit	21	advmod	_	_
18	si	si	PRON	PC	Person=3|PronType=Clit	21	expl:impers	_	SpacesAfter=\s\s\s
19	deve	dovere	AUX	VM	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	21	aux	_	_
20	mai	mai	ADV	B	_	21	advmod	_	_
21	fidare	fidare	VERB	V	VerbForm=Inf	28	advcl	_	_
22	troppo	troppo	ADV	B	_	21	advmod	_	_
23-24	delle	_	_	_	_	_	_	_	_
23	di	di	ADP	E	_	25	case	_	_
24	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	25	det	_	_
25	persone	persona	NOUN	S	Gender=Fem|Number=Plur	21	nmod	_	SpaceAfter=No
26	,	,	PUNCT	FF	_	21	punct	_	_
27	neanche	neanche	ADV	BN	PronType=Neg	28	neg	_	_
28	quelle	quello	PRON	PD	Gender=Fem|Number=Plur|PronType=Dem	14	xcomp	_	_
29	più	più	ADV	B	_	30	advmod	_	_
30	strette	stretto	ADJ	A	Gender=Fem|Number=Plur	28	amod	_	SpacesAfter=\r\n\r\n


# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE116.txt
# newpar
# sent_id = 1
# text = Una piccola vacanza sull’orlo dell’acqua Era una mattina molto fredda quando alle 6 e mezza partimmo per andare in una piccola vacanza con le chiatte (delle piccole barche che viaggiano nei canali).
1	Una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	3	det	_	_
2	piccola	piccolo	ADJ	A	Gender=Fem|Number=Sing	3	amod	_	_
3	vacanza	vacanza	NOUN	S	Gender=Fem|Number=Sing	0	root	_	_
4	sull’orlo	sull’orlo	ADJ	A	_	3	amod	_	_
5	dell’acqua	dell’acqua	PRON	PP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	6	nsubj	_	SpacesAfter=\s\s
6	Era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	3	acl:relcl	_	_
7	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	8	det	_	_
8	mattina	mattina	NOUN	S	Gender=Fem|Number=Sing	6	dobj	_	_
9	molto	molto	ADV	B	_	10	advmod	_	_
10	fredda	freddo	ADJ	A	Gender=Fem|Number=Sing	8	amod	_	_
11	quando	quando	SCONJ	CS	_	17	mark	_	_
12-13	alle	_	_	_	_	_	_	_	_
12	a	a	ADP	E	_	14	case	_	_
13	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	14	det	_	_
14	6	6	NUM	N	NumType=Card	17	nummod	_	_
15	e	e	CONJ	CC	_	14	cc	_	_
16	mezza	mezzo	ADJ	A	Gender=Fem|Number=Sing	14	conj	_	SpacesAfter=\s\s\s
17	partimmo	partire	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Past|VerbForm=Fin	6	advcl	_	_
18	per	per	ADP	E	_	19	mark	_	_
19	andare	andare	VERB	V	VerbForm=Inf	17	xcomp	_	_
20	in	in	ADP	E	_	23	case	_	_
21	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	23	det	_	_
22	piccola	piccolo	ADJ	A	Gender=Fem|Number=Sing	23	amod	_	_
23	vacanza	vacanza	NOUN	S	Gender=Fem|Number=Sing	19	nmod	_	_
24	con	con	ADP	E	_	26	case	_	_
25	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	26	det	_	_
26	chiatte	chiatta	NOUN	S	Gender=Fem|Number=Plur	19	nmod	_	_
27	(	(	PUNCT	FB	_	31	punct	_	SpaceAfter=No
28-29	delle	_	_	_	_	_	_	_	_
28	di	di	ADP	E	_	31	case	_	_
29	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	31	det	_	_
30	piccole	piccolo	ADJ	A	Gender=Fem|Number=Plur	31	amod	_	_
31	barche	barca	NOUN	S	Gender=Fem|Number=Plur	26	nmod	_	_
32	che	che	PRON	PR	PronType=Rel	33	nsubj	_	_
33	viaggiano	viaggiare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	31	acl:relcl	_	_
34-35	nei	_	_	_	_	_	_	_	_
34	in	in	ADP	E	_	36	case	_	_
35	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	36	det	_	_
36	canali	canale	NOUN	S	Gender=Masc|Number=Plur	33	nmod	_	SpaceAfter=No
37	)	)	PUNCT	FB	_	31	punct	_	SpaceAfter=No
38	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 2
# text = Arrivammo a destinazione piú o meno alle 10 del mattino dove ci aspettavano già i miei padrini (Daniela, Cieco e i loro figli Jacopo, Timoti).
1	Arrivammo	Arrivare	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Past|VerbForm=Fin	0	root	_	_
2	a	a	ADP	E	_	3	case	_	_
3	destinazione	destinazione	NOUN	S	Gender=Fem|Number=Sing	1	nmod	_	_
4	piú	piú	PROPN	SP	_	3	nmod	_	_
5	o	o	CONJ	CC	_	4	cc	_	_
6	meno	meno	ADV	B	_	4	conj	_	_
7-8	alle	_	_	_	_	_	_	_	_
7	a	a	ADP	E	_	9	case	_	_
8	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	9	det	_	_
9	10	10	NUM	N	NumType=Card	1	nummod	_	_
10-11	del	_	_	_	_	_	_	_	_
10	di	di	ADP	E	_	12	case	_	_
11	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	12	det	_	_
12	mattino	mattino	NOUN	S	Gender=Masc|Number=Sing	9	nmod	_	_
13	dove	dove	ADV	B	_	15	advmod	_	_
14	ci	ci	PRON	PC	Number=Plur|Person=1|PronType=Clit	15	dobj	_	_
15	aspettavano	aspettare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	12	acl:relcl	_	SpacesAfter=\s\s\s
16	già	già	ADV	B	_	19	advmod	_	_
17	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	19	det	_	_
18	miei	mio	DET	AP	Gender=Masc|Number=Plur|Poss=Yes|PronType=Prs	19	det:poss	_	_
19	padrini	padrino	NOUN	S	Gender=Masc|Number=Plur	15	nsubj	_	_
20	(	(	PUNCT	FB	_	21	punct	_	SpaceAfter=No
21	Daniela	Daniela	PROPN	SP	_	19	nmod	_	SpaceAfter=No
22	,	,	PUNCT	FF	_	21	punct	_	_
23	Cieco	Cieco	PROPN	SP	_	21	conj	_	_
24	e	e	CONJ	CC	_	21	cc	_	_
25	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	27	det	_	_
26	loro	loro	DET	AP	Poss=Yes|PronType=Prs	27	det:poss	_	_
27	figli	figlio	NOUN	S	Gender=Masc|Number=Plur	21	conj	_	_
28	Jacopo	Jacopo	PROPN	SP	_	27	appos	_	SpaceAfter=No
29	,	,	PUNCT	FF	_	28	punct	_	_
30	Timoti	Timoto	PROPN	SP	_	28	conj	_	SpaceAfter=No
31	)	)	PUNCT	FB	_	28	punct	_	SpaceAfter=No
32	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 3
# text = In quell’istante eccola davanti a noi, la mitica avventura acquatica !
1	In	in	ADP	E	_	3	case	_	_
2	quell’istante	quell’istante	ADJ	A	Number=Sing	3	amod	_	_
3	eccola	eccolo	NOUN	S	Gender=Fem|Number=Sing	10	nmod	_	_
4	davanti	davanti	ADV	B	_	6	case	_	_
5	a	a	ADP	E	_	4	mwe	_	_
6	noi	noi	PRON	PE	Number=Plur|Person=1|PronType=Prs	3	nmod	_	SpaceAfter=No
7	,	,	PUNCT	FF	_	3	punct	_	_
8	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	10	det	_	_
9	mitica	mitico	ADJ	A	Gender=Fem|Number=Sing	10	amod	_	_
10	avventura	avventura	NOUN	S	Gender=Fem|Number=Sing	0	root	_	_
11	acquatica	acquatico	ADJ	A	Gender=Fem|Number=Sing	10	amod	_	SpacesAfter=\s\s\s
12	!	!	PUNCT	FS	_	10	punct	_	_

# sent_id = 4
# text = Nolleggiammo le chiatte, ma non partimmo subito, perché avevamo voglia di fare un giro nel paesaggio vicino.
1	Nolleggiammo	Nolleggiare	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Past|VerbForm=Fin	0	root	_	_
2	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	3	det	_	_
3	chiatte	chiatta	NOUN	S	Gender=Fem|Number=Plur	1	dobj	_	SpaceAfter=No
4	,	,	PUNCT	FF	_	1	punct	_	_
5	ma	ma	CONJ	CC	_	1	cc	_	_
6	non	non	ADV	BN	PronType=Neg	7	neg	_	_
7	partimmo	partire	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Past|VerbForm=Fin	1	conj	_	_
8	subito	subito	ADV	B	_	7	advmod	_	SpaceAfter=No
9	,	,	PUNCT	FF	_	1	punct	_	_
10	perché	perché	SCONJ	CS	_	11	mark	_	_
11	avevamo	avere	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	1	advcl	_	_
12	voglia	vogliare	NOUN	S	Gender=Fem|Number=Sing	11	dobj	_	_
13	di	di	ADP	E	_	14	mark	_	_
14	fare	fare	VERB	V	VerbForm=Inf	12	acl	_	_
15	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	16	det	_	_
16	giro	giro	NOUN	S	Gender=Masc|Number=Sing	14	dobj	_	_
17-18	nel	_	_	_	_	_	_	_	_
17	in	in	ADP	E	_	19	case	_	_
18	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	19	det	_	_
19	paesaggio	paesaggio	NOUN	S	Gender=Masc|Number=Sing	14	nmod	_	_
20	vicino	vicino	ADJ	A	Gender=Masc|Number=Sing	19	amod	_	SpaceAfter=No
21	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 5
# text = Al termine della giornata tornammo al nolleggio chiatte dove i miei padrini organizzarono sulla loro barca una bella cenetta.
1-2	Al	_	_	_	_	_	_	_	_
1	A	a	ADP	E	_	3	case	_	_
2	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	3	det	_	_
3	termine	termine	NOUN	S	Gender=Masc|Number=Sing	7	nmod	_	_
4-5	della	_	_	_	_	_	_	_	_
4	di	di	ADP	E	_	6	case	_	_
5	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	6	det	_	_
6	giornata	giornata	NOUN	S	Gender=Fem|Number=Sing	3	nmod	_	_
7	tornammo	tornare	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Past|VerbForm=Fin	0	root	_	_
8-9	al	_	_	_	_	_	_	_	_
8	a	a	ADP	E	_	10	case	_	_
9	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	10	det	_	_
10	nolleggio	nolleggio	NOUN	S	Gender=Masc|Number=Sing	11	nmod	_	_
11	chiatte	chiarre	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	7	advcl	_	_
12	dove	dove	ADV	B	_	16	advmod	_	_
13	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	15	det	_	_
14	miei	mio	DET	AP	Gender=Masc|Number=Plur|Poss=Yes|PronType=Prs	15	det:poss	_	_
15	padrini	padrino	NOUN	S	Gender=Masc|Number=Plur	16	nsubj	_	_
16	organizzarono	organizzarere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Past|VerbForm=Fin	11	acl:relcl	_	_
17-18	sulla	_	_	_	_	_	_	_	_
17	su	su	ADP	E	_	20	case	_	_
18	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	20	det	_	_
19	loro	loro	DET	AP	Poss=Yes|PronType=Prs	20	det:poss	_	_
20	barca	barca	NOUN	S	Gender=Fem|Number=Sing	16	nmod	_	_
21	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	23	det	_	_
22	bella	bello	ADJ	A	Gender=Fem|Number=Sing	23	amod	_	_
23	cenetta	cenetta	NOUN	S	Gender=Fem|Number=Sing	16	dobj	_	SpaceAfter=No
24	.	.	PUNCT	FS	_	7	punct	_	_

# sent_id = 6
# text = Dormimmo molto poco perché dovevamo partire per questa mitica avventura, sull’orlo della fredda acqua primaverile.
1	Dormimmo	Dormire	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Past|VerbForm=Fin	0	root	_	_
2	molto	molto	ADV	B	_	3	advmod	_	_
3	poco	poco	ADV	B	_	1	advmod	_	SpacesAfter=\s\s\s
4	perché	perché	SCONJ	CS	_	6	mark	_	_
5	dovevamo	dovere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	6	aux	_	_
6	partire	partire	VERB	V	VerbForm=Inf	1	advcl	_	_
7	per	per	ADP	E	_	10	case	_	_
8	questa	questo	DET	DD	Gender=Fem|Number=Sing|PronType=Dem	10	det	_	_
9	mitica	mitico	ADJ	A	Gender=Fem|Number=Sing	10	amod	_	_
10	avventura	avventura	NOUN	S	Gender=Fem|Number=Sing	6	nmod	_	SpaceAfter=No
11	,	,	PUNCT	FF	_	6	punct	_	_
12	sull’orlo	sull’orlo	NOUN	S	Gender=Masc|Number=Sing	6	dobj	_	_
13-14	della	_	_	_	_	_	_	_	_
13	di	di	ADP	E	_	16	case	_	_
14	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	16	det	_	_
15	fredda	freddo	ADJ	A	Gender=Fem|Number=Sing	16	amod	_	_
16	acqua	acqua	NOUN	S	Gender=Fem|Number=Sing	12	nmod	_	_
17	primaverile	primaverile	ADJ	A	Number=Sing	16	amod	_	SpaceAfter=No
18	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 7
# text = Partiti da mezzoretta incontrammo la prima piccola diga.
1	Partiti	partire	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	0	root	_	_
2	da	da	ADP	E	_	3	case	_	_
3	mezzoretta	mezzoretta	NOUN	S	Gender=Fem|Number=Sing	1	nmod	_	_
4	incontrammo	incontrare	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Past|VerbForm=Fin	1	advcl	_	_
5	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	8	det	_	_
6	prima	primo	ADJ	NO	Gender=Fem|Number=Sing|NumType=Ord	8	amod	_	_
7	piccola	piccolo	ADJ	A	Gender=Fem|Number=Sing	8	amod	_	_
8	diga	diga	NOUN	S	Gender=Fem|Number=Sing	4	dobj	_	SpaceAfter=No
9	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 8
# text = Ber fortuna prima di partire ci avevano spiegato bene come si doveva superarle.
1	Ber	Bere	NOUN	S	_	8	nsubj	_	_
2	fortuna	fortuna	NOUN	S	Gender=Fem|Number=Sing	1	nmod	_	_
3	prima	prima	ADV	B	_	5	mark	_	_
4	di	di	ADP	E	_	3	mwe	_	_
5	partire	partire	VERB	V	VerbForm=Inf	8	advcl	_	_
6	ci	ci	PRON	PC	Number=Plur|Person=1|PronType=Clit	5	advmod	_	_
7	avevano	avere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	8	aux	_	_
8	spiegato	spiegare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
9	bene	bene	ADV	B	_	8	advmod	_	_
10	come	come	ADP	E	_	13	case	_	_
11	si	si	PRON	PC	Person=3|PronType=Clit	13	expl:impers	_	_
12	doveva	dovere	AUX	VM	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	13	aux	_	SpacesAfter=\s\s\s
13	superarle	superarle	ADJ	A	Gender=Fem|Number=Plur	8	nmod	_	SpaceAfter=No
14	.	.	PUNCT	FS	_	8	punct	_	_

# sent_id = 9
# text = Inanzitutto dovevamo schiacciare un bottone per fare aprire il “rubinetto” che riempiva la vasca.
1	Inanzitutto	Inanzitutto	ADV	B	_	3	advmod	_	_
2	dovevamo	dovere	AUX	VM	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	3	aux	_	_
3	schiacciare	schiacciare	VERB	V	VerbForm=Inf	0	root	_	_
4	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	5	det	_	_
5	bottone	bottone	NOUN	S	Gender=Masc|Number=Sing	3	dobj	_	_
6	per	per	ADP	E	_	7	mark	_	_
7	fare	fare	VERB	V	VerbForm=Inf	3	advcl	_	_
8	aprire	aprire	VERB	V	VerbForm=Inf	7	ccomp	_	_
9	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	11	det	_	_
10	“	“	PUNCT	FB	_	11	punct	_	SpaceAfter=No
11	rubinetto	rubinetto	NOUN	S	Gender=Masc|Number=Sing	8	dobj	_	SpaceAfter=No
12	”	”	PUNCT	FB	_	11	punct	_	_
13	che	che	PRON	PR	PronType=Rel	14	nsubj	_	_
14	riempiva	riempire	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	11	acl:relcl	_	_
15	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	16	det	_	_
16	vasca	vasca	NOUN	S	Gender=Fem|Number=Sing	14	dobj	_	SpaceAfter=No
17	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 10
# text = Aperta la barriera di ferro con le chiatte si andava avanti di poco.
1	Aperta	aprire	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
2	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	3	det	_	_
3	barriera	barriera	NOUN	S	Gender=Fem|Number=Sing	1	nsubj	_	_
4	di	di	ADP	E	_	5	case	_	_
5	ferro	ferro	NOUN	S	Gender=Masc|Number=Sing	3	nmod	_	_
6	con	con	ADP	E	_	8	case	_	_
7	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	8	det	_	_
8	chiatte	chiatta	NOUN	S	Gender=Fem|Number=Plur	10	nmod	_	_
9	si	si	PRON	PC	Person=3|PronType=Clit	10	expl	_	_
10	andava	andare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	1	ccomp	_	_
11	avanti	avanti	ADV	B	_	10	advmod	_	_
12	di	di	ADP	E	_	13	case	_	_
13	poco	poco	ADV	B	_	11	advmod	_	SpaceAfter=No
14	.	.	PUNCT	FS	_	1	punct	_	SpacesAfter=\s\s\s

# sent_id = 11
# text = In seguito si é richiusa la barriera alle nostre spalle e poi abbiamo dovuto scendere (tranne Cieco e mio papa che dovevano stare sulla barca).
1	In	in	ADP	E	_	2	case	_	_
2	seguito	seguito	NOUN	S	Gender=Masc|Number=Sing	4	nmod	_	_
3	si	si	PRON	PC	Person=3|PronType=Clit	4	expl	_	_
4	é	é	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
5	richiusa	richiudere	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	4	xcomp	_	_
6	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	7	det	_	_
7	barriera	barriera	NOUN	S	Gender=Fem|Number=Sing	4	dobj	_	_
8-9	alle	_	_	_	_	_	_	_	_
8	a	a	ADP	E	_	11	case	_	_
9	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	11	det	_	_
10	nostre	nostro	DET	AP	Gender=Fem|Number=Plur|Poss=Yes|PronType=Prs	11	det:poss	_	_
11	spalle	spalla	NOUN	S	Gender=Fem|Number=Plur	7	nmod	_	_
12	e	e	CONJ	CC	_	4	cc	_	_
13	poi	poi	ADV	B	_	16	advmod	_	_
14	abbiamo	avere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	16	aux	_	_
15	dovuto	dovere	AUX	VM	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	16	aux	_	_
16	scendere	scendere	VERB	V	VerbForm=Inf	4	conj	_	_
17	(	(	PUNCT	FB	_	19	punct	_	SpaceAfter=No
18	tranne	tranne	ADP	E	_	19	case	_	_
19	Cieco	Cieco	PROPN	SP	_	16	nmod	_	_
20	e	e	CONJ	CC	_	19	cc	_	_
21	mio	mio	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	22	det:poss	_	_
22	papa	papa	NOUN	S	Gender=Masc|Number=Sing	19	conj	_	_
23	che	che	PRON	PR	PronType=Rel	25	nsubj	_	_
24	dovevano	dovere	AUX	VM	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	25	aux	_	_
25	stare	stare	VERB	V	VerbForm=Inf	22	acl:relcl	_	_
26-27	sulla	_	_	_	_	_	_	_	_
26	su	su	ADP	E	_	28	case	_	_
27	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	28	det	_	_
28	barca	barca	NOUN	S	Gender=Fem|Number=Sing	25	nmod	_	SpaceAfter=No
29	)	)	PUNCT	FB	_	4	punct	_	SpaceAfter=No
30	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 12
# text = A riva abbiamo dovuto attaccare la barca con una corda e poi svuotata la vasca (quasi tutta).
1	A	a	ADP	E	_	2	case	_	_
2	riva	riva	NOUN	S	Gender=Fem|Number=Sing	5	nmod	_	_
3	abbiamo	avere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	5	aux	_	_
4	dovuto	dovere	AUX	VM	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	5	aux	_	_
5	attaccare	attaccare	VERB	V	VerbForm=Inf	0	root	_	_
6	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	7	det	_	_
7	barca	barca	NOUN	S	Gender=Fem|Number=Sing	5	dobj	_	_
8	con	con	ADP	E	_	10	case	_	_
9	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	10	det	_	_
10	corda	corda	NOUN	S	Gender=Fem|Number=Sing	5	nmod	_	_
11	e	e	CONJ	CC	_	10	cc	_	_
12	poi	poi	ADV	B	_	13	advmod	_	_
13	svuotata	svuotare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	10	conj	_	_
14	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	15	det	_	_
15	vasca	vasca	NOUN	S	Gender=Fem|Number=Sing	13	dobj	_	_
16	(	(	PUNCT	FB	_	18	punct	_	SpaceAfter=No
17	quasi	quasi	ADV	B	_	18	advmod	_	_
18	tutta	tutto	ADJ	A	Gender=Fem|Number=Sing	15	amod	_	SpaceAfter=No
19	)	)	PUNCT	FB	_	18	punct	_	SpaceAfter=No
20	.	.	PUNCT	FS	_	5	punct	_	_

# sent_id = 13
# text = Si é poi aperta la barriera davanti e le chiatte (giudate dai papa) sono uscite dalla dighetta.
1	Si	si	PRON	PC	Person=3|PronType=Clit	2	expl	_	_
2	é	é	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
3	poi	poi	ADV	B	_	4	advmod	_	_
4	aperta	aprire	ADJ	A	Gender=Fem|Number=Sing	2	xcomp	_	_
5	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	6	det	_	_
6	barriera	barriera	NOUN	S	Gender=Fem|Number=Sing	2	dobj	_	_
7	davanti	davanti	ADV	B	_	6	advmod	_	_
8	e	e	CONJ	CC	_	6	cc	_	_
9	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	10	det	_	_
10	chiatte	chiatta	NOUN	S	Gender=Fem|Number=Plur	6	conj	_	_
11	(	(	PUNCT	FB	_	12	punct	_	SpaceAfter=No
12	giudate	giudare	VERB	V	Gender=Fem|Number=Plur|Tense=Past|VerbForm=Part	6	acl	_	_
13-14	dai	_	_	_	_	_	_	_	_
13	da	da	ADP	E	_	15	case	_	_
14	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	15	det	_	_
15	papa	papa	NOUN	S	Gender=Masc|Number=Sing	12	nmod	_	SpaceAfter=No
16	)	)	PUNCT	FB	_	12	punct	_	_
17	sono	essere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	18	auxpass	_	_
18	uscite	uscire	VERB	V	Gender=Fem|Number=Plur|Tense=Past|VerbForm=Part	2	nsubj	_	_
19-20	dalla	_	_	_	_	_	_	_	_
19	da	da	ADP	E	_	21	case	_	_
20	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	21	det	_	_
21	dighetta	dighetta	NOUN	S	Gender=Fem|Number=Sing	18	nmod	_	SpaceAfter=No
22	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 14
# text = Poi abbiamo staccato la corda e l’abbiamo potuta tirare sulla chiatta.
1	Poi	poi	ADV	B	_	3	advmod	_	_
2	abbiamo	avere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	3	aux	_	_
3	staccato	staccare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
4	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	5	det	_	_
5	corda	corda	NOUN	S	Gender=Fem|Number=Sing	3	dobj	_	_
6	e	e	CONJ	CC	_	3	cc	_	_
7	l’abbiamo	l’abbare	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	3	conj	_	_
8	potuta	potere	AUX	VM	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	9	aux	_	_
9	tirare	tirare	VERB	V	VerbForm=Inf	7	ccomp	_	_
10-11	sulla	_	_	_	_	_	_	_	_
10	su	su	ADP	E	_	12	case	_	_
11	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	12	det	_	_
12	chiatta	chiatta	NOUN	S	Gender=Fem|Number=Sing	9	nmod	_	SpaceAfter=No
13	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 15
# text = Daniela ha lanciato la corda velocemente e corta quindi si é impigliata nel motore.
1	Daniela	Daniela	PROPN	SP	_	3	nsubj	_	_
2	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	3	aux	_	_
3	lanciato	lanciare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
4	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	5	det	_	_
5	corda	corda	NOUN	S	Gender=Fem|Number=Sing	3	dobj	_	_
6	velocemente	velocemente	ADJ	A	Number=Sing	5	amod	_	_
7	e	e	CONJ	CC	_	6	cc	_	_
8	corta	cortare	ADJ	A	Gender=Fem|Number=Sing	6	conj	_	_
9	quindi	quindi	ADV	B	_	3	advmod	_	_
10	si	si	PRON	PC	Person=3|PronType=Clit	12	expl	_	_
11	é	é	PRON	PP	Number=Sing|Poss=Yes|PronType=Prs	12	dobj	_	_
12	impigliata	impigliare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	3	advcl	_	_
13-14	nel	_	_	_	_	_	_	_	_
13	in	in	ADP	E	_	15	case	_	_
14	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	15	det	_	_
15	motore	motore	NOUN	S	Gender=Masc|Number=Sing	12	nmod	_	SpaceAfter=No
16	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 16
# text = Purtroppo abbiamo dovuto chiamare il padrone della barca.
1	Purtroppo	purtroppo	ADV	B	_	4	advmod	_	_
2	abbiamo	avere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	4	aux	_	_
3	dovuto	dovere	AUX	VM	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	4	aux	_	_
4	chiamare	chiamare	VERB	V	VerbForm=Inf	0	root	_	_
5	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	6	det	_	_
6	padrone	padrone	NOUN	S	Gender=Masc|Number=Sing	4	dobj	_	_
7-8	della	_	_	_	_	_	_	_	_
7	di	di	ADP	E	_	9	case	_	_
8	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	9	det	_	_
9	barca	barca	NOUN	S	Gender=Fem|Number=Sing	6	nmod	_	SpaceAfter=No
10	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 17
# text = Visto che non era lontano ci ha raggiunto in macchina e ci ha anche detto che dovevamo aspettare.
1	Visto	vedere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
2	che	che	SCONJ	CS	_	8	mark	_	_
3	non	non	ADV	BN	PronType=Neg	8	neg	_	_
4	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	8	cop	_	_
5	lontano	lontano	ADV	B	_	8	advmod	_	_
6	ci	ci	PRON	PC	PronType=Clit	8	advmod	_	_
7	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	8	aux	_	_
8	raggiunto	raggiungere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	1	ccomp	_	_
9	in	in	ADP	E	_	10	case	_	_
10	macchina	macchina	NOUN	S	Gender=Fem|Number=Sing	8	nmod	_	_
11	e	e	CONJ	CC	_	8	cc	_	_
12	ci	ci	PRON	PC	PronType=Clit	15	advmod	_	_
13	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	15	aux	_	_
14	anche	anche	ADV	B	_	15	advmod	_	_
15	detto	dire	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	8	conj	_	_
16	che	che	SCONJ	CS	_	18	mark	_	_
17	dovevamo	dovere	AUX	VM	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	18	aux	_	_
18	aspettare	aspettare	VERB	V	VerbForm=Inf	15	ccomp	_	SpaceAfter=No
19	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 18
# text = Quindi, visto che era quasi mezzogiorno abbiamo fatto un piccolo pranzo a base di pasta sulla nostra chiatta.
1	Quindi	quindi	ADV	B	_	3	advmod	_	SpaceAfter=No
2	,	,	PUNCT	FF	_	1	punct	_	_
3	visto	vedere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
4	che	che	SCONJ	CS	_	7	mark	_	_
5	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	7	cop	_	_
6	quasi	quasi	ADV	B	_	7	advmod	_	_
7	mezzogiorno	mezzogiorno	NOUN	S	Gender=Masc|Number=Sing	3	ccomp	_	_
8	abbiamo	avere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	9	aux	_	_
9	fatto	fare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	7	csubj	_	_
10	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	12	det	_	_
11	piccolo	piccolo	ADJ	A	Gender=Masc|Number=Sing	12	amod	_	_
12	pranzo	pranzo	NOUN	S	Gender=Masc|Number=Sing	9	dobj	_	_
13	a	a	ADP	E	_	14	case	_	_
14	base	base	NOUN	S	Gender=Fem|Number=Sing	12	nmod	_	_
15	di	di	ADP	E	_	16	case	_	_
16	pasta	pasta	NOUN	S	Gender=Fem|Number=Sing	14	nmod	_	_
17-18	sulla	_	_	_	_	_	_	_	_
17	su	su	ADP	E	_	20	case	_	_
18	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	20	det	_	_
19	nostra	nostro	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	20	det:poss	_	_
20	chiatta	chiatta	NOUN	S	Gender=Fem|Number=Sing	12	nmod	_	SpaceAfter=No
21	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 19
# text = Dopo pranzo ci ha informato che era a posto e potevamo partire tranquillamente per i lunghi canali.
1	Dopo	dopo	ADP	E	_	2	case	_	_
2	pranzo	pranzo	NOUN	S	Gender=Masc|Number=Sing	5	nmod	_	_
3	ci	ci	PRON	PC	Number=Plur|Person=1|PronType=Clit	5	iobj	_	_
4	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	informato	informare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
6	che	che	SCONJ	CS	_	7	mark	_	_
7	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	5	ccomp	_	_
8	a	a	ADP	E	_	9	case	_	_
9	posto	posto	NOUN	S	Gender=Masc|Number=Sing	7	nmod	_	_
10	e	e	CONJ	CC	_	7	cc	_	_
11	potevamo	potere	AUX	VM	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	12	aux	_	_
12	partire	partire	VERB	V	VerbForm=Inf	7	conj	_	_
13	tranquillamente	tranquillamente	ADV	B	_	12	advmod	_	_
14	per	per	ADP	E	_	17	case	_	_
15	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	17	det	_	_
16	lunghi	lungo	ADJ	A	Gender=Masc|Number=Plur	17	amod	_	_
17	canali	canale	NOUN	S	Gender=Masc|Number=Plur	12	nmod	_	SpaceAfter=No
18	.	.	PUNCT	FS	_	5	punct	_	_

# sent_id = 20
# text = Da questa avventura o imparato che bisogna avere calma per fare le cose bene e che chi va piano va lontano.
1	Da	da	ADP	E	_	3	case	_	_
2	questa	questo	DET	DD	Gender=Fem|Number=Sing|PronType=Dem	3	det	_	_
3	avventura	avventura	NOUN	S	Gender=Fem|Number=Sing	20	nmod	_	_
4	o	o	CONJ	CC	_	3	cc	_	_
5	imparato	imparare	NOUN	S	Gender=Masc|Number=Sing	3	conj	_	_
6	che	che	PRON	PR	PronType=Rel	7	nsubj	_	_
7	bisogna	bisognare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	5	acl:relcl	_	_
8	avere	avere	VERB	V	VerbForm=Inf	7	ccomp	_	_
9	calma	calma	NOUN	S	Gender=Fem|Number=Sing	8	dobj	_	_
10	per	per	ADP	E	_	11	mark	_	_
11	fare	fare	VERB	V	VerbForm=Inf	8	advcl	_	_
12	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	13	det	_	_
13	cose	cosa	NOUN	S	Gender=Fem|Number=Plur	11	dobj	_	_
14	bene	bene	ADJ	A	_	13	amod	_	_
15	e	e	CONJ	CC	_	13	cc	_	_
16	che	che	PRON	PR	PronType=Rel	18	nsubj	_	_
17	chi	chi	PRON	PR	Number=Sing|PronType=Rel	18	dobj	_	_
18	va	andare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	13	acl:relcl	_	_
19	piano	piano	ADV	B	_	18	advmod	_	_
20	va	andare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
21	lontano	lontano	ADV	B	_	20	advmod	_	SpaceAfter=No
22	.	.	PUNCT	FS	_	20	punct	_	SpacesAfter=\r\n\r\n


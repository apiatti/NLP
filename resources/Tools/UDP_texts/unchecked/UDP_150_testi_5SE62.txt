# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE62.txt
# newpar
# sent_id = 1
# text = VIOLINO Io suono il violino, è uno strumento ad arco.
1	VIOLINO	violino	ADV	B	_	3	advmod	_	SpacesAfter=\s\s
2	Io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	3	nsubj	_	_
3	suono	suere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
4	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	5	det	_	_
5	violino	violino	NOUN	S	Gender=Masc|Number=Sing	3	dobj	_	SpaceAfter=No
6	,	,	PUNCT	FF	_	3	punct	_	_
7	è	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	9	cop	_	_
8	uno	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	9	det	_	_
9	strumento	strumento	NOUN	S	Gender=Masc|Number=Sing	3	conj	_	_
10	ad	a	ADP	E	_	11	case	_	_
11	arco	arco	NOUN	S	Gender=Masc|Number=Sing	9	nmod	_	SpaceAfter=No
12	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 2
# text = Ha quattro corde:
1	Ha	avere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
2	quattro	quattro	NUM	N	NumType=Card	3	nummod	_	_
3	corde	corda	NOUN	S	Gender=Fem|Number=Plur	1	dobj	_	SpaceAfter=No
4	:	:	PUNCT	FC	_	1	punct	_	_

# sent_id = 3
# text = MI-LA-RE-SOL.
1	MI-LA-RE-SOL.	MI-LA-RE-SOlira	NOUN	S	Gender=Fem	0	root	_	_

# sent_id = 4
# text = L’arco è fatto di legno; i fili sono coda di cavallo.
1	L’arco	L’arco	PROPN	SP	_	3	nsubjpass	_	_
2	è	essere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	3	auxpass	_	_
3	fatto	fare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
4	di	di	ADP	E	_	5	case	_	_
5	legno	legno	NOUN	S	Gender=Masc|Number=Sing	3	nmod	_	SpaceAfter=No
6	;	;	PUNCT	FC	_	3	punct	_	_
7	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	8	det	_	_
8	fili	filo	NOUN	S	Gender=Masc|Number=Plur	10	nsubj	_	_
9	sono	essere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	10	cop	_	_
10	coda	codare	NOUN	S	Gender=Fem|Number=Sing	3	parataxis	_	_
11	di	di	ADP	E	_	12	case	_	_
12	cavallo	cavallo	NOUN	S	Gender=Masc|Number=Sing	10	nmod	_	SpaceAfter=No
13	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 5
# text = Ogni tanto i “fili„ si staccano, quando ce ne sono pochi bisogna cambiare l’arco.
1	Ogni	ogni	DET	DI	Number=Sing|PronType=Ind	2	det	_	_
2	tanto	tanto	NOUN	S	Gender=Masc	8	nmod	_	_
3	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	5	det	_	_
4	“	“	PUNCT	FB	_	5	punct	_	SpaceAfter=No
5	fili	filo	NOUN	S	Gender=Masc|Number=Plur	8	nsubj	_	SpaceAfter=No
6	„	„	PUNCT	FF	_	5	punct	_	_
7	si	si	PRON	PC	Person=3|PronType=Clit	8	expl	_	_
8	staccano	staccare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	15	advcl	_	SpaceAfter=No
9	,	,	PUNCT	FF	_	8	punct	_	_
10	quando	quando	SCONJ	CS	_	13	mark	_	_
11	ce	ci	PRON	PC	PronType=Clit	13	expl	_	_
12	ne	ne	PRON	PC	PronType=Clit	13	expl	_	_
13	sono	essere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	15	advcl	_	_
14	pochi	poco	PRON	PI	Gender=Masc|Number=Plur|PronType=Ind	15	nsubj	_	_
15	bisogna	bisognare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
16	cambiare	cambiare	VERB	V	VerbForm=Inf	15	ccomp	_	_
17	l’arco	l’arco	NOUN	S	Gender=Masc|Number=Sing	16	dobj	_	SpaceAfter=No
18	.	.	PUNCT	FS	_	15	punct	_	_

# sent_id = 6
# text = Un'altra curiosità è che il violino è fatto di legno d’abete rosso.
1	Un'	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	3	det	_	SpaceAfter=No
2	altra	altro	ADJ	A	Gender=Fem|Number=Sing	3	amod	_	_
3	curiosità	curiosità	NOUN	S	Gender=Fem	4	nsubj	_	_
4	è	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
5	che	che	SCONJ	CS	_	9	mark	_	_
6	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	7	det	_	_
7	violino	violino	NOUN	S	Gender=Masc|Number=Sing	9	nsubjpass	_	_
8	è	essere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	9	auxpass	_	_
9	fatto	fare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	4	ccomp	_	_
10	di	di	ADP	E	_	11	case	_	_
11	legno	legno	NOUN	S	Gender=Masc|Number=Sing	9	nmod	_	_
12	d’abete	d’abeto	VERB	V	Mood=Ind|Number=Plur|Person=2|Tense=Pres|VerbForm=Fin	11	acl	_	_
13	rosso	rosso	ADJ	A	Gender=Masc|Number=Sing	12	xcomp	_	SpaceAfter=No
14	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 7
# text = Lo suono da 5 anni ci sono stati momenti dfficili che volevo smettere di suonare, all’inizio perchè dicevo che non mi piaceva.
1	Lo	lo	PRON	PC	Gender=Masc|Number=Sing|Person=3|PronType=Clit	2	dobj	_	_
2	suono	suere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	8	advcl	_	_
3	da	da	ADP	E	_	5	case	_	_
4	5	5	NUM	N	NumType=Card	5	nummod	_	_
5	anni	anno	NOUN	S	Gender=Masc|Number=Plur	2	nmod	_	_
6	ci	ci	PRON	PC	PronType=Clit	8	expl	_	_
7	sono	essere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	8	aux	_	_
8	stati	essere	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	0	root	_	_
9	momenti	momento	NOUN	S	Gender=Masc|Number=Plur	8	nsubj	_	_
10	dfficili	dfficile	ADJ	A	Number=Plur	9	amod	_	_
11	che	che	PRON	PR	PronType=Rel	13	nsubj	_	_
12	volevo	volere	AUX	VM	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	13	aux	_	_
13	smettere	smettere	VERB	V	VerbForm=Inf	9	acl:relcl	_	_
14	di	di	ADP	E	_	15	mark	_	_
15	suonare	suonare	VERB	V	VerbForm=Inf	13	xcomp	_	SpaceAfter=No
16	,	,	PUNCT	FF	_	15	punct	_	_
17	all’inizio	all’inizio	ADV	B	_	19	advmod	_	SpacesAfter=\s\s
18	perchè	perché	SCONJ	CS	_	19	mark	_	_
19	dicevo	dire	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	15	advcl	_	_
20	che	che	SCONJ	CS	_	23	mark	_	_
21	non	non	ADV	BN	PronType=Neg	23	neg	_	_
22	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	23	iobj	_	_
23	piaceva	piacere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	19	ccomp	_	SpaceAfter=No
24	.	.	PUNCT	FS	_	8	punct	_	_

# sent_id = 8
# text = Ma dopotutto è bello saper suonare uno strumento perchè puoi conoscere di più dei compositori molto famosi.
1	Ma	ma	CONJ	CC	_	4	cc	_	_
2	dopotutto	dopotutto	ADV	B	_	4	advmod	_	_
3	è	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	4	cop	_	_
4	bello	bello	ADJ	A	Gender=Masc|Number=Sing	0	root	_	_
5	saper	sapere	VERB	V	VerbForm=Inf	4	csubj	_	_
6	suonare	suonare	VERB	V	VerbForm=Inf	5	ccomp	_	_
7	uno	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	8	det	_	_
8	strumento	strumento	NOUN	S	Gender=Masc|Number=Sing	6	dobj	_	_
9	perchè	perché	SCONJ	CS	_	11	mark	_	_
10	puoi	potere	AUX	VM	Mood=Ind|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	11	aux	_	_
11	conoscere	conoscere	VERB	V	VerbForm=Inf	6	advcl	_	_
12	di	di	ADP	E	_	13	case	_	_
13	più	più	ADV	B	_	11	advmod	_	_
14-15	dei	_	_	_	_	_	_	_	_
14	di	di	ADP	E	_	16	case	_	_
15	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	16	det	_	_
16	compositori	compositore	NOUN	S	Gender=Masc|Number=Plur	11	nmod	_	_
17	molto	molto	ADV	B	_	18	advmod	_	_
18	famosi	famoso	ADJ	A	Gender=Masc|Number=Plur	16	amod	_	SpaceAfter=No
19	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 9
# text = Ma la storia che mi è capitata è questa qua… Per 5 anni non sono quasi mai stato assente.
1	Ma	ma	CONJ	CC	_	9	cc	_	_
2	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	3	det	_	_
3	storia	storia	NOUN	S	Gender=Fem|Number=Sing	9	nsubj	_	_
4	che	che	PRON	PR	PronType=Rel	7	nsubj	_	_
5	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	7	dobj	_	_
6	è	essere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	7	aux	_	_
7	capitata	capitare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	3	acl:relcl	_	_
8	è	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	9	cop	_	_
9	questa	questo	PRON	PD	Gender=Fem|Number=Sing|PronType=Dem	18	nsubj	_	_
10	qua…	qua…	PUNCT	FF	_	9	punct	_	_
11	Per	per	ADP	E	_	13	case	_	_
12	5	5	NUM	N	NumType=Card	13	nummod	_	_
13	anni	anno	NOUN	S	Gender=Masc|Number=Plur	9	nmod	_	_
14	non	non	ADV	BN	PronType=Neg	18	neg	_	_
15	sono	essere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	18	cop	_	_
16	quasi	quasi	ADV	B	_	18	advmod	_	_
17	mai	mai	ADV	B	_	18	advmod	_	_
18	stato	stato	NOUN	S	Gender=Masc|Number=Sing	0	root	_	_
19	assente	assente	ADJ	A	Number=Sing	18	amod	_	SpaceAfter=No
20	.	.	PUNCT	FS	_	18	punct	_	_

# sent_id = 10
# text = Per questo motivo la maestra Pegula mi ha sempre scelto per le audizioni.
1	Per	per	ADP	E	_	3	case	_	_
2	questo	questo	DET	DD	Gender=Masc|Number=Sing|PronType=Dem	3	det	_	_
3	motivo	motivo	NOUN	S	Gender=Masc|Number=Sing	10	nmod	_	_
4	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	5	det	_	_
5	maestra	maestra	NOUN	S	Gender=Fem|Number=Sing	10	nsubj	_	_
6	Pegula	Pegula	PROPN	SP	_	5	nmod	_	_
7	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	10	dobj	_	_
8	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	10	aux	_	_
9	sempre	sempre	ADV	B	_	10	advmod	_	_
10	scelto	scegliere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
11	per	per	ADP	E	_	13	case	_	_
12	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	13	det	_	_
13	audizioni	audizione	NOUN	S	Gender=Fem|Number=Plur	10	nmod	_	SpaceAfter=No
14	.	.	PUNCT	FS	_	10	punct	_	_

# sent_id = 11
# text = Quest’anno sono stato lunico del mio gruppo (Martina, Giulia, Emma).
1	Quest’anno	Quest’anno	PROPN	SP	_	4	nsubj	_	_
2	sono	essere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	4	aux	_	_
3	stato	essere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	4	cop	_	_
4	lunico	lunico	ADJ	A	Gender=Masc|Number=Sing	0	root	_	_
5-6	del	_	_	_	_	_	_	_	_
5	di	di	ADP	E	_	8	case	_	_
6	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	8	det	_	_
7	mio	mio	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	8	det:poss	_	_
8	gruppo	gruppo	NOUN	S	Gender=Masc|Number=Sing	4	nmod	_	_
9	(	(	PUNCT	FB	_	10	punct	_	SpaceAfter=No
10	Martina	Martina	PROPN	SP	_	4	nmod	_	SpaceAfter=No
11	,	,	PUNCT	FF	_	10	punct	_	_
12	Giulia	Giulia	PROPN	SP	_	10	conj	_	SpaceAfter=No
13	,	,	PUNCT	FF	_	10	punct	_	_
14	Emma	Emma	PROPN	SP	_	10	conj	_	SpaceAfter=No
15	)	)	PUNCT	FB	_	10	punct	_	SpaceAfter=No
16	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 12
# text = Praticamente questi alievi hanno perso un anno di studio.
1	Praticamente	praticamente	ADV	B	_	5	advmod	_	_
2	questi	questo	PRON	PD	Gender=Masc|Number=Plur|PronType=Dem	5	nsubj	_	_
3	alievi	alievo	AUX	VM	Mood=Ind|Number=Sing|Person=2|Tense=Imp|VerbForm=Fin	5	aux	_	_
4	hanno	avere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	perso	perdere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
6	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	7	det	_	_
7	anno	anno	NOUN	S	Gender=Masc|Number=Sing	5	dobj	_	_
8	di	di	ADP	E	_	9	case	_	_
9	studio	studio	NOUN	S	Gender=Masc|Number=Sing	7	nmod	_	SpaceAfter=No
10	.	.	PUNCT	FS	_	5	punct	_	_

# sent_id = 13
# text = Io e una ragazza di circa 17-18 anni che si chiama Emy.
1	Io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	0	root	_	_
2	e	e	CONJ	CC	_	1	cc	_	_
3	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	4	det	_	_
4	ragazza	ragazza	NOUN	S	Gender=Fem|Number=Sing	1	conj	_	_
5	di	di	ADP	E	_	8	case	_	_
6	circa	circa	ADV	B	_	7	advmod	_	_
7	17-18	17-18	NUM	N	NumType=Card	8	nummod	_	_
8	anni	anno	NOUN	S	Gender=Masc|Number=Plur	4	nmod	_	_
9	che	che	PRON	PR	PronType=Rel	11	nsubj	_	_
10	si	si	PRON	PC	Person=3|PronType=Clit	11	expl	_	_
11	chiama	chiamare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	4	acl:relcl	_	_
12	Emy	Emy	PROPN	SP	_	11	xcomp	_	SpaceAfter=No
13	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 14
# text = È molto brava a suonare, è anche molto timida.
1	È	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	3	cop	_	_
2	molto	molto	ADV	B	_	3	advmod	_	_
3	brava	bravo	ADJ	A	Gender=Fem|Number=Sing	0	root	_	_
4	a	a	ADP	E	_	5	mark	_	_
5	suonare	suonare	VERB	V	VerbForm=Inf	3	acl	_	SpaceAfter=No
6	,	,	PUNCT	FF	_	3	punct	_	_
7	è	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	10	cop	_	_
8	anche	anche	ADV	B	_	10	advmod	_	SpacesAfter=\s\s
9	molto	molto	ADV	B	_	10	advmod	_	_
10	timida	timido	ADJ	A	Gender=Fem|Number=Sing	3	conj	_	SpaceAfter=No
11	.	.	PUNCT	FS	_	3	punct	_	SpacesAfter=\s\s

# sent_id = 15
# text = Questa storia che ho vissuto mi ha fatto imparare che si impegna riesce a ottenere qualcosa e chi non s’impegna non ottiene niente.
1	Questa	questo	DET	DD	Gender=Fem|Number=Sing|PronType=Dem	2	det	_	_
2	storia	storia	NOUN	S	Gender=Fem|Number=Sing	8	nsubj	_	_
3	che	che	PRON	PR	PronType=Rel	5	dobj	_	_
4	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	vissuto	vivere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	2	acl:relcl	_	_
6	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	8	iobj	_	_
7	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	8	aux	_	_
8	fatto	fare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
9	imparare	imparare	VERB	V	VerbForm=Inf	8	ccomp	_	SpacesAfter=\s\s
10	che	che	SCONJ	CS	_	12	mark	_	_
11	si	si	PRON	PC	Person=3|PronType=Clit	12	expl:impers	_	_
12	impegna	impegnare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	9	ccomp	_	_
13	riesce	riuscire	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	12	dobj	_	_
14	a	a	ADP	E	_	15	mark	_	_
15	ottenere	ottenere	VERB	V	VerbForm=Inf	13	xcomp	_	_
16	qualcosa	qualcosa	PRON	PI	Number=Sing|PronType=Ind	15	dobj	_	_
17	e	e	CONJ	CC	_	13	cc	_	_
18	chi	chi	PRON	PR	Number=Sing|PronType=Rel	13	conj	_	_
19	non	non	ADV	BN	PronType=Neg	20	neg	_	_
20	s’impegna	s’impegnare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	18	acl:relcl	_	_
21	non	non	ADV	BN	PronType=Neg	22	neg	_	_
22	ottiene	ottenere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	20	advcl	_	_
23	niente	niente	PRON	PI	Number=Sing|PronType=Ind	22	dobj	_	SpaceAfter=No
24	.	.	PUNCT	FS	_	8	punct	_	SpacesAfter=\r\n\r\n


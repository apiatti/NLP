# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE7.txt
# newpar
# sent_id = 1
# text = Se ti impegni nessun obbiettivo è impossibile Un giorno, con la mia squadra di Basket siamo andati a giocare contro due squadre fortissime.
1	Se	se	SCONJ	CS	_	3	mark	_	_
2	ti	ti	PRON	PC	Number=Sing|Person=2|PronType=Clit	3	expl	_	_
3	impegni	impegnare	VERB	V	Mood=Ind|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	7	advcl	_	_
4	nessun	nessuno	DET	DI	Gender=Masc|Number=Sing|PronType=Ind	5	det	_	_
5	obbiettivo	obbiettivo	NOUN	S	Gender=Masc|Number=Sing	7	nsubj	_	_
6	è	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	7	cop	_	_
7	impossibile	impossibile	ADJ	A	Number=Sing	0	root	_	SpacesAfter=\s\s
8	Un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	9	det	_	_
9	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	7	nmod	_	SpaceAfter=No
10	,	,	PUNCT	FF	_	7	punct	_	_
11	con	con	ADP	E	_	14	case	_	_
12	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	14	det	_	_
13	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	14	det:poss	_	_
14	squadra	squadra	NOUN	S	Gender=Fem|Number=Sing	18	nmod	_	_
15	di	di	ADP	E	_	16	case	_	_
16	Basket	basket	PROPN	SP	_	14	nmod	_	_
17	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	18	aux	_	_
18	andati	andare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	7	conj	_	_
19	a	a	ADP	E	_	20	mark	_	_
20	giocare	giocare	VERB	V	VerbForm=Inf	18	xcomp	_	_
21	contro	contro	ADP	E	_	23	case	_	_
22	due	due	NUM	N	NumType=Card	23	nummod	_	_
23	squadre	squadra	NOUN	S	Gender=Fem|Number=Plur	20	nmod	_	_
24	fortissime	forte	ADJ	A	Degree=Abs|Gender=Fem|Number=Plur	23	amod	_	SpaceAfter=No
25	.	.	PUNCT	FS	_	7	punct	_	_

# sent_id = 2
# text = Ma noi, a dir la veritá, ci eravamo un po’ scoraggiati perché eravamo sicuri di perdere.
1	Ma	ma	CONJ	CC	_	10	cc	_	_
2	noi	noi	PRON	PE	Number=Plur|Person=1|PronType=Prs	10	nsubj	_	SpaceAfter=No
3	,	,	PUNCT	FF	_	2	punct	_	_
4	a	a	ADP	E	_	5	mark	_	_
5	dir	dire	VERB	V	VerbForm=Inf	10	advcl	_	_
6	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	7	det	_	_
7	veritá	veritá	PROPN	SP	_	5	dobj	_	SpaceAfter=No
8	,	,	PUNCT	FF	_	5	punct	_	_
9	ci	ci	PRON	PC	PronType=Clit	10	expl	_	_
10	eravamo	essere	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	0	root	_	_
11	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	13	det	_	_
12	po’	ipo	DET	RD	Definite=Def|Number=Sing|PronType=Art	13	det	_	_
13	scoraggiati	scoraggiato	NOUN	S	Gender=Masc|Number=Plur	10	dobj	_	_
14	perché	perché	SCONJ	CS	_	15	mark	_	_
15	eravamo	essere	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	10	advcl	_	_
16	sicuri	sicuro	ADJ	A	Gender=Masc|Number=Plur	15	xcomp	_	_
17	di	di	ADP	E	_	18	mark	_	_
18	perdere	perdere	VERB	V	VerbForm=Inf	16	xcomp	_	SpaceAfter=No
19	.	.	PUNCT	FS	_	10	punct	_	_

# sent_id = 3
# text = Infatti ci hanno battuti peró da quel giorno ogni allenamento ci siamo impegnati tantissimo e anche gli allenamenti sono diventati piú duri.
1	Infatti	infatti	ADV	B	_	4	advmod	_	_
2	ci	ci	PRON	PC	PronType=Clit	4	expl	_	_
3	hanno	avere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	4	aux	_	_
4	battuti	battere	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	13	advcl	_	_
5	peró	peró	PROPN	SP	_	4	nsubj	_	_
6	da	da	ADP	E	_	8	case	_	_
7	quel	quello	DET	DD	Gender=Masc|Number=Sing|PronType=Dem	8	det	_	_
8	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	4	nmod	_	_
9	ogni	ogni	DET	DI	Number=Sing|PronType=Ind	10	det	_	_
10	allenamento	allenamento	NOUN	S	Gender=Masc|Number=Sing	13	nsubj	_	_
11	ci	ci	PRON	PC	PronType=Clit	13	advmod	_	_
12	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	13	aux	_	_
13	impegnati	impegnare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	0	root	_	_
14	tantissimo	tanto	ADJ	A	Degree=Abs|Gender=Masc|Number=Sing	13	xcomp	_	_
15	e	e	CONJ	CC	_	13	cc	_	_
16	anche	anche	ADV	B	_	18	advmod	_	_
17	gli	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	18	det	_	_
18	allenamenti	allenamento	NOUN	S	Gender=Masc|Number=Plur	20	nsubj	_	_
19	sono	essere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	20	aux	_	_
20	diventati	diventare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	13	conj	_	_
21	piú	piú	PROPN	SP	_	20	xcomp	_	_
22	duri	duro	ADJ	A	Gender=Masc|Number=Plur	21	amod	_	SpaceAfter=No
23	.	.	PUNCT	FS	_	13	punct	_	_

# sent_id = 4
# text = Partita dopo partita siamo diventati sempre piú forti, sempre piú convinti di noi.
1	Partita	partire	VERB	V	Mood=Imp|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	5	csubj	_	_
2	dopo	dopo	ADP	E	_	3	case	_	_
3	partita	partita	NOUN	S	Gender=Fem|Number=Sing	1	nmod	_	_
4	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	diventati	diventare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	0	root	_	_
6	sempre	sempre	ADV	B	_	7	advmod	_	_
7	piú	piú	PROPN	SP	_	5	xcomp	_	_
8	forti	forte	ADJ	A	Number=Plur	7	amod	_	SpaceAfter=No
9	,	,	PUNCT	FF	_	5	punct	_	_
10	sempre	sempre	ADV	B	_	11	advmod	_	_
11	piú	piú	PROPN	SP	_	5	nmod	_	_
12	convinti	convinto	ADJ	A	Gender=Masc|Number=Plur	11	amod	_	_
13	di	di	ADP	E	_	14	case	_	_
14	noi	noi	PRON	PE	Number=Plur|Person=1|PronType=Prs	11	nmod	_	SpaceAfter=No
15	.	.	PUNCT	FS	_	5	punct	_	_

# sent_id = 5
# text = Poi se perdevamo va be’ vuol dire che dovevamo impegnarci ancora di piú e noi ci impegnavamo di piú.
1	Poi	poi	ADV	B	_	4	advmod	_	_
2	se	se	SCONJ	CS	_	4	mark	_	_
3	perdevamo	perdere	AUX	VM	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	4	aux	_	_
4	va	andare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
5	be’	ibe	PRON	PC	PronType=Clit	7	advmod	_	_
6	vuol	volere	AUX	VM	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	7	aux	_	_
7	dire	dire	VERB	V	VerbForm=Inf	4	ccomp	_	_
8	che	che	SCONJ	CS	_	10	mark	_	_
9	dovevamo	dovere	AUX	VM	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	10	aux	_	_
10	impegnarci	impegnare	VERB	V	VerbForm=Inf	7	ccomp	_	_
11	ancora	ancora	ADV	B	_	10	advmod	_	_
12	di	di	ADP	E	_	13	case	_	_
13	piú	piú	PROPN	SP	_	10	nmod	_	_
14	e	e	CONJ	CC	_	7	cc	_	_
15	noi	noi	PRON	PE	Number=Plur|Person=1|PronType=Prs	17	nsubj	_	_
16	ci	ci	PRON	PC	Number=Plur|Person=1|PronType=Clit	17	expl	_	_
17	impegnavamo	impegnare	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	7	conj	_	_
18	di	di	ADP	E	_	19	case	_	_
19	piú	piú	PROPN	SP	_	17	nmod	_	SpaceAfter=No
20	.	.	PUNCT	FS	_	7	punct	_	_

# sent_id = 6
# text = Anche gli allenatori facevano nuovi allenamenti sempre piú duri e dava i suoi frutti:
1	Anche	anche	ADV	B	_	3	advmod	_	_
2	gli	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	3	det	_	_
3	allenatori	allenatore	NOUN	S	Gender=Masc|Number=Plur	4	nsubj	_	_
4	facevano	fare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
5	nuovi	nuovo	ADJ	A	Gender=Masc|Number=Plur	6	amod	_	_
6	allenamenti	allenamente	NOUN	S	Gender=Masc|Number=Plur	4	dobj	_	_
7	sempre	sempre	ADV	B	_	8	advmod	_	_
8	piú	piú	PROPN	SP	_	6	nmod	_	_
9	duri	duro	ADJ	A	Gender=Masc|Number=Plur	8	amod	_	_
10	e	e	CONJ	CC	_	4	cc	_	_
11	dava	dare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	4	conj	_	_
12	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	14	det	_	_
13	suoi	suo	DET	AP	Gender=Masc|Number=Plur|Poss=Yes|PronType=Prs	14	det:poss	_	_
14	frutti	frutto	NOUN	S	Gender=Masc|Number=Plur	11	dobj	_	SpaceAfter=No
15	:	:	PUNCT	FC	_	4	punct	_	_

# sent_id = 7
# text = vincendo coppe, medaglie, titoli.
1	vincendo	vincere	VERB	V	VerbForm=Ger	0	root	_	_
2	coppe	coppa	NOUN	S	Gender=Fem|Number=Plur	1	dobj	_	SpaceAfter=No
3	,	,	PUNCT	FF	_	2	punct	_	_
4	medaglie	medaglia	NOUN	S	Gender=Fem|Number=Plur	2	conj	_	SpaceAfter=No
5	,	,	PUNCT	FF	_	2	punct	_	_
6	titoli	titolo	NOUN	S	Gender=Masc|Number=Plur	2	conj	_	SpaceAfter=No
7	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 8
# text = Poi dopo quattro anni siamo riusciti a battere quelle due squadre fortissime e ci siamo qualificati per un torneo internazionale a Barcellona:
1	Poi	poi	ADV	B	_	6	advmod	_	_
2	dopo	dopo	ADP	E	_	4	case	_	_
3	quattro	quattro	NUM	N	NumType=Card	4	nummod	_	_
4	anni	anno	NOUN	S	Gender=Masc|Number=Plur	6	nmod	_	_
5	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	6	aux	_	SpacesAfter=\s\s
6	riusciti	riuscire	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	0	root	_	_
7	a	a	ADP	E	_	8	mark	_	_
8	battere	battere	VERB	V	VerbForm=Inf	6	xcomp	_	_
9	quelle	quello	PRON	PD	Gender=Fem|Number=Plur|PronType=Dem	8	dobj	_	_
10	due	due	NUM	N	NumType=Card	11	nummod	_	_
11	squadre	squadra	NOUN	S	Gender=Fem|Number=Plur	9	nmod	_	_
12	fortissime	forte	ADJ	A	Degree=Abs|Gender=Fem|Number=Plur	11	amod	_	_
13	e	e	CONJ	CC	_	6	cc	_	_
14	ci	ci	PRON	PC	PronType=Clit	16	advmod	_	_
15	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	16	auxpass	_	_
16	qualificati	qualificare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	6	conj	_	_
17	per	per	ADP	E	_	19	case	_	_
18	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	19	det	_	_
19	torneo	torneo	NOUN	S	Gender=Masc|Number=Sing	16	nmod	_	_
20	internazionale	internazionale	ADJ	A	Number=Sing	19	amod	_	_
21	a	a	ADP	E	_	22	case	_	_
22	Barcellona	Barcellona	PROPN	SP	_	19	nmod	_	SpaceAfter=No
23	:	:	PUNCT	FC	_	6	punct	_	_

# sent_id = 9
# text = In Ticino stiamo combattendo per i primi cinque posti.
1	In	in	ADP	E	_	2	case	_	_
2	Ticino	Ticino	PROPN	SP	_	4	nmod	_	_
3	stiamo	stare	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	4	aux	_	_
4	combattendo	combattere	VERB	V	VerbForm=Ger	0	root	_	_
5	per	per	ADP	E	_	9	case	_	_
6	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	9	det	_	_
7	primi	primo	ADJ	NO	Gender=Masc|Number=Plur|NumType=Ord	9	amod	_	_
8	cinque	cinque	NUM	N	NumType=Card	9	nummod	_	_
9	posti	posto	NOUN	S	Gender=Masc|Number=Plur	4	nmod	_	SpaceAfter=No
10	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 10
# text = Riflessione:
1	Riflessione	riflessione	NOUN	S	Gender=Fem|Number=Sing	0	root	_	SpaceAfter=No
2	:	:	PUNCT	FC	_	1	punct	_	_

# sent_id = 11
# text = che se ti impegni nessun obbiettivo è impossibile.
1	che	che	SCONJ	CS	_	8	mark	_	_
2	se	se	SCONJ	CS	_	4	mark	_	_
3	ti	ti	PRON	PC	Number=Sing|Person=2|PronType=Clit	4	expl	_	_
4	impegni	impegnare	VERB	V	Mood=Ind|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	8	advcl	_	_
5	nessun	nessuno	DET	DI	Gender=Masc|Number=Sing|PronType=Ind	6	det	_	_
6	obbiettivo	obbiettivo	NOUN	S	Gender=Masc|Number=Sing	4	dobj	_	_
7	è	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	8	cop	_	_
8	impossibile	impossibile	ADJ	A	Number=Sing	0	root	_	SpaceAfter=No
9	.	.	PUNCT	FS	_	8	punct	_	SpacesAfter=\r\n\r\n


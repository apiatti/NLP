# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE84.txt
# newpar
# sent_id = 1
# text = Un giorno ero a scuola e c’era un maestro che tutti dicevano che era molto cattivo.
1	Un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	0	root	_	_
3	ero	essere	ADJ	A	Gender=Masc|Number=Sing	2	amod	_	_
4	a	a	ADP	E	_	5	case	_	_
5	scuola	scuola	NOUN	S	Gender=Fem|Number=Sing	2	nmod	_	_
6	e	e	CONJ	CC	_	2	cc	_	_
7	c’era	c’erare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	2	conj	_	_
8	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	9	det	_	_
9	maestro	maestro	NOUN	S	Gender=Masc|Number=Sing	7	dobj	_	_
10	che	che	PRON	PR	PronType=Rel	12	dobj	_	_
11	tutti	tutto	PRON	PI	Gender=Masc|Number=Plur|PronType=Ind	12	nsubj	_	_
12	dicevano	dicevare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	9	acl:relcl	_	_
13	che	che	SCONJ	CS	_	16	mark	_	_
14	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	16	cop	_	SpacesAfter=\s\s\s
15	molto	molto	ADV	B	_	16	advmod	_	_
16	cattivo	cattivo	ADJ	A	Gender=Masc|Number=Sing	12	ccomp	_	SpaceAfter=No
17	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 2
# text = Tutti non lo volevano, quando arrivava nelle classi nessuno lo voleva, chidevano tutti di andare in bagno per non stare con lui, certi avevano perfino paura di lui.
1	Tutti	tutto	PRON	PI	Gender=Masc|Number=Plur|PronType=Ind	4	nsubj	_	_
2	non	non	ADV	BN	PronType=Neg	4	neg	_	_
3	lo	lo	PRON	PC	Gender=Masc|Number=Sing|Person=3|PronType=Clit	4	dobj	_	_
4	volevano	volere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	SpaceAfter=No
5	,	,	PUNCT	FF	_	4	punct	_	_
6	quando	quando	SCONJ	CS	_	7	mark	_	_
7	arrivava	arrivare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	13	advcl	_	_
8-9	nelle	_	_	_	_	_	_	_	_
8	in	in	ADP	E	_	10	case	_	_
9	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	10	det	_	_
10	classi	classe	NOUN	S	Gender=Fem|Number=Plur	7	nmod	_	_
11	nessuno	nessuno	PRON	PI	Gender=Masc|Number=Sing|PronType=Ind	13	nsubj	_	_
12	lo	lo	PRON	PC	Gender=Masc|Number=Sing|Person=3|PronType=Clit	13	dobj	_	_
13	voleva	volere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	4	conj	_	SpaceAfter=No
14	,	,	PUNCT	FF	_	4	punct	_	_
15	chidevano	chidere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	4	conj	_	SpacesAfter=\s\s
16	tutti	tutto	PRON	PI	Gender=Masc|Number=Plur|PronType=Ind	15	dobj	_	_
17	di	di	ADP	E	_	18	mark	_	_
18	andare	andare	VERB	V	VerbForm=Inf	15	xcomp	_	_
19	in	in	ADP	E	_	20	case	_	_
20	bagno	bagno	NOUN	S	Gender=Masc|Number=Sing	18	nmod	_	_
21	per	per	ADP	E	_	23	mark	_	_
22	non	non	ADV	BN	PronType=Neg	23	neg	_	_
23	stare	stare	VERB	V	VerbForm=Inf	18	advcl	_	_
24	con	con	ADP	E	_	25	case	_	_
25	lui	lui	PRON	PE	Gender=Masc|Number=Sing|Person=3|PronType=Prs	23	nmod	_	SpaceAfter=No
26	,	,	PUNCT	FF	_	4	punct	_	_
27	certi	certo	ADJ	A	Gender=Masc|Number=Plur	28	nsubj	_	_
28	avevano	avere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	4	conj	_	_
29	perfino	perfino	ADV	B	_	28	advmod	_	_
30	paura	paura	NOUN	S	Gender=Fem|Number=Sing	28	dobj	_	_
31	di	di	ADP	E	_	32	case	_	_
32	lui	lui	PRON	PE	Gender=Masc|Number=Sing|Person=3|PronType=Prs	30	nmod	_	SpaceAfter=No
33	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 3
# text = Un giorno andò in una classe dove c’era un bambino, che ero io, e faceva lezione con me con la mia classe.
1	Un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	3	nmod	_	_
3	andò	andare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
4	in	in	ADP	E	_	6	case	_	_
5	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	6	det	_	_
6	classe	classe	NOUN	S	Gender=Fem|Number=Sing	3	nmod	_	_
7	dove	dove	ADV	B	_	8	advmod	_	_
8	c’era	c’erare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	6	acl:relcl	_	_
9	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	10	det	_	_
10	bambino	bambino	NOUN	S	Gender=Masc|Number=Sing	8	dobj	_	SpaceAfter=No
11	,	,	PUNCT	FF	_	8	punct	_	_
12	che	che	SCONJ	CS	_	13	mark	_	_
13	ero	essere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	8	advcl	_	_
14	io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	13	nsubj	_	SpaceAfter=No
15	,	,	PUNCT	FF	_	13	punct	_	_
16	e	e	CONJ	CC	_	13	cc	_	_
17	faceva	fare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	13	conj	_	_
18	lezione	lezione	NOUN	S	Gender=Fem|Number=Sing	17	dobj	_	_
19	con	con	ADP	E	_	20	case	_	_
20	me	me	PRON	PE	Number=Sing|Person=1|PronType=Prs	17	nmod	_	_
21	con	con	ADP	E	_	24	case	_	_
22	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	24	det	_	_
23	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	24	det:poss	_	_
24	classe	classe	NOUN	S	Gender=Fem|Number=Sing	17	nmod	_	SpaceAfter=No
25	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 4
# text = Quando faceva lezione non sembrava molto cattivo, anzi, il contrario.
1	Quando	quando	SCONJ	CS	_	2	mark	_	_
2	faceva	fare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	5	advcl	_	_
3	lezione	lezione	NOUN	S	Gender=Fem|Number=Sing	2	dobj	_	_
4	non	non	ADV	BN	PronType=Neg	5	neg	_	_
5	sembrava	sembrare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
6	molto	molto	ADV	B	_	7	advmod	_	_
7	cattivo	cattivo	ADJ	A	Gender=Masc|Number=Sing	5	xcomp	_	SpaceAfter=No
8	,	,	PUNCT	FF	_	5	punct	_	_
9	anzi	anzi	ADV	B	_	5	advmod	_	SpaceAfter=No
10	,	,	PUNCT	FF	_	5	punct	_	_
11	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	12	det	_	_
12	contrario	contrario	NOUN	S	Gender=Masc|Number=Sing	5	dobj	_	SpaceAfter=No
13	.	.	PUNCT	FS	_	5	punct	_	_

# sent_id = 5
# text = Quando era andato in un’altra classe ne ho parlato con le mie amiche e io dicevo che non era cattivo come dicevano gli altri.
1	Quando	quando	SCONJ	CS	_	3	mark	_	_
2	era	essere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	3	aux	_	_
3	andato	andare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	9	advcl	_	_
4	in	in	ADP	E	_	6	case	_	_
5	un’altra	un’altro	DET	DI	Gender=Fem|Number=Sing|PronType=Ind	6	det	_	_
6	classe	classe	NOUN	S	Gender=Fem|Number=Sing	3	nmod	_	_
7	ne	ne	PRON	PC	PronType=Clit	9	advmod	_	_
8	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	9	aux	_	_
9	parlato	parlare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
10	con	con	ADP	E	_	12	case	_	_
11	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	12	det	_	_
12	mie	mio	NOUN	S	Gender=Fem|Number=Plur	9	nmod	_	_
13	amiche	amica	ADJ	A	Gender=Fem|Number=Plur	12	amod	_	_
14	e	e	CONJ	CC	_	9	cc	_	_
15	io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	16	nsubj	_	_
16	dicevo	dire	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	9	conj	_	_
17	che	che	SCONJ	CS	_	20	mark	_	_
18	non	non	ADV	BN	PronType=Neg	20	neg	_	_
19	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	20	cop	_	_
20	cattivo	cattivo	ADJ	A	Gender=Masc|Number=Sing	16	ccomp	_	_
21	come	come	SCONJ	CS	_	22	mark	_	_
22	dicevano	dicevare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	20	advcl	_	_
23	gli	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	24	det	_	_
24	altri	altro	PRON	PI	Gender=Masc|Number=Plur|PronType=Ind	22	nsubj	_	SpaceAfter=No
25	.	.	PUNCT	FS	_	9	punct	_	SpacesAfter=\s\s

# sent_id = 6
# text = È meglio vedere e non ascoltare quello che dicono gli altri.
1	È	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	3	cop	_	_
2	meglio	meglio	ADV	B	_	3	advmod	_	_
3	vedere	vedere	VERB	V	VerbForm=Inf	0	root	_	_
4	e	e	CONJ	CC	_	3	cc	_	_
5	non	non	ADV	BN	PronType=Neg	6	neg	_	_
6	ascoltare	ascoltare	VERB	V	VerbForm=Inf	3	conj	_	_
7	quello	quello	PRON	PD	Gender=Masc|Number=Sing|PronType=Dem	6	dobj	_	_
8	che	che	PRON	PR	PronType=Rel	9	dobj	_	_
9	dicono	dire	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	7	acl:relcl	_	_
10	gli	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	11	det	_	_
11	altri	altro	PRON	PI	Gender=Masc|Number=Plur|PronType=Ind	9	nsubj	_	SpaceAfter=No
12	.	.	PUNCT	FS	_	3	punct	_	SpacesAfter=\r\n\r\n


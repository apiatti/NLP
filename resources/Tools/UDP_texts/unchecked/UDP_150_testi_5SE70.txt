# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE70.txt
# newpar
# sent_id = 1
# text = L’EBE E LA GARA Un paio di settimane fá, il mio maestro di nuoto (si chiama Dimitri) ci ha dato un foglio di iscrizione per una gara, una mia compagna l’Ebe, capelli lunghi biondi, magra, si vanta sempre… Continua a dire:
1	L’EBE	L’EBE	PROPN	SP	_	44	nsubj	_	_
2	E	e	PROPN	SP	_	1	name	_	_
3	LA	La	PROPN	SP	_	1	name	_	_
4	GARA	gara	PROPN	SP	_	1	name	_	SpacesAfter=\s\s
5	Un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	6	det	_	_
6	paio	paio	NOUN	S	Gender=Masc|Number=Sing	23	nmod	_	_
7	di	di	ADP	E	_	8	case	_	_
8	settimane	settimana	NOUN	S	Gender=Fem|Number=Plur	6	nmod	_	SpacesAfter=\s\s\s
9	fá	fá	PROPN	SP	_	8	nmod	_	SpaceAfter=No
10	,	,	PUNCT	FF	_	6	punct	_	_
11	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	13	det	_	_
12	mio	mio	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	13	det:poss	_	_
13	maestro	maestro	NOUN	S	Gender=Masc|Number=Sing	23	nsubj	_	_
14	di	di	ADP	E	_	15	case	_	_
15	nuoto	nuoto	NOUN	S	Gender=Fem	13	nmod	_	_
16	(	(	PUNCT	FB	_	18	punct	_	SpaceAfter=No
17	si	si	PRON	PC	Person=3|PronType=Clit	18	expl	_	_
18	chiama	chiamare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	13	advcl	_	_
19	Dimitri	Dimitri	PROPN	SP	_	18	xcomp	_	SpaceAfter=No
20	)	)	PUNCT	FB	_	18	punct	_	_
21	ci	ci	PRON	PC	Number=Plur|Person=1|PronType=Clit	23	iobj	_	_
22	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	23	aux	_	_
23	dato	dare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	44	advcl	_	_
24	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	25	det	_	_
25	foglio	foglio	NOUN	S	Gender=Masc|Number=Sing	23	dobj	_	_
26	di	di	ADP	E	_	27	case	_	_
27	iscrizione	iscrizione	NOUN	S	Gender=Fem|Number=Sing	25	nmod	_	_
28	per	per	ADP	E	_	30	case	_	_
29	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	30	det	_	_
30	gara	gara	NOUN	S	Gender=Fem|Number=Sing	25	nmod	_	SpaceAfter=No
31	,	,	PUNCT	FF	_	30	punct	_	_
32	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	34	det	_	_
33	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	34	det:poss	_	_
34	compagna	compagna	NOUN	S	Gender=Fem|Number=Sing	30	conj	_	SpacesAfter=\s\s\s
35	l’Ebe	l’Ebe	PROPN	SP	_	34	nmod	_	SpaceAfter=No
36	,	,	PUNCT	FF	_	35	punct	_	_
37	capelli	capello	NOUN	S	Gender=Masc|Number=Plur	35	appos	_	_
38	lunghi	lungo	ADJ	A	Gender=Masc|Number=Plur	37	amod	_	_
39	biondi	biondo	ADJ	A	Gender=Masc|Number=Plur	37	amod	_	SpaceAfter=No
40	,	,	PUNCT	FF	_	37	punct	_	_
41	magra	magro	ADJ	A	Gender=Fem|Number=Sing	37	amod	_	SpaceAfter=No
42	,	,	PUNCT	FF	_	23	punct	_	_
43	si	si	PRON	PC	Person=3|PronType=Clit	44	expl	_	_
44	vanta	vantare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
45	sempre…	sempre…	PUNCT	FF	_	44	punct	_	SpacesAfter=\s\s
46	Continua	continuare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	44	parataxis	_	_
47	a	a	ADP	E	_	48	mark	_	_
48	dire	dire	VERB	V	VerbForm=Inf	46	xcomp	_	SpaceAfter=No
49	:	:	PUNCT	FC	_	44	punct	_	_

# sent_id = 2
# text = – Scommetto 5 fr. – che vinco!
1	–	–	PUNCT	FF	_	4	punct	_	_
2	Scommetto	Scommeggere	ADP	E	_	4	case	_	_
3	5	5	NUM	N	NumType=Card	4	nummod	_	_
4	fr.	fr.	NOUN	S	_	0	root	_	_
5	–	–	PUNCT	FB	_	4	punct	_	_
6	che	che	PRON	PR	PronType=Rel	7	dobj	_	_
7	vinco	vinco	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	4	acl:relcl	_	SpaceAfter=No
8	!	!	PUNCT	FS	_	4	punct	_	_

# sent_id = 3
# text = O anche:
1	O	o	CONJ	CC	_	2	cc	_	_
2	anche	anche	ADV	B	_	0	root	_	SpaceAfter=No
3	:	:	PUNCT	FC	_	2	punct	_	_

# sent_id = 4
# text = – Nessuno riuscirebbe mai a superarmi!
1	–	–	PUNCT	FF	_	3	punct	_	_
2	Nessuno	nessuno	PRON	PI	Gender=Masc|Number=Sing|PronType=Ind	3	nsubj	_	_
3	riuscirebbe	riuscire	VERB	V	Mood=Cnd|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
4	mai	mai	ADV	B	_	3	advmod	_	_
5	a	a	ADP	E	_	6	case	_	_
6	superarmi	arma|superarme	NOUN	S	Gender=Fem|Number=Plur	3	nmod	_	SpaceAfter=No
7	!	!	PUNCT	FS	_	3	punct	_	_

# sent_id = 5
# text = Poi il giorno della gara quando era il suo turno all’inizio andó come un fulmine, poi quando gli mancava solo una vasca, rallentò e gli altri miei compagni la superarono.
1	Poi	poi	ADV	B	_	3	advmod	_	_
2	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	3	det	_	_
3	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	21	nmod	_	_
4-5	della	_	_	_	_	_	_	_	_
4	di	di	ADP	E	_	6	case	_	_
5	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	6	det	_	_
6	gara	gara	NOUN	S	Gender=Fem|Number=Sing	3	nmod	_	_
7	quando	quando	SCONJ	CS	_	12	mark	_	_
8	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	12	cop	_	_
9	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	12	det	_	_
10	suo	suo	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	12	det:poss	_	_
11	turno	turno	ADJ	A	Gender=Masc|Number=Sing	12	amod	_	_
12	all’inizio	all’inizio	NOUN	S	Gender=Masc|Number=Sing	3	advcl	_	_
13	andó	andó	PROPN	SP	_	12	nmod	_	_
14	come	come	ADP	E	_	16	case	_	_
15	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	16	det	_	_
16	fulmine	fulmine	NOUN	S	Gender=Masc|Number=Sing	12	nmod	_	SpaceAfter=No
17	,	,	PUNCT	FF	_	3	punct	_	_
18	poi	poi	ADV	B	_	21	advmod	_	_
19	quando	quando	SCONJ	CS	_	21	mark	_	SpacesAfter=\s\s\s
20	gli	il	PRON	PC	Gender=Masc|Number=Sing|Person=3|PronType=Clit	21	iobj	_	_
21	mancava	mancare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
22	solo	solo	ADV	B	_	21	advmod	_	_
23	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	24	det	_	_
24	vasca	vasca	NOUN	S	Gender=Fem|Number=Sing	21	dobj	_	SpaceAfter=No
25	,	,	PUNCT	FF	_	21	punct	_	_
26	rallentò	rallentare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	21	conj	_	_
27	e	e	CONJ	CC	_	21	cc	_	_
28	gli	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	29	det	_	_
29	altri	altro	PRON	PI	Gender=Masc|Number=Plur|PronType=Ind	33	dobj	_	_
30	miei	mio	DET	AP	Gender=Masc|Number=Plur|Poss=Yes|PronType=Prs	31	det	_	_
31	compagni	compagno	NOUN	S	Gender=Masc|Number=Plur	33	nsubj	_	_
32	la	il	PRON	PC	Gender=Fem|Number=Sing|Person=3|PronType=Clit	33	dobj	_	_
33	superarono	superarono	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Past|VerbForm=Fin	21	conj	_	SpaceAfter=No
34	.	.	PUNCT	FS	_	21	punct	_	_

# sent_id = 6
# text = Perse la gara.
1	Perse	perdere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
2	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	3	det	_	_
3	gara	gara	NOUN	S	Gender=Fem|Number=Sing	1	dobj	_	SpaceAfter=No
4	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 7
# text = Da questo episodio ho imparato che non si deve solo dire, ma ci si deve impegnare per realizzarlo.
1	Da	da	ADP	E	_	3	case	_	_
2	questo	questo	DET	DD	Gender=Masc|Number=Sing|PronType=Dem	3	det	_	_
3	episodio	episodio	NOUN	S	Gender=Masc|Number=Sing	5	nmod	_	_
4	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	imparato	imparare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
6	che	che	SCONJ	CS	_	11	mark	_	_
7	non	non	ADV	BN	PronType=Neg	11	neg	_	_
8	si	si	PRON	PC	Person=3|PronType=Clit	11	expl:impers	_	_
9	deve	dovere	AUX	VM	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	11	aux	_	_
10	solo	solo	ADV	B	_	11	advmod	_	_
11	dire	dire	VERB	V	VerbForm=Inf	5	ccomp	_	SpaceAfter=No
12	,	,	PUNCT	FF	_	11	punct	_	_
13	ma	ma	CONJ	CC	_	11	cc	_	_
14	ci	ci	PRON	PC	PronType=Clit	17	expl	_	_
15	si	si	PRON	PC	Person=3|PronType=Clit	17	expl:impers	_	_
16	deve	dovere	AUX	VM	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	17	aux	_	_
17	impegnare	impegnare	VERB	V	VerbForm=Inf	11	conj	_	_
18	per	per	ADP	E	_	19	mark	_	_
19-20	realizzarlo	_	_	_	_	_	_	_	SpaceAfter=No
19	realizzar	realizzare	VERB	V	VerbForm=Inf	17	advcl	_	_
20	lo	lo	PRON	PC	Gender=Masc|Number=Sing|Person=3|PronType=Clit	19	dobj	_	_
21	.	.	PUNCT	FS	_	5	punct	_	SpacesAfter=\r\n\r\n


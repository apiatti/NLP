# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE103.txt
# newpar
# sent_id = 1
# text = Test francese Un bel martedí mattina la nostra gentilissima maestra Claudia ci ha consegnato un compito di francese.
1	Test	test	NOUN	S	Gender=Masc|Number=Sing	14	nmod	_	_
2	francese	francese	ADJ	A	Number=Sing	1	amod	_	SpacesAfter=\s\s
3	Un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	5	det	_	_
4	bel	bello	ADJ	A	Gender=Masc|Number=Sing	5	amod	_	_
5	martedí	martedí	PROPN	SP	_	1	nmod	_	_
6	mattina	mattina	NOUN	S	Gender=Fem|Number=Sing	5	nmod	_	_
7	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	10	det	_	_
8	nostra	nostro	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	10	det:poss	_	_
9	gentilissima	gentile	ADJ	A	Degree=Abs|Gender=Fem|Number=Sing	10	amod	_	_
10	maestra	maestra	NOUN	S	Gender=Fem|Number=Sing	14	nsubj	_	_
11	Claudia	Claudia	PROPN	SP	_	10	nmod	_	_
12	ci	ci	PRON	PC	PronType=Clit	14	advmod	_	_
13	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	14	aux	_	_
14	consegnato	consegnare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
15	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	16	det	_	_
16	compito	compito	NOUN	S	Gender=Masc|Number=Sing	14	dobj	_	_
17	di	di	ADP	E	_	18	case	_	_
18	francese	francese	NOUN	S	Gender=Masc|Number=Sing	16	nmod	_	SpaceAfter=No
19	.	.	PUNCT	FS	_	14	punct	_	_

# sent_id = 2
# text = Questo compito consisteva nel studiare dei vocaboli in francese per la settimana seguente perché c’era un test.
1	Questo	questo	DET	DD	Gender=Masc|Number=Sing|PronType=Dem	2	det	_	_
2	compito	compito	NOUN	S	Gender=Masc|Number=Sing	3	nsubj	_	_
3	consisteva	consistere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
4-5	nel	_	_	_	_	_	_	_	_
4	in	in	ADP	E	_	6	case	_	_
5	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	6	det	_	_
6	studiare	studiare	NOUN	S	Gender=Masc|Number=Sing	3	nmod	_	_
7-8	dei	_	_	_	_	_	_	_	_
7	di	di	ADP	E	_	9	case	_	_
8	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	9	det	_	_
9	vocaboli	vocabolo	NOUN	S	Gender=Masc|Number=Plur	6	nmod	_	_
10	in	in	ADP	E	_	11	case	_	_
11	francese	francese	NOUN	S	Gender=Masc|Number=Sing	9	nmod	_	_
12	per	per	ADP	E	_	14	case	_	_
13	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	14	det	_	_
14	settimana	settimana	NOUN	S	Gender=Fem|Number=Sing	11	nmod	_	_
15	seguente	seguente	ADJ	A	Number=Sing	14	amod	_	_
16	perché	perché	SCONJ	CS	_	17	mark	_	_
17	c’era	c’erare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	3	advcl	_	_
18	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	19	det	_	_
19	test	test	NOUN	S	Gender=Masc	17	dobj	_	SpaceAfter=No
20	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 3
# text = In seguito lo portai a casa per studiare le parole.
1	In	in	ADP	E	_	2	case	_	_
2	seguito	seguito	NOUN	S	Gender=Masc|Number=Sing	4	nmod	_	_
3	lo	lo	PRON	PC	Gender=Masc|Number=Sing|Person=3|PronType=Clit	4	dobj	_	_
4	portai	portare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Past|VerbForm=Fin	0	root	_	_
5	a	a	ADP	E	_	6	case	_	_
6	casa	casa	NOUN	S	Gender=Fem|Number=Sing	4	nmod	_	_
7	per	per	ADP	E	_	8	mark	_	_
8	studiare	studiare	VERB	V	VerbForm=Inf	4	advcl	_	_
9	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	10	det	_	_
10	parole	parola	NOUN	S	Gender=Fem|Number=Plur	8	dobj	_	SpaceAfter=No
11	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 4
# text = Io non avevo mai voglia di studiare.
1	Io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	5	nsubj	_	_
2	non	non	ADV	BN	PronType=Neg	5	neg	_	_
3	avevo	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	5	aux	_	_
4	mai	mai	ADV	B	_	5	advmod	_	_
5	voglia	volere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
6	di	di	ADP	E	_	7	mark	_	_
7	studiare	studiare	VERB	V	VerbForm=Inf	5	xcomp	_	SpaceAfter=No
8	.	.	PUNCT	FS	_	5	punct	_	_

# sent_id = 5
# text = Peró la mia mamma insistette mille volte così fui costretta a studiare ogni benedetta sera prima o dopo cena.
1	Peró	Peró	PROPN	SP	_	5	nmod	_	_
2	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	4	det	_	_
3	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	4	det:poss	_	_
4	mamma	mamma	NOUN	S	Gender=Fem|Number=Sing	5	nsubj	_	_
5	insistette	insistetto	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
6	mille	mille	NUM	N	NumType=Card	7	nummod	_	_
7	volte	volta	NOUN	S	Gender=Fem|Number=Plur	5	nmod	_	_
8	così	così	ADV	B	_	7	advmod	_	_
9	fui	essere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Past|VerbForm=Fin	10	auxpass	_	_
10	costretta	costringere	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	7	acl	_	_
11	a	a	ADP	E	_	12	mark	_	_
12	studiare	studiare	VERB	V	VerbForm=Inf	10	xcomp	_	_
13	ogni	ogni	DET	DI	Number=Sing|PronType=Ind	15	det	_	_
14	benedetta	benedetta	ADJ	A	Gender=Fem|Number=Sing	15	amod	_	_
15	sera	sera	NOUN	S	Gender=Fem|Number=Sing	12	nmod	_	_
16	prima	primo	ADJ	NO	Gender=Fem|Number=Sing|NumType=Ord	15	amod	_	_
17	o	o	CONJ	CC	_	15	cc	_	_
18	dopo	dopo	ADP	E	_	19	case	_	_
19	cena	cena	NOUN	S	Gender=Fem|Number=Sing	15	conj	_	SpaceAfter=No
20	.	.	PUNCT	FS	_	5	punct	_	_

# sent_id = 6
# text = Purtroppo studiavo malvolentieri.
1	Purtroppo	purtroppo	ADV	B	_	2	advmod	_	_
2	studiavo	studiare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	0	root	_	_
3	malvolentieri	malvolentiere	ADV	B	_	2	advmod	_	SpaceAfter=No
4	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 7
# text = Una settimana dopo ci fu il test in classe e a dire la verità ero un po’ nervosa (forse anche troppo)!
1	Una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	2	det	_	_
2	settimana	settimana	NOUN	S	Gender=Fem|Number=Sing	5	nmod	_	_
3	dopo	dopo	ADV	B	_	2	advmod	_	_
4	ci	ci	PRON	PC	PronType=Clit	5	expl	_	_
5	fu	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
6	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	7	det	_	_
7	test	test	NOUN	S	Gender=Masc	5	nsubj	_	_
8	in	in	ADP	E	_	9	case	_	_
9	classe	classe	NOUN	S	Gender=Fem|Number=Sing	7	nmod	_	_
10	e	e	CONJ	CC	_	5	cc	_	_
11	a	a	ADP	E	_	12	mark	_	_
12	dire	dire	VERB	V	VerbForm=Inf	15	advcl	_	_
13	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	14	det	_	_
14	verità	verità	NOUN	S	Gender=Fem	12	dobj	_	_
15	ero	essere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	5	conj	_	_
16	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	18	det	_	_
17	po’	ipo	DET	RD	Definite=Def|Number=Sing|PronType=Art	18	det	_	_
18	nervosa	nervoso	NOUN	S	Gender=Fem|Number=Sing	15	nsubj	_	_
19	(	(	PUNCT	FB	_	22	punct	_	SpaceAfter=No
20	forse	forse	ADV	B	_	22	advmod	_	_
21	anche	anche	ADV	B	_	22	advmod	_	_
22	troppo	troppo	ADV	B	_	18	advmod	_	SpaceAfter=No
23	)	)	PUNCT	FB	_	22	punct	_	SpaceAfter=No
24	!	!	PUNCT	FS	_	15	punct	_	_

# sent_id = 8
# text = Terminato il test lo consegnai a Claudia.
1	Terminato	terminare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
2	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	3	det	_	_
3	test	test	NOUN	S	Gender=Masc	1	dobj	_	_
4	lo	lo	PRON	PC	Gender=Masc|Number=Sing|Person=3|PronType=Clit	5	dobj	_	_
5	consegnai	consegnare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Past|VerbForm=Fin	3	acl	_	_
6	a	a	ADP	E	_	7	case	_	_
7	Claudia	Claudia	PROPN	SP	_	5	nmod	_	SpaceAfter=No
8	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 9
# text = Il venerdí di quella settimana la maestra restituí il test e scoprii che ero stata l’unica ad aver fatto zero errori nel test.
1	Il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	venerdí	venerdí	PROPN	SP	_	0	root	_	_
3	di	di	ADP	E	_	5	case	_	_
4	quella	quello	DET	DD	Gender=Fem|Number=Sing|PronType=Dem	5	det	_	_
5	settimana	settimana	NOUN	S	Gender=Fem|Number=Sing	2	nmod	_	_
6	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	7	det	_	_
7	maestra	maestro	NOUN	S	Gender=Fem|Number=Sing	2	appos	_	_
8	restituí	restituí	PROPN	SP	_	7	nmod	_	_
9	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	10	det	_	_
10	test	test	NOUN	S	Gender=Masc	7	conj	_	_
11	e	e	CONJ	CC	_	10	cc	_	_
12	scoprii	scoprio	NOUN	S	Gender=Masc|Number=Plur	10	conj	_	_
13	che	che	PRON	PR	PronType=Rel	15	nsubj	_	_
14	ero	essere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	15	aux	_	_
15	stata	essere	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	12	acl:relcl	_	_
16	l’unica	l’unico	ADV	B	_	19	advmod	_	_
17	ad	a	ADP	E	_	19	mark	_	_
18	aver	avere	AUX	VA	VerbForm=Inf	19	aux	_	_
19	fatto	fare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	15	advcl	_	_
20	zero	zero	NUM	N	NumType=Card	21	nummod	_	_
21	errori	errore	NOUN	S	Gender=Masc|Number=Plur	19	dobj	_	_
22-23	nel	_	_	_	_	_	_	_	_
22	in	in	ADP	E	_	24	case	_	_
23	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	24	det	_	_
24	test	test	NOUN	S	Gender=Masc	19	nmod	_	SpaceAfter=No
25	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 10
# text = Quando lo portai a casa la mamma mi riempí fino al collo di complimenti.
1	Quando	quando	SCONJ	CS	_	3	mark	_	_
2	lo	lo	PRON	PC	Gender=Masc|Number=Sing|Person=3|PronType=Clit	3	dobj	_	_
3	portai	portare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Past|VerbForm=Fin	0	root	_	_
4	a	a	ADP	E	_	5	case	_	_
5	casa	casa	NOUN	S	Gender=Fem|Number=Sing	3	nmod	_	_
6	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	7	det	_	_
7	mamma	mamma	NOUN	S	Gender=Fem|Number=Sing	3	nsubj	_	_
8	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	7	nmod	_	_
9	riempí	riempí	PROPN	SP	_	8	root	_	_
10	fino	fino	ADV	B	_	13	case	_	_
11-12	al	_	_	_	_	_	_	_	_
11	a	a	ADP	E	_	10	mwe	_	_
12	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	13	det	_	_
13	collo	collo	NOUN	S	Gender=Masc|Number=Sing	9	nmod	_	_
14	di	di	ADP	E	_	15	case	_	_
15	complimenti	complimenti	NOUN	S	Gender=Fem|Number=Sing	13	nmod	_	SpaceAfter=No
16	.	.	PUNCT	FS	_	9	punct	_	_

# sent_id = 11
# text = Da questo episodio ho imparato che devo sempre ascoltare la mamma e il sudore viene sempre premiato.
1	Da	da	ADP	E	_	3	case	_	_
2	questo	questo	DET	DD	Gender=Masc|Number=Sing|PronType=Dem	3	det	_	_
3	episodio	episodio	NOUN	S	Gender=Masc|Number=Sing	5	nmod	_	_
4	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	imparato	imparare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
6	che	che	SCONJ	CS	_	9	mark	_	_
7	devo	dovere	AUX	VM	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	9	aux	_	_
8	sempre	sempre	ADV	B	_	9	advmod	_	_
9	ascoltare	ascoltare	VERB	V	VerbForm=Inf	5	ccomp	_	_
10	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	11	det	_	_
11	mamma	mamma	NOUN	S	Gender=Fem|Number=Sing	9	dobj	_	_
12	e	e	CONJ	CC	_	11	cc	_	_
13	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	14	det	_	_
14	sudore	sudore	NOUN	S	Gender=Masc|Number=Sing	11	conj	_	_
15	viene	venire	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	17	aux	_	_
16	sempre	sempre	ADV	B	_	17	advmod	_	_
17	premiato	premiare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	5	parataxis	_	SpaceAfter=No
18	.	.	PUNCT	FS	_	5	punct	_	SpacesAfter=\r\n\r\n


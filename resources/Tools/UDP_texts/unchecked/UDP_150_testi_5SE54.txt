# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE54.txt
# newpar
# sent_id = 1
# text = Gara di bici La mia storia racconta che il mio Velo Club Arbedo ha scelto sei ragazzi che si impegnano di piú agli allenamenti.
1	Gara	gara	NOUN	S	Gender=Fem|Number=Sing	7	nmod	_	_
2	di	di	ADP	E	_	3	case	_	_
3	bici	bico	NOUN	S	Gender=Masc|Number=Plur	1	nmod	_	SpacesAfter=\s\s
4	La	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	6	det	_	_
5	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	6	det:poss	_	_
6	storia	storia	NOUN	S	Gender=Fem|Number=Sing	7	nsubj	_	_
7	racconta	raccontare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
8	che	che	SCONJ	CS	_	15	mark	_	_
9	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	11	det	_	_
10	mio	mio	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	11	det:poss	_	_
11	Velo	velo	PROPN	SP	_	15	nsubj	_	_
12	Club	Club	PROPN	SP	_	11	name	_	_
13	Arbedo	Arbedo	PROPN	SP	_	11	name	_	_
14	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	15	aux	_	_
15	scelto	scegliere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	7	ccomp	_	_
16	sei	sei	NUM	N	NumType=Card	17	nummod	_	_
17	ragazzi	ragazzo	NOUN	S	Gender=Masc|Number=Plur	15	dobj	_	_
18	che	che	PRON	PR	PronType=Rel	20	nsubj	_	_
19	si	si	PRON	PC	Person=3|PronType=Clit	20	expl	_	SpacesAfter=\s\s\s
20	impegnano	impegnare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	17	acl:relcl	_	_
21	di	di	ADP	E	_	22	case	_	_
22	piú	piú	PROPN	SP	_	20	nmod	_	_
23-24	agli	_	_	_	_	_	_	_	_
23	a	a	ADP	E	_	25	case	_	_
24	gli	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	25	det	_	_
25	allenamenti	allenamento	NOUN	S	Gender=Masc|Number=Plur	20	nmod	_	SpaceAfter=No
26	.	.	PUNCT	FS	_	7	punct	_	_

# sent_id = 2
# text = Il 22 aprile 2012 mio papá mi ha svegliato alle cinque e un quarto di mattina perché dovevo andare con il mio Velo Club, a fare una gara vicino a Zurigo non lo sapevo perché era una sorpresa.
1	Il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	22	22	NUM	N	NumType=Card	6	nummod	_	_
3	aprile	aprile	NOUN	S	Gender=Masc|Number=Sing	2	nmod	_	_
4	2012	2012	NUM	N	NumType=Card	3	nummod	_	_
5	mio	mio	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	6	det:poss	_	_
6	papá	papá	PROPN	SP	_	9	nsubj	_	_
7	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	9	dobj	_	_
8	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	9	aux	_	_
9	svegliato	svegliare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
10-11	alle	_	_	_	_	_	_	_	_
10	a	a	ADP	E	_	12	case	_	_
11	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	12	det	_	_
12	cinque	cinque	NUM	N	NumType=Card	9	nummod	_	_
13	e	e	CONJ	CC	_	12	cc	_	_
14	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	15	det	_	_
15	quarto	quarto	PRON	NO	Gender=Masc|Number=Sing|NumType=Ord	12	conj	_	_
16	di	di	ADP	E	_	17	case	_	_
17	mattina	mattina	NOUN	S	Gender=Fem|Number=Sing	15	nmod	_	_
18	perché	perché	ADV	B	_	20	advmod	_	_
19	dovevo	dovere	AUX	VM	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	20	aux	_	_
20	andare	andare	VERB	V	VerbForm=Inf	9	conj	_	_
21	con	con	ADP	E	_	24	case	_	_
22	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	24	det	_	_
23	mio	mio	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	24	det:poss	_	_
24	Velo	velo	PROPN	SP	_	20	nmod	_	_
25	Club	Club	PROPN	SP	_	24	name	_	SpaceAfter=No
26	,	,	PUNCT	FF	_	20	punct	_	_
27	a	a	ADP	E	_	28	mark	_	_
28	fare	fare	VERB	V	VerbForm=Inf	36	advcl	_	_
29	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	30	det	_	_
30	gara	gara	NOUN	S	Gender=Fem|Number=Sing	28	dobj	_	_
31	vicino	vicino	ADV	B	_	33	case	_	_
32	a	a	ADP	E	_	31	mwe	_	_
33	Zurigo	Zurigo	PROPN	SP	_	28	nmod	_	_
34	non	non	ADV	BN	PronType=Neg	36	neg	_	_
35	lo	lo	PRON	PC	Gender=Masc|Number=Sing|Person=3|PronType=Clit	36	dobj	_	_
36	sapevo	sapere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	20	advcl	_	_
37	perché	perché	SCONJ	CS	_	40	mark	_	_
38	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	40	cop	_	_
39	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	40	det	_	_
40	sorpresa	sorpresa	NOUN	S	Gender=Fem|Number=Sing	36	advcl	_	SpaceAfter=No
41	.	.	PUNCT	FS	_	9	punct	_	_

# sent_id = 3
# text = Durante il viaggio di andata ogni galleria che passevamo chambiava la meteo:
1	Durante	durante	ADP	E	_	3	case	_	_
2	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	3	det	_	_
3	viaggio	viaggio	NOUN	S	Gender=Masc|Number=Sing	0	root	_	_
4	di	di	ADP	E	_	5	mark	_	_
5	andata	andare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	3	acl	_	_
6	ogni	ogni	DET	DI	Number=Sing|PronType=Ind	7	det	_	_
7	galleria	galleria	NOUN	S	Gender=Fem|Number=Sing	5	dobj	_	_
8	che	che	PRON	PR	PronType=Rel	10	nsubj	_	_
9	passevamo	passere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	10	aux	_	_
10	chambiava	chambiare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	7	acl:relcl	_	_
11	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	12	det	_	_
12	meteo	meteo	NOUN	S	Gender=Masc|Number=Sing	10	dobj	_	SpaceAfter=No
13	:	:	PUNCT	FC	_	3	punct	_	_

# sent_id = 4
# text = pioggia, grandine, nebbia e sole.
1	pioggia	pioggia	NOUN	S	Gender=Fem|Number=Sing	0	root	_	SpaceAfter=No
2	,	,	PUNCT	FF	_	1	punct	_	_
3	grandine	grandine	NOUN	S	Gender=Fem|Number=Sing	1	conj	_	SpaceAfter=No
4	,	,	PUNCT	FF	_	1	punct	_	_
5	nebbia	nebbia	NOUN	S	Gender=Fem|Number=Sing	1	conj	_	_
6	e	e	CONJ	CC	_	1	cc	_	_
7	sole	sole	NOUN	S	Gender=Masc|Number=Sing	1	conj	_	SpaceAfter=No
8	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 5
# text = Il viaggio é durato due ore circa, appena arrivati faceva molto freddo ma appena sulla sella della bici ci siamo riscaldati.
1	Il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	viaggio	viaggio	NOUN	S	Gender=Masc|Number=Sing	11	nsubj	_	_
3	é	é	ADV	B	_	4	advmod	_	_
4	durato	durare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	2	acl	_	_
5	due	due	NUM	N	NumType=Card	6	nummod	_	_
6	ore	ora	NOUN	S	Gender=Fem|Number=Plur	4	nmod	_	_
7	circa	circa	ADV	B	_	6	advmod	_	SpaceAfter=No
8	,	,	PUNCT	FF	_	2	punct	_	_
9	appena	appena	ADV	B	_	10	advmod	_	SpacesAfter=\s\s\s
10	arrivati	arrivare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	2	acl	_	_
11	faceva	fare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
12	molto	molto	ADV	B	_	13	advmod	_	_
13	freddo	freddo	ADJ	A	Gender=Masc|Number=Sing	11	xcomp	_	_
14	ma	ma	CONJ	CC	_	11	cc	_	_
15	appena	appena	ADV	B	_	18	advmod	_	_
16-17	sulla	_	_	_	_	_	_	_	_
16	su	su	ADP	E	_	18	case	_	_
17	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	18	det	_	_
18	sella	sella	NOUN	S	Gender=Fem|Number=Sing	11	conj	_	_
19-20	della	_	_	_	_	_	_	_	_
19	di	di	ADP	E	_	21	case	_	_
20	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	21	det	_	_
21	bici	bico	NOUN	S	Gender=Masc|Number=Plur	18	nmod	_	_
22	ci	ci	PRON	PC	Number=Plur|Person=1|PronType=Clit	24	expl	_	_
23	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	24	aux	_	_
24	riscaldati	riscaldare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	21	acl	_	SpaceAfter=No
25	.	.	PUNCT	FS	_	11	punct	_	_

# sent_id = 6
# text = Come prima cosa siamo andati a fare le iscrizioni e poi abbiamo provato una piccola gincana che bisognava fare oltre alla gara.
1	Come	come	ADP	E	_	3	case	_	_
2	prima	primo	ADJ	NO	Gender=Fem|Number=Sing|NumType=Ord	3	amod	_	_
3	cosa	cosa	NOUN	S	Gender=Fem|Number=Sing	5	nmod	_	_
4	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	andati	andare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	0	root	_	_
6	a	a	ADP	E	_	7	mark	_	_
7	fare	fare	VERB	V	VerbForm=Inf	5	xcomp	_	_
8	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	9	det	_	_
9	iscrizioni	iscrizione	NOUN	S	Gender=Fem|Number=Plur	7	dobj	_	_
10	e	e	CONJ	CC	_	5	cc	_	_
11	poi	poi	ADV	B	_	13	advmod	_	_
12	abbiamo	avere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	13	aux	_	_
13	provato	provare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	5	conj	_	_
14	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	16	det	_	_
15	piccola	piccolo	ADJ	A	Gender=Fem|Number=Sing	16	amod	_	_
16	gincana	gincano	NOUN	S	Gender=Fem|Number=Sing	13	dobj	_	_
17	che	che	PRON	PR	PronType=Rel	18	nsubj	_	_
18	bisognava	bisognare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	16	acl:relcl	_	_
19	fare	fare	VERB	V	VerbForm=Inf	18	ccomp	_	_
20	oltre	oltre	ADV	B	_	23	case	_	_
21-22	alla	_	_	_	_	_	_	_	_
21	a	a	ADP	E	_	20	mwe	_	_
22	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	23	det	_	_
23	gara	gara	NOUN	S	Gender=Fem|Number=Sing	19	nmod	_	SpaceAfter=No
24	.	.	PUNCT	FS	_	5	punct	_	_

# sent_id = 7
# text = Nella gincana bisognava fare: uno slalom, prendere un legnetto da terra, prendere un bicchiere con la destra e appogiarlo con la sinistra e stare in equilibrio per almeno dieci secondi.
1-2	Nella	_	_	_	_	_	_	_	_
1	In	in	ADP	E	_	3	case	_	_
2	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	3	det	_	_
3	gincana	gincano	NOUN	S	Gender=Fem|Number=Sing	4	nmod	_	SpacesAfter=\s\s\s
4	bisognava	bisognare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
5	fare	fare	VERB	V	VerbForm=Inf	4	xcomp	_	SpaceAfter=No
6	:	:	PUNCT	FC	_	4	punct	_	_
7	uno	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	8	det	_	_
8	slalom	slalom	NOUN	S	Gender=Masc	4	dobj	_	SpaceAfter=No
9	,	,	PUNCT	FF	_	4	punct	_	_
10	prendere	prendere	VERB	V	VerbForm=Inf	4	conj	_	_
11	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	12	det	_	_
12	legnetto	legnetto	NOUN	S	Gender=Masc|Number=Sing	10	dobj	_	_
13	da	da	ADP	E	_	14	case	_	_
14	terra	terra	NOUN	S	Gender=Fem|Number=Sing	12	nmod	_	SpaceAfter=No
15	,	,	PUNCT	FF	_	4	punct	_	_
16	prendere	prendere	VERB	V	VerbForm=Inf	4	conj	_	_
17	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	18	det	_	_
18	bicchiere	bicchiere	NOUN	S	Gender=Masc|Number=Sing	16	dobj	_	_
19	con	con	ADP	E	_	21	case	_	_
20	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	21	det	_	_
21	destra	destra	NOUN	S	Gender=Fem|Number=Sing	16	nmod	_	_
22	e	e	CONJ	CC	_	21	cc	_	_
23	appogiarlo	appogiarlo	NOUN	S	Gender=Masc|Number=Sing	21	conj	_	_
24	con	con	ADP	E	_	26	case	_	_
25	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	26	det	_	_
26	sinistra	sinistra	NOUN	S	Gender=Fem|Number=Sing	16	nmod	_	SpacesAfter=\s\s\s
27	e	e	CONJ	CC	_	4	cc	_	_
28	stare	stare	VERB	V	VerbForm=Inf	4	conj	_	_
29	in	in	ADP	E	_	30	case	_	_
30	equilibrio	equilibrio	NOUN	S	Gender=Masc|Number=Sing	28	nmod	_	_
31	per	per	ADP	E	_	34	case	_	_
32	almeno	almeno	ADV	B	_	33	advmod	_	_
33	dieci	dieci	NUM	N	NumType=Card	34	nummod	_	_
34	secondi	secondo	NOUN	S	Gender=Masc|Number=Plur	28	nmod	_	SpaceAfter=No
35	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 8
# text = La mia gara era alle dodici e trenta prima la gincana e poi il giro, il giro era di tre chilometri e io dovevo farne venti quattro di chilometri ma ne ho fatti ventuno perché mi hanno doppiato all’ultimo giro, il mio arrivo è stato in volata a tre e l’ho vinta io. Finita la gara siamo andati al nostro furgone per mangiare e cambiarci perché eravamo sudati.
1	La	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	3	det	_	_
2	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	3	det:poss	_	_
3	gara	gara	NOUN	S	Gender=Fem|Number=Sing	4	nsubj	_	_
4	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
5-6	alle	_	_	_	_	_	_	_	_
5	a	a	ADP	E	_	7	case	_	_
6	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	7	det	_	_
7	dodici	dodici	NUM	N	NumType=Card	4	nummod	_	_
8	e	e	CONJ	CC	_	4	cc	_	_
9	trenta	trentare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	4	conj	_	_
10	prima	prima	ADV	B	_	9	advmod	_	_
11	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	12	det	_	_
12	gincana	gincano	NOUN	S	Gender=Fem|Number=Sing	9	nsubj	_	_
13	e	e	CONJ	CC	_	12	cc	_	_
14	poi	poi	ADV	B	_	16	advmod	_	_
15	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	16	det	_	_
16	giro	giro	NOUN	S	Gender=Masc|Number=Sing	12	conj	_	SpaceAfter=No
17	,	,	PUNCT	FF	_	9	punct	_	_
18	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	19	det	_	_
19	giro	giro	NOUN	S	Gender=Masc|Number=Sing	20	nsubj	_	_
20	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	9	conj	_	_
21	di	di	ADP	E	_	23	case	_	SpacesAfter=\s\s\s
22	tre	tre	NUM	N	NumType=Card	23	nummod	_	_
23	chilometri	chilometro	NOUN	S	Gender=Masc|Number=Plur	20	nmod	_	_
24	e	e	CONJ	CC	_	20	cc	_	_
25	io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	27	nsubj	_	_
26	dovevo	dovere	AUX	VM	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	27	aux	_	_
27-28	farne	_	_	_	_	_	_	_	_
27	far	fare	VERB	V	VerbForm=Inf	20	conj	_	_
28	ne	ne	PRON	PC	PronType=Clit	27	advmod	_	_
29	venti	venti	NUM	N	NumType=Card	30	compound	_	SpacesAfter=\s\s\s
30	quattro	quattro	NUM	N	NumType=Card	27	dobj	_	_
31	di	di	ADP	E	_	32	case	_	_
32	chilometri	chilometro	NOUN	S	Gender=Masc|Number=Plur	30	nmod	_	_
33	ma	ma	CONJ	CC	_	27	cc	_	_
34	ne	ne	PRON	PC	PronType=Clit	36	advmod	_	_
35	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	36	aux	_	_
36	fatti	fare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	27	conj	_	_
37	ventuno	ventuno	PRON	PI	Gender=Masc|Number=Sing|PronType=Ind	36	dobj	_	_
38	perché	perché	SCONJ	CS	_	41	mark	_	_
39	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	41	iobj	_	_
40	hanno	avere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	41	aux	_	_
41	doppiato	doppiare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	36	advcl	_	_
42	all’ultimo	all’ultimo	ADJ	A	Gender=Masc|Number=Sing	43	amod	_	_
43	giro	giro	NOUN	S	Gender=Masc|Number=Sing	41	dobj	_	SpaceAfter=No
44	,	,	PUNCT	FF	_	27	punct	_	_
45	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	47	det	_	_
46	mio	mio	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	47	det:poss	_	_
47	arrivo	arrivo	NOUN	S	Gender=Masc|Number=Sing	49	nsubj	_	_
48	è	essere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	49	aux	_	_
49	stato	essere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	27	conj	_	_
50	in	in	ADP	E	_	51	case	_	_
51	volata	volata	NOUN	S	Gender=Fem|Number=Sing	49	nmod	_	_
52	a	a	ADP	E	_	53	case	_	_
53	tre	tre	NUM	N	NumType=Card	49	nummod	_	_
54	e	e	CONJ	CC	_	49	cc	_	_
55	l’ho	l’avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	56	aux	_	_
56	vinta	vincere	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	49	conj	_	_
57	io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	56	xcomp	_	SpaceAfter=No
58	.	.	PUNCT	FS	_	59	punct	_	_
59	Finita	finire	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	63	advcl	_	_
60	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	61	det	_	_
61	gara	gara	NOUN	S	Gender=Fem|Number=Sing	63	nsubj	_	_
62	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	63	aux	_	_
63	andati	andare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	49	advcl	_	_
64-65	al	_	_	_	_	_	_	_	_
64	a	a	ADP	E	_	67	case	_	_
65	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	67	det	_	_
66	nostro	nostro	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	67	det:poss	_	_
67	furgone	furgone	NOUN	S	Gender=Masc|Number=Sing	63	nmod	_	_
68	per	per	ADP	E	_	69	mark	_	_
69	mangiare	mangiare	VERB	V	VerbForm=Inf	63	advcl	_	_
70	e	e	CONJ	CC	_	69	cc	_	_
71	cambiarci	cambiare	VERB	V	VerbForm=Inf	69	conj	_	_
72	perché	perché	SCONJ	CS	_	74	mark	_	_
73	eravamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	74	aux	_	_
74	sudati	sudare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	71	advcl	_	SpaceAfter=No
75	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 9
# text = Alla premiazione ci hanno dato una borraccia, due gel che danno forza e la classifica finale.
1-2	Alla	_	_	_	_	_	_	_	_
1	A	a	ADP	E	_	3	case	_	_
2	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	3	det	_	_
3	premiazione	premiazione	NOUN	S	Gender=Fem|Number=Sing	6	nmod	_	_
4	ci	ci	PRON	PC	Number=Plur|Person=1|PronType=Clit	6	iobj	_	_
5	hanno	avere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	6	aux	_	_
6	dato	dare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
7	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	8	det	_	_
8	borraccia	borraccia	NOUN	S	Gender=Fem|Number=Sing	6	dobj	_	SpaceAfter=No
9	,	,	PUNCT	FF	_	8	punct	_	SpacesAfter=\s\s
10	due	due	NUM	N	NumType=Card	11	nummod	_	_
11	gel	gel	NOUN	S	Gender=Masc	8	conj	_	_
12	che	che	PRON	PR	PronType=Rel	13	nsubj	_	_
13	danno	dare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	11	acl:relcl	_	_
14	forza	forza	NOUN	S	Gender=Fem|Number=Sing	13	dobj	_	_
15	e	e	CONJ	CC	_	14	cc	_	_
16	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	17	det	_	_
17	classifica	classifica	NOUN	S	Gender=Fem|Number=Sing	14	conj	_	_
18	finale	finale	ADJ	A	Number=Sing	17	amod	_	SpaceAfter=No
19	.	.	PUNCT	FS	_	6	punct	_	_

# sent_id = 10
# text = Nel viaggio di ritorno ci siamo fermati ad un autogrill dove ho comperato una rivista di macchine e leggendola il viaggio é durato meno dell’andata.
1-2	Nel	_	_	_	_	_	_	_	_
1	In	in	ADP	E	_	3	case	_	_
2	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	3	det	_	_
3	viaggio	viaggio	NOUN	S	Gender=Masc|Number=Sing	8	nmod	_	_
4	di	di	ADP	E	_	5	case	_	_
5	ritorno	ritorno	NOUN	S	Gender=Masc|Number=Sing	3	nmod	_	_
6	ci	ci	PRON	PC	Number=Plur|Person=1|PronType=Clit	8	expl	_	_
7	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	8	aux	_	_
8	fermati	fermare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	0	root	_	_
9	ad	a	ADP	E	_	11	case	_	_
10	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	11	det	_	_
11	autogrill	autogrill	NOUN	S	Gender=Masc	8	nmod	_	_
12	dove	dove	ADV	B	_	14	advmod	_	_
13	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	14	aux	_	_
14	comperato	comperare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	11	acl:relcl	_	_
15	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	16	det	_	_
16	rivista	rivista	NOUN	S	Gender=Fem|Number=Sing	14	dobj	_	_
17	di	di	ADP	E	_	18	case	_	_
18	macchine	macchina	NOUN	S	Gender=Fem|Number=Plur	16	nmod	_	_
19	e	e	CONJ	CC	_	18	cc	_	_
20	leggendola	leggendolare	NOUN	S	Gender=Fem|Number=Sing	18	conj	_	_
21	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	22	det	_	_
22	viaggio	viaggio	NOUN	S	Gender=Masc|Number=Sing	16	nmod	_	_
23	é	é	ADV	B	_	24	advmod	_	_
24	durato	durare	ADJ	A	Gender=Masc|Number=Sing	22	amod	_	_
25	meno	meno	ADV	B	_	26	advmod	_	_
26	dell’andata	dell’andare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	22	acl	_	SpaceAfter=No
27	.	.	PUNCT	FS	_	8	punct	_	_

# sent_id = 11
# text = È un’esperienza da ripetere.
1	È	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	2	cop	_	_
2	un’esperienza	un’esperienza	NOUN	S	Gender=Fem|Number=Sing	0	root	_	_
3	da	da	ADP	E	_	4	mark	_	_
4	ripetere	ripetere	VERB	V	VerbForm=Inf	2	acl	_	SpaceAfter=No
5	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 12
# text = Morale della storia:
1	Morale	morale	NOUN	S	Gender=Masc|Number=Sing	0	root	_	_
2-3	della	_	_	_	_	_	_	_	_
2	di	di	ADP	E	_	4	case	_	_
3	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	4	det	_	_
4	storia	storia	NOUN	S	Gender=Fem|Number=Sing	1	nmod	_	SpaceAfter=No
5	:	:	PUNCT	FC	_	1	punct	_	_

# sent_id = 13
# text = siamo andati con l’idea di impegnarci al massimo ma non di vincere come succede disolito cui in Ticino.
1	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	2	aux	_	_
2	andati	andare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	0	root	_	_
3	con	con	ADP	E	_	4	case	_	_
4	l’idea	l’idea	NOUN	S	Gender=Fem|Number=Sing	2	nmod	_	_
5	di	di	ADP	E	_	6	mark	_	_
6	impegnarci	impegnarco	VERB	V	VerbForm=Inf	2	xcomp	_	_
7-8	al	_	_	_	_	_	_	_	_
7	a	a	ADP	E	_	9	case	_	_
8	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	9	det	_	_
9	massimo	massimo	ADJ	A	Gender=Masc|Number=Sing	6	nmod	_	_
10	ma	ma	CONJ	CC	_	2	cc	_	_
11	non	non	ADV	BN	PronType=Neg	13	neg	_	_
12	di	di	ADP	E	_	13	mark	_	_
13	vincere	vincere	VERB	V	VerbForm=Inf	2	conj	_	SpacesAfter=\s\s\s
14	come	come	SCONJ	CS	_	15	mark	_	_
15	succede	succede	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	13	ccomp	_	_
16	disolito	disolire	ADV	B	_	17	advmod	_	_
17	cui	cui	PRON	PR	PronType=Rel	15	nsubj	_	_
18	in	in	ADP	E	_	19	case	_	_
19	Ticino	Ticino	PROPN	SP	_	17	nmod	_	SpaceAfter=No
20	.	.	PUNCT	FS	_	2	punct	_	SpacesAfter=\r\n\r\n


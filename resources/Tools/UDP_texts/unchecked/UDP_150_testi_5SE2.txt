# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE2.txt
# newpar
# sent_id = 1
# text = L’aquila e il leone Un giorno guardai un documentario che parlava degli animali.
1	L’aquila	L’aquila	NUM	N	NumType=Card	7	nummod	_	_
2	e	e	CONJ	CC	_	1	cc	_	_
3	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	4	det	_	_
4	leone	leone	NOUN	S	Gender=Masc|Number=Sing	1	conj	_	SpacesAfter=\s\s
5	Un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	6	det	_	_
6	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	4	nmod	_	_
7	guardai	guardare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Past|VerbForm=Fin	0	root	_	_
8	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	9	det	_	_
9	documentario	documentario	NOUN	S	Gender=Masc|Number=Sing	7	dobj	_	_
10	che	che	PRON	PR	PronType=Rel	11	nsubj	_	_
11	parlava	parlare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	9	acl:relcl	_	_
12-13	degli	_	_	_	_	_	_	_	_
12	di	di	ADP	E	_	14	case	_	_
13	gli	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	14	det	_	_
14	animali	animale	NOUN	S	Gender=Masc|Number=Plur	11	nmod	_	SpaceAfter=No
15	.	.	PUNCT	FS	_	7	punct	_	_

# sent_id = 2
# text = Il documentario che stavo guardando parlava di una leonessa, che viveva in Africa, e di un’aquila reale, che viveva in Gran Bretagna.
1	Il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	documentario	documentario	NOUN	S	Gender=Masc|Number=Sing	6	nsubj	_	_
3	che	che	PRON	PR	PronType=Rel	4	dobj	_	_
4	stavo	stare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	2	acl:relcl	_	_
5	guardando	guardare	VERB	V	VerbForm=Ger	4	advcl	_	_
6	parlava	parlare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
7	di	di	ADP	E	_	9	case	_	_
8	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	9	det	_	_
9	leonessa	leonessa	NOUN	S	Gender=Fem|Number=Sing	6	nmod	_	SpaceAfter=No
10	,	,	PUNCT	FF	_	9	punct	_	_
11	che	che	PRON	PR	PronType=Rel	12	nsubj	_	_
12	viveva	vivere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	9	acl:relcl	_	_
13	in	in	ADP	E	_	14	case	_	_
14	Africa	Africa	PROPN	SP	_	12	nmod	_	SpaceAfter=No
15	,	,	PUNCT	FF	_	9	punct	_	_
16	e	e	CONJ	CC	_	9	cc	_	_
17	di	di	ADP	E	_	18	case	_	_
18	un’aquila	un’aquila	NOUN	S	Gender=Fem|Number=Sing	9	conj	_	_
19	reale	reale	ADJ	A	Number=Sing	18	amod	_	SpaceAfter=No
20	,	,	PUNCT	FF	_	18	punct	_	_
21	che	che	PRON	PR	PronType=Rel	22	nsubj	_	_
22	viveva	vivere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	18	acl:relcl	_	_
23	in	in	ADP	E	_	24	case	_	_
24	Gran	Gran	PROPN	SP	_	22	nmod	_	_
25	Bretagna	Bretagna	PROPN	SP	_	24	name	_	SpaceAfter=No
26	.	.	PUNCT	FS	_	6	punct	_	_

# sent_id = 3
# text = L’aquila e la leonessa sono stati sottomessi ad una sfida, vinceva chi si nutriva per primo.
1	L’aquila	L’aquila	NUM	N	NumType=Card	7	nummod	_	_
2	e	e	CONJ	CC	_	1	cc	_	_
3	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	4	det	_	_
4	leonessa	leonessa	NOUN	S	Gender=Fem|Number=Sing	1	conj	_	_
5	sono	essere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	7	aux	_	_
6	stati	essere	AUX	VA	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	7	auxpass	_	_
7	sottomessi	sottomettere	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	12	advcl	_	_
8	ad	a	ADP	E	_	10	case	_	_
9	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	10	det	_	_
10	sfida	sfida	NOUN	S	Gender=Fem|Number=Sing	7	nmod	_	SpaceAfter=No
11	,	,	PUNCT	FF	_	7	punct	_	_
12	vinceva	vincere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
13	chi	chi	PRON	PQ	PronType=Int	15	nsubj	_	_
14	si	si	PRON	PC	Person=3|PronType=Clit	15	expl	_	_
15	nutriva	nutrire	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	12	conj	_	_
16	per	per	ADP	E	_	17	case	_	_
17	primo	primo	PRON	NO	Gender=Masc|Number=Sing|NumType=Ord	15	nmod	_	SpaceAfter=No
18	.	.	PUNCT	FS	_	12	punct	_	_

# sent_id = 4
# text = La leonessa trovò subito una preda, che era una zebra.
1	La	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	2	det	_	_
2	leonessa	leonessa	NOUN	S	Gender=Fem|Number=Sing	3	nsubj	_	_
3	trovò	trovare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
4	subito	subito	ADV	B	_	3	advmod	_	_
5	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	6	det	_	_
6	preda	preda	NOUN	S	Gender=Fem|Number=Sing	3	dobj	_	SpaceAfter=No
7	,	,	PUNCT	FF	_	6	punct	_	_
8	che	che	PRON	PR	PronType=Rel	11	nsubj	_	_
9	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	11	cop	_	_
10	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	11	det	_	_
11	zebra	zebra	NOUN	S	Gender=Fem|Number=Sing	6	acl:relcl	_	SpaceAfter=No
12	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 5
# text = La leonessa allora la attaccò ma fu respinta subito, da una zoccolata;
1	La	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	2	det	_	_
2	leonessa	leonessa	NOUN	S	Gender=Fem|Number=Sing	5	nsubj	_	_
3	allora	allora	ADV	B	_	2	advmod	_	_
4	la	lo	PRON	PC	Gender=Fem|Number=Sing|Person=3|PronType=Clit	5	dobj	_	_
5	attaccò	attaccare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
6	ma	ma	CONJ	CC	_	5	cc	_	_
7	fu	essere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	8	auxpass	_	_
8	respinta	respingere	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	5	conj	_	_
9	subito	subito	ADV	B	_	8	advmod	_	SpaceAfter=No
10	,	,	PUNCT	FF	_	8	punct	_	_
11	da	da	ADP	E	_	13	case	_	_
12	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	13	det	_	_
13	zoccolata	zoccolata	NOUN	S	Gender=Fem|Number=Sing	8	nmod	_	SpaceAfter=No
14	;	;	PUNCT	FC	_	5	punct	_	_

# sent_id = 6
# text = la leonessa si arrese subito, senza un premio.
1	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	2	det	_	_
2	leonessa	leonessa	NOUN	S	Gender=Fem|Number=Sing	4	nsubj	_	_
3	si	si	PRON	PC	Person=3|PronType=Clit	4	expl	_	_
4	arrese	arrendere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
5	subito	subito	ADV	B	_	4	advmod	_	SpaceAfter=No
6	,	,	PUNCT	FF	_	4	punct	_	_
7	senza	senza	ADP	E	_	9	case	_	_
8	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	9	det	_	_
9	premio	premio	NOUN	S	Gender=Masc|Number=Sing	4	nmod	_	SpaceAfter=No
10	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 7
# text = L’aquila dopo due, tre ore di volo avvistò una lepre giovane.
1	L’aquila	L’aquila	NUM	N	NumType=Card	9	nummod	_	_
2	dopo	dopo	ADP	E	_	3	case	_	_
3	due	due	NUM	N	NumType=Card	9	nummod	_	SpaceAfter=No
4	,	,	PUNCT	FF	_	3	punct	_	_
5	tre	tre	NUM	N	NumType=Card	6	nummod	_	_
6	ore	ora	NOUN	S	Gender=Fem|Number=Plur	9	nsubj	_	_
7	di	di	ADP	E	_	8	case	_	_
8	volo	volo	NOUN	S	Gender=Masc|Number=Sing	6	nmod	_	_
9	avvistò	avvistare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
10	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	11	det	_	_
11	lepre	lepra	NOUN	S	Gender=Masc|Number=Sing	9	dobj	_	_
12	giovane	giovane	ADJ	A	Number=Sing	11	amod	_	SpaceAfter=No
13	.	.	PUNCT	FS	_	9	punct	_	_

# sent_id = 8
# text = L’aquila allora attaccò per la prima volta, ma la lepre schivò l’attacco con agilità.
1	L’aquila	L’aquila	NUM	N	NumType=Card	3	nsubj	_	_
2	allora	allora	ADV	B	_	1	advmod	_	_
3	attaccò	attaccare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
4	per	per	ADP	E	_	7	case	_	_
5	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	7	det	_	_
6	prima	primo	ADJ	NO	Gender=Fem|Number=Sing|NumType=Ord	7	amod	_	_
7	volta	volta	NOUN	S	Gender=Fem|Number=Sing	3	nmod	_	SpaceAfter=No
8	,	,	PUNCT	FF	_	3	punct	_	_
9	ma	ma	CONJ	CC	_	3	cc	_	_
10	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	11	det	_	_
11	lepre	lepra	NOUN	S	Gender=Fem|Number=Plur	12	nsubj	_	_
12	schivò	schivare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	3	conj	_	_
13	l’attacco	l’attacco	NOUN	S	Gender=Masc|Number=Sing	12	dobj	_	_
14	con	con	ADP	E	_	15	case	_	_
15	agilità	agilità	NOUN	S	Gender=Fem	13	nmod	_	SpaceAfter=No
16	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 9
# text = L’aquila allora attaccò un’altra volta, ma di nuovo la lepre schivò l’attacco.
1	L’aquila	L’aquila	NUM	N	NumType=Card	3	nummod	_	_
2	allora	allora	ADV	B	_	3	advmod	_	_
3	attaccò	attaccare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
4	un’altra	un’altro	DET	DI	Gender=Fem|Number=Sing|PronType=Ind	5	det	_	_
5	volta	volta	NOUN	S	Gender=Fem|Number=Sing	3	dobj	_	SpaceAfter=No
6	,	,	PUNCT	FF	_	3	punct	_	_
7	ma	ma	CONJ	CC	_	3	cc	_	_
8	di	di	ADP	E	_	9	case	_	_
9	nuovo	nuovo	ADJ	A	Gender=Masc|Number=Sing	12	nmod	_	_
10	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	11	det	_	_
11	lepre	lepra	NOUN	S	Gender=Fem|Number=Plur	12	nsubj	_	_
12	schivò	schivare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	3	conj	_	_
13	l’attacco	l’attacco	NOUN	S	Gender=Masc|Number=Sing	12	dobj	_	SpaceAfter=No
14	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 10
# text = L’aquila non si arrese comunque e fece un terzo attacco, questa volta avendo successo.
1	L’aquila	L’aquila	NUM	N	NumType=Card	4	nsubj	_	_
2	non	non	ADV	BN	PronType=Neg	4	neg	_	_
3	si	si	PRON	PC	Person=3|PronType=Clit	4	expl	_	_
4	arrese	arrendere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
5	comunque	comunque	ADV	B	_	4	advmod	_	_
6	e	e	CONJ	CC	_	4	cc	_	_
7	fece	fare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	4	conj	_	_
8	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	10	det	_	_
9	terzo	terzo	ADJ	NO	Gender=Masc|Number=Sing|NumType=Ord	10	amod	_	_
10	attacco	attacco	NOUN	S	Gender=Masc|Number=Sing	7	dobj	_	SpaceAfter=No
11	,	,	PUNCT	FF	_	4	punct	_	_
12	questa	questo	DET	DD	Gender=Fem|Number=Sing|PronType=Dem	13	det	_	_
13	volta	volta	NOUN	S	Gender=Fem|Number=Sing	4	nmod	_	_
14	avendo	avere	VERB	V	VerbForm=Ger	13	acl	_	_
15	successo	succedere	NOUN	S	Gender=Masc|Number=Sing	14	dobj	_	SpaceAfter=No
16	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 11
# text = L’aquila, quindi, vinse la sfida.
1	L’aquila	L’aquila	NUM	N	NumType=Card	5	nsubj	_	SpaceAfter=No
2	,	,	PUNCT	FF	_	1	punct	_	_
3	quindi	quindi	ADV	B	_	5	advmod	_	SpaceAfter=No
4	,	,	PUNCT	FF	_	3	punct	_	_
5	vinse	vincere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
6	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	7	det	_	_
7	sfida	sfida	NOUN	S	Gender=Fem|Number=Sing	5	dobj	_	SpaceAfter=No
8	.	.	PUNCT	FS	_	5	punct	_	_

# sent_id = 12
# text = Il documentario disse che l’aquila non si arrese mai, invece la leonessa si arrese subito.
1	Il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	documentario	documentario	NOUN	S	Gender=Masc|Number=Sing	3	nsubj	_	_
3	disse	dire	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
4	che	che	SCONJ	CS	_	8	mark	_	_
5	l’aquila	l’aquila	NUM	N	NumType=Card	8	nsubj	_	_
6	non	non	ADV	BN	PronType=Neg	8	neg	_	_
7	si	si	PRON	PC	Person=3|PronType=Clit	8	expl	_	_
8	arrese	arrendere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	3	ccomp	_	_
9	mai	mai	ADV	B	_	8	advmod	_	SpaceAfter=No
10	,	,	PUNCT	FF	_	8	punct	_	_
11	invece	invece	ADV	B	_	15	advmod	_	_
12	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	13	det	_	_
13	leonessa	leonessa	NOUN	S	Gender=Fem|Number=Sing	15	nsubj	_	_
14	si	si	PRON	PC	Person=3|PronType=Clit	15	expl	_	_
15	arrese	arrendere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	8	conj	_	_
16	subito	subito	ADV	B	_	15	advmod	_	SpaceAfter=No
17	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 13
# text = Alla fine del documentario pensai che l’aquila era più valorosa della leonessa, e quindi si meritò la vittoria.
1-2	Alla	_	_	_	_	_	_	_	_
1	A	a	ADP	E	_	3	case	_	_
2	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	3	det	_	_
3	fine	fine	NOUN	S	Gender=Fem|Number=Sing	7	nmod	_	_
4-5	del	_	_	_	_	_	_	_	_
4	di	di	ADP	E	_	6	case	_	_
5	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	6	det	_	_
6	documentario	documentario	NOUN	S	Gender=Masc|Number=Sing	3	nmod	_	_
7	pensai	pensare	VERB	V	Mood=Imp|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	0	root	_	_
8	che	che	SCONJ	CS	_	12	mark	_	_
9	l’aquila	l’aquila	NOUN	S	Gender=Fem	12	nsubj	_	_
10	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	12	cop	_	_
11	più	più	ADV	B	_	12	advmod	_	_
12	valorosa	valoroso	ADJ	A	Gender=Fem|Number=Sing	7	ccomp	_	_
13-14	della	_	_	_	_	_	_	_	_
13	di	di	ADP	E	_	15	case	_	_
14	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	15	det	_	_
15	leonessa	leonessa	NOUN	S	Gender=Fem|Number=Sing	12	nmod	_	SpaceAfter=No
16	,	,	PUNCT	FF	_	12	punct	_	_
17	e	e	CONJ	CC	_	12	cc	_	_
18	quindi	quindi	ADV	B	_	20	advmod	_	_
19	si	si	PRON	PC	Person=3|PronType=Clit	20	expl	_	_
20	meritò	meritare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	12	conj	_	_
21	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	22	det	_	_
22	vittoria	vittoria	NOUN	S	Gender=Fem|Number=Sing	20	dobj	_	SpaceAfter=No
23	.	.	PUNCT	FS	_	7	punct	_	_

# sent_id = 14
# text = Questo documentario mi ha insegnato di non arrendersi mai, perché se non ti arrendi ottieni quel che vuoi.
1	Questo	questo	DET	DD	Gender=Masc|Number=Sing|PronType=Dem	2	det	_	_
2	documentario	documentario	NOUN	S	Gender=Masc|Number=Sing	5	nsubj	_	_
3	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	5	iobj	_	_
4	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	insegnato	insegnare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
6	di	di	ADP	E	_	8	mark	_	_
7	non	non	ADV	BN	PronType=Neg	8	neg	_	_
8-9	arrendersi	_	_	_	_	_	_	_	_
8	arrender	arrendere	VERB	V	VerbForm=Inf	5	xcomp	_	_
9	si	si	PRON	PC	Person=3|PronType=Clit	8	expl	_	_
10	mai	mai	ADV	B	_	8	advmod	_	SpaceAfter=No
11	,	,	PUNCT	FF	_	8	punct	_	_
12	perché	perché	ADV	B	_	15	advmod	_	_
13	se	se	SCONJ	CS	_	15	mark	_	_
14	non	non	ADV	BN	PronType=Neg	15	neg	_	_
15	ti	ti	PRON	PC	Number=Sing|Person=2|PronType=Clit	8	advcl	_	_
16	arrendi	arrendere	NOUN	S	Gender=Masc|Number=Plur	15	dobj	_	_
17	ottieni	ottenere	ADJ	A	Gender=Masc|Number=Plur	16	amod	_	_
18	quel	quello	PRON	PD	Gender=Masc|Number=Sing|PronType=Dem	16	nmod	_	_
19	che	che	PRON	PR	PronType=Rel	20	dobj	_	_
20	vuoi	volere	VERB	V	Mood=Ind|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	18	acl:relcl	_	SpaceAfter=No
21	.	.	PUNCT	FS	_	5	punct	_	SpacesAfter=\r\n\r\n


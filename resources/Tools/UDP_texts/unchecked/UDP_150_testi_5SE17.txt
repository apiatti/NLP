# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE17.txt
# newpar
# sent_id = 1
# text = A pescare col nonno Un bel giorno decisi di andare a pescare e chiesi a mio nonno se veniva anche lui.
1	A	a	ADP	E	_	2	mark	_	_
2	pescare	pescare	VERB	V	VerbForm=Inf	0	root	_	_
3-4	col	_	_	_	_	_	_	_	_
3	con	con	ADP	E	_	5	case	_	_
4	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	5	det	_	_
5	nonno	nonno	NOUN	S	Gender=Masc|Number=Sing	2	nmod	_	SpacesAfter=\s\s
6	Un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	8	det	_	_
7	bel	bello	ADJ	A	Gender=Masc|Number=Sing	8	amod	_	_
8	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	2	dobj	_	_
9	decisi	deciso	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	8	acl	_	_
10	di	di	ADP	E	_	11	mark	_	_
11	andare	andare	VERB	V	VerbForm=Inf	8	acl	_	_
12	a	a	ADP	E	_	13	mark	_	_
13	pescare	pescare	VERB	V	VerbForm=Inf	11	xcomp	_	_
14	e	e	CONJ	CC	_	11	cc	_	_
15	chiesi	chiendere	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	11	conj	_	_
16	a	a	ADP	E	_	18	case	_	_
17	mio	mio	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	18	det:poss	_	_
18	nonno	nonno	NOUN	S	Gender=Masc|Number=Sing	15	nmod	_	_
19	se	se	SCONJ	CS	_	20	mark	_	_
20	veniva	venire	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	15	advcl	_	_
21	anche	anche	ADV	B	_	22	advmod	_	_
22	lui	lui	PRON	PE	Gender=Masc|Number=Sing|Person=3|PronType=Prs	20	nsubj	_	SpaceAfter=No
23	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 2
# text = Accettó e andammo al lago.
1	Accettó	Accettó	PROPN	SP	_	0	root	_	_
2	e	e	CONJ	CC	_	1	cc	_	_
3	andammo	andare	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Past|VerbForm=Fin	1	conj	_	_
4-5	al	_	_	_	_	_	_	_	_
4	a	a	ADP	E	_	6	case	_	_
5	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	6	det	_	_
6	lago	lago	NOUN	S	Gender=Masc|Number=Sing	3	nmod	_	SpaceAfter=No
7	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 3
# text = Quando arrivammo mi aiutó a preparare la canna da pesca e cominciammo a pescare.
1	Quando	quando	SCONJ	CS	_	2	mark	_	_
2	arrivammo	arrivare	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Past|VerbForm=Fin	0	root	_	_
3	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	2	iobj	_	_
4	aiutó	aiutó	PROPN	SP	_	2	dobj	_	_
5	a	a	ADP	E	_	6	mark	_	_
6	preparare	preparare	VERB	V	VerbForm=Inf	2	xcomp	_	_
7	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	8	det	_	_
8	canna	canna	NOUN	S	Gender=Fem|Number=Sing	6	dobj	_	_
9	da	da	ADP	E	_	10	case	_	_
10	pesca	pesca	NOUN	S	Gender=Fem|Number=Sing	6	nmod	_	_
11	e	e	CONJ	CC	_	2	cc	_	_
12	cominciammo	cominciare	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Past|VerbForm=Fin	2	conj	_	_
13	a	a	ADP	E	_	14	mark	_	_
14	pescare	pescare	VERB	V	VerbForm=Inf	12	xcomp	_	SpaceAfter=No
15	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 4
# text = Io, senza esperienza, tiravo svelto il filo dopo un lancio e non pescavo nulla.
1	Io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	7	nsubj	_	SpaceAfter=No
2	,	,	PUNCT	FF	_	1	punct	_	_
3	senza	senza	ADP	E	_	4	case	_	_
4	esperienza	esperienza	NOUN	S	Gender=Fem|Number=Sing	7	nmod	_	SpaceAfter=No
5	,	,	PUNCT	FF	_	4	punct	_	_
6	tiravo	tiravo	AUX	VM	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	7	aux	_	_
7	svelto	svegliere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
8	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	9	det	_	_
9	filo	filo	NOUN	S	Gender=Masc|Number=Sing	7	dobj	_	_
10	dopo	dopo	ADP	E	_	12	case	_	_
11	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	12	det	_	_
12	lancio	lancio	NOUN	S	Gender=Masc|Number=Sing	7	nmod	_	_
13	e	e	CONJ	CC	_	12	cc	_	_
14	non	non	ADV	BN	PronType=Neg	15	neg	_	_
15	pescavo	pescare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	12	conj	_	_
16	nulla	nulla	PRON	PI	Number=Sing|PronType=Ind	15	dobj	_	SpaceAfter=No
17	.	.	PUNCT	FS	_	7	punct	_	_

# sent_id = 5
# text = Mio nonno invece dopo un lancio aspettava anche 3-4 minuti, prima di tirare il filo e prendeva molti pesci.
1	Mio	mio	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	2	det:poss	_	_
2	nonno	nonno	NOUN	S	Gender=Masc|Number=Sing	7	nsubj	_	_
3	invece	invece	ADV	B	_	6	advmod	_	_
4	dopo	dopo	ADP	E	_	6	case	_	_
5	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	6	det	_	_
6	lancio	lancio	NOUN	S	Gender=Masc|Number=Sing	7	nmod	_	_
7	aspettava	aspettare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
8	anche	anche	ADV	B	_	10	advmod	_	_
9	3-4	3-4	NUM	N	NumType=Card	10	nummod	_	_
10	minuti	minuto	NOUN	S	Gender=Masc|Number=Plur	7	nmod	_	SpaceAfter=No
11	,	,	PUNCT	FF	_	7	punct	_	_
12	prima	prima	ADV	B	_	14	mark	_	_
13	di	di	ADP	E	_	12	mwe	_	_
14	tirare	tirare	VERB	V	VerbForm=Inf	7	advcl	_	_
15	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	16	det	_	_
16	filo	filo	NOUN	S	Gender=Masc|Number=Sing	14	dobj	_	_
17	e	e	CONJ	CC	_	7	cc	_	_
18	prendeva	prendere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	7	conj	_	_
19	molti	molto	DET	DI	Gender=Masc|Number=Plur|PronType=Ind	20	det	_	_
20	pesci	pesce	NOUN	S	Gender=Masc|Number=Plur	18	dobj	_	SpaceAfter=No
21	.	.	PUNCT	FS	_	7	punct	_	_

# sent_id = 6
# text = Vedendomi triste venne da me e mi disse “Bisogna avere PAZIENZA e CALMA se vuoi pescare qualcosa”.
1	Vedendomi	Vedendomo	NOUN	S	Gender=Masc|Number=Plur	3	nsubj	_	_
2	triste	triste	ADJ	A	Number=Sing	1	amod	_	_
3	venne	venire	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
4	da	da	ADP	E	_	5	case	_	_
5	me	me	PRON	PE	Number=Sing|Person=1|PronType=Prs	3	nmod	_	_
6	e	e	CONJ	CC	_	3	cc	_	_
7	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	8	iobj	_	_
8	disse	dire	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	3	conj	_	_
9	“	“	PUNCT	FB	_	10	punct	_	SpaceAfter=No
10	Bisogna	Bisogna	PROPN	SP	_	8	xcomp	_	_
11	avere	avere	AUX	VA	VerbForm=Inf	10	xcomp	_	_
12	PAZIENZA	pazienza	PROPN	SP	_	11	dobj	_	_
13	e	e	CONJ	CC	_	12	cc	_	_
14	CALMA	calma	PROPN	SP	_	12	conj	_	_
15	se	se	SCONJ	CS	_	17	mark	_	_
16	vuoi	volere	AUX	VM	Mood=Ind|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	17	aux	_	_
17	pescare	pescare	VERB	V	VerbForm=Inf	11	advcl	_	_
18	qualcosa	qualcosa	PRON	PI	Number=Sing|PronType=Ind	17	dobj	_	SpaceAfter=No
19	”	”	PUNCT	FB	_	17	punct	_	SpaceAfter=No
20	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 7
# text = Così mi tornó voglia e ributtai il filo in acqua e presi un pesciolino.
1	Così	così	ADV	B	_	4	advmod	_	_
2	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	4	iobj	_	_
3	tornó	tornó	PROPN	SP	_	4	nsubj	_	_
4	voglia	vogliare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
5	e	e	CONJ	CC	_	4	cc	_	_
6	ributtai	ributtare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Past|VerbForm=Fin	4	conj	_	_
7	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	8	det	_	_
8	filo	filo	NOUN	S	Gender=Masc|Number=Sing	6	dobj	_	_
9	in	in	ADP	E	_	10	case	_	_
10	acqua	acqua	NOUN	S	Gender=Fem|Number=Sing	6	nmod	_	_
11	e	e	CONJ	CC	_	6	cc	_	_
12	presi	prendere	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	6	conj	_	_
13	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	14	det	_	_
14	pesciolino	pesciolino	NOUN	S	Gender=Masc|Number=Sing	12	nsubjpass	_	SpaceAfter=No
15	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 8
# text = Decisi di darlo a mio nonno, che lo usó per prendere un pesce piú grande.
1	Decisi	Decisi	NOUN	S	Number=Sing	0	root	_	_
2	di	di	ADP	E	_	3	case	_	_
3	darlo	darlo	NOUN	S	Gender=Masc|Number=Sing	1	nmod	_	_
4	a	a	ADP	E	_	6	case	_	_
5	mio	mio	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	6	det:poss	_	_
6	nonno	nonno	NOUN	S	Gender=Masc|Number=Sing	1	nmod	_	SpaceAfter=No
7	,	,	PUNCT	FF	_	1	punct	_	_
8	che	che	PRON	PR	PronType=Rel	1	conj	_	_
9	lo	il	PRON	PC	Gender=Masc|Number=Sing|Person=3|PronType=Clit	12	dobj	_	_
10	usó	usó	PROPN	SP	_	9	nmod	_	_
11	per	per	ADP	E	_	12	mark	_	_
12	prendere	prendere	VERB	V	VerbForm=Inf	8	advcl	_	_
13	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	14	det	_	_
14	pesce	pesce	NOUN	S	Gender=Masc|Number=Sing	12	dobj	_	_
15	piú	piú	PROPN	SP	_	14	nmod	_	_
16	grande	grande	ADJ	A	Number=Sing	15	amod	_	SpaceAfter=No
17	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 9
# text = Tornai a casa molto felice perché avevo imparato delle cose che non servivano solo per quando si pesca, ma anche a scuola.
1	Tornai	tornare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Past|VerbForm=Fin	0	root	_	_
2	a	a	ADP	E	_	3	case	_	_
3	casa	casa	NOUN	S	Gender=Fem|Number=Sing	1	nmod	_	_
4	molto	molto	ADV	B	_	5	advmod	_	_
5	felice	felice	ADJ	A	Number=Sing	1	xcomp	_	_
6	perché	perché	SCONJ	CS	_	8	mark	_	_
7	avevo	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	8	aux	_	_
8	imparato	imparare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	1	advcl	_	_
9-10	delle	_	_	_	_	_	_	_	_
9	di	di	ADP	E	_	11	case	_	_
10	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	11	det	_	_
11	cose	cosa	NOUN	S	Gender=Fem|Number=Plur	8	nmod	_	_
12	che	che	PRON	PR	PronType=Rel	14	dobj	_	_
13	non	non	ADV	BN	PronType=Neg	14	neg	_	_
14	servivano	servire	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	11	acl:relcl	_	_
15	solo	solo	ADV	B	_	14	advmod	_	_
16	per	per	ADP	E	_	17	case	_	_
17	quando	quando	SCONJ	CS	_	19	mark	_	_
18	si	si	PRON	PC	Person=3|PronType=Clit	19	expl	_	_
19	pesca	puscire	VERB	V	Mood=Sub|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	14	advcl	_	SpaceAfter=No
20	,	,	PUNCT	FF	_	8	punct	_	_
21	ma	ma	CONJ	CC	_	8	cc	_	_
22	anche	anche	ADV	B	_	24	advmod	_	_
23	a	a	ADP	E	_	24	case	_	_
24	scuola	scuola	NOUN	S	Gender=Fem|Number=Sing	8	conj	_	SpaceAfter=No
25	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 10
# text = Ho imparato ad: avere pazienza nei lavori e farli con calma e che da una cosa piccola se ne puó fare una grande.
1	Ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	2	aux	_	_
2	imparato	imparare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
3	ad	a	ADP	E	_	5	mark	_	SpaceAfter=No
4	:	:	PUNCT	FC	_	5	punct	_	_
5	avere	avere	VERB	V	VerbForm=Inf	2	xcomp	_	_
6	pazienza	pazienza	NOUN	S	Gender=Fem|Number=Sing	5	dobj	_	_
7-8	nei	_	_	_	_	_	_	_	_
7	in	in	ADP	E	_	9	case	_	_
8	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	9	det	_	_
9	lavori	lavoro	NOUN	S	Gender=Masc|Number=Plur	5	nmod	_	_
10	e	e	CONJ	CC	_	5	cc	_	_
11-12	farli	_	_	_	_	_	_	_	_
11	far	fare	VERB	V	VerbForm=Inf	5	conj	_	_
12	li	li	PRON	PC	Gender=Masc|Number=Plur|Person=3|PronType=Clit	11	dobj	_	_
13	con	con	ADP	E	_	14	case	_	_
14	calma	calma	NOUN	S	Gender=Fem|Number=Sing	11	nmod	_	_
15	e	e	CONJ	CC	_	14	cc	_	_
16	che	che	SCONJ	CS	_	24	mark	_	_
17	da	da	ADP	E	_	19	case	_	_
18	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	19	det	_	_
19	cosa	cosa	NOUN	S	Gender=Fem|Number=Sing	24	nmod	_	_
20	piccola	piccolo	ADJ	A	Gender=Fem|Number=Sing	19	amod	_	_
21	se	se	PRON	PC	Person=3|PronType=Clit	23	expl	_	_
22	ne	ne	PRON	PC	PronType=Clit	23	advmod	_	_
23	puó	puó	PROPN	SP	_	19	nmod	_	_
24	fare	fare	VERB	V	VerbForm=Inf	14	conj	_	_
25	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	26	det	_	_
26	grande	grande	ADJ	A	Number=Sing	24	xcomp	_	SpaceAfter=No
27	.	.	PUNCT	FS	_	5	punct	_	SpacesAfter=\r\n\r\n


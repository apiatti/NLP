# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE38.txt
# newpar
# sent_id = 1
# text = LA DISAVVENTURA SULLA NEVE Qualche anno fa in montagna, stavamo scendendo io mia sorella e degli amici con il bob sulla neve.
1	LA	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	2	det	_	_
2	DISAVVENTURA	DISAVVENTURA	PROPN	SP	_	5	nsubj	_	_
3-4	SULLA	_	_	_	_	_	_	_	_
3	SU	su	PROPN	SP	_	2	name	_	_
4	LA	il	PROPN	SP	_	2	name	_	_
5	NEVE	neve	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	SpacesAfter=\s\s
6	Qualche	qualche	DET	DI	Number=Sing|PronType=Ind	7	det	_	_
7	anno	anno	NOUN	S	Gender=Masc|Number=Sing	5	nmod	_	_
8	fa	fare	ADV	B	_	7	advmod	_	_
9	in	in	ADP	E	_	10	case	_	_
10	montagna	montagna	NOUN	S	Gender=Fem|Number=Sing	5	nmod	_	SpaceAfter=No
11	,	,	PUNCT	FF	_	5	punct	_	_
12	stavamo	stare	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	13	aux	_	_
13	scendendo	scendere	VERB	V	VerbForm=Ger	5	conj	_	_
14	io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	13	dobj	_	_
15	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	16	det:poss	_	_
16	sorella	sorella	NOUN	S	Gender=Fem|Number=Sing	14	nsubj	_	_
17	e	e	CONJ	CC	_	16	cc	_	_
18-19	degli	_	_	_	_	_	_	_	_
18	di	di	ADP	E	_	20	case	_	_
19	gli	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	20	det	_	_
20	amici	amico	NOUN	S	Gender=Masc|Number=Plur	16	conj	_	_
21	con	con	ADP	E	_	23	case	_	_
22	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	23	det	_	_
23	bob	bob	NOUN	S	Gender=Masc	20	nmod	_	_
24-25	sulla	_	_	_	_	_	_	_	_
24	su	su	ADP	E	_	26	case	_	_
25	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	26	det	_	_
26	neve	neve	NOUN	S	Gender=Fem|Number=Sing	23	nmod	_	SpaceAfter=No
27	.	.	PUNCT	FS	_	5	punct	_	_

# sent_id = 2
# text = Dopo tante discese mia mamma disse che ci dovevamo fermare perché il sole stava calando e la neve stava ghiacciando.
1	Dopo	dopo	ADP	E	_	3	case	_	_
2	tante	tanto	DET	DI	Gender=Fem|Number=Plur|PronType=Ind	3	det	_	_
3	discese	discese	NOUN	S	Gender=Fem|Number=Plur	6	nmod	_	_
4	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	5	det:poss	_	_
5	mamma	mamma	NOUN	S	Gender=Fem|Number=Sing	6	nsubj	_	_
6	disse	dire	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
7	che	che	SCONJ	CS	_	10	mark	_	_
8	ci	ci	PRON	PC	Number=Plur|Person=1|PronType=Clit	10	expl	_	_
9	dovevamo	dovere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	10	aux	_	_
10	fermare	fermare	VERB	V	VerbForm=Inf	6	ccomp	_	_
11	perché	perché	SCONJ	CS	_	15	mark	_	_
12	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	13	det	_	_
13	sole	sole	NOUN	S	Gender=Masc|Number=Sing	15	nsubj	_	_
14	stava	stare	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	15	aux	_	_
15	calando	calare	VERB	V	VerbForm=Ger	10	advcl	_	_
16	e	e	CONJ	CC	_	10	cc	_	_
17	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	18	det	_	_
18	neve	neve	NOUN	S	Gender=Fem|Number=Sing	20	nsubj	_	_
19	stava	stare	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	20	aux	_	_
20	ghiacciando	ghiacciare	VERB	V	VerbForm=Ger	10	conj	_	SpaceAfter=No
21	.	.	PUNCT	FS	_	6	punct	_	_

# sent_id = 3
# text = Io testarda come un mulo feci un’altra discesa. Partii velocissima, tentai di frenare ma non riuscii, i freni non frenavano piú, andai contro una rete ruggine, mi tagliai sulla fronte, andai subito all’ospedale e dopo tanti minuti di urla mi fecero alcuni punti.
1	Io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	2	nsubj	_	_
2	testarda	testardare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
3	come	come	ADP	E	_	5	case	_	_
4	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	5	det	_	_
5	mulo	mulo	NOUN	S	Gender=Masc|Number=Sing	2	nmod	_	_
6	feci	feco	ADJ	A	Number=Plur	5	amod	_	_
7	un’altra	un’altro	ADJ	A	Gender=Fem|Number=Sing	8	amod	_	_
8	discesa	discesa	NOUN	S	Gender=Fem|Number=Sing	5	compound	_	SpaceAfter=No
9	.	.	PUNCT	FS	_	2	punct	_	_
10	Partii	Partio	PROPN	SP	_	2	dobj	_	_
11	velocissima	veloce	ADJ	A	Degree=Abs|Gender=Fem|Number=Sing	10	amod	_	SpaceAfter=No
12	,	,	PUNCT	FF	_	2	punct	_	_
13	tentai	tentare	VERB	V	Mood=Ind|Number=Sing|Person=2|Tense=Fut|VerbForm=Fin	2	conj	_	_
14	di	di	ADP	E	_	15	mark	_	_
15	frenare	frenare	VERB	V	VerbForm=Inf	13	xcomp	_	_
16	ma	ma	CONJ	CC	_	13	cc	_	_
17	non	non	ADV	BN	PronType=Neg	18	neg	_	_
18	riuscii	riuscio	ADJ	NO	Number=Sing|NumType=Ord	13	conj	_	SpaceAfter=No
19	,	,	PUNCT	FF	_	18	punct	_	_
20	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	21	det	_	_
21	freni	freno	NOUN	S	Gender=Masc|Number=Plur	23	nsubj	_	_
22	non	non	ADV	BN	PronType=Neg	23	neg	_	_
23	frenavano	frenare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	18	conj	_	_
24	piú	piú	PROPN	SP	_	23	dobj	_	SpaceAfter=No
25	,	,	PUNCT	FF	_	18	punct	_	_
26	andai	andare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Past|VerbForm=Fin	18	conj	_	_
27	contro	contro	ADP	E	_	29	case	_	_
28	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	29	det	_	_
29	rete	rete	NOUN	S	Gender=Fem|Number=Sing	26	nmod	_	_
30	ruggine	ruggine	ADJ	A	Number=Sing	29	amod	_	SpaceAfter=No
31	,	,	PUNCT	FF	_	18	punct	_	_
32	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	33	iobj	_	_
33	tagliai	tagliare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Past|VerbForm=Fin	18	conj	_	_
34-35	sulla	_	_	_	_	_	_	_	_
34	su	su	ADP	E	_	36	case	_	_
35	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	36	det	_	_
36	fronte	fronte	NOUN	S	Number=Sing	33	nmod	_	SpaceAfter=No
37	,	,	PUNCT	FF	_	18	punct	_	_
38	andai	andare	VERB	V	Mood=Ind|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	40	cop	_	_
39	subito	subito	ADV	B	_	40	advmod	_	_
40	all’ospedale	all’ospedale	ADJ	A	Number=Sing	18	conj	_	_
41	e	e	CONJ	CC	_	18	cc	_	_
42	dopo	dopo	ADP	E	_	44	case	_	_
43	tanti	tanto	DET	DI	Gender=Masc|Number=Plur|PronType=Ind	44	det	_	_
44	minuti	minuto	NOUN	S	Gender=Masc|Number=Plur	48	nmod	_	_
45	di	di	ADP	E	_	46	case	_	_
46	urla	urlo	NOUN	S	Gender=Fem|Number=Sing	44	nmod	_	_
47	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	48	iobj	_	_
48	fecero	fare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Past|VerbForm=Fin	18	conj	_	_
49	alcuni	alcuno	DET	DI	Gender=Masc|Number=Plur|PronType=Ind	50	det	_	_
50	punti	punto	NOUN	S	Gender=Masc|Number=Plur	48	dobj	_	SpaceAfter=No
51	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 4
# text = Ahi, sento ancora il male!
1	Ahi	Ahi	NOUN	S	Gender=Masc|Number=Plur	0	root	_	SpaceAfter=No
2	,	,	PUNCT	FF	_	1	punct	_	_
3	sento	sentire	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	1	conj	_	_
4	ancora	ancora	ADV	B	_	3	advmod	_	_
5	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	6	det	_	_
6	male	male	ADV	B	_	3	advmod	_	SpaceAfter=No
7	!	!	PUNCT	FS	_	1	punct	_	_

# sent_id = 5
# text = Da quella volta diedi sempre retta a mia mamma perché fino alla fine ha sempre ragione!
1	Da	da	ADP	E	_	3	case	_	_
2	quella	quello	DET	DD	Gender=Fem|Number=Sing|PronType=Dem	3	det	_	_
3	volta	volta	NOUN	S	Gender=Fem|Number=Sing	4	nmod	_	_
4	diedi	diedio	VERB	V	Mood=Ind|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	0	root	_	_
5	sempre	sempre	ADV	B	_	6	advmod	_	_
6	retta	rettare	ADJ	A	Gender=Fem|Number=Sing	4	dobj	_	_
7	a	a	ADP	E	_	9	case	_	_
8	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	9	det:poss	_	_
9	mamma	mamma	NOUN	S	Gender=Fem|Number=Sing	6	nmod	_	_
10	perché	perché	SCONJ	CS	_	15	mark	_	_
11	fino	fino	ADV	B	_	14	case	_	_
12-13	alla	_	_	_	_	_	_	_	_
12	a	a	ADP	E	_	11	mwe	_	_
13	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	14	det	_	_
14	fine	fine	NOUN	S	Gender=Fem|Number=Sing	15	nmod	_	_
15	ha	avere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	4	advcl	_	_
16	sempre	sempre	ADV	B	_	15	advmod	_	_
17	ragione	ragione	NOUN	S	Gender=Fem|Number=Sing	15	dobj	_	SpaceAfter=No
18	!	!	PUNCT	FS	_	4	punct	_	SpacesAfter=\r\n\r\n


# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE105.txt
# newpar
# sent_id = 1
# text = Torneo calcio scolari sapete tutti cos’é il torneo calcio scolari (torneo calcio scolari e un torneo di calcio per i bambini delle classi che si offrono volontari). stavamo partecipando propio a quel torneo.
1	Torneo	Torneo	ADJ	A	Gender=Masc|Number=Sing	2	amod	_	_
2	calcio	calcio	NOUN	S	Gender=Masc|Number=Sing	6	nsubj	_	_
3	scolari	scolare	ADJ	A	Gender=Masc|Number=Plur	2	amod	_	SpacesAfter=\s\s
4	sapete	sapere	VERB	V	Mood=Ind|Number=Plur|Person=2|Tense=Pres|VerbForm=Fin	2	acl	_	_
5	tutti	tutto	PRON	PI	Gender=Masc|Number=Plur|PronType=Ind	4	dobj	_	_
6	cos’é	cos’é	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
7	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	9	det	_	_
8	torneo	torneo	ADJ	A	Gender=Masc|Number=Sing	9	amod	_	_
9	calcio	calcio	NOUN	S	Gender=Masc|Number=Sing	6	dobj	_	_
10	scolari	scolare	ADJ	A	Number=Plur	9	amod	_	_
11	(	(	PUNCT	FB	_	13	punct	_	SpaceAfter=No
12	torneo	torneo	ADJ	A	Gender=Masc|Number=Sing	13	amod	_	_
13	calcio	calcio	NOUN	S	Gender=Masc|Number=Sing	9	appos	_	_
14	scolari	scolare	ADJ	A	Number=Plur	13	amod	_	_
15	e	e	CONJ	CC	_	13	cc	_	_
16	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	17	det	_	_
17	torneo	torneo	NOUN	S	Gender=Masc|Number=Sing	13	conj	_	_
18	di	di	ADP	E	_	19	case	_	_
19	calcio	calcio	NOUN	S	Gender=Masc|Number=Sing	17	nmod	_	_
20	per	per	ADP	E	_	22	case	_	_
21	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	22	det	_	_
22	bambini	bambino	NOUN	S	Gender=Masc|Number=Plur	17	nmod	_	_
23-24	delle	_	_	_	_	_	_	_	_
23	di	di	ADP	E	_	25	case	_	_
24	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	25	det	_	_
25	classi	classe	NOUN	S	Gender=Fem|Number=Plur	22	nmod	_	_
26	che	che	PRON	PR	PronType=Rel	28	nsubj	_	_
27	si	si	PRON	PC	Person=3|PronType=Clit	28	expl	_	_
28	offrono	offrire	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	25	acl:relcl	_	_
29	volontari	volontario	NOUN	S	Gender=Masc|Number=Plur	28	xcomp	_	SpaceAfter=No
30	)	)	PUNCT	FB	_	13	punct	_	SpaceAfter=No
31	.	.	PUNCT	FS	_	6	punct	_	_
32	stavamo	stare	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	33	aux	_	_
33	partecipando	partecipare	VERB	V	VerbForm=Ger	6	advcl	_	_
34	propio	propio	NOUN	S	Gender=Masc|Number=Sing	33	dobj	_	_
35	a	a	ADP	E	_	37	case	_	_
36	quel	quello	DET	DD	Gender=Masc|Number=Sing|PronType=Dem	37	det	_	_
37	torneo	torneo	NOUN	S	Gender=Masc|Number=Sing	33	nmod	_	SpaceAfter=No
38	.	.	PUNCT	FS	_	6	punct	_	_

# sent_id = 2
# text = Eravamo ad allenarci per la prima partita, entriamo in campo e gli avversari erano, anzi sembravano, un po deboluci ma non era così.
1	Eravamo	essere	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	0	root	_	_
2	ad	a	ADP	E	_	3	case	_	_
3	allenarci	allenarco	NOUN	S	Gender=Fem|Number=Plur	1	nmod	_	_
4	per	per	ADP	E	_	7	case	_	_
5	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	7	det	_	_
6	prima	primo	ADJ	NO	Gender=Fem|Number=Sing|NumType=Ord	7	amod	_	_
7	partita	partita	NOUN	S	Gender=Fem|Number=Sing	3	nmod	_	SpaceAfter=No
8	,	,	PUNCT	FF	_	1	punct	_	_
9	entriamo	entrare	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	1	conj	_	_
10	in	in	ADP	E	_	11	case	_	_
11	campo	campo	NOUN	S	Gender=Masc|Number=Sing	9	nmod	_	_
12	e	e	CONJ	CC	_	1	cc	_	_
13	gli	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	14	det	_	_
14	avversari	avversario	NOUN	S	Gender=Masc|Number=Plur	1	conj	_	_
15	erano	essere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	14	advcl	_	SpaceAfter=No
16	,	,	PUNCT	FF	_	1	punct	_	_
17	anzi	anzi	ADV	B	_	18	advmod	_	_
18	sembravano	sembrare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	1	conj	_	SpaceAfter=No
19	,	,	PUNCT	FF	_	18	punct	_	_
20	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	21	det	_	_
21	po	po	NOUN	S	Gender=Masc|Number=Sing	18	dobj	_	_
22	deboluci	deboluce	ADJ	A	Gender=Masc|Number=Plur	21	amod	_	_
23	ma	ma	CONJ	CC	_	21	cc	_	_
24	non	non	ADV	BN	PronType=Neg	26	neg	_	_
25	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	26	cop	_	_
26	così	così	ADV	B	_	21	advmod	_	SpaceAfter=No
27	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 3
# text = Alla fine della partita il risultato era 3-0 per loro, mica per noi, e lì ci sembrava di essere un po’ deboluci noi.
1-2	Alla	_	_	_	_	_	_	_	_
1	A	a	ADP	E	_	3	case	_	_
2	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	3	det	_	_
3	fine	fine	NOUN	S	Gender=Fem|Number=Sing	10	nmod	_	_
4-5	della	_	_	_	_	_	_	_	_
4	di	di	ADP	E	_	6	case	_	_
5	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	6	det	_	_
6	partita	partita	NOUN	S	Gender=Fem|Number=Sing	3	nmod	_	_
7	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	8	det	_	_
8	risultato	risultato	NOUN	S	Gender=Masc|Number=Sing	10	nsubj	_	_
9	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	10	cop	_	_
10	3-0	3-0	NUM	N	NumType=Card	0	root	_	_
11	per	per	ADP	E	_	12	case	_	_
12	loro	loro	PRON	PE	Number=Plur|Person=3|PronType=Prs	10	nmod	_	SpaceAfter=No
13	,	,	PUNCT	FF	_	10	punct	_	_
14	mica	mica	ADV	BN	PronType=Neg	10	advmod	_	_
15	per	per	ADP	E	_	16	case	_	_
16	noi	noi	PRON	PE	Number=Plur|Person=1|PronType=Prs	14	nmod	_	SpaceAfter=No
17	,	,	PUNCT	FF	_	10	punct	_	_
18	e	e	CONJ	CC	_	10	cc	_	_
19	lì	lì	ADV	B	_	21	advmod	_	_
20	ci	ci	PRON	PC	PronType=Clit	21	advmod	_	_
21	sembrava	sembrare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	10	conj	_	_
22	di	di	ADP	E	_	26	case	_	_
23	essere	essere	VERB	V	VerbForm=Inf	26	cop	_	_
24	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	26	det	_	_
25	po’	ipo	NUM	N	NumType=Card	26	nummod	_	_
26	deboluci	deboluce	NOUN	S	Gender=Masc|Number=Plur	21	nmod	_	_
27	noi	noi	PRON	PE	Number=Plur|Person=1|PronType=Prs	26	nmod	_	SpaceAfter=No
28	.	.	PUNCT	FS	_	10	punct	_	_

# sent_id = 4
# text = Alla seconda partita 5-0 che vergogna ma poi arrivò la maestra e ci dà un gelato, a mi sono dimenticato avvevamo fatto pranzo.
1-2	Alla	_	_	_	_	_	_	_	_
1	A	a	ADP	E	_	4	case	_	_
2	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	4	det	_	_
3	seconda	secondo	ADJ	NO	Gender=Fem|Number=Sing|NumType=Ord	4	amod	_	_
4	partita	partita	NOUN	S	Gender=Fem|Number=Sing	0	root	_	_
5	5-0	5-0	NUM	N	NumType=Card	4	nummod	_	_
6	che	che	PRON	PR	PronType=Rel	7	nsubj	_	_
7	vergogna	vergogna	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	4	acl:relcl	_	_
8	ma	ma	CONJ	CC	_	7	cc	_	_
9	poi	poi	ADV	B	_	10	advmod	_	_
10	arrivò	arrivare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	7	conj	_	_
11	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	12	det	_	_
12	maestra	maestra	NOUN	S	Gender=Fem|Number=Sing	10	dobj	_	_
13	e	e	CONJ	CC	_	7	cc	_	_
14	ci	ci	PRON	PC	Number=Plur|Person=1|PronType=Clit	15	iobj	_	_
15	dà	dare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	7	conj	_	_
16	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	17	det	_	_
17	gelato	gelato	NOUN	S	Gender=Masc|Number=Sing	15	dobj	_	SpaceAfter=No
18	,	,	PUNCT	FF	_	15	punct	_	_
19	a	a	ADP	E	_	20	case	_	_
20	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	22	nmod	_	_
21	sono	essere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	22	aux	_	_
22	dimenticato	dimenticare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	15	conj	_	_
23	avvevamo	avvere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	24	aux	_	_
24	fatto	fare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	22	advcl	_	_
25	pranzo	pranzo	NOUN	S	Gender=Masc|Number=Sing	24	dobj	_	SpaceAfter=No
26	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 5
# text = E a l’ultima partita con dei piccoletti ma la loro forza del calcio era gigantesca e abbiamo perso 9-0 qua in questa partita altro che tristezza come le altre e certi si sono messi a piangere.
1	E	e	CONJ	CC	_	17	cc	_	_
2	a	a	ADP	E	_	4	case	_	_
3	l’ultima	l’ultimo	ADJ	A	Gender=Fem|Number=Sing	4	amod	_	_
4	partita	partita	NOUN	S	Gender=Fem|Number=Sing	17	nmod	_	_
5	con	con	ADP	E	_	8	case	_	_
6-7	dei	_	_	_	_	_	_	_	_
6	di	di	ADP	E	_	8	case	_	_
7	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	8	det	_	_
8	piccoletti	piccoletto	NOUN	S	Gender=Masc|Number=Plur	4	nmod	_	_
9	ma	ma	CONJ	CC	_	4	cc	_	_
10	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	12	det	_	_
11	loro	loro	DET	AP	Poss=Yes|PronType=Prs	12	det:poss	_	_
12	forza	forza	NOUN	S	Gender=Fem|Number=Sing	17	nsubj	_	_
13-14	del	_	_	_	_	_	_	_	_
13	di	di	ADP	E	_	15	case	_	_
14	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	15	det	_	_
15	calcio	calcio	NOUN	S	Gender=Masc|Number=Sing	12	nmod	_	_
16	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	17	cop	_	_
17	gigantesca	gigantesco	ADJ	A	Gender=Fem|Number=Sing	0	root	_	_
18	e	e	CONJ	CC	_	17	cc	_	_
19	abbiamo	avere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	20	aux	_	_
20	perso	perdere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	17	conj	_	_
21	9-0	9-0	NUM	N	NumType=Card	20	dobj	_	_
22	qua	qua	ADV	B	_	21	advmod	_	_
23	in	in	ADP	E	_	25	case	_	_
24	questa	questo	DET	DD	Gender=Fem|Number=Sing|PronType=Dem	25	det	_	_
25	partita	partita	NOUN	S	Gender=Fem|Number=Sing	21	nmod	_	_
26	altro	altro	ADJ	A	Gender=Masc|Number=Sing	25	amod	_	_
27	che	che	PRON	PR	PronType=Rel	28	nsubj	_	_
28	tristezza	tristezza	NOUN	S	Gender=Fem|Number=Sing	25	acl:relcl	_	_
29	come	come	ADP	E	_	31	case	_	_
30	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	31	det	_	_
31	altre	altro	PRON	PI	Gender=Fem|Number=Plur|PronType=Ind	28	nmod	_	_
32	e	e	CONJ	CC	_	31	cc	_	_
33	certi	certo	DET	DI	Gender=Masc|Number=Plur|PronType=Ind	34	det	_	_
34	si	si	PRON	PC	Person=3|PronType=Clit	36	dobj	_	_
35	sono	essere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	36	aux	_	_
36	messi	mettere	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	31	conj	_	_
37	a	a	ADP	E	_	38	mark	_	_
38	piangere	piangere	VERB	V	VerbForm=Inf	36	xcomp	_	SpaceAfter=No
39	.	.	PUNCT	FS	_	17	punct	_	_

# sent_id = 6
# text = Ma sembravano così debolucci.
1	Ma	ma	CONJ	CC	_	4	cc	_	_
2	sembravano	sembrare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	4	cop	_	_
3	così	così	ADV	B	_	4	advmod	_	_
4	debolucci	deboluccio	ADJ	A	Number=Plur	0	root	_	SpaceAfter=No
5	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 7
# text = Ma perdere 9-0, non melavevo mai immaginato di perdere così tanto.
1	Ma	ma	CONJ	CC	_	2	cc	_	_
2	perdere	perdere	VERB	V	VerbForm=Inf	0	root	_	_
3	9-	9-	SYM	X	_	2	dobj	_	SpaceAfter=No
4	0	0	NUM	N	NumType=Card	3	nummod	_	SpaceAfter=No
5	,	,	PUNCT	FF	_	3	punct	_	_
6	non	non	ADV	BN	PronType=Neg	9	neg	_	_
7	melavevo	melavere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	9	aux	_	_
8	mai	mai	ADV	B	_	9	advmod	_	_
9	immaginato	immaginare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	3	conj	_	_
10	di	di	ADP	E	_	11	mark	_	_
11	perdere	perdere	VERB	V	VerbForm=Inf	9	xcomp	_	_
12	così	così	ADV	B	_	13	advmod	_	_
13	tanto	tanto	ADV	B	_	11	advmod	_	SpaceAfter=No
14	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 8
# text = Quel giorno ho scoperto che non si deve mai sottovalutare l’avversario se non si sa la sua forza.
1	Quel	quello	DET	DD	Gender=Masc|Number=Sing|PronType=Dem	2	det	_	_
2	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	4	nmod	_	_
3	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	4	aux	_	_
4	scoperto	scoprire	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
5	che	che	SCONJ	CS	_	10	mark	_	_
6	non	non	ADV	BN	PronType=Neg	10	neg	_	_
7	si	si	PRON	PC	Person=3|PronType=Clit	10	expl:impers	_	_
8	deve	dovere	AUX	VM	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	10	aux	_	_
9	mai	mai	ADV	B	_	10	advmod	_	_
10	sottovalutare	sottovalutare	VERB	V	VerbForm=Inf	4	ccomp	_	_
11	l’avversario	l’avversario	NOUN	S	Gender=Masc|Number=Sing	10	dobj	_	_
12	se	se	SCONJ	CS	_	15	mark	_	_
13	non	non	ADV	BN	PronType=Neg	15	neg	_	_
14	si	si	PRON	PC	Person=3|PronType=Clit	15	expl:impers	_	_
15	sa	sapere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	10	advcl	_	_
16	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	18	det	_	_
17	sua	suo	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	18	det:poss	_	_
18	forza	forza	NOUN	S	Gender=Fem|Number=Sing	15	dobj	_	SpaceAfter=No
19	.	.	PUNCT	FS	_	4	punct	_	SpacesAfter=\r\n\r\n


# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE115.txt
# newpar
# sent_id = 1
# text = La storia degli insegnamenti Qualche sera fa, ero a casa avevo una fame incredibile.
1	La	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	2	det	_	_
2	storia	storia	NOUN	S	Gender=Fem|Number=Sing	10	nsubj	_	_
3-4	degli	_	_	_	_	_	_	_	_
3	di	di	ADP	E	_	5	case	_	_
4	gli	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	5	det	_	_
5	insegnamenti	insegnamento	NOUN	S	Gender=Masc|Number=Plur	2	nmod	_	SpacesAfter=\s\s
6	Qualche	qualche	DET	DI	Number=Sing|PronType=Ind	7	det	_	_
7	sera	sera	NOUN	S	Gender=Fem|Number=Sing	2	nmod	_	_
8	fa	fa	ADV	B	_	7	advmod	_	SpaceAfter=No
9	,	,	PUNCT	FF	_	2	punct	_	_
10	ero	essere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	0	root	_	_
11	a	a	ADP	E	_	12	case	_	_
12	casa	casa	NOUN	S	Gender=Fem|Number=Sing	10	nmod	_	_
13	avevo	avere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	10	root	_	_
14	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	15	det	_	_
15	fame	fame	NOUN	S	Gender=Fem|Number=Sing	13	dobj	_	_
16	incredibile	incredibile	ADJ	A	Number=Sing	15	amod	_	SpaceAfter=No
17	.	.	PUNCT	FS	_	13	punct	_	_

# sent_id = 2
# text = Aspettavo con ansia la cena non vedevo l’ora di addentare il mio pasto.
1	Aspettavo	aspettare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	0	root	_	_
2	con	con	ADP	E	_	3	case	_	_
3	ansia	ansia	NOUN	S	Gender=Fem|Number=Sing	1	nmod	_	_
4	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	5	det	_	_
5	cena	cena	NOUN	S	Gender=Fem|Number=Sing	7	nsubj	_	_
6	non	non	ADV	BN	PronType=Neg	7	neg	_	_
7	vedevo	vedere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	1	advcl	_	_
8	l’ora	l’ora	NOUN	S	Gender=Fem|Number=Sing	7	dobj	_	_
9	di	di	ADP	E	_	10	mark	_	_
10	addentare	addentare	VERB	V	VerbForm=Inf	8	acl	_	_
11	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	13	det	_	_
12	mio	mio	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	13	det:poss	_	_
13	pasto	pasto	NOUN	S	Gender=Masc|Number=Sing	10	dobj	_	SpaceAfter=No
14	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 3
# text = In seguito mia mamma finì di cucinare e ci chiamò a tavola, tutti si sedettero al tavolo e mangiammo.
1	In	in	ADP	E	_	2	case	_	_
2	seguito	seguito	NOUN	S	Gender=Masc|Number=Sing	5	nmod	_	_
3	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	4	det:poss	_	_
4	mamma	mamma	NOUN	S	Gender=Fem|Number=Sing	5	nsubj	_	_
5	finì	finire	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
6	di	di	ADP	E	_	7	mark	_	_
7	cucinare	cucinare	VERB	V	VerbForm=Inf	5	xcomp	_	_
8	e	e	CONJ	CC	_	5	cc	_	_
9	ci	ci	PRON	PC	Number=Plur|Person=1|PronType=Clit	10	dobj	_	_
10	chiamò	chiamare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	5	conj	_	_
11	a	a	ADP	E	_	12	case	_	_
12	tavola	tavola	NOUN	S	Gender=Fem|Number=Sing	10	nmod	_	SpaceAfter=No
13	,	,	PUNCT	FF	_	5	punct	_	_
14	tutti	tutto	PRON	PI	Gender=Masc|Number=Plur|PronType=Ind	16	nsubj	_	_
15	si	si	PRON	PC	Person=3|PronType=Clit	16	expl	_	_
16	sedettero	sedere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Past|VerbForm=Fin	5	conj	_	_
17-18	al	_	_	_	_	_	_	_	_
17	a	a	ADP	E	_	19	case	_	_
18	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	19	det	_	_
19	tavolo	tavolo	NOUN	S	Gender=Masc|Number=Sing	16	nmod	_	_
20	e	e	CONJ	CC	_	19	cc	_	_
21	mangiammo	mangiare	NOUN	S	Gender=Masc|Number=Sing	19	conj	_	SpaceAfter=No
22	.	.	PUNCT	FS	_	5	punct	_	_

# sent_id = 4
# text = A cena c’erano le crêpes dolci.
1	A	a	ADP	E	_	2	case	_	_
2	cena	cena	NOUN	S	Gender=Fem|Number=Sing	3	nmod	_	_
3	c’erano	c’erare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
4	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	5	det	_	_
5	crêpes	crêpes	NOUN	S	Gender=Fem|Number=Plur	3	nsubj	_	_
6	dolci	dolce	ADJ	A	Number=Plur	5	amod	_	SpaceAfter=No
7	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 5
# text = Ero il primo a finire di mangiare, e mio papà non aveva ancora finito la sua.
1	Ero	essere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	0	root	_	_
2	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	3	det	_	_
3	primo	primo	ADJ	NO	Gender=Masc|Number=Sing|NumType=Ord	1	nsubj	_	_
4	a	a	ADP	E	_	5	mark	_	_
5	finire	finire	VERB	V	VerbForm=Inf	1	xcomp	_	_
6	di	di	ADP	E	_	7	mark	_	_
7	mangiare	mangiare	VERB	V	VerbForm=Inf	5	xcomp	_	SpaceAfter=No
8	,	,	PUNCT	FF	_	1	punct	_	_
9	e	e	CONJ	CC	_	1	cc	_	_
10	mio	mio	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	11	det:poss	_	_
11	papà	papà	NOUN	S	Gender=Masc	15	nsubj	_	_
12	non	non	ADV	BN	PronType=Neg	15	neg	_	_
13	aveva	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	15	aux	_	_
14	ancora	ancora	ADV	B	_	15	advmod	_	_
15	finito	finire	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	1	conj	_	_
16	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	17	det	_	_
17	sua	suo	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	15	advmod	_	SpaceAfter=No
18	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 6
# text = Allora me ne diede metà.
1	Allora	allora	ADV	B	_	4	advmod	_	_
2	me	me	PRON	PC	Number=Sing|Person=1|PronType=Clit	4	iobj	_	_
3	ne	ne	PRON	PC	PronType=Clit	4	advmod	_	_
4	diede	dare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
5	metà	metà	NOUN	S	Gender=Fem	4	dobj	_	SpaceAfter=No
6	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 7
# text = Mia mamma mi ripeteva che non dovevo mangiare troppo veloce.
1	Mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	2	det:poss	_	_
2	mamma	mamma	NOUN	S	Gender=Fem|Number=Sing	4	nsubj	_	_
3	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	4	iobj	_	_
4	ripeteva	ripetere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
5	che	che	SCONJ	CS	_	8	mark	_	_
6	non	non	ADV	BN	PronType=Neg	8	neg	_	_
7	dovevo	dovere	AUX	VM	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	8	aux	_	_
8	mangiare	mangiare	VERB	V	VerbForm=Inf	4	ccomp	_	_
9	troppo	troppo	ADV	B	_	10	advmod	_	_
10	veloce	veloce	ADJ	A	Number=Sing	8	xcomp	_	SpaceAfter=No
11	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 8
# text = Io invece continuavo a mangiare.
1	Io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	3	nsubj	_	_
2	invece	invece	ADV	B	_	3	advmod	_	_
3	continuavo	continuare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	0	root	_	_
4	a	a	ADP	E	_	5	mark	_	_
5	mangiare	mangiare	VERB	V	VerbForm=Inf	3	xcomp	_	SpaceAfter=No
6	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 9
# text = Mia mamma mi chiese se volevo anche quella di mia sorella e solo per gola io risposi di sí.
1	Mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	2	det:poss	_	_
2	mamma	mamma	NOUN	S	Gender=Fem|Number=Sing	4	nsubj	_	_
3	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	4	dobj	_	_
4	chiese	chiedere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
5	se	se	SCONJ	CS	_	6	mark	_	_
6	volevo	volere	AUX	VM	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	4	advcl	_	_
7	anche	anche	ADV	B	_	8	advmod	_	_
8	quella	quello	PRON	PD	Gender=Fem|Number=Sing|PronType=Dem	6	nsubj	_	_
9	di	di	ADP	E	_	11	case	_	_
10	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	11	det:poss	_	_
11	sorella	sorella	NOUN	S	Gender=Fem|Number=Sing	8	nmod	_	_
12	e	e	CONJ	CC	_	8	cc	_	_
13	solo	solo	ADV	B	_	15	advmod	_	_
14	per	per	ADP	E	_	15	case	_	_
15	gola	golare	NOUN	S	Gender=Fem|Number=Plur	17	nmod	_	_
16	io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	17	nsubj	_	_
17	risposi	risposi	NOUN	S	Gender=Masc|Number=Plur	8	conj	_	_
18	di	di	ADP	E	_	19	case	_	_
19	sí.	sí.	NOUN	S	Gender=Masc|Number=Sing	17	nmod	_	_

# sent_id = 10
# text = Poi ero pienissimo, ed era ora di dormire.
1	Poi	poi	ADV	B	_	2	advmod	_	_
2	ero	essere	NOUN	S	Gender=Masc|Number=Sing	0	root	_	_
3	pienissimo	pieno	ADJ	A	Degree=Abs|Gender=Masc|Number=Sing	2	amod	_	SpaceAfter=No
4	,	,	PUNCT	FF	_	2	punct	_	_
5	ed	e	CONJ	CC	_	2	cc	_	_
6	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	7	cop	_	_
7	ora	ora	ADV	B	_	9	advmod	_	_
8	di	di	ADP	E	_	9	mark	_	_
9	dormire	dormire	VERB	V	VerbForm=Inf	2	conj	_	SpaceAfter=No
10	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 11
# text = Mi buttai nel mio caldo letto e cercai di dormire ma mi faceva male la pancia, anzi molto male e non riuscivo a dormire.
1	Mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	2	dobj	_	_
2	buttai	buttare	VERB	V	Mood=Ind|Number=Sing|Person=2|Tense=Fut|VerbForm=Fin	0	root	_	_
3-4	nel	_	_	_	_	_	_	_	_
3	in	in	ADP	E	_	6	case	_	_
4	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	6	det	_	_
5	mio	mio	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	6	det:poss	_	_
6	caldo	caldo	NOUN	S	Gender=Masc|Number=Sing	2	nmod	_	_
7	letto	leggere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	6	acl	_	_
8	e	e	CONJ	CC	_	7	cc	_	_
9	cercai	cercare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Past|VerbForm=Fin	7	conj	_	_
10	di	di	ADP	E	_	11	mark	_	_
11	dormire	dormire	VERB	V	VerbForm=Inf	9	xcomp	_	_
12	ma	ma	CONJ	CC	_	2	cc	_	_
13	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	14	iobj	_	_
14	faceva	fare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	2	conj	_	_
15	male	male	ADV	B	_	14	advmod	_	_
16	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	17	det	_	_
17	pancia	pancia	NOUN	S	Gender=Fem|Number=Sing	14	dobj	_	SpaceAfter=No
18	,	,	PUNCT	FF	_	14	punct	_	_
19	anzi	anzi	ADV	B	_	14	advmod	_	_
20	molto	molto	ADV	B	_	21	advmod	_	_
21	male	male	ADV	B	_	19	advmod	_	_
22	e	e	CONJ	CC	_	21	cc	_	_
23	non	non	ADV	BN	PronType=Neg	24	neg	_	_
24	riuscivo	riuscire	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	21	conj	_	_
25	a	a	ADP	E	_	26	mark	_	_
26	dormire	dormire	VERB	V	VerbForm=Inf	24	xcomp	_	SpaceAfter=No
27	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 12
# text = Mi addormentai piú tardi; il giorno dopo mi svegliai, ma ero ancora stanchissimo.
1	Mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	2	expl	_	_
2	addormentai	addormentare	VERB	V	Mood=Ind|Number=Sing|Person=2|Tense=Fut|VerbForm=Fin	0	root	_	_
3	piú	piú	PROPN	SP	_	2	nsubj	_	_
4	tardi	tardi	ADV	B	_	3	advmod	_	SpaceAfter=No
5	;	;	PUNCT	FC	_	2	punct	_	_
6	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	7	det	_	_
7	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	2	nmod	_	_
8	dopo	dopo	ADV	B	_	10	advmod	_	_
9	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	10	expl	_	_
10	svegliai	svegliare	VERB	V	Mood=Ind|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	7	acl	_	SpaceAfter=No
11	,	,	PUNCT	FF	_	7	punct	_	_
12	ma	ma	CONJ	CC	_	7	cc	_	_
13	ero	essere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	7	conj	_	_
14	ancora	ancora	ADV	B	_	15	advmod	_	_
15	stanchissimo	stancho	ADJ	A	Degree=Abs|Gender=Masc|Number=Sing	13	xcomp	_	SpaceAfter=No
16	.	.	PUNCT	FS	_	2	punct	_	SpacesAfter=\s\s

# sent_id = 13
# text = Da quella sera ho imparato di: non mangiare veloce, o per gola e andare presto a letto se il giorno seguente c’é scuola, per evitare di arrivare in ritardo e essere stanchi.
1	Da	da	ADP	E	_	3	case	_	_
2	quella	quello	DET	DD	Gender=Fem|Number=Sing|PronType=Dem	3	det	_	_
3	sera	sera	NOUN	S	Gender=Fem|Number=Sing	5	nmod	_	_
4	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	imparato	imparare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
6	di	di	ADP	E	_	9	mark	_	SpaceAfter=No
7	:	:	PUNCT	FC	_	9	punct	_	_
8	non	non	ADV	BN	PronType=Neg	9	neg	_	_
9	mangiare	mangiare	VERB	V	VerbForm=Inf	5	xcomp	_	_
10	veloce	veloce	ADJ	A	Number=Sing	9	xcomp	_	SpaceAfter=No
11	,	,	PUNCT	FF	_	9	punct	_	_
12	o	o	CONJ	CC	_	9	cc	_	_
13	per	per	ADP	E	_	14	case	_	_
14	gola	gola	NOUN	S	Gender=Fem|Number=Plur	9	conj	_	_
15	e	e	CONJ	CC	_	14	cc	_	_
16	andare	andare	VERB	V	VerbForm=Inf	14	conj	_	_
17	presto	presto	ADV	B	_	16	advmod	_	_
18	a	a	ADP	E	_	19	case	_	_
19	letto	leggere	NOUN	S	Gender=Masc|Number=Sing	16	nmod	_	_
20	se	se	SCONJ	CS	_	24	mark	_	_
21	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	22	det	_	_
22	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	24	nmod	_	_
23	seguente	seguente	ADJ	A	Number=Sing	22	amod	_	_
24	c’é	c’é	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	16	advcl	_	_
25	scuola	scuola	NOUN	S	Gender=Fem|Number=Sing	24	dobj	_	SpaceAfter=No
26	,	,	PUNCT	FF	_	24	punct	_	_
27	per	per	ADP	E	_	28	mark	_	_
28	evitare	evitare	VERB	V	VerbForm=Inf	24	advcl	_	_
29	di	di	ADP	E	_	30	mark	_	_
30	arrivare	arrivare	VERB	V	VerbForm=Inf	28	xcomp	_	_
31	in	in	ADP	E	_	32	case	_	_
32	ritardo	ritardo	NOUN	S	Gender=Masc|Number=Sing	30	nmod	_	_
33	e	e	CONJ	CC	_	28	cc	_	_
34	essere	essere	VERB	V	VerbForm=Inf	35	cop	_	_
35	stanchi	stancare	ADJ	A	Gender=Masc|Number=Plur	28	conj	_	SpaceAfter=No
36	.	.	PUNCT	FS	_	9	punct	_	SpacesAfter=\r\n\r\n


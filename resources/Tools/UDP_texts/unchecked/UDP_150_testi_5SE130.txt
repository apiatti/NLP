# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE130.txt
# newpar
# sent_id = 1
# text = Un giorno la mia maestra ha cominciato ha parlare e a insegnarci i verbi.
1	Un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	7	nmod	_	_
3	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	5	det	_	_
4	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	5	det:poss	_	_
5	maestra	maestra	NOUN	S	Gender=Fem|Number=Sing	7	nsubj	_	_
6	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	7	aux	_	_
7	cominciato	cominciare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
8	ha	avere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	9	aux	_	_
9	parlare	parlare	VERB	V	VerbForm=Inf	7	xcomp	_	_
10	e	e	CONJ	CC	_	9	cc	_	_
11	a	a	ADP	E	_	12	mark	_	_
12	insegnarci	insegnarco	VERB	V	VerbForm=Inf	9	conj	_	_
13	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	14	det	_	_
14	verbi	verbo	NOUN	S	Gender=Masc|Number=Plur	12	dobj	_	SpaceAfter=No
15	.	.	PUNCT	FS	_	7	punct	_	_

# sent_id = 2
# text = Abbiamo parlato di essere e avere di pronomi ecc., dopo aver parlato, spiegato e ripetuto tante volte, la maestra ci ha detto di studiare i verbi, cioè essere e avere.
1	Abbiamo	avere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	2	aux	_	_
2	parlato	parlare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	25	advcl	_	_
3	di	di	ADP	E	_	4	mark	_	_
4	essere	essere	VERB	V	VerbForm=Inf	2	xcomp	_	_
5	e	e	CONJ	CC	_	4	cc	_	_
6	avere	avere	VERB	V	VerbForm=Inf	4	conj	_	_
7	di	di	ADP	E	_	8	case	_	_
8	pronomi	pronome	NOUN	S	Gender=Masc|Number=Plur	6	nmod	_	_
9	ecc.	eccetera	NOUN	S	_	8	compound	_	SpaceAfter=No
10	,	,	PUNCT	FF	_	6	punct	_	_
11	dopo	dopo	SCONJ	CS	_	13	mark	_	_
12	aver	avere	AUX	VA	VerbForm=Inf	13	aux	_	_
13	parlato	parlare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	6	advcl	_	SpaceAfter=No
14	,	,	PUNCT	FF	_	13	punct	_	_
15	spiegato	spiegare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	13	conj	_	_
16	e	e	CONJ	CC	_	13	cc	_	_
17	ripetuto	ripetere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	13	conj	_	SpacesAfter=\s\s\s
18	tante	tanto	DET	DI	Gender=Fem|Number=Plur|PronType=Ind	19	det	_	_
19	volte	volta	NOUN	S	Gender=Fem|Number=Plur	17	nmod	_	SpaceAfter=No
20	,	,	PUNCT	FF	_	2	punct	_	_
21	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	22	det	_	_
22	maestra	maestra	NOUN	S	Gender=Fem|Number=Sing	25	nsubj	_	_
23	ci	ci	PRON	PC	PronType=Clit	25	advmod	_	_
24	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	25	aux	_	_
25	detto	dire	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
26	di	di	ADP	E	_	27	mark	_	_
27	studiare	studiare	VERB	V	VerbForm=Inf	25	xcomp	_	_
28	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	29	det	_	_
29	verbi	verbo	NOUN	S	Gender=Masc|Number=Plur	27	dobj	_	SpaceAfter=No
30	,	,	PUNCT	FF	_	25	punct	_	_
31	cioè	cioè	CONJ	CC	_	25	cc	_	_
32	essere	essere	VERB	V	VerbForm=Inf	25	conj	_	_
33	e	e	CONJ	CC	_	25	cc	_	_
34	avere	avere	VERB	V	VerbForm=Inf	25	conj	_	SpaceAfter=No
35	.	.	PUNCT	FS	_	25	punct	_	_

# sent_id = 3
# text = Noi abbiamo studiato e anch’io ma non abbastanza e dopo aver studiato abbiamo fatto la verifica dei verbi.
1	Noi	noi	PRON	PE	Number=Plur|Person=1|PronType=Prs	3	dobj	_	_
2	abbiamo	avere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	3	aux	_	_
3	studiato	studiare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
4	e	e	CONJ	CC	_	3	cc	_	_
5	anch’io	anch’io	ADV	B	_	3	conj	_	_
6	ma	ma	CONJ	CC	_	3	cc	_	_
7	non	non	ADV	BN	PronType=Neg	8	neg	_	_
8	abbastanza	abbastanza	ADV	B	_	3	conj	_	_
9	e	e	CONJ	CC	_	8	cc	_	_
10	dopo	dopo	SCONJ	CS	_	12	mark	_	_
11	aver	avere	AUX	VA	VerbForm=Inf	12	aux	_	_
12	studiato	studiare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	14	advcl	_	_
13	abbiamo	avere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	14	aux	_	_
14	fatto	fare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	8	conj	_	_
15	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	16	det	_	_
16	verifica	verifica	NOUN	S	Gender=Fem|Number=Sing	14	dobj	_	_
17-18	dei	_	_	_	_	_	_	_	_
17	di	di	ADP	E	_	19	case	_	_
18	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	19	det	_	_
19	verbi	verbo	NOUN	S	Gender=Masc|Number=Plur	16	nmod	_	SpaceAfter=No
20	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 4
# text = Non mi è andata così bene;
1	Non	non	ADV	BN	PronType=Neg	4	neg	_	_
2	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	4	iobj	_	_
3	è	essere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	4	aux	_	_
4	andata	andare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
5	così	così	ADV	B	_	6	advmod	_	_
6	bene	bene	ADV	B	_	4	advmod	_	SpaceAfter=No
7	;	;	PUNCT	FC	_	4	punct	_	_

# sent_id = 5
# text = poi abbiamo cominciato a parlare di are, ere, ire.
1	poi	poi	ADV	B	_	3	advmod	_	_
2	abbiamo	avere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	3	aux	_	_
3	cominciato	cominciare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
4	a	a	ADP	E	_	5	mark	_	_
5	parlare	parlare	VERB	V	VerbForm=Inf	3	xcomp	_	_
6	di	di	ADP	E	_	7	mark	_	_
7	are	are	VERB	V	VerbForm=Inf	5	xcomp	_	SpaceAfter=No
8	,	,	PUNCT	FF	_	7	punct	_	_
9	ere	ere	VERB	V	VerbForm=Inf	7	conj	_	SpaceAfter=No
10	,	,	PUNCT	FF	_	7	punct	_	_
11	ire	ire	VERB	V	VerbForm=Inf	7	conj	_	SpaceAfter=No
12	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 6
# text = Io questa volta sono stata attenta e o capito, poi ancora una volta ci ha detto di studiare e ho studiato tanto, tanto.
1	Io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	6	nsubjpass	_	_
2	questa	questo	DET	DD	Gender=Fem|Number=Sing|PronType=Dem	3	det	_	_
3	volta	volta	NOUN	S	Gender=Fem|Number=Sing	6	nsubjpass	_	_
4	sono	essere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	6	aux	_	_
5	stata	essere	AUX	VA	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	6	auxpass	_	_
6	attenta	attentire	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
7	e	e	CONJ	CC	_	6	cc	_	_
8	o	o	CONJ	CC	_	6	cc	_	_
9	capito	capire	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	6	conj	_	SpaceAfter=No
10	,	,	PUNCT	FF	_	9	punct	_	_
11	poi	poi	ADV	B	_	17	advmod	_	_
12	ancora	ancora	ADV	B	_	17	advmod	_	_
13	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	14	det	_	_
14	volta	volta	NOUN	S	Gender=Fem|Number=Sing	17	nmod	_	_
15	ci	ci	PRON	PC	PronType=Clit	17	advmod	_	_
16	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	17	aux	_	_
17	detto	dire	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	9	conj	_	_
18	di	di	ADP	E	_	19	mark	_	_
19	studiare	studiare	VERB	V	VerbForm=Inf	17	xcomp	_	_
20	e	e	CONJ	CC	_	19	cc	_	_
21	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	22	aux	_	_
22	studiato	studiare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	19	conj	_	_
23	tanto	tanto	ADV	B	_	22	advmod	_	SpaceAfter=No
24	,	,	PUNCT	FF	_	22	punct	_	_
25	tanto	tanto	ADV	B	_	22	advmod	_	SpaceAfter=No
26	.	.	PUNCT	FS	_	6	punct	_	_

# sent_id = 7
# text = Ci ha fatto la verifica ma ad un certo punto c’erano anche i verbi di avere e essere e io non mi li ricordavo più!
1	Ci	ci	PRON	PC	PronType=Clit	3	advmod	_	_
2	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	3	aux	_	_
3	fatto	fare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
4	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	5	det	_	_
5	verifica	verifica	NOUN	S	Gender=Fem|Number=Sing	3	dobj	_	_
6	ma	ma	CONJ	CC	_	5	cc	_	_
7	ad	a	ADP	E	_	10	case	_	_
8	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	10	det	_	_
9	certo	certo	ADJ	A	Gender=Masc|Number=Sing	10	amod	_	_
10	punto	punto	NOUN	S	Gender=Masc|Number=Sing	5	conj	_	_
11	c’erano	c’erare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	3	advcl	_	_
12	anche	anche	ADV	B	_	14	advmod	_	_
13	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	14	det	_	_
14	verbi	verbo	NOUN	S	Gender=Masc|Number=Plur	11	dobj	_	_
15	di	di	ADP	E	_	16	mark	_	_
16	avere	avere	VERB	V	VerbForm=Inf	14	acl	_	_
17	e	e	CONJ	CC	_	16	cc	_	_
18	essere	essere	VERB	V	VerbForm=Inf	16	conj	_	_
19	e	e	CONJ	CC	_	11	cc	_	_
20	io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	24	nsubj	_	_
21	non	non	ADV	BN	PronType=Neg	24	neg	_	_
22	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	24	iobj	_	_
23	li	li	PRON	PC	Gender=Masc|Number=Plur|Person=3|PronType=Clit	24	dobj	_	_
24	ricordavo	ricordare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	11	conj	_	_
25	più	più	ADV	B	_	24	advmod	_	SpaceAfter=No
26	!	!	PUNCT	FS	_	3	punct	_	_

# sent_id = 8
# text = Perché avevo pensato che non c’era più bisogno di sapere a memoria, “studiare” e poi basta!
1	Perché	perché	ADV	B	_	3	advmod	_	_
2	avevo	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	3	aux	_	_
3	pensato	pensare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
4	che	che	SCONJ	CS	_	6	mark	_	_
5	non	non	ADV	BN	PronType=Neg	6	neg	_	_
6	c’era	c’erare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	3	ccomp	_	_
7	più	più	ADV	B	_	6	advmod	_	_
8	bisogno	bisogno	NOUN	S	Gender=Masc|Number=Sing	6	dobj	_	_
9	di	di	ADP	E	_	10	mark	_	_
10	sapere	sapere	VERB	V	VerbForm=Inf	8	acl	_	_
11	a	a	ADP	E	_	12	case	_	_
12	memoria	memoria	NOUN	S	Gender=Fem|Number=Sing	10	nmod	_	SpaceAfter=No
13	,	,	PUNCT	FF	_	6	punct	_	_
14	“	“	PUNCT	FB	_	15	punct	_	SpaceAfter=No
15	studiare	studiare	VERB	V	VerbForm=Inf	6	advcl	_	SpaceAfter=No
16	”	”	PUNCT	FB	_	15	punct	_	_
17	e	e	CONJ	CC	_	15	cc	_	_
18	poi	poi	ADV	B	_	19	advmod	_	_
19	basta	bastare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	15	conj	_	SpaceAfter=No
20	!	!	PUNCT	FS	_	3	punct	_	_

# sent_id = 9
# text = Ma no bisogna sempe studiare a memoria e capire!
1	Ma	ma	CONJ	CC	_	3	cc	_	_
2	no	no	ADV	BN	PronType=Neg	3	neg	_	_
3	bisogna	bisognare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
4	sempe	sempo	ADJ	A	Gender=Fem|Number=Plur	3	xcomp	_	_
5	studiare	studiare	VERB	V	VerbForm=Inf	3	ccomp	_	_
6	a	a	ADP	E	_	7	case	_	_
7	memoria	memoria	NOUN	S	Gender=Fem|Number=Sing	5	nmod	_	_
8	e	e	CONJ	CC	_	5	cc	_	_
9	capire	capire	VERB	V	VerbForm=Inf	5	conj	_	SpaceAfter=No
10	!	!	PUNCT	FS	_	3	punct	_	_

# sent_id = 10
# text = E anche questa volta mi é andato male!!
1	E	e	CONJ	CC	_	6	cc	_	_
2	anche	anche	ADV	B	_	4	advmod	_	_
3	questa	questo	DET	DD	Gender=Fem|Number=Sing|PronType=Dem	4	det	_	_
4	volta	volta	NOUN	S	Gender=Fem|Number=Sing	6	nmod	_	_
5	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	6	expl	_	_
6	é	é	PRON	PP	Number=Sing|Poss=Yes|PronType=Prs	0	root	_	_
7	andato	andare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	6	advcl	_	_
8	male!	male!	PUNCT	FS	_	6	punct	_	SpaceAfter=No
9	!	!	PUNCT	FS	_	6	punct	_	_

# sent_id = 11
# text = Ero molto triste perché pensavo almeno questa volta mi sarebbe andata bene, ma invece no!
1	Ero	essere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	3	cop	_	_
2	molto	molto	ADV	B	_	3	advmod	_	_
3	triste	triste	ADJ	A	Number=Sing	0	root	_	_
4	perché	perché	SCONJ	CS	_	5	mark	_	_
5	pensavo	pensare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	3	advcl	_	_
6	almeno	almeno	ADV	B	_	11	advmod	_	_
7	questa	questo	DET	DD	Gender=Fem|Number=Sing|PronType=Dem	8	det	_	_
8	volta	volta	NOUN	S	Gender=Fem|Number=Sing	11	nmod	_	_
9	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	11	iobj	_	_
10	sarebbe	essere	AUX	VA	Mood=Cnd|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	11	aux	_	_
11	andata	andare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	5	xcomp	_	_
12	bene	bene	ADV	B	_	11	advmod	_	SpaceAfter=No
13	,	,	PUNCT	FF	_	11	punct	_	_
14	ma	ma	CONJ	CC	_	11	cc	_	_
15	invece	invece	ADV	B	_	16	advmod	_	_
16	no	no	ADV	BN	PronType=Neg	11	conj	_	SpaceAfter=No
17	!	!	PUNCT	FS	_	3	punct	_	_

# sent_id = 12
# text = Il giorno dopo siamo rientrati in classe e abbiamo cominciato a parlare sempre di “sentire”, “mangiare”, “ridere”, ma anche di altri nomi con dentro are, ere, ìre.
1	Il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	5	nmod	_	_
3	dopo	dopo	ADV	B	_	2	advmod	_	_
4	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	rientrati	rientrare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	0	root	_	_
6	in	in	ADP	E	_	7	case	_	_
7	classe	classe	NOUN	S	Gender=Fem|Number=Sing	5	nmod	_	_
8	e	e	CONJ	CC	_	5	cc	_	_
9	abbiamo	avere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	10	aux	_	_
10	cominciato	cominciare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	5	conj	_	_
11	a	a	ADP	E	_	12	mark	_	_
12	parlare	parlare	VERB	V	VerbForm=Inf	10	xcomp	_	_
13	sempre	sempre	ADV	B	_	12	advmod	_	_
14	di	di	ADP	E	_	16	mark	_	_
15	“	“	PUNCT	FB	_	16	punct	_	SpaceAfter=No
16	sentire	sentire	VERB	V	VerbForm=Inf	13	ccomp	_	SpaceAfter=No
17	”	”	PUNCT	FB	_	16	punct	_	SpaceAfter=No
18	,	,	PUNCT	FF	_	16	punct	_	_
19	“	“	PUNCT	FB	_	20	punct	_	SpaceAfter=No
20	mangiare	mangiare	VERB	V	VerbForm=Inf	16	conj	_	SpaceAfter=No
21	”	”	PUNCT	FB	_	20	punct	_	SpaceAfter=No
22	,	,	PUNCT	FF	_	20	punct	_	_
23	“	“	PUNCT	FB	_	24	punct	_	SpaceAfter=No
24	ridere	ridere	VERB	V	VerbForm=Inf	20	parataxis	_	SpaceAfter=No
25	”	”	PUNCT	FB	_	24	punct	_	SpaceAfter=No
26	,	,	PUNCT	FF	_	24	punct	_	_
27	ma	ma	CONJ	CC	_	24	cc	_	_
28	anche	anche	ADV	B	_	31	advmod	_	_
29	di	di	ADP	E	_	31	case	_	SpacesAfter=\s\s
30	altri	altro	ADJ	A	Gender=Masc|Number=Plur	31	amod	_	_
31	nomi	nome	NOUN	S	Gender=Masc|Number=Plur	24	conj	_	_
32	con	con	ADP	E	_	34	mark	_	_
33	dentro	dentro	ADV	B	_	34	advmod	_	_
34	are	are	VERB	V	VerbForm=Inf	31	acl	_	SpaceAfter=No
35	,	,	PUNCT	FF	_	24	punct	_	_
36	ere	ere	VERB	V	VerbForm=Inf	24	conj	_	SpaceAfter=No
37	,	,	PUNCT	FF	_	24	punct	_	_
38	ìre	ìre	VERB	V	VerbForm=Inf	24	conj	_	SpaceAfter=No
39	.	.	PUNCT	FS	_	5	punct	_	_

# sent_id = 13
# text = Abbiamo parlato tanto, tanto e anche questa volta ho capito, la maestra ci ha dato da studiare e io ho studiato tanto;
1	Abbiamo	avere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	2	aux	_	_
2	parlato	parlare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	17	advcl	_	_
3	tanto	tanto	ADV	B	_	2	advmod	_	SpaceAfter=No
4	,	,	PUNCT	FF	_	2	punct	_	_
5	tanto	tanto	ADV	B	_	2	advmod	_	_
6	e	e	CONJ	CC	_	5	cc	_	_
7	anche	anche	ADV	B	_	9	advmod	_	_
8	questa	questo	DET	DD	Gender=Fem|Number=Sing|PronType=Dem	9	det	_	_
9	volta	volta	NOUN	S	Gender=Fem|Number=Sing	11	nmod	_	_
10	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	11	aux	_	_
11	capito	capire	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	5	conj	_	SpaceAfter=No
12	,	,	PUNCT	FF	_	2	punct	_	_
13	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	14	det	_	_
14	maestra	maestra	NOUN	S	Gender=Fem|Number=Sing	17	nsubj	_	_
15	ci	ci	PRON	PC	PronType=Clit	17	advmod	_	_
16	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	17	aux	_	_
17	dato	dare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
18	da	da	ADP	E	_	19	mark	_	_
19	studiare	studiare	VERB	V	VerbForm=Inf	17	xcomp	_	_
20	e	e	CONJ	CC	_	19	cc	_	_
21	io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	23	nsubj	_	_
22	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	23	aux	_	_
23	studiato	studiare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	19	conj	_	_
24	tanto	tanto	ADV	B	_	23	advmod	_	SpaceAfter=No
25	;	;	PUNCT	FC	_	17	punct	_	_

# sent_id = 14
# text = anche però i verbi cioé essere e avere molto bene anche quelli della scorsa volta di ire, ere, are.
1	anche	anche	ADV	B	_	2	advmod	_	_
2	però	però	ADV	B	_	0	root	_	_
3	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	4	det	_	_
4	verbi	verbo	NOUN	S	Gender=Masc|Number=Plur	2	nsubj	_	_
5	cioé	cioé	SCONJ	CS	_	6	mark	_	_
6	essere	essere	VERB	V	VerbForm=Inf	4	acl	_	_
7	e	e	CONJ	CC	_	6	cc	_	_
8	avere	avere	VERB	V	VerbForm=Inf	6	conj	_	_
9	molto	molto	ADV	B	_	10	advmod	_	_
10	bene	bene	ADV	B	_	8	advmod	_	_
11	anche	anche	ADV	B	_	12	advmod	_	_
12	quelli	quello	PRON	PD	Gender=Masc|Number=Plur|PronType=Dem	8	dobj	_	_
13-14	della	_	_	_	_	_	_	_	_
13	di	di	ADP	E	_	16	case	_	_
14	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	16	det	_	_
15	scorsa	scorso	ADJ	A	Gender=Fem|Number=Sing	16	amod	_	_
16	volta	volta	NOUN	S	Gender=Fem|Number=Sing	12	nmod	_	_
17	di	di	ADP	E	_	18	mark	_	_
18	ire	ire	VERB	V	VerbForm=Inf	16	acl	_	SpaceAfter=No
19	,	,	PUNCT	FF	_	18	punct	_	_
20	ere	ere	VERB	V	VerbForm=Inf	18	conj	_	SpaceAfter=No
21	,	,	PUNCT	FF	_	18	punct	_	_
22	are	are	VERB	V	VerbForm=Inf	18	conj	_	SpaceAfter=No
23	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 15
# text = Abbiamo fatto la verifica e non ho avuto problemi e mi è andata bene, qualche errore l’ho fatto ma però questa volta ho capito!
1	Abbiamo	avere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	2	aux	_	_
2	fatto	fare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
3	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	4	det	_	_
4	verifica	verifica	NOUN	S	Gender=Fem|Number=Sing	2	dobj	_	_
5	e	e	CONJ	CC	_	2	cc	_	_
6	non	non	ADV	BN	PronType=Neg	8	neg	_	_
7	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	8	aux	_	_
8	avuto	avere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	2	conj	_	_
9	problemi	problema	NOUN	S	Gender=Masc|Number=Plur	8	dobj	_	_
10	e	e	CONJ	CC	_	2	cc	_	_
11	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	13	iobj	_	_
12	è	essere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	13	aux	_	_
13	andata	andare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	2	conj	_	_
14	bene	bene	ADV	B	_	13	advmod	_	SpaceAfter=No
15	,	,	PUNCT	FF	_	13	punct	_	_
16	qualche	qualche	DET	DI	Number=Sing|PronType=Ind	17	det	_	_
17	errore	errore	NOUN	S	Gender=Masc|Number=Sing	19	nsubj	_	_
18	l’ho	l’avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	19	aux	_	_
19	fatto	fare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	13	conj	_	_
20	ma	ma	CONJ	CC	_	13	cc	_	_
21	però	però	ADV	B	_	25	advmod	_	_
22	questa	questo	DET	DD	Gender=Fem|Number=Sing|PronType=Dem	23	det	_	_
23	volta	volta	NOUN	S	Gender=Fem|Number=Sing	25	nmod	_	_
24	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	25	aux	_	_
25	capito	capire	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	13	conj	_	SpaceAfter=No
26	!	!	PUNCT	FS	_	2	punct	_	_

# sent_id = 16
# text = È per questo non si gnifica che se ho studiato ma non ho ascoltato in classe e mi e andata bene la verifica e dopo basta perché non ho bisogno più dei verbi!
1	È	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
2	per	per	ADP	E	_	3	case	_	_
3	questo	questo	PRON	PD	Gender=Masc|Number=Sing|PronType=Dem	1	nmod	_	_
4	non	non	ADV	BN	PronType=Neg	6	neg	_	_
5	si	si	PRON	PC	Person=3|PronType=Clit	6	expl:impers	_	_
6	gnifica	gnificare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	1	advcl	_	_
7	che	che	SCONJ	CS	_	6	dobj	_	_
8	se	se	SCONJ	CS	_	10	mark	_	_
9	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	10	aux	_	_
10	studiato	studiare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	7	advcl	_	_
11	ma	ma	CONJ	CC	_	10	cc	_	_
12	non	non	ADV	BN	PronType=Neg	14	neg	_	_
13	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	14	aux	_	_
14	ascoltato	ascoltare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	10	conj	_	_
15	in	in	ADP	E	_	16	case	_	_
16	classe	classe	NOUN	S	Gender=Fem|Number=Sing	14	nmod	_	_
17	e	e	CONJ	CC	_	10	cc	_	_
18	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	10	conj	_	_
19	e	e	CONJ	CC	_	18	cc	_	_
20	andata	andare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	18	conj	_	_
21	bene	bene	ADV	B	_	20	advmod	_	_
22	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	23	det	_	_
23	verifica	verifica	NOUN	S	Gender=Fem|Number=Sing	20	dobj	_	_
24	e	e	CONJ	CC	_	10	cc	_	_
25	dopo	dopo	ADV	B	_	26	advmod	_	_
26	basta	bastare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	10	conj	_	_
27	perché	perché	SCONJ	CS	_	29	mark	_	_
28	non	non	ADV	BN	PronType=Neg	29	neg	_	_
29	ho	avere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	26	advcl	_	_
30	bisogno	bisogno	NOUN	S	Gender=Masc|Number=Sing	29	dobj	_	_
31	più	più	ADV	B	_	34	advmod	_	_
32-33	dei	_	_	_	_	_	_	_	_
32	di	di	ADP	E	_	34	case	_	_
33	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	34	det	_	_
34	verbi	verbo	NOUN	S	Gender=Masc|Number=Plur	30	nmod	_	SpaceAfter=No
35	!	!	PUNCT	FS	_	1	punct	_	_

# sent_id = 17
# text = Invece no si che ne avrò bisogno e molto!
1	Invece	invece	ADV	B	_	2	advmod	_	_
2	no	no	ADV	BN	PronType=Neg	0	root	_	_
3	si	si	PRON	PC	Person=3|PronType=Clit	2	expl	_	_
4	che	che	PRON	PR	PronType=Rel	6	nsubj	_	_
5	ne	ne	PRON	PC	PronType=Clit	6	advmod	_	_
6	avrò	avere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Fut|VerbForm=Fin	3	acl:relcl	_	_
7	bisogno	bisogno	NOUN	S	Gender=Masc|Number=Sing	6	dobj	_	_
8	e	e	CONJ	CC	_	7	cc	_	_
9	molto	molto	ADV	B	_	7	conj	_	SpaceAfter=No
10	!	!	PUNCT	FS	_	2	punct	_	_

# sent_id = 18
# text = E per questo studiare non per studiare e poi basta, ma per imparare e metterle nella zucca!!
1	E	e	CONJ	CC	_	4	cc	_	_
2	per	per	ADP	E	_	3	case	_	_
3	questo	questo	PRON	PD	Gender=Masc|Number=Sing|PronType=Dem	4	nmod	_	_
4	studiare	studiare	VERB	V	VerbForm=Inf	0	root	_	_
5	non	non	ADV	BN	PronType=Neg	7	neg	_	_
6	per	per	ADP	E	_	7	mark	_	_
7	studiare	studiare	VERB	V	VerbForm=Inf	4	advcl	_	_
8	e	e	CONJ	CC	_	4	cc	_	_
9	poi	poi	ADV	B	_	10	advmod	_	_
10	basta	bastare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	4	conj	_	SpaceAfter=No
11	,	,	PUNCT	FF	_	4	punct	_	_
12	ma	ma	CONJ	CC	_	4	cc	_	_
13	per	per	ADP	E	_	14	mark	_	_
14	imparare	imparare	VERB	V	VerbForm=Inf	4	conj	_	_
15	e	e	CONJ	CC	_	14	cc	_	_
16	metterle	metterle	ADJ	A	Number=Sing	14	conj	_	_
17-18	nella	_	_	_	_	_	_	_	_
17	in	in	ADP	E	_	18	case	_	_
18	la	il	PRON	PC	Gender=Fem|Number=Sing|Person=3|PronType=Clit	16	nmod	_	_
19	zucca!	zucca!	PUNCT	FS	_	18	punct	_	SpaceAfter=No
20	!	!	PUNCT	FS	_	4	punct	_	SpacesAfter=\r\n\r\n


# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE110.txt
# newpar
# sent_id = 1
# text = Lo scivolo d’acqua Tanti anni fa sono andata a un compleanno di una mia amica di classe.
1	Lo	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	scivolo	scivolo	NOUN	S	Gender=Masc|Number=Sing	3	nsubj	_	_
3	d’acqua	d’acquare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	SpacesAfter=\s\s
4	Tanti	tanto	DET	DI	Gender=Masc|Number=Plur|PronType=Ind	5	det	_	_
5	anni	anno	NOUN	S	Gender=Masc|Number=Plur	8	nmod	_	_
6	fa	fa	ADV	B	_	5	advmod	_	_
7	sono	essere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	8	aux	_	_
8	andata	andare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	3	ccomp	_	_
9	a	a	ADP	E	_	11	case	_	_
10	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	11	det	_	_
11	compleanno	compleanno	NOUN	S	Gender=Masc|Number=Sing	8	nmod	_	_
12	di	di	ADP	E	_	15	case	_	_
13	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	15	det	_	_
14	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	15	det:poss	_	_
15	amica	amico	NOUN	S	Gender=Fem|Number=Sing	11	nmod	_	_
16	di	di	ADP	E	_	17	case	_	_
17	classe	classe	NOUN	S	Gender=Fem|Number=Sing	15	nmod	_	SpaceAfter=No
18	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 2
# text = Il compleanno si faceva vicino al lago dove c’era uno scivolo d’acqua.
1	Il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	compleanno	compleanno	NOUN	S	Gender=Masc|Number=Sing	4	nsubj	_	_
3	si	si	PRON	PC	Person=3|PronType=Clit	4	expl	_	_
4	faceva	fare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
5	vicino	vicino	ADV	B	_	8	case	_	_
6-7	al	_	_	_	_	_	_	_	_
6	a	a	ADP	E	_	5	mwe	_	_
7	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	8	det	_	_
8	lago	lago	NOUN	S	Gender=Masc|Number=Sing	4	nmod	_	_
9	dove	dove	ADV	B	_	10	advmod	_	_
10	c’era	c’erare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	8	acl:relcl	_	_
11	uno	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	12	det	_	_
12	scivolo	scivolo	NOUN	S	Gender=Masc|Number=Sing	10	dobj	_	_
13	d’acqua	d’acquo	ADV	B	_	12	advmod	_	SpaceAfter=No
14	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 3
# text = Allora prima abbiamo giocato, mangiato poi siamo andati nel lago a fare il bagno.
1	Allora	allora	ADV	B	_	4	advmod	_	_
2	prima	prima	ADV	B	_	4	advmod	_	_
3	abbiamo	avere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	4	aux	_	_
4	giocato	giocare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	SpaceAfter=No
5	,	,	PUNCT	FF	_	4	punct	_	_
6	mangiato	mangiare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	4	conj	_	_
7	poi	poi	ADV	B	_	6	advmod	_	_
8	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	9	aux	_	_
9	andati	andare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	6	xcomp	_	_
10-11	nel	_	_	_	_	_	_	_	_
10	in	in	ADP	E	_	12	case	_	_
11	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	12	det	_	_
12	lago	lago	NOUN	S	Gender=Masc|Number=Sing	9	nmod	_	_
13	a	a	ADP	E	_	14	mark	_	_
14	fare	fare	VERB	V	VerbForm=Inf	9	xcomp	_	_
15	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	16	det	_	_
16	bagno	bagno	NOUN	S	Gender=Masc|Number=Sing	14	dobj	_	SpaceAfter=No
17	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 4
# text = Siamo stati tanto tempo dentro il lago.
1	Siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	4	aux	_	_
2	stati	essere	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	4	cop	_	_
3	tanto	tanto	DET	DI	Gender=Masc|Number=Sing|PronType=Ind	4	det	_	_
4	tempo	tempo	NOUN	S	Gender=Masc|Number=Sing	0	root	_	_
5	dentro	dentro	ADP	E	_	7	case	_	_
6	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	7	det	_	_
7	lago	lago	NOUN	S	Gender=Masc|Number=Sing	4	nmod	_	SpaceAfter=No
8	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 5
# text = Dopo siamo uscite dall’acqua e la mia amica aveva visto lo scivolo d’acqua Allora decide di andarci.
1	Dopo	dopo	ADV	B	_	3	advmod	_	_
2	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	3	aux	_	_
3	uscite	uscire	VERB	V	Gender=Fem|Number=Plur|Tense=Past|VerbForm=Part	0	root	_	_
4	dall’acqua	dall’acqua	NOUN	S	Gender=Fem|Number=Sing	3	dobj	_	_
5	e	e	CONJ	CC	_	4	cc	_	_
6	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	8	det	_	_
7	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	8	det:poss	_	_
8	amica	amico	NOUN	S	Gender=Fem|Number=Sing	4	conj	_	_
9	aveva	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	10	aux	_	_
10	visto	vedere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	3	advcl	_	_
11	lo	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	12	det	_	_
12	scivolo	scivolo	NOUN	S	Gender=Masc|Number=Sing	10	dobj	_	_
13	d’acqua	d’acquo	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	14	det	_	_
14	Allora	allora	PROPN	SP	_	15	nsubj	_	_
15	decide	decidere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	10	parataxis	_	_
16	di	di	ADP	E	_	17	mark	_	_
17-18	andarci	_	_	_	_	_	_	_	SpaceAfter=No
17	andar	andare	VERB	V	VerbForm=Inf	15	xcomp	_	_
18	ci	ci	PRON	PC	Number=Plur|Person=1|PronType=Clit	17	dobj	_	_
19	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 6
# text = siamo arrivati allo scivolo e erano tutti cosi contenti allora ci sono andati.
1	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	2	aux	_	_
2	arrivati	arrivare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	0	root	_	_
3-4	allo	_	_	_	_	_	_	_	_
3	a	a	ADP	E	_	5	case	_	_
4	lo	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	5	det	_	_
5	scivolo	scivolo	NOUN	S	Gender=Masc|Number=Sing	2	nmod	_	_
6	e	e	CONJ	CC	_	2	cc	_	_
7	erano	essere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	9	cop	_	_
8	tutti	tutto	PRON	PI	Gender=Masc|Number=Plur|PronType=Ind	9	advmod	_	_
9	cosi	coso	ADJ	A	Gender=Masc|Number=Plur	14	nsubj	_	_
10	contenti	contento	VERB	V	Number=Plur|Tense=Pres|VerbForm=Part	9	acl	_	_
11	allora	allora	ADV	B	_	10	advmod	_	_
12	ci	ci	PRON	PC	PronType=Clit	14	expl	_	_
13	sono	essere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	14	aux	_	_
14	andati	andare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	2	conj	_	SpaceAfter=No
15	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 7
# text = Io non volevo andarci perché avevo paura però io mi sentivo una frana.
1	Io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	4	nsubj	_	_
2	non	non	ADV	BN	PronType=Neg	4	neg	_	_
3	volevo	volere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	4	aux	_	_
4-5	andarci	_	_	_	_	_	_	_	_
4	andar	andare	VERB	V	VerbForm=Inf	0	root	_	_
5	ci	ci	PRON	PC	PronType=Clit	4	advmod	_	_
6	perché	perché	SCONJ	CS	_	7	mark	_	_
7	avevo	avere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	12	advcl	_	_
8	paura	paura	NOUN	S	Gender=Fem|Number=Sing	7	xcomp	_	_
9	però	però	ADV	B	_	7	advmod	_	_
10	io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	12	nsubj	_	_
11	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	12	iobj	_	_
12	sentivo	sentire	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	4	ccomp	_	_
13	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	14	det	_	_
14	frana	frana	NOUN	S	Gender=Fem|Number=Sing	12	xcomp	_	SpaceAfter=No
15	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 8
# text = E la mia amica mi chiedeva sempre vieni vieni.
1	E	e	CONJ	CC	_	6	cc	_	_
2	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	4	det	_	_
3	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	4	det:poss	_	_
4	amica	amico	NOUN	S	Gender=Fem|Number=Sing	6	nsubj	_	_
5	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	6	iobj	_	_
6	chiedeva	chiedere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
7	sempre	sempre	ADV	B	_	6	advmod	_	_
8	vieni	vieno	NOUN	S	Gender=Masc|Number=Plur	6	dobj	_	SpacesAfter=\s\s
9	vieni	vieno	ADJ	A	Gender=Masc|Number=Plur	8	amod	_	SpaceAfter=No
10	.	.	PUNCT	FS	_	6	punct	_	_

# sent_id = 9
# text = Guardo che in due si andava piú veloce nello scivolo.
1	Guardo	guardare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	0	root	_	_
2	che	che	SCONJ	CS	_	6	mark	_	_
3	in	in	ADP	E	_	4	case	_	_
4	due	due	NUM	N	NumType=Card	6	nummod	_	_
5	si	si	PRON	PC	Person=3|PronType=Clit	6	expl	_	_
6	andava	andare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	1	ccomp	_	_
7	piú	piú	PROPN	SP	_	6	nsubj	_	_
8	veloce	veloce	ADJ	A	Number=Sing	7	amod	_	_
9-10	nello	_	_	_	_	_	_	_	_
9	in	in	ADP	E	_	11	case	_	_
10	lo	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	11	det	_	_
11	scivolo	scivolo	NOUN	S	Gender=Masc|Number=Sing	6	nmod	_	SpaceAfter=No
12	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 10
# text = Cosi ho chiesto alla mia amica se voleva andare con me allo scivolo perché dasola avevo paura di andare.
1	Cosi	Coso	PROPN	SP	_	3	nsubj	_	_
2	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	3	aux	_	_
3	chiesto	chiedere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
4-5	alla	_	_	_	_	_	_	_	_
4	a	a	ADP	E	_	7	case	_	_
5	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	7	det	_	_
6	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	7	det:poss	_	_
7	amica	amico	NOUN	S	Gender=Fem|Number=Sing	3	nmod	_	_
8	se	se	SCONJ	CS	_	10	mark	_	_
9	voleva	volere	AUX	VM	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	10	aux	_	_
10	andare	andare	VERB	V	VerbForm=Inf	17	advcl	_	_
11	con	con	ADP	E	_	12	case	_	_
12	me	me	PRON	PE	Number=Sing|Person=1|PronType=Prs	10	nmod	_	_
13-14	allo	_	_	_	_	_	_	_	_
13	a	a	ADP	E	_	15	case	_	_
14	lo	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	15	det	_	_
15	scivolo	scivolo	NOUN	S	Gender=Masc|Number=Sing	10	nmod	_	_
16	perché	perché	ADV	B	_	17	advmod	_	_
17	dasola	dasolo	ADJ	A	Gender=Fem|Number=Sing	3	xcomp	_	_
18	avevo	avere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	17	root	_	_
19	paura	paura	NOUN	S	Gender=Fem|Number=Sing	18	dobj	_	_
20	di	di	ADP	E	_	21	mark	_	_
21	andare	andare	VERB	V	VerbForm=Inf	19	acl	_	SpaceAfter=No
22	.	.	PUNCT	FS	_	18	punct	_	_

# sent_id = 11
# text = Allora siamo partite e guando siamo arrivate giú io mi sono fatta male.
1	Allora	allora	ADV	B	_	3	advmod	_	_
2	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	3	aux	_	_
3	partite	partire	VERB	V	Gender=Fem|Number=Plur|Tense=Past|VerbForm=Part	0	root	_	_
4	e	e	CONJ	CC	_	3	cc	_	_
5	guando	guando	VERB	V	VerbForm=Ger	7	advcl	_	_
6	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	7	aux	_	_
7	arrivate	arrivare	VERB	V	Gender=Fem|Number=Plur|Tense=Past|VerbForm=Part	3	conj	_	_
8	giú	giú	PROPN	SP	_	7	xcomp	_	_
9	io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	12	nsubj	_	_
10	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	12	expl	_	_
11	sono	essere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	12	aux	_	_
12	fatta	fare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	8	acl	_	_
13	male	male	ADV	B	_	12	advmod	_	SpaceAfter=No
14	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 12
# text = Da quel momento ho capito che non devi guardare quello che fanno gli altri pensando che loro sono migliori di te ma ognuno deve fare cio che sente seguendo il proprio cuore.
1	Da	da	ADP	E	_	3	case	_	_
2	quel	quello	DET	DD	Gender=Masc|Number=Sing|PronType=Dem	3	det	_	_
3	momento	momento	NOUN	S	Gender=Masc|Number=Sing	5	nmod	_	_
4	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	capito	capire	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
6	che	che	SCONJ	CS	_	9	mark	_	_
7	non	non	ADV	BN	PronType=Neg	9	neg	_	_
8	devi	dovere	AUX	VM	Mood=Ind|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	9	aux	_	_
9	guardare	guardare	VERB	V	VerbForm=Inf	5	ccomp	_	_
10	quello	quello	PRON	PD	Gender=Masc|Number=Sing|PronType=Dem	9	dobj	_	_
11	che	che	PRON	PR	PronType=Rel	12	dobj	_	_
12	fanno	fare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	10	acl:relcl	_	_
13	gli	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	14	det	_	_
14	altri	altro	PRON	PI	Gender=Masc|Number=Plur|PronType=Ind	12	nsubj	_	_
15	pensando	pensare	VERB	V	VerbForm=Ger	12	advcl	_	_
16	che	che	SCONJ	CS	_	19	mark	_	_
17	loro	loro	PRON	PE	Number=Plur|Person=3|PronType=Prs	19	nsubj	_	_
18	sono	essere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	19	cop	_	_
19	migliori	migliore	ADJ	A	Degree=Cmp|Number=Plur	15	ccomp	_	_
20	di	di	ADP	E	_	21	case	_	_
21	te	te	PRON	PE	Number=Sing|Person=2|PronType=Prs	19	nmod	_	_
22	ma	ma	CONJ	CC	_	19	cc	_	_
23	ognuno	ognuno	PRON	PI	Gender=Masc|Number=Sing|PronType=Ind	25	nsubj	_	_
24	deve	dovere	AUX	VM	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	25	aux	_	_
25	fare	fare	VERB	V	VerbForm=Inf	19	conj	_	_
26	cio	cio	NOUN	S	Gender=Masc|Number=Sing	25	dobj	_	_
27	che	che	PRON	PR	PronType=Rel	28	nsubj	_	_
28	sente	sentire	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	26	acl:relcl	_	_
29	seguendo	seguire	VERB	V	VerbForm=Ger	28	advcl	_	_
30	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	32	det	_	_
31	proprio	proprio	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	32	det:poss	_	_
32	cuore	cuore	NOUN	S	Gender=Masc|Number=Sing	29	dobj	_	SpaceAfter=No
33	.	.	PUNCT	FS	_	5	punct	_	SpacesAfter=\r\n\r\n


# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE131.txt
# newpar
# sent_id = 1
# text = Un giorno volevo fare la spaccata perché i miei compagni di danza la sapevano fare e io no.
1	Un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	4	nmod	_	_
3	volevo	volere	AUX	VM	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	4	aux	_	_
4	fare	fare	VERB	V	VerbForm=Inf	0	root	_	_
5	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	6	det	_	_
6	spaccata	spaccata	NOUN	S	Gender=Fem|Number=Sing	4	dobj	_	_
7	perché	perché	SCONJ	CS	_	14	mark	_	_
8	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	10	det	_	_
9	miei	mio	DET	AP	Gender=Masc|Number=Plur|Poss=Yes|PronType=Prs	10	det:poss	_	_
10	compagni	compagno	NOUN	S	Gender=Masc|Number=Plur	14	nsubj	_	_
11	di	di	ADP	E	_	12	case	_	_
12	danza	danza	NOUN	S	Gender=Fem|Number=Sing	10	nmod	_	_
13	la	lo	PRON	PC	Gender=Fem|Number=Sing|Person=3|PronType=Clit	14	dobj	_	_
14	sapevano	sapere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	4	advcl	_	_
15	fare	fare	VERB	V	VerbForm=Inf	14	xcomp	_	_
16	e	e	CONJ	CC	_	14	cc	_	_
17	io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	14	conj	_	_
18	no	no	ADV	BN	PronType=Neg	17	neg	_	SpaceAfter=No
19	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 2
# text = Ho provato a farla e non ci sono riuscita nonostante tutti i miei sforzi.
1	Ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	2	aux	_	_
2	provato	provare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
3	a	a	ADP	E	_	4	mark	_	_
4-5	farla	_	_	_	_	_	_	_	_
4	far	fare	VERB	V	VerbForm=Inf	2	xcomp	_	_
5	la	lo	PRON	PC	Gender=Fem|Number=Sing|Person=3|PronType=Clit	4	dobj	_	_
6	e	e	CONJ	CC	_	2	cc	_	_
7	non	non	ADV	BN	PronType=Neg	10	neg	_	_
8	ci	ci	PRON	PC	PronType=Clit	10	expl	_	_
9	sono	essere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	10	aux	_	_
10	riuscita	riuscire	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	2	conj	_	_
11	nonostante	nonostante	ADP	E	_	15	case	_	_
12	tutti	tutto	DET	T	Gender=Masc|Number=Plur	15	det:predet	_	_
13	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	15	det	_	_
14	miei	mio	DET	AP	Gender=Masc|Number=Plur|Poss=Yes|PronType=Prs	15	det:poss	_	_
15	sforzi	sforzo	NOUN	S	Gender=Masc|Number=Plur	10	nmod	_	SpaceAfter=No
16	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 3
# text = Faceva molto male e allora ho deciso di allenarmi ogni sera per sedermi in spaccata. Ho iniziato circa a dicembre ad allenarmi.
1	Faceva	fare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
2	molto	molto	ADV	B	_	3	advmod	_	_
3	male	male	ADV	B	_	1	advmod	_	_
4	e	e	CONJ	CC	_	1	cc	_	_
5	allora	allora	ADV	B	_	7	advmod	_	_
6	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	7	aux	_	_
7	deciso	decidere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	1	conj	_	_
8	di	di	ADP	E	_	9	case	_	_
9	allenarmi	arma|allenarme	NOUN	S	Gender=Masc|Number=Plur	7	nmod	_	_
10	ogni	ogni	DET	DI	Number=Sing|PronType=Ind	11	det	_	_
11	sera	sera	NOUN	S	Gender=Fem|Number=Sing	7	nmod	_	_
12	per	per	ADP	E	_	13	case	_	_
13	sedermi	sedermo	NOUN	S	Gender=Masc|Number=Plur	7	nmod	_	_
14	in	in	ADP	E	_	15	case	_	_
15	spaccata	spaccata	NOUN	S	Gender=Fem|Number=Sing	13	nmod	_	SpaceAfter=No
16	.	.	PUNCT	FS	_	18	punct	_	_
17	Ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	18	aux	_	_
18	iniziato	iniziare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	1	conj	_	_
19	circa	circa	ADV	B	_	21	advmod	_	_
20	a	a	ADP	E	_	21	case	_	_
21	dicembre	dicembre	NOUN	S	Gender=Masc|Number=Sing	18	nmod	_	_
22	ad	a	ADP	E	_	23	case	_	_
23	allenarmi	arma|allenarme	NOUN	S	Gender=Masc|Number=Plur	18	nmod	_	SpaceAfter=No
24	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 4
# text = Facevo molta fatica e dopo tre mesi mi mancavano quindici centimetri.
1	Facevo	Facevere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	0	root	_	_
2	molta	molto	DET	DI	Gender=Fem|Number=Sing|PronType=Ind	3	det	_	_
3	fatica	fatica	NOUN	S	Gender=Fem|Number=Sing	1	dobj	_	_
4	e	e	CONJ	CC	_	1	cc	_	_
5	dopo	dopo	ADP	E	_	7	case	_	_
6	tre	tre	NUM	N	NumType=Card	7	nummod	_	_
7	mesi	mese	NOUN	S	Gender=Masc|Number=Plur	9	nmod	_	_
8	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	9	iobj	_	_
9	mancavano	mancare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	1	conj	_	_
10	quindici	quindici	NUM	N	NumType=Card	11	nummod	_	_
11	centimetri	centimetro	NOUN	S	Gender=Masc|Number=Plur	9	dobj	_	SpaceAfter=No
12	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 5
# text = Ho provato molte volte a sedermi e mi sono allenata ogni sera per quasi tutto l’anno.
1	Ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	2	aux	_	_
2	provato	provare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
3	molte	molto	DET	DI	Gender=Fem|Number=Plur|PronType=Ind	4	det	_	_
4	volte	volta	NOUN	S	Gender=Fem|Number=Plur	2	dobj	_	_
5	a	a	ADP	E	_	6	case	_	_
6	sedermi	sedermo	NOUN	S	Gender=Masc|Number=Plur	2	nmod	_	_
7	e	e	CONJ	CC	_	2	cc	_	_
8	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	10	expl	_	_
9	sono	essere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	10	aux	_	_
10	allenata	allenare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	2	conj	_	_
11	ogni	ogni	DET	DI	Number=Sing|PronType=Ind	12	det	_	_
12	sera	sera	NOUN	S	Gender=Fem|Number=Sing	10	nmod	_	_
13	per	per	ADP	E	_	16	mark	_	_
14	quasi	quasi	ADV	B	_	16	advmod	_	_
15	tutto	tutto	PRON	PI	Gender=Masc|Number=Sing|PronType=Ind	16	nsubj	_	_
16	l’anno	l’anno	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	10	advcl	_	SpaceAfter=No
17	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 6
# text = Alla lezione di danza, prima delle vacanze di Pasqua la maestra ci ha chiesto se sapevamo fare le spaccate e io ci ho provato e lei mi ha spinto giù:
1-2	Alla	_	_	_	_	_	_	_	_
1	A	a	ADP	E	_	3	case	_	_
2	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	3	det	_	_
3	lezione	lezione	NOUN	S	Gender=Fem|Number=Sing	17	nmod	_	_
4	di	di	ADP	E	_	5	case	_	_
5	danza	danza	NOUN	S	Gender=Fem|Number=Sing	3	nmod	_	SpaceAfter=No
6	,	,	PUNCT	FF	_	3	punct	_	_
7	prima	prima	ADV	B	_	10	case	_	_
8-9	delle	_	_	_	_	_	_	_	_
8	di	di	ADP	E	_	7	mwe	_	_
9	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	10	det	_	_
10	vacanze	vacanza	NOUN	S	Gender=Fem|Number=Plur	17	nmod	_	_
11	di	di	ADP	E	_	12	case	_	_
12	Pasqua	Pasqua	PROPN	SP	_	10	nmod	_	_
13	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	14	det	_	_
14	maestra	maestra	NOUN	S	Gender=Fem|Number=Sing	17	nsubj	_	_
15	ci	ci	PRON	PC	Number=Plur|Person=1|PronType=Clit	17	iobj	_	_
16	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	17	aux	_	_
17	chiesto	chiedere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
18	se	se	SCONJ	CS	_	20	mark	_	_
19	sapevamo	sapere	AUX	VM	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	20	aux	_	_
20	fare	fare	VERB	V	VerbForm=Inf	17	ccomp	_	_
21	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	22	det	_	_
22	spaccate	spaccare	NOUN	S	Gender=Fem|Number=Plur	20	dobj	_	_
23	e	e	CONJ	CC	_	20	cc	_	_
24	io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	27	nsubj	_	_
25	ci	ci	PRON	PC	PronType=Clit	27	advmod	_	_
26	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	27	aux	_	_
27	provato	provare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	20	conj	_	_
28	e	e	CONJ	CC	_	20	cc	_	_
29	lei	lei	PRON	PE	Number=Sing|Person=3|PronType=Prs	32	nsubj	_	_
30	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	32	dobj	_	_
31	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	32	aux	_	_
32	spinto	spingere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	20	conj	_	_
33	giù	giù	ADV	B	_	32	advmod	_	SpaceAfter=No
34	:	:	PUNCT	FC	_	17	punct	_	_

# sent_id = 7
# text = mi mancavano soltanto cinque centimetri!
1	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	2	iobj	_	_
2	mancavano	mancare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
3	soltanto	soltanto	ADV	B	_	2	advmod	_	_
4	cinque	cinque	NUM	N	NumType=Card	5	nummod	_	_
5	centimetri	centimetro	NOUN	S	Gender=Masc|Number=Plur	2	dobj	_	SpaceAfter=No
6	!	!	PUNCT	FS	_	2	punct	_	_

# sent_id = 8
# text = Alle vacanze di Pasqua, mentre ero in Ucraina mi è venuta tanta voglia di sedermi in spaccata e dopo tre allenamenti la gamba toccava per terra e mi ero quasi seduta.
1-2	Alle	_	_	_	_	_	_	_	_
1	A	a	ADP	E	_	3	case	_	_
2	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	3	det	_	_
3	vacanze	vacanza	NOUN	S	Gender=Fem|Number=Plur	13	nmod	_	_
4	di	di	ADP	E	_	5	case	_	_
5	Pasqua	Pasqua	PROPN	SP	_	3	nmod	_	SpaceAfter=No
6	,	,	PUNCT	FF	_	3	punct	_	_
7	mentre	mentre	SCONJ	CS	_	8	mark	_	_
8	ero	essere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	13	advcl	_	_
9	in	in	ADP	E	_	10	case	_	_
10	Ucraina	Ucraina	PROPN	SP	_	8	nmod	_	_
11	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	13	iobj	_	_
12	è	essere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	13	aux	_	_
13	venuta	venire	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
14	tanta	tanto	DET	DI	Gender=Fem|Number=Sing|PronType=Ind	15	det	_	_
15	voglia	voglia	NOUN	S	Gender=Fem|Number=Sing	13	nsubj	_	_
16	di	di	ADP	E	_	17	case	_	_
17	sedermi	sedermo	NOUN	S	Gender=Masc|Number=Plur	15	nmod	_	_
18	in	in	ADP	E	_	19	case	_	_
19	spaccata	spaccata	NOUN	S	Gender=Fem|Number=Sing	17	nmod	_	_
20	e	e	CONJ	CC	_	15	cc	_	_
21	dopo	dopo	ADP	E	_	23	case	_	_
22	tre	tre	NUM	N	NumType=Card	23	nummod	_	_
23	allenamenti	allenamento	NOUN	S	Gender=Masc|Number=Plur	26	nmod	_	_
24	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	25	det	_	_
25	gamba	gamba	NOUN	S	Gender=Fem|Number=Sing	26	nsubj	_	_
26	toccava	toccare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	15	conj	_	_
27	per	per	ADP	E	_	28	case	_	_
28	terra	terra	NOUN	S	Gender=Fem|Number=Sing	26	nmod	_	_
29	e	e	CONJ	CC	_	13	cc	_	_
30	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	33	expl	_	_
31	ero	essere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	33	aux	_	_
32	quasi	quasi	ADV	B	_	33	advmod	_	_
33	seduta	sedere	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	13	conj	_	SpaceAfter=No
34	.	.	PUNCT	FS	_	13	punct	_	_

# sent_id = 9
# text = Un giorno io e la mia amica stavamo facendo un po’ di ginnastica (ruote, spaccate, salti…).
1	Un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	9	nmod	_	_
3	io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	9	nsubj	_	_
4	e	e	CONJ	CC	_	3	cc	_	_
5	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	7	det	_	_
6	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	7	det:poss	_	_
7	amica	amico	NOUN	S	Gender=Fem|Number=Sing	3	conj	_	_
8	stavamo	stare	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	9	aux	_	_
9	facendo	fare	VERB	V	VerbForm=Ger	0	root	_	_
10	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	11	det	_	_
11	po’	ipo	PRON	PC	PronType=Clit	9	dobj	_	_
12	di	di	ADP	E	_	13	case	_	_
13	ginnastica	ginnastica	NOUN	S	Gender=Fem|Number=Sing	11	nmod	_	_
14	(	(	PUNCT	FB	_	15	punct	_	SpaceAfter=No
15	ruote	ruota	NOUN	S	Gender=Masc|Number=Sing	11	appos	_	SpaceAfter=No
16	,	,	PUNCT	FF	_	15	punct	_	_
17	spaccate	spaccare	NOUN	S	Gender=Fem|Number=Plur	15	conj	_	SpaceAfter=No
18	,	,	PUNCT	FF	_	15	punct	_	_
19	salti…	salti…	PUNCT	FF	_	15	punct	_	SpaceAfter=No
20	)	)	PUNCT	FB	_	15	punct	_	SpaceAfter=No
21	.	.	PUNCT	FS	_	9	punct	_	_

# sent_id = 10
# text = Alla fine con tutti i miei sforzi mi sono seduta.
1-2	Alla	_	_	_	_	_	_	_	_
1	A	a	ADP	E	_	3	case	_	_
2	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	3	det	_	_
3	fine	fine	NOUN	S	Gender=Fem|Number=Sing	11	nmod	_	_
4	con	con	ADP	E	_	8	case	_	_
5	tutti	tutto	DET	T	Gender=Masc|Number=Plur	8	det:predet	_	_
6	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	8	det	_	_
7	miei	mio	DET	AP	Gender=Masc|Number=Plur|Poss=Yes|PronType=Prs	8	det:poss	_	_
8	sforzi	sforzo	NOUN	S	Gender=Masc|Number=Plur	11	nmod	_	_
9	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	11	iobj	_	_
10	sono	essere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	11	aux	_	_
11	seduta	sedere	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	SpaceAfter=No
12	.	.	PUNCT	FS	_	11	punct	_	_

# sent_id = 11
# text = Ho imparato che con la voglia puoi sederti in spaccata e fare tante altre cose.
1	Ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	2	aux	_	_
2	imparato	imparare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
3	che	che	SCONJ	CS	_	8	mark	_	_
4	con	con	ADP	E	_	6	case	_	_
5	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	6	det	_	_
6	voglia	voglia	NOUN	S	Gender=Fem|Number=Sing	8	nmod	_	_
7	puoi	potere	AUX	VM	Mood=Ind|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	8	aux	_	_
8	sederti	sedrire	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	2	ccomp	_	_
9	in	in	ADP	E	_	10	case	_	_
10	spaccata	spaccata	NOUN	S	Gender=Fem|Number=Sing	8	nmod	_	_
11	e	e	CONJ	CC	_	8	cc	_	_
12	fare	fare	VERB	V	VerbForm=Inf	8	conj	_	_
13	tante	tanto	DET	DI	Gender=Fem|Number=Plur|PronType=Ind	15	det	_	_
14	altre	altro	ADJ	A	Gender=Fem|Number=Plur	15	amod	_	_
15	cose	cosa	NOUN	S	Gender=Fem|Number=Plur	12	dobj	_	SpaceAfter=No
16	.	.	PUNCT	FS	_	2	punct	_	SpacesAfter=\s\s

# sent_id = 12
# text = Non puoi prendere sei senza avere la voglia di studiare e non puoi passare la classe senza avere la voglia di studiare e imparare cose nuove.
1	Non	non	ADV	BN	PronType=Neg	3	neg	_	_
2	puoi	potere	AUX	VM	Mood=Ind|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	3	aux	_	_
3	prendere	prendere	VERB	V	VerbForm=Inf	0	root	_	_
4	sei	sei	NUM	N	NumType=Card	3	dobj	_	_
5	senza	senza	ADP	E	_	6	mark	_	_
6	avere	avere	VERB	V	VerbForm=Inf	3	advcl	_	_
7	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	8	det	_	_
8	voglia	voglia	NOUN	S	Gender=Fem|Number=Sing	6	dobj	_	_
9	di	di	ADP	E	_	10	mark	_	_
10	studiare	studiare	VERB	V	VerbForm=Inf	8	acl	_	_
11	e	e	CONJ	CC	_	10	cc	_	_
12	non	non	ADV	BN	PronType=Neg	14	neg	_	_
13	puoi	potere	AUX	VM	Mood=Ind|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	14	aux	_	_
14	passare	passare	VERB	V	VerbForm=Inf	10	conj	_	_
15	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	16	det	_	_
16	classe	classe	NOUN	S	Gender=Fem|Number=Sing	14	dobj	_	_
17	senza	senza	ADP	E	_	18	mark	_	_
18	avere	avere	VERB	V	VerbForm=Inf	14	advcl	_	_
19	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	20	det	_	_
20	voglia	voglia	NOUN	S	Gender=Fem|Number=Sing	18	dobj	_	_
21	di	di	ADP	E	_	22	mark	_	_
22	studiare	studiare	VERB	V	VerbForm=Inf	20	acl	_	_
23	e	e	CONJ	CC	_	22	cc	_	_
24	imparare	imparare	VERB	V	VerbForm=Inf	22	conj	_	_
25	cose	cosa	NOUN	S	Gender=Fem|Number=Plur	24	dobj	_	_
26	nuove	nuovo	ADJ	A	Gender=Fem|Number=Plur	25	amod	_	SpaceAfter=No
27	.	.	PUNCT	FS	_	3	punct	_	SpacesAfter=\r\n\r\n


# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE4.txt
# newpar
# sent_id = 1
# text = Io l’altra volta, volevo giocare a calcio con il mio fratellino, e un suo amico.
1	Io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	6	nsubj	_	_
2	l’altra	l’altro	DET	DI	Gender=Fem|Number=Sing|PronType=Ind	3	det	_	_
3	volta	volta	NOUN	S	Gender=Fem|Number=Sing	6	nmod	_	SpaceAfter=No
4	,	,	PUNCT	FF	_	3	punct	_	_
5	volevo	volere	AUX	VM	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	6	aux	_	_
6	giocare	giocare	VERB	V	VerbForm=Inf	0	root	_	_
7	a	a	ADP	E	_	8	case	_	_
8	calcio	calcio	NOUN	S	Gender=Masc|Number=Sing	6	nmod	_	_
9	con	con	ADP	E	_	12	case	_	_
10	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	12	det	_	_
11	mio	mio	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	12	det:poss	_	_
12	fratellino	fratellino	NOUN	S	Gender=Masc|Number=Sing	6	nmod	_	SpaceAfter=No
13	,	,	PUNCT	FF	_	6	punct	_	_
14	e	e	CONJ	CC	_	6	cc	_	_
15	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	17	det	_	_
16	suo	suo	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	17	det:poss	_	_
17	amico	amico	NOUN	S	Gender=Masc|Number=Sing	6	conj	_	SpaceAfter=No
18	.	.	PUNCT	FS	_	6	punct	_	_

# sent_id = 2
# text = Quando è iniziata li avevo sottovalutati dandoli 4 punti di vantaggio e quindi vinsero.
1	Quando	quando	SCONJ	CS	_	3	mark	_	_
2	è	essere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	3	aux	_	_
3	iniziata	iniziare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	6	advcl	_	_
4	li	li	PRON	PC	Gender=Masc|Number=Plur|Person=3|PronType=Clit	3	nsubj	_	_
5	avevo	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	6	aux	_	_
6	sottovalutati	sottovalutare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	0	root	_	_
7	dandoli	dandolo	NOUN	S	Gender=Masc|Number=Plur	6	dobj	_	_
8	4	4	NUM	N	NumType=Card	9	nummod	_	_
9	punti	punto	NOUN	S	Gender=Masc|Number=Plur	7	nmod	_	_
10	di	di	ADP	E	_	11	case	_	_
11	vantaggio	vantaggio	NOUN	S	Gender=Masc|Number=Sing	9	nmod	_	_
12	e	e	CONJ	CC	_	6	cc	_	_
13	quindi	quindi	ADV	B	_	14	advmod	_	_
14	vinsero	vindere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Past|VerbForm=Fin	6	conj	_	SpaceAfter=No
15	.	.	PUNCT	FS	_	6	punct	_	_

# sent_id = 3
# text = Poi quando vinsero hanno cominciato a figheggiarsi, e quindi ho voluto la rivincita.
1	Poi	poi	ADV	B	_	3	advmod	_	_
2	quando	quando	SCONJ	CS	_	3	mark	_	_
3	vinsero	vindere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
4	hanno	avere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	cominciato	cominciare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	3	csubj	_	_
6	a	a	ADP	E	_	7	case	_	_
7	figheggiarsi	figheggiarsio	NOUN	S	Gender=Masc|Number=Plur	5	nmod	_	SpaceAfter=No
8	,	,	PUNCT	FF	_	5	punct	_	_
9	e	e	CONJ	CC	_	5	cc	_	_
10	quindi	quindi	ADV	B	_	12	advmod	_	_
11	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	12	aux	_	_
12	voluto	volere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	5	conj	_	_
13	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	14	det	_	_
14	rivincita	rivincita	NOUN	S	Gender=Fem|Number=Sing	12	dobj	_	SpaceAfter=No
15	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 4
# text = E la vinsi senza sottovalutare l’avversario.
1	E	e	CONJ	CC	_	3	cc	_	_
2	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	3	det	_	_
3	vinsi	vinso	NOUN	S	Gender=Fem	0	root	_	_
4	senza	senza	ADP	E	_	5	mark	_	_
5	sottovalutare	sottovalutare	VERB	V	VerbForm=Inf	3	acl	_	_
6	l’avversario	l’avversario	NOUN	S	Gender=Masc|Number=Sing	5	dobj	_	SpaceAfter=No
7	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 5
# text = Riflessioni:
1	Riflessioni	riflessione	NOUN	S	Gender=Fem|Number=Plur	0	root	_	SpaceAfter=No
2	:	:	PUNCT	FC	_	1	punct	_	_

# sent_id = 6
# text = Meglio non dare punti di vantaggio senò è sicuro o quasi sicuro che perdi.
1	Meglio	meglio	ADV	B	_	3	advmod	_	_
2	non	non	ADV	BN	PronType=Neg	3	neg	_	_
3	dare	dare	VERB	V	VerbForm=Inf	7	advcl	_	_
4	punti	punto	NOUN	S	Gender=Masc|Number=Plur	3	dobj	_	_
5	di	di	ADP	E	_	6	case	_	_
6	vantaggio	vantaggio	NOUN	S	Gender=Masc|Number=Sing	4	nmod	_	_
7	senò	senare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	9	csubj	_	_
8	è	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	9	cop	_	_
9	sicuro	sicuro	ADJ	A	Gender=Masc|Number=Sing	0	root	_	_
10	o	o	CONJ	CC	_	9	cc	_	_
11	quasi	quasi	ADV	B	_	12	advmod	_	_
12	sicuro	sicuro	ADJ	A	Gender=Masc|Number=Sing	9	conj	_	_
13	che	che	SCONJ	CS	_	14	mark	_	_
14	perdi	perdo	VERB	V	Mood=Sub|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	12	ccomp	_	SpaceAfter=No
15	.	.	PUNCT	FS	_	9	punct	_	_

# sent_id = 7
# text = Insegnamento:
1	Insegnamento	insegnamento	NOUN	S	Gender=Masc|Number=Sing	0	root	_	SpaceAfter=No
2	:	:	PUNCT	FC	_	1	punct	_	_

# sent_id = 8
# text = Mi ha insegnato a non sottovalutare l’avversario, dandoli punti di vantaggio.
1	Mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	3	dobj	_	_
2	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	3	aux	_	_
3	insegnato	insegnare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
4	a	a	ADP	E	_	6	mark	_	_
5	non	non	ADV	BN	PronType=Neg	6	neg	_	_
6	sottovalutare	sottovalutare	VERB	V	VerbForm=Inf	3	xcomp	_	_
7	l’avversario	l’avversario	NOUN	S	Gender=Masc|Number=Sing	6	dobj	_	SpaceAfter=No
8	,	,	PUNCT	FF	_	7	punct	_	_
9	dandoli	dandolo	ADJ	A	Number=Plur	10	amod	_	_
10	punti	punto	NOUN	S	Gender=Masc|Number=Plur	7	nmod	_	_
11	di	di	ADP	E	_	12	case	_	_
12	vantaggio	vantaggio	NOUN	S	Gender=Masc|Number=Sing	10	nmod	_	SpaceAfter=No
13	.	.	PUNCT	FS	_	3	punct	_	SpacesAfter=\r\n\r\n


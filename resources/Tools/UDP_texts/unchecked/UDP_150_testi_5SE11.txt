# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE11.txt
# newpar
# sent_id = 1
# text = Lo SKATE PARK Un giorno quando sono andato la prima volta allo SKATE PARK mi prendevano in giro perché non sapevo tutte le piste dello SKATE PARK.
1	Lo	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	SKATE	SKATE	PROPN	SP	_	17	nsubj	_	_
3	PARK	Park	PROPN	SP	_	2	name	_	_
4	Un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	5	det	_	_
5	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	17	nmod	_	_
6	quando	quando	SCONJ	CS	_	8	mark	_	_
7	sono	essere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	8	aux	_	_
8	andato	andare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	17	advcl	_	_
9	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	11	det	_	_
10	prima	primo	ADJ	NO	Gender=Fem|Number=Sing|NumType=Ord	11	amod	_	_
11	volta	volta	NOUN	S	Gender=Fem|Number=Sing	8	nsubj	_	_
12-13	allo	_	_	_	_	_	_	_	_
12	a	a	ADP	E	_	14	case	_	_
13	lo	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	14	det	_	_
14	SKATE	SKATE	PROPN	SP	_	11	nmod	_	_
15	PARK	Park	PROPN	SP	_	14	name	_	_
16	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	17	dobj	_	_
17	prendevano	prendere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
18	in	in	ADP	E	_	19	case	_	_
19	giro	giro	NOUN	S	Gender=Masc|Number=Sing	17	nmod	_	_
20	perché	perché	SCONJ	CS	_	22	mark	_	_
21	non	non	ADV	BN	PronType=Neg	22	neg	_	_
22	sapevo	sapere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	17	advcl	_	_
23	tutte	tutto	DET	T	Gender=Fem|Number=Plur	25	det:predet	_	_
24	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	25	det	_	_
25	piste	pista	NOUN	S	Gender=Fem|Number=Plur	22	dobj	_	_
26-27	dello	_	_	_	_	_	_	_	_
26	di	di	ADP	E	_	28	case	_	_
27	lo	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	28	det	_	_
28	SKATE	SKATE	PROPN	SP	_	25	nmod	_	_
29	PARK	Park	PROPN	SP	_	28	name	_	SpaceAfter=No
30	.	.	PUNCT	FS	_	17	punct	_	_

# sent_id = 2
# text = Ma dopo quando siamo andati speso ci siamo migliorati e non ci prendevano più in giro.
1	Ma	ma	CONJ	CC	_	9	cc	_	_
2	dopo	dopo	ADV	B	_	5	advmod	_	_
3	quando	quando	SCONJ	CS	_	5	mark	_	_
4	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	andati	andare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	9	advcl	_	_
6	speso	speso	ADJ	A	Gender=Masc|Number=Sing	5	xcomp	_	_
7	ci	ci	PRON	PC	Number=Plur|Person=1|PronType=Clit	9	iobj	_	_
8	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	9	aux	_	_
9	migliorati	migliorare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	0	root	_	_
10	e	e	CONJ	CC	_	9	cc	_	_
11	non	non	ADV	BN	PronType=Neg	13	neg	_	_
12	ci	ci	PRON	PC	Number=Plur|Person=1|PronType=Clit	13	dobj	_	_
13	prendevano	prendere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	9	conj	_	_
14	più	più	ADV	B	_	13	advmod	_	_
15	in	in	ADP	E	_	16	case	_	_
16	giro	giro	NOUN	S	Gender=Masc|Number=Sing	13	nmod	_	SpaceAfter=No
17	.	.	PUNCT	FS	_	9	punct	_	_

# sent_id = 3
# text = Dopo quando siamo andati spesso ci piaceva tanto per quello andiamo sempre al mercoledì e al sabato.
1	Dopo	dopo	ADP	E	_	2	case	_	_
2	quando	quando	SCONJ	CS	_	4	nmod	_	_
3	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	4	aux	_	_
4	andati	andare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	7	advcl	_	_
5	spesso	spesso	ADV	B	_	4	advmod	_	_
6	ci	ci	PRON	PC	Number=Plur|Person=1|PronType=Clit	7	dobj	_	_
7	piaceva	piacere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
8	tanto	tanto	ADV	B	_	10	advmod	_	_
9	per	per	ADP	E	_	10	case	_	_
10	quello	quello	PRON	PD	Gender=Masc|Number=Sing|PronType=Dem	7	nmod	_	_
11	andiamo	andare	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	7	advcl	_	_
12	sempre	sempre	ADV	B	_	11	advmod	_	_
13-14	al	_	_	_	_	_	_	_	_
13	a	a	ADP	E	_	15	case	_	_
14	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	15	det	_	_
15	mercoledì	mercoledì	NOUN	S	Gender=Masc	11	nmod	_	_
16	e	e	CONJ	CC	_	15	cc	_	_
17-18	al	_	_	_	_	_	_	_	_
17	a	a	ADP	E	_	19	case	_	_
18	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	19	det	_	_
19	sabato	sabato	NOUN	S	Gender=Masc|Number=Sing	15	conj	_	SpaceAfter=No
20	.	.	PUNCT	FS	_	7	punct	_	_

# sent_id = 4
# text = E quando andiamo troviamo altri amici.
1	E	e	CONJ	CC	_	4	cc	_	_
2	quando	quando	SCONJ	CS	_	3	mark	_	_
3	andiamo	andare	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	4	advcl	_	_
4	troviamo	trovare	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	0	root	_	_
5	altri	altro	ADJ	A	Gender=Masc|Number=Plur	6	amod	_	_
6	amici	amico	NOUN	S	Gender=Masc|Number=Plur	4	dobj	_	SpaceAfter=No
7	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 5
# text = E dopo abbiamo imparato certe cose tipo il 180° e il 360° e altri trik e mi piace fare la nera, blu e la gialla.
1	E	e	CONJ	CC	_	4	cc	_	_
2	dopo	dopo	ADV	B	_	4	advmod	_	_
3	abbiamo	avere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	4	aux	_	_
4	imparato	imparare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
5	certe	certo	DET	DI	Gender=Fem|Number=Plur|PronType=Ind	6	det	_	_
6	cose	cosa	NOUN	S	Gender=Fem|Number=Plur	4	dobj	_	_
7	tipo	tipo	NOUN	S	Gender=Masc|Number=Sing	6	compound	_	_
8	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	9	det	_	_
9	180°	180umero	NOUN	S	Gender=Masc	7	nmod	_	_
10	e	e	CONJ	CC	_	9	cc	_	_
11	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	12	det	_	_
12	360°	360umero	NOUN	S	Gender=Masc	9	conj	_	_
13	e	e	CONJ	CC	_	9	cc	_	_
14	altri	altro	PRON	PI	Gender=Masc|Number=Plur|PronType=Ind	9	conj	_	_
15	trik	trik	ADJ	A	_	14	amod	_	_
16	e	e	CONJ	CC	_	4	cc	_	_
17	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	18	iobj	_	_
18	piace	piacere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	4	conj	_	_
19	fare	fare	VERB	V	VerbForm=Inf	18	ccomp	_	_
20	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	21	det	_	_
21	nera	nera	NOUN	S	Gender=Fem|Number=Sing	19	dobj	_	SpaceAfter=No
22	,	,	PUNCT	FF	_	21	punct	_	_
23	blu	blu	ADJ	A	_	21	conj	_	_
24	e	e	CONJ	CC	_	21	cc	_	_
25	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	26	det	_	_
26	gialla	gialla	NOUN	S	Gender=Fem|Number=Sing	21	conj	_	SpaceAfter=No
27	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 6
# text = Dal altra parte mi piace le rampe mo non posso andare perche è solo per gli, SKATE BORD ma quando non ci sono vado anche lì.
1-2	Dal	_	_	_	_	_	_	_	_
1	Da	da	ADP	E	_	4	case	_	_
2	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	4	det	_	_
3	altra	altro	ADJ	A	Gender=Fem|Number=Sing	4	amod	_	_
4	parte	parte	NOUN	S	Gender=Fem|Number=Sing	6	nmod	_	_
5	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	6	iobj	_	_
6	piace	piacere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
7	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	8	det	_	_
8	rampe	rampo	NOUN	S	Gender=Fem|Number=Plur	6	nsubj	_	_
9	mo	modo	ADJ	A	Gender=Masc|Number=Sing	8	amod	_	_
10	non	non	ADV	BN	PronType=Neg	12	neg	_	_
11	posso	potere	AUX	VM	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	12	aux	_	_
12	andare	andare	VERB	V	VerbForm=Inf	6	conj	_	_
13	perche	perca	NOUN	S	Gender=Fem|Number=Plur	19	nsubj	_	_
14	è	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	19	cop	_	_
15	solo	solo	ADV	B	_	19	advmod	_	_
16	per	per	ADP	E	_	19	case	_	_
17	gli	gli	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	19	det	_	SpaceAfter=No
18	,	,	PUNCT	FF	_	17	punct	_	_
19	SKATE	SKATE	PROPN	SP	_	12	dobj	_	_
20	BORD	BORD	PROPN	SP	_	19	name	_	_
21	ma	ma	CONJ	CC	_	19	cc	_	_
22	quando	quando	SCONJ	CS	_	25	mark	_	_
23	non	non	ADV	BN	PronType=Neg	25	neg	_	_
24	ci	ci	PRON	PC	PronType=Clit	25	expl	_	_
25	sono	essere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	19	advcl	_	_
26	vado	andare	SCONJ	CS	_	28	mark	_	_
27	anche	anche	ADV	B	_	28	advmod	_	_
28	lì	lì	ADV	B	_	25	advmod	_	SpaceAfter=No
29	.	.	PUNCT	FS	_	6	punct	_	_

# sent_id = 7
# text = Al mio compleano ho ricevuto il monopatino pro. E vorei imparare andare allo SKATA PARK con la BMX e fare i salti in giro.
1-2	Al	_	_	_	_	_	_	_	_
1	A	a	ADP	E	_	4	case	_	_
2	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	4	det	_	_
3	mio	mio	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	4	det:poss	_	_
4	compleano	compleano	NOUN	S	Gender=Masc|Number=Sing	6	nmod	_	_
5	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	6	aux	_	_
6	ricevuto	ricevere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
7	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	8	det	_	_
8	monopatino	monopatino	NOUN	S	Gender=Masc|Number=Sing	6	dobj	_	SpacesAfter=\s\s
9	pro	pro	ADJ	A	Gender=Masc|Number=Sing	8	amod	_	SpaceAfter=No
10	.	.	PUNCT	FS	_	6	punct	_	_
11	E	e	CONJ	CC	_	6	cc	_	_
12	vorei	voere	AUX	VM	Mood=Cnd|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	13	aux	_	_
13	imparare	imparare	VERB	V	VerbForm=Inf	6	conj	_	_
14	andare	andare	VERB	V	VerbForm=Inf	13	ccomp	_	_
15-16	allo	_	_	_	_	_	_	_	_
15	a	a	ADP	E	_	17	case	_	_
16	lo	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	17	det	_	_
17	SKATA	SKATA	PROPN	SP	_	14	nmod	_	_
18	PARK	Park	PROPN	SP	_	17	name	_	_
19	con	con	ADP	E	_	21	case	_	_
20	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	21	det	_	_
21	BMX	BMX	PROPN	SP	_	14	nmod	_	_
22	e	e	CONJ	CC	_	14	cc	_	_
23	fare	fare	VERB	V	VerbForm=Inf	14	conj	_	_
24	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	25	det	_	_
25	salti	salto	NOUN	S	Gender=Masc|Number=Plur	23	dobj	_	_
26	in	in	ADP	E	_	27	case	_	_
27	giro	giro	NOUN	S	Gender=Masc|Number=Sing	23	nmod	_	SpaceAfter=No
28	.	.	PUNCT	FS	_	6	punct	_	_

# sent_id = 8
# text = E riesco già scivolare sul ferro.
1	E	e	CONJ	CC	_	2	cc	_	_
2	riesco	riuscire	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	0	root	_	_
3	già	già	ADV	B	_	4	advmod	_	_
4	scivolare	scivolare	VERB	V	VerbForm=Inf	2	conj	_	_
5-6	sul	_	_	_	_	_	_	_	_
5	su	su	ADP	E	_	7	case	_	_
6	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	7	det	_	_
7	ferro	ferro	NOUN	S	Gender=Masc|Number=Sing	4	nmod	_	SpaceAfter=No
8	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 9
# text = E riusciamo saltare 6 scalini.
1	E	e	CONJ	CC	_	2	cc	_	_
2	riusciamo	riuscire	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	0	root	_	_
3	saltare	saltare	VERB	V	VerbForm=Inf	2	conj	_	_
4	6	6	NUM	N	NumType=Card	5	nummod	_	_
5	scalini	scalino	NOUN	S	Gender=Masc|Number=Plur	3	dobj	_	SpaceAfter=No
6	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 10
# text = Là dove ci sono i SKATE BORD ci sono tipo delle onde e dentro c’era acqua e non lo sapevo che c’era l’acqua e mi sono bagnato i miei pantaloni.
1	Là	là	ADV	B	_	9	advmod	_	_
2	dove	dove	ADV	B	_	4	advmod	_	_
3	ci	ci	PRON	PC	PronType=Clit	4	expl	_	_
4	sono	essere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	1	acl:relcl	_	_
5	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	6	det	_	_
6	SKATE	SKATE	PROPN	SP	_	4	nsubj	_	_
7	BORD	BORD	PROPN	SP	_	6	name	_	_
8	ci	ci	PRON	PC	PronType=Clit	9	expl	_	_
9	sono	essere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
10	tipo	tipo	NOUN	S	Gender=Masc|Number=Sing	9	nsubj	_	_
11-12	delle	_	_	_	_	_	_	_	_
11	di	di	ADP	E	_	13	case	_	_
12	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	13	det	_	_
13	onde	onda	NOUN	S	Gender=Fem|Number=Plur	10	nmod	_	_
14	e	e	CONJ	CC	_	9	cc	_	_
15	dentro	dentro	ADV	B	_	16	advmod	_	_
16	c’era	c’ero	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	9	conj	_	_
17	acqua	acqua	NOUN	S	Gender=Fem|Number=Sing	16	dobj	_	_
18	e	e	CONJ	CC	_	9	cc	_	_
19	non	non	ADV	BN	PronType=Neg	21	neg	_	_
20	lo	lo	PRON	PC	Gender=Masc|Number=Sing|Person=3|PronType=Clit	21	dobj	_	_
21	sapevo	sapere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	9	conj	_	_
22	che	che	SCONJ	CS	_	23	mark	_	_
23	c’era	c’erare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	21	ccomp	_	_
24	l’acqua	l’acqua	ADJ	A	Gender=Fem|Number=Sing	23	xcomp	_	_
25	e	e	CONJ	CC	_	23	cc	_	_
26	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	28	expl	_	_
27	sono	essere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	28	aux	_	_
28	bagnato	bagnare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	23	conj	_	_
29	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	31	det	_	_
30	miei	mio	DET	AP	Gender=Masc|Number=Plur|Poss=Yes|PronType=Prs	31	det:poss	_	_
31	pantaloni	pantalone	NOUN	S	Gender=Masc|Number=Plur	28	nsubj	_	SpaceAfter=No
32	.	.	PUNCT	FS	_	9	punct	_	_

# sent_id = 11
# text = Ho imparato che se qualcuno mi prende in giro non devo ascoltare.
1	Ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	2	aux	_	_
2	imparato	imparare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
3	che	che	SCONJ	CS	_	12	mark	_	_
4	se	se	SCONJ	CS	_	7	mark	_	_
5	qualcuno	qualcuno	PRON	PI	Gender=Masc|Number=Sing|PronType=Ind	7	nsubj	_	_
6	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	7	iobj	_	_
7	prende	prendere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	12	advcl	_	_
8	in	in	ADP	E	_	9	case	_	_
9	giro	giro	NOUN	S	Gender=Masc|Number=Sing	7	nmod	_	_
10	non	non	ADV	BN	PronType=Neg	12	neg	_	_
11	devo	dovere	AUX	VM	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	12	aux	_	_
12	ascoltare	ascoltare	VERB	V	VerbForm=Inf	2	ccomp	_	SpaceAfter=No
13	.	.	PUNCT	FS	_	2	punct	_	SpacesAfter=\r\n\r\n


# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE106.txt
# newpar
# sent_id = 1
# text = La caduta in bicicletta In 5a finalmente potevo andare in bici a scuola, il tragitto era da casa fino a scuola.
1	La	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	2	det	_	_
2	caduta	caduta	NOUN	S	Gender=Fem|Number=Sing	9	nsubj	_	_
3	in	in	ADP	E	_	4	case	_	_
4	bicicletta	bicicletta	NOUN	S	Gender=Fem|Number=Sing	2	nmod	_	SpacesAfter=\s\s
5	In	in	ADP	E	_	6	case	_	_
6	5a	5a	NOUN	S	Gender=Fem|Number=Sing	9	nmod	_	_
7	finalmente	finalmente	ADV	B	_	9	advmod	_	_
8	potevo	potere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	9	aux	_	_
9	andare	andare	VERB	V	VerbForm=Inf	0	root	_	_
10	in	in	ADP	E	_	11	case	_	_
11	bici	bico	NOUN	S	Gender=Masc|Number=Plur	9	nmod	_	_
12	a	a	ADP	E	_	13	case	_	_
13	scuola	scuola	NOUN	S	Gender=Fem|Number=Sing	11	nmod	_	SpaceAfter=No
14	,	,	PUNCT	FF	_	9	punct	_	_
15	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	16	det	_	_
16	tragitto	tragitto	NOUN	S	Gender=Masc|Number=Sing	17	nsubj	_	_
17	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	9	conj	_	_
18	da	da	ADP	E	_	19	case	_	_
19	casa	casa	NOUN	S	Gender=Fem|Number=Sing	17	nmod	_	_
20	fino	fino	ADP	E	_	22	case	_	_
21	a	a	ADP	E	_	20	mwe	_	_
22	scuola	scuola	NOUN	S	Gender=Fem|Number=Sing	17	nmod	_	SpaceAfter=No
23	.	.	PUNCT	FS	_	9	punct	_	_

# sent_id = 2
# text = Ero partito ed ero tutto tranquillo.
1	Ero	essere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	2	aux	_	_
2	partito	partire	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
3	ed	e	CONJ	CC	_	2	cc	_	_
4	ero	essere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	2	conj	_	_
5	tutto	tutto	PRON	PI	Gender=Masc|Number=Sing|PronType=Ind	4	nsubj	_	_
6	tranquillo	tranquillo	ADJ	A	Gender=Masc|Number=Sing	5	amod	_	SpaceAfter=No
7	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 3
# text = quando iniziai a prendere velocità all’ultima curva e nel pezzo dritto curvai troppo tardi e persi il controllo del mio manubrio e, non é finita, uno stupido sasso mi bucó la ruota anteriore, cosí caddi dalla bici e scivolai lungo la strada e per fortuna venni visto.
1	quando	quando	SCONJ	CS	_	2	mark	_	_
2	iniziai	iniziare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Past|VerbForm=Fin	7	advcl	_	_
3	a	a	ADP	E	_	4	mark	_	_
4	prendere	prendere	VERB	V	VerbForm=Inf	2	xcomp	_	_
5	velocità	velocità	NOUN	S	Gender=Fem	4	dobj	_	_
6	all’ultima	all’ultimo	ADJ	A	Gender=Fem|Number=Sing	5	amod	_	_
7	curva	curvo	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
8	e	e	CONJ	CC	_	7	cc	_	_
9-10	nel	_	_	_	_	_	_	_	_
9	in	in	ADP	E	_	11	case	_	_
10	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	11	det	_	_
11	pezzo	pezzo	NOUN	S	Gender=Masc|Number=Sing	13	nmod	_	_
12	dritto	dritto	ADJ	A	Gender=Masc|Number=Sing	11	amod	_	_
13	curvai	curvare	VERB	V	Mood=Ind|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	7	conj	_	_
14	troppo	troppo	ADV	B	_	15	advmod	_	_
15	tardi	tardi	ADV	B	_	13	advmod	_	_
16	e	e	CONJ	CC	_	13	cc	_	_
17	persi	pergere	VERB	V	VerbForm=Inf	13	conj	_	_
18	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	19	det	_	_
19	controllo	controllo	NOUN	S	Gender=Masc|Number=Sing	17	dobj	_	_
20-21	del	_	_	_	_	_	_	_	_
20	di	di	ADP	E	_	23	case	_	_
21	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	23	det	_	_
22	mio	mio	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	23	det:poss	_	_
23	manubrio	manubrio	NOUN	S	Gender=Masc|Number=Sing	19	nmod	_	_
24	e	e	CONJ	CC	_	7	cc	_	SpaceAfter=No
25	,	,	PUNCT	FF	_	7	punct	_	_
26	non	non	ADV	BN	PronType=Neg	28	neg	_	_
27	é	é	SCONJ	CS	_	28	mark	_	_
28	finita	finire	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	7	advcl	_	SpaceAfter=No
29	,	,	PUNCT	FF	_	28	punct	_	_
30	uno	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	31	det	_	_
31	stupido	stupido	NOUN	S	Gender=Masc|Number=Sing	28	nsubj	_	_
32	sasso	sasso	ADJ	A	Gender=Masc|Number=Sing	31	amod	_	_
33	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	34	iobj	_	_
34	bucó	bucó	PROPN	SP	_	31	nmod	_	_
35	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	36	det	_	_
36	ruota	ruota	NOUN	S	Gender=Fem|Number=Sing	31	nmod	_	_
37	anteriore	anteriore	ADJ	A	Number=Sing	36	amod	_	SpaceAfter=No
38	,	,	PUNCT	FF	_	36	punct	_	_
39	cosí	cosí	PROPN	SP	_	36	conj	_	_
40	caddi	caddo	NOUN	S	Gender=Masc|Number=Plur	39	root	_	_
41-42	dalla	_	_	_	_	_	_	_	_
41	da	da	ADP	E	_	43	case	_	_
42	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	43	det	_	_
43	bici	bico	NOUN	S	Gender=Masc|Number=Plur	40	nmod	_	_
44	e	e	CONJ	CC	_	43	cc	_	_
45	scivolai	scivolai	NOUN	S	Gender=Masc|Number=Plur	43	conj	_	_
46	lungo	lungo	ADP	E	_	48	case	_	_
47	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	48	det	_	_
48	strada	strada	NOUN	S	Gender=Fem|Number=Sing	40	nmod	_	_
49	e	e	CONJ	CC	_	48	cc	_	_
50	per	per	ADP	E	_	51	case	_	_
51	fortuna	fortuna	NOUN	S	Gender=Fem|Number=Sing	53	nmod	_	_
52	venni	venne	ADJ	A	Number=Plur	51	amod	_	_
53	visto	vedere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	48	conj	_	SpaceAfter=No
54	.	.	PUNCT	FS	_	40	punct	_	_

# sent_id = 4
# text = Non riuscivo piú a respirare e vedevo tutto rosso e bianco.
1	Non	non	ADV	BN	PronType=Neg	2	neg	_	_
2	riuscivo	riuscire	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	0	root	_	_
3	piú	piú	PROPN	SP	_	2	dobj	_	_
4	a	a	ADP	E	_	5	mark	_	_
5	respirare	respirare	VERB	V	VerbForm=Inf	2	xcomp	_	_
6	e	e	CONJ	CC	_	2	cc	_	_
7	vedevo	vedere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	2	conj	_	_
8	tutto	tutto	PRON	PI	Gender=Masc|Number=Sing|PronType=Ind	7	nsubj	_	_
9	rosso	rosso	ADJ	A	Gender=Masc|Number=Sing	8	amod	_	_
10	e	e	CONJ	CC	_	9	cc	_	_
11	bianco	bianco	ADJ	A	Gender=Masc|Number=Sing	9	conj	_	SpaceAfter=No
12	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 5
# text = Poi dentro la scuola mi medicarono un po’ e chiamarono mia mamma che mi portó al pronto soccorso dove mi medicarono ma la notizia peggiore fu quando mi dissero che dovevo essere cucito.
1	Poi	poi	ADV	B	_	4	advmod	_	_
2	dentro	dentro	ADP	E	_	4	case	_	_
3	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	4	det	_	_
4	scuola	scuola	NOUN	S	Gender=Fem|Number=Sing	6	nmod	_	_
5	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	6	iobj	_	_
6	medicarono	medicare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
7	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	8	det	_	_
8	po’	ipo	PRON	PC	PronType=Clit	6	advmod	_	_
9	e	e	CONJ	CC	_	6	cc	_	_
10	chiamarono	chiamare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Past|VerbForm=Fin	6	conj	_	_
11	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	12	det:poss	_	_
12	mamma	mamma	NOUN	S	Gender=Fem|Number=Sing	13	dobj	_	_
13	che	che	PRON	PR	PronType=Rel	10	dobj	_	_
14	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	13	nsubj	_	_
15	portó	portó	PROPN	SP	_	14	nmod	_	_
16-17	al	_	_	_	_	_	_	_	_
16	a	a	ADP	E	_	19	case	_	_
17	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	19	det	_	_
18	pronto	pronto	ADJ	A	Gender=Masc|Number=Sing	19	amod	_	_
19	soccorso	soccorso	NOUN	S	Gender=Masc|Number=Sing	15	nmod	_	_
20	dove	dove	ADV	B	_	22	advmod	_	_
21	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	22	iobj	_	_
22	medicarono	medicare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Past|VerbForm=Fin	19	acl:relcl	_	_
23	ma	ma	CONJ	CC	_	22	cc	_	_
24	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	25	det	_	_
25	notizia	notizia	NOUN	S	Gender=Fem|Number=Sing	27	nsubj	_	_
26	peggiore	peggiore	ADJ	A	Degree=Cmp|Number=Sing	25	amod	_	_
27	fu	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	22	conj	_	_
28	quando	quando	SCONJ	CS	_	30	mark	_	_
29	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	30	expl	_	_
30	dissero	dire	VERB	V	Mood=Sub|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	27	advcl	_	_
31	che	che	SCONJ	CS	_	34	mark	_	_
32	dovevo	dovere	AUX	VM	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	34	aux	_	_
33	essere	essere	AUX	VA	VerbForm=Inf	34	auxpass	_	_
34	cucito	cucire	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	30	ccomp	_	SpaceAfter=No
35	.	.	PUNCT	FS	_	27	punct	_	_

# sent_id = 6
# text = Mi addormentarono il mento e iniziarono a cucire, era bruttissimo mi sentivo l’ago passare nel mento e mi misero un telo in faccia per non farmi vedere, poi mi misero un gel per vedere se dentro di me niente era spostato.
1	Mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	2	iobj	_	_
2	addormentarono	addormentare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
3	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	4	det	_	_
4	mento	mento	NOUN	S	Gender=Masc|Number=Sing	2	dobj	_	_
5	e	e	CONJ	CC	_	2	cc	_	_
6	iniziarono	iniziare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Past|VerbForm=Fin	2	conj	_	_
7	a	a	ADP	E	_	8	mark	_	_
8	cucire	cucire	VERB	V	VerbForm=Inf	6	xcomp	_	SpaceAfter=No
9	,	,	PUNCT	FF	_	6	punct	_	_
10	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	11	cop	_	_
11	bruttissimo	brutto	ADJ	A	Degree=Abs|Gender=Masc|Number=Sing	6	conj	_	_
12	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	13	iobj	_	_
13	sentivo	sentire	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	11	csubj	_	_
14	l’ago	l’ago	ADJ	A	Gender=Masc|Number=Sing	13	xcomp	_	_
15	passare	passare	VERB	V	VerbForm=Inf	13	xcomp	_	_
16-17	nel	_	_	_	_	_	_	_	_
16	in	in	ADP	E	_	18	case	_	_
17	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	18	det	_	_
18	mento	mento	NOUN	S	Gender=Masc|Number=Sing	15	nmod	_	_
19	e	e	CONJ	CC	_	13	cc	_	_
20	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	21	iobj	_	_
21	misero	midere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Past|VerbForm=Fin	13	conj	_	_
22	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	23	det	_	_
23	telo	telo	NOUN	S	Gender=Masc|Number=Sing	21	dobj	_	_
24	in	in	ADP	E	_	25	case	_	_
25	faccia	faccia	NOUN	S	Gender=Fem|Number=Sing	21	nmod	_	_
26	per	per	ADP	E	_	28	case	_	_
27	non	non	ADV	BN	PronType=Neg	28	neg	_	_
28	farmi	arma|farme	ADJ	A	Number=Plur	21	nmod	_	_
29	vedere	vedere	VERB	V	VerbForm=Inf	28	ccomp	_	SpaceAfter=No
30	,	,	PUNCT	FF	_	21	punct	_	_
31	poi	poi	ADV	B	_	33	advmod	_	_
32	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	33	iobj	_	_
33	misero	midere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Past|VerbForm=Fin	21	conj	_	_
34	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	35	det	_	_
35	gel	gel	NOUN	S	Gender=Masc	33	dobj	_	_
36	per	per	ADP	E	_	37	mark	_	_
37	vedere	vedere	VERB	V	VerbForm=Inf	33	advcl	_	_
38	se	se	SCONJ	CS	_	44	mark	_	_
39	dentro	dentro	ADV	B	_	44	advmod	_	_
40	di	di	ADP	E	_	41	case	_	_
41	me	me	PRON	PE	Number=Sing|Person=1|PronType=Prs	39	nmod	_	_
42	niente	niente	PRON	PI	Gender=Masc|Number=Sing|PronType=Ind	44	nsubj	_	_
43	era	essere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	44	aux	_	_
44	spostato	spostare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	37	advcl	_	SpaceAfter=No
45	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 7
# text = In un’altra stanza mi fecero la radiografia alla mascella perché me la sentivo tutta spostata.
1	In	in	ADP	E	_	3	case	_	_
2	un’altra	un’altro	DET	DI	Gender=Fem|Number=Sing|PronType=Ind	3	det	_	_
3	stanza	stanza	NOUN	S	Gender=Fem|Number=Sing	5	nmod	_	_
4	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	5	iobj	_	_
5	fecero	fare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
6	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	7	det	_	_
7	radiografia	radiografia	NOUN	S	Gender=Fem|Number=Sing	5	dobj	_	_
8-9	alla	_	_	_	_	_	_	_	_
8	a	a	ADP	E	_	10	case	_	_
9	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	10	det	_	_
10	mascella	mascella	NOUN	S	Gender=Fem|Number=Sing	5	nmod	_	_
11	perché	perché	SCONJ	CS	_	14	mark	_	_
12	me	me	PRON	PC	Number=Sing|Person=1|PronType=Clit	14	expl	_	_
13	la	il	PRON	PC	Gender=Fem|Number=Sing|Person=3|PronType=Clit	14	dobj	_	_
14	sentivo	sentivo	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	5	advcl	_	_
15	tutta	tutto	DET	T	Gender=Fem|Number=Sing	16	det	_	_
16	spostata	spostato	NOUN	S	Gender=Fem|Number=Sing	14	nsubj	_	SpaceAfter=No
17	.	.	PUNCT	FS	_	5	punct	_	_

# sent_id = 8
# text = Quando tornai a casa non riuscivo a masticare niente per due o piú giorni, e non riuscivo a muovermi.
1	Quando	quando	SCONJ	CS	_	2	mark	_	_
2	tornai	tornare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Past|VerbForm=Fin	6	advcl	_	_
3	a	a	ADP	E	_	4	case	_	_
4	casa	casa	NOUN	S	Gender=Fem|Number=Sing	2	nmod	_	_
5	non	non	ADV	BN	PronType=Neg	6	neg	_	_
6	riuscivo	riuscire	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	0	root	_	_
7	a	a	ADP	E	_	8	mark	_	_
8	masticare	masticare	VERB	V	VerbForm=Inf	6	xcomp	_	_
9	niente	niente	PRON	PI	Gender=Masc|Number=Sing|PronType=Ind	8	dobj	_	_
10	per	per	ADP	E	_	14	case	_	_
11	due	due	NUM	N	NumType=Card	14	nummod	_	_
12	o	o	CONJ	CC	_	11	cc	_	_
13	piú	piú	PROPN	SP	_	11	conj	_	_
14	giorni	giorno	NOUN	S	Gender=Masc|Number=Plur	8	nmod	_	SpaceAfter=No
15	,	,	PUNCT	FF	_	6	punct	_	_
16	e	e	CONJ	CC	_	6	cc	_	_
17	non	non	ADV	BN	PronType=Neg	18	neg	_	_
18	riuscivo	riuscire	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	6	conj	_	_
19	a	a	ADP	E	_	20	case	_	_
20	muovermi	muovermo	NOUN	S	Gender=Masc|Number=Plur	18	nmod	_	SpaceAfter=No
21	.	.	PUNCT	FS	_	6	punct	_	_

# sent_id = 9
# text = Da questo episodio ho imparato di rimanere sempre concentrato e di non accelerare in discesa.
1	Da	da	ADP	E	_	3	case	_	_
2	questo	questo	DET	DD	Gender=Masc|Number=Sing|PronType=Dem	3	det	_	_
3	episodio	episodio	NOUN	S	Gender=Masc|Number=Sing	5	nmod	_	_
4	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	imparato	imparare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
6	di	di	ADP	E	_	7	mark	_	_
7	rimanere	rimanere	VERB	V	VerbForm=Inf	5	xcomp	_	_
8	sempre	sempre	ADV	B	_	9	advmod	_	_
9	concentrato	concentrato	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	7	xcomp	_	_
10	e	e	CONJ	CC	_	9	cc	_	_
11	di	di	ADP	E	_	13	mark	_	_
12	non	non	ADV	BN	PronType=Neg	13	neg	_	_
13	accelerare	accelerare	VERB	V	VerbForm=Inf	9	conj	_	_
14	in	in	ADP	E	_	15	case	_	_
15	discesa	discesa	NOUN	S	Gender=Fem|Number=Sing	13	nmod	_	SpaceAfter=No
16	.	.	PUNCT	FS	_	5	punct	_	SpacesAfter=\r\n\r\n


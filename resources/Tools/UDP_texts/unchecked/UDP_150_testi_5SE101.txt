# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE101.txt
# newpar
# sent_id = 1
# text = A ritmica All’inizio dell’anno scolastico inizia la ritmica (e sono gia tre anni che lo faccio).
1	A	a	ADP	E	_	2	case	_	_
2	ritmica	ritmico	NOUN	S	Gender=Fem|Number=Sing	4	nmod	_	SpacesAfter=\s\s
3	All’inizio	All’inizio	PROPN	SP	_	4	nsubj	_	_
4	dell’anno	dell’e	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Fut|VerbForm=Fin	0	root	_	_
5	scolastico	scolastico	ADJ	A	Gender=Masc|Number=Sing	4	xcomp	_	_
6	inizia	iniziare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	4	advcl	_	_
7	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	8	det	_	_
8	ritmica	ritmico	NOUN	S	Gender=Fem|Number=Sing	6	nsubj	_	_
9	(	(	PUNCT	FB	_	14	punct	_	SpaceAfter=No
10	e	e	CONJ	CC	_	6	cc	_	_
11	sono	essere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	14	cop	_	_
12	gia	giare	ADV	B	_	14	advmod	_	_
13	tre	tre	NUM	N	NumType=Card	14	nummod	_	_
14	anni	anno	NOUN	S	Gender=Masc|Number=Plur	6	conj	_	_
15	che	che	PRON	PR	PronType=Rel	17	dobj	_	_
16	lo	lo	PRON	PC	Gender=Masc|Number=Sing|Person=3|PronType=Clit	17	dobj	_	_
17	faccio	fare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	14	acl:relcl	_	SpaceAfter=No
18	)	)	PUNCT	FB	_	14	punct	_	SpaceAfter=No
19	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 2
# text = E imparo velocemente un nuovo balletto sempre piú difficile e complicato.
1	E	e	CONJ	CC	_	2	cc	_	_
2	imparo	imparare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	0	root	_	_
3	velocemente	velocemente	ADV	B	_	2	advmod	_	_
4	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	6	det	_	_
5	nuovo	nuovo	ADJ	A	Gender=Masc|Number=Sing	6	amod	_	_
6	balletto	balletto	NOUN	S	Gender=Masc|Number=Sing	2	dobj	_	_
7	sempre	sempre	ADV	B	_	2	advmod	_	_
8	piú	piú	PROPN	SP	_	2	xcomp	_	_
9	difficile	difficile	ADJ	A	Number=Sing	8	amod	_	_
10	e	e	CONJ	CC	_	9	cc	_	_
11	complicato	complicato	ADJ	A	Gender=Masc|Number=Sing	9	conj	_	SpaceAfter=No
12	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 3
# text = Ogni venerdí pomeriggio vado a ritmica a Losone perché devo allenarmi per le gare, ma non quasi sempre mi impegno totalmente.
1	Ogni	ogni	DET	DI	Number=Sing|PronType=Ind	2	det	_	_
2	venerdí	venerdí	PROPN	SP	_	0	root	_	_
3	pomeriggio	pomeriggio	NOUN	S	Gender=Masc|Number=Sing	2	nmod	_	_
4	vado	andare	ADJ	A	Gender=Masc|Number=Sing	3	amod	_	_
5	a	a	ADP	E	_	6	case	_	_
6	ritmica	ritmico	NOUN	S	Gender=Fem|Number=Sing	2	nmod	_	_
7	a	a	ADP	E	_	8	case	_	_
8	Losone	Losone	PROPN	SP	_	6	nmod	_	_
9	perché	perché	SCONJ	CS	_	10	mark	_	_
10	devo	dovere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	2	advcl	_	_
11	allenarmi	arma|allenarme	NOUN	S	Gender=Fem|Number=Plur	10	nsubj	_	_
12	per	per	ADP	E	_	14	case	_	_
13	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	14	det	_	_
14	gare	gara	NOUN	S	Gender=Fem|Number=Plur	11	nmod	_	SpaceAfter=No
15	,	,	PUNCT	FF	_	10	punct	_	_
16	ma	ma	CONJ	CC	_	10	cc	_	_
17	non	non	ADV	BN	PronType=Neg	18	neg	_	_
18	quasi	quasi	ADV	B	_	19	advmod	_	_
19	sempre	sempre	ADV	B	_	21	advmod	_	_
20	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	21	iobj	_	_
21	impegno	impegre	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	10	conj	_	_
22	totalmente	totalmente	ADV	B	_	21	advmod	_	SpaceAfter=No
23	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 4
# text = Ma quest’anno alle gare ottengo risultati un po’ meno belli rispetto all’anno scorso, inseguito sono riuscita allegramente a passare il test 2a.
1	Ma	ma	CONJ	CC	_	2	cc	_	_
2	quest’anno	quest’e	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
3-4	alle	_	_	_	_	_	_	_	_
3	a	a	ADP	E	_	5	case	_	_
4	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	5	det	_	_
5	gare	gara	NOUN	S	Gender=Fem|Number=Plur	2	nmod	_	_
6	ottengo	ottenere	ADP	E	_	7	case	_	_
7	risultati	risultato	NOUN	S	Gender=Masc|Number=Plur	5	nmod	_	_
8	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	9	det	_	_
9	po’	ipo	PRON	PC	PronType=Clit	2	nsubj	_	_
10	meno	meno	ADV	B	_	11	advmod	_	_
11	belli	bello	ADJ	A	Gender=Masc|Number=Plur	9	amod	_	_
12	rispetto	rispetto	ADP	E	_	13	case	_	_
13	all’anno	all’anno	NOUN	S	Gender=Masc|Number=Sing	11	nmod	_	_
14	scorso	scorso	ADJ	A	Gender=Masc|Number=Sing	13	amod	_	SpaceAfter=No
15	,	,	PUNCT	FF	_	2	punct	_	_
16	inseguito	inseguito	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	2	advcl	_	_
17	sono	essere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	18	aux	_	_
18	riuscita	riuscire	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	16	conj	_	_
19	allegramente	allegramente	ADV	B	_	18	advmod	_	_
20	a	a	ADP	E	_	21	mark	_	_
21	passare	passare	VERB	V	VerbForm=Inf	18	xcomp	_	_
22	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	23	det	_	_
23	test	test	NOUN	S	Gender=Masc	21	dobj	_	_
24	2a	2o	ADJ	A	Gender=Fem|Number=Sing	23	amod	_	SpaceAfter=No
25	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 5
# text = Ho capito che se mi impegno di piú riesco ad ottenere un risultato piú bello.
1	Ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	2	aux	_	_
2	capito	capire	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
3	che	che	SCONJ	CS	_	9	mark	_	_
4	se	se	SCONJ	CS	_	9	mark	_	_
5	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	9	dobj	_	_
6	impegno	impegre	NOUN	S	Gender=Masc|Number=Sing	9	nmod	_	_
7	di	di	ADP	E	_	8	case	_	_
8	piú	piú	PROPN	SP	_	6	nmod	_	_
9	riesco	riuscire	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	2	ccomp	_	_
10	ad	a	ADP	E	_	11	mark	_	_
11	ottenere	ottenere	VERB	V	VerbForm=Inf	9	xcomp	_	_
12	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	13	det	_	_
13	risultato	risultato	NOUN	S	Gender=Masc|Number=Sing	11	dobj	_	_
14	piú	piú	PROPN	SP	_	13	nmod	_	_
15	bello	bello	ADJ	A	Gender=Masc|Number=Sing	14	amod	_	SpaceAfter=No
16	.	.	PUNCT	FS	_	9	punct	_	SpacesAfter=\r\n\r\n


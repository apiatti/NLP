# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE72.txt
# newpar
# sent_id = 1
# text = Cenerentola C’era una volta un ricco uomo vedovo con una figlia.
1	Cenerentola	cenerentola	PROPN	SP	_	7	nsubj	_	SpacesAfter=\s\s
2	C’era	C’era	PROPN	SP	_	1	name	_	_
3	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	4	det	_	_
4	volta	volta	NOUN	S	Gender=Fem|Number=Sing	7	nmod	_	_
5	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	7	det	_	_
6	ricco	ricco	ADJ	A	Gender=Masc|Number=Sing	7	amod	_	_
7	uomo	uomo	NOUN	S	Gender=Masc|Number=Sing	0	root	_	_
8	vedovo	vedovo	NOUN	S	Gender=Masc|Number=Sing	7	compound	_	_
9	con	con	ADP	E	_	11	case	_	_
10	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	11	det	_	_
11	figlia	figlia	NOUN	S	Gender=Fem|Number=Sing	7	nmod	_	SpaceAfter=No
12	.	.	PUNCT	FS	_	7	punct	_	_

# sent_id = 2
# text = Decise di sposarsi con una donna con due figlie.
1	Decise	deciso	NOUN	S	Gender=Fem|Number=Plur	0	root	_	_
2	di	di	ADP	E	_	3	mark	_	_
3-4	sposarsi	_	_	_	_	_	_	_	_
3	sposar	sposare	VERB	V	VerbForm=Inf	1	acl	_	_
4	si	si	PRON	PC	Person=3|PronType=Clit	3	expl	_	_
5	con	con	ADP	E	_	7	case	_	_
6	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	7	det	_	_
7	donna	donna	NOUN	S	Gender=Fem|Number=Sing	3	nmod	_	_
8	con	con	ADP	E	_	10	case	_	_
9	due	due	NUM	N	NumType=Card	10	nummod	_	_
10	figlie	figlio	NOUN	S	Gender=Fem|Number=Plur	3	nmod	_	SpaceAfter=No
11	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 3
# text = Un giorno il padre morì, e la matrigna prese l’eredità.
1	Un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	5	nmod	_	_
3	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	4	det	_	_
4	padre	padre	NOUN	S	Gender=Masc|Number=Sing	5	nsubj	_	_
5	morì	morire	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	SpaceAfter=No
6	,	,	PUNCT	FF	_	5	punct	_	_
7	e	e	CONJ	CC	_	5	cc	_	_
8	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	9	det	_	_
9	matrigna	matrigna	NOUN	S	Gender=Fem|Number=Sing	10	nsubj	_	_
10	prese	prendere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	5	conj	_	_
11	l’eredità	l’eredità	NOUN	S	Gender=Fem	10	dobj	_	SpaceAfter=No
12	.	.	PUNCT	FS	_	5	punct	_	_

# sent_id = 4
# text = Costringeva la figlia a pulire, mentre le sue figlie davano ordini alla povera Cenerentola.
1	Costringeva	Costringere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
2	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	3	det	_	_
3	figlia	figlio	NOUN	S	Gender=Fem|Number=Sing	1	dobj	_	_
4	a	a	ADP	E	_	5	mark	_	_
5	pulire	pulire	VERB	V	VerbForm=Inf	1	xcomp	_	SpaceAfter=No
6	,	,	PUNCT	FF	_	1	punct	_	_
7	mentre	mentre	SCONJ	CS	_	11	mark	_	_
8	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	10	det	_	_
9	sue	suo	DET	AP	Gender=Fem|Number=Plur|Poss=Yes|PronType=Prs	10	det:poss	_	_
10	figlie	figlio	NOUN	S	Gender=Fem|Number=Plur	11	nsubj	_	SpacesAfter=\s\s\s
11	davano	dare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	1	advcl	_	_
12	ordini	ordine	NOUN	S	Gender=Masc|Number=Plur	11	dobj	_	_
13-14	alla	_	_	_	_	_	_	_	_
13	a	a	ADP	E	_	15	case	_	_
14	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	15	det	_	_
15	povera	povero	NOUN	S	Gender=Fem|Number=Sing	11	nmod	_	_
16	Cenerentola	cenerentola	PROPN	SP	_	15	nmod	_	SpaceAfter=No
17	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 5
# text = Una mattina arrivò una lettera, con l’invito al ballo dell principe permesso alle fanciulle dell reame.
1	Una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	2	det	_	_
2	mattina	mattina	NOUN	S	Gender=Fem|Number=Sing	3	nsubj	_	_
3	arrivò	arrivare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
4	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	5	det	_	_
5	lettera	lettera	NOUN	S	Gender=Fem|Number=Sing	3	dobj	_	SpaceAfter=No
6	,	,	PUNCT	FF	_	5	punct	_	SpacesAfter=\s\s\s
7	con	con	ADP	E	_	8	case	_	_
8	l’invito	l’invito	NOUN	S	Gender=Masc|Number=Sing	5	nmod	_	_
9-10	al	_	_	_	_	_	_	_	_
9	a	a	ADP	E	_	11	case	_	_
10	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	11	det	_	_
11	ballo	ballo	NOUN	S	Gender=Masc|Number=Sing	8	nmod	_	SpacesAfter=\s\s\s
12	dell	dell	ADJ	A	Gender=Masc|Number=Sing	11	amod	_	_
13	principe	principe	ADJ	A	Number=Sing	11	amod	_	_
14	permesso	permettere	NOUN	S	Gender=Masc|Number=Sing	11	compound	_	_
15-16	alle	_	_	_	_	_	_	_	_
15	a	a	ADP	E	_	17	case	_	_
16	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	17	det	_	_
17	fanciulle	fanciullo	NOUN	S	Gender=Fem|Number=Plur	14	nmod	_	_
18	dell	dell	NOUN	S	_	17	compound	_	_
19	reame	reame	ADJ	A	Gender=Fem|Number=Plur	18	amod	_	SpaceAfter=No
20	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 6
# text = Le sorellastre e la matrigna darono tanto lavoro per non farla andare al ballo.
1	Le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	2	det	_	_
2	sorellastre	sorellastra	NOUN	S	Gender=Fem|Number=Plur	6	nsubj	_	_
3	e	e	CONJ	CC	_	2	cc	_	_
4	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	5	det	_	_
5	matrigna	matrigna	NOUN	S	Gender=Fem|Number=Sing	2	conj	_	_
6	darono	dare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
7	tanto	tanto	DET	DI	Gender=Masc|Number=Sing|PronType=Ind	8	det	_	_
8	lavoro	lavoro	NOUN	S	Gender=Masc|Number=Sing	6	dobj	_	_
9	per	per	ADP	E	_	11	mark	_	_
10	non	non	ADV	BN	PronType=Neg	11	neg	_	_
11-12	farla	_	_	_	_	_	_	_	_
11	far	fare	VERB	V	VerbForm=Inf	6	advcl	_	_
12	la	lo	PRON	PC	Gender=Fem|Number=Sing|Person=3|PronType=Clit	11	dobj	_	_
13	andare	andare	VERB	V	VerbForm=Inf	11	ccomp	_	_
14-15	al	_	_	_	_	_	_	_	_
14	a	a	ADP	E	_	16	case	_	_
15	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	16	det	_	_
16	ballo	ballo	NOUN	S	Gender=Masc|Number=Sing	13	nmod	_	SpaceAfter=No
17	.	.	PUNCT	FS	_	6	punct	_	_

# sent_id = 7
# text = Il sogno di cenerentola era di ballare con un principe, ma non smetteva di credere.
1	Il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	sogno	sogno	NOUN	S	Gender=Masc|Number=Sing	5	nsubj	_	_
3	di	di	ADP	E	_	4	case	_	_
4	cenerentola	cenerentola	NOUN	S	Gender=Fem|Number=Sing	2	nmod	_	_
5	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
6	di	di	ADP	E	_	7	mark	_	_
7	ballare	ballare	VERB	V	VerbForm=Inf	5	ccomp	_	_
8	con	con	ADP	E	_	10	case	_	_
9	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	10	det	_	_
10	principe	principe	NOUN	S	Gender=Masc|Number=Sing	7	nmod	_	SpaceAfter=No
11	,	,	PUNCT	FF	_	5	punct	_	_
12	ma	ma	CONJ	CC	_	5	cc	_	_
13	non	non	ADV	BN	PronType=Neg	14	neg	_	_
14	smetteva	smettere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	5	conj	_	_
15	di	di	ADP	E	_	16	mark	_	_
16	credere	credere	VERB	V	VerbForm=Inf	14	xcomp	_	SpaceAfter=No
17	.	.	PUNCT	FS	_	5	punct	_	_

# sent_id = 8
# text = Andate al ballo corse in cantina a piangere, quando apparve la fata Madrina.
1	Andate	andare	VERB	V	Gender=Fem|Number=Plur|Tense=Past|VerbForm=Part	0	root	_	_
2-3	al	_	_	_	_	_	_	_	_
2	a	a	ADP	E	_	4	case	_	_
3	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	4	det	_	_
4	ballo	ballo	NOUN	S	Gender=Masc|Number=Sing	1	nmod	_	_
5	corse	correre	NOUN	S	Gender=Fem|Number=Plur	4	compound	_	_
6	in	in	ADP	E	_	7	case	_	_
7	cantina	cantina	NOUN	S	Gender=Fem|Number=Sing	1	nmod	_	_
8	a	a	ADP	E	_	9	mark	_	_
9	piangere	piangere	VERB	V	VerbForm=Inf	7	acl	_	SpaceAfter=No
10	,	,	PUNCT	FF	_	1	punct	_	_
11	quando	quando	SCONJ	CS	_	12	mark	_	_
12	apparve	apparire	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	1	advcl	_	_
13	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	14	det	_	_
14	fata	fata	NOUN	S	Gender=Fem|Number=Sing	12	dobj	_	_
15	Madrina	madrina	PROPN	SP	_	14	nmod	_	SpaceAfter=No
16	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 9
# text = Cenerentola voleva andare al ballo e chiese di procurarli un vestito adatto.
1	Cenerentola	cenerentola	PROPN	SP	_	3	nsubj	_	_
2	voleva	volere	AUX	VM	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	3	aux	_	_
3	andare	andare	VERB	V	VerbForm=Inf	0	root	_	_
4-5	al	_	_	_	_	_	_	_	_
4	a	a	ADP	E	_	6	case	_	_
5	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	6	det	_	_
6	ballo	ballo	NOUN	S	Gender=Masc|Number=Sing	3	nmod	_	_
7	e	e	CONJ	CC	_	6	cc	_	_
8	chiese	chiese	NOUN	S	Number=Sing	6	conj	_	_
9	di	di	ADP	E	_	10	case	_	_
10	procurarli	procurarlare	NOUN	S	Gender=Masc|Number=Plur	8	nmod	_	_
11	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	12	det	_	_
12	vestito	vestito	NOUN	S	Gender=Masc|Number=Sing	3	dobj	_	_
13	adatto	adatto	ADJ	A	Gender=Masc|Number=Sing	12	amod	_	SpaceAfter=No
14	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 10
# text = La fata fece un vestito azzurro e le raccomando di rientrare a mezzanotte se no tutto svaniva.
1	La	la	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	2	det	_	_
2	fata	fare	NOUN	S	Gender=Fem|Number=Sing	3	nsubj	_	_
3	fece	fare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
4	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	5	det	_	_
5	vestito	vestito	NOUN	S	Gender=Masc|Number=Sing	3	dobj	_	_
6	azzurro	azzurro	ADJ	A	Gender=Masc|Number=Sing	5	amod	_	_
7	e	e	CONJ	CC	_	3	cc	_	_
8	le	le	PRON	PC	Gender=Fem|Number=Sing|Person=3|PronType=Clit	9	dobj	_	_
9	raccomando	raccomare	VERB	V	VerbForm=Ger	3	conj	_	_
10	di	di	ADP	E	_	11	mark	_	_
11	rientrare	rientrare	VERB	V	VerbForm=Inf	9	xcomp	_	_
12	a	a	ADP	E	_	13	case	_	_
13	mezzanotte	mezzanotte	NOUN	S	Gender=Fem|Number=Sing	11	nmod	_	_
14	se	se	SCONJ	CS	_	17	mark	_	_
15	no	no	ADV	BN	PronType=Neg	17	neg	_	_
16	tutto	tutto	PRON	PI	Gender=Masc|Number=Sing|PronType=Ind	17	nsubj	_	_
17	svaniva	svanivo	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	11	advcl	_	SpaceAfter=No
18	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 11
# text = Andata al castello, il principe si innamoro e chiese di ballare.
1	Andata	andare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
2-3	al	_	_	_	_	_	_	_	_
2	a	a	ADP	E	_	4	case	_	_
3	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	4	det	_	_
4	castello	castello	NOUN	S	Gender=Masc|Number=Sing	1	nmod	_	SpaceAfter=No
5	,	,	PUNCT	FF	_	1	punct	_	_
6	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	7	det	_	_
7	principe	principe	NOUN	S	Gender=Masc|Number=Sing	13	nsubj	_	_
8	si	si	PRON	PC	Person=3|PronType=Clit	13	expl	_	_
9	innamoro	innamoro	ADJ	A	Gender=Masc|Number=Sing	8	amod	_	_
10	e	e	CONJ	CC	_	9	cc	_	_
11	chiese	chiese	ADJ	A	Number=Sing	9	conj	_	_
12	di	di	ADP	E	_	13	mark	_	_
13	ballare	ballare	VERB	V	VerbForm=Inf	1	conj	_	SpaceAfter=No
14	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 12
# text = Mezzanotte in punto scappo e perse la scarpa.
1	Mezzanotte	mezzanurre	NOUN	S	Gender=Fem|Number=Sing	0	root	_	_
2	in	in	ADP	E	_	3	case	_	_
3	punto	punto	NOUN	S	Gender=Masc|Number=Sing	1	nmod	_	_
4	scappo	scappo	ADJ	A	Gender=Masc|Number=Sing	3	amod	_	_
5	e	e	CONJ	CC	_	1	cc	_	_
6	perse	perdere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	1	conj	_	_
7	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	8	det	_	_
8	scarpa	scarpa	NOUN	S	Gender=Fem|Number=Sing	6	dobj	_	SpaceAfter=No
9	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 13
# text = Il giorno seguente il principe ando in tutte le case a vedere a chi stava quella scarpetta.
1	Il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	0	root	_	_
3	seguente	seguente	ADJ	A	Number=Sing	2	amod	_	_
4	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	5	det	_	_
5	principe	principe	NOUN	S	Gender=Masc|Number=Sing	2	nmod	_	_
6	ando	are	VERB	V	VerbForm=Ger	5	acl	_	_
7	in	in	ADP	E	_	10	case	_	_
8	tutte	tutto	DET	T	Gender=Fem|Number=Plur	10	det:predet	_	_
9	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	10	det	_	_
10	case	casa	NOUN	S	Gender=Fem|Number=Plur	6	nmod	_	_
11	a	a	ADP	E	_	12	mark	_	_
12	vedere	vedere	VERB	V	VerbForm=Inf	6	xcomp	_	_
13	a	a	ADP	E	_	14	case	_	_
14	chi	chi	PRON	PR	Number=Sing|PronType=Rel	12	nmod	_	_
15	stava	stare	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	17	aux	_	_
16	quella	quello	PRON	PD	Gender=Fem|Number=Sing|PronType=Dem	17	nsubj	_	_
17	scarpetta	scarpetto	VERB	V	Mood=Sub|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	14	acl:relcl	_	SpaceAfter=No
18	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 14
# text = Arrivo in casa di Cenerentola e provo lè scarpe alle due sorellastre, ma era troppo piccola per entrare perfettamente al loro piedone.
1	Arrivo	arrivere	NOUN	S	Gender=Masc|Number=Sing	8	nsubj	_	_
2	in	in	ADP	E	_	3	case	_	_
3	casa	casa	NOUN	S	Gender=Fem|Number=Sing	1	nmod	_	_
4	di	di	ADP	E	_	5	case	_	_
5	Cenerentola	cenerentola	PROPN	SP	_	1	nmod	_	_
6	e	e	CONJ	CC	_	5	cc	_	_
7	provo	provare	NOUN	S	Gender=Masc|Number=Sing	5	conj	_	_
8	lè	lé	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
9	scarpe	scarpa	NOUN	S	Gender=Fem	8	dobj	_	_
10-11	alle	_	_	_	_	_	_	_	_
10	a	a	ADP	E	_	13	case	_	_
11	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	13	det	_	_
12	due	due	NUM	N	NumType=Card	13	nummod	_	_
13	sorellastre	sorellastra	NOUN	S	Gender=Fem|Number=Plur	9	nmod	_	SpaceAfter=No
14	,	,	PUNCT	FF	_	8	punct	_	_
15	ma	ma	CONJ	CC	_	8	cc	_	_
16	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	18	cop	_	_
17	troppo	troppo	ADV	B	_	18	advmod	_	_
18	piccola	piccolo	ADJ	A	Gender=Fem|Number=Sing	8	conj	_	_
19	per	per	ADP	E	_	20	mark	_	_
20	entrare	entrare	VERB	V	VerbForm=Inf	18	advcl	_	_
21	perfettamente	perfettamente	ADV	B	_	20	advmod	_	_
22-23	al	_	_	_	_	_	_	_	_
22	a	a	ADP	E	_	25	case	_	_
23	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	25	det	_	_
24	loro	loro	DET	AP	Poss=Yes|PronType=Prs	25	det:poss	_	_
25	piedone	piedone	NOUN	S	Gender=Masc|Number=Sing	20	nmod	_	SpaceAfter=No
26	.	.	PUNCT	FS	_	8	punct	_	_

# sent_id = 15
# text = Provarono a Cenerentola e entro perfettamente nel suo piede.
1	Provarono	Provare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
2	a	a	ADP	E	_	3	case	_	_
3	Cenerentola	cenerentola	PROPN	SP	_	1	nmod	_	_
4	e	e	CONJ	CC	_	3	cc	_	_
5	entro	entro	ADP	E	_	6	case	_	_
6	perfettamente	perfettamente	ADV	B	_	3	conj	_	_
7-8	nel	_	_	_	_	_	_	_	_
7	in	in	ADP	E	_	10	case	_	_
8	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	10	det	_	_
9	suo	suo	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	10	det:poss	_	_
10	piede	piede	NOUN	S	Gender=Masc|Number=Sing	6	nmod	_	SpaceAfter=No
11	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 16
# text = Poi il principe e Cenerentola si sposarono e vissero felice e contenti.
1	Poi	poi	ADV	B	_	3	advmod	_	_
2	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	3	det	_	_
3	principe	principe	NOUN	S	Gender=Masc|Number=Sing	7	nsubj	_	_
4	e	e	CONJ	CC	_	3	cc	_	_
5	Cenerentola	cenerentola	PROPN	SP	_	3	conj	_	_
6	si	si	PRON	PC	Person=3|PronType=Clit	7	expl	_	_
7	sposarono	sposare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
8	e	e	CONJ	CC	_	7	cc	_	_
9	vissero	vivere	VERB	V	Mood=Sub|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	7	conj	_	_
10	felice	felice	ADJ	A	Number=Sing	9	amod	_	_
11	e	e	CONJ	CC	_	10	cc	_	_
12	contenti	contento	ADJ	A	Gender=Masc|Number=Plur	10	conj	_	SpaceAfter=No
13	.	.	PUNCT	FS	_	7	punct	_	SpacesAfter=\s\s

# sent_id = 17
# text = Ho imparato: es:
1	Ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	2	aux	_	_
2	imparato	imparare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	SpaceAfter=No
3	:	:	PUNCT	FC	_	2	punct	_	_
4	es	es	NOUN	S	_	2	dobj	_	SpaceAfter=No
5	:	:	PUNCT	FC	_	2	punct	_	_

# sent_id = 18
# text = Io voglio diventare una dottoressa e devo sempre credere in me e non smettere di sognare.
1	Io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	3	nsubj	_	_
2	voglio	volere	AUX	VM	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	3	aux	_	_
3	diventare	diventare	VERB	V	VerbForm=Inf	0	root	_	_
4	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	5	det	_	_
5	dottoressa	dottoressa	NOUN	S	Gender=Fem|Number=Sing	3	dobj	_	_
6	e	e	CONJ	CC	_	3	cc	_	_
7	devo	dovere	AUX	VM	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	9	aux	_	_
8	sempre	sempre	ADV	B	_	9	advmod	_	_
9	credere	credere	VERB	V	VerbForm=Inf	3	conj	_	_
10	in	in	ADP	E	_	11	case	_	_
11	me	me	PRON	PE	Number=Sing|Person=1|PronType=Prs	9	nmod	_	_
12	e	e	CONJ	CC	_	3	cc	_	_
13	non	non	ADV	BN	PronType=Neg	14	neg	_	_
14	smettere	smettere	VERB	V	VerbForm=Inf	3	conj	_	_
15	di	di	ADP	E	_	16	mark	_	_
16	sognare	sognare	VERB	V	VerbForm=Inf	14	xcomp	_	SpaceAfter=No
17	.	.	PUNCT	FS	_	3	punct	_	SpacesAfter=\r\n\r\n


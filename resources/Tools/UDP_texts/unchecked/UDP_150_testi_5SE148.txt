# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE148.txt
# newpar
# sent_id = 1
# text = L’allenamento di calcio Un giorno dovevo andare all’allenamento di calcio.
1	L’allenamento	L’allenamento	NOUN	S	Gender=Masc|Number=Sing	7	nsubj	_	_
2	di	di	ADP	E	_	3	case	_	_
3	calcio	calcio	NOUN	S	Gender=Masc|Number=Sing	1	nmod	_	SpacesAfter=\s\s
4	Un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	5	det	_	_
5	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	7	nmod	_	_
6	dovevo	dovere	AUX	VM	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	7	aux	_	_
7	andare	andare	VERB	V	VerbForm=Inf	0	root	_	_
8	all’allenamento	all’allenamento	NOUN	S	Gender=Masc|Number=Sing	7	dobj	_	_
9	di	di	ADP	E	_	10	case	_	_
10	calcio	calcio	NOUN	S	Gender=Masc|Number=Sing	8	nmod	_	SpaceAfter=No
11	.	.	PUNCT	FS	_	7	punct	_	_

# sent_id = 2
# text = Avevo messo tutto nella borsa del calcio tranne la k-wai.
1	Avevo	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	2	aux	_	_
2	messo	mettere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
3	tutto	tutto	PRON	PI	Gender=Masc|Number=Sing|PronType=Ind	2	dobj	_	_
4-5	nella	_	_	_	_	_	_	_	_
4	in	in	ADP	E	_	6	case	_	_
5	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	6	det	_	_
6	borsa	borsa	NOUN	S	Gender=Fem|Number=Sing	2	nmod	_	_
7-8	del	_	_	_	_	_	_	_	_
7	di	di	ADP	E	_	9	case	_	_
8	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	9	det	_	_
9	calcio	calcio	NOUN	S	Gender=Masc|Number=Sing	6	nmod	_	_
10	tranne	tranne	ADP	E	_	12	case	_	_
11	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	12	det	_	_
12	k-wai	k-waio	NOUN	S	Gender=Masc|Number=Plur	2	nmod	_	SpaceAfter=No
13	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 3
# text = La mamma mi aveva detto di metterla nella borsa ma io non l’ho ascoltata e quindi sono andato all’allenamento senza la k-wai.
1	La	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	2	det	_	_
2	mamma	mamma	NOUN	S	Gender=Fem|Number=Sing	5	nsubj	_	_
3	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	5	iobj	_	_
4	aveva	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	5	aux	_	_
5	detto	dire	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
6	di	di	ADP	E	_	7	case	_	_
7	metterla	metterla	NOUN	S	Gender=Fem|Number=Sing	5	nmod	_	_
8-9	nella	_	_	_	_	_	_	_	_
8	in	in	ADP	E	_	10	case	_	_
9	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	10	det	_	_
10	borsa	borsa	NOUN	S	Gender=Fem|Number=Sing	5	nmod	_	_
11	ma	ma	CONJ	CC	_	5	cc	_	_
12	io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	15	nsubj	_	_
13	non	non	ADV	BN	PronType=Neg	15	neg	_	_
14	l’ho	l’avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	15	aux	_	_
15	ascoltata	ascoltare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	5	conj	_	_
16	e	e	CONJ	CC	_	5	cc	_	_
17	quindi	quindi	ADV	B	_	19	advmod	_	_
18	sono	essere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	19	aux	_	_
19	andato	andare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	5	conj	_	_
20	all’allenamento	all’allenamento	NOUN	S	Gender=Masc|Number=Sing	19	xcomp	_	_
21	senza	senza	ADP	E	_	23	case	_	_
22	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	23	det	_	_
23	k-wai	k-waio	NOUN	S	Gender=Masc|Number=Plur	20	nmod	_	SpaceAfter=No
24	.	.	PUNCT	FS	_	5	punct	_	_

# sent_id = 4
# text = C’erano delle nuvole nere in cielo.
1	C’erano	C’erare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
2-3	delle	_	_	_	_	_	_	_	_
2	di	di	ADP	E	_	4	case	_	_
3	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	4	det	_	_
4	nuvole	nuvola	NOUN	S	Gender=Fem|Number=Plur	1	nmod	_	_
5	nere	nero	ADJ	A	Gender=Fem|Number=Plur	4	amod	_	_
6	in	in	ADP	E	_	7	case	_	_
7	cielo	cielo	NOUN	S	Gender=Masc|Number=Sing	4	nmod	_	SpaceAfter=No
8	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 5
# text = Speravo che non venisse a piovere.
1	Speravo	sperare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	0	root	_	_
2	che	che	SCONJ	CS	_	4	mark	_	_
3	non	non	ADV	BN	PronType=Neg	4	neg	_	_
4	venisse	venire	VERB	V	Mood=Sub|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	1	ccomp	_	_
5	a	a	ADP	E	_	6	mark	_	_
6	piovere	piovere	VERB	V	VerbForm=Inf	4	xcomp	_	SpaceAfter=No
7	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 6
# text = Quando abbiamo iniziato l’allenamento, non pioveva.
1	Quando	quando	SCONJ	CS	_	3	advmod	_	_
2	abbiamo	avere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	3	aux	_	_
3	iniziato	iniziare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
4	l’allenamento	l’allenamento	NOUN	S	Gender=Masc|Number=Sing	3	xcomp	_	SpaceAfter=No
5	,	,	PUNCT	FF	_	3	punct	_	_
6	non	non	ADV	BN	PronType=Neg	7	neg	_	_
7	pioveva	piovere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	3	conj	_	SpaceAfter=No
8	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 7
# text = Per fortuna.
1	Per	per	ADP	E	_	2	case	_	_
2	fortuna	fortuna	NOUN	S	Gender=Fem|Number=Sing	0	root	_	SpaceAfter=No
3	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 8
# text = É andato bene per venticinque minuti.
1	É	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
2	andato	andare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	1	ccomp	_	_
3	bene	bene	ADV	B	_	2	advmod	_	_
4	per	per	ADP	E	_	6	case	_	_
5	venticinque	venticinque	NUM	N	NumType=Card	6	nummod	_	_
6	minuti	minuto	NOUN	S	Gender=Masc|Number=Plur	2	nmod	_	SpaceAfter=No
7	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 9
# text = Poi ha cominciato a piovere.
1	Poi	poi	ADV	B	_	3	advmod	_	_
2	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	3	aux	_	_
3	cominciato	cominciare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
4	a	a	ADP	E	_	5	mark	_	_
5	piovere	piovere	VERB	V	VerbForm=Inf	3	xcomp	_	SpaceAfter=No
6	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 10
# text = Avevo un po’ freddo e ero bagnato fradicio.
1	Avevo	avere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	0	root	_	_
2	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	4	det	_	_
3	po’	ipo	DET	RD	Definite=Def|Number=Sing|PronType=Art	4	det	_	_
4	freddo	freddo	NOUN	S	Gender=Masc|Number=Sing	1	dobj	_	_
5	e	e	CONJ	CC	_	1	cc	_	_
6	ero	essere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	7	aux	_	_
7	bagnato	bagnare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	1	conj	_	_
8	fradicio	fradicio	NOUN	S	Gender=Masc|Number=Sing	7	dobj	_	SpaceAfter=No
9	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 11
# text = Cosí mi sono preso il raffreddore.
1	Cosí	Cosí	PROPN	SP	_	4	nsubj	_	_
2	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	4	iobj	_	_
3	sono	essere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	4	aux	_	_
4	preso	prendere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
5	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	6	det	_	_
6	raffreddore	raffreddore	NOUN	S	Gender=Masc|Number=Sing	4	dobj	_	SpaceAfter=No
7	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 12
# text = Da questo evento ho imparato di ascoltare sempre i genitori.
1	Da	da	ADP	E	_	3	case	_	_
2	questo	questo	DET	DD	Gender=Masc|Number=Sing|PronType=Dem	3	det	_	_
3	evento	evento	NOUN	S	Gender=Masc|Number=Sing	5	nmod	_	_
4	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	imparato	imparare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
6	di	di	ADP	E	_	7	mark	_	_
7	ascoltare	ascoltare	VERB	V	VerbForm=Inf	5	xcomp	_	_
8	sempre	sempre	ADV	B	_	7	advmod	_	_
9	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	10	det	_	_
10	genitori	genitore	NOUN	S	Gender=Masc|Number=Plur	7	dobj	_	SpaceAfter=No
11	.	.	PUNCT	FS	_	5	punct	_	SpacesAfter=\r\n\r\n


# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE73.txt
# newpar
# sent_id = 1
# text = Una giornata a Parigi Un giorno d’estate decidemmo di andare a Parigi telefonammo ad una agenzia viaggi e rispose una signorina e mio padre le chiese se si poteva andare a Parigi una settimana in un hotel a cinque stelle.
1	Una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	2	det	_	_
2	giornata	giornata	NOUN	S	Gender=Fem|Number=Sing	8	nsubj	_	_
3	a	a	ADP	E	_	4	case	_	_
4	Parigi	Parigi	PROPN	SP	_	2	nmod	_	SpacesAfter=\s\s
5	Un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	6	det	_	_
6	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	8	nmod	_	_
7	d’estate	d’estare	AUX	VA	Gender=Fem|Number=Plur|Tense=Past|VerbForm=Part	8	aux	_	_
8	decidemmo	decidemmo	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Past|VerbForm=Fin	0	root	_	_
9	di	di	ADP	E	_	10	mark	_	_
10	andare	andare	VERB	V	VerbForm=Inf	8	xcomp	_	_
11	a	a	ADP	E	_	12	case	_	_
12	Parigi	Parigi	PROPN	SP	_	10	nmod	_	_
13	telefonammo	telefonare	NOUN	S	Gender=Masc|Number=Sing	10	dobj	_	_
14	ad	a	ADP	E	_	16	case	_	_
15	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	16	det	_	_
16	agenzia	agenzia	NOUN	S	Gender=Fem|Number=Sing	10	nmod	_	_
17	viaggi	viaggio	NOUN	S	Gender=Masc|Number=Plur	16	compound	_	_
18	e	e	CONJ	CC	_	8	cc	_	_
19	rispose	rispondere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	8	conj	_	_
20	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	21	det	_	_
21	signorina	signorina	NOUN	S	Gender=Fem|Number=Sing	19	dobj	_	_
22	e	e	CONJ	CC	_	21	cc	_	_
23	mio	mio	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	24	det:poss	_	_
24	padre	padre	NOUN	S	Gender=Masc|Number=Sing	21	conj	_	_
25	le	il	PRON	PC	Gender=Fem|Number=Sing|Person=3|PronType=Clit	26	dobj	_	_
26	chiese	chiese	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	19	advcl	_	_
27	se	se	SCONJ	CS	_	30	mark	_	_
28	si	si	PRON	PC	Person=3|PronType=Clit	30	expl	_	_
29	poteva	potere	AUX	VM	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	30	aux	_	_
30	andare	andare	VERB	V	VerbForm=Inf	26	advcl	_	_
31	a	a	ADP	E	_	32	case	_	_
32	Parigi	Parigi	PROPN	SP	_	30	nmod	_	_
33	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	34	det	_	_
34	settimana	settimana	NOUN	S	Gender=Fem|Number=Sing	30	dobj	_	_
35	in	in	ADP	E	_	37	case	_	_
36	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	37	det	_	_
37	hotel	hotel	NOUN	S	Gender=Masc	34	nmod	_	_
38	a	a	ADP	E	_	40	case	_	_
39	cinque	cinque	NUM	N	NumType=Card	40	nummod	_	_
40	stelle	stella	NOUN	S	Gender=Fem|Number=Plur	30	nmod	_	SpaceAfter=No
41	.	.	PUNCT	FS	_	8	punct	_	_

# sent_id = 2
# text = Lei rispose che un alberxxxo a cinque stelle c’era ma era lontano alla Tour Eiffel.
1	Lei	lei	PRON	PE	Number=Sing|Person=3|PronType=Prs	2	nsubj	_	_
2	rispose	rispondere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
3	che	che	SCONJ	CS	_	9	mark	_	_
4	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	5	det	_	_
5	alberxxxo	alberxxxo	NOUN	S	Gender=Masc|Number=Sing	9	nsubj	_	_
6	a	a	ADP	E	_	8	case	_	_
7	cinque	cinque	NUM	N	NumType=Card	8	nummod	_	_
8	stelle	stei	NOUN	S	Gender=Fem|Number=Plur	5	nmod	_	_
9	c’era	c’era	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	2	ccomp	_	_
10	ma	ma	CONJ	CC	_	2	cc	_	_
11	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	2	conj	_	_
12	lontano	lontano	ADV	B	_	15	case	_	_
13-14	alla	_	_	_	_	_	_	_	_
13	a	a	ADP	E	_	12	mwe	_	_
14	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	15	det	_	_
15	Tour	Tour	PROPN	SP	_	11	nmod	_	_
16	Eiffel	Eiffel	PROPN	SP	_	15	name	_	SpaceAfter=No
17	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 3
# text = Per fortuna peró c’era un quattro stelle in faccia alla Tour Eiffel e noi lo presimo (cioè pesimo una stanza).
1	Per	per	ADP	E	_	2	case	_	_
2	fortuna	fortuno	NOUN	S	Gender=Fem|Number=Sing	4	nmod	_	_
3	peró	peró	PROPN	SP	_	4	nsubj	_	_
4	c’era	c’erare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
5	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	7	det	_	_
6	quattro	quattro	NUM	N	NumType=Card	7	nummod	_	_
7	stelle	stella	NOUN	S	Gender=Fem|Number=Plur	4	dobj	_	_
8	in	in	ADP	E	_	9	case	_	_
9	faccia	faccia	NOUN	S	Gender=Fem|Number=Sing	4	nmod	_	_
10-11	alla	_	_	_	_	_	_	_	_
10	a	a	ADP	E	_	12	case	_	_
11	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	12	det	_	_
12	Tour	Tour	PROPN	SP	_	9	nmod	_	_
13	Eiffel	Eiffel	PROPN	SP	_	12	name	_	_
14	e	e	CONJ	CC	_	4	cc	_	_
15	noi	noi	PRON	PE	Number=Plur|Person=1|PronType=Prs	17	nsubj	_	_
16	lo	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	17	det	_	_
17	presimo	presimo	NOUN	S	Gender=Masc|Number=Sing	4	conj	_	_
18	(	(	PUNCT	FB	_	20	punct	_	SpaceAfter=No
19	cioè	cioè	ADV	B	_	20	advmod	_	_
20	pesimo	pesimo	ADJ	A	Gender=Masc|Number=Sing	17	amod	_	_
21	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	22	det	_	_
22	stanza	stanza	NOUN	S	Gender=Fem|Number=Sing	20	nsubj	_	SpaceAfter=No
23	)	)	PUNCT	FB	_	20	punct	_	SpaceAfter=No
24	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 4
# text = Quando arrivammo a Parigi andammo subito in albergo a mettere le valige e andammo alla Tour Eiffel era alta e luminosa.
1	Quando	quando	SCONJ	CS	_	2	mark	_	_
2	arrivammo	arrivare	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Past|VerbForm=Fin	5	advcl	_	_
3	a	a	ADP	E	_	4	case	_	_
4	Parigi	Parigi	PROPN	SP	_	2	nmod	_	_
5	andammo	andare	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Past|VerbForm=Fin	0	root	_	_
6	subito	subito	ADV	B	_	5	advmod	_	_
7	in	in	ADP	E	_	8	case	_	_
8	albergo	albergo	NOUN	S	Gender=Masc|Number=Sing	5	nmod	_	_
9	a	a	ADP	E	_	10	mark	_	_
10	mettere	mettere	VERB	V	VerbForm=Inf	5	xcomp	_	_
11	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	12	det	_	_
12	valige	valigia	NOUN	S	Gender=Fem|Number=Plur	10	dobj	_	_
13	e	e	CONJ	CC	_	10	cc	_	_
14	andammo	andare	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Past|VerbForm=Fin	10	conj	_	_
15-16	alla	_	_	_	_	_	_	_	_
15	a	a	ADP	E	_	17	case	_	_
16	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	17	det	_	_
17	Tour	Tour	PROPN	SP	_	14	nmod	_	_
18	Eiffel	Eiffel	PROPN	SP	_	17	name	_	_
19	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	20	cop	_	_
20	alta	alto	ADJ	A	Gender=Fem|Number=Sing	14	xcomp	_	_
21	e	e	CONJ	CC	_	20	cc	_	_
22	luminosa	luminoso	ADJ	A	Gender=Fem|Number=Sing	20	conj	_	SpaceAfter=No
23	.	.	PUNCT	FS	_	5	punct	_	_

# sent_id = 5
# text = Io peró ero arrabbiata perche non siamo saliti sulla Tour Eiffel.
1	Io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	4	nsubj	_	_
2	peró	peró	PROPN	SP	_	1	name	_	_
3	ero	essere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	4	aux	_	_
4	arrabbiata	arrabbiare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
5	perche	perca	NOUN	S	Gender=Fem|Number=Plur	8	nsubjpass	_	_
6	non	non	ADV	BN	PronType=Neg	8	neg	_	_
7	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	8	auxpass	_	_
8	saliti	salire	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	4	xcomp	_	_
9-10	sulla	_	_	_	_	_	_	_	_
9	su	su	ADP	E	_	11	case	_	_
10	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	11	det	_	_
11	Tour	Tour	PROPN	SP	_	8	nmod	_	_
12	Eiffel.	Eiffelii	PROPN	SP	_	8	xcomp	_	_

# sent_id = 6
# text = E per tutta la vacanza non ero contenta.
1	E	e	CONJ	CC	_	7	cc	_	_
2	per	per	ADP	E	_	5	case	_	_
3	tutta	tutto	DET	T	Gender=Fem|Number=Sing	5	det:predet	_	_
4	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	5	det	_	_
5	vacanza	vacanza	NOUN	S	Gender=Fem|Number=Sing	7	nmod	_	_
6	non	non	ADV	BN	PronType=Neg	7	neg	_	_
7	ero	essere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	0	root	_	_
8	contenta	contentare	ADJ	A	Gender=Fem|Number=Sing	7	xcomp	_	SpaceAfter=No
9	.	.	PUNCT	FS	_	7	punct	_	SpacesAfter=\r\n\r\n


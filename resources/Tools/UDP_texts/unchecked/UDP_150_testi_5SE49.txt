# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE49.txt
# newpar
# sent_id = 1
# text = Una squadra di calcio C’é una squadra di calcio che si vanta con gli amici perché sono bravi, un’altra squadra piú debole, vuole sfidarli.
1	Una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	2	det	_	_
2	squadra	squadra	NOUN	S	Gender=Fem|Number=Sing	25	nsubj	_	_
3	di	di	ADP	E	_	4	case	_	_
4	calcio	calcio	NOUN	S	Gender=Masc|Number=Sing	2	nmod	_	SpacesAfter=\s\s
5	C’é	C’é	PROPN	SP	_	2	nmod	_	_
6	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	7	det	_	_
7	squadra	squadra	NOUN	S	Gender=Fem|Number=Sing	2	nsubj	_	_
8	di	di	ADP	E	_	9	case	_	_
9	calcio	calcio	NOUN	S	Gender=Masc|Number=Sing	7	nmod	_	_
10	che	che	PRON	PR	PronType=Rel	12	nsubj	_	_
11	si	si	PRON	PC	Person=3|PronType=Clit	12	expl	_	_
12	vanta	vantare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	7	acl:relcl	_	_
13	con	con	ADP	E	_	15	case	_	_
14	gli	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	15	det	_	_
15	amici	amico	NOUN	S	Gender=Masc|Number=Plur	12	nmod	_	_
16	perché	perché	SCONJ	CS	_	18	mark	_	_
17	sono	essere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	18	cop	_	_
18	bravi	bravo	ADJ	A	Gender=Masc|Number=Plur	2	advcl	_	SpaceAfter=No
19	,	,	PUNCT	FF	_	18	punct	_	_
20	un’altra	un’altro	DET	DI	Gender=Fem|Number=Sing|PronType=Ind	21	det	_	_
21	squadra	squadra	NOUN	S	Gender=Fem|Number=Sing	18	appos	_	_
22	piú	piú	PROPN	SP	_	21	nmod	_	_
23	debole	debole	ADJ	A	Number=Sing	22	amod	_	SpaceAfter=No
24	,	,	PUNCT	FF	_	2	punct	_	_
25	vuole	volere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
26	sfidarli	sfidarlare	NOUN	S	Gender=Masc|Number=Plur	25	dobj	_	SpaceAfter=No
27	.	.	PUNCT	FS	_	25	punct	_	_

# sent_id = 2
# text = Allora inizia la partita, la squadra piú forte si porta in vantaggio con 1 a 0. Alla fine del primo tempo la squadra piú forte fanno 5 gol, iniziato il secondo tempo i più deboli vanno verso la porta e fanno gol, dopo ancora 15 minuti del secondo tempo si aggiudicano 4 gol per i piú deboli.
1	Allora	allora	ADV	B	_	2	advmod	_	_
2	inizia	iniziare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	11	advcl	_	_
3	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	4	det	_	_
4	partita	partita	NOUN	S	Gender=Fem|Number=Sing	2	nsubj	_	SpaceAfter=No
5	,	,	PUNCT	FF	_	2	punct	_	_
6	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	7	det	_	_
7	squadra	squadra	NOUN	S	Gender=Fem|Number=Sing	11	nsubj	_	_
8	piú	piú	PROPN	SP	_	7	nmod	_	_
9	forte	forte	ADJ	A	Number=Sing	7	amod	_	_
10	si	si	PRON	PC	Person=3|PronType=Clit	11	expl	_	_
11	porta	portare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
12	in	in	ADP	E	_	13	case	_	_
13	vantaggio	vantaggio	NOUN	S	Gender=Masc|Number=Sing	11	nmod	_	_
14	con	con	ADP	E	_	15	case	_	_
15	1	1	NUM	N	NumType=Card	11	nummod	_	_
16	a	a	ADP	E	_	17	case	_	_
17	0	0	NUM	N	NumType=Card	15	nummod	_	SpaceAfter=No
18	.	.	PUNCT	FS	_	30	punct	_	_
19-20	Alla	_	_	_	_	_	_	_	_
19	A	a	ADP	E	_	21	case	_	_
20	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	21	det	_	_
21	fine	fine	NOUN	S	Gender=Fem|Number=Sing	30	nmod	_	_
22-23	del	_	_	_	_	_	_	_	_
22	di	di	ADP	E	_	25	case	_	_
23	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	25	det	_	_
24	primo	primo	ADJ	NO	Gender=Masc|Number=Sing|NumType=Ord	25	amod	_	_
25	tempo	tempo	NOUN	S	Gender=Masc|Number=Sing	21	nmod	_	_
26	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	27	det	_	_
27	squadra	squadra	NOUN	S	Gender=Fem|Number=Sing	30	nsubj	_	_
28	piú	piú	PROPN	SP	_	27	nmod	_	_
29	forte	forte	ADV	B	_	30	advmod	_	_
30	fanno	fare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	11	ccomp	_	_
31	5	5	NUM	N	NumType=Card	32	nummod	_	_
32	gol	gol	NOUN	S	Gender=Masc	30	dobj	_	SpaceAfter=No
33	,	,	PUNCT	FF	_	30	punct	_	_
34	iniziato	iniziare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	30	conj	_	_
35	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	37	det	_	_
36	secondo	secondo	ADJ	NO	Gender=Masc|Number=Sing|NumType=Ord	37	amod	_	_
37	tempo	tempo	NOUN	S	Gender=Masc|Number=Sing	34	dobj	_	_
38	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	40	det	_	_
39	più	più	ADV	B	_	40	advmod	_	_
40	deboli	debole	ADJ	A	Number=Plur	41	nsubj	_	_
41	vanno	andare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	34	advcl	_	_
42	verso	verso	ADP	E	_	44	case	_	_
43	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	44	det	_	_
44	porta	porta	NOUN	S	Gender=Fem|Number=Sing	41	nmod	_	_
45	e	e	CONJ	CC	_	41	cc	_	_
46	fanno	fare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	41	conj	_	_
47	gol	gol	NOUN	S	Gender=Masc	46	dobj	_	SpaceAfter=No
48	,	,	PUNCT	FF	_	46	punct	_	_
49	dopo	dopo	ADP	E	_	52	case	_	_
50	ancora	ancora	ADV	B	_	52	advmod	_	_
51	15	15	NUM	N	NumType=Card	52	nummod	_	_
52	minuti	minuto	NOUN	S	Gender=Masc|Number=Plur	46	nmod	_	_
53-54	del	_	_	_	_	_	_	_	_
53	di	di	ADP	E	_	56	case	_	_
54	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	56	det	_	_
55	secondo	secondo	ADJ	NO	Gender=Masc|Number=Sing|NumType=Ord	56	amod	_	_
56	tempo	tempo	NOUN	S	Gender=Masc|Number=Sing	52	nmod	_	_
57	si	si	PRON	PC	Person=3|PronType=Clit	58	expl	_	_
58	aggiudicano	aggiudicare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	46	conj	_	_
59	4	4	NUM	N	NumType=Card	60	nummod	_	_
60	gol	gol	NOUN	S	Gender=Masc	58	nmod	_	_
61	per	per	ADP	E	_	63	case	_	_
62	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	63	det	_	_
63	piú	piú	PROPN	SP	_	58	nmod	_	_
64	deboli	debole	ADJ	A	Number=Plur	63	amod	_	SpaceAfter=No
65	.	.	PUNCT	FS	_	11	punct	_	SpacesAfter=\s\s\s

# sent_id = 3
# text = Finita la partita i piú deboli fanno ancora 2 gol e si aggiudicano vincitori, quando l’arbitro fischia la fine della partita si stringono la mano per la bella partita, i piú forti si vantavano ma alla fine hanno perso.
1	Finita	finire	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	7	advcl	_	_
2	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	3	det	_	_
3	partita	partita	NOUN	S	Gender=Fem|Number=Sing	7	nsubj	_	_
4	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	5	det	_	_
5	piú	piú	PROPN	SP	_	3	nmod	_	_
6	deboli	debole	ADJ	A	Number=Plur	5	amod	_	_
7	fanno	fare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
8	ancora	ancora	ADV	B	_	7	advmod	_	_
9	2	2	NUM	N	NumType=Card	10	nummod	_	_
10	gol	gol	NOUN	S	Gender=Masc	7	dobj	_	_
11	e	e	CONJ	CC	_	7	cc	_	_
12	si	si	PRON	PC	Person=3|PronType=Clit	13	expl	_	_
13	aggiudicano	aggiudicare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	7	conj	_	_
14	vincitori	vincitore	NOUN	S	Gender=Masc|Number=Plur	13	nsubj	_	SpaceAfter=No
15	,	,	PUNCT	FF	_	13	punct	_	_
16	quando	quando	SCONJ	CS	_	18	mark	_	_
17	l’arbitro	l’arbitro	PRON	PP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	18	nsubj	_	_
18	fischia	fischiare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	25	advcl	_	_
19	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	20	det	_	_
20	fine	fine	NOUN	S	Gender=Fem|Number=Sing	18	dobj	_	_
21-22	della	_	_	_	_	_	_	_	_
21	di	di	ADP	E	_	23	case	_	_
22	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	23	det	_	_
23	partita	partita	NOUN	S	Gender=Fem|Number=Sing	20	nmod	_	_
24	si	si	PRON	PC	Person=3|PronType=Clit	25	expl	_	_
25	stringono	stringere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	13	conj	_	_
26	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	27	det	_	_
27	mano	mano	NOUN	S	Gender=Fem|Number=Sing	25	nsubj	_	_
28	per	per	ADP	E	_	31	case	_	_
29	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	31	det	_	_
30	bella	bello	ADJ	A	Gender=Fem|Number=Sing	31	amod	_	_
31	partita	partita	NOUN	S	Gender=Fem|Number=Sing	27	nmod	_	SpaceAfter=No
32	,	,	PUNCT	FF	_	13	punct	_	_
33	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	34	det	_	_
34	piú	piú	PROPN	SP	_	37	nsubj	_	_
35	forti	forte	ADJ	A	Number=Plur	34	amod	_	_
36	si	si	PRON	PC	Person=3|PronType=Clit	37	expl	_	_
37	vantavano	vantare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	13	conj	_	_
38	ma	ma	CONJ	CC	_	13	cc	_	_
39-40	alla	_	_	_	_	_	_	_	_
39	a	a	ADP	E	_	41	case	_	_
40	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	41	det	_	_
41	fine	fine	NOUN	S	Gender=Fem|Number=Sing	43	nmod	_	_
42	hanno	avere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	43	aux	_	_
43	perso	perdere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	13	conj	_	SpaceAfter=No
44	.	.	PUNCT	FS	_	7	punct	_	SpacesAfter=\s\s

# sent_id = 4
# text = Riflessione Magari i piú forti potevano vinciere se non si vantavano cosí tanto.
1	Riflessione	riflessione	NOUN	S	Gender=Fem|Number=Sing	7	nsubj	_	SpacesAfter=\s\s
2	Magari	magari	PROPN	SP	_	1	nmod	_	_
3	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	4	det	_	_
4	piú	piú	PROPN	SP	_	1	nmod	_	_
5	forti	forte	ADJ	A	Number=Plur	4	amod	_	_
6	potevano	potere	AUX	VM	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	7	aux	_	_
7	vinciere	vinciere	VERB	V	VerbForm=Inf	0	root	_	_
8	se	se	SCONJ	CS	_	11	mark	_	_
9	non	non	ADV	BN	PronType=Neg	11	neg	_	_
10	si	si	PRON	PC	Person=3|PronType=Clit	11	expl	_	_
11	vantavano	vantare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	7	advcl	_	_
12	cosí	cosí	PROPN	SP	_	11	nsubj	_	_
13	tanto	tanto	ADV	B	_	12	advmod	_	SpaceAfter=No
14	.	.	PUNCT	FS	_	7	punct	_	_

# sent_id = 5
# text = E si impegnavano di piú.
1	E	e	CONJ	CC	_	3	cc	_	_
2	si	si	PRON	PC	Person=3|PronType=Clit	3	expl	_	_
3	impegnavano	impegnavare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
4	di	di	ADP	E	_	5	case	_	_
5	piú	piú	PROPN	SP	_	3	nmod	_	SpaceAfter=No
6	.	.	PUNCT	FS	_	3	punct	_	SpacesAfter=\r\n\r\n


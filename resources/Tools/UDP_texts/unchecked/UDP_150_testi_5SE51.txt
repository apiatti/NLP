# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE51.txt
# newpar
# sent_id = 1
# text = Il ladro Una notte qualunque quando stavo dormendo l’allarme di casa è suonata e io e mia mamma ci siamo svegliati a vedere cosa stava succedendo.
1	Il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	ladro	ladro	NOUN	S	Gender=Masc|Number=Sing	13	nsubjpass	_	SpacesAfter=\s\s
3	Una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	4	det	_	_
4	notte	notte	NOUN	S	Gender=Fem|Number=Sing	2	nmod	_	_
5	qualunque	qualunque	ADV	B	_	7	advmod	_	_
6	quando	quando	SCONJ	CS	_	7	mark	_	_
7	stavo	stare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	2	advcl	_	_
8	dormendo	dormire	VERB	V	VerbForm=Ger	7	advcl	_	_
9	l’allarme	l’allarme	NOUN	S	Gender=Masc|Number=Sing	8	dobj	_	_
10	di	di	ADP	E	_	11	case	_	_
11	casa	casa	NOUN	S	Gender=Fem|Number=Sing	9	nmod	_	_
12	è	essere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	13	auxpass	_	_
13	suonata	suonare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
14	e	e	CONJ	CC	_	13	cc	_	_
15	io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	21	nsubj	_	_
16	e	e	CONJ	CC	_	15	cc	_	_
17	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	18	det:poss	_	_
18	mamma	mamma	NOUN	S	Gender=Fem|Number=Sing	15	conj	_	_
19	ci	ci	PRON	PC	Number=Plur|Person=1|PronType=Clit	21	expl	_	_
20	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	21	aux	_	_
21	svegliati	svegliare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	13	conj	_	_
22	a	a	ADP	E	_	23	mark	_	_
23	vedere	vedere	VERB	V	VerbForm=Inf	21	xcomp	_	_
24	cosa	cosa	PRON	PQ	Number=Sing|PronType=Int	23	dobj	_	_
25	stava	stare	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	26	aux	_	_
26	succedendo	succedere	VERB	V	VerbForm=Ger	23	advcl	_	SpaceAfter=No
27	.	.	PUNCT	FS	_	13	punct	_	_

# sent_id = 2
# text = E abbiamo visto una persona (ladro) che stava rubando dei gioielli;
1	E	e	CONJ	CC	_	3	cc	_	_
2	abbiamo	avere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	3	aux	_	_
3	visto	vedere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
4	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	5	det	_	_
5	persona	persona	NOUN	S	Gender=Fem|Number=Sing	3	dobj	_	_
6	(	(	PUNCT	FB	_	7	punct	_	SpaceAfter=No
7	ladro	ladro	NOUN	S	Gender=Masc|Number=Sing	3	nmod	_	SpaceAfter=No
8	)	)	PUNCT	FB	_	7	punct	_	_
9	che	che	PRON	PR	PronType=Rel	11	nsubj	_	_
10	stava	stare	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	11	aux	_	_
11	rubando	rubare	VERB	V	VerbForm=Ger	3	acl:relcl	_	_
12-13	dei	_	_	_	_	_	_	_	_
12	di	di	ADP	E	_	14	case	_	_
13	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	14	det	_	_
14	gioielli	gioiello	NOUN	S	Gender=Masc|Number=Plur	11	nmod	_	SpaceAfter=No
15	;	;	PUNCT	FC	_	3	punct	_	_

# sent_id = 3
# text = “Il ladro è riuscito a entrare spaccando il vetro della porta d’entrata”;
1	“	“	PUNCT	FB	_	5	punct	_	SpaceAfter=No
2	Il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	3	det	_	_
3	ladro	ladro	NOUN	S	Gender=Masc|Number=Sing	5	nsubj	_	_
4	è	essere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	riuscito	riuscire	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
6	a	a	ADP	E	_	7	mark	_	SpacesAfter=\s\s
7	entrare	entrare	VERB	V	VerbForm=Inf	5	xcomp	_	_
8	spaccando	spaccando	VERB	V	VerbForm=Ger	7	advcl	_	_
9	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	10	det	_	_
10	vetro	vetro	NOUN	S	Gender=Masc|Number=Sing	8	dobj	_	_
11-12	della	_	_	_	_	_	_	_	_
11	di	di	ADP	E	_	13	case	_	_
12	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	13	det	_	_
13	porta	porta	NOUN	S	Gender=Fem|Number=Sing	10	nmod	_	_
14	d’entrata	d’entrato	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	13	acl	_	SpaceAfter=No
15	”	”	PUNCT	FB	_	5	punct	_	SpaceAfter=No
16	;	;	PUNCT	FC	_	5	punct	_	_

# sent_id = 4
# text = Dopo mia mamma cercò sull’elenco telefonico il numero della polizia, e quando lo trovò la chiamò.
1	Dopo	dopo	ADP	E	_	3	case	_	_
2	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	3	det:poss	_	_
3	mamma	mamma	NOUN	S	Gender=Fem|Number=Sing	4	nmod	_	_
4	cercò	cercare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
5	sull’elenco	sull’elenco	NOUN	S	Gender=Masc|Number=Sing	4	dobj	_	_
6	telefonico	telefonico	ADJ	A	Gender=Masc|Number=Sing	5	amod	_	_
7	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	8	det	_	_
8	numero	numero	NOUN	S	Gender=Masc|Number=Sing	5	nmod	_	_
9-10	della	_	_	_	_	_	_	_	SpacesAfter=\s\s\s
9	di	di	ADP	E	_	11	case	_	_
10	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	11	det	_	_
11	polizia	polizia	NOUN	S	Gender=Fem|Number=Sing	8	nmod	_	SpaceAfter=No
12	,	,	PUNCT	FF	_	5	punct	_	_
13	e	e	CONJ	CC	_	5	cc	_	_
14	quando	quando	SCONJ	CS	_	16	mark	_	_
15	lo	lo	PRON	PC	Gender=Masc|Number=Sing|Person=3|PronType=Clit	16	dobj	_	_
16	trovò	trovare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	18	advcl	_	_
17	la	il	PRON	PC	Gender=Fem|Number=Sing|Person=3|PronType=Clit	18	dobj	_	_
18	chiamò	chiamò	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	5	conj	_	SpaceAfter=No
19	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 5
# text = E quando era arrivata il ladro era gia scappato e dopo noi siamo andati a raccontarlo a mia nonna.
1	E	e	CONJ	CC	_	9	cc	_	_
2	quando	quando	SCONJ	CS	_	4	mark	_	_
3	era	essere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	4	aux	_	_
4	arrivata	arrivare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	9	advcl	_	_
5	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	6	det	_	_
6	ladro	ladro	NOUN	S	Gender=Masc|Number=Sing	4	nsubj	_	_
7	era	essere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	9	aux	_	_
8	gia	gia	ADV	B	_	9	advmod	_	_
9	scappato	scappato	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
10	e	e	CONJ	CC	_	9	cc	_	_
11	dopo	dopo	ADP	E	_	12	case	_	_
12	noi	noi	PRON	PE	Number=Plur|Person=1|PronType=Prs	14	nmod	_	_
13	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	14	aux	_	_
14	andati	andare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	9	conj	_	_
15	a	a	ADP	E	_	16	case	_	_
16	raccontarlo	raccontarlo	NOUN	S	Gender=Masc|Number=Sing	14	nmod	_	_
17	a	a	ADP	E	_	19	case	_	_
18	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	19	det:poss	_	_
19	nonna	nonno	NOUN	S	Gender=Fem|Number=Sing	14	nmod	_	SpaceAfter=No
20	.	.	PUNCT	FS	_	9	punct	_	_

# sent_id = 6
# text = “Ma noi eravamo al mare in camera e il ladro entrai nella nostra stanza.
1	“	“	PUNCT	FB	_	4	punct	_	SpaceAfter=No
2	Ma	ma	CONJ	CC	_	4	cc	_	_
3	noi	noi	PRON	PE	Number=Plur|Person=1|PronType=Prs	4	nsubj	_	_
4	eravamo	essere	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	0	root	_	_
5-6	al	_	_	_	_	_	_	_	_
5	a	a	ADP	E	_	7	case	_	_
6	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	7	det	_	_
7	mare	mare	NOUN	S	Gender=Masc|Number=Sing	4	nmod	_	_
8	in	in	ADP	E	_	9	case	_	_
9	camera	camera	NOUN	S	Gender=Fem|Number=Sing	4	nmod	_	_
10	e	e	CONJ	CC	_	4	cc	_	_
11	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	12	det	_	_
12	ladro	ladro	NOUN	S	Gender=Masc|Number=Sing	4	conj	_	_
13	entrai	entrare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Past|VerbForm=Fin	12	acl	_	_
14-15	nella	_	_	_	_	_	_	_	_
14	in	in	ADP	E	_	17	case	_	_
15	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	17	det	_	_
16	nostra	nostro	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	17	det:poss	_	_
17	stanza	stanza	NOUN	S	Gender=Fem|Number=Sing	13	nmod	_	SpaceAfter=No
18	.	.	PUNCT	FS	_	4	punct	_	SpacesAfter=\s\s

# sent_id = 7
# text = Commento:
1	Commento	commento	NOUN	S	Gender=Masc|Number=Sing	0	root	_	SpaceAfter=No
2	:	:	PUNCT	FC	_	1	punct	_	_

# sent_id = 8
# text = quella è stata una Esperienza paurosa che fino a quel giorno non ho mai vissuto.
1	quella	quello	PRON	PD	Gender=Fem|Number=Sing|PronType=Dem	5	nsubj	_	_
2	è	essere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	5	aux	_	_
3	stata	essere	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	5	cop	_	_
4	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	5	det	_	_
5	Esperienza	esperienza	NOUN	S	Gender=Fem|Number=Sing	0	root	_	_
6	paurosa	pauroso	ADJ	A	Gender=Fem|Number=Sing	5	amod	_	_
7	che	che	PRON	PR	PronType=Rel	15	nsubj	_	_
8	fino	fino	ADV	B	_	11	case	_	_
9	a	a	ADP	E	_	8	mwe	_	_
10	quel	quello	DET	DD	Gender=Masc|Number=Sing|PronType=Dem	11	det	_	_
11	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	15	nmod	_	_
12	non	non	ADV	BN	PronType=Neg	15	neg	_	_
13	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	15	aux	_	_
14	mai	mai	ADV	B	_	15	advmod	_	_
15	vissuto	vivere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	5	acl:relcl	_	SpaceAfter=No
16	.	.	PUNCT	FS	_	5	punct	_	SpacesAfter=\s\s

# sent_id = 9
# text = Riflessione:
1	Riflessione	riflessione	NOUN	S	Gender=Fem|Number=Sing	0	root	_	SpaceAfter=No
2	:	:	PUNCT	FC	_	1	punct	_	_

# sent_id = 10
# text = Questa storia mi a insegnato che non si deve rubare.
1	Questa	questo	DET	DD	Gender=Fem|Number=Sing|PronType=Dem	2	det	_	_
2	storia	storia	NOUN	S	Gender=Fem|Number=Sing	0	root	_	_
3	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	2	nmod	_	_
4	a	a	ADP	E	_	5	mark	_	_
5	insegnato	insegnare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	3	acl	_	_
6	che	che	SCONJ	CS	_	10	mark	_	_
7	non	non	ADV	BN	PronType=Neg	10	neg	_	_
8	si	si	PRON	PC	Person=3|PronType=Clit	10	expl:impers	_	_
9	deve	dovere	AUX	VM	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	10	aux	_	_
10	rubare	rubare	VERB	V	VerbForm=Inf	5	ccomp	_	SpaceAfter=No
11	.	.	PUNCT	FS	_	3	punct	_	SpacesAfter=\r\n\r\n


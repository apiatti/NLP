# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE77.txt
# newpar
# sent_id = 1
# text = Un bel giorno di sole la lepre e la tartaruga si incontrarono per caso e la lepre talmente era sicura di se stessa che propose alla tartaruga di sfidarsi di chi arrivava prima al traguardo la tartaruga accettò e così la lepre come una freccia e ad un certo punto la lepre smise di correre perchè era sicura di se stessa e dormì.
1	Un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	3	det	_	_
2	bel	bello	ADJ	A	Gender=Masc|Number=Sing	3	amod	_	_
3	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	12	dobj	_	_
4	di	di	ADP	E	_	5	case	_	_
5	sole	sole	NOUN	S	Gender=Masc|Number=Sing	3	nmod	_	_
6	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	7	det	_	_
7	lepre	lepra	NOUN	S	Gender=Fem|Number=Plur	12	nsubj	_	_
8	e	e	CONJ	CC	_	7	cc	_	_
9	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	10	det	_	_
10	tartaruga	tartaruga	NOUN	S	Gender=Fem|Number=Sing	7	conj	_	_
11	si	si	PRON	PC	Person=3|PronType=Clit	12	expl	_	_
12	incontrarono	incontrare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
13	per	per	ADP	E	_	14	case	_	_
14	caso	caso	NOUN	S	Gender=Masc|Number=Sing	12	nmod	_	_
15	e	e	CONJ	CC	_	12	cc	_	_
16	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	17	det	_	_
17	lepre	lepra	NOUN	S	Gender=Fem|Number=Plur	20	nsubj	_	_
18	talmente	talmente	ADV	B	_	20	advmod	_	_
19	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	20	cop	_	_
20	sicura	sicuro	ADJ	A	Gender=Fem|Number=Sing	12	conj	_	_
21	di	di	ADP	E	_	22	case	_	_
22	se	se	SCONJ	CS	_	40	mark	_	_
23	stessa	stesso	PRON	PD	Gender=Fem|Number=Sing|PronType=Dem	40	nsubj	_	_
24	che	che	PRON	PR	PronType=Rel	25	nsubj	_	_
25	propose	proporre	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	23	acl:relcl	_	_
26-27	alla	_	_	_	_	_	_	_	_
26	a	a	ADP	E	_	28	case	_	_
27	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	28	det	_	_
28	tartaruga	tartaruga	NOUN	S	Gender=Fem|Number=Sing	25	nmod	_	_
29	di	di	ADP	E	_	30	case	_	_
30	sfidarsi	sfidarsio	NOUN	S	Gender=Masc|Number=Plur	28	nmod	_	_
31	di	di	ADP	E	_	32	case	_	_
32	chi	chi	PRON	PR	PronType=Rel	30	nmod	_	_
33	arrivava	arrivare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	32	acl:relcl	_	_
34	prima	prima	ADV	B	_	37	case	_	_
35-36	al	_	_	_	_	_	_	_	_
35	a	a	ADP	E	_	34	mwe	_	_
36	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	37	det	_	_
37	traguardo	traguardo	NOUN	S	Gender=Masc|Number=Sing	33	nmod	_	_
38	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	39	det	_	_
39	tartaruga	tartaruga	NOUN	S	Gender=Fem|Number=Sing	33	dobj	_	_
40	accettò	accettare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	20	advcl	_	_
41	e	e	CONJ	CC	_	12	cc	_	_
42	così	così	ADV	B	_	44	advmod	_	_
43	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	44	det	_	_
44	lepre	lepra	NOUN	S	Gender=Fem|Number=Plur	12	conj	_	_
45	come	come	ADP	E	_	47	case	_	_
46	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	47	det	_	_
47	freccia	freccia	NOUN	S	Gender=Fem|Number=Sing	44	nmod	_	_
48	e	e	CONJ	CC	_	12	cc	_	_
49	ad	a	ADP	E	_	52	case	_	_
50	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	52	det	_	_
51	certo	certo	ADJ	A	Gender=Masc|Number=Sing	52	amod	_	_
52	punto	punto	NOUN	S	Gender=Masc|Number=Sing	55	nmod	_	_
53	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	54	det	_	_
54	lepre	lepra	NOUN	S	Gender=Fem|Number=Plur	55	nsubj	_	_
55	smise	smettere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	12	conj	_	_
56	di	di	ADP	E	_	57	mark	_	_
57	correre	correre	VERB	V	VerbForm=Inf	55	xcomp	_	_
58	perchè	perché	SCONJ	CS	_	60	mark	_	_
59	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	60	cop	_	_
60	sicura	sicuro	ADJ	A	Gender=Fem|Number=Sing	57	ccomp	_	_
61	di	di	ADP	E	_	62	case	_	_
62	se	se	SCONJ	CS	_	63	mark	_	_
63	stessa	stesso	ADJ	A	Gender=Fem|Number=Sing	60	csubj	_	_
64	e	e	CONJ	CC	_	63	cc	_	_
65	dormì	dormì	ADV	B	_	63	conj	_	SpaceAfter=No
66	.	.	PUNCT	FS	_	12	punct	_	_

# sent_id = 2
# text = La tartaruga andò al traguardo.
1	La	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	2	det	_	_
2	tartaruga	tartaruga	NOUN	S	Gender=Fem|Number=Sing	3	nsubj	_	_
3	andò	andare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
4-5	al	_	_	_	_	_	_	_	_
4	a	a	ADP	E	_	6	case	_	_
5	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	6	det	_	_
6	traguardo	traguardo	NOUN	S	Gender=Masc|Number=Sing	3	nmod	_	SpaceAfter=No
7	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 3
# text = La lepre si meravigliò di questa sua vittoria.
1	La	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	2	det	_	_
2	lepre	lepra	NOUN	S	Gender=Fem|Number=Plur	4	nsubj	_	_
3	si	si	PRON	PC	Person=3|PronType=Clit	4	expl	_	_
4	meravigliò	meravigliò	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
5	di	di	ADP	E	_	8	case	_	_
6	questa	questo	DET	DD	Gender=Fem|Number=Sing|PronType=Dem	8	det	_	_
7	sua	suo	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	8	det:poss	_	_
8	vittoria	vittoria	NOUN	S	Gender=Fem|Number=Sing	4	nmod	_	SpaceAfter=No
9	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 4
# text = La lepre li fece i suoi complimenti.
1	La	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	2	det	_	_
2	lepre	lepra	NOUN	S	Gender=Fem|Number=Plur	4	nsubj	_	_
3	li	li	PRON	PC	Gender=Masc|Number=Plur|Person=3|PronType=Clit	4	dobj	_	_
4	fece	fare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
5	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	7	det	_	_
6	suoi	suo	DET	AP	Gender=Masc|Number=Plur|Poss=Yes|PronType=Prs	7	det:poss	_	_
7	complimenti	complimento	NOUN	S	Gender=Masc|Number=Plur	4	dobj	_	SpaceAfter=No
8	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 5
# text = La tartaruga era felice di aver vinto era molto felice che andò a dirlo a tutti gli animali oppure chi vedeva passare davanti a lui dalla felicità.
1	La	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	2	det	_	_
2	tartaruga	tartaruga	NOUN	S	Gender=Fem|Number=Sing	4	nsubj	_	_
3	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	4	cop	_	_
4	felice	felice	ADJ	A	Number=Sing	0	root	_	_
5	di	di	ADP	E	_	7	mark	_	_
6	aver	avere	AUX	VA	VerbForm=Inf	7	aux	_	_
7	vinto	vincere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	4	acl	_	_
8	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	10	cop	_	_
9	molto	molto	ADV	B	_	10	advmod	_	_
10	felice	felice	ADJ	A	Number=Sing	7	xcomp	_	_
11	che	che	PRON	PR	PronType=Rel	12	nsubj	_	_
12	andò	andare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	10	acl:relcl	_	_
13	a	a	ADP	E	_	14	mark	_	_
14-15	dirlo	_	_	_	_	_	_	_	_
14	dir	dire	VERB	V	VerbForm=Inf	12	xcomp	_	_
15	lo	lo	PRON	PC	Gender=Masc|Number=Sing|Person=3|PronType=Clit	14	dobj	_	_
16	a	a	ADP	E	_	19	case	_	_
17	tutti	tutto	DET	T	Gender=Masc|Number=Plur	19	det:predet	_	_
18	gli	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	19	det	_	_
19	animali	animale	NOUN	S	Gender=Masc|Number=Plur	14	nmod	_	_
20	oppure	oppure	CONJ	CC	_	14	cc	_	_
21	chi	chi	PRON	PR	Number=Sing|PronType=Rel	14	conj	_	_
22	vedeva	vedere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	21	acl:relcl	_	_
23	passare	passare	VERB	V	VerbForm=Inf	22	xcomp	_	_
24	davanti	davanti	ADV	B	_	26	case	_	_
25	a	a	ADP	E	_	24	mwe	_	_
26	lui	lui	PRON	PE	Gender=Masc|Number=Sing|Person=3|PronType=Prs	23	nmod	_	_
27-28	dalla	_	_	_	_	_	_	_	_
27	da	da	ADP	E	_	29	case	_	_
28	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	29	det	_	_
29	felicità	felicità	NOUN	S	Gender=Fem	23	nmod	_	SpaceAfter=No
30	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 6
# text = Così fecero una festa di vittoria per la tartaruga.
1	Così	così	ADV	B	_	2	advmod	_	_
2	fecero	fare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
3	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	4	det	_	_
4	festa	festa	NOUN	S	Gender=Fem|Number=Sing	2	dobj	_	_
5	di	di	ADP	E	_	6	case	_	_
6	vittoria	vittoria	NOUN	S	Gender=Fem|Number=Sing	4	nmod	_	_
7	per	per	ADP	E	_	9	case	_	_
8	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	9	det	_	_
9	tartaruga	tartaruga	NOUN	S	Gender=Fem|Number=Sing	4	nmod	_	SpaceAfter=No
10	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 7
# text = Questa storia mi ha insegnato a non offendere nessuno ma essere gentili.
1	Questa	questo	DET	DD	Gender=Fem|Number=Sing|PronType=Dem	2	det	_	_
2	storia	storia	NOUN	S	Gender=Fem|Number=Sing	5	nsubj	_	_
3	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	5	dobj	_	_
4	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	insegnato	insegnare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
6	a	a	ADP	E	_	8	mark	_	_
7	non	non	ADV	BN	PronType=Neg	8	neg	_	_
8	offendere	offendere	VERB	V	VerbForm=Inf	5	advcl	_	_
9	nessuno	nessuno	PRON	PI	Gender=Masc|Number=Sing|PronType=Ind	8	dobj	_	_
10	ma	ma	CONJ	CC	_	8	cc	_	_
11	essere	essere	VERB	V	VerbForm=Inf	12	cop	_	_
12	gentili	gentile	ADJ	A	Number=Plur	8	conj	_	SpaceAfter=No
13	.	.	PUNCT	FS	_	5	punct	_	SpacesAfter=\r\n\r\n


# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE13.txt
# newpar
# sent_id = 1
# text = Un giorno all’oratorio Questa favola mi ha ricordato che, quando sono andato all’oratorio c’era un ragazzo delle medie che era bravissimo a giocare a ping-pong.
1	Un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	8	nmod	_	_
3	all’oratorio	all’oratorio	ADJ	A	Gender=Masc|Number=Sing	2	amod	_	SpacesAfter=\s\s
4	Questa	questo	DET	DD	Gender=Fem|Number=Sing|PronType=Dem	5	det	_	_
5	favola	favola	NOUN	S	Gender=Fem|Number=Sing	8	nsubj	_	_
6	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	8	dobj	_	_
7	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	8	aux	_	_
8	ricordato	ricordare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
9	che	che	SCONJ	CS	_	8	mwe	_	SpaceAfter=No
10	,	,	PUNCT	FF	_	8	punct	_	_
11	quando	quando	SCONJ	CS	_	13	mark	_	_
12	sono	essere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	13	aux	_	_
13	andato	andare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	15	advcl	_	_
14	all’oratorio	all’oratorio	ADJ	A	Gender=Masc|Number=Sing	13	xcomp	_	_
15	c’era	c’erare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	8	conj	_	_
16	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	17	det	_	_
17	ragazzo	ragazzo	NOUN	S	Gender=Masc|Number=Sing	15	dobj	_	_
18-19	delle	_	_	_	_	_	_	_	_
18	di	di	ADP	E	_	20	case	_	_
19	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	20	det	_	_
20	medie	media	NOUN	S	Gender=Fem|Number=Plur	17	nmod	_	_
21	che	che	PRON	PR	PronType=Rel	23	nsubj	_	_
22	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	23	cop	_	_
23	bravissimo	bravo	ADJ	A	Degree=Abs|Gender=Masc|Number=Sing	20	acl:relcl	_	_
24	a	a	ADP	E	_	25	mark	_	_
25	giocare	giocare	VERB	V	VerbForm=Inf	23	acl	_	_
26	a	a	ADP	E	_	27	case	_	_
27	ping-pong	ping-pong	NOUN	S	_	25	nmod	_	SpaceAfter=No
28	.	.	PUNCT	FS	_	8	punct	_	_

# sent_id = 2
# text = Quando l’ho voluto sfidare lui mi ha detto che non ce l’avrei mai fatta contro di lui.
1	Quando	quando	SCONJ	CS	_	4	mark	_	_
2	l’ho	l’avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	4	aux	_	_
3	voluto	volere	AUX	VM	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	4	aux	_	_
4	sfidare	sfidare	VERB	V	VerbForm=Inf	8	advcl	_	_
5	lui	lui	PRON	PE	Gender=Masc|Number=Sing|Person=3|PronType=Prs	8	nsubj	_	_
6	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	8	iobj	_	_
7	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	8	aux	_	_
8	detto	dire	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
9	che	che	SCONJ	CS	_	14	mark	_	_
10	non	non	ADV	BN	PronType=Neg	14	neg	_	_
11	ce	ce	PRON	PC	PronType=Clit	14	expl	_	_
12	l’avrei	l’avere	AUX	VM	Mood=Cnd|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	14	aux	_	_
13	mai	mai	ADV	B	_	14	advmod	_	_
14	fatta	fare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	8	ccomp	_	_
15	contro	contro	ADP	E	_	17	case	_	_
16	di	di	ADP	E	_	15	mwe	_	_
17	lui	lui	PRON	PE	Gender=Masc|Number=Sing|Person=3|PronType=Prs	14	nmod	_	SpaceAfter=No
18	.	.	PUNCT	FS	_	8	punct	_	_

# sent_id = 3
# text = Quando abbiamo incominciato lui era in vantaggio ma dopo ho rimontato di un casino e per pura fortuna ho vinto.
1	Quando	quando	SCONJ	CS	_	3	mark	_	_
2	abbiamo	avere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	3	aux	_	_
3	incominciato	incominciare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	5	advcl	_	_
4	lui	lui	PRON	PE	Gender=Masc|Number=Sing|Person=3|PronType=Prs	5	nsubj	_	_
5	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
6	in	in	ADP	E	_	7	case	_	_
7	vantaggio	vantaggio	NOUN	S	Gender=Masc|Number=Sing	5	nmod	_	_
8	ma	ma	CONJ	CC	_	5	cc	_	_
9	dopo	dopo	ADV	B	_	11	advmod	_	_
10	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	11	aux	_	_
11	rimontato	rimontare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	5	conj	_	_
12	di	di	ADP	E	_	14	case	_	_
13	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	14	det	_	_
14	casino	casino	NOUN	S	Gender=Masc|Number=Sing	11	nmod	_	_
15	e	e	CONJ	CC	_	14	cc	_	_
16	per	per	ADP	E	_	18	case	_	_
17	pura	puro	ADJ	A	Gender=Fem|Number=Sing	18	amod	_	_
18	fortuna	fortuna	NOUN	S	Gender=Fem|Number=Sing	20	nmod	_	_
19	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	20	aux	_	_
20	vinto	vincere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	14	conj	_	SpaceAfter=No
21	.	.	PUNCT	FS	_	5	punct	_	_

# sent_id = 4
# text = Quell’evento mi ha insegnato che non basta essere grandi per sapere che vincerai ma bisogna confrontarsi con qulcuno.
1	Quell’evento	Quell’evento	NUM	N	NumType=Card	4	nsubj	_	_
2	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	4	iobj	_	_
3	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	4	aux	_	_
4	insegnato	insegnare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
5	che	che	SCONJ	CS	_	7	mark	_	_
6	non	non	ADV	BN	PronType=Neg	7	neg	_	_
7	basta	bastare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	4	ccomp	_	_
8	essere	essere	VERB	V	VerbForm=Inf	9	cop	_	_
9	grandi	grande	ADJ	A	Number=Plur	7	xcomp	_	_
10	per	per	ADP	E	_	11	mark	_	_
11	sapere	sapere	VERB	V	VerbForm=Inf	9	acl	_	_
12	che	che	SCONJ	CS	_	13	mark	_	_
13	vincerai	vinceere	VERB	V	Mood=Ind|Number=Sing|Person=2|Tense=Fut|VerbForm=Fin	11	ccomp	_	_
14	ma	ma	CONJ	CC	_	13	cc	_	_
15	bisogna	bisognare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	13	conj	_	_
16	confrontarsi	confrontarsio	NOUN	S	Gender=Masc|Number=Plur	15	dobj	_	_
17	con	con	ADP	E	_	18	case	_	_
18	qulcuno	qulcuno	PRON	PI	Gender=Masc|Number=Sing|PronType=Ind	15	nmod	_	SpaceAfter=No
19	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 5
# text = Ho riflettuto e mi sono detto che tutti possono giocare divertendosi o solo per la voglia di vincere e passare in finale.
1	Ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	2	aux	_	_
2	riflettuto	riflettere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
3	e	e	CONJ	CC	_	2	cc	_	_
4	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	6	iobj	_	_
5	sono	essere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	6	aux	_	_
6	detto	dire	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	2	conj	_	_
7	che	che	SCONJ	CS	_	10	mark	_	_
8	tutti	tutto	PRON	PI	Gender=Masc|Number=Plur|PronType=Ind	10	nsubj	_	_
9	possono	potere	AUX	VM	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	10	aux	_	_
10	giocare	giocare	VERB	V	VerbForm=Inf	6	ccomp	_	_
11	divertendosi	divertendoso	ADJ	A	Gender=Masc|Number=Plur	10	xcomp	_	_
12	o	o	CONJ	CC	_	11	cc	_	_
13	solo	solo	ADV	B	_	16	advmod	_	_
14	per	per	ADP	E	_	16	case	_	_
15	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	16	det	_	_
16	voglia	voglia	NOUN	S	Gender=Fem|Number=Sing	11	conj	_	_
17	di	di	ADP	E	_	18	mark	_	_
18	vincere	vincere	VERB	V	VerbForm=Inf	16	acl	_	_
19	e	e	CONJ	CC	_	18	cc	_	_
20	passare	passare	VERB	V	VerbForm=Inf	18	conj	_	_
21	in	in	ADP	E	_	22	case	_	_
22	finale	finale	NOUN	S	Gender=Masc|Number=Sing	20	nmod	_	SpaceAfter=No
23	.	.	PUNCT	FS	_	2	punct	_	SpacesAfter=\r\n\r\n


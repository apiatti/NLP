# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE3.txt
# newpar
# sent_id = 1
# text = La vincita immediata La mia storia inizia un giorno quando stavo dormendo da mio cugino.
1	La	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	2	det	_	_
2	vincita	vincita	NOUN	S	Gender=Fem|Number=Sing	7	nsubj	_	_
3	immediata	immediare	ADJ	A	Gender=Fem|Number=Sing	2	amod	_	SpacesAfter=\s\s
4	La	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	6	det	_	_
5	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	6	det:poss	_	_
6	storia	storia	NOUN	S	Gender=Fem|Number=Sing	2	nmod	_	_
7	inizia	iniziare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
8	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	9	det	_	_
9	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	7	dobj	_	_
10	quando	quando	SCONJ	CS	_	11	mark	_	_
11	stavo	stare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	7	advcl	_	_
12	dormendo	dormire	VERB	V	VerbForm=Ger	11	advcl	_	_
13	da	da	ADP	E	_	15	case	_	_
14	mio	mio	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	15	det:poss	_	_
15	cugino	cugino	NOUN	S	Gender=Masc|Number=Sing	12	nmod	_	SpaceAfter=No
16	.	.	PUNCT	FS	_	7	punct	_	_

# sent_id = 2
# text = Eravamo nel parchetto dove facevamo sfide di parc-qur.
1	Eravamo	essere	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	0	root	_	_
2-3	nel	_	_	_	_	_	_	_	_
2	in	in	ADP	E	_	4	case	_	_
3	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	4	det	_	_
4	parchetto	parchetto	NOUN	S	Gender=Masc|Number=Sing	1	nmod	_	_
5	dove	dove	ADV	B	_	6	advmod	_	_
6	facevamo	facere	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	4	acl:relcl	_	_
7	sfide	sfida	NOUN	S	Gender=Fem|Number=Plur	6	dobj	_	_
8	di	di	ADP	E	_	9	case	_	_
9	parc-qur	parc-qure	NOUN	S	Gender=Masc	7	nmod	_	SpaceAfter=No
10	.	.	PUNCT	FS	_	1	punct	_	_

# sent_id = 3
# text = La prima partita consisteva in chi saliva piú velocemente su un albero e i suoi secondi erano 12.56 e i miei erano 7.02. Poi mi sentivo invincibile:
1	La	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	3	det	_	_
2	prima	primo	ADJ	NO	Gender=Fem|Number=Sing|NumType=Ord	3	amod	_	_
3	partita	partita	NOUN	S	Gender=Fem|Number=Sing	4	nsubj	_	_
4	consisteva	consistere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
5	in	in	ADP	E	_	6	case	_	_
6	chi	chi	PRON	PR	Number=Sing|PronType=Rel	4	nmod	_	_
7	saliva	salire	ADJ	A	Gender=Fem|Number=Sing	8	amod	_	_
8	piú	piú	PROPN	SP	_	18	nmod	_	_
9	velocemente	velocemente	ADV	B	_	8	advmod	_	_
10	su	su	ADP	E	_	12	case	_	_
11	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	12	det	_	_
12	albero	albero	NOUN	S	Gender=Masc|Number=Sing	8	nmod	_	_
13	e	e	CONJ	CC	_	12	cc	_	_
14	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	16	det	_	_
15	suoi	suo	DET	AP	Gender=Masc|Number=Plur|Poss=Yes|PronType=Prs	16	det:poss	_	_
16	secondi	secondo	NOUN	S	Gender=Masc|Number=Plur	12	conj	_	_
17	erano	essere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	18	cop	_	_
18	12.56	12.56	NUM	N	NumType=Card	6	acl:relcl	_	_
19	e	e	CONJ	CC	_	4	cc	_	_
20	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	21	det	_	_
21	miei	mio	PRON	PP	Gender=Masc|Number=Plur|Poss=Yes|PronType=Prs	23	nsubj	_	_
22	erano	essere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	23	cop	_	_
23	7.02	7.02	NUM	N	NumType=Card	4	conj	_	SpaceAfter=No
24	.	.	PUNCT	FS	_	4	punct	_	_
25	Poi	poi	ADV	B	_	27	advmod	_	_
26	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	27	expl	_	_
27	sentivo	sentire	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	4	conj	_	_
28	invincibile	invincibile	ADJ	A	Number=Sing	27	amod	_	SpaceAfter=No
29	:	:	PUNCT	FC	_	4	punct	_	_

# sent_id = 4
# text = la seconda gara consisteva a correre 10 volte su circa 10 m2; e là era pareggio i secondi erano 35.92.
1	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	3	det	_	_
2	seconda	secondo	ADJ	NO	Gender=Fem|Number=Sing|NumType=Ord	3	amod	_	_
3	gara	gara	NOUN	S	Gender=Fem|Number=Sing	4	nsubj	_	_
4	consisteva	consistere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
5	a	a	ADP	E	_	6	mark	_	_
6	correre	correre	VERB	V	VerbForm=Inf	4	xcomp	_	_
7	10	10	NUM	N	NumType=Card	8	nummod	_	_
8	volte	volta	NOUN	S	Gender=Fem|Number=Plur	6	nmod	_	_
9	su	su	ADP	E	_	12	case	_	_
10	circa	circa	ADV	B	_	11	advmod	_	_
11	10	10	NUM	N	NumType=Card	12	compound	_	_
12	m2	m2	NUM	N	NumType=Card	6	nummod	_	SpaceAfter=No
13	;	;	PUNCT	FC	_	4	punct	_	_
14	e	e	CONJ	CC	_	4	cc	_	_
15	là	là	ADV	B	_	17	advmod	_	_
16	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	17	cop	_	_
17	pareggio	pareggio	NOUN	S	Gender=Masc|Number=Sing	4	conj	_	_
18	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	19	det	_	_
19	secondi	secondo	NOUN	S	Gender=Masc|Number=Plur	17	nmod	_	_
20	erano	essere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	21	cop	_	_
21	35.92	35.92	NUM	N	NumType=Card	19	nummod	_	SpaceAfter=No
22	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 5
# text = Poi la terza gara consisteva nel salto in lungo, e lui aveva vinto con 1.20 e io perso con 1.10 m.
1	Poi	poi	ADV	B	_	5	advmod	_	_
2	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	4	det	_	_
3	terza	terzo	ADJ	NO	Gender=Fem|Number=Sing|NumType=Ord	4	amod	_	_
4	gara	gara	NOUN	S	Gender=Fem|Number=Sing	5	nsubj	_	_
5	consisteva	consistere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
6-7	nel	_	_	_	_	_	_	_	_
6	in	in	ADP	E	_	8	case	_	_
7	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	8	det	_	_
8	salto	salto	NOUN	S	Gender=Masc|Number=Sing	5	nmod	_	_
9	in	in	ADP	E	_	10	case	_	_
10	lungo	lungo	NOUN	S	Gender=Masc|Number=Sing	5	nmod	_	SpaceAfter=No
11	,	,	PUNCT	FF	_	5	punct	_	_
12	e	e	CONJ	CC	_	5	cc	_	_
13	lui	lui	PRON	PE	Gender=Masc|Number=Sing|Person=3|PronType=Prs	15	nsubj	_	_
14	aveva	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	15	aux	_	_
15	vinto	vincere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	5	conj	_	_
16	con	con	ADP	E	_	17	case	_	_
17	1.20	1.20	NUM	N	NumType=Card	15	nummod	_	_
18	e	e	CONJ	CC	_	15	cc	_	_
19	io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	15	conj	_	_
20	perso	perdere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	19	acl	_	_
21	con	con	ADP	E	_	23	case	_	_
22	1.10	1.10	NUM	N	NumType=Card	23	nummod	_	_
23	m	metro	NOUN	S	Gender=Masc	20	nmod	_	SpaceAfter=No
24	.	.	PUNCT	FS	_	5	punct	_	_

# sent_id = 6
# text = Poi sono arrivati 2 bambini a sfidarci a parc-qur.
1	Poi	poi	ADV	B	_	3	advmod	_	_
2	sono	essere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	3	aux	_	_
3	arrivati	arrivare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	0	root	_	_
4	2	2	NUM	N	NumType=Card	5	nummod	_	_
5	bambini	bambino	NOUN	S	Gender=Masc|Number=Plur	3	nsubj	_	_
6	a	a	ADP	E	_	7	case	_	_
7	sfidarci	sfidarco	NOUN	S	Gender=Masc|Number=Plur	5	nmod	_	_
8	a	a	ADP	E	_	9	case	_	_
9	parc-qur	parc-qure	NOUN	S	Gender=Masc	7	nmod	_	SpaceAfter=No
10	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 7
# text = Li avevamo battuti ed eravamo felici e siamo tornati a fare parc-qur.
1	Li	li	PRON	PC	Gender=Masc|Number=Plur|Person=3|PronType=Clit	3	dobj	_	_
2	avevamo	avere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	3	aux	_	_
3	battuti	battere	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	0	root	_	_
4	ed	e	CONJ	CC	_	3	cc	_	_
5	eravamo	essere	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	3	conj	_	_
6	felici	felice	ADJ	A	Number=Plur	5	xcomp	_	_
7	e	e	CONJ	CC	_	3	cc	_	_
8	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	9	aux	_	_
9	tornati	tornare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	3	conj	_	_
10	a	a	ADP	E	_	11	mark	_	_
11	fare	fare	VERB	V	VerbForm=Inf	9	xcomp	_	_
12	parc-qur	parc-qure	ADV	B	_	11	advmod	_	SpaceAfter=No
13	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 8
# text = Mi ha insegnato che se le cose si fanno Insieme si fanno meglio.
1	Mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	3	nsubj	_	_
2	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	3	aux	_	_
3	insegnato	insegnare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
4	che	che	SCONJ	CS	_	12	mark	_	_
5	se	se	SCONJ	CS	_	9	mark	_	_
6	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	7	det	_	_
7	cose	cosa	NOUN	S	Gender=Fem|Number=Plur	9	dobj	_	_
8	si	si	PRON	PC	Person=3|PronType=Clit	9	expl:impers	_	_
9	fanno	fare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	12	advcl	_	_
10	Insieme	insieme	ADV	B	_	9	advmod	_	_
11	si	si	PRON	PC	Person=3|PronType=Clit	12	expl:impers	_	_
12	fanno	fare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	3	ccomp	_	_
13	meglio	meglio	ADV	B	_	12	advmod	_	SpaceAfter=No
14	.	.	PUNCT	FS	_	3	punct	_	SpacesAfter=\r\n\r\n


# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE12.txt
# newpar
# sent_id = 1
# text = Al mare con i nonni!
1-2	Al	_	_	_	_	_	_	_	_
1	A	a	ADP	E	_	3	case	_	_
2	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	3	det	_	_
3	mare	mare	NOUN	S	Gender=Masc|Number=Sing	0	root	_	_
4	con	con	ADP	E	_	6	case	_	_
5	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	6	det	_	_
6	nonni	nonno	NOUN	S	Gender=Masc|Number=Plur	3	nmod	_	SpaceAfter=No
7	!	!	PUNCT	FS	_	3	punct	_	SpacesAfter=\s\s

# sent_id = 2
# text = Una mattina d’etate, ad agosto, eravamo al mare in Portogallo.
1	Una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	2	det	_	_
2	mattina	mattina	NOUN	S	Gender=Fem|Number=Sing	8	nsubj	_	_
3	d’etate	d’etare	VERB	V	Gender=Fem|Number=Plur|Tense=Past|VerbForm=Part	2	acl	_	SpaceAfter=No
4	,	,	PUNCT	FF	_	2	punct	_	_
5	ad	a	ADP	E	_	6	case	_	_
6	agosto	agosto	NOUN	S	Gender=Masc|Number=Sing	8	nmod	_	SpaceAfter=No
7	,	,	PUNCT	FF	_	6	punct	_	_
8	eravamo	essere	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	0	root	_	_
9-10	al	_	_	_	_	_	_	_	_
9	a	a	ADP	E	_	11	case	_	_
10	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	11	det	_	_
11	mare	mare	NOUN	S	Gender=Masc|Number=Sing	8	nmod	_	_
12	in	in	ADP	E	_	13	case	_	_
13	Portogallo	Portogallo	PROPN	SP	_	8	nmod	_	SpaceAfter=No
14	.	.	PUNCT	FS	_	8	punct	_	_

# sent_id = 3
# text = Io e mio fratello abbiamo chiesto se potevammo andare a fare il bagno, ma i nonni dissero di non andare perché c’erano tante onde.
1	Io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	6	nsubj	_	_
2	e	e	CONJ	CC	_	1	cc	_	_
3	mio	mio	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	4	det:poss	_	_
4	fratello	fratello	NOUN	S	Gender=Masc|Number=Sing	1	conj	_	_
5	abbiamo	avere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	6	aux	_	_
6	chiesto	chiedere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
7	se	se	SCONJ	CS	_	8	mark	_	_
8	potevammo	potevare	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Past|VerbForm=Fin	6	ccomp	_	_
9	andare	andare	VERB	V	VerbForm=Inf	8	ccomp	_	_
10	a	a	ADP	E	_	11	mark	_	SpacesAfter=\s\s
11	fare	fare	VERB	V	VerbForm=Inf	9	xcomp	_	_
12	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	13	det	_	_
13	bagno	bagno	NOUN	S	Gender=Masc|Number=Sing	11	dobj	_	SpaceAfter=No
14	,	,	PUNCT	FF	_	8	punct	_	_
15	ma	ma	CONJ	CC	_	8	cc	_	_
16	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	17	det	_	_
17	nonni	nonno	NOUN	S	Gender=Masc|Number=Plur	18	nsubj	_	_
18	dissero	dire	VERB	V	Mood=Sub|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	8	conj	_	_
19	di	di	ADP	E	_	21	mark	_	_
20	non	non	ADV	BN	PronType=Neg	21	neg	_	_
21	andare	andare	VERB	V	VerbForm=Inf	18	xcomp	_	_
22	perché	perché	SCONJ	CS	_	23	mark	_	_
23	c’erano	c’erare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	21	advcl	_	_
24	tante	tanto	DET	DI	Gender=Fem|Number=Plur|PronType=Ind	25	det	_	_
25	onde	onda	NOUN	S	Gender=Fem|Number=Plur	23	dobj	_	SpaceAfter=No
26	.	.	PUNCT	FS	_	6	punct	_	_

# sent_id = 4
# text = La sera siamo andati in campeggio e io mio fratello ci siamo messi a giocare.
1	La	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	2	det	_	_
2	sera	sera	NOUN	S	Gender=Fem|Number=Sing	4	nsubj	_	_
3	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	4	aux	_	_
4	andati	andare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	0	root	_	_
5	in	in	ADP	E	_	6	case	_	_
6	campeggio	campeggio	NOUN	S	Gender=Masc|Number=Sing	4	nmod	_	_
7	e	e	CONJ	CC	_	4	cc	_	_
8	io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	13	nsubj	_	_
9	mio	mio	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	10	det:poss	_	_
10	fratello	fratello	NOUN	S	Gender=Masc|Number=Sing	13	nmod	_	_
11	ci	ci	PRON	PC	PronType=Clit	13	advmod	_	_
12	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	13	aux	_	_
13	messi	mettere	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	4	conj	_	_
14	a	a	ADP	E	_	15	mark	_	_
15	giocare	giocare	VERB	V	VerbForm=Inf	13	xcomp	_	SpaceAfter=No
16	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 5
# text = Per esempio giocavamo a tennis e mio fratello si arrabbiava perché riuscivo meglio di lui.
1	Per	per	ADP	E	_	2	case	_	_
2	esempio	esempio	NOUN	S	Gender=Masc|Number=Sing	3	nmod	_	_
3	giocavamo	giocare	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	0	root	_	_
4	a	a	ADP	E	_	5	case	_	_
5	tennis	tennis	NOUN	S	Gender=Fem	3	nmod	_	_
6	e	e	CONJ	CC	_	3	cc	_	_
7	mio	mio	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	8	det:poss	_	_
8	fratello	fratello	NOUN	S	Gender=Masc|Number=Sing	10	dobj	_	_
9	si	si	PRON	PC	Person=3|PronType=Clit	10	expl:impers	_	_
10	arrabbiava	arrabbiare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	3	conj	_	_
11	perché	perché	SCONJ	CS	_	12	mark	_	_
12	riuscivo	riuscire	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	10	advcl	_	_
13	meglio	meglio	ADV	B	_	12	advmod	_	_
14	di	di	ADP	E	_	15	case	_	_
15	lui	lui	PRON	PE	Gender=Masc|Number=Sing|Person=3|PronType=Prs	12	nmod	_	SpaceAfter=No
16	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 6
# text = Poi siamo andati a mangiare e dopo cena ci siamo gustati un gelato e dopo una passeggiata siamo andati a dormire.
1	Poi	poi	ADV	B	_	3	advmod	_	_
2	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	3	aux	_	_
3	andati	andare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	0	root	_	_
4	a	a	ADP	E	_	5	mark	_	_
5	mangiare	mangiare	VERB	V	VerbForm=Inf	3	xcomp	_	_
6	e	e	CONJ	CC	_	3	cc	_	_
7	dopo	dopo	ADP	E	_	8	case	_	_
8	cena	cena	NOUN	S	Gender=Fem|Number=Sing	11	nmod	_	_
9	ci	ci	PRON	PC	Number=Plur|Person=1|PronType=Clit	11	expl	_	_
10	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	11	aux	_	_
11	gustati	gustare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	3	conj	_	_
12	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	13	det	_	_
13	gelato	gelato	ADJ	A	Gender=Masc|Number=Sing	11	xcomp	_	_
14	e	e	CONJ	CC	_	13	cc	_	_
15	dopo	dopo	ADP	E	_	17	case	_	_
16	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	17	det	_	_
17	passeggiata	passeggiata	NOUN	S	Gender=Fem|Number=Sing	19	nmod	_	_
18	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	19	aux	_	_
19	andati	andare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	13	conj	_	_
20	a	a	ADP	E	_	21	mark	_	_
21	dormire	dormire	VERB	V	VerbForm=Inf	19	xcomp	_	SpaceAfter=No
22	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 7
# text = Il giorno dopo siamo dovuti tornare a casa e arrivati a casa abbiamo fatto pranzo, il pomeriggio siamo andati da nostro cugino a prendere i cani e poi siamo andati in piscina Lì giocato e ci siamo divertiti un mondo.
1	Il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	5	nmod	_	_
3	dopo	dopo	ADV	B	_	5	advmod	_	_
4	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	dovuti	dovere	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	0	root	_	_
6	tornare	tornare	VERB	V	VerbForm=Inf	5	xcomp	_	_
7	a	a	ADP	E	_	8	case	_	_
8	casa	casa	NOUN	S	Gender=Fem|Number=Sing	6	nmod	_	_
9	e	e	CONJ	CC	_	6	cc	_	_
10	arrivati	arrivare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	6	conj	_	_
11	a	a	ADP	E	_	12	case	_	_
12	casa	casa	NOUN	S	Gender=Fem|Number=Sing	10	nmod	_	_
13	abbiamo	avere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	14	aux	_	_
14	fatto	fare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	5	advcl	_	_
15	pranzo	pranzo	NOUN	S	Gender=Masc|Number=Sing	14	dobj	_	SpaceAfter=No
16	,	,	PUNCT	FF	_	14	punct	_	_
17	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	18	det	_	SpacesAfter=\s\s
18	pomeriggio	pomeriggio	NOUN	S	Gender=Masc|Number=Sing	20	nsubjpass	_	_
19	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	20	auxpass	_	_
20	andati	andare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	14	conj	_	_
21	da	da	ADP	E	_	23	case	_	_
22	nostro	nostro	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	23	det:poss	_	_
23	cugino	cugino	NOUN	S	Gender=Masc|Number=Sing	20	nmod	_	_
24	a	a	ADP	E	_	25	mark	_	_
25	prendere	prendere	VERB	V	VerbForm=Inf	20	xcomp	_	_
26	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	27	det	_	SpacesAfter=\s\s
27	cani	cane	NOUN	S	Gender=Masc|Number=Plur	25	dobj	_	_
28	e	e	CONJ	CC	_	14	cc	_	_
29	poi	poi	ADV	B	_	31	advmod	_	_
30	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	31	aux	_	_
31	andati	andare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	14	conj	_	_
32	in	in	ADP	E	_	33	case	_	_
33	piscina	piscina	NOUN	S	Gender=Fem|Number=Sing	31	nmod	_	_
34	Lì	lì	ADV	B	_	35	advmod	_	_
35	giocato	giocare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	31	xcomp	_	_
36	e	e	CONJ	CC	_	14	cc	_	_
37	ci	ci	PRON	PC	PronType=Clit	39	advmod	_	_
38	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	39	aux	_	_
39	divertiti	divertire	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	14	conj	_	_
40	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	41	det	_	_
41	mondo	mondo	NOUN	S	Gender=Masc|Number=Sing	39	dobj	_	SpaceAfter=No
42	.	.	PUNCT	FS	_	5	punct	_	_

# sent_id = 8
# text = Nel tardo poriggio é arrivata nostra cugina di 3 anni.
1-2	Nel	_	_	_	_	_	_	_	_
1	In	in	ADP	E	_	4	case	_	_
2	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	4	det	_	_
3	tardo	tardo	ADJ	A	Gender=Masc|Number=Sing	4	amod	_	_
4	poriggio	poriggio	NOUN	S	Gender=Masc|Number=Sing	5	nmod	_	_
5	é	é	PRON	PP	Number=Sing|Poss=Yes|PronType=Prs	0	root	_	_
6	arrivata	arrivare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	5	acl	_	_
7	nostra	nostro	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	8	det:poss	_	_
8	cugina	cugina	NOUN	S	Gender=Fem|Number=Sing	6	nmod	_	_
9	di	di	ADP	E	_	11	case	_	_
10	3	3	NUM	N	NumType=Card	11	nummod	_	_
11	anni	anno	NOUN	S	Gender=Masc|Number=Plur	8	nmod	_	SpaceAfter=No
12	.	.	PUNCT	FS	_	5	punct	_	_

# sent_id = 9
# text = È voluta venire in piscina, ma non sa nuotare e ha rischiato di annegare.
1	È	essere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	2	aux	_	_
2	voluta	volere	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
3	venire	venire	VERB	V	VerbForm=Inf	2	xcomp	_	_
4	in	in	ADP	E	_	5	case	_	_
5	piscina	piscina	NOUN	S	Gender=Fem|Number=Sing	3	nmod	_	SpaceAfter=No
6	,	,	PUNCT	FF	_	2	punct	_	_
7	ma	ma	CONJ	CC	_	2	cc	_	_
8	non	non	ADV	BN	PronType=Neg	10	neg	_	_
9	sa	sapere	AUX	VM	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	10	aux	_	_
10	nuotare	nuotare	VERB	V	VerbForm=Inf	2	conj	_	_
11	e	e	CONJ	CC	_	2	cc	_	_
12	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	13	aux	_	_
13	rischiato	rischiare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	2	conj	_	_
14	di	di	ADP	E	_	15	mark	_	_
15	annegare	annegare	VERB	V	VerbForm=Inf	13	xcomp	_	SpaceAfter=No
16	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 10
# text = L’abbiamo aiutata ad uscire dalla piscina e le abbiamo messo i braccioli.
1	L’abbiamo	l’abbiamo	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	0	root	_	_
2	aiutata	aiutare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	1	xcomp	_	_
3	ad	a	ADP	E	_	4	mark	_	_
4	uscire	uscire	VERB	V	VerbForm=Inf	2	xcomp	_	_
5-6	dalla	_	_	_	_	_	_	_	_
5	da	da	ADP	E	_	7	case	_	_
6	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	7	det	_	_
7	piscina	piscina	NOUN	S	Gender=Fem|Number=Sing	4	nmod	_	_
8	e	e	CONJ	CC	_	1	cc	_	_
9	le	lo	PRON	PC	Gender=Fem|Number=Sing|Person=3|PronType=Clit	11	dobj	_	_
10	abbiamo	avere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	11	aux	_	_
11	messo	mettere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	1	conj	_	_
12	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	13	det	_	SpacesAfter=\s\s
13	braccioli	bracciolo	NOUN	S	Gender=Masc|Number=Plur	11	nsubj	_	SpaceAfter=No
14	.	.	PUNCT	FS	_	1	punct	_	SpacesAfter=\s\s

# sent_id = 11
# text = insegnamento:
1	insegnamento	insegnamento	NOUN	S	Gender=Masc|Number=Sing	0	root	_	SpaceAfter=No
2	:	:	PUNCT	FC	_	1	punct	_	_

# sent_id = 12
# text = Mi ha insegnato ad avere prudenza e attenzione alle cose (vedi piscina) che capitano e non arrabbiarsi per niente (vedi tennis)!
1	Mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	3	dobj	_	_
2	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	3	aux	_	_
3	insegnato	insegnare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
4	ad	a	ADP	E	_	5	mark	_	_
5	avere	avere	VERB	V	VerbForm=Inf	3	xcomp	_	_
6	prudenza	prudenza	NOUN	S	Gender=Fem|Number=Sing	5	dobj	_	_
7	e	e	CONJ	CC	_	6	cc	_	_
8	attenzione	attenzione	NOUN	S	Gender=Fem|Number=Sing	6	conj	_	_
9-10	alle	_	_	_	_	_	_	_	_
9	a	a	ADP	E	_	11	case	_	_
10	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	11	det	_	_
11	cose	cosa	NOUN	S	Gender=Fem|Number=Plur	5	nmod	_	_
12	(	(	PUNCT	FB	_	13	punct	_	SpaceAfter=No
13	vedi	vedere	VERB	V	Mood=Imp|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	3	advcl	_	_
14	piscina	piscina	NOUN	S	Gender=Fem|Number=Sing	13	dobj	_	SpaceAfter=No
15	)	)	PUNCT	FB	_	13	punct	_	_
16	che	che	PRON	PR	PronType=Rel	17	nsubj	_	_
17	capitano	capitare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	3	acl:relcl	_	_
18	e	e	CONJ	CC	_	17	cc	_	_
19	non	non	ADV	BN	PronType=Neg	20	neg	_	_
20	arrabbiarsi	arrabbiarire	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	17	conj	_	_
21	per	per	ADP	E	_	22	case	_	_
22	niente	niente	PRON	PI	Gender=Masc|Number=Sing|PronType=Ind	20	nmod	_	_
23	(	(	PUNCT	FB	_	24	punct	_	SpaceAfter=No
24	vedi	vedere	VERB	V	Mood=Imp|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	22	advcl	_	_
25	tennis)!	tennis)!	PUNCT	FS	_	3	punct	_	SpacesAfter=\s\s

# sent_id = 13
# text = THE END.
1	THE	the	PROPN	SP	_	0	root	_	_
2	END.	END.	PROPN	SP	_	1	name	_	SpacesAfter=\r\n\r\n


# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE80.txt
# newpar
# sent_id = 1
# text = Milan vs Juventus In una partita di calcio era l’inizio Juve aveva fatto un fallo avevano battuto la, palla girava il portiere l’aveva parata pero la palla girava il portiere si vantava e la palla entrò in porta.
1	Milan	Milan	PROPN	SP	_	10	nsubj	_	_
2	vs	vs	PROPN	SP	_	1	name	_	_
3	Juventus	Juventus	PROPN	SP	_	1	name	_	_
4	In	in	ADP	E	_	6	case	_	_
5	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	6	det	_	_
6	partita	partita	NOUN	S	Gender=Fem|Number=Sing	10	nmod	_	_
7	di	di	ADP	E	_	8	case	_	_
8	calcio	calcio	NOUN	S	Gender=Masc|Number=Sing	6	nmod	_	_
9	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	10	cop	_	_
10	l’inizio	l’inizio	NOUN	S	Gender=Masc|Number=Sing	0	root	_	_
11	Juve	Juve	PROPN	SP	_	13	nsubj	_	_
12	aveva	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	13	aux	_	_
13	fatto	fare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	10	parataxis	_	_
14	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	15	det	_	_
15	fallo	fallo	NOUN	S	Gender=Masc|Number=Sing	17	nsubj	_	_
16	avevano	avere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	17	aux	_	_
17	battuto	battere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	13	ccomp	_	_
18	la	la	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	20	det	_	SpaceAfter=No
19	,	,	PUNCT	FF	_	18	punct	_	_
20	palla	palla	NOUN	S	Gender=Fem|Number=Sing	21	nsubj	_	_
21	girava	girare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	17	ccomp	_	_
22	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	23	det	_	_
23	portiere	portiere	NOUN	S	Gender=Masc|Number=Sing	25	nsubj	_	_
24	l’aveva	l’avere	AUX	VM	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	25	aux	_	_
25	parata	parare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	21	advcl	_	_
26	pero	perare	NOUN	S	Gender=Masc|Number=Sing	33	nsubj	_	_
27	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	28	det	_	_
28	palla	palla	NOUN	S	Gender=Fem|Number=Sing	29	nsubj	_	_
29	girava	girare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	26	acl:relcl	_	_
30	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	31	det	_	_
31	portiere	portiere	NOUN	S	Gender=Masc|Number=Sing	29	dobj	_	_
32	si	si	PRON	PC	Person=3|PronType=Clit	33	expl	_	_
33	vantava	vantare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	25	advcl	_	_
34	e	e	CONJ	CC	_	33	cc	_	_
35	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	36	det	_	_
36	palla	palla	NOUN	S	Gender=Fem|Number=Sing	37	nsubj	_	_
37	entrò	entrare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	33	conj	_	_
38	in	in	ADP	E	_	39	case	_	_
39	porta	porta	NOUN	S	Gender=Fem|Number=Sing	37	nmod	_	SpaceAfter=No
40	.	.	PUNCT	FS	_	10	punct	_	SpacesAfter=\r\n\r\n


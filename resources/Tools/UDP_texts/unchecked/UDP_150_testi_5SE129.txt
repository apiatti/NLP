# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE129.txt
# newpar
# sent_id = 1
# text = Un giorno sono dovuta andare ad un matrimonio di Suzana e Andrea.
1	Un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	4	nmod	_	_
3	sono	essere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	4	aux	_	_
4	dovuta	dovere	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
5	andare	andare	VERB	V	VerbForm=Inf	4	xcomp	_	_
6	ad	a	ADP	E	_	8	case	_	_
7	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	8	det	_	_
8	matrimonio	matrimonio	NOUN	S	Gender=Masc|Number=Sing	5	nmod	_	_
9	di	di	ADP	E	_	10	case	_	_
10	Suzana	Suzana	PROPN	SP	_	8	nmod	_	_
11	e	e	CONJ	CC	_	10	cc	_	_
12	Andrea	Andrea	PROPN	SP	_	10	conj	_	SpaceAfter=No
13	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 2
# text = C’erano anche due bambini:
1	C’erano	C’erare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
2	anche	anche	ADV	B	_	3	advmod	_	_
3	due	due	NUM	N	NumType=Card	4	nummod	_	_
4	bambini	bambino	NOUN	S	Gender=Masc|Number=Plur	1	dobj	_	SpaceAfter=No
5	:	:	PUNCT	FC	_	1	punct	_	_

# sent_id = 3
# text = il più grande aveva circa nove anni e l’altro sette.
1	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	3	det	_	_
2	più	più	ADV	B	_	3	advmod	_	_
3	grande	grande	ADJ	A	Number=Sing	4	nsubj	_	_
4	aveva	avere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
5	circa	circa	ADV	B	_	7	advmod	_	_
6	nove	nove	NUM	N	NumType=Card	7	nummod	_	_
7	anni	anno	NOUN	S	Gender=Masc|Number=Plur	4	dobj	_	_
8	e	e	CONJ	CC	_	4	cc	_	_
9	l’altro	l’altro	DET	DI	Gender=Masc|Number=Sing|PronType=Ind	10	det	_	_
10	sette	sette	NUM	N	NumType=Card	4	conj	_	SpaceAfter=No
11	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 4
# text = Loro all’inizio giocavano a calcio dato che a pochi metri c’è un campo di calcio :
1	Loro	loro	DET	AP	Gender=Masc|Poss=Yes|PronType=Prs	2	det	_	_
2	all’inizio	all’inizio	NOUN	S	Gender=Masc|Number=Sing	3	nsubj	_	_
3	giocavano	giocare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	0	root	_	_
4	a	a	ADP	E	_	5	case	_	_
5	calcio	calcio	NOUN	S	Gender=Masc|Number=Sing	3	nmod	_	_
6	dato	dare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	3	advcl	_	_
7	che	che	SCONJ	CS	_	11	mark	_	_
8	a	a	ADP	E	_	10	case	_	_
9	pochi	poco	DET	DI	Gender=Masc|Number=Plur|PronType=Ind	10	det	_	_
10	metri	metro	NOUN	S	Gender=Masc|Number=Plur	11	nmod	_	_
11	c’è	c’ere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	6	ccomp	_	_
12	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	13	det	_	_
13	campo	campo	NOUN	S	Gender=Masc|Number=Sing	11	dobj	_	_
14	di	di	ADP	E	_	15	case	_	_
15	calcio	calcio	NOUN	S	Gender=Masc|Number=Sing	13	nmod	_	_
16	:	:	PUNCT	FC	_	3	punct	_	_

# sent_id = 5
# text = Io e Suzana (la sposa) siamo andate a giocare a calcio con i bambini.
1	Io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	9	nsubj	_	_
2	e	e	CONJ	CC	_	1	cc	_	_
3	Suzana	Suzana	PROPN	SP	_	1	conj	_	_
4	(	(	PUNCT	FB	_	6	punct	_	SpaceAfter=No
5	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	6	det	_	_
6	sposa	sposo	NOUN	S	Gender=Fem|Number=Sing	1	appos	_	SpaceAfter=No
7	)	)	PUNCT	FB	_	6	punct	_	_
8	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	9	aux	_	_
9	andate	andare	VERB	V	Gender=Fem|Number=Plur|Tense=Past|VerbForm=Part	0	root	_	_
10	a	a	ADP	E	_	11	mark	_	_
11	giocare	giocare	VERB	V	VerbForm=Inf	9	xcomp	_	_
12	a	a	ADP	E	_	13	case	_	_
13	calcio	calcio	NOUN	S	Gender=Masc|Number=Sing	11	nmod	_	_
14	con	con	ADP	E	_	16	case	_	_
15	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	16	det	_	_
16	bambini	bambino	NOUN	S	Gender=Masc|Number=Plur	11	nmod	_	SpaceAfter=No
17	.	.	PUNCT	FS	_	9	punct	_	_

# sent_id = 6
# text = Io avevo le ballerine ma lei aveva le scarpe col tacco di quindici centimetri, quindi cadeva molto spesso e i due bambini la prendevano in giro, lei gli avvisò che era lei infine a prendere in giro loro, i bambini di misero a ridere.
1	Io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	2	nsubj	_	_
2	avevo	avere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	0	root	_	_
3	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	4	det	_	_
4	ballerine	ballerina	NOUN	S	Gender=Fem|Number=Plur	2	dobj	_	_
5	ma	ma	CONJ	CC	_	2	cc	_	_
6	lei	lei	PRON	PE	Number=Sing|Person=3|PronType=Prs	7	nsubj	_	_
7	aveva	avere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	2	conj	_	_
8	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	9	det	_	_
9	scarpe	scarpa	NOUN	S	Gender=Fem|Number=Plur	7	dobj	_	_
10-11	col	_	_	_	_	_	_	_	_
10	con	con	ADP	E	_	12	case	_	_
11	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	12	det	_	_
12	tacco	tacco	NOUN	S	Gender=Masc|Number=Sing	7	nmod	_	_
13	di	di	ADP	E	_	15	case	_	_
14	quindici	quindici	NUM	N	NumType=Card	15	nummod	_	_
15	centimetri	centimetro	NOUN	S	Gender=Masc|Number=Plur	12	nmod	_	SpaceAfter=No
16	,	,	PUNCT	FF	_	2	punct	_	_
17	quindi	quindi	ADV	B	_	18	advmod	_	_
18	cadeva	cadere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	2	conj	_	_
19	molto	molto	ADV	B	_	20	advmod	_	_
20	spesso	spesso	ADV	B	_	18	advmod	_	_
21	e	e	CONJ	CC	_	20	cc	_	_
22	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	24	det	_	_
23	due	due	NUM	N	NumType=Card	24	nummod	_	_
24	bambini	bambino	NOUN	S	Gender=Masc|Number=Plur	26	nsubj	_	_
25	la	la	PRON	PC	Gender=Fem|Number=Sing|Person=3|PronType=Clit	26	dobj	_	_
26	prendevano	prendere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	20	conj	_	_
27	in	in	ADP	E	_	28	case	_	_
28	giro	giro	NOUN	S	Gender=Masc|Number=Sing	26	nmod	_	SpaceAfter=No
29	,	,	PUNCT	FF	_	2	punct	_	_
30	lei	lei	PRON	PE	Number=Sing|Person=3|PronType=Prs	32	nsubj	_	_
31	gli	gli	PRON	PC	Gender=Masc|Number=Sing|Person=3|PronType=Clit	32	iobj	_	_
32	avvisò	avvisare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	2	conj	_	_
33	che	che	SCONJ	CS	_	35	mark	_	_
34	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	35	cop	_	_
35	lei	lei	PRON	PE	Number=Sing|Person=3|PronType=Prs	32	ccomp	_	_
36	infine	infine	ADV	B	_	35	advmod	_	_
37	a	a	ADP	E	_	38	mark	_	_
38	prendere	prendere	VERB	V	VerbForm=Inf	35	ccomp	_	_
39	in	in	ADP	E	_	40	case	_	_
40	giro	giro	NOUN	S	Gender=Masc|Number=Sing	38	nmod	_	_
41	loro	loro	DET	AP	Poss=Yes|PronType=Prs	40	det:poss	_	SpaceAfter=No
42	,	,	PUNCT	FF	_	38	punct	_	_
43	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	44	det	_	_
44	bambini	bambino	NOUN	S	Gender=Masc|Number=Plur	38	dobj	_	_
45	di	di	ADP	E	_	46	mark	_	_
46	misero	midere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Past|VerbForm=Fin	44	acl	_	_
47	a	a	ADP	E	_	48	mark	_	_
48	ridere	ridere	VERB	V	VerbForm=Inf	46	xcomp	_	SpaceAfter=No
49	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 7
# text = Suzana si arrabbiò moltissimo.
1	Suzana	Suzana	PROPN	SP	_	3	nsubj	_	_
2	si	si	PRON	PC	Person=3|PronType=Clit	3	expl	_	_
3	arrabbiò	arrabbiare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
4	moltissimo	molto	ADV	B	Degree=Abs	3	advmod	_	SpaceAfter=No
5	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 8
# text = I due bambini sono veramente delle pesti continuavano ad entrare in tutte le stanza, solo cinque stanze erano aperte la sesta l’aprirono con una forcina per per capelli.
1	I	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	3	det	_	_
2	due	due	NUM	N	NumType=Card	3	nummod	_	_
3	bambini	bambino	NOUN	S	Gender=Masc|Number=Plur	8	nsubj	_	_
4	sono	essere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	8	cop	_	_
5	veramente	veramente	ADV	B	_	8	advmod	_	_
6-7	delle	_	_	_	_	_	_	_	_
6	di	di	ADP	E	_	8	case	_	_
7	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	8	det	_	_
8	pesti	pesto	NOUN	S	Gender=Masc|Number=Plur	0	root	_	_
9	continuavano	continuare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	8	acl	_	_
10	ad	a	ADP	E	_	11	mark	_	_
11	entrare	entrare	VERB	V	VerbForm=Inf	9	xcomp	_	_
12	in	in	ADP	E	_	15	case	_	_
13	tutte	tutto	DET	T	Gender=Fem|Number=Plur	15	det:predet	_	_
14	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	15	det	_	_
15	stanza	stanza	NOUN	S	Gender=Fem|Number=Sing	11	nmod	_	SpaceAfter=No
16	,	,	PUNCT	FF	_	11	punct	_	_
17	solo	solo	ADV	B	_	21	advmod	_	_
18	cinque	cinque	NUM	N	NumType=Card	19	nummod	_	_
19	stanze	stanza	NOUN	S	Gender=Fem|Number=Plur	21	nsubjpass	_	_
20	erano	essere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	21	auxpass	_	_
21	aperte	aprire	VERB	V	Gender=Fem|Number=Plur|Tense=Past|VerbForm=Part	11	conj	_	_
22	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	23	det	_	_
23	sesta	sesto	NOUN	S	Gender=Fem|Number=Sing	24	nsubj	_	_
24	l’aprirono	l’aprirono	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Past|VerbForm=Fin	21	advcl	_	_
25	con	con	ADP	E	_	27	case	_	_
26	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	27	det	_	_
27	forcina	forcina	NOUN	S	Gender=Fem|Number=Sing	24	nmod	_	_
28	per	per	ADP	E	_	30	case	_	_
29	per	per	ADP	E	_	30	case	_	_
30	capelli	capello	NOUN	S	Gender=Masc|Number=Plur	27	nmod	_	SpaceAfter=No
31	.	.	PUNCT	FS	_	8	punct	_	_

# sent_id = 9
# text = Solo che non hanno pensato che se chiudono la porta non si apre più e hanno anche rotto la forcina, non potevano più uscire.
1	Solo	solo	ADV	B	_	5	advmod	_	_
2	che	che	PRON	PR	PronType=Rel	5	nsubj	_	_
3	non	non	ADV	BN	PronType=Neg	5	neg	_	_
4	hanno	avere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	pensato	pensare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
6	che	che	SCONJ	CS	_	8	mark	_	_
7	se	se	PRON	PC	Person=3|PronType=Clit	8	expl	_	_
8	chiudono	chiudere	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	13	advcl	_	_
9	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	10	det	_	_
10	porta	porta	NOUN	S	Gender=Fem|Number=Sing	13	nsubj	_	_
11	non	non	ADV	BN	PronType=Neg	13	neg	_	_
12	si	si	PRON	PC	Person=3|PronType=Clit	13	expl	_	_
13	apre	aprire	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	5	ccomp	_	_
14	più	più	ADV	B	_	13	advmod	_	_
15	e	e	CONJ	CC	_	5	cc	_	_
16	hanno	avere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	18	aux	_	_
17	anche	anche	ADV	B	_	18	advmod	_	_
18	rotto	rompere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	5	conj	_	_
19	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	20	det	_	_
20	forcina	forcina	NOUN	S	Gender=Fem|Number=Sing	18	dobj	_	SpaceAfter=No
21	,	,	PUNCT	FF	_	5	punct	_	_
22	non	non	ADV	BN	PronType=Neg	25	neg	_	_
23	potevano	potere	AUX	VM	Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin	25	aux	_	_
24	più	più	ADV	B	_	25	advmod	_	_
25	uscire	uscire	VERB	V	VerbForm=Inf	5	conj	_	SpaceAfter=No
26	.	.	PUNCT	FS	_	5	punct	_	_

# sent_id = 10
# text = Ma poi io stavo andando in bagno per cambiare il pannolino ad Jamila (una bambina di cinque mesi) e ho sentito le loro urla in fondo al corridoio, sono andata a vedere da dove provenivano le urla, mi accorsi che c’erano i due bambini dentro la stanza.
1	Ma	ma	CONJ	CC	_	4	cc	_	_
2	poi	poi	ADV	B	_	4	advmod	_	_
3	io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	4	nsubj	_	_
4	stavo	stavo	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	0	root	_	_
5	andando	andare	VERB	V	VerbForm=Ger	4	advcl	_	_
6	in	in	ADP	E	_	7	case	_	_
7	bagno	bagno	NOUN	S	Gender=Masc|Number=Sing	5	nmod	_	_
8	per	per	ADP	E	_	9	mark	_	_
9	cambiare	cambiare	VERB	V	VerbForm=Inf	5	advcl	_	_
10	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	11	det	_	_
11	pannolino	pannolino	NOUN	S	Gender=Masc|Number=Sing	9	dobj	_	_
12	ad	a	ADP	E	_	13	case	_	_
13	Jamila	Jamila	PROPN	SP	_	9	nmod	_	_
14	(	(	PUNCT	FB	_	16	punct	_	SpaceAfter=No
15	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	16	det	_	_
16	bambina	bambino	NOUN	S	Gender=Fem|Number=Sing	13	nmod	_	_
17	di	di	ADP	E	_	19	case	_	_
18	cinque	cinque	NUM	N	NumType=Card	19	nummod	_	_
19	mesi	mese	NOUN	S	Gender=Masc|Number=Plur	16	nmod	_	SpaceAfter=No
20	)	)	PUNCT	FB	_	16	punct	_	_
21	e	e	CONJ	CC	_	5	cc	_	_
22	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	23	aux	_	_
23	sentito	sentire	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	5	conj	_	_
24	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	26	det	_	_
25	loro	loro	DET	AP	Poss=Yes|PronType=Prs	26	det:poss	_	_
26	urla	urlo	NOUN	S	Gender=Fem|Number=Sing	23	dobj	_	_
27	in	in	ADP	E	_	28	case	_	_
28	fondo	fondo	NOUN	S	Gender=Masc|Number=Sing	23	nmod	_	_
29-30	al	_	_	_	_	_	_	_	_
29	a	a	ADP	E	_	31	case	_	_
30	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	31	det	_	_
31	corridoio	corridoio	NOUN	S	Gender=Masc|Number=Sing	28	nmod	_	SpaceAfter=No
32	,	,	PUNCT	FF	_	4	punct	_	_
33	sono	essere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	34	aux	_	_
34	andata	andare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	4	conj	_	_
35	a	a	ADP	E	_	36	mark	_	_
36	vedere	vedere	VERB	V	VerbForm=Inf	34	xcomp	_	_
37	da	da	ADP	E	_	38	case	_	_
38	dove	dove	ADV	B	_	39	mark	_	_
39	provenivano	provenire	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	36	ccomp	_	_
40	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	41	det	_	_
41	urla	urla	NOUN	S	Gender=Fem|Number=Plur	39	dobj	_	SpaceAfter=No
42	,	,	PUNCT	FF	_	41	punct	_	_
43	mi	mi	NOUN	S	Gender=Masc|Number=Plur	41	appos	_	_
44	accorsi	accorrere	ADJ	A	Gender=Masc|Number=Plur	43	amod	_	_
45	che	che	PRON	PR	PronType=Rel	46	nsubj	_	_
46	c’erano	c’erare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	43	acl:relcl	_	_
47	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	49	det	_	_
48	due	due	NUM	N	NumType=Card	49	nummod	_	_
49	bambini	bambino	NOUN	S	Gender=Masc|Number=Plur	46	dobj	_	_
50	dentro	dentro	ADP	E	_	52	case	_	_
51	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	52	det	_	_
52	stanza	stanza	NOUN	S	Gender=Fem|Number=Sing	46	nmod	_	SpaceAfter=No
53	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 11
# text = Chiamai un signore che lavorava in cucina che per fortuna aveva le chiavi della stanza;
1	Chiamai	chiamare	VERB	V	Mood=Imp|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	0	root	_	_
2	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	3	det	_	_
3	signore	signore	NOUN	S	Gender=Masc|Number=Sing	1	dobj	_	_
4	che	che	PRON	PR	PronType=Rel	5	nsubj	_	_
5	lavorava	lavorare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	3	acl:relcl	_	_
6	in	in	ADP	E	_	7	case	_	_
7	cucina	cucina	NOUN	S	Gender=Fem|Number=Sing	5	nmod	_	_
8	che	che	SCONJ	CS	_	11	mark	_	_
9	per	per	ADP	E	_	10	case	_	_
10	fortuna	fortuna	NOUN	S	Gender=Fem|Number=Sing	11	nmod	_	_
11	aveva	avere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	5	ccomp	_	_
12	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	13	det	_	_
13	chiavi	chiave	NOUN	S	Gender=Fem|Number=Plur	11	dobj	_	_
14-15	della	_	_	_	_	_	_	_	_
14	di	di	ADP	E	_	16	case	_	_
15	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	16	det	_	_
16	stanza	stanza	NOUN	S	Gender=Fem|Number=Sing	13	nmod	_	SpaceAfter=No
17	;	;	PUNCT	FC	_	1	punct	_	_

# sent_id = 12
# text = aprì la porta e i bambini uscirono velocemente dalla stanza senza dire grazie al signore, incontrarono subitò la sposa e gli disse solo:
1	aprì	aprire	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	19	advcl	_	_
2	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	3	det	_	_
3	porta	porta	NOUN	S	Gender=Fem|Number=Sing	1	dobj	_	_
4	e	e	CONJ	CC	_	3	cc	_	_
5	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	6	det	_	_
6	bambini	bambino	NOUN	S	Gender=Masc|Number=Plur	3	conj	_	_
7	uscirono	uscire	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Past|VerbForm=Fin	1	xcomp	_	_
8	velocemente	velocemente	ADV	B	_	7	advmod	_	_
9-10	dalla	_	_	_	_	_	_	_	_
9	da	da	ADP	E	_	11	case	_	_
10	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	11	det	_	_
11	stanza	stanza	NOUN	S	Gender=Fem|Number=Sing	7	nmod	_	_
12	senza	senza	ADP	E	_	13	mark	_	_
13	dire	dire	VERB	V	VerbForm=Inf	7	advcl	_	_
14	grazie	grazie	NOUN	S	_	17	case	_	_
15-16	al	_	_	_	_	_	_	_	_
15	a	a	ADP	E	_	14	mwe	_	_
16	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	17	det	_	_
17	signore	signore	NOUN	S	Gender=Masc|Number=Sing	13	nmod	_	SpaceAfter=No
18	,	,	PUNCT	FF	_	1	punct	_	_
19	incontrarono	incontrare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
20	subitò	subite	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	19	root	_	_
21	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	22	det	_	_
22	sposa	sposo	NOUN	S	Gender=Fem|Number=Sing	20	dobj	_	_
23	e	e	CONJ	CC	_	20	cc	_	_
24	gli	lo	PRON	PC	Gender=Masc|Number=Sing|Person=3|PronType=Clit	25	iobj	_	_
25	disse	dire	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	20	conj	_	_
26	solo	solo	ADV	B	_	25	advmod	_	SpaceAfter=No
27	:	:	PUNCT	FC	_	20	punct	_	_

# sent_id = 13
# text = – Andate a pianiucolare da mammina!
1	–	–	PUNCT	FF	_	2	punct	_	_
2	Andate	andare	VERB	V	Mood=Imp|Number=Plur|Person=2|Tense=Pres|VerbForm=Fin	0	root	_	_
3	a	a	ADP	E	_	4	mark	_	_
4	pianiucolare	pianiucolare	VERB	V	VerbForm=Inf	2	xcomp	_	_
5	da	da	ADP	E	_	6	case	_	_
6	mammina	mammina	NOUN	S	Gender=Fem|Number=Sing	4	nmod	_	SpaceAfter=No
7	!	!	PUNCT	FS	_	2	punct	_	_

# sent_id = 14
# text = – Mai scherzare gli altri sennò finisci di essere scherzato te in un modo peggiore .
1	–	–	PUNCT	FF	_	6	punct	_	_
2	Mai	mai	ADV	B	_	3	advmod	_	_
3	scherzare	scherzare	VERB	V	VerbForm=Inf	6	advcl	_	_
4	gli	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	5	det	_	_
5	altri	altro	PRON	PI	Gender=Masc|Number=Plur|PronType=Ind	3	dobj	_	_
6	sennò	sennò	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
7	finisci	finire	NOUN	S	Gender=Masc	6	dobj	_	_
8	di	di	ADP	E	_	10	mark	_	_
9	essere	essere	AUX	VA	VerbForm=Inf	10	aux	_	_
10	scherzato	scherzare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	7	acl	_	_
11	te	ti	PRON	PC	Number=Sing|Person=2|PronType=Clit	10	dobj	_	_
12	in	in	ADP	E	_	14	case	_	_
13	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	14	det	_	_
14	modo	modo	NOUN	S	Gender=Masc|Number=Sing	10	nmod	_	_
15	peggiore	peggiore	ADJ	A	Degree=Cmp|Number=Sing	14	amod	_	_
16	.	.	PUNCT	FS	_	6	punct	_	SpacesAfter=\r\n\r\n


# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE144.txt
# newpar
# sent_id = 1
# text = Il regalo dimenticato Un giorno, dovevo andare ad un compleanno, subito dopo scuola. Prima di andare a scuola, mia mamma, mi continuava a raccomandare di non perdere il regalo e di non lasciarlo alla fermata del bus. Io, continuavo a dire di sì e che non lo perderò.
1	Il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	regalo	regalo	NOUN	S	Gender=Masc|Number=Sing	8	nsubj	_	_
3	dimenticato	dimenticare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	2	acl	_	SpacesAfter=\s\s
4	Un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	5	det	_	_
5	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	3	nmod	_	SpaceAfter=No
6	,	,	PUNCT	FF	_	2	punct	_	_
7	dovevo	dovere	AUX	VM	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	8	aux	_	_
8	andare	andare	VERB	V	VerbForm=Inf	0	root	_	_
9	ad	a	ADP	E	_	11	case	_	_
10	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	11	det	_	_
11	compleanno	compleanno	NOUN	S	Gender=Masc|Number=Sing	8	nmod	_	SpaceAfter=No
12	,	,	PUNCT	FF	_	8	punct	_	_
13	subito	subito	ADV	B	_	15	advmod	_	_
14	dopo	dopo	ADP	E	_	15	case	_	_
15	scuola	scuola	NOUN	S	Gender=Fem|Number=Sing	8	nmod	_	SpaceAfter=No
16	.	.	PUNCT	FS	_	15	punct	_	_
17	Prima	prima	ADV	B	_	19	mark	_	_
18	di	di	ADP	E	_	17	mwe	_	_
19	andare	andare	VERB	V	VerbForm=Inf	15	advcl	_	_
20	a	a	ADP	E	_	21	case	_	_
21	scuola	scuola	NOUN	S	Gender=Fem|Number=Sing	19	nmod	_	SpaceAfter=No
22	,	,	PUNCT	FF	_	15	punct	_	_
23	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	24	det:poss	_	_
24	mamma	mamma	NOUN	S	Gender=Fem|Number=Sing	15	nmod	_	SpaceAfter=No
25	,	,	PUNCT	FF	_	8	punct	_	_
26	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	27	iobj	_	_
27	continuava	continuare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	8	conj	_	_
28	a	a	ADP	E	_	29	mark	_	_
29	raccomandare	raccomandare	VERB	V	VerbForm=Inf	27	xcomp	_	_
30	di	di	ADP	E	_	32	mark	_	_
31	non	non	ADV	BN	PronType=Neg	32	neg	_	_
32	perdere	perdere	VERB	V	VerbForm=Inf	29	xcomp	_	_
33	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	34	det	_	_
34	regalo	regalo	NOUN	S	Gender=Masc|Number=Sing	32	dobj	_	_
35	e	e	CONJ	CC	_	32	cc	_	_
36	di	di	ADP	E	_	38	mark	_	_
37	non	non	ADV	BN	PronType=Neg	38	neg	_	_
38-39	lasciarlo	_	_	_	_	_	_	_	_
38	lasciar	lasciare	VERB	V	VerbForm=Inf	32	conj	_	_
39	lo	lo	PRON	PC	Gender=Masc|Number=Sing|Person=3|PronType=Clit	38	dobj	_	_
40-41	alla	_	_	_	_	_	_	_	_
40	a	a	ADP	E	_	42	case	_	_
41	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	42	det	_	_
42	fermata	fermata	NOUN	S	Gender=Fem|Number=Sing	48	nmod	_	_
43-44	del	_	_	_	_	_	_	_	_
43	di	di	ADP	E	_	45	case	_	_
44	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	45	det	_	_
45	bus.	bus.	NOUN	S	Gender=Masc|Number=Sing	48	nmod	_	_
46	Io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	48	nsubj	_	SpaceAfter=No
47	,	,	PUNCT	FF	_	46	punct	_	_
48	continuavo	continuare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	38	advcl	_	_
49	a	a	ADP	E	_	50	mark	_	_
50	dire	dire	VERB	V	VerbForm=Inf	48	xcomp	_	_
51	di	di	ADP	E	_	52	case	_	_
52	sì	sì	ADV	B	_	50	advmod	_	_
53	e	e	CONJ	CC	_	50	cc	_	_
54	che	che	SCONJ	CS	_	57	mark	_	_
55	non	non	ADV	BN	PronType=Neg	57	neg	_	_
56	lo	lo	PRON	PC	Gender=Masc|Number=Sing|Person=3|PronType=Clit	57	dobj	_	_
57	perderò	perdere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Fut|VerbForm=Fin	50	conj	_	SpaceAfter=No
58	.	.	PUNCT	FS	_	48	punct	_	_

# sent_id = 2
# text = Invece arrivata alla fermata del bus, l’avevo appoggiato (il regalo) accanto a dei sassi.
1	Invece	invece	ADV	B	_	2	advmod	_	_
2	arrivata	arrivare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
3-4	alla	_	_	_	_	_	_	_	_
3	a	a	ADP	E	_	5	case	_	_
4	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	5	det	_	_
5	fermata	fermata	NOUN	S	Gender=Fem|Number=Sing	2	nmod	_	_
6-7	del	_	_	_	_	_	_	_	_
6	di	di	ADP	E	_	8	case	_	_
7	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	8	det	_	_
8	bus	bus	NOUN	S	Gender=Masc	5	nmod	_	SpaceAfter=No
9	,	,	PUNCT	FF	_	2	punct	_	_
10	l’avevo	l’avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	11	aux	_	_
11	appoggiato	appoggiare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	2	conj	_	_
12	(	(	PUNCT	FB	_	14	punct	_	SpaceAfter=No
13	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	14	det	_	_
14	regalo	regalo	NOUN	S	Gender=Masc|Number=Sing	11	nmod	_	SpaceAfter=No
15	)	)	PUNCT	FB	_	14	punct	_	_
16	accanto	accanto	ADV	B	_	11	advmod	_	_
17	a	a	ADP	E	_	20	case	_	SpacesAfter=\s\s
18-19	dei	_	_	_	_	_	_	_	_
18	di	di	ADP	E	_	17	mwe	_	_
19	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	20	det	_	_
20	sassi	sasso	NOUN	S	Gender=Masc|Number=Plur	11	nmod	_	SpaceAfter=No
21	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 3
# text = Poi quando era arrivato il bus, stavo parlando con l’Enea, ed ero salita senza il regalo.
1	Poi	poi	ADV	B	_	4	advmod	_	_
2	quando	quando	SCONJ	CS	_	4	mark	_	_
3	era	essere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	4	aux	_	_
4	arrivato	arrivare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
5	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	6	det	_	_
6	bus	bus	NOUN	S	Gender=Masc|Number=Sing	4	nsubj	_	SpaceAfter=No
7	,	,	PUNCT	FF	_	6	punct	_	_
8	stavo	stare	NOUN	S	Gender=Masc|Number=Sing	6	appos	_	_
9	parlando	parlare	VERB	V	VerbForm=Ger	8	acl	_	_
10	con	con	ADP	E	_	11	case	_	_
11	l’Enea	l’Enea	PROPN	SP	_	9	nmod	_	SpaceAfter=No
12	,	,	PUNCT	FF	_	9	punct	_	_
13	ed	e	CONJ	CC	_	9	cc	_	_
14	ero	essere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	15	aux	_	_
15	salita	salire	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	9	conj	_	_
16	senza	senza	ADP	E	_	18	case	_	_
17	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	18	det	_	_
18	regalo	regalo	NOUN	S	Gender=Masc|Number=Sing	15	nmod	_	SpaceAfter=No
19	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 4
# text = Arrivata a scuola, dopo un po’, mi ero accorta che il regalo l’avevo lasciato alla fermata.
1	Arrivata	arrivare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	10	advcl	_	_
2	a	a	ADP	E	_	3	case	_	_
3	scuola	scuola	NOUN	S	Gender=Fem|Number=Sing	1	nmod	_	SpaceAfter=No
4	,	,	PUNCT	FF	_	1	punct	_	_
5	dopo	dopo	ADP	E	_	7	case	_	_
6	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	7	det	_	_
7	po’,	po’,	X	X	_	10	nmod	_	_
8	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	10	iobj	_	_
9	ero	essere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	10	aux	_	_
10	accorta	accorgere	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
11	che	che	SCONJ	CS	_	15	mark	_	_
12	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	13	det	_	_
13	regalo	regalo	NOUN	S	Gender=Masc|Number=Sing	15	nsubj	_	_
14	l’avevo	l’avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	15	aux	_	_
15	lasciato	lasciare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	10	ccomp	_	_
16-17	alla	_	_	_	_	_	_	_	_
16	a	a	ADP	E	_	18	case	_	_
17	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	18	det	_	_
18	fermata	fermata	NOUN	S	Gender=Fem|Number=Sing	15	nmod	_	SpaceAfter=No
19	.	.	PUNCT	FS	_	10	punct	_	_

# sent_id = 5
# text = Allora penzai di telefonare a mia mamma, ma poi mi ricordai che mia mamma era andata a fare la suplente a Tesserete.
1	Allora	allora	ADV	B	_	2	advmod	_	_
2	penzai	penzai	VERB	V	Mood=Imp|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	0	root	_	_
3	di	di	ADP	E	_	4	mark	_	_
4	telefonare	telefonare	VERB	V	VerbForm=Inf	2	xcomp	_	_
5	a	a	ADP	E	_	7	case	_	_
6	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	7	det:poss	_	_
7	mamma	mamma	NOUN	S	Gender=Fem|Number=Sing	4	nmod	_	SpaceAfter=No
8	,	,	PUNCT	FF	_	2	punct	_	_
9	ma	ma	CONJ	CC	_	2	cc	_	_
10	poi	poi	ADV	B	_	12	advmod	_	_
11	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	12	expl	_	_
12	ricordai	ricordare	VERB	V	Mood=Ind|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	2	conj	_	_
13	che	che	SCONJ	CS	_	17	mark	_	_
14	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	15	det:poss	_	_
15	mamma	mamma	NOUN	S	Gender=Fem|Number=Sing	17	nsubj	_	_
16	era	essere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	17	aux	_	_
17	andata	andare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	12	ccomp	_	_
18	a	a	ADP	E	_	19	mark	_	_
19	fare	fare	VERB	V	VerbForm=Inf	17	xcomp	_	_
20	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	21	det	_	_
21	suplente	suplente	NOUN	S	Gender=Fem|Number=Sing	19	dobj	_	_
22	a	a	ADP	E	_	23	case	_	_
23	Tesserete	Tesserete	PROPN	SP	_	19	nmod	_	SpaceAfter=No
24	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 6
# text = Ma poi mi ricordai anche che mio fratello, Siro, aveva ora buca a scuola ed era a casa, cosí decisi di telefonargli a casa.
1	Ma	ma	CONJ	CC	_	4	cc	_	_
2	poi	poi	ADV	B	_	4	advmod	_	_
3	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	4	expl	_	_
4	ricordai	ricordare	VERB	V	Mood=Ind|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin	0	root	_	_
5	anche	anche	ADV	B	_	12	advmod	_	_
6	che	che	SCONJ	CS	_	12	mark	_	_
7	mio	mio	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	8	det:poss	_	_
8	fratello	fratello	NOUN	S	Gender=Masc|Number=Sing	12	nsubj	_	SpaceAfter=No
9	,	,	PUNCT	FF	_	8	punct	_	_
10	Siro	Siro	PROPN	SP	_	8	nmod	_	SpaceAfter=No
11	,	,	PUNCT	FF	_	8	punct	_	_
12	aveva	avere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	4	advcl	_	_
13	ora	ora	ADV	B	_	14	advmod	_	_
14	buca	burre	ADJ	A	Gender=Fem|Number=Sing	12	dobj	_	_
15	a	a	ADP	E	_	16	case	_	_
16	scuola	scuola	NOUN	S	Gender=Fem|Number=Sing	14	nmod	_	_
17	ed	e	CONJ	CC	_	4	cc	_	_
18	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	4	conj	_	_
19	a	a	ADP	E	_	20	case	_	_
20	casa	casa	NOUN	S	Gender=Fem|Number=Sing	18	nmod	_	SpaceAfter=No
21	,	,	PUNCT	FF	_	18	punct	_	_
22	cosí	cosí	PROPN	SP	_	18	nsubj	_	_
23	decisi	deciso	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	22	acl	_	_
24	di	di	ADP	E	_	25	case	_	_
25	telefonargli	telefonarglio	NOUN	S	Gender=Masc|Number=Plur	23	nmod	_	_
26	a	a	ADP	E	_	27	case	_	_
27	casa	casa	NOUN	S	Gender=Fem|Number=Sing	25	nmod	_	SpaceAfter=No
28	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 7
# text = Per fortuna che il Siro era ancora a casa, così gli chiesi se poteva portarmi il regalo a scuola.
1	Per	per	ADP	E	_	2	case	_	_
2	fortuna	fortuna	NOUN	S	Gender=Fem|Number=Sing	0	root	_	_
3	che	che	SCONJ	CS	_	6	mark	_	_
4	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	5	det	_	_
5	Siro	Siro	PROPN	SP	_	6	nsubj	_	_
6	era	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	2	advcl	_	_
7	ancora	ancora	ADV	B	_	6	advmod	_	_
8	a	a	ADP	E	_	9	case	_	_
9	casa	casa	NOUN	S	Gender=Fem|Number=Sing	6	nmod	_	SpaceAfter=No
10	,	,	PUNCT	FF	_	6	punct	_	_
11	così	così	ADV	B	_	13	advmod	_	_
12	gli	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	13	det	_	_
13	chiesi	chiesi	NOUN	S	Gender=Masc|Number=Plur	6	nmod	_	_
14	se	se	SCONJ	CS	_	16	mark	_	_
15	poteva	potere	AUX	VM	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	16	aux	_	_
16-17	portarmi	_	_	_	_	_	_	_	_
16	portar	portare	VERB	V	VerbForm=Inf	13	advcl	_	_
17	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	16	expl	_	_
18	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	19	det	_	_
19	regalo	regalo	NOUN	S	Gender=Masc|Number=Sing	16	dobj	_	_
20	a	a	ADP	E	_	21	case	_	_
21	scuola	scuola	NOUN	S	Gender=Fem|Number=Sing	16	nmod	_	SpaceAfter=No
22	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 8
# text = Poco dopo Siro arrivò (a scuola) e mi diede il regalo.
1	Poco	poco	ADV	B	_	3	advmod	_	_
2	dopo	dopo	ADP	E	_	3	case	_	_
3	Siro	Siro	PROPN	SP	_	4	nmod	_	_
4	arrivò	arrivare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	0	root	_	_
5	(	(	PUNCT	FB	_	7	punct	_	SpaceAfter=No
6	a	a	ADP	E	_	7	case	_	_
7	scuola	scuola	NOUN	S	Gender=Fem|Number=Sing	4	nmod	_	SpaceAfter=No
8	)	)	PUNCT	FB	_	7	punct	_	_
9	e	e	CONJ	CC	_	4	cc	_	_
10	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	11	iobj	_	_
11	diede	dare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	4	conj	_	_
12	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	13	det	_	_
13	regalo	regalo	NOUN	S	Gender=Masc|Number=Sing	11	dobj	_	SpaceAfter=No
14	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 9
# text = Questo episdio mi ha insegnato a guardare sempre se ho presto tutto da casa, per esempio i compiti o dei fogli da consegnare ai maestri, prima di salire sul bus.
1	Questo	questo	DET	DD	Gender=Masc|Number=Sing|PronType=Dem	2	det	_	_
2	episdio	episdio	NOUN	S	Gender=Masc|Number=Sing	5	nsubj	_	_
3	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	5	dobj	_	_
4	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	insegnato	insegnare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
6	a	a	ADP	E	_	7	mark	_	_
7	guardare	guardare	VERB	V	VerbForm=Inf	5	xcomp	_	_
8	sempre	sempre	ADV	B	_	7	advmod	_	_
9	se	se	SCONJ	CS	_	10	mark	_	_
10	ho	avere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	7	advcl	_	_
11	presto	presto	ADV	B	_	12	advmod	_	_
12	tutto	tutto	PRON	PI	Gender=Masc|Number=Sing|PronType=Ind	10	dobj	_	_
13	da	da	ADP	E	_	14	case	_	_
14	casa	casa	NOUN	S	Gender=Fem|Number=Sing	12	nmod	_	SpaceAfter=No
15	,	,	PUNCT	FF	_	7	punct	_	_
16	per	per	ADP	E	_	17	case	_	_
17	esempio	esempio	NOUN	S	Gender=Masc|Number=Sing	7	nmod	_	_
18	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	19	det	_	_
19	compiti	compito	NOUN	S	Gender=Masc|Number=Plur	7	dobj	_	_
20	o	o	CONJ	CC	_	19	cc	_	_
21-22	dei	_	_	_	_	_	_	_	_
21	di	di	ADP	E	_	23	case	_	_
22	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	23	det	_	_
23	fogli	foglio	NOUN	S	Gender=Masc|Number=Plur	19	conj	_	_
24	da	da	ADP	E	_	25	mark	_	_
25	consegnare	consegnare	VERB	V	VerbForm=Inf	7	advcl	_	_
26-27	ai	_	_	_	_	_	_	_	_
26	a	a	ADP	E	_	28	case	_	_
27	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	28	det	_	_
28	maestri	maestro	NOUN	S	Gender=Masc|Number=Plur	25	nmod	_	SpaceAfter=No
29	,	,	PUNCT	FF	_	25	punct	_	_
30	prima	prima	ADV	B	_	32	mark	_	_
31	di	di	ADP	E	_	30	mwe	_	_
32	salire	salire	VERB	V	VerbForm=Inf	25	advcl	_	_
33-34	sul	_	_	_	_	_	_	_	_
33	su	su	ADP	E	_	35	case	_	_
34	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	35	det	_	_
35	bus	bus	NOUN	S	Gender=Masc	32	nmod	_	SpaceAfter=No
36	.	.	PUNCT	FS	_	5	punct	_	SpacesAfter=\r\n\r\n


# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE37.txt
# newpar
# sent_id = 1
# text = L’occhio A me è successo che inzigando un compagno ho ricevuto un sasso nell’occhio.
1	L’occhio	L’occhio	NOUN	S	Gender=Masc|Number=Sing	5	nsubj	_	SpacesAfter=\s\s
2	A	a	ADP	E	_	3	case	_	_
3	me	me	PRON	PE	Number=Sing|Person=1|PronType=Prs	1	nmod	_	_
4	è	essere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	successo	successo	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
6	che	che	SCONJ	CS	_	11	mark	_	_
7	inzigando	inzigare	VERB	V	VerbForm=Ger	11	advcl	_	_
8	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	9	det	_	_
9	compagno	compagno	NOUN	S	Gender=Masc|Number=Sing	7	dobj	_	_
10	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	11	aux	_	_
11	ricevuto	ricevere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	5	ccomp	_	_
12	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	14	det	_	_
13	sasso	sasso	ADJ	A	Gender=Masc|Number=Sing	14	amod	_	_
14	nell’occhio	nell’occhio	NOUN	S	Gender=Masc|Number=Sing	11	dobj	_	SpaceAfter=No
15	.	.	PUNCT	FS	_	5	punct	_	_

# sent_id = 2
# text = È successo a ricreazione stavamo litigando sembrava una litigata da niente ma il “Quan” l’ha presa male poi ci siamo insultati per 5 minuti e poi il “Quan” ha preso un sasso in mano e l’ha lanciato contro di me, dopo mezz’ora sono andato dall’oculista a farmi un controllo così mi ha dato delle gocce di atropina che dilatano la pupilla e dell’anestetico per il dolore.
1	È	essere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	2	aux	_	_
2	successo	successo	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
3	a	a	ADP	E	_	4	case	_	_
4	ricreazione	ricreazione	NOUN	S	Gender=Fem|Number=Sing	2	nmod	_	_
5	stavamo	stare	AUX	VM	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	6	aux	_	_
6	litigando	litigare	VERB	V	VerbForm=Ger	2	advcl	_	_
7	sembrava	sembrare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	6	advcl	_	_
8	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	9	det	_	_
9	litigata	litigata	NOUN	S	Gender=Fem|Number=Sing	7	dobj	_	_
10	da	da	ADP	E	_	11	case	_	_
11	niente	niente	PRON	PI	Gender=Masc|Number=Sing|PronType=Ind	7	nmod	_	_
12	ma	ma	CONJ	CC	_	11	cc	_	_
13	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	15	det	_	_
14	“	“	PUNCT	FB	_	15	punct	_	SpaceAfter=No
15	Quan	Quan	PROPN	SP	_	11	conj	_	SpaceAfter=No
16	”	”	PUNCT	FB	_	18	punct	_	_
17	l’ha	l’avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	18	aux	_	_
18	presa	prendere	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	7	advcl	_	_
19	male	male	ADV	B	_	23	advmod	_	_
20	poi	poi	ADV	B	_	23	advmod	_	_
21	ci	ci	PRON	PC	Number=Plur|Person=1|PronType=Clit	23	expl	_	_
22	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	23	aux	_	_
23	insultati	insultare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	18	advcl	_	_
24	per	per	ADP	E	_	26	case	_	_
25	5	5	NUM	N	NumType=Card	26	nummod	_	_
26	minuti	minuto	NOUN	S	Gender=Masc|Number=Plur	23	nmod	_	_
27	e	e	CONJ	CC	_	26	cc	_	_
28	poi	poi	ADV	B	_	34	advmod	_	_
29	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	31	det	_	_
30	“	“	PUNCT	FB	_	31	punct	_	SpaceAfter=No
31	Quan	Quan	PROPN	SP	_	34	nsubj	_	SpaceAfter=No
32	”	”	PUNCT	FB	_	34	punct	_	_
33	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	34	aux	_	_
34	preso	prendere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	26	conj	_	_
35	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	36	det	_	_
36	sasso	sasso	NOUN	S	Gender=Masc|Number=Sing	34	dobj	_	_
37	in	in	ADP	E	_	38	case	_	_
38	mano	mano	NOUN	S	Gender=Fem|Number=Sing	36	nmod	_	_
39	e	e	CONJ	CC	_	23	cc	_	_
40	l’ha	l’avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	41	aux	_	_
41	lanciato	lanciare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	23	conj	_	_
42	contro	contro	ADP	E	_	44	case	_	_
43	di	di	ADP	E	_	42	mwe	_	_
44	me	me	PRON	PE	Number=Sing|Person=1|PronType=Prs	41	nmod	_	SpaceAfter=No
45	,	,	PUNCT	FF	_	41	punct	_	_
46	dopo	dopo	ADP	E	_	47	case	_	_
47	mezz’ora	mezz’ora	NOUN	S	Gender=Fem|Number=Sing	41	nmod	_	_
48	sono	essere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	49	aux	_	_
49	andato	andare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	18	nsubj	_	_
50	dall’oculista	dall’oculista	NOUN	S	Number=Sing	49	xcomp	_	_
51	a	a	ADP	E	_	52	case	_	_
52	farmi	arma|farme	NOUN	S	Gender=Masc|Number=Plur	50	nmod	_	_
53	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	54	det	_	_
54	controllo	controllo	NOUN	S	Gender=Masc|Number=Sing	49	nsubj	_	_
55	così	così	ADV	B	_	58	advmod	_	_
56	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	58	dobj	_	_
57	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	58	aux	_	_
58	dato	dare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	54	acl	_	_
59-60	delle	_	_	_	_	_	_	_	_
59	di	di	ADP	E	_	61	case	_	_
60	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	61	det	_	_
61	gocce	goccia	NOUN	S	Gender=Fem|Number=Plur	58	nmod	_	_
62	di	di	ADP	E	_	63	case	_	_
63	atropina	atropina	NOUN	S	Gender=Fem|Number=Sing	61	nmod	_	_
64	che	che	PRON	PR	PronType=Rel	65	nsubj	_	_
65	dilatano	dilatare	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	54	acl:relcl	_	_
66	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	67	det	_	_
67	pupilla	pupilla	NOUN	S	Gender=Fem|Number=Sing	65	dobj	_	_
68	e	e	CONJ	CC	_	67	cc	_	_
69	dell’anestetico	dell’anestetico	ADJ	A	Gender=Masc|Number=Sing	67	conj	_	_
70	per	per	ADP	E	_	72	case	_	_
71	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	72	det	_	_
72	dolore	dolore	NOUN	S	Gender=Masc|Number=Sing	69	nmod	_	SpaceAfter=No
73	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 3
# text = Il giorno dopo il Mauro ha fatto un riassunto di quello che è successo.
1	Il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	7	nsubj	_	_
3	dopo	dopo	ADP	E	_	5	case	_	_
4	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	5	det	_	_
5	Mauro	Mauro	PROPN	SP	_	7	nmod	_	_
6	ha	avere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	7	aux	_	_
7	fatto	fare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
8	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	9	det	_	_
9	riassunto	riassunto	NOUN	S	Gender=Masc|Number=Sing	7	dobj	_	_
10	di	di	ADP	E	_	11	case	_	_
11	quello	quello	PRON	PD	Gender=Masc|Number=Sing|PronType=Dem	9	nmod	_	_
12	che	che	PRON	PR	PronType=Rel	14	nsubj	_	_
13	è	essere	AUX	VA	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	14	aux	_	_
14	successo	succedere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	11	acl:relcl	_	SpaceAfter=No
15	.	.	PUNCT	FS	_	7	punct	_	SpacesAfter=\s\s

# sent_id = 4
# text = Sono andato a fare altri controlli.
1	Sono	essere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	2	aux	_	_
2	andato	andare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
3	a	a	ADP	E	_	4	mark	_	_
4	fare	fare	VERB	V	VerbForm=Inf	2	xcomp	_	_
5	altri	altro	ADJ	A	Gender=Masc|Number=Plur	6	amod	_	_
6	controlli	controllo	NOUN	S	Gender=Masc|Number=Plur	4	dobj	_	SpaceAfter=No
7	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 5
# text = La cosa che ho imparato è di non inzigare gli altri così siamo tutti più tranquilli.
1	La	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	2	det	_	_
2	cosa	cosa	NOUN	S	Gender=Fem|Number=Sing	6	nsubj	_	_
3	che	che	PRON	PR	PronType=Rel	5	dobj	_	_
4	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	5	aux	_	_
5	imparato	imparare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	2	acl:relcl	_	_
6	è	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
7	di	di	ADP	E	_	9	mark	_	_
8	non	non	ADV	BN	PronType=Neg	9	neg	_	_
9	inzigare	inzigare	VERB	V	VerbForm=Inf	6	ccomp	_	_
10	gli	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	11	det	_	_
11	altri	altro	PRON	PI	Gender=Masc|Number=Plur|PronType=Ind	9	dobj	_	_
12	così	così	ADV	B	_	13	advmod	_	SpacesAfter=\s\s\s
13	siamo	essere	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	9	advcl	_	_
14	tutti	tutto	PRON	PI	Gender=Masc|Number=Plur|PronType=Ind	13	nsubj	_	_
15	più	più	ADV	B	_	16	advmod	_	_
16	tranquilli	tranquillo	ADJ	A	Gender=Masc|Number=Plur	14	amod	_	SpaceAfter=No
17	.	.	PUNCT	FS	_	6	punct	_	_

# sent_id = 6
# text = E la seconda cosa è che bisogna essere responsabili.
1	E	e	CONJ	CC	_	5	cc	_	_
2	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	4	det	_	_
3	seconda	secondo	ADJ	NO	Gender=Fem|Number=Sing|NumType=Ord	4	amod	_	_
4	cosa	cosa	NOUN	S	Gender=Fem|Number=Sing	5	nsubj	_	_
5	è	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
6	che	che	SCONJ	CS	_	7	mark	_	_
7	bisogna	bisognare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	5	ccomp	_	_
8	essere	essere	VERB	V	VerbForm=Inf	9	cop	_	_
9	responsabili	responsabile	ADJ	A	Number=Plur	7	xcomp	_	SpaceAfter=No
10	.	.	PUNCT	FS	_	5	punct	_	_

# sent_id = 7
# text = Se avessi fatto quello che mi dicono i maestri non sarebbe mai capitato.
1	Se	se	SCONJ	CS	_	3	mark	_	_
2	avessi	avere	AUX	VA	Mood=Sub|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	3	aux	_	_
3	fatto	fare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	13	advcl	_	_
4	quello	quello	PRON	PD	Gender=Masc|Number=Sing|PronType=Dem	3	dobj	_	_
5	che	che	PRON	PR	PronType=Rel	7	nsubj	_	_
6	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	7	iobj	_	_
7	dicono	dire	VERB	V	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	4	acl:relcl	_	_
8	i	il	DET	RD	Definite=Def|Gender=Masc|Number=Plur|PronType=Art	9	det	_	_
9	maestri	maestro	NOUN	S	Gender=Masc|Number=Plur	7	dobj	_	_
10	non	non	ADV	BN	PronType=Neg	13	neg	_	_
11	sarebbe	essere	AUX	VA	Mood=Cnd|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	13	aux	_	_
12	mai	mai	ADV	B	_	13	advmod	_	_
13	capitato	capitare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	SpaceAfter=No
14	.	.	PUNCT	FS	_	13	punct	_	SpacesAfter=\r\n\r\n


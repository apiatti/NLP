# newdoc id = /Users/aris/Documents/Scuola-.-/SUPSI/I3A/Progetto_Bachelor/Tools/Inputs/150_testi_5SE88.txt
# newpar
# sent_id = 1
# text = Un giorno io e la mia migliore amica siamo andati in un bosco.
1	Un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	2	det	_	_
2	giorno	giorno	NOUN	S	Gender=Masc|Number=Sing	10	nmod	_	_
3	io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	10	nsubj	_	_
4	e	e	CONJ	CC	_	3	cc	_	_
5	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	8	det	_	_
6	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	8	det:poss	_	_
7	migliore	migliore	ADJ	A	Degree=Cmp|Number=Sing	8	amod	_	_
8	amica	amico	NOUN	S	Gender=Fem|Number=Sing	3	conj	_	_
9	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	10	aux	_	_
10	andati	andare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	0	root	_	_
11	in	in	ADP	E	_	13	case	_	_
12	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	13	det	_	_
13	bosco	bosco	NOUN	S	Gender=Masc|Number=Sing	10	nmod	_	SpaceAfter=No
14	.	.	PUNCT	FS	_	10	punct	_	_

# sent_id = 2
# text = Io in mano avevo una cosa molto importante poi lo appoggiato in un scavo e sono andata a vedere se c’éra una strada per andare nel fiume.
1	Io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	4	nsubj	_	_
2	in	in	ADP	E	_	3	case	_	_
3	mano	mano	NOUN	S	Gender=Fem|Number=Sing	4	nmod	_	_
4	avevo	avere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	0	root	_	_
5	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	6	det	_	_
6	cosa	cosa	NOUN	S	Gender=Fem|Number=Sing	4	dobj	_	_
7	molto	molto	ADV	B	_	8	advmod	_	_
8	importante	importante	ADJ	A	Number=Sing	6	amod	_	_
9	poi	poi	ADV	B	_	4	advmod	_	_
10	lo	lo	PRON	PC	Gender=Masc|Number=Sing|Person=3|PronType=Clit	11	dobj	_	_
11	appoggiato	appoggiare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	4	xcomp	_	_
12	in	in	ADP	E	_	14	case	_	_
13	un	uno	DET	RI	Definite=Ind|Gender=Masc|Number=Sing|PronType=Art	14	det	_	_
14	scavo	scavo	ADJ	A	Gender=Masc|Number=Sing	11	nmod	_	_
15	e	e	CONJ	CC	_	4	cc	_	_
16	sono	essere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	17	aux	_	_
17	andata	andare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	4	conj	_	_
18	a	a	ADP	E	_	19	mark	_	_
19	vedere	vedere	VERB	V	VerbForm=Inf	17	xcomp	_	_
20	se	se	SCONJ	CS	_	21	mark	_	_
21	c’éra	c’érare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	17	advcl	_	_
22	una	uno	DET	RI	Definite=Ind|Gender=Fem|Number=Sing|PronType=Art	23	det	_	_
23	strada	strada	NOUN	S	Gender=Fem|Number=Sing	21	dobj	_	_
24	per	per	ADP	E	_	25	mark	_	_
25	andare	andare	VERB	V	VerbForm=Inf	21	advcl	_	_
26-27	nel	_	_	_	_	_	_	_	_
26	in	in	ADP	E	_	28	case	_	_
27	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	28	det	_	_
28	fiume	fiume	NOUN	S	Gender=Masc|Number=Sing	25	nmod	_	SpaceAfter=No
29	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 3
# text = Quando sono tornata non c’éra più siamo andati per il sentiero quatro o cinque volte ma non lo trovavo.
1	Quando	quando	SCONJ	CS	_	3	advmod	_	_
2	sono	essere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	3	aux	_	_
3	tornata	tornare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
4	non	non	ADV	BN	PronType=Neg	8	neg	_	_
5	c’éra	c’érare	ADV	B	_	8	advmod	_	_
6	più	più	ADV	B	_	8	advmod	_	_
7	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	8	aux	_	_
8	andati	andare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	3	xcomp	_	_
9	per	per	ADP	E	_	11	case	_	_
10	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	11	det	_	_
11	sentiero	sentiero	NOUN	S	Gender=Masc|Number=Sing	8	nmod	_	_
12	quatro	quatro	DET	AP	Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs	11	det:poss	_	_
13	o	o	CONJ	CC	_	12	cc	_	_
14	cinque	cinque	NUM	N	NumType=Card	15	nummod	_	_
15	volte	volta	NOUN	S	Gender=Fem|Number=Plur	12	conj	_	_
16	ma	ma	CONJ	CC	_	3	cc	_	_
17	non	non	ADV	BN	PronType=Neg	19	neg	_	_
18	lo	lo	PRON	PC	Gender=Masc|Number=Sing|Person=3|PronType=Clit	19	dobj	_	_
19	trovavo	trovare	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	3	conj	_	SpaceAfter=No
20	.	.	PUNCT	FS	_	3	punct	_	_

# sent_id = 4
# text = Poi mi sono fermata e la mia migliore amica mi consolava mai io ho perso qualcosa di importante.
1	Poi	poi	ADV	B	_	4	advmod	_	_
2	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	4	expl	_	_
3	sono	essere	AUX	VA	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	4	aux	_	_
4	fermata	fermare	VERB	V	Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part	0	root	_	_
5	e	e	CONJ	CC	_	4	cc	_	_
6	la	il	DET	RD	Definite=Def|Gender=Fem|Number=Sing|PronType=Art	9	det	_	_
7	mia	mio	DET	AP	Gender=Fem|Number=Sing|Poss=Yes|PronType=Prs	9	det:poss	_	_
8	migliore	migliore	ADJ	A	Degree=Cmp|Number=Sing	9	amod	_	_
9	amica	amico	NOUN	S	Gender=Fem|Number=Sing	11	nsubj	_	_
10	mi	mi	PRON	PC	Number=Sing|Person=1|PronType=Clit	11	dobj	_	_
11	consolava	consolare	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin	4	conj	_	_
12	mai	mai	ADV	B	_	15	advmod	_	_
13	io	io	PRON	PE	Number=Sing|Person=1|PronType=Prs	15	nsubj	_	_
14	ho	avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	15	aux	_	_
15	perso	perdere	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	11	advcl	_	_
16	qualcosa	qualcosa	PRON	PI	Number=Sing|PronType=Ind	15	dobj	_	_
17	di	di	ADP	E	_	18	case	_	_
18	importante	importante	ADJ	A	Number=Sing	16	nmod	_	SpaceAfter=No
19	.	.	PUNCT	FS	_	4	punct	_	_

# sent_id = 5
# text = Poi ricomiciammo a cercare, ma niente, quando stavamo per andare a casa che siamo passati nel posto in cui lo perso l’ho ritrovato ero così felice che appena eravamo a casa lo misi al sicuro, e non lo feci più uscire con mè lontano.
1	Poi	poi	ADV	B	_	2	advmod	_	SpacesAfter=\s\s\s
2	ricomiciammo	ricomiciare	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Past|VerbForm=Fin	0	root	_	_
3	a	a	ADP	E	_	4	mark	_	_
4	cercare	cercare	VERB	V	VerbForm=Inf	2	xcomp	_	SpaceAfter=No
5	,	,	PUNCT	FF	_	2	punct	_	_
6	ma	ma	CONJ	CC	_	2	cc	_	_
7	niente	niente	PRON	PI	Gender=Masc|Number=Sing|PronType=Ind	2	conj	_	SpaceAfter=No
8	,	,	PUNCT	FF	_	7	punct	_	_
9	quando	quando	SCONJ	CS	_	10	mark	_	_
10	stavamo	stare	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	7	advcl	_	_
11	per	per	ADP	E	_	12	mark	_	_
12	andare	andare	VERB	V	VerbForm=Inf	10	xcomp	_	_
13	a	a	ADP	E	_	14	case	_	_
14	casa	casa	NOUN	S	Gender=Fem|Number=Sing	12	nmod	_	_
15	che	che	PRON	PR	PronType=Rel	17	dobj	_	_
16	siamo	essere	AUX	VA	Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	17	aux	_	_
17	passati	passare	VERB	V	Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part	14	acl:relcl	_	_
18-19	nel	_	_	_	_	_	_	_	_
18	in	in	ADP	E	_	20	case	_	_
19	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	20	det	_	_
20	posto	posto	NOUN	S	Gender=Masc|Number=Sing	17	nmod	_	_
21	in	in	ADP	E	_	22	case	_	_
22	cui	cui	PRON	PR	PronType=Rel	26	nmod	_	_
23	lo	lo	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	24	det	_	_
24	perso	perdere	NOUN	S	Gender=Masc|Number=Sing	26	nsubj	_	_
25	l’ho	l’avere	AUX	VA	Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin	26	aux	_	_
26	ritrovato	ritrovare	VERB	V	Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part	17	acl:relcl	_	_
27	ero	essere	VERB	V	Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin	29	cop	_	_
28	così	così	ADV	B	_	29	advmod	_	_
29	felice	felice	ADJ	A	Number=Sing	26	xcomp	_	_
30	che	che	SCONJ	CS	_	32	mark	_	_
31	appena	appena	ADV	B	_	32	advmod	_	_
32	eravamo	essere	VERB	V	Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin	36	advcl	_	_
33	a	a	ADP	E	_	34	case	_	_
34	casa	casa	NOUN	S	Gender=Fem|Number=Sing	32	nmod	_	_
35	lo	il	PRON	PC	Gender=Masc|Number=Sing|Person=3|PronType=Clit	36	dobj	_	_
36	misi	misi	VERB	V	Mood=Sub|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	29	csubj	_	_
37-38	al	_	_	_	_	_	_	_	_
37	a	a	ADP	E	_	39	case	_	_
38	il	il	DET	RD	Definite=Def|Gender=Masc|Number=Sing|PronType=Art	39	det	_	_
39	sicuro	sicuro	ADJ	A	Gender=Masc|Number=Sing	36	nmod	_	SpaceAfter=No
40	,	,	PUNCT	FF	_	36	punct	_	_
41	e	e	CONJ	CC	_	36	cc	_	_
42	non	non	ADV	BN	PronType=Neg	46	neg	_	_
43	lo	il	PRON	PC	Gender=Masc|Number=Sing|Person=3|PronType=Clit	46	dobj	_	_
44	feci	feco	ADJ	A	Gender=Masc|Number=Plur	43	amod	_	_
45	più	più	ADV	B	_	46	advmod	_	_
46	uscire	uscire	VERB	V	VerbForm=Inf	36	conj	_	_
47	con	con	ADP	E	_	48	case	_	_
48	mè	mè	NOUN	S	Gender=Masc	46	nmod	_	_
49	lontano	lontano	ADJ	A	Gender=Masc|Number=Sing	48	amod	_	SpaceAfter=No
50	.	.	PUNCT	FS	_	2	punct	_	_

# sent_id = 6
# text = L’insegnamento è non lasciare le cose importanti in giro.
1	L’insegnamento	L’insegnamento	NOUN	S	Gender=Masc|Number=Sing	4	nsubj	_	_
2	è	essere	VERB	V	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	4	aux	_	_
3	non	non	ADV	BN	PronType=Neg	4	neg	_	_
4	lasciare	lasciare	VERB	V	VerbForm=Inf	0	root	_	_
5	le	il	DET	RD	Definite=Def|Gender=Fem|Number=Plur|PronType=Art	6	det	_	_
6	cose	cosa	NOUN	S	Gender=Fem|Number=Plur	4	dobj	_	_
7	importanti	importante	ADJ	A	Number=Plur	6	amod	_	_
8	in	in	ADP	E	_	9	case	_	_
9	giro	giro	NOUN	S	Gender=Masc|Number=Sing	4	nmod	_	SpaceAfter=No
10	.	.	PUNCT	FS	_	4	punct	_	SpacesAfter=\r\n\r\n


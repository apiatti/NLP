1	Un	un	_	RI	_	2	det	_	_
2	giorno	giorno	_	S	_	0	root	_	_
3	mentre	mentre	_	CS	_	4	mark	_	_
4	stavo	stare	_	V	_	2	advcl	_	_
5	facendo	fare	_	V	_	4	advcl	_	_
6	un	un	_	RI	_	7	det	_	_
7	gioco	gioco	_	S	_	5	dobj	_	_
8	persi	perdere	_	V+PC	_	5	advcl	_	_
9	un	un	_	RI	_	10	det	_	_
10	pezzo	pezzo	_	S	_	8	dobj	_	_
11	.	[PUNCT]	_	FS	_	2	punct	_	_

12	Cominciai	cominciare	_	V	_	0	root	_	_
13	a	a	_	E	_	14	mark	_	_
14	cercarlo	cercare	_	V+PC	_	12	xcomp	_	_
15	sotto	sotto	_	E	_	17	case	_	_
16	il	il	_	RD	_	17	det	_	_
17	tavolo	tavolo	_	S	_	14	nmod	_	_
18	,	[PUNCT]	_	FF	_	14	punct	_	_
19	sotto	sotto	_	E	_	21	case	_	_
20	la	la	_	RD	_	21	det	_	_
21	sedia	sedia	_	S	_	14	nmod	_	_
22	,	[PUNCT]	_	FF	_	21	punct	_	_
23	in	in	_	E	_	24	case	_	_
24	cucina	cucina	_	S	_	21	nmod	_	_
25	,	[PUNCT]	_	FF	_	14	punct	_	_
26	in	in	_	E	_	27	case	_	_
27	salotto	salotto	_	S	_	14	nmod	_	_
28	,	[PUNCT]	_	FF	_	14	punct	_	_
29	in	in	_	E	_	30	case	_	_
30	camera	camera	_	S	_	14	nmod	_	_
31	da	da	_	E	_	32	case	_	_
32	letto	letto	_	S	_	30	nmod	_	_
33	,	[PUNCT]	_	FF	_	14	punct	_	_
34	in	in	_	E	_	35	case	_	_
35	cantina	cantina	_	S	_	14	nmod	_	_
36	e	e	_	CC	_	35	cc	_	_
37	persino	persino	_	B	_	39	advmod	_	_
38	in	in	_	E	_	39	case	_	_
39	bagno	bagno	_	S	_	35	conj	_	_
40	.	[PUNCT]	_	FS	_	12	punct	_	_

41	Non	non	_	BN	_	43	neg	_	_
42	lo	lo	_	PC	_	43	dobj	_	_
43	trovavo	trovare	_	V	_	0	root	_	_
44	da	da	_	E	_	46	case	_	_
45	nessuna	nessuno	_	DI	_	46	det	_	_
46	parte	parte	_	S	_	43	nmod	_	_
47	,	[PUNCT]	_	FF	_	43	punct	_	_
48	ero	essere	_	V	_	49	cop	_	_
49	disperato	disperato	_	A	_	43	conj	_	_
50	non	non	_	BN	_	51	neg	_	_
51	sapevo	sapere	_	V	_	49	parataxis	_	_
52	piú	più	_	B	_	51	advmod	_	_
53	dove	dove	_	B	_	54	advmod	_	_
54	cercare	cercare	_	V	_	51	ccomp	_	_
55	.	[PUNCT]	_	FS	_	43	punct	_	_

56	Ogni	ogni	_	DI	_	57	det	_	_
57	giorno	giorno	_	S	_	0	root	_	_
58	dal	dal	_	E+RD	_	59	case	_	_
59	lunedí	lunedí	_	S	_	57	nmod	_	_
60	al	al	_	E+RD	_	61	case	_	_
61	venerdí	venerdí	_	S	_	57	nmod	_	_
62	cercavo	cercare	_	V	_	61	acl	_	_
63	in	in	_	E	_	65	case	_	_
64	un	un	_	RI	_	65	det	_	_
65	luogo	luogo	_	S	_	62	nmod	_	_
66	diverso	diverso	_	A	_	65	amod	_	_
67	.	[PUNCT]	_	FS	_	57	punct	_	_

68	Lunedí	Lunedí	_	SP	_	69	nsubj	_	_
69	cercai	cercare	_	V	_	0	root	_	_
70	in	in	_	E	_	71	case	_	_
71	cucina	cucina	_	S	_	69	nmod	_	_
72	ma	ma	_	CC	_	69	cc	_	_
73	non	non	_	BN	_	74	neg	_	_
74	trovai	trovare	_	V	_	69	conj	_	_
75	niente	niente	_	PI	_	74	dobj	_	_
76	.	[PUNCT]	_	FS	_	69	punct	_	_

77	Martedí	Martedí	_	SP	_	78	nsubj	_	_
78	cercai	cercare	_	V	_	0	root	_	_
79	in	in	_	E	_	80	case	_	_
80	bagno	bagno	_	S	_	78	nmod	_	_
81	ma	ma	_	CC	_	78	cc	_	_
82	anche	anche	_	B	_	83	advmod	_	_
83	lí	lí	_	S	_	85	nsubj	_	_
84	non	non	_	BN	_	85	neg	_	_
85	trovai	trovare	_	V	_	78	conj	_	_
86	nulla	nulla	_	PI	_	85	dobj	_	_
87	.	[PUNCT]	_	FS	_	78	punct	_	_

88	Mercoledí	Mercoledí	_	SP	_	89	nsubj	_	_
89	cercai	cercare	_	V	_	0	root	_	_
90	in	in	_	E	_	91	case	_	_
91	salotto	salotto	_	S	_	89	nmod	_	_
92	ma	ma	_	CC	_	89	cc	_	_
93	anche	anche	_	B	_	95	advmod	_	_
94	in	in	_	E	_	95	case	_	_
95	salotto	salotto	_	S	_	98	nmod	_	_
96	non	non	_	BN	_	98	neg	_	_
97	c'	c'	_	PC	_	98	expl	_	_
98	era	essere	_	V	_	89	conj	_	_
99	niente	niente	_	PI	_	98	xcomp	_	_
100	.	[PUNCT]	_	FS	_	89	punct	_	_

101	Giovedí	Giovedí	_	CS	_	102	mark	_	_
102	cercai	cercare	_	V	_	0	root	_	_
103	in	in	_	E	_	104	case	_	_
104	camera	camera	_	S	_	102	nmod	_	_
105	da	da	_	E	_	112	mark	_	_
106	le	la	_	RD	_	107	det	_	_
107	ma	ma	_	CC	_	112	cc	_	_
108	anche	anche	_	B	_	109	advmod	_	_
109	lí	lí	_	B	_	112	advmod	_	_
110	non	non	_	BN	_	112	neg	_	_
111	c'	c'	_	PC	_	112	expl	_	_
112	era	essere	_	V	_	102	advcl	_	_
113	nulla	nulla	_	PI	_	112	xcomp	_	_
114	.	[PUNCT]	_	FS	_	102	punct	_	_

115	Venerdí	Venerdí	_	SP	_	122	nsubj	_	_
116	mentre	mentre	_	CS	_	117	mark	_	_
117	cercavo	cercare	_	V	_	115	advcl	_	_
118	in	in	_	E	_	119	case	_	_
119	cantina	cantina	_	S	_	117	nmod	_	_
120	non	non	_	BN	_	122	neg	_	_
121	ho	avere	_	VA	_	122	aux	_	_
122	trovato	trovare	_	V	_	0	root	_	_
123	niente	niente	_	PI	_	122	dobj	_	_
124	.	[PUNCT]	_	FS	_	122	punct	_	_

125	Sabato	sabato	_	S	_	132	nmod	_	_
126	sera	sera	_	S	_	125	compound	_	_
127	mentre	mentre	_	CS	_	128	mark	_	_
128	andavo	andare	_	V	_	132	advcl	_	_
129	a	a	_	E	_	130	mark	_	_
130	dormire	dormire	_	V	_	128	xcomp	_	_
131	ho	avere	_	VA	_	132	aux	_	_
132	trovato	trovare	_	V	_	0	root	_	_
133	quel	quello	_	DD	_	134	det	_	_
134	pezzo	pezzo	_	S	_	132	dobj	_	_
135	che	che	_	PR	_	136	nsubj	_	_
136	cercavo	cercare	_	V	_	134	acl:relcl	_	_
137	da	da	_	E	_	138	case	_	_
138	giorni	giorno	_	S	_	136	nmod	_	_
139	.	[PUNCT]	_	FS	_	132	punct	_	_

140	Questo	questo	_	DD	_	141	det	_	_
141	episodio	episodio	_	S	_	144	nsubj	_	_
142	mi	mi	_	PC	_	144	dobj	_	_
143	ha	avere	_	VA	_	144	aux	_	_
144	insegnato	insegnare	_	V	_	0	root	_	_
145	a	a	_	E	_	146	mark	_	_
146	cercare	cercare	_	V	_	144	xcomp	_	_
147	meglio	meglio	_	B	_	146	advmod	_	_
148	.	[PUNCT]	_	FS	_	144	punct	_	_


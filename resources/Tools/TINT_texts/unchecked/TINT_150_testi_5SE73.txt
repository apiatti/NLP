1	Una	una	_	RI	_	2	det	_	_
2	giornata	giornata	_	S	_	9	nmod	_	_
3	a	a	_	E	_	4	case	_	_
4	Parigi	Parigi	_	SP	_	2	nmod	_	_
5	Un	un	_	RI	_	6	det	_	_
6	giorno	giorno	_	S	_	9	nmod	_	_
7	d'	da	_	E	_	8	case	_	_
8	estate	estate	_	S	_	6	nmod	_	_
9	decidemmo	decidere	_	V	_	0	root	_	_
10	di	di	_	E	_	11	mark	_	_
11	andare	andare	_	V	_	9	xcomp	_	_
12	a	a	_	E	_	13	case	_	_
13	Parigi	Parigi	_	SP	_	11	nmod	_	_
14	telefonammo	telefonare	_	V	_	11	xcomp	_	_
15	ad	ad	_	E	_	17	case	_	_
16	una	una	_	RI	_	17	det	_	_
17	agenzia	agenzia	_	S	_	14	nmod	_	_
18	viaggi	viaggio	_	S	_	17	nmod	_	_
19	e	e	_	CC	_	9	cc	_	_
20	rispose	rispondere	_	V	_	27	advcl	_	_
21	una	una	_	RI	_	22	det	_	_
22	signorina	signorina	_	S	_	20	dobj	_	_
23	e	e	_	CC	_	22	cc	_	_
24	mio	mio	_	AP	_	25	det:poss	_	_
25	padre	padre	_	S	_	22	conj	_	_
26	le	le	_	PC	_	27	iobj	_	_
27	chiese	chiedere	_	V	_	9	conj	_	_
28	se	se	_	CS	_	31	mark	_	_
29	si	si	_	PC	_	31	expl:impers	_	_
30	poteva	potere	_	VM	_	31	aux	_	_
31	andare	andare	_	V	_	27	ccomp	_	_
32	a	a	_	E	_	33	case	_	_
33	Parigi	Parigi	_	SP	_	31	nmod	_	_
34	una	una	_	RI	_	35	det	_	_
35	settimana	settimana	_	S	_	31	dobj	_	_
36	in	in	_	E	_	38	case	_	_
37	un	un	_	RI	_	38	det	_	_
38	hotel	hotel	_	S	_	35	nmod	_	_
39	a	a	_	E	_	41	case	_	_
40	cinque	cinque	_	N	_	41	nummod	_	_
41	stelle	stella	_	S	_	38	nmod	_	_
42	.	[PUNCT]	_	FS	_	9	punct	_	_

43	Lei	lei	_	PE	_	44	nsubj	_	_
44	rispose	rispondere	_	V	_	0	root	_	_
45	che	che	_	CS	_	52	mark	_	_
46	un	un	_	RI	_	47	det	_	_
47	alberxxxo	alberxxxo	_	S	_	52	nsubj	_	_
48	a	a	_	E	_	50	case	_	_
49	cinque	cinque	_	N	_	50	nummod	_	_
50	stelle	stella	_	S	_	47	nmod	_	_
51	c'	c'	_	PC	_	52	expl	_	_
52	era	essere	_	V	_	44	ccomp	_	_
53	ma	ma	_	CC	_	52	cc	_	_
54	era	essere	_	V	_	52	conj	_	_
55	lontano	lontano	_	B	_	57	advmod	_	_
56	alla	alla	_	E+RD	_	57	case	_	_
57	Tour	Tour	_	SP	_	54	nmod	_	_
58	Eiffel	Eiffel	_	SP	_	57	name	_	_
59	.	[PUNCT]	_	FS	_	44	punct	_	_

60	Per	per	_	E	_	61	case	_	_
61	fortuna	fortuna	_	S	_	64	nmod	_	_
62	peró	peró	_	S	_	61	compound	_	_
63	c'	c'	_	PC	_	64	expl	_	_
64	era	essere	_	V	_	0	root	_	_
65	un	un	_	RI	_	67	det	_	_
66	quattro	quattro	_	N	_	67	nummod	_	_
67	stelle	stella	_	S	_	64	nsubj	_	_
68	in	in	_	E	_	69	case	_	_
69	faccia	faccia	_	S	_	64	nmod	_	_
70	alla	alla	_	E+RD	_	71	case	_	_
71	Tour	Tour	_	SP	_	69	nmod	_	_
72	Eiffel	Eiffel	_	SP	_	71	name	_	_
73	e	e	_	CC	_	64	cc	_	_
74	noi	noi	_	PE	_	76	nsubj	_	_
75	lo	lo	_	PC	_	76	dobj	_	_
76	presimo	presimo	_	V	_	64	conj	_	_
77	(	[PUNCT]	_	FB	_	79	punct	_	_
78	cioè	cioè	_	B	_	79	advmod	_	_
79	pesimo	pesimo	_	V	_	76	advcl	_	_
80	una	una	_	RI	_	81	det	_	_
81	stanza	stanza	_	S	_	79	dobj	_	_
82	)	[PUNCT]	_	FB	_	79	punct	_	_
83	.	[PUNCT]	_	FS	_	64	punct	_	_

84	Quando	quando	_	CS	_	85	mark	_	_
85	arrivammo	arrivare	_	V	_	88	advcl	_	_
86	a	a	_	E	_	87	case	_	_
87	Parigi	Parigi	_	SP	_	85	nmod	_	_
88	andammo	andare	_	V	_	0	root	_	_
89	subito	subito	_	B	_	88	advmod	_	_
90	in	in	_	E	_	91	case	_	_
91	albergo	albergo	_	S	_	88	nmod	_	_
92	a	a	_	E	_	93	mark	_	_
93	mettere	mettere	_	V	_	88	xcomp	_	_
94	le	la	_	RD	_	95	det	_	_
95	valige	valigia	_	S	_	93	dobj	_	_
96	e	e	_	CC	_	93	cc	_	_
97	andammo	andare	_	V	_	102	cop	_	_
98	alla	alla	_	E+RD	_	99	case	_	_
99	Tour	Tour	_	SP	_	97	nmod	_	_
100	Eiffel	Eiffel	_	SP	_	99	name	_	_
101	era	essere	_	V	_	102	cop	_	_
102	alta	alto	_	A	_	93	conj	_	_
103	e	e	_	CC	_	93	cc	_	_
104	luminosa	luminoso	_	A	_	93	conj	_	_
105	.	[PUNCT]	_	FS	_	88	punct	_	_

106	Io	io	_	PE	_	109	nsubj	_	_
107	peró	peró	_	BN	_	109	neg	_	_
108	ero	essere	_	VA	_	109	aux	_	_
109	arrabbiata	arrabbiare	_	V	_	0	root	_	_
110	perche	perché	_	S	_	109	dobj	_	_
111	non	non	_	BN	_	113	neg	_	_
112	siamo	essere	_	V	_	113	cop	_	_
113	saliti	salire	_	V	_	109	advcl	_	_
114	sulla	sulla	_	E+RD	_	115	case	_	_
115	Tour	Tour	_	SP	_	113	nmod	_	_
116	Eiffel	Eiffel	_	SP	_	115	name	_	_
117	.	[PUNCT]	_	FS	_	109	punct	_	_

118	E	e	_	CC	_	125	cc	_	_
119	per	per	_	E	_	122	case	_	_
120	tutta	tutto	_	T	_	122	det:predet	_	_
121	la	la	_	RD	_	122	det	_	_
122	vacanza	vacanza	_	S	_	125	nmod	_	_
123	non	non	_	BN	_	125	neg	_	_
124	ero	essere	_	V	_	125	cop	_	_
125	contenta	contento	_	A	_	0	root	_	_
126	.	[PUNCT]	_	FS	_	125	punct	_	_


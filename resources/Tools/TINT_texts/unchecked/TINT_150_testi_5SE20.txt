1	Un	un	_	RI	_	2	det	_	_
2	anno	anno	_	S	_	6	nmod	_	_
3	fa	fa	_	B	_	2	advmod	_	_
4	,	[PUNCT]	_	FF	_	2	punct	_	_
5	stavamo	stare	_	VA	_	6	aux	_	_
6	giocando	giocare	_	V	_	0	root	_	_
7	l'	l'	_	RD	_	9	det	_	_
8	ultima	ultimo	_	A	_	9	amod	_	_
9	partita	partita	_	S	_	6	dobj	_	_
10	di	di	_	E	_	11	case	_	_
11	campionato	campionato	_	S	_	9	nmod	_	_
12	,	[PUNCT]	_	FF	_	6	punct	_	_
13	e	e	_	CC	_	6	cc	_	_
14	stavamo	stare	_	V	_	6	conj	_	_
15	perdendo	perdere	_	V	_	14	advcl	_	_
16	quattro	quattro	_	N	_	15	dobj	_	_
17	a	a	_	E	_	18	case	_	_
18	due	due	_	N	_	16	nummod	_	_
19	.	[PUNCT]	_	FS	_	6	punct	_	_

20	Mancavano	mancare	_	V	_	0	root	_	_
21	15	15	_	N	_	22	nummod	_	_
22	minuti	minuto	_	S	_	20	nmod	_	_
23	alla	alla	_	E+RD	_	24	case	_	_
24	fine	fine	_	S	_	20	nmod	_	_
25	della	della	_	E+RD	_	26	case	_	_
26	partita	partita	_	S	_	24	nmod	_	_
27	e	e	_	CC	_	20	cc	_	_
28	noi	noi	_	PE	_	30	nsubj	_	_
29	eravamo	essere	_	V	_	30	cop	_	_
30	stanchissimi	stanco	_	A	_	20	conj	_	_
31	,	[PUNCT]	_	FF	_	30	punct	_	_
32	il	il	_	RD	_	34	det	_	_
33	nostro	nostro	_	AP	_	34	det:poss	_	_
34	allenatore	allenatore	_	S	_	36	nsubj	_	_
35	stava	stare	_	VA	_	36	aux	_	_
36	gridando	gridare	_	V	_	30	conj	_	_
37	tantissimo	tanto	_	A	_	36	xcomp	_	_
38	.	[PUNCT]	_	FS	_	20	punct	_	_

39	Dopo	dopo	_	E	_	41	case	_	_
40	cinque	cinque	_	N	_	41	nummod	_	_
41	minuti	minuto	_	S	_	43	nmod	_	_
42	abbiamo	avere	_	VA	_	43	aux	_	_
43	fatto	fare	_	V	_	0	root	_	_
44	un	un	_	RI	_	45	det	_	_
45	goal	goal	_	S	_	43	dobj	_	_
46	,	[PUNCT]	_	FF	_	43	punct	_	_
47	e	e	_	CC	_	43	cc	_	_
48	da	da	_	E	_	49	case	_	_
49	lí	lí	_	S	_	52	nmod	_	_
50	ci	ci	_	PC	_	52	iobj	_	_
51	abbiamo	avere	_	VA	_	52	aux	_	_
52	creduto	credere	_	V	_	43	conj	_	_
53	fino	fino	_	B	_	55	case	_	_
54	alla	alla	_	E+RD	_	53	mwe	_	_
55	fine	fine	_	S	_	52	nmod	_	_
56	.	[PUNCT]	_	FS	_	43	punct	_	_

57	Poi	poi	_	B	_	60	advmod	_	_
58	un'	un'	_	RI	_	60	det	_	_
59	altro	altro	_	A	_	60	amod	_	_
60	goal	goal	_	S	_	0	root	_	_
61	e	e	_	CC	_	60	cc	_	_
62	allora	allora	_	B	_	63	advmod	_	_
63	divenne	divenire	_	V	_	60	conj	_	_
64	un	un	_	RI	_	65	det	_	_
65	pareggio	pareggio	_	S	_	63	xcomp	_	_
66	.	[PUNCT]	_	FS	_	60	punct	_	_

67	Mancavano	mancare	_	V	_	0	root	_	_
68	due	due	_	N	_	69	nummod	_	_
69	minuti	minuto	_	S	_	67	nmod	_	_
70	e	e	_	CC	_	67	cc	_	_
71	stavamo	stare	_	VA	_	72	aux	_	_
72	lottando	lottare	_	V	_	67	conj	_	_
73	per	per	_	E	_	74	mark	_	_
74	vincere	vincere	_	V	_	72	advcl	_	_
75	,	[PUNCT]	_	FF	_	74	punct	_	_
76	poi	poi	_	B	_	77	advmod	_	_
77	proprio	proprio	_	B	_	81	advmod	_	_
78	quando	quando	_	CS	_	81	mark	_	_
79	l'	l'	_	RD	_	80	det	_	_
80	albitro	albitro	_	S	_	81	nsubj	_	_
81	stava	stare	_	V	_	74	advcl	_	_
82	per	per	_	E	_	83	mark	_	_
83	fischiare	fischiare	_	V	_	81	advcl	_	_
84	la	la	_	RD	_	85	det	_	_
85	fine	fine	_	S	_	83	dobj	_	_
86	,	[PUNCT]	_	FF	_	81	punct	_	_
87	io	io	_	PE	_	89	nsubj	_	_
88	ho	avere	_	VA	_	89	aux	_	_
89	fatto	fare	_	V	_	81	conj	_	_
90	il	il	_	RD	_	91	det	_	_
91	goal	goal	_	S	_	89	dobj	_	_
92	decisivo	decisivo	_	A	_	91	amod	_	_
93	e	e	_	CC	_	81	cc	_	_
94	quindi	quindi	_	B	_	96	advmod	_	_
95	abbiamo	avere	_	VA	_	96	aux	_	_
96	vinto	vincere	_	V	_	81	conj	_	_
97	la	la	_	RD	_	98	det	_	_
98	partita	partita	_	S	_	96	dobj	_	_
99	.	[PUNCT]	_	FS	_	67	punct	_	_

100	Questa	questo	_	DD	_	101	det	_	_
101	esperienza	esperienza	_	S	_	104	nsubj	_	_
102	mi	mi	_	PC	_	104	iobj	_	_
103	ha	avere	_	VA	_	104	aux	_	_
104	insegnato	insegnare	_	V	_	0	root	_	_
105	che	che	_	CS	_	108	mark	_	_
106	anche	anche	_	B	_	108	advmod	_	_
107	se	se	_	CS	_	108	mark	_	_
108	stai	stare	_	V	_	114	advcl	_	_
109	perdendo	perdere	_	V	_	108	advcl	_	_
110	la	la	_	RD	_	111	det	_	_
111	partita	partita	_	S	_	109	dobj	_	_
112	devi	deviare	_	VM	_	114	aux	_	_
113	comunque	comunque	_	B	_	114	advmod	_	_
114	dare	dare	_	V	_	104	ccomp	_	_
115	il	il	_	RD	_	116	det	_	_
116	massimo	massimo	_	A	_	114	dobj	_	_
117	e	e	_	CC	_	116	cc	_	_
118	crederci	credere	_	V+PC	_	116	conj	_	_
119	fino	fino	_	B	_	121	case	_	_
120	alla	alla	_	E+RD	_	119	mwe	_	_
121	fine	fine	_	S	_	114	nmod	_	_
122	.	[PUNCT]	_	FS	_	104	punct	_	_


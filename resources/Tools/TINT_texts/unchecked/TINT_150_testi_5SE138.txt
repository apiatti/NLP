1	La	la	_	RD	_	6	det	_	_
2	mia	mio	_	AP	_	6	det:poss	_	_
3	cara	caro	_	A	_	6	amod	_	_
4	e	e	_	CC	_	3	cc	_	_
5	dolce	dolce	_	A	_	3	conj	_	_
6	nonna	nonna	_	S	_	8	nsubj	_	_
7	Io	io	_	PE	_	6	nmod	_	_
8	avevo	avere	_	V	_	0	root	_	_
9	una	una	_	RI	_	10	det	_	_
10	nonna	nonna	_	S	_	8	dobj	_	_
11	bravissima	bravo	_	A	_	10	amod	_	_
12	,	[PUNCT]	_	FF	_	8	punct	_	_
13	mi	mi	_	PC	_	14	iobj	_	_
14	voleva	volere	_	V	_	8	conj	_	_
15	un	un	_	RI	_	16	det	_	_
16	mondo	mondo	_	S	_	14	dobj	_	_
17	di	di	_	E	_	18	case	_	_
18	bene	bene	_	S	_	16	nmod	_	_
19	,	[PUNCT]	_	FF	_	8	punct	_	_
20	quando	quando	_	CS	_	26	mark	_	_
21	la	la	_	RD	_	22	det	_	_
22	mamma	mamma	_	S	_	26	nsubj	_	_
23	e	e	_	CC	_	22	cc	_	_
24	il	il	_	RD	_	25	det	_	_
25	papá	papá	_	S	_	22	conj	_	_
26	lavoravano	lavorare	_	V	_	33	advcl	_	_
27	io	io	_	PE	_	26	dobj	_	_
28	,	[PUNCT]	_	FF	_	26	punct	_	_
29	dopo	dopo	_	E	_	31	case	_	_
30	la	la	_	RD	_	31	det	_	_
31	scuola	scuola	_	S	_	26	nmod	_	_
32	,	[PUNCT]	_	FF	_	26	punct	_	_
33	andavo	andare	_	V	_	8	parataxis	_	_
34	da	da	_	E	_	35	case	_	_
35	lei	lei	_	PE	_	33	nmod	_	_
36	.	[PUNCT]	_	FS	_	8	punct	_	_

37	Mi	mi	_	PC	_	38	iobj	_	_
38	aspettava	aspettare	_	V	_	0	root	_	_
39	sempre	sempre	_	B	_	38	advmod	_	_
40	davanti	davanti	_	B	_	42	case	_	_
41	alla	alla	_	E+RD	_	40	mwe	_	_
42	scuola	scuola	_	S	_	38	nmod	_	_
43	,	[PUNCT]	_	FF	_	38	punct	_	_
44	con	con	_	E	_	46	case	_	_
45	la	la	_	RD	_	46	det	_	_
46	merenda	merenda	_	S	_	38	nmod	_	_
47	in	in	_	E	_	48	case	_	_
48	automobile	automobile	_	S	_	46	nmod	_	_
49	e	e	_	CC	_	38	cc	_	_
50	quando	quando	_	CS	_	53	mark	_	_
51	arrivavo	arrivare	_	V	_	53	advcl	_	_
52	mi	mi	_	PC	_	53	expl	_	_
53	salutava	salutare	_	V	_	67	advcl	_	_
54	e	e	_	CC	_	53	cc	_	_
55	mi	mi	_	PC	_	56	dobj	_	_
56	abbracciava	abbracciare	_	V	_	53	conj	_	_
57	,	[PUNCT]	_	FF	_	53	punct	_	_
58	sapeva	sapere	_	V	_	53	conj	_	_
59	sempre	sempre	_	B	_	58	advmod	_	_
60	cosa	cosa	_	PQ	_	61	dobj	_	_
61	avevo	avere	_	V	_	58	ccomp	_	_
62	voglia	voglia	_	S	_	61	nsubj	_	_
63	di	di	_	E	_	64	mark	_	_
64	fare	fare	_	V	_	62	acl	_	_
65	,	[PUNCT]	_	FF	_	53	punct	_	_
66	mi	mi	_	PC	_	67	iobj	_	_
67	faceva	fare	_	V	_	38	conj	_	_
68	sentire	sentire	_	V	_	67	ccomp	_	_
69	speciale	speciale	_	A	_	68	xcomp	_	_
70	,	[PUNCT]	_	FF	_	67	punct	_	_
71	anche	anche	_	B	_	72	advmod	_	_
72	io	io	_	S	_	67	nmod	_	_
73	e	e	_	CC	_	72	cc	_	_
74	tutte	tutto	_	T	_	77	det:predet	_	_
75	le	la	_	RD	_	77	det	_	_
76	altre	altro	_	A	_	77	amod	_	_
77	persone	persona	_	S	_	79	nsubj	_	_
78	le	le	_	PC	_	79	dobj	_	_
79	volevano	volere	_	V	_	72	conj	_	_
80	bene	bene	_	B	_	79	advmod	_	_
81	.	[PUNCT]	_	FS	_	38	punct	_	_

82	Un	un	_	RI	_	83	det	_	_
83	giorno	giorno	_	S	_	0	root	_	_
84	pero	pero	_	S	_	83	nmod	_	_
85	,	[PUNCT]	_	FF	_	83	punct	_	_
86	visto	vistare	_	V	_	83	acl	_	_
87	che	che	_	CS	_	93	mark	_	_
88	l'	l'	_	RD	_	89	det	_	_
89	anno	anno	_	S	_	93	nsubj	_	_
90	precedente	precedente	_	A	_	89	amod	_	_
91	non	non	_	BN	_	93	neg	_	_
92	era	essere	_	VA	_	93	aux	_	_
93	andata	andare	_	V	_	86	ccomp	_	_
94	neppure	neppure	_	BN	_	96	neg	_	_
95	una	una	_	RI	_	96	det	_	_
96	volta	volta	_	S	_	93	dobj	_	_
97	dal	dal	_	E+RD	_	98	case	_	_
98	medico	medico	_	S	_	96	nmod	_	_
99	,	[PUNCT]	_	FF	_	83	punct	_	_
100	é	essere	_	CS	_	101	mark	_	_
101	andata	andare	_	V	_	83	acl	_	_
102	a	a	_	E	_	103	mark	_	_
103	fare	fare	_	V	_	101	xcomp	_	_
104	un	un	_	RI	_	105	det	_	_
105	controllo	controllo	_	S	_	103	dobj	_	_
106	,	[PUNCT]	_	FF	_	101	punct	_	_
107	i	il	_	RD	_	108	det	_	_
108	risultati	risultato	_	S	_	101	nsubj	_	_
109	dicevano	dire	_	V	_	108	acl	_	_
110	che	che	_	PR	_	111	nsubj	_	_
111	aveva	avere	_	V	_	108	acl:relcl	_	_
112	un	un	_	RI	_	113	det	_	_
113	tumore	tumore	_	S	_	111	dobj	_	_
114	al	al	_	E+RD	_	115	case	_	_
115	polmone	polmone	_	S	_	111	nmod	_	_
116	.	[PUNCT]	_	FS	_	83	punct	_	_

117	Il	il	_	RD	_	118	det	_	_
118	medico	medico	_	S	_	123	nsubj	_	_
119	,	[PUNCT]	_	FF	_	118	punct	_	_
120	fortunatamente	fortunatamente	_	B	_	123	advmod	_	_
121	,	[PUNCT]	_	FF	_	120	punct	_	_
122	aveva	avere	_	VA	_	123	aux	_	_
123	detto	dire	_	V	_	0	root	_	_
124	che	che	_	CS	_	126	mark	_	_
125	era	essere	_	V	_	126	cop	_	_
126	operabile	operabile	_	A	_	136	advcl	_	_
127	cosí	cosí	_	S	_	126	nsubj	_	_
128	,	[PUNCT]	_	FF	_	126	punct	_	_
129	dopo	dopo	_	E	_	131	case	_	_
130	una	una	_	RI	_	131	det	_	_
131	serie	serie	_	S	_	126	nmod	_	_
132	di	di	_	E	_	133	case	_	_
133	esami	esame	_	S	_	131	nmod	_	_
134	,	[PUNCT]	_	FF	_	126	punct	_	_
135	venne	venire	_	VA	_	136	auxpass	_	_
136	operata	operare	_	V	_	123	ccomp	_	_
137	.	[PUNCT]	_	FS	_	123	punct	_	_

138	Non	non	_	BN	_	139	neg	_	_
139	aveva	avere	_	V	_	0	root	_	_
140	paura	paura	_	S	_	139	dobj	_	_
141	di	di	_	E	_	142	case	_	_
142	niente	niente	_	PI	_	140	nmod	_	_
143	,	[PUNCT]	_	FF	_	139	punct	_	_
144	ne	ne	_	PC	_	146	advmod	_	_
145	dell'	dell'	_	E+RD	_	146	case	_	_
146	operazione	operazione	_	S	_	139	nmod	_	_
147	ne	ne	_	PC	_	149	advmod	_	_
148	di	di	_	E	_	149	case	_	_
149	quello	quello	_	PD	_	146	nmod	_	_
150	che	che	_	PR	_	151	nsubj	_	_
151	seguiva	seguire	_	V	_	149	acl:relcl	_	_
152	.	[PUNCT]	_	FS	_	139	punct	_	_

153	fece	fare	_	V	_	157	advcl	_	_
154	l'	l'	_	RD	_	155	det	_	_
155	operazione	operazione	_	S	_	153	dobj	_	_
156	,	[PUNCT]	_	FF	_	153	punct	_	_
157	fece	fare	_	V	_	0	root	_	_
158	la	la	_	RD	_	159	det	_	_
159	chemioterapia	chemioterapia	_	S	_	157	dobj	_	_
160	,	[PUNCT]	_	FF	_	157	punct	_	_
161	fece	fare	_	V	_	157	advcl	_	_
162	le	la	_	RD	_	163	det	_	_
163	radiazioni	radiazione	_	S	_	161	dobj	_	_
164	(	[PUNCT]	_	FB	_	165	punct	_	_
165	tutto	tutto	_	PI	_	161	nmod	_	_
166	questo	questo	_	DD	_	168	det	_	_
167	per	per	_	E	_	168	mark	_	_
168	eliminare	eliminare	_	V	_	165	advcl	_	_
169	del	del	_	E+RD	_	172	case	_	_
170	tutto	tutto	_	T	_	172	det:predet	_	_
171	il	il	_	RD	_	172	det	_	_
172	cancro	cancro	_	S	_	168	nmod	_	_
173	)	[PUNCT]	_	FB	_	165	punct	_	_
174	e	e	_	CC	_	161	cc	_	_
175	una	una	_	RI	_	176	det	_	_
176	settimana	settimana	_	S	_	183	nmod	_	_
177	dopo	dopo	_	E	_	179	case	_	_
178	le	la	_	RD	_	179	det	_	_
179	radiazioni	radiazione	_	S	_	176	nmod	_	_
180	il	il	_	RD	_	181	det	_	_
181	dottore	dottore	_	S	_	183	nsubj	_	_
182	ci	ci	_	PC	_	183	iobj	_	_
183	disse	dire	_	V	_	161	conj	_	_
184	che	che	_	CS	_	188	mark	_	_
185	delle	della	_	E+RD	_	186	case	_	_
186	metastasi	metastasi	_	S	_	188	nmod	_	_
187	erano	essere	_	VA	_	188	aux	_	_
188	arrivate	arrivare	_	V	_	183	ccomp	_	_
189	al	al	_	E+RD	_	190	case	_	_
190	cervello	cervello	_	S	_	188	nmod	_	_
191	(	[PUNCT]	_	FB	_	192	punct	_	_
192	tre	tre	_	N	_	188	nummod	_	_
193	)	[PUNCT]	_	FB	_	192	punct	_	_
194	.	[PUNCT]	_	FS	_	157	punct	_	_

195	Io	io	_	PE	_	198	nsubj	_	_
196	in	in	_	E	_	197	case	_	_
197	principio	principio	_	S	_	198	nmod	_	_
198	credevo	credere	_	V	_	0	root	_	_
199	che	che	_	PR	_	201	nsubj	_	_
200	sarebbe	essere	_	V	_	201	cop	_	_
201	guarita	guarire	_	A	_	198	acl:relcl	_	_
202	,	[PUNCT]	_	FF	_	201	punct	_	_
203	ma	ma	_	CC	_	201	cc	_	_
204	dopo	dopo	_	B	_	206	advmod	_	_
205	qualche	qualche	_	DI	_	206	det	_	_
206	mese	mese	_	S	_	210	nmod	_	_
207	la	la	_	RD	_	208	det	_	_
208	mamma	mamma	_	S	_	210	nsubj	_	_
209	mi	mi	_	PC	_	210	dobj	_	_
210	fece	fare	_	V	_	201	conj	_	_
211	capire	capire	_	V	_	210	ccomp	_	_
212	che	che	_	CS	_	216	mark	_	_
213	non	non	_	BN	_	216	neg	_	_
214	sarebbe	essere	_	VA	_	216	aux	_	_
215	mai	mai	_	B	_	216	advmod	_	_
216	guarita	guarire	_	V	_	211	ccomp	_	_
217	.	[PUNCT]	_	FS	_	198	punct	_	_

218	Nonostante	nonostante	_	E	_	219	case	_	_
219	questo	questo	_	PD	_	223	nmod	_	_
220	la	la	_	RD	_	221	det	_	_
221	nonna	nonna	_	S	_	223	nsubj	_	_
222	non	non	_	BN	_	223	neg	_	_
223	aveva	avere	_	V	_	0	root	_	_
224	paura	paura	_	S	_	223	dobj	_	_
225	,	[PUNCT]	_	FF	_	223	punct	_	_
226	andava	andare	_	V	_	228	cop	_	_
227	avanti	avanti	_	B	_	228	advmod	_	_
228	felice	felice	_	A	_	223	conj	_	_
229	.	[PUNCT]	_	FS	_	223	punct	_	_

230	Ma	ma	_	CC	_	234	cc	_	_
231	settimana	settimana	_	S	_	234	nmod	_	_
232	dopo	dopo	_	E	_	233	case	_	_
233	settimana	settimana	_	S	_	231	nmod	_	_
234	continuava	continuare	_	V	_	0	root	_	_
235	a	a	_	E	_	236	mark	_	_
236	peggiorare	peggiorare	_	V	_	234	xcomp	_	_
237	:	[PUNCT]	_	FC	_	234	punct	_	_
238	prima	primo	_	NO	_	240	nsubj	_	_
239	ha	avere	_	VA	_	240	aux	_	_
240	perso	perdere	_	V	_	234	advcl	_	_
241	l'	l'	_	RD	_	242	det	_	_
242	equilibrio	equilibrio	_	S	_	240	dobj	_	_
243	,	[PUNCT]	_	FF	_	240	punct	_	_
244	poi	poi	_	B	_	245	advmod	_	_
245	straparlava	straparlare	_	V	_	240	conj	_	_
246	e	e	_	CC	_	240	cc	_	_
247	gli	il	_	RD	_	249	det	_	_
248	ultimi	ultimo	_	A	_	249	amod	_	_
249	giorni	giorno	_	S	_	278	nsubj	_	_
250	era	essere	_	V	_	278	advcl	_	_
251	in	in	_	E	_	253	case	_	_
252	uno	un	_	RI	_	253	det	_	_
253	stato	stato	_	S	_	250	nmod	_	_
254	di	di	_	E	_	255	case	_	_
255	coma	coma	_	S	_	253	nmod	_	_
256	,	[PUNCT]	_	FF	_	250	punct	_	_
257	faticava	faticare	_	V	_	278	advcl	_	_
258	a	a	_	E	_	259	mark	_	_
259	respirare	respirare	_	V	_	257	xcomp	_	_
260	,	[PUNCT]	_	FF	_	257	punct	_	_
261	fino	fino	_	B	_	264	case	_	_
262	ad	ad	_	E	_	261	mwe	_	_
263	una	una	_	RI	_	264	det	_	_
264	mattina	mattina	_	S	_	269	nmod	_	_
265	,	[PUNCT]	_	FF	_	264	punct	_	_
266	la	la	_	RD	_	267	det	_	_
267	mamma	mamma	_	S	_	269	nsubj	_	_
268	era	essere	_	VA	_	269	aux	_	_
269	passata	passare	_	V	_	257	conj	_	_
270	a	a	_	E	_	271	mark	_	_
271	vederla	vedere	_	V+PC	_	269	xcomp	_	_
272	prima	prima	_	B	_	274	mark	_	_
273	di	di	_	E	_	272	mwe	_	_
274	andare	andare	_	V	_	271	advcl	_	_
275	al	al	_	E+RD	_	276	case	_	_
276	lavoro	lavoro	_	S	_	274	nmod	_	_
277	,	[PUNCT]	_	FF	_	257	punct	_	_
278	vide	vedere	_	V	_	240	conj	_	_
279	che	che	_	CS	_	280	mark	_	_
280	cominciava	cominciare	_	V	_	278	ccomp	_	_
281	a	a	_	E	_	282	mark	_	_
282	respirare	respirare	_	V	_	280	xcomp	_	_
283	sempre	sempre	_	B	_	284	advmod	_	_
284	piú	più	_	A	_	282	xcomp	_	_
285	lentamente	lentamente	_	B	_	282	advmod	_	_
286	e	e	_	CC	_	282	cc	_	_
287	poi	poi	_	B	_	289	advmod	_	_
288	ha	avere	_	VA	_	289	aux	_	_
289	smesso	smettere	_	V	_	282	conj	_	_
290	di	di	_	E	_	291	mark	_	_
291	respirare	respirare	_	V	_	289	xcomp	_	_
292	.	[PUNCT]	_	FS	_	234	punct	_	_

293	Questo	questo	_	PD	_	296	nsubj	_	_
294	mi	mi	_	PC	_	296	dobj	_	_
295	ha	avere	_	VA	_	296	aux	_	_
296	insegniato	insegniato	_	V	_	0	root	_	_
297	di	di	_	E	_	298	mark	_	_
298	godere	godere	_	V	_	296	xcomp	_	_
299	le	la	_	RD	_	300	det	_	_
300	cose	cosa	_	S	_	298	dobj	_	_
301	fino	fino	_	B	_	298	advmod	_	_
302	che	che	_	CS	_	301	mwe	_	_
303	ci	ci	_	PC	_	304	expl	_	_
304	sono	essere	_	V	_	301	conj	_	_
305	,	[PUNCT]	_	FF	_	304	punct	_	_
306	perché	perché	_	CS	_	308	mark	_	_
307	non	non	_	BN	_	308	neg	_	_
308	tutto	tutto	_	PI	_	309	advmod	_	_
309	resta	restare	_	V	_	304	conj	_	_
310	per	per	_	E	_	311	case	_	_
311	sempre	sempre	_	B	_	309	advmod	_	_
312	.	[PUNCT]	_	FS	_	296	punct	_	_


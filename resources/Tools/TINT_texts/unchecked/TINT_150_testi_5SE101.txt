1	A	a	_	E	_	2	case	_	_
2	ritmica	ritmico	_	A	_	8	nmod	_	_
3	All'	all'	_	E+RD	_	4	case	_	_
4	inizio	inizio	_	S	_	2	nmod	_	_
5	dell'	dell'	_	E+RD	_	6	case	_	_
6	anno	anno	_	S	_	4	nmod	_	_
7	scolastico	scolastico	_	A	_	6	amod	_	_
8	inizia	iniziare	_	V	_	0	root	_	_
9	la	la	_	RD	_	10	det	_	_
10	ritmica	ritmico	_	A	_	8	xcomp	_	_
11	(	[PUNCT]	_	FB	_	10	punct	_	_
12	e	e	_	CC	_	8	cc	_	_
13	sono	essere	_	VA	_	14	aux	_	_
14	gia	gia	_	V	_	8	conj	_	_
15	tre	tre	_	N	_	16	nummod	_	_
16	anni	anno	_	S	_	14	nmod	_	_
17	che	che	_	PR	_	19	nsubj	_	_
18	lo	lo	_	PC	_	19	dobj	_	_
19	faccio	fare	_	V	_	16	acl:relcl	_	_
20	)	[PUNCT]	_	FB	_	8	punct	_	_
21	.	[PUNCT]	_	FS	_	8	punct	_	_

22	E	e	_	CC	_	23	cc	_	_
23	imparo	imparare	_	V	_	0	root	_	_
24	velocemente	velocemente	_	B	_	23	advmod	_	_
25	un	un	_	RI	_	27	det	_	_
26	nuovo	nuovo	_	A	_	27	amod	_	_
27	balletto	balletto	_	S	_	23	dobj	_	_
28	sempre	sempre	_	B	_	29	advmod	_	_
29	piú	più	_	A	_	27	amod	_	_
30	difficile	difficile	_	A	_	29	amod	_	_
31	e	e	_	CC	_	30	cc	_	_
32	complicato	complicato	_	A	_	30	conj	_	_
33	.	[PUNCT]	_	FS	_	23	punct	_	_

34	Ogni	ogni	_	DI	_	35	det	_	_
35	venerdí	venerdí	_	S	_	37	nsubj	_	_
36	pomeriggio	pomeriggio	_	S	_	35	nmod	_	_
37	vado	andare	_	V	_	0	root	_	_
38	a	a	_	E	_	39	case	_	_
39	ritmica	ritmico	_	A	_	37	nmod	_	_
40	a	a	_	E	_	41	case	_	_
41	Losone	Losone	_	SP	_	39	nmod	_	_
42	perché	perché	_	CS	_	44	mark	_	_
43	devo	dovere	_	VM	_	44	aux	_	_
44	allenarmi	allenare	_	V+PC	_	37	advcl	_	_
45	per	per	_	E	_	47	case	_	_
46	le	la	_	RD	_	47	det	_	_
47	gare	gara	_	S	_	44	nmod	_	_
48	,	[PUNCT]	_	FF	_	44	punct	_	_
49	ma	ma	_	CC	_	44	cc	_	_
50	non	non	_	BN	_	51	neg	_	_
51	quasi	quasi	_	B	_	54	advmod	_	_
52	sempre	sempre	_	B	_	54	advmod	_	_
53	mi	mi	_	PC	_	54	iobj	_	_
54	impegno	impegno	_	S	_	44	conj	_	_
55	totalmente	totalmente	_	B	_	54	advmod	_	_
56	.	[PUNCT]	_	FS	_	37	punct	_	_

57	Ma	ma	_	CC	_	62	cc	_	_
58	quest'	questo	_	DD	_	59	det	_	_
59	anno	anno	_	S	_	62	nmod	_	_
60	alle	alla	_	E+RD	_	61	case	_	_
61	gare	gara	_	S	_	62	nmod	_	_
62	ottengo	ottenere	_	V	_	75	advcl	_	_
63	risultati	risultato	_	S	_	62	dobj	_	_
64	un	un	_	RI	_	65	det	_	_
65	po'	po'	_	B	_	66	advmod	_	_
66	meno	meno	_	B	_	67	advmod	_	_
67	belli	bello	_	A	_	62	nmod	_	_
68	rispetto	rispetto	_	E	_	70	case	_	_
69	all'	all'	_	E+RD	_	68	mwe	_	_
70	anno	anno	_	S	_	67	nmod	_	_
71	scorso	scorso	_	A	_	70	amod	_	_
72	,	[PUNCT]	_	FF	_	62	punct	_	_
73	inseguito	inseguire	_	V	_	75	csubj	_	_
74	sono	essere	_	VA	_	75	aux	_	_
75	riuscita	riuscire	_	V	_	0	root	_	_
76	allegramente	allegramente	_	B	_	75	advmod	_	_
77	a	a	_	E	_	78	mark	_	_
78	passare	passare	_	V	_	75	xcomp	_	_
79	il	il	_	RD	_	80	det	_	_
80	test	test	_	S	_	78	dobj	_	_
81	2a	2a	_	SP	_	80	nmod	_	_
82	.	[PUNCT]	_	FS	_	75	punct	_	_

83	Ho	avere	_	VA	_	84	aux	_	_
84	capito	capire	_	V	_	0	root	_	_
85	che	che	_	CS	_	91	mark	_	_
86	se	se	_	CS	_	88	mark	_	_
87	mi	mi	_	PC	_	88	advmod	_	_
88	impegno	impegno	_	S	_	91	nsubj	_	_
89	di	di	_	E	_	90	case	_	_
90	piú	più	_	S	_	88	nmod	_	_
91	riesco	riuscire	_	V	_	84	ccomp	_	_
92	ad	ad	_	E	_	93	mark	_	_
93	ottenere	ottenere	_	V	_	91	xcomp	_	_
94	un	un	_	RI	_	95	det	_	_
95	risultato	risultato	_	S	_	93	dobj	_	_
96	piú	più	_	A	_	95	amod	_	_
97	bello	bello	_	A	_	95	amod	_	_
98	.	[PUNCT]	_	FS	_	84	punct	_	_


1	Valentina	Valentina	_	SP	_	0	root	_	_
2	e	e	_	CC	_	1	cc	_	_
3	l'	l'	_	RD	_	4	det	_	_
4	incidente	incidente	_	S	_	10	nmod	_	_
5	Un	un	_	RI	_	6	det	_	_
6	giorno	giorno	_	S	_	10	nmod	_	_
7	la	la	_	RD	_	8	det	_	_
8	mamma	mamma	_	S	_	10	nsubj	_	_
9	ha	avere	_	VA	_	10	aux	_	_
10	organizzato	organizzare	_	V	_	1	conj	_	_
11	una	una	_	RI	_	12	det	_	_
12	festa	festa	_	S	_	10	nsubj	_	_
13	in	in	_	E	_	14	case	_	_
14	cui	cui	_	PR	_	16	nmod	_	_
15	sono	essere	_	VA	_	16	aux	_	_
16	arrivati	arrivare	_	V	_	12	acl:relcl	_	_
17	tutti	tutto	_	T	_	19	det:predet	_	_
18	gli	il	_	RD	_	19	det	_	_
19	abitanti	abitante	_	S	_	16	nsubj	_	_
20	di	di	_	E	_	21	case	_	_
21	Lelgio	Lelgio	_	SP	_	19	nmod	_	_
22	,	[PUNCT]	_	FF	_	1	punct	_	_
23	tranne	tranne	_	E	_	26	case	_	_
24	un	un	_	RI	_	26	det	_	_
25	'	[PUNCT]	_	FB	_	26	punct	_	_
26	signore	signora	_	S	_	1	nmod	_	_
27	che	che	_	PR	_	28	nsubj	_	_
28	era	essere	_	V	_	26	acl:relcl	_	_
29	via	via	_	B	_	28	advmod	_	_
30	.	[PUNCT]	_	FS	_	1	punct	_	_

31	Tra	tra	_	E	_	33	case	_	_
32	gli	gli	_	PC	_	33	iobj	_	_
33	invitati	invitare	_	V	_	35	nmod	_	_
34	c'	c'	_	PC	_	35	expl	_	_
35	era	essere	_	V	_	0	root	_	_
36	la	la	_	RD	_	38	det	_	_
37	mia	mio	_	AP	_	38	det:poss	_	_
38	vicina	vicino	_	A	_	35	nsubj	_	_
39	di	di	_	E	_	40	case	_	_
40	casa	casa	_	S	_	38	nmod	_	_
41	che	che	_	PR	_	43	nsubj	_	_
42	si	si	_	PC	_	43	expl	_	_
43	chiama	chiamare	_	V	_	40	acl:relcl	_	_
44	Valentina	Valentina	_	SP	_	43	xcomp	_	_
45	.	[PUNCT]	_	FS	_	35	punct	_	_

46	Mentre	mentre	_	CS	_	47	mark	_	_
47	attraversava	attraversare	_	V	_	0	root	_	_
48	la	la	_	RD	_	49	det	_	_
49	strada	strada	_	S	_	47	dobj	_	_
50	per	per	_	E	_	51	mark	_	_
51	giocare	giocare	_	V	_	47	advcl	_	_
52	,	[PUNCT]	_	FF	_	51	punct	_	_
53	senza	senza	_	E	_	54	mark	_	_
54	guardare	guardare	_	V	_	51	advcl	_	_
55	,	[PUNCT]	_	FF	_	54	punct	_	_
56	arrivó	arrivó	_	A	_	54	amod	_	_
57	improvvisamente	improvvisamente	_	B	_	56	advmod	_	_
58	,	[PUNCT]	_	FF	_	54	punct	_	_
59	con	con	_	E	_	61	case	_	_
60	la	la	_	RD	_	61	det	_	_
61	moto	moto	_	S	_	54	nmod	_	_
62	l'	l'	_	RD	_	63	det	_	_
63	unico	unico	_	S	_	54	dobj	_	_
64	invitato	invitare	_	V	_	63	acl	_	_
65	ritardatario	ritardatario	_	S	_	64	dobj	_	_
66	.	[PUNCT]	_	FS	_	47	punct	_	_

67	Valentina	Valentina	_	SP	_	73	nsubj	_	_
68	e	e	_	CC	_	67	cc	_	_
69	il	il	_	RD	_	70	det	_	_
70	signore	signore	_	S	_	67	conj	_	_
71	non	non	_	BN	_	73	neg	_	_
72	si	si	_	PC	_	73	dobj	_	_
73	videro	vedere	_	V	_	0	root	_	_
74	e	e	_	CC	_	73	cc	_	_
75	si	si	_	PC	_	76	expl	_	_
76	scontrarono	scontrare	_	V	_	73	conj	_	_
77	.	[PUNCT]	_	FS	_	73	punct	_	_

78	Valentina	Valentina	_	SP	_	80	nsubj	_	_
79	si	si	_	PC	_	80	expl	_	_
80	fece	fare	_	V	_	0	root	_	_
81	male	male	_	B	_	80	advmod	_	_
82	a	a	_	E	_	84	case	_	_
83	un	un	_	RI	_	84	det	_	_
84	braccio	braccio	_	S	_	87	nmod	_	_
85	tutti	tutti	_	PI	_	87	nsubj	_	_
86	ci	ci	_	PC	_	87	advmod	_	_
87	spaventammo	spaventare	_	V	_	80	ccomp	_	_
88	molto	molto	_	B	_	87	advmod	_	_
89	.	[PUNCT]	_	FS	_	80	punct	_	_

90	Tutti	tutti	_	PI	_	91	nsubj	_	_
91	pensammo	pensare	_	V	_	0	root	_	_
92	che	che	_	CS	_	97	mark	_	_
93	fosse	essere	_	VA	_	97	aux	_	_
94	stato	essere	_	V	_	97	cop	_	_
95	molto	molto	_	B	_	96	advmod	_	_
96	piú	più	_	B	_	97	advmod	_	_
97	grave	grave	_	A	_	91	ccomp	_	_
98	.	[PUNCT]	_	FS	_	91	punct	_	_

99	Questo	questo	_	DD	_	100	det	_	_
100	incidente	incidente	_	S	_	103	nsubj	_	_
101	mi	mi	_	PC	_	103	dobj	_	_
102	ha	avere	_	VA	_	103	aux	_	_
103	insegniato	insegniato	_	V	_	0	root	_	_
104	anche	anche	_	B	_	106	advmod	_	_
105	nei	nel	_	E+RD	_	106	case	_	_
106	posti	posto	_	S	_	103	nmod	_	_
107	piú	più	_	A	_	106	amod	_	_
108	tranquilli	tranquillo	_	A	_	106	amod	_	_
109	di	di	_	E	_	110	mark	_	_
110	fare	fare	_	V	_	106	acl	_	_
111	sempre	sempre	_	B	_	112	advmod	_	_
112	tantissima	tanto	_	A	_	113	amod	_	_
113	attenzione	attenzione	_	S	_	110	dobj	_	_
114	.	[PUNCT]	_	FS	_	103	punct	_	_


1	lepre	lepre	_	S	_	0	root	_	_
2	o	o	_	CC	_	1	cc	_	_
3	tartaruga	tartaruga	_	S	_	1	conj	_	_
4	Spesso	spesso	_	B	_	5	advmod	_	_
5	sono	essere	_	V	_	3	acl	_	_
6	come	come	_	E	_	8	case	_	_
7	la	la	_	RD	_	8	det	_	_
8	tartaruga	tartaruga	_	S	_	5	nmod	_	_
9	,	[PUNCT]	_	FF	_	5	punct	_	_
10	credo	credere	_	V	_	5	advcl	_	_
11	che	che	_	CS	_	17	mark	_	_
12	le	la	_	RD	_	13	det	_	_
13	cose	cosa	_	S	_	17	nsubj	_	_
14	che	che	_	PR	_	15	dobj	_	_
15	faccio	fare	_	V	_	13	acl:relcl	_	_
16	sono	essere	_	V	_	17	cop	_	_
17	difficili	difficile	_	A	_	10	ccomp	_	_
18	e	e	_	CC	_	17	cc	_	_
19	ogni	ogni	_	DI	_	20	det	_	_
20	giorno	giorno	_	S	_	21	nmod	_	_
21	imparo	imparare	_	V	_	17	conj	_	_
22	bene	bene	_	B	_	21	advmod	_	_
23	e	e	_	CC	_	10	cc	_	_
24	poi	poi	_	B	_	25	advmod	_	_
25	ricevo	ricevere	_	V	_	10	conj	_	_
26	un	un	_	RI	_	28	det	_	_
27	buon	buon	_	A	_	28	amod	_	_
28	risultato	risultato	_	S	_	25	dobj	_	_
29	.	[PUNCT]	_	FS	_	1	punct	_	_

30	Per	per	_	E	_	31	case	_	_
31	esempio	esempio	_	S	_	35	nmod	_	_
32	:	[PUNCT]	_	FC	_	31	punct	_	_
33	In	In	_	SP	_	35	nsubj	_	_
34	Berna	Berna	_	SP	_	33	name	_	_
35	avevo	avere	_	V	_	45	advcl	_	_
36	un	un	_	RI	_	37	det	_	_
37	test	test	_	S	_	35	dobj	_	_
38	di	di	_	E	_	39	case	_	_
39	salfeggio	salfeggio	_	S	_	37	nmod	_	_
40	,	[PUNCT]	_	FF	_	35	punct	_	_
41	ogni	ogni	_	DI	_	42	det	_	_
42	giorno	giorno	_	S	_	45	nsubj	_	_
43	mi	mi	_	PC	_	45	expl	_	_
44	sono	essere	_	VA	_	45	aux	_	_
45	preparato	preparare	_	V	_	0	root	_	_
46	benissimo	benissimo	_	B	_	45	advmod	_	_
47	e	e	_	CC	_	45	cc	_	_
48	al	al	_	E+RD	_	49	case	_	_
49	test	test	_	S	_	52	nmod	_	_
50	mi	mi	_	PC	_	52	iobj	_	_
51	hanno	avere	_	VA	_	52	aux	_	_
52	dato	dare	_	V	_	45	conj	_	_
53	la	la	_	RD	_	54	det	_	_
54	nota	nota	_	S	_	52	dobj	_	_
55	6	6	_	N	_	54	nummod	_	_
56	!	[PUNCT]	_	FS	_	45	punct	_	_

57	Poche	poco	_	DI	_	58	det	_	_
58	volte	volta	_	S	_	61	nsubj	_	_
59	sono	essere	_	V	_	61	cop	_	_
60	la	la	_	RD	_	61	det	_	_
61	lepre	lepre	_	S	_	0	root	_	_
62	e	e	_	CC	_	61	cc	_	_
63	credo	credere	_	V	_	61	conj	_	_
64	che	che	_	CS	_	82	mark	_	_
65	ho	avere	_	V	_	82	advcl	_	_
66	sufficiente	sufficiente	_	A	_	67	amod	_	_
67	tempo	tempo	_	S	_	65	dobj	_	_
68	ma	ma	_	CC	_	65	cc	_	_
69	in	in	_	E	_	70	case	_	_
70	realtà	realtà	_	S	_	72	nmod	_	_
71	ci	ci	_	PC	_	72	advmod	_	_
72	resta	restare	_	V	_	65	conj	_	_
73	un	un	_	RI	_	74	det	_	_
74	bel	bel	_	A	_	72	xcomp	_	_
75	poco	poco	_	B	_	74	advmod	_	_
76	e	e	_	CC	_	74	cc	_	_
77	quando	quando	_	CS	_	79	mark	_	_
78	sta	stare	_	VA	_	79	aux	_	_
79	finendo	finire	_	V	_	74	conj	_	_
80	il	il	_	RD	_	81	det	_	_
81	tempo	tempo	_	S	_	79	nsubj	_	_
82	vedo	vedere	_	V	_	63	ccomp	_	_
83	che	che	_	CS	_	86	mark	_	_
84	non	non	_	BN	_	86	neg	_	_
85	ho	avere	_	VA	_	86	aux	_	_
86	fatto	fare	_	V	_	82	ccomp	_	_
87	sufficiente	sufficiente	_	A	_	86	xcomp	_	_
88	.	[PUNCT]	_	FS	_	61	punct	_	_

89	Secondo	secondo	_	E	_	90	case	_	_
90	me	me	_	PE	_	96	nmod	_	_
91	il	il	_	RD	_	92	det	_	_
92	carattere	carattere	_	S	_	96	nsubj	_	_
93	più	più	_	B	_	94	advmod	_	_
94	intelligente	intelligente	_	A	_	92	amod	_	_
95	è	essere	_	V	_	96	cop	_	_
96	quello	quello	_	PD	_	0	root	_	_
97	della	della	_	E+RD	_	98	case	_	_
98	tartaruga	tartaruga	_	S	_	96	nmod	_	_
99	.	[PUNCT]	_	FS	_	96	punct	_	_

100	Voglio	volere	_	VM	_	101	aux	_	_
101	essere	essere	_	V	_	0	root	_	_
102	come	come	_	E	_	104	case	_	_
103	la	la	_	RD	_	104	det	_	_
104	tartaruga	tartaruga	_	S	_	101	nmod	_	_
105	!	[PUNCT]	_	FS	_	101	punct	_	_


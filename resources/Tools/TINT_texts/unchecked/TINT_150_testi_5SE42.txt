1	La	la	_	RD	_	4	det	_	_
2	mia	mio	_	AP	_	4	det:poss	_	_
3	prima	primo	_	NO	_	4	amod	_	_
4	corsa	corsa	_	S	_	14	nsubj	_	_
5	in	in	_	E	_	6	case	_	_
6	Ticino	Ticino	_	SP	_	4	nmod	_	_
7	Sabato	sabato	_	S	_	6	nmod	_	_
8	scorso	scorso	_	A	_	7	amod	_	_
9	durante	durante	_	E	_	11	case	_	_
10	le	la	_	RD	_	11	det	_	_
11	vacanze	vacanza	_	S	_	4	nmod	_	_
12	sono	essere	_	VA	_	14	aux	_	_
13	andato	andare	_	VA	_	14	expl	_	_
14	ha	avere	_	V	_	0	root	_	_
15	un	un	_	RI	_	16	det	_	_
16	allenamento	allenamento	_	S	_	18	nsubj	_	_
17	poi	poi	_	B	_	18	advmod	_	_
18	ha	avere	_	V	_	14	parataxis	_	_
19	una	una	_	RI	_	20	det	_	_
20	corsa	corsa	_	S	_	18	dobj	_	_
21	.	[PUNCT]	_	FS	_	14	punct	_	_

22	Mi	mi	_	PC	_	24	expl	_	_
23	sono	essere	_	VA	_	24	aux	_	_
24	alzato	alzare	_	V	_	0	root	_	_
25	verso	verso	_	E	_	27	case	_	_
26	le	la	_	RD	_	27	det	_	_
27	8	8	_	N	_	24	nummod	_	_
28	:	[PUNCT]	_	FC	_	24	punct	_	_
29	30	30	_	N	_	42	nummod	_	_
30	per	per	_	E	_	31	mark	_	_
31	andare	andare	_	V	_	29	advcl	_	_
32	ad	ad	_	E	_	34	case	_	_
33	un	un	_	RI	_	34	det	_	_
34	allenamento	allenamento	_	S	_	31	nmod	_	_
35	con	con	_	E	_	37	case	_	_
36	il	il	_	RD	_	37	det	_	_
37	Team	Team	_	SP	_	34	nmod	_	_
38	Tri	Tri	_	SP	_	37	name	_	_
39	B	B	_	SP	_	37	name	_	_
40	,	[PUNCT]	_	FF	_	29	punct	_	_
41	abbiamo	avere	_	VA	_	42	aux	_	_
42	fatto	fare	_	V	_	24	parataxis	_	_
43	i	il	_	RD	_	45	det	_	_
44	tre	tre	_	N	_	45	nummod	_	_
45	sport	sport	_	S	_	42	dobj	_	_
46	quindi	quindi	_	B	_	47	advmod	_	_
47	nuoto	nuoto	_	A	_	45	amod	_	_
48	,	[PUNCT]	_	FF	_	45	punct	_	_
49	corsa	corsa	_	S	_	45	conj	_	_
50	e	e	_	CC	_	45	cc	_	_
51	ciclismo	ciclismo	_	S	_	45	conj	_	_
52	,	[PUNCT]	_	FF	_	24	punct	_	_
53	appena	appena	_	B	_	54	advmod	_	_
54	finito	finire	_	V	_	24	advcl	_	_
55	l'	l'	_	RD	_	56	det	_	_
56	allenamento	allenamento	_	S	_	59	nsubj	_	_
57	mi	mi	_	PC	_	59	dobj	_	_
58	sono	essere	_	VA	_	59	aux	_	_
59	recato	recare	_	V	_	54	xcomp	_	_
60	ha	avere	_	VA	_	61	aux	_	_
61	mendrisio	mendrisio	_	V	_	59	xcomp	_	_
62	per	per	_	E	_	64	case	_	_
63	una	una	_	RI	_	64	det	_	_
64	gara	gara	_	S	_	61	nmod	_	_
65	che	che	_	PR	_	67	nsubj	_	_
66	si	si	_	PC	_	67	dobj	_	_
67	disputava	disputare	_	V	_	64	acl:relcl	_	_
68	dentro	dentro	_	B	_	67	advmod	_	_
69	il	il	_	RD	_	70	det	_	_
70	manicomio	manicomio	_	S	_	72	nsubj	_	_
71	ho	avere	_	VA	_	72	aux	_	_
72	incontrato	incontrare	_	V	_	67	ccomp	_	_
73	dei	del	_	E+RD	_	75	case	_	_
74	miei	mio	_	AP	_	75	det:poss	_	_
75	amici	amico	_	S	_	72	nmod	_	_
76	uno	un	_	RI	_	77	det	_	_
77	era	era	_	S	_	72	dobj	_	_
78	molto	molto	_	B	_	79	advmod	_	_
79	nervoso	nervoso	_	A	_	77	amod	_	_
80	e	e	_	CC	_	77	cc	_	_
81	pensava	pensare	_	V	_	77	conj	_	_
82	di	di	_	E	_	84	mark	_	_
83	non	non	_	BN	_	84	neg	_	_
84	riuscire	riuscire	_	V	_	81	xcomp	_	_
85	ad	ad	_	E	_	86	mark	_	_
86	arrivare	arrivare	_	V	_	84	xcomp	_	_
87	bene	bene	_	B	_	86	advmod	_	_
88	.	[PUNCT]	_	FS	_	24	punct	_	_

89	Io	io	_	PE	_	92	nsubj	_	_
90	ero	essere	_	V	_	92	cop	_	_
91	anche	anche	_	B	_	92	advmod	_	_
92	nervoso	nervoso	_	A	_	0	root	_	_
93	ma	ma	_	CC	_	92	cc	_	_
94	al	al	_	E+RD	_	95	case	_	_
95	contrario	contrario	_	S	_	98	nmod	_	_
96	di	di	_	E	_	97	case	_	_
97	lui	lui	_	PE	_	95	nmod	_	_
98	sapevo	sapere	_	V	_	92	conj	_	_
99	di	di	_	E	_	100	mark	_	_
100	riuscire	riuscire	_	V	_	98	xcomp	_	_
101	ad	ad	_	E	_	102	mark	_	_
102	arrivare	arrivare	_	V	_	100	xcomp	_	_
103	bene	bene	_	B	_	102	advmod	_	_
104	.	[PUNCT]	_	FS	_	92	punct	_	_

105	Alla	alla	_	E+RD	_	106	case	_	_
106	partenza	partenza	_	S	_	109	nmod	_	_
107	sono	essere	_	VA	_	109	aux	_	_
108	dovuto	dovere	_	VM	_	109	aux	_	_
109	partire	partire	_	V	_	0	root	_	_
110	un	un	_	RI	_	111	det	_	_
111	po'	po'	_	B	_	113	advmod	_	_
112	in	in	_	E	_	113	case	_	_
113	fondo	fondo	_	S	_	109	nmod	_	_
114	ma	ma	_	CC	_	109	cc	_	_
115	sono	essere	_	VA	_	116	aux	_	_
116	riuscito	riuscire	_	V	_	109	conj	_	_
117	subito	subito	_	B	_	116	advmod	_	_
118	ha	avere	_	VA	_	119	aux	_	_
119	rimontarli	rimontare	_	V+PC	_	116	xcomp	_	_
120	e	e	_	CC	_	116	cc	_	_
121	ha	avere	_	V	_	122	aux	_	_
122	superarli	superare	_	V+PC	_	116	conj	_	_
123	ma	ma	_	CC	_	116	cc	_	_
124	ero	essere	_	V	_	116	conj	_	_
125	troppo	troppo	_	B	_	126	advmod	_	_
126	preso	prendere	_	V	_	124	advcl	_	_
127	dalla	dalla	_	E+RD	_	128	case	_	_
128	felicita	felicitare	_	S	_	150	nmod	_	_
129	di	di	_	E	_	130	mark	_	_
130	essere	essere	_	V	_	150	cop	_	_
131	con	con	_	E	_	133	case	_	_
132	i	il	_	RD	_	133	det	_	_
133	primi	primo	_	NO	_	130	nmod	_	_
134	di	di	_	E	_	136	case	_	_
135	un	un	_	RI	_	136	det	_	_
136	anno	anno	_	S	_	133	nmod	_	_
137	in	in	_	E	_	138	case	_	_
138	più	più	_	B	_	150	advmod	_	_
139	che	che	_	CS	_	138	mwe	_	_
140	nera	nero	_	A	_	142	amod	_	_
141	anche	anche	_	B	_	142	advmod	_	_
142	uno	uno	_	PI	_	150	nsubj	_	_
143	della	della	_	E+RD	_	145	case	_	_
144	mia	mio	_	AP	_	145	det:poss	_	_
145	età	età	_	S	_	142	nmod	_	_
146	,	[PUNCT]	_	FF	_	142	punct	_	_
147	io	io	_	PE	_	148	nsubj	_	_
148	ero	essere	_	V	_	150	cop	_	_
149	troppo	troppo	_	B	_	150	advmod	_	_
150	felice	felice	_	A	_	126	ccomp	_	_
151	e	e	_	CC	_	150	cc	_	_
152	cercando	cercare	_	V	_	160	advcl	_	_
153	di	di	_	E	_	154	mark	_	_
154	rimontare	rimontare	_	V	_	160	advcl	_	_
155	i	il	_	RD	_	157	det	_	_
156	2	2	_	N	_	157	nummod	_	_
157	primi	primo	_	NO	_	160	nsubj	_	_
158	mi	mi	_	PC	_	160	expl	_	_
159	sono	essere	_	VA	_	160	aux	_	_
160	stancato	stancare	_	V	_	150	conj	_	_
161	troppo	troppo	_	B	_	160	advmod	_	_
162	e	e	_	CC	_	126	cc	_	_
163	quelli	quelli	_	PD	_	169	nsubj	_	_
164	del	del	_	E+RD	_	166	case	_	_
165	mio	mio	_	AP	_	166	det:poss	_	_
166	gruppetto	gruppetto	_	S	_	163	nmod	_	_
167	mi	mi	_	PC	_	169	dobj	_	_
168	hanno	avere	_	VA	_	169	aux	_	_
169	staccato	staccare	_	V	_	126	conj	_	_
170	.	[PUNCT]	_	FS	_	109	punct	_	_

171	Nonostante	nonostante	_	E	_	172	case	_	_
172	tutto	tutto	_	PI	_	174	nmod	_	_
173	sono	essere	_	VA	_	174	aux	_	_
174	arrivato	arrivare	_	V	_	0	root	_	_
175	10	10	_	N	_	178	nummod	_	_
176	su	su	_	E	_	177	case	_	_
177	27	27	_	N	_	178	nummod	_	_
178	corridori	corridore	_	S	_	174	nsubj	_	_
179	.	[PUNCT]	_	FS	_	174	punct	_	_

180	Le	la	_	RD	_	181	det	_	_
181	morali	morale	_	A	_	182	nsubj	_	_
182	sono	essere	_	V	_	0	root	_	_
183	che	che	_	CS	_	187	mark	_	_
184	non	non	_	BN	_	187	neg	_	_
185	devi	deviare	_	VM	_	187	aux	_	_
186	mai	mai	_	B	_	187	advmod	_	_
187	pensare	pensare	_	V	_	182	ccomp	_	_
188	di	di	_	E	_	189	mark	_	_
189	arrivare	arrivare	_	V	_	187	ccomp	_	_
190	male	male	_	B	_	189	advmod	_	_
191	perché	perché	_	CS	_	194	mark	_	_
192	se'	se'	_	SP	_	194	nsubj	_	_
193	nò	nò	_	PC	_	194	expl	_	_
194	arrivi	arrivare	_	V	_	189	advcl	_	_
195	veramente	veramente	_	B	_	196	advmod	_	_
196	male	male	_	B	_	194	advmod	_	_
197	.	[PUNCT]	_	FS	_	182	punct	_	_

198	E	e	_	CC	_	0	root	_	_
199	la	la	_	RD	_	201	det	_	_
200	seconda	secondo	_	NO	_	201	amod	_	_
201	morale	morale	_	S	_	198	nsubj	_	_
202	e	e	_	CC	_	201	cc	_	_
203	che	che	_	PR	_	207	nsubj	_	_
204	non	non	_	BN	_	207	neg	_	_
205	devi	deviare	_	VM	_	207	aux	_	_
206	mai	mai	_	B	_	207	advmod	_	_
207	farti	fare	_	V+PC	_	201	acl:relcl	_	_
208	prendere	prendere	_	V	_	214	advcl	_	_
209	dalla	dalla	_	E+RD	_	210	case	_	_
210	felicità	felicità	_	S	_	208	nmod	_	_
211	se'	se'	_	A	_	210	amod	_	_
212	nò	nò	_	PE	_	214	nsubj	_	_
213	non	non	_	BN	_	214	neg	_	_
214	ragonerai	ragonerai	_	V	_	207	advcl	_	_
215	più	più	_	B	_	214	advmod	_	_
216	e	e	_	CC	_	214	cc	_	_
217	tirerai	tirare	_	V	_	214	conj	_	_
218	sempre	sempre	_	B	_	217	advmod	_	_
219	te	te	_	PE	_	214	nmod	_	_
220	,	[PUNCT]	_	FF	_	214	punct	_	_
221	arriverai	arrivare	_	V	_	214	conj	_	_
222	senpre	senpre	_	V	_	221	xcomp	_	_
223	10	10	_	N	_	222	dobj	_	_
224	su	su	_	E	_	225	case	_	_
225	27	27	_	N	_	223	nmod	_	_
226	e	e	_	CC	_	214	cc	_	_
227	vincere	vincere	_	V	_	231	csubj	_	_
228	sarà	essere	_	V	_	231	cop	_	_
229	veramente	veramente	_	B	_	231	advmod	_	_
230	molto	molto	_	B	_	231	advmod	_	_
231	faticoso	faticoso	_	A	_	214	conj	_	_
232	devi	deviare	_	VM	_	233	aux	_	_
233	diventare	diventare	_	V	_	231	xcomp	_	_
234	sempre	sempre	_	B	_	233	advmod	_	_
235	più	più	_	B	_	236	advmod	_	_
236	saggo	saggo	_	A	_	233	xcomp	_	_
237	e	e	_	CC	_	236	cc	_	_
238	furbo	furbo	_	A	_	236	conj	_	_
239	se	se	_	CS	_	241	mark	_	_
240	vuoi	volere	_	VM	_	241	aux	_	_
241	vincere	vincere	_	V	_	233	advcl	_	_
242	.	[PUNCT]	_	FS	_	198	punct	_	_


Il	ART	il	f ART
ragazzino	NOUN	ragazzino	f NOUN
cafone	ADJ	cafone	f ADJ NOUN
Un	VER:fin	<unknown>	s NPR VER:fin
giorno	NOUN	giorno	f NOUN
dopo	PRE	dopo	f ADJ ADV CON PRE
la	ART	la	f ART CLI
scuola	NOUN	scuola	f NOUN
stavo	VER:fin	stare	f VER:fin
giocando	VER:geru	giocare	f VER:geru
a	PRE	a	f PRE
calcio	NOUN	calcio	f NOUN VER:fin
con	PRE	con	f PRE
i	ART	il	f ART
miei	DET:poss	mio	f DET:poss PRO:poss
amici	NOUN	amico	f ADJ NOUN
e	CON	e	f CON
visto	NOUN	visto	f NOUN VER:fin
che	CHE	che	f CHE CON DET:wh
avevo	VER:fin	avere	f AUX:fin VER:fin
un	ART	un	f ART
gipfel	NOUN	<unknown>	s NOUN NPR
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
ho	AUX:fin	avere	f AUX:fin VER:fin
offerto	VER:ppast	offrire	f VER:ppast
ai	ARTPRE	al	f ARTPRE
miei	DET:poss	mio	f DET:poss PRO:poss
amici	NOUN	amico	f ADJ NOUN
che	CHE	che	f CHE CON DET:wh
mi	CLI	mi	f CLI
hanno	AUX:fin	avere	f AUX:fin VER:fin
ringraziato	VER:ppast	ringraziare	f VER:ppast
e	CON	e	f CON
poi	ADV	poi	f ADV
se	CON	se	f CLI CON
ne	CLI	ne	f CLI
sono	AUX:fin	essere	f AUX:fin VER:fin
andati	VER:ppast	andare	f ADJ VER:ppast
.	SENT	.	f SENT
Visto	NOUN	visto	f NOUN VER:fin
che	CHE	che	f CHE CON DET:wh
non	NEG	non	f NEG
avevo	VER:fin	avere	f AUX:fin VER:fin
più	ADV	più	f ADV
un	ART	un	f ART
pallone	NOUN	pallone	f NOUN
con	PRE	con	f PRE
cui	WH	cui	f WH
giocare	VER:infi	giocare	f VER:infi
sono	AUX:fin	essere	f AUX:fin VER:fin
tornato	VER:ppast	tornare	f VER:ppast
a	PRE	a	f PRE
casa	NOUN	casa	f NOUN
per	PRE	per	f PRE
prendere	VER:infi	prendere	f VER:infi
il	ART	il	f ART
mio	DET:poss	mio	f DET:poss PRO:poss
pallone	NOUN	pallone	f NOUN
e	CON	e	f CON
poi	ADV	poi	f ADV
sono	AUX:fin	essere	f AUX:fin VER:fin
tornato	VER:ppast	tornare	f VER:ppast
a	PRE	a	f PRE
scuola	NOUN	scuola	f NOUN
a	PRE	a	f PRE
giocare	VER:infi	giocare	f VER:infi
.	SENT	.	f SENT
Mentre	CON	mentre	f CON
stavo	VER:fin	stare	f VER:fin
giocando	VER:geru	giocare	f VER:geru
da	PRE	da	f PRE
solo	ADV	solo	f ADJ ADV VER:fin
è	AUX:fin	essere	f AUX:fin VER:fin
arrivato	VER:ppast	arrivare	f ADJ VER:ppast
un	ART	un	f ART
mio	DET:poss	mio	f DET:poss PRO:poss
amico	NOUN	amico	f ADJ NOUN VER:fin
e	CON	e	f CON
mi	CLI	mi	f CLI
ha	AUX:fin	avere	f AUX:fin VER:fin
chiesto	VER:ppast	chiedere	f ADJ VER:ppast
se	CON	se	f CLI CON
poteva	VER2:fin	potere	f VER2:fin VER:fin
giocare	VER:infi	giocare	f VER:infi
e	CON	e	f CON
gli	CLI	gli	f ART CLI
ho	AUX:fin	avere	f AUX:fin VER:fin
detto	VER:ppast	dire	f VER:fin VER:ppast
che	CHE	che	f CHE CON DET:wh
poteva	VER2:fin	potere	f VER2:fin VER:fin
giocare	VER:infi	giocare	f VER:infi
.	SENT	.	f SENT
Abbiamo	AUX:fin	avere	f AUX:fin VER:fin
giocato	VER:ppast	giocare	f VER:ppast
circa	ADV	circa	f ADV PRE
venti	DET:num	venti	f DET:num NOUN PRO:num
minuto	NOUN	minuto	f ADJ NOUN
poi	ADV	poi	f ADV
lui	PRO:pers	lui	f PRO:pers
se	CON	se	f CLI CON
né	CON	né	f CON
dovuto	VER:ppast	dovere	f ADJ VER2:ppre VER:ppast
andare	VER:infi	andare	f VER:infi
.	SENT	.	f SENT
Subito	ADV	subito	f ADJ ADV VER:ppast
dopo	ADV	dopo	f ADJ ADV CON PRE
che	CHE	che	f CHE CON DET:wh
il	ART	il	f ART
mio	DET:poss	mio	f DET:poss PRO:poss
amico	NOUN	amico	f ADJ NOUN VER:fin
era	AUX:fin	essere	f AUX:fin NOUN VER:fin
andato	VER:ppast	andare	f ADJ VER:ppast
via	ADV	via	f ADV NOCAT NOUN PRE
era	AUX:fin	essere	f AUX:fin NOUN VER:fin
arrivato	VER:ppast	arrivare	f ADJ VER:ppast
un	ART	un	f ART
ragazzino	NOUN	ragazzino	f NOUN
che	CHE	che	f CHE CON DET:wh
si	CLI	si	f CLI
faceva	VER:fin	fare	f VER2:fin VER:fin
cafone	ADJ	cafone	f ADJ NOUN
perché	WH	perché	f WH
sapeva	VER:fin	sapere	f VER:fin
giocare	VER:infi	giocare	f VER:infi
benissimo	ADV	benissimo	f ADV
a	PRE	a	f PRE
calcio	NOUN	calcio	f NOUN VER:fin
,	PUN	,	f PUN
almeno	ADV	almeno	f ADV
così	ADV	così	f ADV
diceva	VER:fin	dire	f VER:fin
.	SENT	.	f SENT
Alla	ARTPRE	alla	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
avevo	AUX:fin	avere	f AUX:fin VER:fin
battuto	VER:ppast	battere	f ADJ VER:ppast
tre	DET:num	tre	f DET:num PRO:num
a	PRE	a	f PRE
uno	PRO:indef	uno	f ART NOUN PRO:indef
.	SENT	.	f SENT
Da	PRE	da	f PRE
questa	DET:demo	questo	f DET:demo PRO:demo
situazione	NOUN	situazione	f NOUN
ho	AUX:fin	avere	f AUX:fin VER:fin
imparato	VER:ppast	imparare	f VER:ppast
che	CHE	che	f CHE CON DET:wh
per	PRE	per	f PRE
vincere	VER:infi	vincere	f VER:infi
ci	CLI	ci	f CLI
si	CLI	si	f CLI
deve	VER2:fin	dovere	f VER2:fin VER:fin
impegnare	VER:infi	impegnare	f VER:infi
.	SENT	.	f SENT
Io	PRO:pers	io	f PRO:pers
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
altra	DET:indef	altro	f ADJ DET:indef PRO:indef
volta	NOUN	volta	f ADJ NOUN VER:fin VER:ppast
,	PUN	,	f PUN
volevo	VER2:fin	volere	f VER2:fin VER:fin
giocare	VER:infi	giocare	f VER:infi
a	PRE	a	f PRE
calcio	NOUN	calcio	f NOUN VER:fin
con	PRE	con	f PRE
il	ART	il	f ART
mio	DET:poss	mio	f DET:poss PRO:poss
fratellino	NOUN	fratellino	f NOUN
,	PUN	,	f PUN
e	CON	e	f CON
un	ART	un	f ART
suo	DET:poss	suo	f DET:poss PRO:poss
amico	NOUN	amico	f ADJ NOUN VER:fin
.	SENT	.	f SENT
Quando	WH	quando	f WH
è	AUX:fin	essere	f AUX:fin VER:fin
iniziata	VER:ppast	iniziare	f ADJ VER:ppast
li	CLI	li	f CLI
avevo	AUX:fin	avere	f AUX:fin VER:fin
sottovalutati	VER:ppast	sottovalutare	f VER:ppast
dandoli	VER:geru:cli	dare	f VER:geru:cli
4	NUM	@card@	f NUM
punti	NOUN	punto	f ADJ NOUN VER:fin VER:ppast
di	PRE	di	f PRE
vantaggio	NOUN	vantaggio	f NOUN
e	CON	e	f CON
quindi	ADV	quindi	f ADV
vinsero	VER:fin	vincere	f VER:fin
.	SENT	.	f SENT
Poi	ADV	poi	f ADV
quando	WH	quando	f WH
vinsero	VER:fin	vincere	f VER:fin
hanno	AUX:fin	avere	f AUX:fin VER:fin
cominciato	VER:ppast	cominciare	f VER:ppast
a	PRE	a	f PRE
figheggiarsi	VER:infi:cli	<unknown>	s NOUN VER:infi:cli
,	PUN	,	f PUN
e	CON	e	f CON
quindi	ADV	quindi	f ADV
ho	AUX:fin	avere	f AUX:fin VER:fin
voluto	VER:ppast	volere	f ADJ VER2:ppre VER:ppast
la	ART	la	f ART CLI
rivincita	NOUN	rivincita	f NOUN
.	SENT	.	f SENT
E	CON	e	f CON
la	CLI	la	f ART CLI
vinsi	VER:fin	vincere	f VER:fin
senza	PRE	senza	f CON PRE
sottovalutare	VER:infi	sottovalutare	f VER:infi
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
avversario	NOUN	avversario	f NOUN
.	SENT	.	f SENT
Riflessioni	NOUN	riflessione	f NOUN
:	PUN	:	f PUN
Meglio	ADV	meglio	s NOUN NPR VER:fin
non	NEG	non	f NEG
dare	VER:infi	dare	f VER:infi
punti	NOUN	punto	f ADJ NOUN VER:fin VER:ppast
di	PRE	di	f PRE
vantaggio	NOUN	vantaggio	f NOUN
senò	VER:fin	<unknown>	s VER:fin
è	VER:fin	essere	f AUX:fin VER:fin
sicuro	ADV	sicuro	f ADJ ADV
o	CON	o	f CON
quasi	ADV	quasi	f ADV
sicuro	ADV	sicuro	f ADJ ADV
che	CHE	che	f CHE CON DET:wh
perdi	VER:fin	perdere	f VER:fin
.	SENT	.	f SENT
Insegnamento	NOUN	insegnamento	f NOUN
:	PUN	:	f PUN
Mi	CLI	mi	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
ha	AUX:fin	avere	f AUX:fin VER:fin
insegnato	VER:ppast	insegnare	f VER:ppast
a	PRE	a	f PRE
non	NEG	non	f NEG
sottovalutare	VER:infi	sottovalutare	f VER:infi
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
avversario	NOUN	avversario	f NOUN
,	PUN	,	f PUN
dandoli	VER:geru:cli	dare	f VER:geru:cli
punti	NOUN	punto	f ADJ NOUN VER:fin VER:ppast
di	PRE	di	f PRE
vantaggio	NOUN	vantaggio	f NOUN
.	SENT	.
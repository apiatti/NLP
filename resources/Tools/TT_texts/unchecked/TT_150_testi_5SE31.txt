Un	ART	un	f ART
giorno	NOUN	giorno	f NOUN
,	PUN	,	f PUN
durante	PRE	durante	f PRE VER:ppre
ginnastica	NOUN	ginnastica	f ADJ NOUN
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
percorso	VER:ppast	percorrere	f ADJ NOUN VER:ppast
sessanta	DET:num	sessanta	f DET:num PRO:num
metri	NOUN	metro	f NOUN
a	PRE	a	f PRE
corsa	NOUN	corsa	f ADJ NOUN VER:ppast
contro	PRE	contro	f ADV PRE
un	ART	un	f ART
nostro	DET:poss	nostro	f DET:poss PRO:poss
compagno/a	NOUN	<unknown>	s ADJ NOUN VER:fin VER:infi VER:ppast VER:ppre
.	SENT	.	f SENT
Io	PRO:pers	io	f PRO:pers
pensavo	VER:fin	pensare	f VER:fin
di	PRE	di	f PRE
batterlo	VER:infi:cli	battere	f VER:infi:cli
perché	WH	perché	f WH
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
anno	NOUN	anno	f NOUN
scorso	ADJ	scorso	f ADJ VER:ppast
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
ho	AUX:fin	avere	f AUX:fin VER:fin
battuto	VER:ppast	battere	f ADJ VER:ppast
e	CON	e	f CON
quest’	ADJ	<unknown>	s ADJ NOCAT NOUN VER:fin VER:infi VER:ppast VER:ppre
anno	NOUN	anno	f NOUN
pensavo	VER:fin	pensare	f VER:fin
di	PRE	di	f PRE
batterlo	VER:infi:cli	battere	f VER:infi:cli
,	PUN	,	f PUN
però	ADV	però	f ADV
ero	VER:fin	essere	f AUX:fin AUX:geru AUX:ppre VER:fin
un	ART	un	f ART
po	NOUN	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
’	NOCAT	<unknown>	s NOCAT
teso	VER:ppast	tendere	f ADJ VER:fin VER:ppast
.	SENT	.	f SENT
Alla	ARTPRE	alla	f ARTPRE
partenza	NOUN	partenza	f NOUN
siamo	VER:fin	essere	f AUX:fin VER:fin
partiti	NOUN	partito	f ADJ NOUN VER:ppast
uguali	ADJ	uguale	f ADJ
ma	CON	ma	f CON
poi	ADV	poi	f ADV
lui	PRO:pers	lui	f PRO:pers
è	AUX:fin	essere	f AUX:fin VER:fin
passato	VER:ppast	passare	f ADJ NOUN VER:ppast
in	PRE	in	f PRE
testa	NOUN	testa	f NOUN VER:fin
,	PUN	,	f PUN
alla	ARTPRE	alla	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
mi	CLI	mi	f CLI
ha	AUX:fin	avere	f AUX:fin VER:fin
battuto	VER:ppast	battere	f ADJ VER:ppast
,	PUN	,	f PUN
ma	CON	ma	f CON
io	PRO:pers	io	f PRO:pers
non	NEG	non	f NEG
mi	CLI	mi	f CLI
sono	AUX:fin	essere	f AUX:fin VER:fin
arrabbiato	VER:ppast	arrabbiare	f ADJ NOUN VER:ppast
perché	WH	perché	f WH
è	VER:fin	essere	f AUX:fin VER:fin
mio	DET:poss	mio	f DET:poss PRO:poss
amico	NOUN	amico	f ADJ NOUN VER:fin
e	CON	e	f CON
a	PRE	a	f PRE
me	PRO:pers	me	f CLI PRO:pers
non	NEG	non	f NEG
faceva	VER:fin	fare	f VER2:fin VER:fin
niente	PRO:indef	niente	f ADJ ADV PRO:indef
,	PUN	,	f PUN
tanto	ADV	tanto	f ADJ ADV DET:indef
non	NEG	non	f NEG
si	CLI	si	f CLI
vinceva	VER:fin	vincere	f VER:fin
qualcosa	PRO:indef	qualcosa	f PRO:indef
.	SENT	.	f SENT
Quel	DET:demo	quello	f DET:demo
giorno	NOUN	giorno	f NOUN
ho	AUX:fin	avere	f AUX:fin VER:fin
imparato	VER:ppast	imparare	f VER:ppast
che	CHE	che	f CHE CON DET:wh
non	NEG	non	f NEG
devo	VER2:fin	dovere	f VER2:fin VER:fin
sottovalutare	VER:infi	sottovalutare	f VER:infi
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
avversario	NOUN	avversario	f NOUN
e	CON	e	f CON
ho	AUX:fin	avere	f AUX:fin VER:fin
imparato	VER:ppast	imparare	f VER:ppast
la	ART	la	f ART CLI
lezione	NOUN	lezione	f NOUN

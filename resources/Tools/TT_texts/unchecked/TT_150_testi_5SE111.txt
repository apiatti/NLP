La	ART	la	f ART CLI
partita	NOUN	partita	f ADJ NOUN VER:ppast
di	PRE	di	f PRE
calcio	NOUN	calcio	f NOUN VER:fin
–	NOCAT	<unknown>	s NOCAT
Un	VER:fin	<unknown>	s NPR VER:fin
sabato	NOUN	sabato	f NOUN
mattina	NOUN	mattina	f NOUN
del	ARTPRE	del	f ARTPRE
12	NUM	@card@	f NUM
aprile	NOUN	aprile	f NOUN
la	ART	la	f ART CLI
mia	DET:poss	mio	f DET:poss PRO:poss
squadra	NOUN	squadra	f NOUN VER:fin
di	PRE	di	f PRE
calcio	NOUN	calcio	f NOUN VER:fin
,	PUN	,	f PUN
avevamo	VER:fin	avere	f AUX:fin VER:fin
una	ART	una	f ART PRO:indef
partita	NOUN	partita	f ADJ NOUN VER:ppast
di	PRE	di	f PRE
calcio	NOUN	calcio	f NOUN VER:fin
,	PUN	,	f PUN
il	ART	il	f ART
ritrovo	VER:fin	ritrovare	f VER:fin
era	VER:fin	essere	f AUX:fin NOUN VER:fin
alla	ARTPRE	alla	f ARTPRE
chiesa	NOUN	chiesa	f NOUN
di	PRE	di	f PRE
Gerra	NPR	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
Piano	NPR	<unknown>	s NOUN NPR VER:fin
,	PUN	,	f PUN
solo	ADV	solo	f ADJ ADV VER:fin
che	CHE	che	f CHE CON DET:wh
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
erano	VER:fin	essere	f AUX:fin VER:fin
poche	DET:indef	poco	f ADJ DET:indef PRO:indef
macchine	NOUN	macchina	f NOUN
anzi	ADV	anzi	f ADV
non	NEG	non	f NEG
erano	VER:fin	essere	f AUX:fin VER:fin
abbastanza	ADV	abbastanza	f ADV
quindi	ADV	quindi	f ADV
siamo	AUX:fin	essere	f AUX:fin VER:fin
andati	VER:ppast	andare	f ADJ VER:ppast
in	PRE	in	f PRE
bus	NOUN	bus	f NOUN
.	SENT	.	f SENT
Il	ART	il	f ART
bus	NOUN	bus	f NOUN
era	VER:fin	essere	f AUX:fin NOUN VER:fin
troppo	ADV	troppo	f ADJ ADV DET:indef
presto	ADV	presto	f ADJ ADV VER:fin
e	CON	e	f CON
quindi	ADV	quindi	f ADV
siamo	AUX:fin	essere	f AUX:fin VER:fin
arrivati	VER:ppast	arrivare	f ADJ VER:ppast
prima	ADV	prima	f ADJ ADV
.	SENT	.	f SENT
Quando	WH	quando	f WH
sono	AUX:fin	essere	f AUX:fin VER:fin
arrivati	VER:ppast	arrivare	f ADJ VER:ppast
gli	ART	il	f ART CLI
altri	PRO:indef	altri	f ADJ DET:indef PRO:indef
,	PUN	,	f PUN
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
altra	DET:indef	altro	f ADJ DET:indef PRO:indef
squadra	NOUN	squadra	f NOUN VER:fin
tutti	DET:indef	tutto	f ADJ DET:indef PRO:indef
noi	PRO:pers	noi	f PRO:pers
ci	CLI	ci	f CLI
siamo	AUX:fin	essere	f AUX:fin VER:fin
messi	VER:ppast	mettere	f ADJ NOUN VER:ppast
a	PRE	a	f PRE
ridere	VER:infi	ridere	f VER:infi
AH	NPR	<unknown>	s NOUN NPR
AH	NPR	<unknown>	s NOUN NPR
AH	NPR	<unknown>	s NOUN NPR
AH	NPR	<unknown>	s NOUN NPR
AH	NPR	<unknown>	s NOUN NPR
AH	NPR	<unknown>	s NOUN NPR
AH	NPR	<unknown>	s NOUN NPR
.	SENT	.	f SENT
Ops	NPR	<unknown>	s ADJ NOUN NPR VER:fin
scusate	VER:ppast	scusare	f VER:fin VER:ppast
non	NEG	non	f NEG
mi	CLI	mi	f CLI
sono	AUX:fin	essere	f AUX:fin VER:fin
presentato	VER:ppast	presentare	f VER:ppast
io	PRO:pers	io	f PRO:pers
sono	VER:fin	essere	f AUX:fin VER:fin
Litbgu	NPR	<unknown>	s NPR
un	ART	un	f ART
nome	NOUN	nome	f NOUN
bellissimo	ADJ	bello	f ADJ
io	PRO:pers	io	f PRO:pers
gioco	NOUN	gioco	f NOUN VER:fin
nel	ARTPRE	nel	f ARTPRE
A.C.	NPR	<unknown>	s ADJ NOCAT NPR
BELLINZONA	VER:fin	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
oggi	ADV	oggi	f ADV NOUN
giochiamo	VER:fin	giocare	f VER:fin
contro	PRE	contro	f ADV PRE
il	ART	il	f ART
SASSARIENTE	NPR	<unknown>	s ADJ NOUN NPR VER:ppre
.	SENT	.	f SENT
Sará	NPR	<unknown>	s ADJ NOCAT NPR VER:fin
una	ART	una	f ART PRO:indef
partita	NOUN	partita	f ADJ NOUN VER:ppast
facilisssssssssima	ADJ	<unknown>	s ADJ VER:fin
finceremo	VER:fin	<unknown>	s VER:fin
sicuramente	ADV:mente	sicuramente	f ADV:mente
100	NUM	@card@	f NUM
%	NOCAT	%	f NOCAT
–	NOCAT	<unknown>	s NOCAT
.	SENT	.	f SENT
Ragazzi	NOUN	ragazzo	f NOUN
vincere	VER:infi	vincere	f VER:infi
vincere	VER:infi	vincere	f VER:infi
vincere	VER:infi	vincere	f VER:infi
vincere	VER:infi	vincere	f VER:infi
vincere	VER:infi	vincere	f VER:infi
dobbiamo	VER2:fin	dovere	f VER2:fin VER:fin
vincere	VER:infi	vincere	f VER:infi
disse	VER:fin	dire	f VER:fin
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
allenatore	NOUN	allenatore	f NOUN
del	ARTPRE	del	f ARTPRE
A.C.	NPR	<unknown>	s ADJ NOCAT NPR
BELLINZONA	NPR	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
in	PRE	in	f PRE
spogliatoio	NOUN	spogliatoio	f NOUN
.	SENT	.	f SENT
Oggi	ADV	oggi	f ADV NOUN
commenterà	VER:fin	commentare	f VER:fin
la	ART	la	f ART CLI
partita	NOUN	partita	f ADJ NOUN VER:ppast
:	PUN	:	f PUN
Beppe	NPR	Beppe	f NPR
Bergomi	NPR	Bergomi	f NPR
,	PUN	,	f PUN
sarà	VER:fin	essere	f AUX:fin VER:fin
una	ART	una	f ART PRO:indef
partita	NOUN	partita	f ADJ NOUN VER:ppast
piena	ADJ	pieno	f ADJ NOUN
di	PRE	di	f PRE
goal	NOUN	goal	f NOUN
,	PUN	,	f PUN
si	CLI	si	f CLI
inizia	VER:fin	iniziare	f VER:fin
tra	PRE	tra	f CON PRE
:	PUN	:	f PUN
tre	DET:num	tre	f DET:num PRO:num
,	PUN	,	f PUN
due	DET:num	due	f DET:num PRO:num
,	PUN	,	f PUN
uno	PRO:indef	uno	f ART NOUN PRO:indef
viaaaa	VER:fin	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
é	AUX:fin	essere	f AUX:fin VER:fin
cominciata	VER:ppast	cominciare	f VER:ppast
!	SENT	!	f SENT
dieci	DET:num	dieci	f DET:num PRO:num
minuti	NOUN	minuto	f ADJ NOUN
dopo	CON	dopo	f ADJ ADV CON PRE
non	NEG	non	f NEG
sta	VER:fin	stare	f VER:fin
andando	VER:geru	andare	f VER:geru
come	WH	come	f WH
dovrebbe	VER2:fin	dovere	f VER2:fin VER:fin
andare	VER:infi	andare	f VER:infi
disse	VER:fin	dire	f VER:fin
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
allenatore	NOUN	allenatore	f NOUN
del	ARTPRE	del	f ARTPRE
Belli	NPR	Belli	f NPR
stiamo	VER:fin	stare	f VER:fin
perdento	VER:fin	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
3-0	NUM	@card@	f NUM
disse	VER:fin	dire	f VER:fin
ancora	ADV	ancora	f ADV VER:fin
.	SENT	.	f SENT
Un	ART	un	f ART
ora	NOUN	ora	f ADV NOUN VER:fin
dopo	PRE	dopo	f ADJ ADV CON PRE
,	PUN	,	f PUN
la	ART	la	f ART CLI
partita	NOUN	partita	f ADJ NOUN VER:ppast
é	AUX:fin	essere	f AUX:fin VER:fin
finita	VER:ppast	finire	f ADJ VER:ppast
–	NOCAT	<unknown>	s NOCAT
Sono	NPR	<unknown>	s NPR VER:fin
Beppe	NPR	Beppe	f NPR
.	SENT	.	f SENT
B	NPR	B	f NPR
é	AUX:fin	essere	f AUX:fin VER:fin
stata	VER:ppast	essere|stare	f AUX:ppast VER:ppast
una	ART	una	f ART PRO:indef
partita	NOUN	partita	f ADJ NOUN VER:ppast
ultra	ADJ	ultra	f ADJ NOUN
bella	ADJ	bello	f ADJ NOUN
il	ART	il	f ART
risultato	NOUN	risultato	f NOUN VER:ppast
finale	ADJ	finale	f ADJ NOUN
4-1	NUM	@card@	f NUM
per	PRE	per	f PRE
il	ART	il	f ART
,	PUN	,	f PUN
un	ART	un	f ART
po	NOUN	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
’	NOCAT	<unknown>	s NOCAT
suspence	NOUN	suspence	f NOUN
ha	AUX:fin	avere	f AUX:fin VER:fin
vinto	VER:ppast	vincere	f ADJ VER:ppast
il	ART	il	f ART
Sassariente	NPR	<unknown>	s ADJ NOUN NPR VER:ppre
YEA	NPR	<unknown>	s ADJ NOUN NPR
.	SENT	.	f SENT
Ragazzi	NOUN	ragazzo	f NOUN
vi	CLI	vi	f CLI
ho	AUX:fin	avere	f AUX:fin VER:fin
detto	VER:ppast	dire	f VER:fin VER:ppast
di	PRE	di	f PRE
non	NEG	non	f NEG
sottuvalutare	NOUN	<unknown>	s ADJ NOUN VER:infi
mai	ADV	mai	f ADV
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
avversario	NOUN	avversario	f NOUN
disse	VER:fin	dire	f VER:fin
urlando	VER:geru	urlare	f VER:geru
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
allenatore	NOUN	allenatore	f NOUN
del	ARTPRE	del	f ARTPRE
Belli	NPR	Belli	f NPR
.	SENT	.	f SENT
–	NOCAT	<unknown>	s NOCAT
Qui	VER:fin	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
in	PRE	in	f PRE
linea	NOUN	linea	f NOUN VER:fin
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
é	VER:fin	essere	f AUX:fin VER:fin
Beppe	NPR	Beppe	f NPR
.	SENT	.	f SENT
B	NPR	B	f NPR
oltre	ADV	oltre	f ADV PRE
che	CHE	che	f CHE CON DET:wh
pioveva	VER:fin	piovere	f VER:fin
hanno	AUX:fin	avere	f AUX:fin VER:fin
giocato	VER:ppast	giocare	f VER:ppast
alla	ARTPRE	alla	f ARTPRE
prossima	ADJ	prossimo	f ADJ
ciao	NOCAT	ciao	f NOCAT
!	SENT	!	f SENT

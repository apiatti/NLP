lepre	NOUN	lepre	f NOUN
o	CON	o	f CON
tartaruga	NOUN	tartaruga	f NOUN
Spesso	NPR	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
sono	VER:fin	essere	f AUX:fin VER:fin
come	WH	come	f WH
la	ART	la	f ART CLI
tartaruga	NOUN	tartaruga	f NOUN
,	PUN	,	f PUN
credo	VER:fin	credere	f VER:fin
che	CHE	che	f CHE CON DET:wh
le	ART	la	f ART CLI
cose	NOUN	cosa	f NOUN
che	CHE	che	f CHE CON DET:wh
faccio	VER:fin	fare	f VER2:fin VER:fin
sono	VER:fin	essere	f AUX:fin VER:fin
difficili	ADJ	difficile	f ADJ
e	CON	e	f CON
ogni	DET:indef	ogni	f ADJ DET:indef
giorno	NOUN	giorno	f NOUN
imparo	VER:fin	imparare	f VER:fin
bene	ADV	bene	f ADV NOUN
e	CON	e	f CON
poi	ADV	poi	f ADV
ricevo	VER:fin	ricevere	f VER:fin
un	ART	un	f ART
buon	ADJ	buon	f ADJ
risultato	NOUN	risultato	f NOUN VER:ppast
.	SENT	.	f SENT
Per	PRE	per	f PRE
esempio	NOUN	esempio	f NOUN
:	PUN	:	f PUN
In	PRE	in	s ADJ NPR VER:fin
Berna	NPR	Berna	f NPR
avevo	VER:fin	avere	f AUX:fin VER:fin
un	ART	un	f ART
test	NOUN	test	f NOUN
di	PRE	di	f PRE
salfeggio	NOUN	<unknown>	s NOUN VER:fin
,	PUN	,	f PUN
ogni	DET:indef	ogni	f ADJ DET:indef
giorno	NOUN	giorno	f NOUN
mi	CLI	mi	f CLI
sono	AUX:fin	essere	f AUX:fin VER:fin
preparato	VER:ppast	preparare	f ADJ VER:ppast
benissimo	ADV	benissimo	f ADV
e	CON	e	f CON
al	ARTPRE	al	f ARTPRE
test	NOUN	test	f NOUN
mi	CLI	mi	f CLI
hanno	AUX:fin	avere	f AUX:fin VER:fin
dato	VER:ppast	dare	f ADJ NOUN VER:fin VER:ppast
la	ART	la	f ART CLI
nota	NOUN	nota	f ADJ NOUN VER:fin
6	NUM	@card@	f NUM
!	SENT	!	f SENT
Poche	DET:indef	poco	f ADJ DET:indef PRO:indef
volte	NOUN	volta	f ADJ NOUN VER:ppast
sono	VER:fin	essere	f AUX:fin VER:fin
la	ART	la	f ART CLI
lepre	NOUN	lepre	f NOUN
e	CON	e	f CON
credo	VER:fin	credere	f VER:fin
che	CHE	che	f CHE CON DET:wh
ho	VER:fin	avere	f AUX:fin VER:fin
sufficiente	ADJ	sufficiente	f ADJ
tempo	NOUN	tempo	f NOUN
ma	CON	ma	f CON
in	PRE	in	f PRE
realtà	NOUN	realtà	f NOUN
ci	CLI	ci	f CLI
resta	VER:fin	restare	f VER:fin
un	ART	un	f ART
bel	ADJ	bel	f ADJ
poco	ADV	poco	f ADJ ADV DET:indef PRO:indef
e	CON	e	f CON
quando	WH	quando	f WH
sta	VER:fin	stare	f VER:fin
finendo	VER:geru	finire	f VER:geru
il	ART	il	f ART
tempo	NOUN	tempo	f NOUN
vedo	VER:fin	vedere	f VER:fin
che	CHE	che	f CHE CON DET:wh
non	NEG	non	f NEG
ho	AUX:fin	avere	f AUX:fin VER:fin
fatto	VER:ppast	fare	f NOUN VER2:ppast VER:ppast
sufficiente	ADJ	sufficiente	f ADJ
.	SENT	.	f SENT
Secondo	PRE	secondo	f ADJ NOUN PRE VER:fin
me	PRO:pers	me	f CLI PRO:pers
il	ART	il	f ART
carattere	NOUN	carattere	f NOUN
più	ADV	più	f ADV
intelligente	ADJ	intelligente	f ADJ NOUN
è	VER:fin	essere	f AUX:fin VER:fin
quello	PRO:demo	quello	f DET:demo PRO:demo
della	ARTPRE	della	f ARTPRE
tartaruga	NOUN	tartaruga	f NOUN
.	SENT	.	f SENT
Voglio	VER2:fin	volere	f VER2:fin VER:fin
essere	AUX:infi	essere	f AUX:infi NOUN VER:infi
come	WH	come	f WH
la	ART	la	f ART CLI
tartaruga	NOUN	tartaruga	f NOUN
!	SENT	!	f SENT

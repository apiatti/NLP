La	ART	la	f ART CLI
passeggiata	NOUN	passeggiata	f NOUN VER:ppast
in	PRE	in	f PRE
montagna	NOUN	montagna	f NOUN
Un	VER:fin	<unknown>	s NPR VER:fin
giorno	NOUN	giorno	f NOUN
,	PUN	,	f PUN
con	PRE	con	f PRE
i	ART	il	f ART
miei	DET:poss	mio	f DET:poss PRO:poss
genitori	NOUN	genitore	f NOUN
siamo	AUX:fin	essere	f AUX:fin VER:fin
andati	VER:ppast	andare	f ADJ VER:ppast
a	PRE	a	f PRE
fare	VER:infi	fare	f VER2:infi VER:infi
una	ART	una	f ART PRO:indef
passeggiata	NOUN	passeggiata	f NOUN VER:ppast
in	PRE	in	f PRE
montagna	NOUN	montagna	f NOUN
.	SENT	.	f SENT
Durante	PRE	durante	f PRE VER:ppre
il	ART	il	f ART
tragitto	NOUN	tragitto	f NOUN
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
sentito	VER:ppast	sentire	f ADJ VER:ppast
delle	ARTPRE	della	f ARTPRE
urla	NOUN	urlo	f NOUN VER:fin
,	PUN	,	f PUN
vedendo	VER:geru	vedere	f VER:geru
altra	DET:indef	altro	f ADJ DET:indef PRO:indef
gente	NOUN	gente	f NOUN
che	CHE	che	f CHE CON DET:wh
camminava	VER:fin	camminare	f VER:fin
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
pensato	VER:ppast	pensare	f VER:ppast
che	CHE	che	f CHE CON DET:wh
erano	VER:fin	essere	f AUX:fin VER:fin
loro	PRO:pers	loro	f DET:poss PRO:pers PRO:poss
che	CHE	che	f CHE CON DET:wh
facevano	VER:fin	fare	f VER2:fin VER:fin
quelle	DET:demo	quello	f DET:demo PRO:demo
urla	NOUN	urlo	f NOUN VER:fin
,	PUN	,	f PUN
invece	ADV	invece	f ADV
,	PUN	,	f PUN
alla	ARTPRE	alla	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
del	ARTPRE	del	f ARTPRE
tragitto	NOUN	tragitto	f NOUN
abbiamo	VER:fin	avere	f AUX:fin VER:fin
visto	NOUN	visto	f NOUN VER:fin
un	ART	un	f ART
signore	NOUN	signora|signore	f NOUN
a	PRE	a	f PRE
terra	NOUN	terra	f NOUN
che	CHE	che	f CHE CON DET:wh
si	CLI	si	f CLI
era	AUX:fin	essere	f AUX:fin NOUN VER:fin
fatto	VER:ppast	fare	f NOUN VER2:ppast VER:ppast
un	ART	un	f ART
taglio	NOUN	taglio	f NOUN VER:fin
nel	ARTPRE	nel	f ARTPRE
ginocchio	NOUN	ginocchio	f NOUN
.	SENT	.	f SENT
I	ART	il	f ART
miei	DET:poss	mio	f DET:poss PRO:poss
genitori	NOUN	genitore	f NOUN
gli	CLI	gli	f ART CLI
hanno	AUX:fin	avere	f AUX:fin VER:fin
chiesto	VER:ppast	chiedere	f ADJ VER:ppast
dove	WH	dove	f WH
abitava	VER:fin	abitare	f VER:fin
,	PUN	,	f PUN
il	ART	il	f ART
signore	NOUN	signora|signore	f NOUN
ha	AUX:fin	avere	f AUX:fin VER:fin
detto	VER:ppast	dire	f VER:fin VER:ppast
che	CHE	che	f CHE CON DET:wh
abitava	VER:fin	abitare	f VER:fin
in	PRE	in	f PRE
una	ART	una	f ART PRO:indef
cascina	NOUN	cascina	f NOUN
li	CLI	li	f CLI
vicino	ADJ	vicino	f ADJ NOUN
.	SENT	.	f SENT
I	ART	il	f ART
miei	DET:poss	mio	f DET:poss PRO:poss
genitori	NOUN	genitore	f NOUN
lo	CLI	lo	f ART CLI
hanno	AUX:fin	avere	f AUX:fin VER:fin
accompagnato	VER:ppast	accompagnare	f VER:ppast
alla	ARTPRE	alla	f ARTPRE
sua	DET:poss	suo	f DET:poss PRO:poss
cascina	NOUN	cascina	f NOUN
e	CON	e	f CON
gli	CLI	gli	f ART CLI
hanno	AUX:fin	avere	f AUX:fin VER:fin
disinfettato	VER:ppast	disinfettare	f VER:ppast
e	CON	e	f CON
bendato	VER:ppast	bendare	f VER:ppast
il	ART	il	f ART
ginocchio	NOUN	ginocchio	f NOUN
.	SENT	.	f SENT
Il	ART	il	f ART
signore	NOUN	signora|signore	f NOUN
per	PRE	per	f PRE
ringraziarci	VER:infi:cli	<unknown>	s ADJ VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
della	ARTPRE	della	f ARTPRE
nostra	DET:poss	nostro	f DET:poss PRO:poss
gentilezza	NOUN	gentilezza	f NOUN
ci	CLI	ci	f CLI
ha	AUX:fin	avere	f AUX:fin VER:fin
offerto	VER:ppast	offrire	f VER:ppast
da	PRE	da	f PRE
bere	VER:infi	bere	f VER:infi
.	SENT	.	f SENT
Dopo	CON	dopo	f ADJ ADV CON PRE
esserci	AUX:infi:cli	essere	f AUX:infi:cli VER:infi:cli
salutati	VER:ppast	salutare	f VER:ppast
io	PRO:pers	io	f PRO:pers
e	CON	e	f CON
i	ART	il	f ART
miei	DET:poss	mio	f DET:poss PRO:poss
genitori	NOUN	genitore	f NOUN
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
continuato	VER:ppast	continuare	f ADJ VER:ppast
la	ART	la	f ART CLI
passeggiata	NOUN	passeggiata	f NOUN VER:ppast
.	SENT	.	f SENT
Dopo	PRE	dopo	f ADJ ADV CON PRE
alcune	DET:indef	alcun	f DET:indef PRO:indef
settimane	NOUN	settimana	f NOUN
sempre	ADV	sempre	f ADV
con	PRE	con	f PRE
i	ART	il	f ART
miei	DET:poss	mio	f DET:poss PRO:poss
genitori	NOUN	genitore	f NOUN
,	PUN	,	f PUN
siamo	AUX:fin	essere	f AUX:fin VER:fin
andati	VER:ppast	andare	f ADJ VER:ppast
a	PRE	a	f PRE
trovare	VER:infi	trovare	f VER:infi
il	ART	il	f ART
signore	NOUN	signora|signore	f NOUN
e	CON	e	f CON
a	PRE	a	f PRE
vedere	VER:infi	vedere	f VER:infi
come	WH	come	f WH
stava	VER:fin	stare	f VER:fin
,	PUN	,	f PUN
il	ART	il	f ART
signore	NOUN	signora|signore	f NOUN
molto	ADV	molto	f ADJ ADV DET:indef
felice	ADJ	felice	f ADJ
di	PRE	di	f PRE
rivederci	VER:infi:cli	rivedere	f VER:infi:cli
ci	CLI	ci	f CLI
ha	AUX:fin	avere	f AUX:fin VER:fin
offerto	VER:ppast	offrire	f VER:ppast
da	PRE	da	f PRE
bere	VER:infi	bere	f VER:infi
,	PUN	,	f PUN
e	CON	e	f CON
ci	CLI	ci	f CLI
ha	AUX:fin	avere	f AUX:fin VER:fin
ancora	ADV	ancora	f ADV VER:fin
ringraziato	VER:ppast	ringraziare	f VER:ppast
per	PRE	per	f PRE
averli	AUX:infi:cli	avere	f AUX:infi:cli VER:infi:cli
medicato	VER:ppast	medicare	f ADJ VER:ppast
il	ART	il	f ART
ginocchio	NOUN	ginocchio	f NOUN
.	SENT	.	f SENT
Da	PRE	da	f PRE
questo	DET:demo	questo	f DET:demo PRO:demo
fatto	NOUN	fatto	f NOUN VER2:ppast VER:ppast
accaduto	VER:ppast	accadere	f ADJ VER:ppast
ho	AUX:fin	avere	f AUX:fin VER:fin
imparato	VER:ppast	imparare	f VER:ppast
che	CHE	che	f CHE CON DET:wh
si	CLI	si	f CLI
deve	VER2:fin	dovere	f VER2:fin VER:fin
essere	AUX:infi	essere	f AUX:infi NOUN VER:infi
buoni	ADJ	buono	f ADJ NOUN
e	CON	e	f CON
generosi	ADJ	generoso	f ADJ
anche	ADV	anche	f ADV
con	PRE	con	f PRE
le	ART	la	f ART CLI
persone	NOUN	persona	f NOUN
che	CHE	che	f CHE CON DET:wh
non	NEG	non	f NEG
si	CLI	si	f CLI
conoscono	VER:fin	conoscere	f VER:fin
.	SENT	.	f SENT

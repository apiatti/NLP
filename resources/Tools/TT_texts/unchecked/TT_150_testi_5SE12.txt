Al	ARTPRE	al	f ARTPRE NPR
mare	NOUN	mare	f NOUN
con	PRE	con	f PRE
i	ART	il	f ART
nonni	NOUN	nonno	f NOUN
!	SENT	!	f SENT
Una	ART	una	f ART PRO:indef
mattina	NOUN	mattina	f NOUN
d’	ADJ	<unknown>	s ADJ NOCAT VER:fin
etate	VER:fin	<unknown>	s ADJ NOUN VER:fin VER:ppast
,	PUN	,	f PUN
ad	PRE	ad	f PRE
agosto	NOUN	agosto	f NOUN
,	PUN	,	f PUN
eravamo	VER:fin	essere	f AUX:fin VER:fin
al	ARTPRE	al	f ARTPRE
mare	NOUN	mare	f NOUN
in	PRE	in	f PRE
Portogallo	NPR	Portogallo	f NPR
.	SENT	.	f SENT
Io	PRO:pers	io	f PRO:pers
e	CON	e	f CON
mio	DET:poss	mio	f DET:poss PRO:poss
fratello	NOUN	fratello	f NOUN
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
chiesto	VER:ppast	chiedere	f ADJ VER:ppast
se	CON	se	f CLI CON
potevammo	VER:fin	<unknown>	s VER:fin
andare	VER:infi	andare	f VER:infi
a	PRE	a	f PRE
fare	VER:infi	fare	f VER2:infi VER:infi
il	ART	il	f ART
bagno	NOUN	bagno	f NOUN VER:fin
,	PUN	,	f PUN
ma	CON	ma	f CON
i	ART	il	f ART
nonni	NOUN	nonno	f NOUN
dissero	VER:fin	dire	f VER:fin
di	PRE	di	f PRE
non	NEG	non	f NEG
andare	VER:infi	andare	f VER:infi
perché	WH	perché	f WH
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
erano	VER:fin	essere	f AUX:fin VER:fin
tante	DET:indef	tanto	f ADJ DET:indef PRO:indef
onde	NOUN	onda	f NOUN
.	SENT	.	f SENT
La	ART	la	f ART CLI
sera	NOUN	sera	f NOUN
siamo	AUX:fin	essere	f AUX:fin VER:fin
andati	VER:ppast	andare	f ADJ VER:ppast
in	PRE	in	f PRE
campeggio	NOUN	campeggio	f NOUN VER:fin
e	CON	e	f CON
io	PRO:pers	io	f PRO:pers
mio	DET:poss	mio	f DET:poss PRO:poss
fratello	NOUN	fratello	f NOUN
ci	CLI	ci	f CLI
siamo	AUX:fin	essere	f AUX:fin VER:fin
messi	VER:ppast	mettere	f ADJ NOUN VER:ppast
a	PRE	a	f PRE
giocare	VER:infi	giocare	f VER:infi
.	SENT	.	f SENT
Per	PRE	per	f PRE
esempio	NOUN	esempio	f NOUN
giocavamo	VER:fin	giocare	f VER:fin
a	PRE	a	f PRE
tennis	NOUN	tennis	f NOUN
e	CON	e	f CON
mio	DET:poss	mio	f DET:poss PRO:poss
fratello	NOUN	fratello	f NOUN
si	CLI	si	f CLI
arrabbiava	VER:fin	arrabbiare	f VER:fin
perché	WH	perché	f WH
riuscivo	VER:fin	riuscire	f VER:fin
meglio	ADV	meglio	f ADJ ADV NOUN
di	PRE	di	f PRE
lui	PRO:pers	lui	f PRO:pers
.	SENT	.	f SENT
Poi	ADV	poi	f ADV
siamo	AUX:fin	essere	f AUX:fin VER:fin
andati	VER:ppast	andare	f ADJ VER:ppast
a	PRE	a	f PRE
mangiare	VER:infi	mangiare	f VER:infi
e	CON	e	f CON
dopo	PRE	dopo	f ADJ ADV CON PRE
cena	NOUN	cena	f NOUN VER:fin
ci	CLI	ci	f CLI
siamo	AUX:fin	essere	f AUX:fin VER:fin
gustati	VER:ppast	gustare	f VER:ppast
un	ART	un	f ART
gelato	NOUN	gelato	f ADJ NOUN VER:ppast
e	CON	e	f CON
dopo	PRE	dopo	f ADJ ADV CON PRE
una	ART	una	f ART PRO:indef
passeggiata	NOUN	passeggiata	f NOUN VER:ppast
siamo	AUX:fin	essere	f AUX:fin VER:fin
andati	VER:ppast	andare	f ADJ VER:ppast
a	PRE	a	f PRE
dormire	VER:infi	dormire	f VER:infi
.	SENT	.	f SENT
Il	ART	il	f ART
giorno	NOUN	giorno	f NOUN
dopo	CON	dopo	f ADJ ADV CON PRE
siamo	AUX:fin	essere	f AUX:fin VER:fin
dovuti	VER2:ppast	dovere	f ADJ VER2:ppast VER:ppast
tornare	VER:infi	tornare	f VER:infi
a	PRE	a	f PRE
casa	NOUN	casa	f NOUN
e	CON	e	f CON
arrivati	VER:ppast	arrivare	f ADJ VER:ppast
a	PRE	a	f PRE
casa	NOUN	casa	f NOUN
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
fatto	VER:ppast	fare	f NOUN VER2:ppast VER:ppast
pranzo	NOUN	pranzo	f NOUN VER:fin
,	PUN	,	f PUN
il	ART	il	f ART
pomeriggio	NOUN	pomeriggio	f NOUN
siamo	AUX:fin	essere	f AUX:fin VER:fin
andati	VER:ppast	andare	f ADJ VER:ppast
da	PRE	da	f PRE
nostro	DET:poss	nostro	f DET:poss PRO:poss
cugino	NOUN	cugino	f NOUN
a	PRE	a	f PRE
prendere	VER:infi	prendere	f VER:infi
i	ART	il	f ART
cani	NOUN	cane	f NOUN
e	CON	e	f CON
poi	ADV	poi	f ADV
siamo	AUX:fin	essere	f AUX:fin VER:fin
andati	VER:ppast	andare	f ADJ VER:ppast
in	PRE	in	f PRE
piscina	NOUN	piscina	f NOUN
Lì	VER:fin	<unknown>	s NPR VER:fin
giocato	VER:ppast	giocare	f VER:ppast
e	CON	e	f CON
ci	CLI	ci	f CLI
siamo	AUX:fin	essere	f AUX:fin VER:fin
divertiti	VER:ppast	divertire	f ADJ NOUN VER:ppast
un	ART	un	f ART
mondo	NOUN	mondo	f ADJ NOUN VER:fin
.	SENT	.	f SENT
Nel	ARTPRE	nel	f ARTPRE
tardo	ADJ	tardo	f ADJ ADV VER:fin
poriggio	NOUN	<unknown>	s NOUN VER:fin
é	AUX:fin	essere	f AUX:fin VER:fin
arrivata	VER:ppast	arrivare	f ADJ VER:ppast
nostra	DET:poss	nostro	f DET:poss PRO:poss
cugina	NOUN	cugina	f NOUN
di	PRE	di	f PRE
3	NUM	@card@	f NUM
anni	NOUN	anno	f NOUN
.	SENT	.	f SENT
È	AUX:fin	essere	f AUX:fin VER:fin
voluta	VER:ppast	volere	f ADJ VER2:ppast VER:ppast
venire	VER:infi	venire	f AUX:infi VER:infi
in	PRE	in	f PRE
piscina	NOUN	piscina	f NOUN
,	PUN	,	f PUN
ma	CON	ma	f CON
non	NEG	non	f NEG
sa	VER:fin	sapere	f VER:fin
nuotare	VER:infi	nuotare	f VER:infi
e	CON	e	f CON
ha	AUX:fin	avere	f AUX:fin VER:fin
rischiato	VER:ppast	rischiare	f VER:ppast
di	PRE	di	f PRE
annegare	VER:infi	annegare	f VER:infi
.	SENT	.	f SENT
L’	NPR	<unknown>	s ADJ NOCAT NPR VER:fin
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
aiutata	VER:ppast	aiutare	f VER:ppast
ad	PRE	ad	f PRE
uscire	VER:infi	uscire	f VER:infi
dalla	ARTPRE	dalla	f ARTPRE
piscina	NOUN	piscina	f NOUN
e	CON	e	f CON
le	CLI	le	f ART CLI
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
messo	VER:ppast	mettere	f ADJ VER:ppast
i	ART	il	f ART
braccioli	NOUN	bracciolo	f NOUN
.	SENT	.	f SENT
insegnamento	NOUN	insegnamento	f NOUN
:	PUN	:	f PUN
Mi	CLI	mi	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
ha	AUX:fin	avere	f AUX:fin VER:fin
insegnato	VER:ppast	insegnare	f VER:ppast
ad	PRE	ad	f PRE
avere	VER:infi	avere	f AUX:infi NOUN VER:infi
prudenza	NOUN	prudenza	f NOUN
e	CON	e	f CON
attenzione	NOUN	attenzione	f NOUN
alle	ARTPRE	alla	f ARTPRE
cose	NOUN	cosa	f NOUN
(	PUN	(	f PUN
vedi	VER:fin	vedere	f VER:fin
piscina	NOUN	piscina	f NOUN
)	PUN	)	f PUN
che	CHE	che	f CHE CON DET:wh
capitano	NOUN	capitano	f NOUN VER:fin
e	CON	e	f CON
non	NEG	non	f NEG
arrabbiarsi	VER:infi:cli	arrabbiare	f VER:infi:cli
per	PRE	per	f PRE
niente	PRO:indef	niente	f ADJ ADV PRO:indef
(	PUN	(	f PUN
vedi	VER:fin	vedere	f VER:fin
tennis	NOUN	tennis	f NOUN
)	PUN	)	f PUN
!	SENT	!	f SENT
THE	NPR	The	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
END	NPR	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
.	SENT	.	f SENT

Chi	WH	chi	f WH
tralcia	VER:fin	<unknown>	s VER:fin
puó	ADJ	<unknown>	s ADJ NOCAT VER:fin
fare	VER:infi	fare	f VER2:infi VER:infi
male	ADV	male	f ADJ ADV NOUN
Era	VER:fin	essere	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
il	ART	il	f ART
17	NUM	@card@	f NUM
gennaio	NOUN	gennaio	f NOUN
2012	NUM	@card@	f NUM
quando	WH	quando	f WH
eravamo	VER:fin	essere	f AUX:fin VER:fin
a	PRE	a	f PRE
ricreazione	NOUN	ricreazione	f NOUN
e	CON	e	f CON
giocavamo	VER:fin	giocare	f VER:fin
a	PRE	a	f PRE
calcio	NOUN	calcio	f NOUN VER:fin
quando	WH	quando	f WH
Angielo	NPR	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
mi	CLI	mi	f CLI
tralcia	VER:fin	<unknown>	s VER:fin
.	SENT	.	f SENT
All’	ADJ	<unknown>	s ADJ NOCAT NPR VER:fin
inizio	NOUN	inizio	f NOUN VER:fin
avevo	VER:fin	avere	f AUX:fin VER:fin
tanto	ADV	tanto	f ADJ ADV DET:indef
male	ADV	male	f ADJ ADV NOUN
,	PUN	,	f PUN
ma	CON	ma	f CON
dopo	PRE	dopo	f ADJ ADV CON PRE
quando	WH	quando	f WH
sono	AUX:fin	essere	f AUX:fin VER:fin
tornato	VER:ppast	tornare	f VER:ppast
in	PRE	in	f PRE
classe	NOUN	classe	f NOUN
e	CON	e	f CON
ho	AUX:fin	avere	f AUX:fin VER:fin
messo	VER:ppast	mettere	f ADJ VER:ppast
il	ART	il	f ART
ghiaccio	NOUN	ghiaccio	f NOUN VER:fin
,	PUN	,	f PUN
il	ART	il	f ART
dolore	NOUN	dolore	f NOUN
é	AUX:fin	essere	f AUX:fin VER:fin
diminuito	VER:ppast	diminuire	f VER:ppast
.	SENT	.	f SENT
Alle	ARTPRE	alla	f ARTPRE
11:45	NUM	@card@	f NUM
sono	AUX:fin	essere	f AUX:fin VER:fin
tornato	VER:ppast	tornare	f VER:ppast
a	PRE	a	f PRE
casa	NOUN	casa	f NOUN
,	PUN	,	f PUN
ho	AUX:fin	avere	f AUX:fin VER:fin
fatto	VER:ppast	fare	f NOUN VER2:ppast VER:ppast
un	ART	un	f ART
po	NOUN	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
’	NOCAT	<unknown>	s NOCAT
di	PRE	di	f PRE
fatica	NOUN	fatica	f NOUN VER:fin
peró	ADJ	<unknown>	s ADJ NOCAT VER:fin
alla	ARTPRE	alla	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
sono	AUX:fin	essere	f AUX:fin VER:fin
arrivato	VER:ppast	arrivare	f ADJ VER:ppast
.	SENT	.	f SENT
Dopo	PRE	dopo	f ADJ ADV CON PRE
aver	AUX:infi	avere	f AUX:infi VER:infi
mangiato	VER:ppast	mangiare	f VER:ppast
mi	CLI	mi	f CLI
sono	AUX:fin	essere	f AUX:fin VER:fin
sdraiato	VER:ppast	sdraiare	f ADJ VER:ppast
sul	ARTPRE	sul	f ARTPRE
divano	NOUN	divano	f NOUN
e	CON	e	f CON
ho	AUX:fin	avere	f AUX:fin VER:fin
giocato	VER:ppast	giocare	f VER:ppast
con	PRE	con	f PRE
la	ART	la	f ART CLI
PSP	NPR	<unknown>	s NOUN NPR
.	SENT	.	f SENT
Alle	ARTPRE	alla	f ARTPRE
14:00	NUM	@card@	f NUM
sono	AUX:fin	essere	f AUX:fin VER:fin
andato	VER:ppast	andare	f ADJ VER:ppast
dal	ARTPRE	dal	f ARTPRE
pediatra	NOUN	pediatra	f NOUN
Dr	NPR	<unknown>	s NOUN NPR
.	SENT	.	f SENT
Bianchetti	ADJ	bianchetto	f ADJ
,	PUN	,	f PUN
mi	CLI	mi	f CLI
ha	AUX:fin	avere	f AUX:fin VER:fin
fatto	VER2:ppast	fare	f NOUN VER2:ppast VER:ppast
fare	VER:infi	fare	f VER2:infi VER:infi
dei	ARTPRE	del	f ARTPRE NOUN
movimenti	NOUN	movimento	f NOUN VER:fin
e	CON	e	f CON
camminare	VER:infi	camminare	f VER:infi
.	SENT	.	f SENT
Alla	ARTPRE	alla	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
peró	ADJ	<unknown>	s ADJ NOCAT VER:fin
ci	CLI	ci	f CLI
disse	VER:fin	dire	f VER:fin
che	CHE	che	f CHE CON DET:wh
era	VER:fin	essere	f AUX:fin NOUN VER:fin
solo	ADV	solo	f ADJ ADV VER:fin
il	ART	il	f ART
colpo	NOUN	colpo	f NOUN
.	SENT	.	f SENT
Angelo	NPR	Angelo	f NOUN NPR
era	AUX:fin	essere	f AUX:fin NOUN VER:fin
molto	ADV	molto	f ADJ ADV DET:indef
dispiaciuto	VER:ppast	dispiacere	f ADJ VER:ppast
.	SENT	.	f SENT
Il	ART	il	f ART
giorno	NOUN	giorno	f NOUN
dopo	CON	dopo	f ADJ ADV CON PRE
sono	AUX:fin	essere	f AUX:fin VER:fin
andato	VER:ppast	andare	f ADJ VER:ppast
a	PRE	a	f PRE
scuola	NOUN	scuola	f NOUN
in	PRE	in	f PRE
stampelle	NOUN	stampella	f NOUN
.	SENT	.	f SENT
Giovedí	NOCAT	<unknown>	s ADJ NOCAT NPR VER:fin
19	NUM	@card@	f NUM
gennaio	NOUN	gennaio	f NOUN
sono	AUX:fin	essere	f AUX:fin VER:fin
andato	VER:ppast	andare	f ADJ VER:ppast
a	PRE	a	f PRE
scuola	NOUN	scuola	f NOUN
con	PRE	con	f PRE
una	ART	una	f ART PRO:indef
stampella	NOUN	stampella	f NOUN
,	PUN	,	f PUN
alla	ARTPRE	alla	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
venerdí	ADJ	<unknown>	s ADJ NOCAT VER:fin
sono	AUX:fin	essere	f AUX:fin VER:fin
andato	VER:ppast	andare	f ADJ VER:ppast
a	PRE	a	f PRE
scuola	NOUN	scuola	f NOUN
senza	PRE	senza	f CON PRE
stampelle	NOUN	stampella	f NOUN
.	SENT	.	f SENT
Riflessione	NOUN	riflessione	f NOUN
:	PUN	:	f PUN
Adesso	ADV	adesso	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
ho	AUX:fin	avere	f AUX:fin VER:fin
capito	VER:ppast	capire	f VER:fin VER:ppast
che	CHE	che	f CHE CON DET:wh
non	NEG	non	f NEG
si	CLI	si	f CLI
deve	VER2:fin	dovere	f VER2:fin VER:fin
tralciare	VER:infi	<unknown>	s VER:fin VER:infi VER:ppast VER:ppre
perché	WH	perché	f WH
puoi	VER2:fin	potere	f VER2:fin VER:fin
fare	VER:infi	fare	f VER2:infi VER:infi
male	ADV	male	f ADJ ADV NOUN
.	SENT	.	f SENT

La	ART	la	f ART CLI
corsa	NOUN	corsa	f ADJ NOUN VER:ppast
con	PRE	con	f PRE
Elia	NPR	Elia	f NPR
Un	VER:fin	<unknown>	s NPR VER:fin
giorno	NOUN	giorno	f NOUN
a	PRE	a	f PRE
Chironico	NPR	<unknown>	s ADJ NOUN NPR
stavo	VER:fin	stare	f VER:fin
parlando	VER:geru	parlare	f VER:geru
con	PRE	con	f PRE
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
Elia	NPR	Elia	f NPR
(	PUN	(	f PUN
mio	DET:poss	mio	f DET:poss PRO:poss
fratello	NOUN	fratello	f NOUN
)	PUN	)	f PUN
e	CON	e	f CON
gli	CLI	gli	f ART CLI
ho	AUX:fin	avere	f AUX:fin VER:fin
chiesto	VER:ppast	chiedere	f ADJ VER:ppast
:	PUN	:	f PUN
–	NOCAT	<unknown>	s NOCAT
Vuoi	VER:fin	volere	s NPR VER:fin
fare	VER:infi	fare	f VER2:infi VER:infi
una	ART	una	f ART PRO:indef
gara	NOUN	gara	f NOUN
di	PRE	di	f PRE
corsa	NOUN	corsa	f ADJ NOUN VER:ppast
con	PRE	con	f PRE
me	PRO:pers	me	f CLI PRO:pers
?	SENT	?	f SENT
–	NOCAT	<unknown>	s NOCAT
Lui	VER:fin	<unknown>	s ADJ NOUN NPR VER:fin
all’	ADJ	<unknown>	s ADJ NOCAT VER:fin
inizio	NOUN	inizio	f NOUN VER:fin
rifiutò	VER:fin	rifiutare	f VER:fin
,	PUN	,	f PUN
perché	WH	perché	f WH
sapeva	VER:fin	sapere	f VER:fin
che	CHE	che	f CHE CON DET:wh
avrebbe	AUX:fin	avere	f AUX:fin VER:fin
perso	VER:ppast	perdere	f ADJ VER:ppast
,	PUN	,	f PUN
però	ADV	però	f ADV
dopo	PRE	dopo	f ADJ ADV CON PRE
un	ART	un	f ART
quarto	NOUN	quarto	f ADJ NOUN
d’	ADJ	<unknown>	s ADJ NOCAT VER:fin
ora	ADV	ora	f ADV NOUN VER:fin
disse	VER:fin	dire	f VER:fin
:	PUN	:	f PUN
–	NOCAT	<unknown>	s NOCAT
OK	NPR	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
se	CLI	se	f CLI CON
vuoi	VER2:fin	volere	f VER2:fin VER:fin
facciamo	VER:fin	fare	f VER2:fin VER:fin
la	ART	la	f ART CLI
gara	NOUN	gara	f NOUN
questo	DET:demo	questo	f DET:demo PRO:demo
pomeriggio	NOUN	pomeriggio	f NOUN
,	PUN	,	f PUN
da	PRE	da	f PRE
casa	NOUN	casa	f NOUN
nostra	DET:poss	nostro	f DET:poss PRO:poss
,	PUN	,	f PUN
alla	ARTPRE	alla	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
della	ARTPRE	della	f ARTPRE
strada	NOUN	strada	f NOUN
dove	WH	dove	f WH
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
é	VER:fin	essere	f AUX:fin VER:fin
una	ART	una	f ART PRO:indef
specie	NOUN	specie	f ADV NOUN
di	PRE	di	f PRE
vecchia	ADJ	vecchio	f ADJ NOUN
stalla	NOUN	stalla	f NOUN
,	PUN	,	f PUN
dove	WH	dove	f WH
suonano	VER:fin	suonare	f VER:fin
nostro	DET:poss	nostro	f DET:poss PRO:poss
cugino	NOUN	cugino	f NOUN
e	CON	e	f CON
i	ART	il	f ART
suoi	DET:poss	suo	f DET:poss PRO:poss
amici	NOUN	amico	f ADJ NOUN
.	SENT	.	f SENT
–	NOCAT	<unknown>	s NOCAT
Io	ADJ	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
gli	CLI	gli	f ART CLI
ho	AUX:fin	avere	f AUX:fin VER:fin
risposto	VER:ppast	rispondere	f VER:ppast
:	PUN	:	f PUN
–	NOCAT	<unknown>	s NOCAT
Va	VER:fin	andare	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
bene	ADV	bene	f ADV NOUN
,	PUN	,	f PUN
ma	CON	ma	f CON
tanto	ADV	tanto	f ADJ ADV DET:indef
vinco	VER:fin	vincere	f VER:fin
io	PRO:pers	io	f PRO:pers
perché	WH	perché	f WH
sono	VER:fin	essere	f AUX:fin VER:fin
più	ADV	più	f ADV
grande	ADJ	grande	f ADJ NOUN
.	SENT	.	f SENT
–	NOCAT	<unknown>	s NOCAT
É	NOUN	<unknown>	s ADJ NOUN VER:fin
arrivato	VER:ppast	arrivare	f ADJ VER:ppast
il	ART	il	f ART
pomeriggio	NOUN	pomeriggio	f NOUN
e	CON	e	f CON
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
fatto	VER:ppast	fare	f NOUN VER2:ppast VER:ppast
la	ART	la	f ART CLI
gara	NOUN	gara	f NOUN
visto	NOUN	visto	f NOUN VER:fin
che	CHE	che	f CHE CON DET:wh
ero	VER:fin	essere	f AUX:fin AUX:geru AUX:ppre VER:fin
quasi	ADV	quasi	f ADV
arriva	VER:fin	arrivare	f VER:fin
al	ARTPRE	al	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
ho	AUX:fin	avere	f AUX:fin VER:fin
rallentato	VER:ppast	rallentare	f VER:ppast
,	PUN	,	f PUN
però	ADV	però	f ADV
dal	ARTPRE	dal	f ARTPRE
dietro	ADV	dietro	f ADV PRE
é	AUX:fin	essere	f AUX:fin VER:fin
sbucato	VER:ppast	sbucare	f VER:ppast
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
Elia	NPR	Elia	f NPR
e	CON	e	f CON
mi	CLI	mi	f CLI
ha	AUX:fin	avere	f AUX:fin VER:fin
superata	VER:ppast	superare	f ADJ VER:ppast
visto	NOUN	visto	f NOUN VER:fin
che	CHE	che	f CHE CON DET:wh
avevo	AUX:fin	avere	f AUX:fin VER:fin
rallentato	VER:ppast	rallentare	f VER:ppast
é	AUX:fin	essere	f AUX:fin VER:fin
stato	AUX:ppast	essere	f AUX:ppast NOUN VER:ppast
impossibile	ADJ	impossibile	f ADJ
raggiungerlo	VER:infi:cli	raggiungere	f VER:infi:cli
,	PUN	,	f PUN
cosí	ADJ	<unknown>	s ADJ NOCAT VER:fin
ha	AUX:fin	avere	f AUX:fin VER:fin
vinto	VER:ppast	vincere	f ADJ VER:ppast
.	SENT	.	f SENT
Da	PRE	da	f PRE
questa	DET:demo	questo	f DET:demo PRO:demo
esperienza	NOUN	esperienza	f NOUN
ho	AUX:fin	avere	f AUX:fin VER:fin
imparato	VER:ppast	imparare	f VER:ppast
che	CHE	che	f CHE CON DET:wh
anche	ADV	anche	f ADV
se	CLI	se	f CLI CON
sei	AUX:fin	essere	f AUX:fin DET:num PRO:num VER:fin
più	ADV	più	f ADV
grande	ADJ	grande	f ADJ NOUN
non	NEG	non	f NEG
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
entra	VER:fin	entrare	f VER:fin
,	PUN	,	f PUN
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
importante	ADJ	importante	f ADJ VER:ppre
é	VER:fin	essere	f AUX:fin VER:fin
impegnarsi	VER:infi:cli	impegnare	f VER:infi:cli
sempre	ADV	sempre	f ADV
.	SENT	.	f SENT

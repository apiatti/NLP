La	ART	la	f ART CLI
giornata	NOUN	giornata	f NOUN
senza	PRE	senza	f CON PRE
giacchettino	NOUN	<unknown>	s NOUN VER:fin
Giovedí	ADJ	<unknown>	s ADJ NOCAT NPR
pomeriggio	NOUN	pomeriggio	f NOUN
non	NEG	non	f NEG
volevo	VER2:fin	volere	f VER2:fin VER:fin
mettere	VER:infi	mettere	f VER:infi
il	ART	il	f ART
giacchettino	NOUN	<unknown>	s NOUN VER:fin
perché	WH	perché	f WH
avevo	VER:fin	avere	f AUX:fin VER:fin
caldo	ADJ	caldo	f ADJ NOUN
.	SENT	.	f SENT
Ma	CON	ma	f CON
mia	DET:poss	mio	f DET:poss PRO:poss
mamma	NOUN	mamma	f NOUN
mi	CLI	mi	f CLI
diceva	VER:fin	dire	f VER:fin
di	PRE	di	f PRE
mettere	VER:infi	mettere	f VER:infi
il	ART	il	f ART
giacchettino	NOUN	<unknown>	s NOUN VER:fin
,	PUN	,	f PUN
peró	ADJ	<unknown>	s ADJ NOCAT VER:fin
io	PRO:pers	io	f PRO:pers
non	NEG	non	f NEG
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
ho	AUX:fin	avere	f AUX:fin VER:fin
messo	VER:ppast	mettere	f ADJ VER:ppast
.	SENT	.	f SENT
Così	ADV	così	f ADV
sono	AUX:fin	essere	f AUX:fin VER:fin
andata	VER:ppast	andare	f ADJ VER:ppast
a	PRE	a	f PRE
scuola	NOUN	scuola	f NOUN
col	ARTPRE	col	f ARTPRE
bus	NOUN	bus	f NOUN
.	SENT	.	f SENT
Arrivata	VER:ppast	arrivare	f ADJ VER:ppast
a	PRE	a	f PRE
scuola	NOUN	scuola	f NOUN
sono	AUX:fin	essere	f AUX:fin VER:fin
cominciate	VER:ppast	cominciare	f VER:fin VER:ppast
le	ART	la	f ART CLI
lezioni	NOUN	lezione	f NOUN
.	SENT	.	f SENT
In	PRE	in	f PRE
aula	NOUN	aula	f NOUN VER:fin
stavo	VER:fin	stare	f VER:fin
bene	ADV	bene	f ADV NOUN
,	PUN	,	f PUN
anche	ADV	anche	f ADV
se	CLI	se	f CLI CON
avevo	VER:fin	avere	f AUX:fin VER:fin
solo	ADV	solo	f ADJ ADV VER:fin
la	ART	la	f ART CLI
maglietta	NOUN	maglietta	f NOUN
.	SENT	.	f SENT
Fuori	ADV	fuori	f ADV PRE
faceva	VER:fin	fare	f VER2:fin VER:fin
caldo	ADJ	caldo	f ADJ NOUN
ma	CON	ma	f CON
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
era	VER:fin	essere	f AUX:fin NOUN VER:fin
un	ART	un	f ART
po	NOUN	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
’	NOCAT	<unknown>	s NOCAT
di	PRE	di	f PRE
vento	NOUN	vento	f NOUN
.	SENT	.	f SENT
Perciò	ADV	perciò	f ADV
non	NEG	non	f NEG
sono	AUX:fin	essere	f AUX:fin VER:fin
uscita	VER:ppast	uscire	f NOUN VER:ppast
a	PRE	a	f PRE
ricre	VER:infi:cli	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
perché	WH	perché	f WH
non	NEG	non	f NEG
volevo	VER2:fin	volere	f VER2:fin VER:fin
andare	VER:infi	andare	f VER:infi
fuori	ADV	fuori	f ADV PRE
.	SENT	.	f SENT
Finita	VER:ppast	finire	f ADJ VER:ppast
la	ART	la	f ART CLI
scuola	NOUN	scuola	f NOUN
sono	AUX:fin	essere	f AUX:fin VER:fin
andata	VER:ppast	andare	f ADJ VER:ppast
ad	PRE	ad	f PRE
aspettare	VER:infi	aspettare	f VER:infi
il	ART	il	f ART
bus	NOUN	bus	f NOUN
,	PUN	,	f PUN
ma	CON	ma	f CON
il	ART	il	f ART
bus	NOUN	bus	f NOUN
non	NEG	non	f NEG
arrivava	VER:fin	arrivare	f VER:fin
e	CON	e	f CON
ho	AUX:fin	avere	f AUX:fin VER:fin
dovuto	VER:ppast	dovere	f ADJ VER2:ppre VER:ppast
aspettare	VER:infi	aspettare	f VER:infi
a	PRE	a	f PRE
lungo	ADJ	lungo	f ADJ PRE
.	SENT	.	f SENT
Mentre	CON	mentre	f CON
aspettavo	VER:fin	aspettare	f VER:fin
era	AUX:fin	essere	f AUX:fin NOUN VER:fin
diventato	VER:ppast	diventare	f VER:ppast
freddo	ADJ	freddo	f ADJ NOUN VER:fin
,	PUN	,	f PUN
ho	AUX:fin	avere	f AUX:fin VER:fin
pensato	VER:ppast	pensare	f VER:ppast
che	CHE	che	f CHE CON DET:wh
avrei	AUX:fin	avere	f AUX:fin VER:fin
dovuto	VER:ppast	dovere	f ADJ VER2:ppre VER:ppast
prendere	VER:infi	prendere	f VER:infi
il	ART	il	f ART
giacchettino	NOUN	<unknown>	s NOUN VER:fin
.	SENT	.	f SENT
Il	ART	il	f ART
giorno	NOUN	giorno	f NOUN
dopo	PRE	dopo	f ADJ ADV CON PRE
,	PUN	,	f PUN
la	ART	la	f ART CLI
sera	NOUN	sera	f NOUN
,	PUN	,	f PUN
non	NEG	non	f NEG
stavo	VER:fin	stare	f VER:fin
bene	ADV	bene	f ADV NOUN
,	PUN	,	f PUN
ho	AUX:fin	avere	f AUX:fin VER:fin
misurato	VER:ppast	misurare	f ADJ VER:ppast
la	ART	la	f ART CLI
febbre	NOUN	febbre	f NOUN
e	CON	e	f CON
ne	CLI	ne	f CLI
avevo	VER:fin	avere	f AUX:fin VER:fin
un	ART	un	f ART
po	NOUN	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
’	NOCAT	<unknown>	s NOCAT
.	SENT	.	f SENT
Questo	DET:demo	questo	f DET:demo PRO:demo
fatto	NOUN	fatto	f NOUN VER2:ppast VER:ppast
mi	CLI	mi	f CLI
ha	AUX:fin	avere	f AUX:fin VER:fin
insegnato	VER:ppast	insegnare	f VER:ppast
che	CHE	che	f CHE CON DET:wh
devo	VER2:fin	dovere	f VER2:fin VER:fin
prendere	VER:infi	prendere	f VER:infi
il	ART	il	f ART
giacchettino	NOUN	<unknown>	s NOUN VER:fin
anche	ADV	anche	f ADV
se	CON	se	f CLI CON
fa	ADV	fa	f ADV VER2:fin VER:fin
caldo	ADJ	caldo	f ADJ NOUN
,	PUN	,	f PUN
perché	WH	perché	f WH
se	CLI	se	f CLI CON
cambia	VER:fin	cambiare	f VER:fin
il	ART	il	f ART
tempo	NOUN	tempo	f NOUN
me	PRO:pers	me	f CLI PRO:pers
lo	CLI	lo	f ART CLI
posso	VER2:fin	potere	f VER2:fin VER:fin
mettere	VER:infi	mettere	f VER:infi
.	SENT	.	f SENT

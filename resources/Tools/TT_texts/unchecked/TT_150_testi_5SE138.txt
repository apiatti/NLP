La	ART	la	f ART CLI
mia	DET:poss	mio	f DET:poss PRO:poss
cara	ADJ	caro	f ADJ
e	CON	e	f CON
dolce	ADJ	dolce	f ADJ
nonna	NOUN	nonna	f NOUN
Io	ADJ	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
avevo	VER:fin	avere	f AUX:fin VER:fin
una	ART	una	f ART PRO:indef
nonna	NOUN	nonna	f NOUN
bravissima	ADJ	bravo	f ADJ
,	PUN	,	f PUN
mi	CLI	mi	f CLI
voleva	VER:fin	volere	f VER2:fin VER:fin
un	ART	un	f ART
mondo	NOUN	mondo	f ADJ NOUN VER:fin
di	PRE	di	f PRE
bene	NOUN	bene	f ADV NOUN
,	PUN	,	f PUN
quando	WH	quando	f WH
la	ART	la	f ART CLI
mamma	NOUN	mamma	f NOUN
e	CON	e	f CON
il	ART	il	f ART
papá	ADJ	<unknown>	s ADJ NOCAT VER:fin
lavoravano	VER:fin	lavorare	f VER:fin
io	PRO:pers	io	f PRO:pers
,	PUN	,	f PUN
dopo	PRE	dopo	f ADJ ADV CON PRE
la	ART	la	f ART CLI
scuola	NOUN	scuola	f NOUN
,	PUN	,	f PUN
andavo	VER:fin	andare	f VER:fin
da	PRE	da	f PRE
lei	PRO:pers	lei	f PRO:pers
.	SENT	.	f SENT
Mi	CLI	mi	f CLI
aspettava	VER:fin	aspettare	f VER:fin
sempre	ADV	sempre	f ADV
davanti	ADV	davanti	f ADJ ADV PRE
alla	ARTPRE	alla	f ARTPRE
scuola	NOUN	scuola	f NOUN
,	PUN	,	f PUN
con	PRE	con	f PRE
la	ART	la	f ART CLI
merenda	NOUN	merenda	f NOUN
in	PRE	in	f PRE
automobile	NOUN	automobile	f ADJ NOUN
e	CON	e	f CON
quando	WH	quando	f WH
arrivavo	VER:fin	arrivare	f VER:fin
mi	CLI	mi	f CLI
salutava	VER:fin	salutare	f VER:fin
e	CON	e	f CON
mi	CLI	mi	f CLI
abbracciava	VER:fin	abbracciare	f VER:fin
,	PUN	,	f PUN
sapeva	VER:fin	sapere	f VER:fin
sempre	ADV	sempre	f ADV
cosa	WH	cosa	f NOUN WH
avevo	VER:fin	avere	f AUX:fin VER:fin
voglia	NOUN	voglia	f NOUN VER2:fin VER:fin
di	PRE	di	f PRE
fare	VER:infi	fare	f VER2:infi VER:infi
,	PUN	,	f PUN
mi	CLI	mi	f CLI
faceva	VER2:fin	fare	f VER2:fin VER:fin
sentire	VER:infi	sentire	f VER:infi
speciale	ADJ	speciale	f ADJ
,	PUN	,	f PUN
anche	ADV	anche	f ADV
io	PRO:pers	io	f PRO:pers
e	CON	e	f CON
tutte	DET:indef	tutto	f ADJ DET:indef PRO:indef
le	ART	la	f ART CLI
altre	DET:indef	altro	f ADJ DET:indef PRO:indef
persone	NOUN	persona	f NOUN
le	CLI	le	f ART CLI
volevano	VER:fin	volere	f VER2:fin VER:fin
bene	ADV	bene	f ADV NOUN
.	SENT	.	f SENT
Un	ART	un	f ART
giorno	NOUN	giorno	f NOUN
pero	NOUN	pero	f NOUN
,	PUN	,	f PUN
visto	NOUN	visto	f NOUN VER:fin
che	CHE	che	f CHE CON DET:wh
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
anno	NOUN	anno	f NOUN
precedente	ADJ	precedente	f ADJ NOUN VER:ppre
non	NEG	non	f NEG
era	AUX:fin	essere	f AUX:fin NOUN VER:fin
andata	VER:ppast	andare	f ADJ VER:ppast
neppure	ADV	neppure	f ADV
una	ART	una	f ART PRO:indef
volta	NOUN	volta	f ADJ NOUN VER:fin VER:ppast
dal	ARTPRE	dal	f ARTPRE
medico	NOUN	medico	f ADJ NOUN VER:fin
,	PUN	,	f PUN
é	AUX:fin	essere	f AUX:fin VER:fin
andata	VER:ppast	andare	f ADJ VER:ppast
a	PRE	a	f PRE
fare	VER:infi	fare	f VER2:infi VER:infi
un	ART	un	f ART
controllo	NOUN	controllo	f NOUN VER:fin
,	PUN	,	f PUN
i	ART	il	f ART
risultati	NOUN	risultato	f NOUN VER:ppast
dicevano	VER:fin	dire	f VER:fin
che	CHE	che	f CHE CON DET:wh
aveva	VER:fin	avere	f AUX:fin VER:fin
un	ART	un	f ART
tumore	NOUN	tumore	f NOUN
al	ARTPRE	al	f ARTPRE
polmone	NOUN	polmone	f NOUN
.	SENT	.	f SENT
Il	ART	il	f ART
medico	NOUN	medico	f ADJ NOUN VER:fin
,	PUN	,	f PUN
fortunatamente	ADV:mente	fortunatamente	f ADV:mente
,	PUN	,	f PUN
aveva	AUX:fin	avere	f AUX:fin VER:fin
detto	VER:ppast	dire	f VER:fin VER:ppast
che	CHE	che	f CHE CON DET:wh
era	VER:fin	essere	f AUX:fin NOUN VER:fin
operabile	ADJ	operabile	f ADJ
cosí	ADJ	<unknown>	s ADJ NOCAT VER:fin
,	PUN	,	f PUN
dopo	PRE	dopo	f ADJ ADV CON PRE
una	ART	una	f ART PRO:indef
serie	NOUN	serie	f ADJ NOUN
di	PRE	di	f PRE
esami	NOUN	esame	f NOUN
,	PUN	,	f PUN
venne	AUX:fin	venire	f AUX:fin VER:fin
operata	VER:ppast	operare	f ADJ VER:ppast
.	SENT	.	f SENT
Non	NEG	non	f NEG
aveva	VER:fin	avere	f AUX:fin VER:fin
paura	NOUN	paura	f NOUN
di	PRE	di	f PRE
niente	PRO:indef	niente	f ADJ ADV PRO:indef
,	PUN	,	f PUN
ne	CLI	ne	f CLI
dell’	ADJ	<unknown>	s ADJ NOCAT VER:fin
operazione	NOUN	operazione	f NOUN
ne	CLI	ne	f CLI
di	PRE	di	f PRE
quello	PRO:demo	quello	f DET:demo PRO:demo
che	CHE	che	f CHE CON DET:wh
seguiva	VER:fin	seguire	f VER:fin
.	SENT	.	f SENT
fece	VER:fin	fare	f NOUN VER2:fin VER:fin
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
operazione	NOUN	operazione	f NOUN
,	PUN	,	f PUN
fece	VER:fin	fare	f NOUN VER2:fin VER:fin
la	ART	la	f ART CLI
chemioterapia	NOUN	chemioterapia	f NOUN
,	PUN	,	f PUN
fece	VER:fin	fare	f NOUN VER2:fin VER:fin
le	ART	la	f ART CLI
radiazioni	NOUN	radiazione	f NOUN
(	PUN	(	f PUN
tutto	DET:indef	tutto	f ADJ DET:indef PRO:indef
questo	PRO:demo	questo	f DET:demo PRO:demo
per	PRE	per	f PRE
eliminare	VER:infi	eliminare	f VER:infi
del	ARTPRE	del	f ARTPRE
tutto	PRO:indef	tutto	f ADJ DET:indef PRO:indef
il	ART	il	f ART
cancro	NOUN	cancro	f NOUN
)	PUN	)	f PUN
e	CON	e	f CON
una	ART	una	f ART PRO:indef
settimana	NOUN	settimana	f NOUN
dopo	PRE	dopo	f ADJ ADV CON PRE
le	ART	la	f ART CLI
radiazioni	NOUN	radiazione	f NOUN
il	ART	il	f ART
dottore	NOUN	dottore	f NOUN
ci	CLI	ci	f CLI
disse	VER:fin	dire	f VER:fin
che	CHE	che	f CHE CON DET:wh
delle	ARTPRE	della	f ARTPRE
metastasi	NOUN	metastasi	f NOUN
erano	AUX:fin	essere	f AUX:fin VER:fin
arrivate	VER:ppast	arrivare	f ADJ VER:fin VER:ppast
al	ARTPRE	al	f ARTPRE
cervello	NOUN	cervello	f NOUN
(	PUN	(	f PUN
tre	DET:num	tre	f DET:num PRO:num
)	PUN	)	f PUN
.	SENT	.	f SENT
Io	PRO:pers	io	f PRO:pers
in	PRE	in	f PRE
principio	NOUN	principio	f NOUN VER:fin
credevo	VER:fin	credere	f VER:fin
che	CHE	che	f CHE CON DET:wh
sarebbe	AUX:fin	essere	f AUX:fin VER:fin
guarita	VER:ppast	guarire	f VER:ppast
,	PUN	,	f PUN
ma	CON	ma	f CON
dopo	PRE	dopo	f ADJ ADV CON PRE
qualche	DET:indef	qualche	f ADJ DET:indef
mese	NOUN	mese	f NOUN
la	ART	la	f ART CLI
mamma	NOUN	mamma	f NOUN
mi	CLI	mi	f CLI
fece	VER2:fin	fare	f NOUN VER2:fin VER:fin
capire	VER:infi	capire	f VER:infi
che	CHE	che	f CHE CON DET:wh
non	NEG	non	f NEG
sarebbe	AUX:fin	essere	f AUX:fin VER:fin
mai	ADV	mai	f ADV
guarita	VER:ppast	guarire	f VER:ppast
.	SENT	.	f SENT
Nonostante	PRE	nonostante	f CON PRE
questo	DET:demo	questo	f DET:demo PRO:demo
la	ART	la	f ART CLI
nonna	NOUN	nonna	f NOUN
non	NEG	non	f NEG
aveva	VER:fin	avere	f AUX:fin VER:fin
paura	NOUN	paura	f NOUN
,	PUN	,	f PUN
andava	VER:fin	andare	f VER:fin
avanti	ADV	avanti	f ADJ ADV
felice	ADJ	felice	f ADJ
.	SENT	.	f SENT
Ma	CON	ma	f CON
settimana	NOUN	settimana	f NOUN
dopo	PRE	dopo	f ADJ ADV CON PRE
settimana	NOUN	settimana	f NOUN
continuava	VER:fin	continuare	f VER:fin
a	PRE	a	f PRE
peggiorare	VER:infi	peggiorare	f VER:infi
:	PUN	:	f PUN
prima	ADV	prima	f ADJ ADV
ha	AUX:fin	avere	f AUX:fin VER:fin
perso	VER:ppast	perdere	f ADJ VER:ppast
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
equilibrio	NOUN	equilibrio	f NOUN
,	PUN	,	f PUN
poi	ADV	poi	f ADV
straparlava	VER:fin	straparlare	f VER:fin
e	CON	e	f CON
gli	ART	il	f ART CLI
ultimi	ADJ	ultimo	f ADJ NOUN VER:fin
giorni	NOUN	giorno	f NOUN
era	VER:fin	essere	f AUX:fin NOUN VER:fin
in	PRE	in	f PRE
uno	ART	un	f ART NOUN PRO:indef
stato	NOUN	stato	f AUX:ppast NOUN VER:ppast
di	PRE	di	f PRE
coma	NOUN	coma	f NOUN
,	PUN	,	f PUN
faticava	VER:fin	faticare	f VER:fin
a	PRE	a	f PRE
respirare	VER:infi	respirare	f VER:infi
,	PUN	,	f PUN
fino	PRE	fino	f ADJ PRE
ad	PRE	ad	f PRE
una	ART	una	f ART PRO:indef
mattina	NOUN	mattina	f NOUN
,	PUN	,	f PUN
la	ART	la	f ART CLI
mamma	NOUN	mamma	f NOUN
era	AUX:fin	essere	f AUX:fin NOUN VER:fin
passata	VER:ppast	passare	f ADJ VER:ppast
a	PRE	a	f PRE
vederla	VER:infi:cli	vedere	f VER:infi:cli
prima	ADV	prima	f ADJ ADV
di	PRE	di	f PRE
andare	VER:infi	andare	f VER:infi
al	ARTPRE	al	f ARTPRE
lavoro	NOUN	lavoro	f NOUN VER:fin
,	PUN	,	f PUN
vide	VER:fin	vedere	f VER:fin
che	CHE	che	f CHE CON DET:wh
cominciava	VER:fin	cominciare	f VER:fin
a	PRE	a	f PRE
respirare	VER:infi	respirare	f VER:infi
sempre	ADV	sempre	f ADV
piú	ADV	più	f ADV
lentamente	ADV:mente	lentamente	f ADV:mente
e	CON	e	f CON
poi	ADV	poi	f ADV
ha	AUX:fin	avere	f AUX:fin VER:fin
smesso	VER:ppast	smettere	f ADJ VER:ppast
di	PRE	di	f PRE
respirare	VER:infi	respirare	f VER:infi
.	SENT	.	f SENT
Questo	PRO:demo	questo	f DET:demo PRO:demo
mi	CLI	mi	f CLI
ha	AUX:fin	avere	f AUX:fin VER:fin
insegniato	VER:ppast	<unknown>	s ADJ NOUN VER:fin VER:ppast
di	PRE	di	f PRE
godere	VER:infi	godere	f VER:infi
le	ART	la	f ART CLI
cose	NOUN	cosa	f NOUN
fino	PRE	fino	f ADJ PRE
che	CHE	che	f CHE CON DET:wh
ci	CLI	ci	f CLI
sono	VER:fin	essere	f AUX:fin VER:fin
,	PUN	,	f PUN
perché	WH	perché	f WH
non	NEG	non	f NEG
tutto	PRO:indef	tutto	f ADJ DET:indef PRO:indef
resta	VER:fin	restare	f VER:fin
per	PRE	per	f PRE
sempre	ADV	sempre	f ADV
.	SENT	.	f SENT

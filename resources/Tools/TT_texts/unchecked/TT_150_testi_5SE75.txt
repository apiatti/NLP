Il	ART	il	f ART
canarino	NOUN	canarino	f ADJ NOUN
sparito	VER:ppast	<unknown>	s ADJ NOUN VER:fin VER:ppast
Un	VER:fin	<unknown>	s NPR VER:fin
giorno	NOUN	giorno	f NOUN
stavo	VER:fin	stare	f VER:fin
guardando	VER:geru	guardare	f VER:geru
un	ART	un	f ART
canarino	NOUN	canarino	f ADJ NOUN
mi	CLI	mi	f CLI
piaceva	VER:fin	piacere	f VER:fin
tantissimo	ADJ	tanto	f ADJ
,	PUN	,	f PUN
e	CON	e	f CON
mi	CLI	mi	f CLI
venisse	VER:fin	venire	f AUX:fin VER:fin
voglia	NOUN	voglia	f NOUN VER2:fin VER:fin
di	PRE	di	f PRE
aprire	VER:infi	aprire	f VER:infi
la	ART	la	f ART CLI
gabbia	NOUN	gabbia	f NOUN
e	CON	e	f CON
di	PRE	di	f PRE
farlo	VER2:infi:cli	fare	f VER2:infi:cli VER:infi:cli
volare	VER:infi	volare	f VER:infi
per	PRE	per	f PRE
casa	NOUN	casa	f NOUN
e	CON	e	f CON
lo	CLI	lo	f ART CLI
feci	VER2:fin	fare	f NOUN VER2:fin VER:fin
volare	VER:infi	volare	f VER:infi
ma	CON	ma	f CON
non	NEG	non	f NEG
mi	CLI	mi	f CLI
accorsi	VER:fin	accorgere|accorrere	f ADJ NOUN VER:fin VER:ppast
della	ARTPRE	della	f ARTPRE
finestra	NOUN	finestra	f NOUN
aperta	ADJ	aperto	f ADJ VER:ppast
e	CON	e	f CON
il	ART	il	f ART
canarino	NOUN	canarino	f ADJ NOUN
scappó	ADJ	<unknown>	s ADJ NOCAT NOUN VER:fin VER:infi VER:ppast VER:ppre
,	PUN	,	f PUN
io	PRO:pers	io	f PRO:pers
lo	CLI	lo	f ART CLI
cercai	VER:fin	cercare	f VER:fin
d’	ADJ	<unknown>	s ADJ NOCAT VER:fin
appertutto	ADJ	<unknown>	s ADJ VER:fin
persino	ADV	persino	f ADV
sotto	PRE	sotto	f ADJ ADV PRE
il	ART	il	f ART
divano	NOUN	divano	f NOUN
poi	ADV	poi	f ADV
mi	CLI	mi	f CLI
accorsi	VER:fin	accorgere|accorrere	f ADJ NOUN VER:fin VER:ppast
che	CHE	che	f CHE CON DET:wh
la	ART	la	f ART CLI
finestra	NOUN	finestra	f NOUN
era	AUX:fin	essere	f AUX:fin NOUN VER:fin
aperta	VER:ppast	aprire	f ADJ VER:ppast
e	CON	e	f CON
il	ART	il	f ART
canarino	NOUN	canarino	f ADJ NOUN
era	VER:fin	essere	f AUX:fin NOUN VER:fin
sul	ARTPRE	sul	f ARTPRE
balcone	NOUN	balcone	f NOUN
lo	ART	il	f ART CLI
presi	ADJ	preso	f ADJ VER:fin VER:ppast
e	CON	e	f CON
lo	CLI	lo	f ART CLI
portai	VER:fin	portare	f VER:fin
in	PRE	in	f PRE
casa	NOUN	casa	f NOUN
e	CON	e	f CON
lo	ART	il	f ART CLI
messi	NOUN	messe	f ADJ NOUN VER:ppast
nella	ARTPRE	nella	f ARTPRE
gabbia	NOUN	gabbia	f NOUN
e	CON	e	f CON
lo	CLI	lo	f ART CLI
continuai	VER:fin	continuare	f VER:fin
a	PRE	a	f PRE
guardare	VER:infi	guardare	f VER:infi
.	SENT	.	f SENT
Questa	DET:demo	questo	f DET:demo PRO:demo
storia	NOUN	storia	f NOUN
mi	CLI	mi	f CLI
a	PRE	a	f PRE
insegnato	VER:ppast	insegnare	f VER:ppast
che	CHE	che	f CHE CON DET:wh
non	NEG	non	f NEG
devo	VER2:fin	dovere	f VER2:fin VER:fin
voler	VER2:infi	volere	f VER2:infi VER:infi
sempre	ADV	sempre	f ADV
le	ART	la	f ART CLI
cose	NOUN	cosa	f NOUN
o	CON	o	f CON
farle	VER:infi:cli	fare	f VER2:infi:cli VER:infi:cli
ma	CON	ma	f CON
basta	VER:fin	bastare	f VER:fin
anche	ADV	anche	f ADV
solo	ADV	solo	f ADJ ADV VER:fin
stare	VER:infi	stare	f ADV VER:infi
a	PRE	a	f PRE
guardare	VER:infi	guardare	f VER:infi
.	SENT	.	f SENT

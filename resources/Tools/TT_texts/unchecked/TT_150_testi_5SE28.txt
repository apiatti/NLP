L’	ADJ	<unknown>	s ADJ NOCAT NPR VER:fin
insegnamento	NOUN	insegnamento	f NOUN
della	ARTPRE	della	f ARTPRE
volpe	NOUN	volpe	f NOUN
mi	CLI	mi	f CLI
ha	AUX:fin	avere	f AUX:fin VER:fin
fatto	VER2:ppast	fare	f NOUN VER2:ppast VER:ppast
pensare	VER:infi	pensare	f VER:infi
quando	WH	quando	f WH
eravamo	VER:fin	essere	f AUX:fin VER:fin
al	ARTPRE	al	f ARTPRE
triatlon	NOUN	<unknown>	s NOUN VER:fin
,	PUN	,	f PUN
io	PRO:pers	io	f PRO:pers
io	PRO:pers	io	f PRO:pers
ero	VER:fin	essere	f AUX:fin AUX:geru AUX:ppre VER:fin
convito	NOUN	convito	f NOUN VER:fin
che	CHE	che	f CHE CON DET:wh
battevo	VER:fin	battere	f VER:fin
il	ART	il	f ART
Samuel	NPR	Samuel	f NPR
,	PUN	,	f PUN
(	PUN	(	f PUN
il	ART	il	f ART
Samuel	NPR	Samuel	f NPR
é	VER:fin	essere	f AUX:fin VER:fin
un	ART	un	f ART
compagno	NOUN	compagno	f NOUN
di	PRE	di	f PRE
classe	NOUN	classe	f NOUN
che	CHE	che	f CHE CON DET:wh
andava	VER:fin	andare	f VER:fin
nella	ARTPRE	nella	f ARTPRE
4B	NOUN	<unknown>	s NOCAT NOUN NPR
)	PUN	)	f PUN
ma	CON	ma	f CON
il	ART	il	f ART
Samuel	NPR	Samuel	f NPR
si	CLI	si	f CLI
è	AUX:fin	essere	f AUX:fin VER:fin
veramente	ADV:mente	veramente	f ADV:mente
impegnato	VER:ppast	impegnare	f ADJ VER:ppast
e	CON	e	f CON
ce	CLI	ce	f CLI
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
ha	AUX:fin	avere	f AUX:fin VER:fin
fatta	VER:ppast	fare	f VER2:ppast VER:ppast
,	PUN	,	f PUN
da	PRE	da	f PRE
li	CLI	li	f CLI
ho	AUX:fin	avere	f AUX:fin VER:fin
ricevuto	VER:ppast	ricevere	f VER:ppast
un	ART	un	f ART
piccolo	ADJ	piccolo	f ADJ NOUN
colpo	NOUN	colpo	f NOUN
.	SENT	.	f SENT
Alla	ARTPRE	alla	f ARTPRE
prossima	ADJ	prossimo	f ADJ
gara	NOUN	gara	f NOUN
ho	AUX:fin	avere	f AUX:fin VER:fin
pensato	VER:ppast	pensare	f VER:ppast
di	PRE	di	f PRE
perdere	VER:infi	perdere	f VER:infi
di	PRE	di	f PRE
nuovo	NOUN	nuovo	f ADJ NOUN
,	PUN	,	f PUN
ma	CON	ma	f CON
questa	DET:demo	questo	f DET:demo PRO:demo
volta	NOUN	volta	f ADJ NOUN VER:fin VER:ppast
contro	PRE	contro	f ADV PRE
un	ART	un	f ART
altro	PRO:indef	altro	f ADJ DET:indef PRO:indef
il	ART	il	f ART
Leonardo	NPR	Leonardo	f NPR
,	PUN	,	f PUN
il	ART	il	f ART
Leonardo	NPR	Leonardo	f NPR
è	VER:fin	essere	f AUX:fin VER:fin
un	ART	un	f ART
compagno	NOUN	compagno	f NOUN
di	PRE	di	f PRE
classe	NOUN	classe	f NOUN
di	PRE	di	f PRE
Samuel	NPR	Samuel	f NPR
.	SENT	.	f SENT
Ma	CON	ma	f CON
quella	DET:demo	quello	f DET:demo PRO:demo
volta	NOUN	volta	f ADJ NOUN VER:fin VER:ppast
ha	AUX:fin	avere	f AUX:fin VER:fin
creduto	VER:ppast	credere	f VER:ppast
in	PRE	in	f PRE
me	PRO:pers	me	f CLI PRO:pers
e	CON	e	f CON
sono	AUX:fin	essere	f AUX:fin VER:fin
riuscito	VER:ppast	riuscire	f VER:ppast
a	PRE	a	f PRE
batterlo	VER:infi:cli	battere	f VER:infi:cli
solo	ADV	solo	f ADJ ADV VER:fin
di	PRE	di	f PRE
poco	ADV	poco	f ADJ ADV DET:indef PRO:indef
perché	WH	perché	f WH
anche	ADV	anche	f ADV
lui	PRO:pers	lui	f PRO:pers
ha	AUX:fin	avere	f AUX:fin VER:fin
fatto	VER:ppast	fare	f NOUN VER2:ppast VER:ppast
il	ART	il	f ART
suo	DET:poss	suo	f DET:poss PRO:poss
meglio	NOUN	meglio	f ADJ ADV NOUN
e	CON	e	f CON
quasi	ADV	quasi	f ADV
quasi	ADV	quasi	f ADV
mi	CLI	mi	f CLI
stava	VER:fin	stare	f VER:fin
per	PRE	per	f PRE
superare	VER:infi	superare	f VER:infi
.	SENT	.	f SENT

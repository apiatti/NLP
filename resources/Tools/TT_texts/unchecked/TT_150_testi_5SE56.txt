La	ART	la	f ART CLI
partita	NOUN	partita	f ADJ NOUN VER:ppast
Una	VER:fin	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
volta	NOUN	volta	f ADJ NOUN VER:fin VER:ppast
io	PRO:pers	io	f PRO:pers
e	CON	e	f CON
la	ART	la	f ART CLI
mia	DET:poss	mio	f DET:poss PRO:poss
squadra	NOUN	squadra	f NOUN VER:fin
di	PRE	di	f PRE
calcio	NOUN	calcio	f NOUN VER:fin
stavevamo	VER:fin	<unknown>	s VER:fin
facendo	VER:geru	fare	f VER2:geru VER:geru
una	ART	una	f ART PRO:indef
partita	NOUN	partita	f ADJ NOUN VER:ppast
contro	PRE	contro	f ADV PRE
una	ART	una	f ART PRO:indef
squadra	NOUN	squadra	f NOUN VER:fin
non	NEG	non	f NEG
molto	ADV	molto	f ADJ ADV DET:indef
forte	ADJ	forte	f ADJ NOUN
,	PUN	,	f PUN
a	PRE	a	f PRE
Mezzovico	NPR	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
.	SENT	.	f SENT
Passarono	VER:fin	passare	f VER:fin
10	NUM	@card@	f NUM
o	CON	o	f CON
piú	ADV	più	f ADV
minuti	NOUN	minuto	f ADJ NOUN
dall’	ADJ	<unknown>	s ADJ NOCAT
inizio	NOUN	inizio	f NOUN VER:fin
e	CON	e	f CON
noi	PRO:pers	noi	f PRO:pers
stavevamo	VER:fin	<unknown>	s VER:fin
giá	ADJ	<unknown>	s ADJ NOCAT VER:fin
1-0	NUM	@card@	f NUM
,	PUN	,	f PUN
al	ARTPRE	al	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
primo	ADJ	primo	f ADJ NOUN
tempo	NOUN	tempo	f NOUN
eravamo	VER:fin	essere	f AUX:fin VER:fin
3-0	NUM	@card@	f NUM
poi	ADV	poi	f ADV
visto	NOUN	visto	f NOUN VER:fin
che	CHE	che	f CHE CON DET:wh
eravamo	VER:fin	essere	f AUX:fin VER:fin
in	PRE	in	f PRE
vantaggio	NOUN	vantaggio	f NOUN
di	PRE	di	f PRE
tre	DET:num	tre	f DET:num PRO:num
gol	NOUN	gol	f NOUN
al	ARTPRE	al	f ARTPRE
inizio	NOUN	inizio	f NOUN VER:fin
del	ARTPRE	del	f ARTPRE
secondo	ADJ	secondo	f ADJ NOUN PRE VER:fin
tempo	NOUN	tempo	f NOUN
,	PUN	,	f PUN
sapendo	VER:geru	sapere	f VER:geru
giá	ADJ	<unknown>	s ADJ NOCAT VER:fin
di	PRE	di	f PRE
vincere	VER:infi	vincere	f VER:infi
in	PRE	in	f PRE
campo	NOUN	campo	f NOUN VER:fin
perdevamo	VER:fin	perdere	f VER:fin
tempo	NOUN	tempo	f NOUN
buttevamo	VER:fin	<unknown>	s VER:fin
la	ART	la	f ART CLI
palla	NOUN	palla	f NOUN
fuori	ADV	fuori	f ADV PRE
ecc	ADJ	<unknown>	s ADJ NOUN NPR VER:fin
…	NOCAT	<unknown>	s NOCAT
Ma	VER:fin	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
dopo	PRE	dopo	f ADJ ADV CON PRE
gli	ART	il	f ART CLI
avversari	NOUN	avversario	f NOUN
in	PRE	in	f PRE
dieci	DET:num	dieci	f DET:num PRO:num
minuti	NOUN	minuto	f ADJ NOUN
segnarono	VER:fin	segnare	f VER:fin
due	DET:num	due	f DET:num PRO:num
gol	NOUN	gol	f NOUN
e	CON	e	f CON
stavano	VER:fin	stare	f VER:fin
per	PRE	per	f PRE
fare	VER:infi	fare	f VER2:infi VER:infi
il	ART	il	f ART
terzo	ADJ	terzo	f ADJ NOUN
,	PUN	,	f PUN
poi	ADV	poi	f ADV
un	ART	un	f ART
mio	DET:poss	mio	f DET:poss PRO:poss
compagno	NOUN	compagno	f NOUN
dalla	ARTPRE	dalla	f ARTPRE
paura	NOUN	paura	f NOUN
fece	VER:fin	fare	f NOUN VER2:fin VER:fin
un	ART	un	f ART
fallo	NOUN	fallo	f NOUN VER:fin
a	PRE	a	f PRE
un	ART	un	f ART
avversario	NOUN	avversario	f NOUN
e	CON	e	f CON
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
arbitro	NOUN	arbitro	f NOUN
disse	VER:fin	dire	f VER:fin
che	CHE	che	f CHE CON DET:wh
era	VER:fin	essere	f AUX:fin NOUN VER:fin
rigore	NOUN	rigore	f NOUN
,	PUN	,	f PUN
su	PRE	su	f PRE
rigore	NOUN	rigore	f NOUN
segnó	ADJ	<unknown>	s ADJ NOCAT VER:fin
e	CON	e	f CON
il	ART	il	f ART
punteggio	NOUN	punteggio	f NOUN VER:fin
stava	VER:fin	stare	f VER:fin
a	PRE	a	f PRE
3-3	NUM	@card@	f NUM
.	SENT	.	f SENT
Alla	ARTPRE	alla	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
mancava	VER:fin	mancare	f VER:fin
poco	ADV	poco	f ADJ ADV DET:indef PRO:indef
peró	ADJ	<unknown>	s ADJ NOCAT VER:fin
nessuno	PRO:indef	nessuno	f ADJ PRO:indef
fece	VER:fin	fare	f NOUN VER2:fin VER:fin
piú	ADV	più	f ADV
gol	NOUN	gol	f NOUN
.	SENT	.	f SENT
La	ART	la	f ART CLI
partita	NOUN	partita	f ADJ NOUN VER:ppast
terminò	VER:fin	terminare	f VER:fin
3-3	NUM	@card@	f NUM
e	CON	e	f CON
noi	PRO:pers	noi	f PRO:pers
eravamo	AUX:fin	essere	f AUX:fin VER:fin
molto	ADV	molto	f ADJ ADV DET:indef
delusi	VER:ppast	deludere	f ADJ NOUN VER:fin VER:ppast
perché	WH	perché	f WH
invece	ADV	invece	f ADV
di	PRE	di	f PRE
perdere	VER:infi	perdere	f VER:infi
tempo	NOUN	tempo	f NOUN
ecc	ADJ	<unknown>	s ADJ NOUN NPR VER:fin
…	NOCAT	<unknown>	s NOCAT
avremmo	AUX:fin	avere	f AUX:fin VER:fin
potuto	VER:ppast	potere	f VER2:ppre VER:ppast
provare	VER:infi	provare	f VER:infi
a	PRE	a	f PRE
fare	VER:infi	fare	f VER2:infi VER:infi
gol	NOUN	gol	f NOUN
e	CON	e	f CON
difendere	VER:infi	difendere	f VER:infi
in	PRE	in	f PRE
difesa	NOUN	difesa	f ADJ NOUN VER:ppast
.	SENT	.	f SENT
La	ART	la	f ART CLI
morale	NOUN	morale	f ADJ NOUN
di	PRE	di	f PRE
questa	DET:demo	questo	f DET:demo PRO:demo
partita	NOUN	partita	f ADJ NOUN VER:ppast
:	PUN	:	f PUN
anche	ADV	anche	f ADV
se	CON	se	f CLI CON
giochi	NOUN	gioco	f NOUN VER:fin
contro	PRE	contro	f ADV PRE
una	ART	una	f ART PRO:indef
squadra	NOUN	squadra	f NOUN VER:fin
debole	ADJ	debole	f ADJ NOUN
devi	VER2:fin	dovere	f VER2:fin VER:fin
concentrarti	VER:infi:cli	concentrare	f VER:infi:cli
e	CON	e	f CON
non	NEG	non	f NEG
fare	VER:infi	fare	f VER2:infi VER:infi
cavolate	NOUN	<unknown>	s ADJ NOUN VER:fin VER:ppast
perché	WH	perché	f WH
potrebbe	VER2:fin	potere	f VER2:fin VER:fin
batterti	VER:infi:cli	battere	f VER:infi:cli
.	SENT	.	f SENT

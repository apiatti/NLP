L’	ADJ	<unknown>	s ADJ NOCAT NPR VER:fin
anno	NOUN	anno	f NOUN
scorso	ADJ	scorso	f ADJ VER:ppast
nel	ARTPRE	nel	f ARTPRE
campionato	NOUN	campionato	f NOUN VER:ppast
di	PRE	di	f PRE
calcio	NOUN	calcio	f NOUN VER:fin
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
giocato	VER:ppast	giocare	f VER:ppast
contro	PRE	contro	f ADV PRE
il	ART	il	f ART
sassariente	NOUN	<unknown>	s ADJ NOUN VER:ppre
piano	NOUN	piano	f ADJ NOUN VER:fin
8	NUM	@card@	f NUM
in	PRE	in	f PRE
spogliatoio	NOUN	spogliatoio	f NOUN
avevo	AUX:fin	avere	f AUX:fin VER:fin
sotto	ADV	sotto	f ADJ ADV PRE
valutato	VER:ppast	valutare	f VER:ppast
il	ART	il	f ART
avversario	NOUN	avversario	f NOUN
in	PRE	in	f PRE
partita	NOUN	partita	f ADJ NOUN VER:ppast
ho	VER:fin	avere	f AUX:fin VER:fin
visto	NOUN	visto	f NOUN VER:fin
che	CHE	che	f CHE CON DET:wh
ci	CLI	ci	f CLI
stavano	VER:fin	stare	f VER:fin
pattendo	VER:geru	<unknown>	s VER:fin VER:geru
2:0	NUM	@card@	f NUM
peró	ADJ	<unknown>	s ADJ NOCAT VER:fin
nel	ARTPRE	nel	f ARTPRE
secondo	ADJ	secondo	f ADJ NOUN PRE VER:fin
tempo	NOUN	tempo	f NOUN
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
ricuperato	VER:ppast	ricuperare	f VER:ppast
fino	PRE	fino	f ADJ PRE
a	PRE	a	f PRE
2:2	NUM	@card@	f NUM
quando	WH	quando	f WH
ci	CLI	ci	f CLI
mancavano	VER:fin	mancare	f VER:fin
due	DET:num	due	f DET:num PRO:num
tre	DET:num	tre	f DET:num PRO:num
minuti	NOUN	minuto	f ADJ NOUN
eravamo	VER:fin	essere	f AUX:fin VER:fin
un	ART	un	f ART
po	NOUN	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
’	NOCAT	<unknown>	s NOCAT
emozionati	VER:ppast	emozionare	f VER:ppast
peró	ADJ	<unknown>	s ADJ NOCAT VER:fin
nei	ARTPRE	nel	f ARTPRE NOUN
ultimi	ADJ	ultimo	f ADJ NOUN VER:fin
50	NUM	@card@	f NUM
secondi	NOUN	secondo	f ADJ NOUN VER:fin
i	ART	il	f ART
avversari	NOUN	avversario	f NOUN
anno	NOUN	anno	f NOUN
ancora	ADV	ancora	f ADV VER:fin
segnato	VER:ppast	segnare	f ADJ VER:ppast
un	ART	un	f ART
Gol	NOUN	gol	s NOUN NPR
per	PRE	per	f PRE
quello	PRO:demo	quello	f DET:demo PRO:demo
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
perso	VER:ppast	perdere	f ADJ VER:ppast
la	ART	la	f ART CLI
partita	NOUN	partita	f ADJ NOUN VER:ppast
.	SENT	.	f SENT
In	PRE	in	f PRE
spogliatoi	NOUN	spogliatoio	f NOUN
il	ART	il	f ART
allenatore	NOUN	allenatore	f NOUN
era	VER:fin	essere	f AUX:fin NOUN VER:fin
abbaxxxtanza	NOUN	<unknown>	s ADJ NOUN VER:fin
contento	ADJ	contento	f ADJ NOUN VER:fin
di	PRE	di	f PRE
come	WH	come	f WH
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
giocato	VER:ppast	giocare	f VER:ppast
.	SENT	.	f SENT
Nei	ARTPRE	nel	f ARTPRE NOUN
allenamenti	NOUN	allenamento	f NOUN
il	ART	il	f ART
allenatore	NOUN	allenatore	f NOUN
aveva	AUX:fin	avere	f AUX:fin VER:fin
detto	VER:ppast	dire	f VER:fin VER:ppast
di	PRE	di	f PRE
non	NEG	non	f NEG
sotto	PRE	sotto	f ADJ ADV PRE
valutare	VER:infi	valutare	f VER:infi
gli	ART	il	f ART CLI
aversari	NOUN	<unknown>	s ADJ NOUN NPR VER:infi:cli
e	CON	e	f CON
aveva	AUX:fin	avere	f AUX:fin VER:fin
anche	ADV	anche	f ADV
detto	VER:ppast	dire	f VER:fin VER:ppast
di	PRE	di	f PRE
correre	VER:infi	correre	f VER:infi
perché	WH	perché	f WH
se	CON	se	f CLI CON
non	NEG	non	f NEG
cori	NOUN	coro	f NOUN
non	NEG	non	f NEG
piuoi	VER:fin	<unknown>	s ADJ NOUN VER:fin
segnare	VER:infi	segnare	f VER:infi
.	SENT	.	f SENT
L’	ADJ	<unknown>	s ADJ NOCAT NPR VER:fin
insegnamento	NOUN	insegnamento	f NOUN
che	CHE	che	f CHE CON DET:wh
ho	AUX:fin	avere	f AUX:fin VER:fin
ricevuto	VER:ppast	ricevere	f VER:ppast
é	VER:fin	essere	f AUX:fin VER:fin
di	PRE	di	f PRE
non	NEG	non	f NEG
sotto	PRE	sotto	f ADJ ADV PRE
valutare	VER:infi	valutare	f VER:infi
xxx	NOUN	<unknown>	s ADJ NOUN
i	ART	il	f ART
avversari	NOUN	avversario	f NOUN
e	CON	e	f CON
mi	CLI	mi	f CLI
sono	AUX:fin	essere	f AUX:fin VER:fin
vantato	VER:ppast	vantare	f VER:ppast
troppo	ADV	troppo	f ADJ ADV DET:indef
e	CON	e	f CON
non	NEG	non	f NEG
ho	VER:fin	avere	f AUX:fin VER:fin
corso	NOUN	corso	f ADJ NOUN VER:ppast
.	SENT	.	f SENT

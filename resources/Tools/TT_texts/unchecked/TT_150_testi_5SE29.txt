Il	ART	il	f ART
mio	DET:poss	mio	f DET:poss PRO:poss
insegnamento	NOUN	insegnamento	f NOUN
Un	VER:fin	<unknown>	s NPR VER:fin
giorno	NOUN	giorno	f NOUN
mia	DET:poss	mio	f DET:poss PRO:poss
madre	NOUN	madre	f ADJ NOUN
è	AUX:fin	essere	f AUX:fin VER:fin
andata	VER:ppast	andare	f ADJ VER:ppast
a	PRE	a	f PRE
parlare	VER:infi	parlare	f VER:infi
con	PRE	con	f PRE
MARIKA	NPR	<unknown>	s ADJ NOUN NPR VER:fin
,	PUN	,	f PUN
hanno	AUX:fin	avere	f AUX:fin VER:fin
parlato	VER:ppast	parlare	f ADJ VER:ppast
e	CON	e	f CON
si	CLI	si	f CLI
sono	AUX:fin	essere	f AUX:fin VER:fin
messe	VER:ppast	mettere	f ADJ NOUN VER:ppast
daccordo	VER:fin	<unknown>	s ADJ NOUN VER:fin
che	CHE	che	f CHE CON DET:wh
quasi	ADV	quasi	f ADV
tutti	DET:indef	tutto	f ADJ DET:indef PRO:indef
i	ART	il	f ART
giorni	NOUN	giorno	f NOUN
porto	VER:ppast	porgere	f ADJ NOUN VER:fin VER:ppast
delle	ARTPRE	della	f ARTPRE
schede	NOUN	scheda	f NOUN
di	PRE	di	f PRE
Dimat	NPR	<unknown>	s NOUN NPR
a	PRE	a	f PRE
casa	NOUN	casa	f NOUN
per	PRE	per	f PRE
migliorare	VER:infi	migliorare	f VER:infi
in	PRE	in	f PRE
matematica	NOUN	matematica	f ADJ NOUN
.	SENT	.	f SENT
Io	PRO:pers	io	f PRO:pers
non	NEG	non	f NEG
vuolevo	VER:fin	<unknown>	s VER:fin
,	PUN	,	f PUN
ma	CON	ma	f CON
dopo	PRE	dopo	f ADJ ADV CON PRE
un	ART	un	f ART
po	ADJ	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
’	NOCAT	<unknown>	s NOCAT
ho	VER:fin	avere	f AUX:fin VER:fin
visto	NOUN	visto	f NOUN VER:fin
che	CHE	che	f CHE CON DET:wh
valeva	VER:fin	valere	f VER:fin
la	ART	la	f ART CLI
pena	NOUN	pena	f NOUN VER:fin
allora	ADV	allora	f ADV
ho	AUX:fin	avere	f AUX:fin VER:fin
cominciato	VER:ppast	cominciare	f VER:ppast
a	PRE	a	f PRE
impegnarmi	VER:infi:cli	impegnare	f VER:infi:cli
.	SENT	.	f SENT
Dopo	ADV	dopo	f ADJ ADV CON PRE
sono	AUX:fin	essere	f AUX:fin VER:fin
sempre	ADV	sempre	f ADV
diventato	VER:ppast	diventare	f VER:ppast
più	ADV	più	f ADV
bravo	ADJ	bravo	f ADJ
.	SENT	.	f SENT
E	CON	e	f CON
si	CLI	si	f CLI
vedeva	VER:fin	vedere	f VER:fin
il	ART	il	f ART
cambiamento	NOUN	cambiamento	f NOUN
,	PUN	,	f PUN
soprattutto	ADV	soprattutto	f ADV
nel	ARTPRE	nel	f ARTPRE
Dimat	NPR	<unknown>	s NOUN NPR
e	CON	e	f CON
in	PRE	in	f PRE
altre	DET:indef	altro	f ADJ DET:indef PRO:indef
schede	NOUN	scheda	f NOUN
.	SENT	.	f SENT
Venerdì	NOUN	venerdì	f NOUN
Abbiamo	VER:fin	avere	s NPR VER:fin
fatto	VER:ppast	fare	f NOUN VER2:ppast VER:ppast
allenamento	NOUN	allenamento	f NOUN
per	PRE	per	f PRE
il	ART	il	f ART
torneo	NOUN	torneo	f NOUN VER:fin
scolari	ADJ	scolare	f ADJ NOUN
.	SENT	.	f SENT
Poi	ADV	poi	f ADV
è	AUX:fin	essere	f AUX:fin VER:fin
arrivato	VER:ppast	arrivare	f ADJ VER:ppast
Aris	NPR	<unknown>	s NPR
a	PRE	a	f PRE
chiedere	VER:infi	chiedere	f VER:infi
se	CON	se	f CLI CON
volevamo	VER2:fin	volere	f VER2:fin VER:fin
fare	VER:infi	fare	f VER2:infi VER:infi
una	ART	una	f ART PRO:indef
partita	NOUN	partita	f ADJ NOUN VER:ppast
contro	PRE	contro	f ADV PRE
di	PRE	di	f PRE
loro	PRO:pers	loro	f DET:poss PRO:pers PRO:poss
io	PRO:pers	io	f PRO:pers
dicevo	VER:fin	dire	f VER:fin
di	PRE	di	f PRE
no	ADV	no	f ADV NOUN
perché	WH	perché	f WH
da	PRE	da	f PRE
loro	PRO:pers	loro	f DET:poss PRO:pers PRO:poss
giocavano	VER:fin	giocare	f VER:fin
tutti	PRO:indef	tutti	f ADJ DET:indef PRO:indef
a	PRE	a	f PRE
calcio	NOUN	calcio	f NOUN VER:fin
apparte	VER:fin	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
Samuel	NPR	Samuel	f NPR
invece	ADV	invece	f ADV
da	PRE	da	f PRE
noi	PRO:pers	noi	f PRO:pers
solo	ADV	solo	f ADJ ADV VER:fin
in	PRE	in	f PRE
3	NUM	@card@	f NUM
.	SENT	.	f SENT
Peró	NPR	<unknown>	s ADJ NOCAT NPR VER:fin
poi	ADV	poi	f ADV
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
fatto	VER:ppast	fare	f NOUN VER2:ppast VER:ppast
la	ART	la	f ART CLI
partita	NOUN	partita	f ADJ NOUN VER:ppast
e	CON	e	f CON
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
Aramis	NPR	<unknown>	s NPR
mi	CLI	mi	f CLI
diceva	VER:fin	dire	f VER:fin
ai	ARTPRE	al	f ARTPRE
paura	NOUN	paura	f NOUN
di	PRE	di	f PRE
perdere	VER:infi	perdere	f VER:infi
io	PRO:pers	io	f PRO:pers
dicevo	VER:fin	dire	f VER:fin
di	PRE	di	f PRE
no	ADV	no	f ADV NOUN
anche	ADV	anche	f ADV
se	CON	se	f CLI CON
in	PRE	in	f PRE
verità	NOUN	verità	f NOUN
si	CLI	si	f CLI
.	SENT	.	f SENT
Poi	ADV	poi	f ADV
io	PRO:pers	io	f PRO:pers
ci	CLI	ci	f CLI
ho	AUX:fin	avere	f AUX:fin VER:fin
messo	VER:ppast	mettere	f ADJ VER:ppast
tutto	DET:indef	tutto	f ADJ DET:indef PRO:indef
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
impegnio	VER:fin	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
che	CHE	che	f CHE CON DET:wh
avevo	VER:fin	avere	f AUX:fin VER:fin
e	CON	e	f CON
poi	ADV	poi	f ADV
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
vinto	VER:ppast	vincere	f ADJ VER:ppast
e	CON	e	f CON
io	PRO:pers	io	f PRO:pers
ho	AUX:fin	avere	f AUX:fin VER:fin
fatto	VER:ppast	fare	f NOUN VER2:ppast VER:ppast
due	DET:num	due	f DET:num PRO:num
Goal	NPR	<unknown>	s NOUN NPR
!	SENT	!	f SENT
Queste	DET:demo	questo	f DET:demo PRO:demo
due	DET:num	due	f DET:num PRO:num
esperienze	NOUN	esperienza	f NOUN
mi	CLI	mi	f CLI
hanno	AUX:fin	avere	f AUX:fin VER:fin
fatto	VER2:ppast	fare	f NOUN VER2:ppast VER:ppast
capire	VER:infi	capire	f VER:infi
che	CHE	che	f CHE CON DET:wh
conviene	VER:fin	convenire	f VER:fin

LA	ART	la	f ART CLI
DISAVVENTURA	NPR	<unknown>	s ADJ NOCAT NOUN NPR VER:fin
SULLA	NPR	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
NEVE	NPR	<unknown>	s NOCAT NOUN NPR VER:fin
Qualche	NPR	<unknown>	s ADJ NOUN NPR VER:fin
anno	NOUN	anno	f NOUN
fa	VER:fin	fare	f ADV VER2:fin VER:fin
in	PRE	in	f PRE
montagna	NOUN	montagna	f NOUN
,	PUN	,	f PUN
stavamo	VER:fin	stare	f VER:fin
scendendo	VER:geru	scendere	f VER:geru
io	PRO:pers	io	f PRO:pers
mia	DET:poss	mio	f DET:poss PRO:poss
sorella	NOUN	sorella	f NOUN
e	CON	e	f CON
degli	ARTPRE	del	f ARTPRE
amici	NOUN	amico	f ADJ NOUN
con	PRE	con	f PRE
il	ART	il	f ART
bob	NOUN	bob	f NOUN
sulla	ARTPRE	sulla	f ARTPRE
neve	NOUN	neve	f NOUN
.	SENT	.	f SENT
Dopo	PRE	dopo	f ADJ ADV CON PRE
tante	DET:indef	tanto	f ADJ DET:indef PRO:indef
discese	NOUN	discesa	f NOUN VER:fin VER:ppast
mia	DET:poss	mio	f DET:poss PRO:poss
mamma	NOUN	mamma	f NOUN
disse	VER:fin	dire	f VER:fin
che	CHE	che	f CHE CON DET:wh
ci	CLI	ci	f CLI
dovevamo	VER2:fin	dovere	f VER2:fin VER:fin
fermare	VER:infi	fermare	f VER:infi
perché	WH	perché	f WH
il	ART	il	f ART
sole	NOUN	sole	f ADJ NOUN
stava	VER:fin	stare	f VER:fin
calando	VER:geru	calare	f VER:geru
e	CON	e	f CON
la	ART	la	f ART CLI
neve	NOUN	neve	f NOUN
stava	VER:fin	stare	f VER:fin
ghiacciando	VER:geru	ghiacciare	f VER:geru
.	SENT	.	f SENT
Io	PRO:pers	io	f PRO:pers
testarda	ADJ	testardo	f ADJ NOUN
come	WH	come	f WH
un	ART	un	f ART
mulo	NOUN	mulo	f NOUN
feci	VER:fin	fare	f NOUN VER2:fin VER:fin
un’	ADJ	<unknown>	s ADJ NOCAT VER:fin
altra	DET:indef	altro	f ADJ DET:indef PRO:indef
discesa	NOUN	discesa	f NOUN VER:ppast
.	SENT	.	f SENT
Partii	VER:fin	partire	f VER:fin
velocissima	ADJ	veloce	f ADJ
,	PUN	,	f PUN
tentai	VER:fin	tentare	f VER:fin
di	PRE	di	f PRE
frenare	VER:infi	frenare	f VER:infi
ma	CON	ma	f CON
non	NEG	non	f NEG
riuscii	VER:fin	riuscire	f VER:fin
,	PUN	,	f PUN
i	ART	il	f ART
freni	NOUN	freno	f NOUN VER:fin
non	NEG	non	f NEG
frenavano	VER:fin	frenare	f VER:fin
piú	ADV	più	f ADV
,	PUN	,	f PUN
andai	VER:fin	andare	f VER:fin
contro	PRE	contro	f ADV PRE
una	ART	una	f ART PRO:indef
rete	NOUN	rete	f NOUN
ruggine	NOUN	ruggine	f NOUN
,	PUN	,	f PUN
mi	CLI	mi	f CLI
tagliai	VER:fin	tagliare	f VER:fin
sulla	ARTPRE	sulla	f ARTPRE
fronte	NOUN	fronte	f NOUN
,	PUN	,	f PUN
andai	VER:fin	andare	f VER:fin
subito	ADV	subito	f ADJ ADV VER:ppast
all’	ADJ	<unknown>	s ADJ NOCAT VER:fin
ospedale	NOUN	ospedale	f NOUN
e	CON	e	f CON
dopo	PRE	dopo	f ADJ ADV CON PRE
tanti	DET:indef	tanto	f ADJ DET:indef PRO:indef
minuti	NOUN	minuto	f ADJ NOUN
di	PRE	di	f PRE
urla	NOUN	urlo	f NOUN VER:fin
mi	CLI	mi	f CLI
fecero	VER:fin	fare	f VER2:fin VER:fin
alcuni	DET:indef	alcun	f DET:indef PRO:indef
punti	NOUN	punto	f ADJ NOUN VER:fin VER:ppast
.	SENT	.	f SENT
Ahi	NPR	<unknown>	s ADJ NOCAT NPR
,	PUN	,	f PUN
sento	VER:fin	sentire	f VER:fin
ancora	ADV	ancora	f ADV VER:fin
il	ART	il	f ART
male	NOUN	male	f ADJ ADV NOUN
!	SENT	!	f SENT
Da	PRE	da	f PRE
quella	DET:demo	quello	f DET:demo PRO:demo
volta	NOUN	volta	f ADJ NOUN VER:fin VER:ppast
diedi	VER:fin	dare	f VER:fin
sempre	ADV	sempre	f ADV
retta	VER:ppast	reggere	f ADJ NOUN VER:ppast
a	PRE	a	f PRE
mia	DET:poss	mio	f DET:poss PRO:poss
mamma	NOUN	mamma	f NOUN
perché	WH	perché	f WH
fino	PRE	fino	f ADJ PRE
alla	ARTPRE	alla	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
ha	AUX:fin	avere	f AUX:fin VER:fin
sempre	ADV	sempre	f ADV
ragione	NOUN	ragione	f NOUN
!	SENT	!	f SENT

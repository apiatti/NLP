Una	ART	una	f ART PRO:indef
brutta	ADJ	brutto	f ADJ VER:fin
storia	NOUN	storia	f NOUN
Un	VER:fin	<unknown>	s NPR VER:fin
giorno	NOUN	giorno	f NOUN
ero	VER:fin	essere	f AUX:fin AUX:geru AUX:ppre VER:fin
a	PRE	a	f PRE
casa	NOUN	casa	f NOUN
mia	DET:poss	mio	f DET:poss PRO:poss
con	PRE	con	f PRE
mio	DET:poss	mio	f DET:poss PRO:poss
papá	ADJ	<unknown>	s ADJ NOCAT VER:fin
e	CON	e	f CON
mia	DET:poss	mio	f DET:poss PRO:poss
mamma	NOUN	mamma	f NOUN
e	CON	e	f CON
non	NEG	non	f NEG
sapevamo	VER:fin	sapere	f VER:fin
cosa	WH	cosa	f NOUN WH
fare	VER:infi	fare	f VER2:infi VER:infi
,	PUN	,	f PUN
dopo	PRE	dopo	f ADJ ADV CON PRE
un	ART	un	f ART
po	ADJ	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
’	NOCAT	<unknown>	s NOCAT
mio	DET:poss	mio	f DET:poss PRO:poss
papá	ADJ	<unknown>	s ADJ NOCAT VER:fin
ha	AUX:fin	avere	f AUX:fin VER:fin
detto	VER:ppast	dire	f VER:fin VER:ppast
:	PUN	:	f PUN
–	NOCAT	<unknown>	s NOCAT
Ma	VER:fin	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
possiam	ADJ	<unknown>	s ADJ NOUN NPR VER:fin
beh	NOCAT	beh	f NOCAT
andare	VER:infi	andare	f VER:infi
a	PRE	a	f PRE
fare	VER:infi	fare	f VER2:infi VER:infi
un	ART	un	f ART
giro	NOUN	giro	f NOUN VER:fin
in	PRE	in	f PRE
bici	NOUN	bici	f NOUN
a	PRE	a	f PRE
Malvaglia	NPR	<unknown>	s NOUN NPR VER:fin
.	SENT	.	f SENT
Allora	ADV	allora	f ADV
io	PRO:pers	io	f PRO:pers
e	CON	e	f CON
mio	DET:poss	mio	f DET:poss PRO:poss
papá	ADJ	<unknown>	s ADJ NOCAT VER:fin
ci	CLI	ci	f CLI
siamo	AUX:fin	essere	f AUX:fin VER:fin
messi	VER:ppast	mettere	f ADJ NOUN VER:ppast
a	PRE	a	f PRE
lavoro	NOUN	lavoro	f NOUN VER:fin
e	CON	e	f CON
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
iniziato	VER:ppast	iniziare	f ADJ VER:ppast
a	PRE	a	f PRE
pulir	VER:infi	pulire	f VER:infi
le	ART	la	f ART CLI
bici	NOUN	bici	f NOUN
.	SENT	.	f SENT
E	CON	e	f CON
dopo	PRE	dopo	f ADJ ADV CON PRE
mez’ora	NOUN	<unknown>	s ADJ NOUN VER:fin
siamo	AUX:fin	essere	f AUX:fin VER:fin
partiti	VER:ppast	partire	f ADJ NOUN VER:ppast
per	PRE	per	f PRE
andare	VER:infi	andare	f VER:infi
a	PRE	a	f PRE
Malvaglia	NPR	<unknown>	s NOUN NPR VER:fin
.	SENT	.	f SENT
Quando	WH	quando	f WH
siamo	AUX:fin	essere	f AUX:fin VER:fin
arrivati	VER:ppast	arrivare	f ADJ VER:ppast
circa	ADV	circa	f ADV PRE
a	PRE	a	f PRE
metá	ADJ	<unknown>	s ADJ NOCAT VER:fin
abbiamo	VER:fin	avere	f AUX:fin VER:fin
visto	NOUN	visto	f NOUN VER:fin
che	CHE	che	f CHE CON DET:wh
stava	VER:fin	stare	f VER:fin
arrivando	VER:geru	arrivare	f VER:geru
il	ART	il	f ART
bus	NOUN	bus	f NOUN
con	PRE	con	f PRE
sopra	ADV	sopra	f ADJ ADV PRE
tutti	DET:indef	tutto	f ADJ DET:indef PRO:indef
i	ART	il	f ART
bambini	NOUN	bambino	f NOUN
,	PUN	,	f PUN
dopo	PRE	dopo	f ADJ ADV CON PRE
un	ART	un	f ART
po	ADJ	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
’	NOCAT	<unknown>	s NOCAT
i	ART	il	f ART
bambini	NOUN	bambino	f NOUN
sono	AUX:fin	essere	f AUX:fin VER:fin
scesi	VER:ppast	scendere	f VER:fin VER:ppast
dal	ARTPRE	dal	f ARTPRE
bus	NOUN	bus	f NOUN
sono	AUX:fin	essere	f AUX:fin VER:fin
stati	AUX:ppast	essere	f AUX:ppast NOUN VER:ppast
li	CLI	li	f CLI
un	ART	un	f ART
po	NOUN	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
’	NOCAT	<unknown>	s NOCAT
a	PRE	a	f PRE
giocare	VER:infi	giocare	f VER:infi
a	PRE	a	f PRE
buttarsi	VER:infi:cli	buttare	f VER:infi:cli
addosso	VER:fin	addossare	f VER:fin
le	ART	la	f ART CLI
palle	NOUN	palla	f NOUN
di	PRE	di	f PRE
neve	NOUN	neve	f NOUN
,	PUN	,	f PUN
dopo	PRE	dopo	f ADJ ADV CON PRE
un	ART	un	f ART
po	ADJ	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
’	NOCAT	<unknown>	s NOCAT
una	ART	una	f ART PRO:indef
bambina	NOUN	bambina	f NOUN
di	PRE	di	f PRE
nome	NOUN	nome	f NOUN
Lisa	NPR	Lisa	f NPR
é	AUX:fin	essere	f AUX:fin VER:fin
scappata	VER:ppast	scappare	f VER:ppast
dai	ARTPRE	dal	f ARTPRE VER:fin
suoi	DET:poss	suo	f DET:poss PRO:poss
amici	NOUN	amico	f ADJ NOUN
,	PUN	,	f PUN
e	CON	e	f CON
voleva	VER2:fin	volere	f VER2:fin VER:fin
attraversare	VER:infi	attraversare	f VER:infi
la	ART	la	f ART CLI
strada	NOUN	strada	f NOUN
,	PUN	,	f PUN
ma	CON	ma	f CON
per	PRE	per	f PRE
sbaglio	NOUN	sbaglio	f NOUN VER:fin
non	NEG	non	f NEG
ha	AUX:fin	avere	f AUX:fin VER:fin
guardato	VER:ppast	guardare	f VER:ppast
la	ART	la	f ART CLI
strada	NOUN	strada	f NOUN
e	CON	e	f CON
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
hanno	AUX:fin	avere	f AUX:fin VER:fin
schiacciata	VER:ppast	schiacciare	f ADJ NOUN VER:ppast
.	SENT	.	f SENT
Il	ART	il	f ART
mio	DET:poss	mio	f DET:poss PRO:poss
insegnamento	NOUN	insegnamento	f NOUN
é	VER:fin	essere	f AUX:fin VER:fin
che	CHE	che	f CHE CON DET:wh
quando	WH	quando	f WH
vuoi	VER2:fin	volere	f VER2:fin VER:fin
attraversare	VER:infi	attraversare	f VER:infi
la	ART	la	f ART CLI
strada	NOUN	strada	f NOUN
,	PUN	,	f PUN
devi	VER2:fin	dovere	f VER2:fin VER:fin
sempre	ADV	sempre	f ADV
guardare	VER:infi	guardare	f VER:infi
anche	ADV	anche	f ADV
se	CON	se	f CLI CON
si	CLI	si	f CLI
é	VER:fin	essere	f AUX:fin VER:fin
di	PRE	di	f PRE
fretta	NOUN	fretta	f NOUN
.	SENT	.	f SENT

Una	ART	una	f ART PRO:indef
discesa	NOUN	discesa	f NOUN VER:ppast
Un	VER:fin	<unknown>	s NPR VER:fin
giorno	NOUN	giorno	f NOUN
sono	AUX:fin	essere	f AUX:fin VER:fin
andato	VER:ppast	andare	f ADJ VER:ppast
a	PRE	a	f PRE
fare	VER:infi	fare	f VER2:infi VER:infi
un	ART	un	f ART
giro	NOUN	giro	f NOUN VER:fin
in	PRE	in	f PRE
bicicletta	NOUN	bicicletta	f NOUN
ero	VER:fin	essere	f AUX:fin AUX:geru AUX:ppre VER:fin
molto	ADV	molto	f ADJ ADV DET:indef
stanco	ADJ	stanco	f ADJ VER:fin
perché	WH	perché	f WH
ho	AUX:fin	avere	f AUX:fin VER:fin
fatto	VER:ppast	fare	f NOUN VER2:ppast VER:ppast
una	ART	una	f ART PRO:indef
salita	NOUN	salita	f NOUN VER:ppast
molto	ADV	molto	f ADJ ADV DET:indef
lunga	ADJ	lungo	f ADJ
e	CON	e	f CON
dopo	ADV	dopo	f ADJ ADV CON PRE
mi	CLI	mi	f CLI
sono	AUX:fin	essere	f AUX:fin VER:fin
fermato	VER:ppast	fermare	f NOUN VER:ppast
per	PRE	per	f PRE
riposarmi	VER:infi:cli	riposare	f VER:infi:cli
dopo	PRE	dopo	f ADJ ADV CON PRE
un	ART	un	f ART
pò	PRO:indef	po'	f PRO:indef
mi	CLI	mi	f CLI
sono	AUX:fin	essere	f AUX:fin VER:fin
ancora	ADV	ancora	f ADV VER:fin
messo	VER:ppast	mettere	f ADJ VER:ppast
in	PRE	in	f PRE
bicicletta	NOUN	bicicletta	f NOUN
dopo	PRE	dopo	f ADJ ADV CON PRE
un	ART	un	f ART
qualche	DET:indef	qualche	f ADJ DET:indef
chilometro	NOUN	chilometro	f NOUN
ho	VER:fin	avere	f AUX:fin VER:fin
visto	NOUN	visto	f NOUN VER:fin
una	ART	una	f ART PRO:indef
discesa	NOUN	discesa	f NOUN VER:ppast
e	CON	e	f CON
mia	DET:poss	mio	f DET:poss PRO:poss
mamma	NOUN	mamma	f NOUN
mi	CLI	mi	f CLI
ha	AUX:fin	avere	f AUX:fin VER:fin
detto	VER:ppast	dire	f VER:fin VER:ppast
di	PRE	di	f PRE
non	NEG	non	f NEG
scendere	VER:infi	scendere	f VER:infi
e	CON	e	f CON
io	PRO:pers	io	f PRO:pers
sono	AUX:fin	essere	f AUX:fin VER:fin
sceso	VER:ppast	scendere	f VER:ppast
iniziavo	VER:fin	iniziare	f VER:fin
a	PRE	a	f PRE
perdere	VER:infi	perdere	f VER:infi
il	ART	il	f ART
controllo	NOUN	controllo	f NOUN VER:fin
della	ARTPRE	della	f ARTPRE
bici	NOUN	bici	f NOUN
e	CON	e	f CON
poi	ADV	poi	f ADV
sono	AUX:fin	essere	f AUX:fin VER:fin
caduto	VER:ppast	cadere	f VER:ppast
sul	ARTPRE	sul	f ARTPRE
catrame	NOUN	catrame	f NOUN
e	CON	e	f CON
mi	CLI	mi	f CLI
sono	AUX:fin	essere	f AUX:fin VER:fin
spellato	VER:ppast	spellare	f VER:ppast
tutto	PRO:indef	tutto	f ADJ DET:indef PRO:indef
e	CON	e	f CON
ho	AUX:fin	avere	f AUX:fin VER:fin
capito	VER:ppast	capire	f VER:fin VER:ppast
che	CHE	che	f CHE CON DET:wh
bisogna	VER:fin	bisognare	f VER2:fin VER:fin
ascoltare	VER:infi	ascoltare	f VER:infi
la	ART	la	f ART CLI
mamma	NOUN	mamma	f NOUN
perché	WH	perché	f WH
senó	ADJ	<unknown>	s ADJ NOCAT VER:fin
si	CLI	si	f CLI
pagano	VER:fin	pagare	f ADJ NOUN VER:fin
le	ART	la	f ART CLI
conseguenze	NOUN	conseguenza	f NOUN
.	SENT	.	f SENT

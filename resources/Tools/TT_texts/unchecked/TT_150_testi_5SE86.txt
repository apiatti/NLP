Mai	ADV	mai	f ADV
sottovalutare	VER:infi	sottovalutare	f VER:infi
gli	ART	il	f ART CLI
avversari	NOUN	avversario	f NOUN
Ero	VER:fin	essere	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
in	PRE	in	f PRE
partenza	NOUN	partenza	f NOUN
per	PRE	per	f PRE
un	ART	un	f ART
torneo	NOUN	torneo	f NOUN VER:fin
a	PRE	a	f PRE
Chiasso	NPR	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
.	SENT	.	f SENT
Arrivati	VER:ppast	arrivare	f ADJ VER:ppast
sono	AUX:fin	essere	f AUX:fin VER:fin
andato	VER:ppast	andare	f ADJ VER:ppast
negli	ARTPRE	nel	f ARTPRE
spogliatoi	NOUN	spogliatoio	f NOUN
e	CON	e	f CON
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
erano	VER:fin	essere	f AUX:fin VER:fin
tutti	DET:indef	tutto	f ADJ DET:indef PRO:indef
i	ART	il	f ART
miei	DET:poss	mio	f DET:poss PRO:poss
compagni	NOUN	compagno	f NOUN
di	PRE	di	f PRE
hockei	NOUN	<unknown>	s ADJ NOUN VER:fin
.	SENT	.	f SENT
Siamo	AUX:fin	essere	f AUX:fin VER:fin
andati	VER:ppast	andare	f ADJ VER:ppast
a	PRE	a	f PRE
fare	VER:infi	fare	f VER2:infi VER:infi
riscaldamento	NOUN	riscaldamento	f NOUN
di	PRE	di	f PRE
fuori	ADV	fuori	f ADV PRE
sul	ARTPRE	sul	f ARTPRE
piazzale	NOUN	piazzale	f NOUN VER:fin:cli
.	SENT	.	f SENT
La	ART	la	f ART CLI
prima	ADJ	primo	f ADJ ADV
partita	NOUN	partita	f ADJ NOUN VER:ppast
era	VER:fin	essere	f AUX:fin NOUN VER:fin
contro	PRE	contro	f ADV PRE
il	ART	il	f ART
Chiasso	NPR	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
e	CON	e	f CON
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
vinto	VER:ppast	vincere	f ADJ VER:ppast
cinque	DET:num	cinque	f DET:num PRO:num
a	PRE	a	f PRE
tre	DET:num	tre	f DET:num PRO:num
.	SENT	.	f SENT
La	ART	la	f ART CLI
seconda	ADJ	secondo	f ADJ NOUN VER:fin
partita	NOUN	partita	f ADJ NOUN VER:ppast
era	VER:fin	essere	f AUX:fin NOUN VER:fin
contro	PRE	contro	f ADV PRE
L’	NPR	<unknown>	s ADJ NOCAT NPR
ascona	VER:fin	<unknown>	s VER:fin VER:infi VER:ppre
,	PUN	,	f PUN
finita	VER:ppast	finire	f ADJ VER:ppast
la	ART	la	f ART CLI
partita	NOUN	partita	f ADJ NOUN VER:ppast
ne	CLI	ne	f CLI
iniziava	VER:fin	iniziare	f VER:fin
un’	ADJ	<unknown>	s ADJ NOCAT VER:fin
altra	PRO:indef	altra	f ADJ DET:indef PRO:indef
contro	PRE	contro	f ADV PRE
il	ART	il	f ART
Lugano	NPR	Lugano	f NPR
un	ART	un	f ART
derbi	ADJ	<unknown>	s ADJ NOUN VER:fin VER:infi VER:ppast VER:ppre
.	SENT	.	f SENT
Alla	ARTPRE	alla	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
e	CON	e	f CON
finita	VER:ppast	finire	f ADJ VER:ppast
sette	DET:num	sette	f DET:num NOUN PRO:num
ha	VER:fin	avere	f AUX:fin VER:fin
due	DET:num	due	f DET:num PRO:num
a	PRE	a	f PRE
vinto	VER:ppast	vincere	f ADJ VER:ppast
il	ART	il	f ART
Lugano	NPR	Lugano	f NPR
,	PUN	,	f PUN
siamo	AUX:fin	essere	f AUX:fin VER:fin
arrivati	VER:ppast	arrivare	f ADJ VER:ppast
secondi	ADJ	secondo	f ADJ NOUN VER:fin
adesso	ADV	adesso	f ADV
mi	CLI	mi	f CLI
rendo	VER:fin	rendere	f VER:fin
conto	NOUN	conto	f NOUN VER:fin
che	CHE	che	f CHE CON DET:wh
Mai	VER:fin	<unknown>	s NPR VER:fin
sottovalutare	VER:infi	sottovalutare	f VER:infi
gli	ART	il	f ART CLI
avversari	NOUN	avversario	f NOUN
.	SENT	.	f SENT

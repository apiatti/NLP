Un	ART	un	f ART
giorno	NOUN	giorno	f NOUN
mentre	CON	mentre	f CON
stavo	VER:fin	stare	f VER:fin
facendo	VER:geru	fare	f VER2:geru VER:geru
un	ART	un	f ART
gioco	NOUN	gioco	f NOUN VER:fin
persi	VER:fin	perdere	f ADJ VER:fin
un	ART	un	f ART
pezzo	NOUN	pezzo	f NOUN
.	SENT	.	f SENT
Cominciai	VER:fin	cominciare	f VER:fin
a	PRE	a	f PRE
cercarlo	VER:infi:cli	cercare	f VER:infi:cli
sotto	PRE	sotto	f ADJ ADV PRE
il	ART	il	f ART
tavolo	NOUN	tavolo	f NOUN
,	PUN	,	f PUN
sotto	PRE	sotto	f ADJ ADV PRE
la	ART	la	f ART CLI
sedia	NOUN	sedia	f NOUN
,	PUN	,	f PUN
in	PRE	in	f PRE
cucina	NOUN	cucina	f NOUN VER:fin
,	PUN	,	f PUN
in	PRE	in	f PRE
salotto	NOUN	salotto	f NOUN
,	PUN	,	f PUN
in	PRE	in	f PRE
camera	NOUN	camera	f NOUN
da	PRE	da	f PRE
letto	NOUN	letto	f ADJ NOUN VER:ppast
,	PUN	,	f PUN
in	PRE	in	f PRE
cantina	NOUN	cantina	f NOUN
e	CON	e	f CON
persino	ADV	persino	f ADV
in	PRE	in	f PRE
bagno	NOUN	bagno	f NOUN VER:fin
.	SENT	.	f SENT
Non	NEG	non	f NEG
lo	CLI	lo	f ART CLI
trovavo	VER:fin	trovare	f VER:fin
da	PRE	da	f PRE
nessuna	DET:indef	nessun	f ADJ DET:indef PRO:indef
parte	NOUN	parte	f NOUN VER:fin
,	PUN	,	f PUN
ero	AUX:fin	essere	f AUX:fin AUX:geru AUX:ppre VER:fin
disperato	VER:ppast	disperare	f ADJ NOUN VER:ppast
non	NEG	non	f NEG
sapevo	VER:fin	sapere	f VER:fin
piú	ADV	più	f ADV
dove	WH	dove	f WH
cercare	VER:infi	cercare	f VER:infi
.	SENT	.	f SENT
Ogni	DET:indef	ogni	f ADJ DET:indef
giorno	NOUN	giorno	f NOUN
dal	ARTPRE	dal	f ARTPRE
lunedí	NOUN	<unknown>	s ADJ NOCAT NOUN
al	ARTPRE	al	f ARTPRE
venerdí	ADJ	<unknown>	s ADJ NOCAT VER:fin
cercavo	VER:fin	cercare	f VER:fin
in	PRE	in	f PRE
un	ART	un	f ART
luogo	NOUN	luogo	f NOUN
diverso	ADJ	diverso	f ADJ
.	SENT	.	f SENT
Lunedí	NPR	<unknown>	s ADJ NOCAT NOUN NPR
cercai	VER:fin	cercare	f VER:fin
in	PRE	in	f PRE
cucina	NOUN	cucina	f NOUN VER:fin
ma	CON	ma	f CON
non	NEG	non	f NEG
trovai	VER:fin	trovare	f VER:fin
niente	PRO:indef	niente	f ADJ ADV PRO:indef
.	SENT	.	f SENT
Martedí	NPR	<unknown>	s ADJ NOCAT NOUN NPR VER:fin VER:geru VER:infi VER:infi:cli VER:ppast VER:ppre
cercai	VER:fin	cercare	f VER:fin
in	PRE	in	f PRE
bagno	NOUN	bagno	f NOUN VER:fin
ma	CON	ma	f CON
anche	ADV	anche	f ADV
lí	ADJ	<unknown>	s ADJ NOCAT VER:fin
non	NEG	non	f NEG
trovai	VER:fin	trovare	f VER:fin
nulla	PRO:indef	nulla	f ADJ PRO:indef
.	SENT	.	f SENT
Mercoledí	NPR	<unknown>	s ADJ NOCAT NOUN NPR
cercai	VER:fin	cercare	f VER:fin
in	PRE	in	f PRE
salotto	NOUN	salotto	f NOUN
ma	CON	ma	f CON
anche	ADV	anche	f ADV
in	PRE	in	f PRE
salotto	NOUN	salotto	f NOUN
non	NEG	non	f NEG
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
era	VER:fin	essere	f AUX:fin NOUN VER:fin
niente	PRO:indef	niente	f ADJ ADV PRO:indef
.	SENT	.	f SENT
Giovedí	NPR	<unknown>	s ADJ NOCAT NPR VER:fin
cercai	VER:fin	cercare	f VER:fin
in	PRE	in	f PRE
camera	NOUN	camera	f NOUN
da	PRE	da	f PRE
le	ART	la	f ART CLI
ma	CON	ma	f CON
anche	ADV	anche	f ADV
lí	ADJ	<unknown>	s ADJ NOCAT VER:fin
non	NEG	non	f NEG
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
era	VER:fin	essere	f AUX:fin NOUN VER:fin
nulla	PRO:indef	nulla	f ADJ PRO:indef
.	SENT	.	f SENT
Venerdí	NPR	<unknown>	s ADJ NOCAT NPR VER:fin
mentre	CON	mentre	f CON
cercavo	VER:fin	cercare	f VER:fin
in	PRE	in	f PRE
cantina	NOUN	cantina	f NOUN
non	NEG	non	f NEG
ho	AUX:fin	avere	f AUX:fin VER:fin
trovato	VER:ppast	trovare	f VER:ppast
niente	PRO:indef	niente	f ADJ ADV PRO:indef
.	SENT	.	f SENT
Sabato	NOUN	sabato	f NOUN
sera	NOUN	sera	f NOUN
mentre	CON	mentre	f CON
andavo	VER:fin	andare	f VER:fin
a	PRE	a	f PRE
dormire	VER:infi	dormire	f VER:infi
ho	AUX:fin	avere	f AUX:fin VER:fin
trovato	VER:ppast	trovare	f VER:ppast
quel	DET:demo	quello	f DET:demo
pezzo	NOUN	pezzo	f NOUN
che	CHE	che	f CHE CON DET:wh
cercavo	VER:fin	cercare	f VER:fin
da	PRE	da	f PRE
giorni	NOUN	giorno	f NOUN
.	SENT	.	f SENT
Questo	DET:demo	questo	f DET:demo PRO:demo
episodio	NOUN	episodio	f NOUN
mi	CLI	mi	f CLI
ha	AUX:fin	avere	f AUX:fin VER:fin
insegnato	VER:ppast	insegnare	f VER:ppast
a	PRE	a	f PRE
cercare	VER:infi	cercare	f VER:infi
meglio	ADV	meglio	f ADJ ADV NOUN
.	SENT	.	f SENT

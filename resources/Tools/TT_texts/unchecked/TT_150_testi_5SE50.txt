Un	ART	un	f ART
pesciolino	NOUN	pesciolino	f NOUN
nel	ARTPRE	nel	f ARTPRE
mare	NOUN	mare	f NOUN
Un	VER:fin	<unknown>	s NPR VER:fin
giorno	NOUN	giorno	f NOUN
a	PRE	a	f PRE
casa	NOUN	casa	f NOUN
partiamo	VER:fin	partire	f VER:fin
per	PRE	per	f PRE
la	ART	la	f ART CLI
Croazia	NPR	Croazia	f NPR
,	PUN	,	f PUN
con	PRE	con	f PRE
una	ART	una	f ART PRO:indef
macchina	NOUN	macchina	f NOUN VER:fin
:	PUN	:	f PUN
con	PRE	con	f PRE
tante	DET:indef	tanto	f ADJ DET:indef PRO:indef
cose	NOUN	cosa	f NOUN
valigie	NOUN	valigia	f NOUN
medicine	NOUN	medicina	f NOUN
e	CON	e	f CON
cibo	NOUN	cibo	f NOUN VER:fin
ecc	ADJ	<unknown>	s ADJ NOUN NPR VER:fin
.	SENT	.	f SENT
,	PUN	,	f PUN
era	VER:fin	essere	f AUX:fin NOUN VER:fin
piento	ADV:mente	<unknown>	s ADV:mente NOUN VER:fin
il	ART	il	f ART
baule	NOUN	baule	f NOUN
.	SENT	.	f SENT
Arrivati	VER:ppast	arrivare	f ADJ VER:ppast
al	ARTPRE	al	f ARTPRE
traghetto	NOUN	traghetto	f ADJ NOUN VER:fin
non	NEG	non	f NEG
sapevamo	VER:fin	sapere	f VER:fin
dei	ARTPRE	del	f ARTPRE NOUN
biglietti	NOUN	biglietto	f NOUN
e	CON	e	f CON
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
saltato	VER:ppast	saltare	f VER:ppast
il	ART	il	f ART
primo	ADJ	primo	f ADJ NOUN
traghetto	NOUN	traghetto	f ADJ NOUN VER:fin
e	CON	e	f CON
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
preso	VER:ppast	prendere	f ADJ VER:ppast
il	ART	il	f ART
secondo	ADJ	secondo	f ADJ NOUN PRE VER:fin
,	PUN	,	f PUN
arrivati	VER:ppast	arrivare	f ADJ VER:ppast
avevamo	VER:fin	avere	f AUX:fin VER:fin
ancora	ADV	ancora	f ADV VER:fin
un	ART	un	f ART
problemino	NOUN	problemino	f NOUN
trovare	VER:infi	trovare	f VER:infi
la	ART	la	f ART CLI
casa	NOUN	casa	f NOUN
.	SENT	.	f SENT
Il	ART	il	f ART
signore	NOUN	signora|signore	f NOUN
ci	CLI	ci	f CLI
aveva	AUX:fin	avere	f AUX:fin VER:fin
detto	VER:ppast	dire	f VER:fin VER:ppast
che	CHE	che	f CHE CON DET:wh
era	VER:fin	essere	f AUX:fin NOUN VER:fin
bianca	ADJ	bianco	f ADJ NOUN
vicino	ADJ	vicino	f ADJ NOUN
ad	PRE	ad	f PRE
un	ART	un	f ART
cancello	NOUN	cancello	f NOUN VER:fin
due	DET:num	due	f DET:num PRO:num
palme	NOUN	palma	f NOUN
e	CON	e	f CON
che	CHE	che	f CHE CON DET:wh
era	VER:fin	essere	f AUX:fin NOUN VER:fin
a	PRE	a	f PRE
100m	NOUN	<unknown>	s ADJ NOCAT NOUN NPR
dal	ARTPRE	dal	f ARTPRE
mare	NOUN	mare	f NOUN
.	SENT	.	f SENT
In	PRE	in	f PRE
una	ART	una	f ART PRO:indef
salita	NOUN	salita	f NOUN VER:ppast
mio	DET:poss	mio	f DET:poss PRO:poss
papá	ADJ	<unknown>	s ADJ NOCAT VER:fin
vide	VER:fin	vedere	f VER:fin
due	DET:num	due	f DET:num PRO:num
palme	NOUN	palma	f NOUN
ci	CLI	ci	f CLI
siamo	AUX:fin	essere	f AUX:fin VER:fin
andati	VER:ppast	andare	f ADJ VER:ppast
a	PRE	a	f PRE
piedi	NOUN	piede	f NOUN
ma	CON	ma	f CON
non	NEG	non	f NEG
era	VER:fin	essere	f AUX:fin NOUN VER:fin
quella	PRO:demo	quella	f DET:demo PRO:demo
poi	ADV	poi	f ADV
io	PRO:pers	io	f PRO:pers
piú	ADV	più	f ADV
in	PRE	in	f PRE
sú	NOUN	<unknown>	s ADJ NOCAT NOUN
o	CON	o	f CON
visto	NOUN	visto	f NOUN VER:fin
due	DET:num	due	f DET:num PRO:num
palme	NOUN	palma	f NOUN
ed	CON	ed	f CON
era	VER:fin	essere	f AUX:fin NOUN VER:fin
quella	PRO:demo	quella	f DET:demo PRO:demo
giusta	ADJ	giusto	f ADJ
.	SENT	.	f SENT
Messi	VER:ppast	mettere	f ADJ NOUN VER:ppast
tutto	PRO:indef	tutto	f ADJ DET:indef PRO:indef
a	PRE	a	f PRE
posto	NOUN	posto	f ADJ NOUN VER:fin VER:ppast
avevamo	AUX:fin	avere	f AUX:fin VER:fin
preso	VER:ppast	prendere	f ADJ VER:ppast
le	ART	la	f ART CLI
maschere	NOUN	maschera	f NOUN
da	PRE	da	f PRE
sonorkelin	VER:fin	<unknown>	s VER:fin
e	CON	e	f CON
mi	CLI	mi	f CLI
tuffai	VER:fin	tuffare	f VER:fin
per	PRE	per	f PRE
vedere	VER:infi	vedere	f VER:infi
i	ART	il	f ART
pesci	NOUN	pesce	f NOUN
con	PRE	con	f PRE
la	ART	la	f ART CLI
maschera	NOUN	maschera	f NOUN VER:fin
e	CON	e	f CON
vidi	VER:fin	vedere	f VER:fin
un	ART	un	f ART
pesciolino	NOUN	pesciolino	f NOUN
giallo	ADJ	giallo	f ADJ NOUN
ma	CON	ma	f CON
era	VER:fin	essere	f AUX:fin NOUN VER:fin
diverso	ADJ	diverso	f ADJ
da	PRE	da	f PRE
gli	ART	il	f ART CLI
altri	PRO:indef	altri	f ADJ DET:indef PRO:indef
aveva	VER:fin	avere	f AUX:fin VER:fin
la	ART	la	f ART CLI
testa	NOUN	testa	f NOUN VER:fin
nera	ADJ	nero	f ADJ
e	CON	e	f CON
la	ART	la	f ART CLI
coda	NOUN	coda	f NOUN
bianca	ADJ	bianco	f ADJ NOUN
e	CON	e	f CON
non	NEG	non	f NEG
aveva	VER:fin	avere	f AUX:fin VER:fin
amici	NOUN	amico	f ADJ NOUN
era	VER:fin	essere	f AUX:fin NOUN VER:fin
tutto	PRO:indef	tutto	f ADJ DET:indef PRO:indef
solo	ADV	solo	f ADJ ADV VER:fin
nascosto	VER:ppast	nascondere	f ADJ VER:ppast
in	PRE	in	f PRE
mezzo	NOUN	mezzo	f ADJ NOUN
ad	PRE	ad	f PRE
uno	ART	un	f ART NOUN PRO:indef
scoglio	NOUN	scoglio	f NOUN
.	SENT	.	f SENT
E	CON	e	f CON
mi	CLI	mi	f CLI
ha	AUX:fin	avere	f AUX:fin VER:fin
fatto	VER2:ppast	fare	f NOUN VER2:ppast VER:ppast
capire	VER:infi	capire	f VER:infi
che	CHE	che	f CHE CON DET:wh
i	ART	il	f ART
diversi	ADJ	diverso	f ADJ
possono	VER2:fin	potere	f VER2:fin VER:fin
stare	VER:infi	stare	f ADV VER:infi
con	PRE	con	f PRE
noi	PRO:pers	noi	f PRO:pers
.	SENT	.	f SENT

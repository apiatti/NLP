La	ART	la	f ART CLI
lepre	NOUN	lepre	f NOUN
e	CON	e	f CON
la	ART	la	f ART CLI
tartaruga	NOUN	tartaruga	f NOUN
Quando	NPR	<unknown>	s NPR VER:geru
ho	AUX:fin	avere	f AUX:fin VER:fin
cominciato	VER:ppast	cominciare	f VER:ppast
a	PRE	a	f PRE
sentire	VER:infi	sentire	f VER:infi
la	ART	la	f ART CLI
favola	NOUN	favola	f NOUN
della	ARTPRE	della	f ARTPRE
lepre	NOUN	lepre	f NOUN
e	CON	e	f CON
della	ARTPRE	della	f ARTPRE
tartaruga	NOUN	tartaruga	f NOUN
ho	AUX:fin	avere	f AUX:fin VER:fin
imparato	VER:ppast	imparare	f VER:ppast
che	CHE	che	f CHE CON DET:wh
non	NEG	non	f NEG
si	CLI	si	f CLI
deve	VER2:fin	dovere	f VER2:fin VER:fin
sempre	ADV	sempre	f ADV
essere	AUX:infi	essere	f AUX:infi NOUN VER:infi
sicuri	ADJ	sicuro	f ADJ
di	PRE	di	f PRE
sé	PRO:pers	sé	f PRO:pers
ma	CON	ma	f CON
avere	AUX:infi	avere	f AUX:infi NOUN VER:infi
fede	NOUN	fede	f NOUN
è	AUX:fin	essere	f AUX:fin VER:fin
successo	VER:ppast	succedere	f NOUN VER:ppast
proprio	ADV	proprio	f ADJ ADV DET:poss PRO:poss
così	ADV	così	f ADV
la	ART	la	f ART CLI
lepre	NOUN	lepre	f NOUN
prendeva	VER:fin	prendere	f VER:fin
in	PRE	in	f PRE
giro	NOUN	giro	f NOUN VER:fin
la	ART	la	f ART CLI
tartaruga	NOUN	tartaruga	f NOUN
perchè	WH	perché	f WH
era	VER:fin	essere	f AUX:fin NOUN VER:fin
lenta	ADJ	lento	f ADJ
e	CON	e	f CON
questa	DET:demo	questo	f DET:demo PRO:demo
lepre	NOUN	lepre	f NOUN
era	VER:fin	essere	f AUX:fin NOUN VER:fin
sicura	ADJ	sicuro	f ADJ
di	PRE	di	f PRE
sé	PRO:pers	sé	f PRO:pers
mentre	CON	mentre	f CON
la	ART	la	f ART CLI
tartaruga	NOUN	tartaruga	f NOUN
era	VER:fin	essere	f AUX:fin NOUN VER:fin
fedele	ADJ	fedele	f ADJ NOUN
ed	CON	ed	f CON
ha	AUX:fin	avere	f AUX:fin VER:fin
avuto	VER:ppast	avere	f AUX:ppast VER:ppast
la	ART	la	f ART CLI
vittoria	NOUN	vittoria	f NOUN
.	SENT	.	f SENT
Ad	PRE	ad	f PRE
un	ART	un	f ART
certo	ADJ	certo	f ADJ ADV
punto	NOUN	punto	f ADJ ADV NOUN VER:fin VER:ppast
la	ART	la	f ART CLI
lepre	NOUN	lepre	f NOUN
era	VER:fin	essere	f AUX:fin NOUN VER:fin
così	ADV	così	f ADV
sicura	ADJ	sicuro	f ADJ
della	ARTPRE	della	f ARTPRE
vittoria	NOUN	vittoria	f NOUN
che	CHE	che	f CHE CON DET:wh
si	CLI	si	f CLI
addormentò	VER:fin	addormentare	f VER:fin
la	ART	la	f ART CLI
tartaruga	NOUN	tartaruga	f NOUN
la	CLI	la	f ART CLI
raggiunse	VER:fin	raggiungere	f VER:fin
,	PUN	,	f PUN
la	CLI	la	f ART CLI
superò	VER:fin	superare	f VER:fin
e	CON	e	f CON
arrivò	VER:fin	arrivare	f VER:fin
al	ARTPRE	al	f ARTPRE
traguardo	NOUN	traguardo	f NOUN
.	SENT	.	f SENT
Invece	ADV	invece	f ADV
la	ART	la	f ART CLI
lepre	NOUN	lepre	f NOUN
caduta	NOUN	caduta	f NOUN VER:ppast
nel	ARTPRE	nel	f ARTPRE
sonno	NOUN	sonno	f NOUN
si	CLI	si	f CLI
svegliò	VER:fin	svegliare	f VER:fin
troppo	ADV	troppo	f ADJ ADV DET:indef
tardi	ADJ	tardo	f ADJ ADV VER:fin
.	SENT	.	f SENT

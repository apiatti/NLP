I	ART	il	f ART
pesci	NOUN	pesce	f NOUN
troppo	ADV	troppo	f ADJ ADV DET:indef
piccoli	ADJ	piccolo	f ADJ NOUN
Stavo	VER:fin	stare	s NPR VER:fin
pescando	VER:geru	pescare	f VER:geru
in	PRE	in	f PRE
uno	ART	un	f ART NOUN PRO:indef
stagno	NOUN	stagno	f ADJ NOUN VER:fin
vicino	ADJ	vicino	f ADJ NOUN
a	PRE	a	f PRE
Biasca	NPR	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
,	PUN	,	f PUN
la	ART	la	f ART CLI
fortuna	NOUN	fortuna	f NOUN
non	NEG	non	f NEG
stava	VER:fin	stare	f VER:fin
mai	ADV	mai	f ADV
dalla	ARTPRE	dalla	f ARTPRE
mia	DET:poss	mio	f DET:poss PRO:poss
parte	NOUN	parte	f NOUN VER:fin
.	SENT	.	f SENT
All’	NPR	<unknown>	s ADJ NOCAT NPR VER:fin
improvviso	ADJ	improvviso	f ADJ VER:fin
abboccò	VER:fin	abboccare	f VER:fin
un	ART	un	f ART
pesce	NOUN	pesce	f NOUN
,	PUN	,	f PUN
lo	CLI	lo	f ART CLI
tirai	VER:fin	tirare	f VER:fin
fuori	ADV	fuori	f ADV PRE
e	CON	e	f CON
vidi	VER:fin	vedere	f VER:fin
che	CHE	che	f CHE CON DET:wh
era	VER:fin	essere	f AUX:fin NOUN VER:fin
troppo	ADV	troppo	f ADJ ADV DET:indef
piccolo	ADJ	piccolo	f ADJ NOUN
.	SENT	.	f SENT
Mi	CLI	mi	f CLI
arrabbiai	VER:fin	arrabbiare	f VER:fin
,	PUN	,	f PUN
e	CON	e	f CON
allora	ADV	allora	f ADV
decisi	ADJ	deciso	f ADJ VER:fin VER:ppast
di	PRE	di	f PRE
prenderlo	VER:infi:cli	<unknown>	s ADJ NOUN VER:fin VER:fin:cli VER:geru VER:infi VER:infi:cli VER:ppast VER:ppre
comunque	WH	comunque	f WH
,	PUN	,	f PUN
fù	ADJ	<unknown>	s ADJ NOUN VER:fin
stata	VER:ppast	essere|stare	f AUX:ppast VER:ppast
la	ART	la	f ART CLI
prima	ADJ	primo	f ADJ ADV
volta	NOUN	volta	f ADJ NOUN VER:fin VER:ppast
che	CHE	che	f CHE CON DET:wh
infrangevo	VER:fin	infrangere	f VER:fin
le	ART	la	f ART CLI
regole	NOUN	regola	f NOUN
ma	CON	ma	f CON
non	NEG	non	f NEG
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
ultima	ADJ	ultimo	f ADJ NOUN VER:fin
,	PUN	,	f PUN
Così	NPR	<unknown>	s NPR VER:fin
andò	VER:fin	andare	f VER:fin
avanti	ADV	avanti	f ADJ ADV
per	PRE	per	f PRE
settimane	NOUN	settimana	f NOUN
,	PUN	,	f PUN
ma	CON	ma	f CON
giorno	NOUN	giorno	f NOUN
notai	NOUN	notaio	f NOUN VER:fin
che	CHE	che	f CHE CON DET:wh
non	NEG	non	f NEG
cen’erano	VER:fin	<unknown>	s ADJ NOUN VER:fin
più	ADV	più	f ADV
.	SENT	.	f SENT
Allora	ADV	allora	f ADV
non	NEG	non	f NEG
ci	CLI	ci	f CLI
tornai	VER:fin	tornare	f VER:fin
più	ADV	più	f ADV
aparte	ADJ	<unknown>	s ADJ VER:fin
una	ART	una	f ART PRO:indef
volta	NOUN	volta	f ADJ NOUN VER:fin VER:ppast
dopo	PRE	dopo	f ADJ ADV CON PRE
sei	DET:num	sei	f AUX:fin DET:num PRO:num VER:fin
mesi	NOUN	mese	f NOUN
per	PRE	per	f PRE
guardare	VER:infi	guardare	f VER:infi
se	CON	se	f CLI CON
erano	AUX:fin	essere	f AUX:fin VER:fin
tornati	VER:ppast	tornare	f VER:ppast
ma	CON	ma	f CON
non	NEG	non	f NEG
tornarono	VER:fin	tornare	f VER:fin
.	SENT	.	f SENT
Allora	ADV	allora	f ADV
decisi	ADJ	deciso	f ADJ VER:fin VER:ppast
di	PRE	di	f PRE
rimediare	VER:infi	rimediare	f VER:infi
,	PUN	,	f PUN
compra	VER:fin	comprare	f VER:fin
dei	ARTPRE	del	f ARTPRE NOUN
pesci	NOUN	pesce	f NOUN
vivi	ADJ	vivo	f ADJ VER:fin
e	CON	e	f CON
li	CLI	li	f CLI
misi	VER:fin	mettere	f VER:fin
nello	ARTPRE	nel	f ARTPRE
stagno	NOUN	stagno	f ADJ NOUN VER:fin
,	PUN	,	f PUN
preparai	VER:fin	preparare	f VER:fin
un	ART	un	f ART
cartello	NOUN	cartello	f NOUN
e	CON	e	f CON
da	PRE	da	f PRE
lì	ADV	lì	f ADV
diventò	VER:fin	diventare	f VER:fin
uno	ART	un	f ART NOUN PRO:indef
stagno	NOUN	stagno	f ADJ NOUN VER:fin
protetto	ADJ	protetto	f ADJ NOUN VER:ppast
.	SENT	.	f SENT
Da	PRE	da	f PRE
lì	ADV	lì	f ADV
imparai	VER:fin	imparare	f VER:fin
che	CHE	che	f CHE CON DET:wh
:	PUN	:	f PUN
“	NOCAT	<unknown>	s NOCAT
Chi	NPR	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
ha	AUX:fin	avere	f AUX:fin VER:fin
fretta	NOUN	fretta	f NOUN
i	ART	il	f ART
pesci	NOUN	pesce	f NOUN
non	NEG	non	f NEG
li	CLI	li	f CLI
piglia	VER:fin	pigliare	f VER:fin
a	PRE	a	f PRE
lungo	ADJ	lungo	f ADJ PRE
.	SENT	.	f SENT
”	NOCAT	<unknown>	s NOCAT

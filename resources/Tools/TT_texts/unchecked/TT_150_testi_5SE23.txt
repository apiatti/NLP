Un	ART	un	f ART
giorno	NOUN	giorno	f NOUN
la	ART	la	f ART CLI
nostra	DET:poss	nostro	f DET:poss PRO:poss
maestra	NOUN	maestra	f ADJ NOUN
ci	CLI	ci	f CLI
ha	AUX:fin	avere	f AUX:fin VER:fin
letto	VER:ppast	leggere	f ADJ NOUN VER:ppast
la	ART	la	f ART CLI
storia	NOUN	storia	f NOUN
della	ARTPRE	della	f ARTPRE
lepre	NOUN	lepre	f NOUN
e	CON	e	f CON
la	ART	la	f ART CLI
tartaruga	NOUN	tartaruga	f NOUN
e	CON	e	f CON
ci	CLI	ci	f CLI
ha	AUX:fin	avere	f AUX:fin VER:fin
chiesto	VER:ppast	chiedere	f ADJ VER:ppast
di	PRE	di	f PRE
pensare	VER:infi	pensare	f VER:infi
a	PRE	a	f PRE
un	ART	un	f ART
momento	NOUN	momento	f NOUN
dove	WH	dove	f WH
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
avuto	VER:ppast	avere	f AUX:ppast VER:ppast
un	ART	un	f ART
insegnamento	NOUN	insegnamento	f NOUN
,	PUN	,	f PUN
a	PRE	a	f PRE
me	PRO:pers	me	f CLI PRO:pers
e	CON	e	f CON
venuto	VER:ppast	venire	f ADJ AUX:ppast VER:ppast
in	PRE	in	f PRE
mente	NOUN	mente	f NOUN VER:fin
che	CHE	che	f CHE CON DET:wh
proprio	ADV	proprio	f ADJ ADV DET:poss PRO:poss
quel	DET:demo	quello	f DET:demo
giorno	NOUN	giorno	f NOUN
ero	AUX:fin	essere	f AUX:fin AUX:geru AUX:ppre VER:fin
seduta	VER:ppast	sedere	f NOUN VER:ppast
su	PRE	su	f PRE
un	ART	un	f ART
sasso	NOUN	sasso	f NOUN
e	CON	e	f CON
sono	AUX:fin	essere	f AUX:fin VER:fin
arrivati	VER:ppast	arrivare	f ADJ VER:ppast
li	CLI	li	f CLI
vicino	ADJ	vicino	f ADJ NOUN
dei	ARTPRE	del	f ARTPRE NOUN
bambini	NOUN	bambino	f NOUN
tre	DET:num	tre	f DET:num PRO:num
di	PRE	di	f PRE
prima	ADV	prima	f ADJ ADV
e	CON	e	f CON
uno	PRO:indef	uno	f ART NOUN PRO:indef
di	PRE	di	f PRE
seconda	ADJ	secondo	f ADJ NOUN VER:fin
,	PUN	,	f PUN
quello	PRO:demo	quello	f DET:demo PRO:demo
di	PRE	di	f PRE
seconda	ADJ	secondo	f ADJ NOUN VER:fin
e	CON	e	f CON
altri	DET:indef	altro	f ADJ DET:indef PRO:indef
due	DET:num	due	f DET:num PRO:num
di	PRE	di	f PRE
prima	ADV	prima	f ADJ ADV
picchiavano	VER:fin	picchiare	f VER:fin
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
altro	PRO:indef	altro	f ADJ DET:indef PRO:indef
di	PRE	di	f PRE
prima	ADV	prima	f ADJ ADV
io	PRO:pers	io	f PRO:pers
mi	CLI	mi	f CLI
sono	AUX:fin	essere	f AUX:fin VER:fin
messa	VER:ppast	mettere	f ADJ NOUN VER:ppast
in	PRE	in	f PRE
mezzo	NOUN	mezzo	f ADJ NOUN
dicendo	VER:geru	dire	f VER:geru
di	PRE	di	f PRE
smetterla	VER:infi:cli	smettere	f VER:infi:cli
ma	CON	ma	f CON
quello	PRO:demo	quello	f DET:demo PRO:demo
di	PRE	di	f PRE
seconda	ADJ	secondo	f ADJ NOUN VER:fin
continuava	VER:fin	continuare	f VER:fin
a	PRE	a	f PRE
dire	VER:infi	dire	f ADJ VER:infi
che	CHE	che	f CHE CON DET:wh
deve	VER2:fin	dovere	f VER2:fin VER:fin
esserci	AUX:infi:cli	essere	f AUX:infi:cli VER:infi:cli
rispetto	NOUN	rispetto	f NOUN PRE VER:fin
per	PRE	per	f PRE
i	ART	il	f ART
più	ADV	più	f ADV
grandi	ADJ	grande	f ADJ NOUN
ma	CON	ma	f CON
non	NEG	non	f NEG
capivo	VER:fin	capire	f VER:fin
perché	WH	perché	f WH
lo	CLI	lo	f ART CLI
dicesse	VER:fin	dire	f VER:fin
dopo	ADV	dopo	f ADJ ADV CON PRE
mi	CLI	mi	f CLI
hanno	AUX:fin	avere	f AUX:fin VER:fin
spiegato	VER:ppast	spiegare	f VER:ppast
che	CHE	che	f CHE CON DET:wh
cosa	WH	cosa	f NOUN WH
era	AUX:fin	essere	f AUX:fin NOUN VER:fin
successo	VER:ppast	succedere	f NOUN VER:ppast
alla	ARTPRE	alla	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
era	AUX:fin	essere	f AUX:fin NOUN VER:fin
successo	VER:ppast	succedere	f NOUN VER:ppast
che	CHE	che	f CHE CON DET:wh
il	ART	il	f ART
bambino	NOUN	bambino	f NOUN
di	PRE	di	f PRE
prima	ADV	prima	f ADJ ADV
aveva	AUX:fin	avere	f AUX:fin VER:fin
picchiato	VER:ppast	picchiare	f VER:ppast
la	ART	la	f ART CLI
sorella	NOUN	sorella	f NOUN
del	ARTPRE	del	f ARTPRE
bambino	NOUN	bambino	f NOUN
di	PRE	di	f PRE
seconda	ADJ	secondo	f ADJ NOUN VER:fin
allora	ADV	allora	f ADV
ho	AUX:fin	avere	f AUX:fin VER:fin
voluto	VER:ppast	volere	f ADJ VER2:ppre VER:ppast
difenderla	VER:infi:cli	<unknown>	s ADJ NOUN VER:fin VER:infi:cli VER:ppast
anche	ADV	anche	f ADV
se	CON	se	f CLI CON
la	ART	la	f ART CLI
sorella	NOUN	sorella	f NOUN
era	VER:fin	essere	f AUX:fin NOUN VER:fin
più	ADV	più	f ADV
grande	ADJ	grande	f ADJ NOUN
invece	ADV	invece	f ADV
quelli	PRO:demo	quelli	f DET:demo PRO:demo
diprima	VER:fin	<unknown>	s NOUN VER:fin
uno	PRO:indef	uno	f ART NOUN PRO:indef
ha	AUX:fin	avere	f AUX:fin VER:fin
detto	VER:ppast	dire	f VER:fin VER:ppast
che	CHE	che	f CHE CON DET:wh
non	NEG	non	f NEG
gli	CLI	gli	f ART CLI
ha	AUX:fin	avere	f AUX:fin VER:fin
fatto	VER:ppast	fare	f NOUN VER2:ppast VER:ppast
gnente	ADJ	<unknown>	s ADJ NOUN VER:fin VER:ppre
mentre	CON	mentre	f CON
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
altro	PRO:indef	altro	f ADJ DET:indef PRO:indef
ha	AUX:fin	avere	f AUX:fin VER:fin
detto	VER:ppast	dire	f VER:fin VER:ppast
che	CHE	che	f CHE CON DET:wh
mentre	CON	mentre	f CON
giocava	VER:fin	giocare	f VER:fin
a	PRE	a	f PRE
calcio	NOUN	calcio	f NOUN VER:fin
gli	CLI	gli	f ART CLI
ha	AUX:fin	avere	f AUX:fin VER:fin
tirato	VER:ppast	tirare	f ADJ VER:ppast
una	ART	una	f ART PRO:indef
pallonata	NOUN	pallonata	f NOUN
dopo	PRE	dopo	f ADJ ADV CON PRE
quello	PRO:demo	quello	f DET:demo PRO:demo
di	PRE	di	f PRE
seconda	ADJ	secondo	f ADJ NOUN VER:fin
ha	AUX:fin	avere	f AUX:fin VER:fin
detto	VER:ppast	dire	f VER:fin VER:ppast
ancora	ADV	ancora	f ADV VER:fin
che	CHE	che	f CHE CON DET:wh
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
è	VER:fin	essere	f AUX:fin VER:fin
bisogno	NOUN	bisogno	f NOUN VER2:fin VER:fin
di	PRE	di	f PRE
rispetto	NOUN	rispetto	f NOUN PRE VER:fin
per	PRE	per	f PRE
i	ART	il	f ART
piu	ADV	più	f ADV
grandi	ADJ	grande	f ADJ NOUN
dopo	CON	dopo	f ADJ ADV CON PRE
gli	CLI	gli	f ART CLI
ho	AUX:fin	avere	f AUX:fin VER:fin
detto	VER:ppast	dire	f VER:fin VER:ppast
che	CHE	che	f CHE CON DET:wh
se	CLI	se	f CLI CON
vuole	VER2:fin	volere	f VER2:fin VER:fin
essere	AUX:infi	essere	f AUX:infi NOUN VER:infi
considerat	NOUN	<unknown>	s ADJ NOUN NPR VER:fin
grande	ADJ	grande	f ADJ NOUN
di	PRE	di	f PRE
comportarsi	VER:infi:cli	comportare	f VER:infi:cli
come	WH	come	f WH
tale	ADJ	tale	f ADJ NOUN
,	PUN	,	f PUN
in	PRE	in	f PRE
un	ART	un	f ART
certo	ADJ	certo	f ADJ ADV
senso	NOUN	senso	f NOUN
o	CON	o	f CON
difeso	VER:ppast	difendere	f ADJ VER:ppast
il	ART	il	f ART
bambino	NOUN	bambino	f NOUN
che	CHE	che	f CHE CON DET:wh
è	AUX:fin	essere	f AUX:fin VER:fin
stato	AUX:ppast	essere	f AUX:ppast NOUN VER:ppast
attacato	VER:ppast	<unknown>	s ADJ NOUN VER:fin VER:ppast
quindi	ADV	quindi	f ADV
gli	ART	il	f ART CLI
altri	DET:indef	altro	f ADJ DET:indef PRO:indef
tre	DET:num	tre	f DET:num PRO:num
se	CON	se	f CLI CON
la	CLI	la	f ART CLI
son	AUX:fin	essere	f AUX:fin VER:fin
presi	VER:ppast	prendere	f ADJ VER:fin VER:ppast
con	PRE	con	f PRE
me	PRO:pers	me	f CLI PRO:pers
.	SENT	.	f SENT
INSEGNAMENTO	NOUN	insegnamento	f NOUN
Ho	VER:fin	avere	s NPR VER:fin
imparato	VER:ppast	imparare	f VER:ppast
che	CHE	che	f CHE CON DET:wh
in	PRE	in	f PRE
certe	ADJ	certo	f ADJ
faccende	NOUN	faccenda	f NOUN
é	VER:fin	essere	f AUX:fin VER:fin
meglio	ADV	meglio	f ADJ ADV NOUN
non	NEG	non	f NEG
intromettersi	VER:fin	<unknown>	s ADJ VER:fin VER:infi:cli VER:ppast
perché	WH	perché	f WH
se	CON	se	f CLI CON
no	ADV	no	f ADV NOUN
si	CLI	si	f CLI
paga	VER:fin	pagare	f ADJ NOUN VER:fin
come	WH	come	f WH
nel	ARTPRE	nel	f ARTPRE
mio	DET:poss	mio	f DET:poss PRO:poss
esempio	NOUN	esempio	f NOUN
tutti	PRO:indef	tutti	f ADJ DET:indef PRO:indef
eano	VER:fin	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
contro	ADV	contro	f ADV PRE
di	PRE	di	f PRE
me	PRO:pers	me	f CLI PRO:pers
.	SENT	.	f SENT

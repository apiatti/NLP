Un	ART	un	f ART
giorno	NOUN	giorno	f NOUN
d’	ADJ	<unknown>	s ADJ NOCAT VER:fin
estate	NOUN	estate	f NOUN
avevo	VER:fin	avere	f AUX:fin VER:fin
un	ART	un	f ART
allenamento	NOUN	allenamento	f NOUN
di	PRE	di	f PRE
ginnastica	NOUN	ginnastica	f ADJ NOUN
attrezzistica	ADJ	<unknown>	s ADJ NOUN VER:fin
.	SENT	.	f SENT
Entrai	VER:fin	entrare	f VER:fin
in	PRE	in	f PRE
palestra	NOUN	palestra	f NOUN
ma	CON	ma	f CON
il	ART	il	f ART
mio	DET:poss	mio	f DET:poss PRO:poss
monitore	NOUN	<unknown>	s ADJ NOUN VER:fin
non	NEG	non	f NEG
era	AUX:fin	essere	f AUX:fin NOUN VER:fin
ancora	ADV	ancora	f ADV VER:fin
arrivato	VER:ppast	arrivare	f ADJ VER:ppast
,	PUN	,	f PUN
però	ADV	però	f ADV
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
erano	VER:fin	essere	f AUX:fin VER:fin
già	ADV	già	f ADV
dei	ARTPRE	del	f ARTPRE NOUN
miei	DET:poss	mio	f DET:poss PRO:poss
compagni	NOUN	compagno	f NOUN
.	SENT	.	f SENT
Nella	ARTPRE	nella	f ARTPRE
palestra	NOUN	palestra	f NOUN
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
erano	AUX:fin	essere	f AUX:fin VER:fin
piazzati	VER:ppast	piazzare	f ADJ NOUN VER:ppast
gli	ART	il	f ART CLI
attrezzi	NOUN	attrezzo	f NOUN VER:fin
che	CHE	che	f CHE CON DET:wh
usavano	VER:fin	usare	f VER:fin
quelli	PRO:demo	quelli	f DET:demo PRO:demo
che	CHE	che	f CHE CON DET:wh
facevano	VER:fin	fare	f VER2:fin VER:fin
ginnastica	NOUN	ginnastica	f ADJ NOUN
artistica	ADJ	artistico	f ADJ
e	CON	e	f CON
allora	ADV	allora	f ADV
iniziammo	VER:fin	iniziare	f VER:fin
a	PRE	a	f PRE
giocare	VER:infi	giocare	f VER:infi
con	PRE	con	f PRE
le	ART	la	f ART CLI
travi	NOUN	trave	f NOUN VER:fin
mentre	CON	mentre	f CON
aspettavamo	VER:fin	aspettare	f VER:fin
il	ART	il	f ART
monitore	NOUN	<unknown>	s ADJ NOUN VER:fin
.	SENT	.	f SENT
Dopo	PRE	dopo	f ADJ ADV CON PRE
un	ART	un	f ART
po	ADJ	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
’	NOCAT	<unknown>	s NOCAT
giocando	VER:geru	giocare	f VER:geru
,	PUN	,	f PUN
feci	VER:fin	fare	f NOUN VER2:fin VER:fin
una	ART	una	f ART PRO:indef
capriola	NOUN	capriola	f NOUN
sotto	PRE	sotto	f ADJ ADV PRE
una	ART	una	f ART PRO:indef
trave	NOUN	trave	f NOUN
e	CON	e	f CON
alzandomi	VER:geru:cli	<unknown>	s VER:geru:cli VER:ppre
picchiai	VER:fin	picchiare	f VER:fin
la	ART	la	f ART CLI
testa	NOUN	testa	f NOUN VER:fin
contro	PRE	contro	f ADV PRE
la	ART	la	f ART CLI
trave	NOUN	trave	f NOUN
.	SENT	.	f SENT
Per	PRE	per	f PRE
fortuna	NOUN	fortuna	f NOUN
fuori	ADV	fuori	f ADV PRE
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
era	VER:fin	essere	f AUX:fin NOUN VER:fin
mia	DET:poss	mio	f DET:poss PRO:poss
mamma	NOUN	mamma	f NOUN
e	CON	e	f CON
la	ART	la	f ART CLI
mamma	NOUN	mamma	f NOUN
di	PRE	di	f PRE
un	ART	un	f ART
mio	DET:poss	mio	f DET:poss PRO:poss
compagno	NOUN	compagno	f NOUN
di	PRE	di	f PRE
ginnastica	NOUN	ginnastica	f ADJ NOUN
che	CHE	che	f CHE CON DET:wh
mi	CLI	mi	f CLI
portarono	VER:fin	portare	f VER:fin
subito	ADV	subito	f ADJ ADV VER:ppast
in	PRE	in	f PRE
ospedale	NOUN	ospedale	f NOUN
e	CON	e	f CON
la	ART	la	f ART CLI
dottoressa	NOUN	dottoressa	f NOUN
mi	CLI	mi	f CLI
fece	VER:fin	fare	f NOUN VER2:fin VER:fin
i	ART	il	f ART
punti	NOUN	punto	f ADJ NOUN VER:fin VER:ppast
sulla	ARTPRE	sulla	f ARTPRE
testa	NOUN	testa	f NOUN VER:fin
,	PUN	,	f PUN
quando	WH	quando	f WH
arrivò	VER:fin	arrivare	f VER:fin
mio	DET:poss	mio	f DET:poss PRO:poss
padre	NOUN	padre	f NOUN
.	SENT	.	f SENT
Poi	ADV	poi	f ADV
tornai	VER:fin	tornare	f VER:fin
ad	PRE	ad	f PRE
allenamento	NOUN	allenamento	f NOUN
ma	CON	ma	f CON
,	PUN	,	f PUN
purtroppo	ADV	purtroppo	f ADV
,	PUN	,	f PUN
solo	ADV	solo	f ADJ ADV VER:fin
per	PRE	per	f PRE
far	VER2:infi	fare	f VER2:infi VER:infi
vedere	VER:infi	vedere	f VER:infi
al	ARTPRE	al	f ARTPRE
mio	DET:poss	mio	f DET:poss PRO:poss
monitore	NOUN	<unknown>	s ADJ NOUN VER:fin
cosa	WH	cosa	f NOUN WH
mi	CLI	mi	f CLI
ero	AUX:fin	essere	f AUX:fin AUX:geru AUX:ppre VER:fin
fatto	VER:ppast	fare	f NOUN VER2:ppast VER:ppast
.	SENT	.	f SENT
Dato	VER:ppast	dare	f ADJ NOUN VER:fin VER:ppast
che	CHE	che	f CHE CON DET:wh
era	VER:fin	essere	f AUX:fin NOUN VER:fin
estate	NOUN	estate	f NOUN
avevo	VER:fin	avere	f AUX:fin VER:fin
anche	ADV	anche	f ADV
il	ART	il	f ART
corso	NOUN	corso	f ADJ NOUN VER:ppast
di	PRE	di	f PRE
nuoto	NOUN	nuoto	f NOUN VER:fin
estivo	ADJ	estivo	f ADJ VER:fin
e	CON	e	f CON
,	PUN	,	f PUN
per	PRE	per	f PRE
un	ART	un	f ART
po	ADJ	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
’	NOCAT	<unknown>	s NOCAT
,	PUN	,	f PUN
non	NEG	non	f NEG
potei	VER2:fin	potere	f VER2:fin VER:fin
fare	VER:infi	fare	f VER2:infi VER:infi
ne	CLI	ne	f CLI
quello	PRO:demo	quello	f DET:demo PRO:demo
ne	CLI	ne	f CLI
gli	ART	il	f ART CLI
allenamenti	NOUN	allenamento	f NOUN
di	PRE	di	f PRE
ginnastica	NOUN	ginnastica	f ADJ NOUN
e	CON	e	f CON
pure	ADV	pure	f ADJ ADV
alle	ARTPRE	alla	f ARTPRE
vacanze	NOUN	vacanza	f NOUN
al	ARTPRE	al	f ARTPRE
mare	NOUN	mare	f NOUN
dovetti	VER2:fin	dovere	f VER2:fin VER:fin
fare	VER:infi	fare	f VER2:infi VER:infi
molta	DET:indef	molto	f ADJ DET:indef
attenzione	NOUN	attenzione	f NOUN
.	SENT	.	f SENT
Da	PRE	da	f PRE
quel	DET:demo	quello	f DET:demo
giorno	NOUN	giorno	f NOUN
imparai	VER:fin	imparare	f VER:fin
che	CHE	che	f CHE CON DET:wh
:	PUN	:	f PUN
le	ART	la	f ART CLI
cose	NOUN	cosa	f NOUN
devono	VER2:fin	dovere	f VER2:fin VER:fin
essere	AUX:infi	essere	f AUX:infi NOUN VER:infi
usate	VER:ppast	usare	f ADJ VER:fin VER:ppast
per	PRE	per	f PRE
quello	PRO:demo	quello	f DET:demo PRO:demo
che	CHE	che	f CHE CON DET:wh
sono	AUX:fin	essere	f AUX:fin VER:fin
state	AUX:ppast	essere	f AUX:ppast VER:fin VER:ppast
fatte	VER:ppast	fare	f VER2:ppast VER:ppast
(	PUN	(	f PUN
esempio	NOUN	esempio	f NOUN
:	PUN	:	f PUN
le	ART	la	f ART CLI
travi	NOUN	trave	f NOUN VER:fin
sono	AUX:fin	essere	f AUX:fin VER:fin
fatte	VER:ppast	fare	f VER2:ppast VER:ppast
per	PRE	per	f PRE
fare	VER:infi	fare	f VER2:infi VER:infi
ginnastica	NOUN	ginnastica	f ADJ NOUN
e	CON	e	f CON
non	NEG	non	f NEG
per	PRE	per	f PRE
giocare	VER:infi	giocare	f VER:infi
)	PUN	)	f PUN
.	SENT	.	f SENT

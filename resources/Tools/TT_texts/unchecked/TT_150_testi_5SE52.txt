I	ART	il	f ART
nuovi	ADJ	nuovo	f ADJ NOUN
maestri	NOUN	maestro	f ADJ NOUN
del	ARTPRE	del	f ARTPRE
calcio	NOUN	calcio	f NOUN VER:fin
Siamo	VER:fin	essere	s NPR VER:fin
negli	ARTPRE	nel	f ARTPRE
anni	NOUN	anno	f NOUN
50	NUM	@card@	f NUM
‘	NOCAT	<unknown>	s NOCAT
,	PUN	,	f PUN
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
Inghilterra	NPR	Inghilterra	f NPR
a	PRE	a	f PRE
quel	DET:demo	quello	f DET:demo
tempo	NOUN	tempo	f NOUN
era	VER:fin	essere	f AUX:fin NOUN VER:fin
la	ART	la	f ART CLI
miglior	ADJ	miglior	f ADJ
squadra	NOUN	squadra	f NOUN VER:fin
al	ARTPRE	al	f ARTPRE
mondo	NOUN	mondo	f ADJ NOUN VER:fin
di	PRE	di	f PRE
calcio	NOUN	calcio	f NOUN VER:fin
,	PUN	,	f PUN
anche	ADV	anche	f ADV
se	CON	se	f CLI CON
era	AUX:fin	essere	f AUX:fin NOUN VER:fin
stato	AUX:ppast	essere	f AUX:ppast NOUN VER:ppast
messo	VER:ppast	mettere	f ADJ VER:ppast
in	PRE	in	f PRE
dubbio	NOUN	dubbio	f ADJ NOUN
il	ART	il	f ART
suo	DET:poss	suo	f DET:poss PRO:poss
valore	NOUN	valore	f NOUN
da	PRE	da	f PRE
un’	ADJ	<unknown>	s ADJ NOCAT VER:fin
uscita	NOUN	uscita	f NOUN VER:ppast
dai	ARTPRE	dal	f ARTPRE VER:fin
mondiali	ADJ	mondiale	f ADJ NOUN
e	CON	e	f CON
da	PRE	da	f PRE
un	ART	un	f ART
partita	NOUN	partita	f ADJ NOUN VER:ppast
persa	ADJ	perso	f ADJ
contro	PRE	contro	f ADV PRE
una	ART	una	f ART PRO:indef
delle	ARTPRE	della	f ARTPRE
squadre	NOUN	squadra	f NOUN
più	ADV	più	f ADV
deboli	ADJ	debole	f ADJ NOUN
,	PUN	,	f PUN
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
Ungheria	NPR	Ungheria	f NPR
era	VER:fin	essere	f AUX:fin NOUN VER:fin
una	PRO:indef	una	f ART PRO:indef
di	PRE	di	f PRE
queste	DET:demo	questo	f DET:demo PRO:demo
,	PUN	,	f PUN
ma	CON	ma	f CON
stava	VER:fin	stare	f VER:fin
crescendo	VER:geru	crescere	f VER:geru
sempre	ADV	sempre	f ADV
di	PRE	di	f PRE
più	ADV	più	f ADV
.	SENT	.	f SENT
Quel	DET:demo	quello	f DET:demo
giorno	NOUN	giorno	f NOUN
lo	ART	il	f ART CLI
stadio	NOUN	stadio	f NOUN
Wembley	NPR	Wembley	f NPR
era	AUX:fin	essere	f AUX:fin NOUN VER:fin
tutto	PRO:indef	tutto	f ADJ DET:indef PRO:indef
esaurito	VER:ppast	esaurire	f ADJ VER:ppast
(	PUN	(	f PUN
100.000	NUM	@card@	f NUM
posti	NOUN	posto	f ADJ NOUN VER:fin VER:ppast
)	PUN	)	f PUN
,	PUN	,	f PUN
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
arbitro	NOUN	arbitro	f NOUN
che	CHE	che	f CHE CON DET:wh
avrebbe	AUX:fin	avere	f AUX:fin VER:fin
diretto	VER:ppast	dirigere	f ADJ VER:ppast
la	ART	la	f ART CLI
gara	NOUN	gara	f NOUN
era	VER:fin	essere	f AUX:fin NOUN VER:fin
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
olandese	ADJ	olandese	f ADJ NOUN
Leo	NPR	Leo	f NPR
Horn	NPR	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
.	SENT	.	f SENT
Gli	ART	il	f ART CLI
inglesi	NOUN	inglese	f ADJ NOUN
si	CLI	si	f CLI
schieravano	VER:fin	schierare	f VER:fin
con	PRE	con	f PRE
il	ART	il	f ART
classico	ADJ	classico	f ADJ NOUN
2-3-5	NUM	@card@	f NUM
,	PUN	,	f PUN
mentre	CON	mentre	f CON
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
Ungheria	NPR	Ungheria	f NPR
con	PRE	con	f PRE
uno	ART	un	f ART NOUN PRO:indef
strano	ADJ	strano	f ADJ NOUN
4-2-4	NUM	@card@	f NUM
.	SENT	.	f SENT
I	ART	il	f ART
due	DET:num	due	f DET:num PRO:num
capitani	NOUN	capitano	f NOUN VER:fin
andarono	VER:fin	andare	f VER:fin
al	ARTPRE	al	f ARTPRE
centro	NOUN	centro	f NOUN VER:fin
per	PRE	per	f PRE
il	ART	il	f ART
lancio	NOUN	lancio	f NOUN VER:fin
della	ARTPRE	della	f ARTPRE
monetina	NOUN	monetina	f NOUN
,	PUN	,	f PUN
avrebbe	AUX:fin	avere	f AUX:fin VER:fin
battuto	VER:ppast	battere	f ADJ VER:ppast
il	ART	il	f ART
calcio	NOUN	calcio	f NOUN VER:fin
d’	ADJ	<unknown>	s ADJ NOCAT VER:fin
inizio	NOUN	inizio	f NOUN VER:fin
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
Ungheria	NPR	Ungheria	f NPR
,	PUN	,	f PUN
quando	WH	quando	f WH
si	CLI	si	f CLI
strinsero	VER:fin	stringere	f VER:fin
la	ART	la	f ART CLI
mano	NOUN	mano	f NOUN
il	ART	il	f ART
centrocampista	NOUN	centrocampista	f NOUN
inglese	ADJ	inglese	f ADJ NOUN
Wricht	NPR	<unknown>	s NOUN NPR
notò	VER:fin	notare	f VER:fin
che	CHE	che	f CHE CON DET:wh
i	ART	il	f ART
giocatori	NOUN	giocatore	f NOUN
dell’	ADJ	<unknown>	s ADJ NOCAT VER:fin
Ungheria	NPR	Ungheria	f NPR
indossavano	VER:fin	indossare	f VER:fin
quelle	DET:demo	quello	f DET:demo PRO:demo
scarpe	NOUN	scarpa	f NOUN
ultra	ADJ	ultra	f ADJ NOUN
leggere	ADJ	leggero	f ADJ VER:infi
,	PUN	,	f PUN
si	CLI	si	f CLI
voltò	VER:fin	voltare	f VER:fin
verso	PRE	verso	f ADJ NOUN PRE VER:fin
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
allenatore	NOUN	allenatore	f NOUN
e	CON	e	f CON
gli	CLI	gli	f ART CLI
disse	VER:fin	dire	f VER:fin
“	NOCAT	<unknown>	s NOCAT
Con	VER:fin	<unknown>	s NPR VER:fin
questi	PRO:demo	questi	f DET:demo PRO:demo
è	VER:fin	essere	f AUX:fin VER:fin
facile	ADJ	facile	f ADJ
,	PUN	,	f PUN
indossano	VER:fin	indossare	f VER:fin
perfino	ADV	perfino	f ADV
le	ART	la	f ART CLI
scarpe	NOUN	scarpa	f NOUN
sbagliate	ADJ	sbagliato	f ADJ VER:fin VER:ppast
”	NOCAT	<unknown>	s NOCAT
.	SENT	.	f SENT
L’	NPR	<unknown>	s ADJ NOCAT NPR VER:fin
Ungheria	NPR	Ungheria	f NPR
partì	VER:fin	partire	f VER:fin
forte	ADJ	forte	f ADJ NOUN
e	CON	e	f CON
la	ART	la	f ART CLI
strana	ADJ	strano	f ADJ
posizione	NOUN	posizione	f NOUN
del	ARTPRE	del	f ARTPRE
centravanti	NOUN	centravanti	f NOUN
ungherese	ADJ	ungherese	f ADJ NOUN
,	PUN	,	f PUN
che	CHE	che	f CHE CON DET:wh
giocava	VER:fin	giocare	f VER:fin
più	ADV	più	f ADV
arretrato	ADJ	arretrato	f ADJ NOUN VER:ppast
,	PUN	,	f PUN
mise	VER:fin	mettere	f VER:fin
in	PRE	in	f PRE
difficolta	VER:fin	<unknown>	s ADJ NOUN VER:fin VER:ppast
gli	ART	il	f ART CLI
inglesi	NOUN	inglese	f ADJ NOUN
e	CON	e	f CON
dopo	PRE	dopo	f ADJ ADV CON PRE
una	ART	una	f ART PRO:indef
serie	NOUN	serie	f ADJ NOUN
di	PRE	di	f PRE
triangoli	NOUN	triangolo	f NOUN VER:fin
portò	VER:fin	portare	f VER:fin
in	PRE	in	f PRE
vantaggio	NOUN	vantaggio	f NOUN
la	ART	la	f ART CLI
sua	DET:poss	suo	f DET:poss PRO:poss
squadra	NOUN	squadra	f NOUN VER:fin
(	PUN	(	f PUN
dopo	PRE	dopo	f ADJ ADV CON PRE
solo	ADV	solo	f ADJ ADV VER:fin
2	NUM	@card@	f NUM
minuti	NOUN	minuto	f ADJ NOUN
)	PUN	)	f PUN
.	SENT	.	f SENT
Sette	DET:num	sette	f DET:num NOUN PRO:num
minuti	NOUN	minuto	f ADJ NOUN
più	ADV	più	f ADV
tardi	ADV	tardi	f ADJ ADV VER:fin
gli	ART	il	f ART CLI
inglesi	NOUN	inglese	f ADJ NOUN
agguantarono	VER:fin	agguantare	f VER:fin
il	ART	il	f ART
pareggio	NOUN	pareggio	f NOUN VER:fin
.	SENT	.	f SENT
lo	ART	il	f ART CLI
stadio	NOUN	stadio	f NOUN
esplose	VER:fin	esplodere	f ADJ VER:fin VER:ppast
in	PRE	in	f PRE
un	ART	un	f ART
boato	NOUN	boato	f NOUN
terrificante	VER:ppre	terrificare	f VER:ppre
,	PUN	,	f PUN
che	CHE	che	f CHE CON DET:wh
duró	VER:fin	<unknown>	s ADJ NOCAT VER:fin
solo	ADV	solo	f ADJ ADV VER:fin
7	NUM	@card@	f NUM
minuti	NOUN	minuto	f ADJ NOUN
,	PUN	,	f PUN
infatti	ADV	infatti	f ADV
dopo	PRE	dopo	f ADJ ADV CON PRE
20	NUM	@card@	f NUM
min	VER:fin	<unknown>	s VER:fin
.	SENT	.	f SENT
di	PRE	di	f PRE
gioco	NOUN	gioco	f NOUN VER:fin
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
ungheria	NOUN	<unknown>	s ADJ NOUN
si	CLI	si	f CLI
portó	VER:fin	<unknown>	s ADJ NOCAT VER:fin
sul	ARTPRE	sul	f ARTPRE
1:2	NUM	@card@	f NUM
.	SENT	.	f SENT
Passano	VER:fin	passare	f VER:fin
7	NUM	@card@	f NUM
minuti	NOUN	minuto	f ADJ NOUN
e	CON	e	f CON
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
Ungheria	NPR	Ungheria	f NPR
è	VER:fin	essere	f AUX:fin VER:fin
sul	ARTPRE	sul	f ARTPRE
1:4	NUM	@card@	f NUM
,	PUN	,	f PUN
dapprima	ADV	dapprima	f ADV
con	PRE	con	f PRE
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
attaccante	NOUN	attaccante	f ADJ NOUN VER:ppre
arretrato	ADJ	arretrato	f ADJ NOUN VER:ppast
che	CHE	che	f CHE CON DET:wh
segna	VER:fin	segnare	f VER:fin
uno	ART	un	f ART NOUN PRO:indef
doppietta	NOUN	doppietta	f NOUN
,	PUN	,	f PUN
e	CON	e	f CON
poi	ADV	poi	f ADV
,	PUN	,	f PUN
dopo	PRE	dopo	f ADJ ADV CON PRE
una	ART	una	f ART PRO:indef
serie	NOUN	serie	f ADJ NOUN
di	PRE	di	f PRE
rimpalli	NOUN	rimpallo	f NOUN VER:fin
,	PUN	,	f PUN
arriva	VER:fin	arrivare	f VER:fin
il	ART	il	f ART
quarto	ADJ	quarto	f ADJ NOUN
gol	NOUN	gol	f NOUN
.	SENT	.	f SENT
L’	NPR	<unknown>	s ADJ NOCAT NPR VER:fin
Inghilterra	NPR	Inghilterra	f NPR
non	NEG	non	f NEG
si	CLI	si	f CLI
da	PRE	da	f PRE
per	PRE	per	f PRE
vinta	ADJ	vinto	f ADJ VER:ppast
e	CON	e	f CON
dimezza	VER:fin	dimezzare	f VER:fin
le	ART	la	f ART CLI
distanze	NOUN	distanza	f NOUN
al	ARTPRE	al	f ARTPRE
trentottesimo	ADJ	trentottesimo	f ADJ
.	SENT	.	f SENT
Dopo	PRE	dopo	f ADJ ADV CON PRE
soli	NOUN	sole	f ADJ NOUN VER:fin
6	NUM	@card@	f NUM
minuti	NOUN	minuto	f ADJ NOUN
arrivano	VER:fin	arrivare	f VER:fin
due	DET:num	due	f DET:num PRO:num
gol	NOUN	gol	f NOUN
tutti	DET:indef	tutto	f ADJ DET:indef PRO:indef
dell’	ADJ	<unknown>	s ADJ NOCAT VER:fin
Ungheria	NPR	Ungheria	f NPR
,	PUN	,	f PUN
al	ARTPRE	al	f ARTPRE
cinquantesimo	ADJ	cinquantesimo	f ADJ
il	ART	il	f ART
centravanti	NOUN	centravanti	f NOUN
segna	VER:fin	segnare	f VER:fin
la	ART	la	f ART CLI
sua	DET:poss	suo	f DET:poss PRO:poss
tripletta	NOUN	tripletta	f NOUN
,	PUN	,	f PUN
e	CON	e	f CON
poi	ADV	poi	f ADV
serve	VER:fin	servire	f VER:fin
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
assist	NOUN	assist	f NOUN
al	ARTPRE	al	f ARTPRE
suo	DET:poss	suo	f DET:poss PRO:poss
trequartista	NOUN	trequartista	f NOUN
che	CHE	che	f CHE CON DET:wh
gonfia	VER:fin	gonfiare	f ADJ NOUN VER:fin
la	ART	la	f ART CLI
rete	NOUN	rete	f NOUN
,	PUN	,	f PUN
il	ART	il	f ART
Wembley	NPR	Wembley	f NPR
stadium	NOUN	<unknown>	s ADJ NOUN NPR VER:fin
rimasse	VER:fin	rimare	f VER:fin
in	PRE	in	f PRE
silenzio	NOUN	silenzio	f NOUN VER:fin
,	PUN	,	f PUN
2:6	NUM	@card@	f NUM
.	SENT	.	f SENT
L’	NPR	<unknown>	s ADJ NOCAT NPR VER:fin
Inghilterra	NPR	Inghilterra	f NPR
trovó	ADJ	<unknown>	s ADJ NOCAT VER:fin
il	ART	il	f ART
definitivo	ADJ	definitivo	f ADJ
3:6	NUM	@card@	f NUM
su	PRE	su	f PRE
punizione	NOUN	punizione	f NOUN
,	PUN	,	f PUN
a	PRE	a	f PRE
fine	NOUN	fine	f ADJ NOUN
partita	VER:ppast	partire	f ADJ NOUN VER:ppast
il	ART	il	f ART
Wembley	NPR	Wembley	f NPR
stadium	NOUN	<unknown>	s ADJ NOUN NPR VER:fin
applaude	VER:fin	applaudire	f VER:fin
i	ART	il	f ART
nuovi	ADJ	nuovo	f ADJ NOUN
maestri	NOUN	maestro	f ADJ NOUN
del	ARTPRE	del	f ARTPRE
calcio	NOUN	calcio	f NOUN VER:fin
.	SENT	.	f SENT
Questo	DET:demo	questo	f DET:demo PRO:demo
fatto	NOUN	fatto	f NOUN VER2:ppast VER:ppast
ci	CLI	ci	f CLI
insegna	VER:fin	insegnare	f NOUN VER:fin
di	PRE	di	f PRE
non	NEG	non	f NEG
dire	VER:infi	dire	f ADJ VER:infi
di	PRE	di	f PRE
essere	VER:infi	essere	f AUX:infi NOUN VER:infi
più	ADV	più	f ADV
forte	ADJ	forte	f ADJ NOUN
di	PRE	di	f PRE
qualcuno	PRO:indef	qualcuno	f PRO:indef
se	CON	se	f CLI CON
prima	ADV	prima	f ADJ ADV
non	NEG	non	f NEG
si	CLI	si	f CLI
è	AUX:fin	essere	f AUX:fin VER:fin
gareggiato	VER:ppast	gareggiare	f VER:ppast
.	SENT	.	f SENT

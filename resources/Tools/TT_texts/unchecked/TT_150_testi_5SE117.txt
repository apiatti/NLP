Un	ART	un	f ART
mese	NOUN	mese	f NOUN
fa	VER:fin	fare	f ADV VER2:fin VER:fin
io	PRO:pers	io	f PRO:pers
e	CON	e	f CON
una	ART	una	f ART PRO:indef
mia	DET:poss	mio	f DET:poss PRO:poss
amica	NOUN	amica	f ADJ NOUN VER:fin
che	CHE	che	f CHE CON DET:wh
si	CLI	si	f CLI
chiama	VER:fin	chiamare	f VER:fin
Diana	NPR	Diana	f NPR
avevamo	AUX:fin	avere	f AUX:fin VER:fin
litigato	VER:ppast	litigare	f VER:ppast
,	PUN	,	f PUN
lei	PRO:pers	lei	f PRO:pers
aveva	VER:fin	avere	f AUX:fin VER:fin
un	ART	un	f ART
comportamento	NOUN	comportamento	f NOUN
strano	ADJ	strano	f ADJ NOUN
quando	WH	quando	f WH
voleva	VER2:fin	volere	f VER2:fin VER:fin
stare	VER:infi	stare	f ADV VER:infi
con	PRE	con	f PRE
me	PRO:pers	me	f CLI PRO:pers
mi	CLI	mi	f CLI
chiamava	VER:fin	chiamare	f VER:fin
e	CON	e	f CON
mi	CLI	mi	f CLI
chiedeva	VER:fin	chiedere	f VER:fin
di	PRE	di	f PRE
uscire	VER:infi	uscire	f VER:infi
con	PRE	con	f PRE
lei	PRO:pers	lei	f PRO:pers
,	PUN	,	f PUN
e	CON	e	f CON
in	PRE	in	f PRE
vece	NOUN	vece	f NOUN
quando	WH	quando	f WH
preferiva	VER:fin	preferire	f VER:fin
giocare	VER:infi	giocare	f VER:infi
o	CON	o	f CON
studiare	VER:infi	studiare	f VER:infi
con	PRE	con	f PRE
altre	DET:indef	altro	f ADJ DET:indef PRO:indef
persone	NOUN	persona	f NOUN
,	PUN	,	f PUN
si	CLI	si	f CLI
inventava	VER:fin	inventare	f VER:fin
scuse	NOUN	scusa	f NOUN
per	PRE	per	f PRE
non	NEG	non	f NEG
stare	VER:infi	stare	f ADV VER:infi
con	PRE	con	f PRE
me	PRO:pers	me	f CLI PRO:pers
!	SENT	!	f SENT
Io	PRO:pers	io	f PRO:pers
ci	CLI	ci	f CLI
pensavo	VER:fin	pensare	f VER:fin
e	CON	e	f CON
mi	CLI	mi	f CLI
chiedevo	VER:fin	chiedere	f VER:fin
nella	ARTPRE	nella	f ARTPRE
mia	DET:poss	mio	f DET:poss PRO:poss
mente	NOUN	mente	f NOUN VER:fin
se	CON	se	f CLI CON
avevo	AUX:fin	avere	f AUX:fin VER:fin
fatto	VER:ppast	fare	f NOUN VER2:ppast VER:ppast
qualcosa	PRO:indef	qualcosa	f PRO:indef
di	PRE	di	f PRE
male	NOUN	male	f ADJ ADV NOUN
ma	CON	ma	f CON
a	PRE	a	f PRE
quella	DET:demo	quello	f DET:demo PRO:demo
domanda	NOUN	domanda	f NOUN VER:fin
riuscivo	VER:fin	riuscire	f VER:fin
a	PRE	a	f PRE
rispondermi	VER:infi:cli	<unknown>	s ADJ NOUN VER:fin VER:geru VER:infi VER:infi:cli VER:ppast VER:ppre
da	PRE	da	f PRE
sola	ADJ	solo	f ADJ VER:fin
no	NOUN	no	f ADV NOUN
!	SENT	!	f SENT
Non	NEG	non	f NEG
avevo	AUX:fin	avere	f AUX:fin VER:fin
fatto	VER:ppast	fare	f NOUN VER2:ppast VER:ppast
niente	ADV	niente	f ADJ ADV PRO:indef
di	PRE	di	f PRE
male	NOUN	male	f ADJ ADV NOUN
,	PUN	,	f PUN
allora	ADV	allora	f ADV
forse	ADV	forse	f ADV
non	NEG	non	f NEG
voleva	VER2:fin	volere	f VER2:fin VER:fin
più	ADV	più	f ADV
essere	AUX:infi	essere	f AUX:infi NOUN VER:infi
mia	DET:poss	mio	f DET:poss PRO:poss
amica	NOUN	amica	f ADJ NOUN VER:fin
,	PUN	,	f PUN
a	PRE	a	f PRE
quella	DET:demo	quello	f DET:demo PRO:demo
domanda	NOUN	domanda	f NOUN VER:fin
invece	ADV	invece	f ADV
non	NEG	non	f NEG
sapevo	VER:fin	sapere	f VER:fin
rispondere	VER:infi	rispondere	f VER:infi
.	SENT	.	f SENT
Mi	CLI	mi	f CLI
sentivo	VER:fin	sentire	f VER:fin
usata	VER:ppast	usare	f ADJ VER:ppast
come	WH	come	f WH
le	ART	la	f ART CLI
macchina	NOUN	macchina	f NOUN VER:fin
fotografiche	ADJ	fotografico	f ADJ
usa	VER:fin	usare	f ADJ VER:fin
e	CON	e	f CON
getta	VER:fin	gettare	f VER:fin
,	PUN	,	f PUN
dentro	ADV	dentro	f ADV PRE
alle	ARTPRE	alla	f ARTPRE
macchine	NOUN	macchina	f NOUN
fotografiche	ADJ	fotografico	f ADJ
usa	VER:fin	usare	f ADJ VER:fin
e	CON	e	f CON
getta	VER:fin	gettare	f VER:fin
ci	CLI	ci	f CLI
sono	VER:fin	essere	f AUX:fin VER:fin
dei	ARTPRE	del	f ARTPRE NOUN
ricordi	NOUN	ricordo	f NOUN VER:fin
,	PUN	,	f PUN
e	CON	e	f CON
anche	ADV	anche	f ADV
dentro	ADV	dentro	f ADV PRE
di	PRE	di	f PRE
me	PRO:pers	me	f CLI PRO:pers
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
erano	VER:fin	essere	f AUX:fin VER:fin
tanti	DET:indef	tanto	f ADJ DET:indef PRO:indef
ricordi	NOUN	ricordo	f NOUN VER:fin
,	PUN	,	f PUN
che	CHE	che	f CHE CON DET:wh
erano	VER:fin	essere	f AUX:fin VER:fin
anche	ADV	anche	f ADV
belli	ADJ	bello	f ADJ
ma	CON	ma	f CON
rovinati	VER:ppast	rovinare	f VER:ppast
.	SENT	.	f SENT
Diana	NPR	Diana	f NPR
a	PRE	a	f PRE
scuola	NOUN	scuola	f NOUN
aveva	AUX:fin	avere	f AUX:fin VER:fin
incominciato	VER:ppast	incominciare	f VER:ppast
a	PRE	a	f PRE
frequentare	VER:infi	frequentare	f VER:infi
altre	DET:indef	altro	f ADJ DET:indef PRO:indef
amiche	ADJ	amico	f ADJ NOUN
(	PUN	(	f PUN
Jessica	NPR	<unknown>	s ADJ NOUN NPR VER:fin
,	PUN	,	f PUN
Valeria	NPR	Valeria	f NPR
e	CON	e	f CON
xxxleini	ADJ	<unknown>	s ADJ NOUN VER:fin
)	PUN	)	f PUN
mi	CLI	mi	f CLI
lasciava	VER:fin	lasciare	f VER:fin
in	PRE	in	f PRE
disparte	ADV	disparte	f ADV
e	CON	e	f CON
mi	CLI	mi	f CLI
evitava	VER:fin	evitare	f VER:fin
.	SENT	.	f SENT
Io	PRO:pers	io	f PRO:pers
non	NEG	non	f NEG
volevo	VER2:fin	volere	f VER2:fin VER:fin
stare	VER:infi	stare	f ADV VER:infi
da	PRE	da	f PRE
sola	ADJ	solo	f ADJ VER:fin
e	CON	e	f CON
quindi	ADV	quindi	f ADV
,	PUN	,	f PUN
mi	CLI	mi	f CLI
trovai	VER:fin	trovare	f VER:fin
altre	DET:indef	altro	f ADJ DET:indef PRO:indef
amiche	ADJ	amico	f ADJ NOUN
.	SENT	.	f SENT
Io	PRO:pers	io	f PRO:pers
con	PRE	con	f PRE
le	ART	la	f ART CLI
mie	DET:poss	mio	f DET:poss PRO:poss
nuove	ADJ	nuovo	f ADJ
amiche	NOUN	amica	f ADJ NOUN
mi	CLI	mi	f CLI
trovavo	VER:fin	trovare	f VER:fin
bene	ADV	bene	f ADV NOUN
loro	DET:poss	loro	f DET:poss PRO:pers PRO:poss
rispetto	NOUN	rispetto	f NOUN PRE VER:fin
a	PRE	a	f PRE
Diana	NPR	Diana	f NPR
non	NEG	non	f NEG
mi	CLI	mi	f CLI
dicevano	VER:fin	dire	f VER:fin
bugie	ADJ	bugio	f ADJ NOUN
.	SENT	.	f SENT
Diana	NPR	Diana	f NPR
una	ART	una	f ART PRO:indef
sera	NOUN	sera	f NOUN
chiamò	VER:fin	chiamare	f VER:fin
mia	DET:poss	mio	f DET:poss PRO:poss
mamma	NOUN	mamma	f NOUN
e	CON	e	f CON
le	ART	la	f ART CLI
chiese	NOUN	chiesa	f NOUN VER:fin
perché	WH	perché	f WH
la	CLI	la	f ART CLI
evitavo	VER:fin	evitare	f VER:fin
,	PUN	,	f PUN
mia	DET:poss	mio	f DET:poss PRO:poss
mamma	NOUN	mamma	f NOUN
disse	VER:fin	dire	f VER:fin
che	CHE	che	f CHE CON DET:wh
non	NEG	non	f NEG
sapeva	VER:fin	sapere	f VER:fin
nulla	PRO:indef	nulla	f ADJ PRO:indef
.	SENT	.	f SENT
La	ART	la	f ART CLI
sera	NOUN	sera	f NOUN
dopo	PRE	dopo	f ADJ ADV CON PRE
Diana	NPR	Diana	f NPR
mi	CLI	mi	f CLI
chiamò	VER:fin	chiamare	f VER:fin
e	CON	e	f CON
mi	CLI	mi	f CLI
disse	VER:fin	dire	f VER:fin
va	VER:fin	andare	f VER:fin
bene	ADV	bene	f ADV NOUN
se	CLI	se	f CLI CON
siamo	VER:fin	essere	f AUX:fin VER:fin
amiche	ADJ	amico	f ADJ NOUN
solo	ADV	solo	f ADJ ADV VER:fin
dopo	PRE	dopo	f ADJ ADV CON PRE
la	ART	la	f ART CLI
scuola	NOUN	scuola	f NOUN
?	SENT	?	f SENT
Io	PRO:pers	io	f PRO:pers
dissi	VER:fin	dire	f VER:fin
se	CLI	se	f CLI CON
hai	AUX:fin	avere	f AUX:fin VER:fin
vergogna	NOUN	vergogna	f NOUN VER:fin
di	PRE	di	f PRE
me	PRO:pers	me	f CLI PRO:pers
,	PUN	,	f PUN
non	NEG	non	f NEG
fa	VER2:fin	fare	f ADV VER2:fin VER:fin
gnente	VER:ppre	<unknown>	s ADJ NOUN VER:fin VER:ppre
continuiamo	VER:fin	continuare	f VER:fin
cosi	ADV	così	f ADV
.	SENT	.	f SENT
Io	PRO:pers	io	f PRO:pers
e	CON	e	f CON
Diana	NPR	Diana	f NPR
dopo	CON	dopo	f ADJ ADV CON PRE
avevamo	AUX:fin	avere	f AUX:fin VER:fin
fatto	VER:ppast	fare	f NOUN VER2:ppast VER:ppast
pace	NOUN	pace	f NOUN
ma	CON	ma	f CON
di	PRE	di	f PRE
lei	PRO:pers	lei	f PRO:pers
non	NEG	non	f NEG
mi	CLI	mi	f CLI
fidai	VER:fin	fidare	f VER:fin
come	WH	come	f WH
prima	ADJ	primo	f ADJ ADV
.	SENT	.	f SENT
La	ART	la	f ART CLI
morale	NOUN	morale	f ADJ NOUN
é	VER:fin	essere	f AUX:fin VER:fin
che	CHE	che	f CHE CON DET:wh
chi	WH	chi	f WH
usa	VER:fin	usare	f ADJ VER:fin
le	ART	la	f ART CLI
persone	NOUN	persona	f NOUN
quasi	ADV	quasi	f ADV
sempre	ADV	sempre	f ADV
resterà	VER:fin	restare	f VER:fin
da	PRE	da	f PRE
solo	ADV	solo	f ADJ ADV VER:fin
.	SENT	.	f SENT

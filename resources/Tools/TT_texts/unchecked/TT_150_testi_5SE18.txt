La	ART	la	f ART CLI
partita	NOUN	partita	f ADJ NOUN VER:ppast
di	PRE	di	f PRE
ping-pong	NOUN	ping-pong	f NOUN
Era	VER:fin	essere	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
estate	NOUN	estate	f NOUN
,	PUN	,	f PUN
fuori	ADV	fuori	f ADV PRE
faceva	VER:fin	fare	f VER2:fin VER:fin
molto	ADV	molto	f ADJ ADV DET:indef
caldo	ADJ	caldo	f ADJ NOUN
.	SENT	.	f SENT
Prima	ADV	prima	f ADJ ADV
di	PRE	di	f PRE
uscire	VER:infi	uscire	f VER:infi
peró	ADJ	<unknown>	s ADJ NOCAT VER:fin
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
giocato	VER:ppast	giocare	f VER:ppast
tutta	DET:indef	tutto	f ADJ DET:indef
la	ART	la	f ART CLI
famiglia	NOUN	famiglia	f NOUN
a	PRE	a	f PRE
Uno	NPR	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
.	SENT	.	f SENT
Dopo	CON	dopo	f ADJ ADV CON PRE
ci	CLI	ci	f CLI
siamo	AUX:fin	essere	f AUX:fin VER:fin
messi	VER:ppast	mettere	f ADJ NOUN VER:ppast
tutti	DET:indef	tutto	f ADJ DET:indef PRO:indef
il	ART	il	f ART
costume	NOUN	costume	f NOUN
e	CON	e	f CON
siamo	AUX:fin	essere	f AUX:fin VER:fin
andati	VER:ppast	andare	f ADJ VER:ppast
a	PRE	a	f PRE
fare	VER:infi	fare	f VER2:infi VER:infi
un	ART	un	f ART
bagno	NOUN	bagno	f NOUN VER:fin
in	PRE	in	f PRE
piscina	NOUN	piscina	f NOUN
,	PUN	,	f PUN
dietro	ADV	dietro	f ADV PRE
la	ART	la	f ART CLI
nostra	DET:poss	nostro	f DET:poss PRO:poss
casa	NOUN	casa	f NOUN
.	SENT	.	f SENT
Erano	VER:fin	essere	f AUX:fin VER:fin
già	ADV	già	f ADV
le	ART	la	f ART CLI
16:30	NUM	@card@	f NUM
e	CON	e	f CON
allora	ADV	allora	f ADV
siamo	AUX:fin	essere	f AUX:fin VER:fin
usciti	VER:ppast	uscire	f VER:ppast
dalla	ARTPRE	dalla	f ARTPRE
piscina	NOUN	piscina	f NOUN
e	CON	e	f CON
ci	CLI	ci	f CLI
siamo	VER:fin	essere	f AUX:fin VER:fin
un	ART	un	f ART
po	NOUN	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
’	NOCAT	<unknown>	s NOCAT
asciugati	VER:ppast	asciugare	f VER:ppast
.	SENT	.	f SENT
Abbiamo	AUX:fin	avere	f AUX:fin VER:fin
poi	ADV	poi	f ADV
mangiato	VER:ppast	mangiare	f VER:ppast
tutti	DET:indef	tutto	f ADJ DET:indef PRO:indef
un	ART	un	f ART
gelato	NOUN	gelato	f ADJ NOUN VER:ppast
alla	ARTPRE	alla	f ARTPRE
vaniglia	NOUN	<unknown>	s ADJ NOUN VER:fin
,	PUN	,	f PUN
per	PRE	per	f PRE
poi	ADV	poi	f ADV
andare	VER:infi	andare	f VER:infi
davanti	ADV	davanti	f ADJ ADV PRE
a	PRE	a	f PRE
casa	NOUN	casa	f NOUN
per	PRE	per	f PRE
giocare	VER:infi	giocare	f VER:infi
.	SENT	.	f SENT
Quando	WH	quando	f WH
siamo	AUX:fin	essere	f AUX:fin VER:fin
andati	VER:ppast	andare	f ADJ VER:ppast
davanti	PRE	davanti	f ADJ ADV PRE
il	ART	il	f ART
mio	DET:poss	mio	f DET:poss PRO:poss
fratellino	NOUN	fratellino	f NOUN
minore	ADJ	minore	f ADJ
mi	CLI	mi	f CLI
aveva	AUX:fin	avere	f AUX:fin VER:fin
chiesto	VER:ppast	chiedere	f ADJ VER:ppast
se	CON	se	f CLI CON
io	PRO:pers	io	f PRO:pers
volevo	VER2:fin	volere	f VER2:fin VER:fin
giocare	VER:infi	giocare	f VER:infi
con	PRE	con	f PRE
lui	PRO:pers	lui	f PRO:pers
a	PRE	a	f PRE
ping-pong	NOUN	ping-pong	f NOUN
,	PUN	,	f PUN
io	PRO:pers	io	f PRO:pers
gli	CLI	gli	f ART CLI
ho	AUX:fin	avere	f AUX:fin VER:fin
risposto	VER:ppast	rispondere	f VER:ppast
di	PRE	di	f PRE
no	ADV	no	f ADV NOUN
perché	WH	perché	f WH
gli	CLI	gli	f ART CLI
ho	AUX:fin	avere	f AUX:fin VER:fin
detto	VER:ppast	dire	f VER:fin VER:ppast
che	CHE	che	f CHE CON DET:wh
tanto	ADV	tanto	f ADJ ADV DET:indef
vincevo	VER:fin	vincere	f VER:fin
io	PRO:pers	io	f PRO:pers
ed	CON	ed	f CON
era	VER:fin	essere	f AUX:fin NOUN VER:fin
inutile	ADJ	inutile	f ADJ
giocare	VER:infi	giocare	f VER:infi
.	SENT	.	f SENT
Allora	ADV	allora	f ADV
ci	CLI	ci	f CLI
siamo	AUX:fin	essere	f AUX:fin VER:fin
messi	VER:ppast	mettere	f ADJ NOUN VER:ppast
a	PRE	a	f PRE
giocare	VER:infi	giocare	f VER:infi
a	PRE	a	f PRE
volano	NOUN	volano	f NOUN VER:fin
,	PUN	,	f PUN
una	ART	una	f ART PRO:indef
partita	NOUN	partita	f ADJ NOUN VER:ppast
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
ho	AUX:fin	avere	f AUX:fin VER:fin
vinta	VER:ppast	vincere	f ADJ VER:ppast
io	PRO:pers	io	f PRO:pers
,	PUN	,	f PUN
mentre	CON	mentre	f CON
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
altra	DET:indef	altro	f ADJ DET:indef PRO:indef
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
ha	AUX:fin	avere	f AUX:fin VER:fin
vinta	VER:ppast	vincere	f ADJ VER:ppast
il	ART	il	f ART
mio	DET:poss	mio	f DET:poss PRO:poss
fratellino	NOUN	fratellino	f NOUN
minore	ADJ	minore	f ADJ
.	SENT	.	f SENT
Dopo	PRE	dopo	f ADJ ADV CON PRE
queste	DET:demo	questo	f DET:demo PRO:demo
due	DET:num	due	f DET:num PRO:num
belle	ADJ	bello	f ADJ NOUN
e	CON	e	f CON
divertenti	ADJ	divertente	f ADJ VER:ppre
partite	NOUN	partita	f ADJ NOUN VER:fin VER:ppast
a	PRE	a	f PRE
volano	NOUN	volano	f NOUN VER:fin
,	PUN	,	f PUN
il	ART	il	f ART
mio	DET:poss	mio	f DET:poss PRO:poss
fratellino	NOUN	fratellino	f NOUN
mi	CLI	mi	f CLI
ha	AUX:fin	avere	f AUX:fin VER:fin
ancora	ADV	ancora	f ADV VER:fin
chiesto	VER:ppast	chiedere	f ADJ VER:ppast
se	CON	se	f CLI CON
volevo	VER2:fin	volere	f VER2:fin VER:fin
giocare	VER:infi	giocare	f VER:infi
con	PRE	con	f PRE
lui	PRO:pers	lui	f PRO:pers
a	PRE	a	f PRE
ping-pong	NOUN	ping-pong	f NOUN
e	CON	e	f CON
io	PRO:pers	io	f PRO:pers
a	PRE	a	f PRE
quel	DET:demo	quello	f DET:demo
punto	NOUN	punto	f ADJ ADV NOUN VER:fin VER:ppast
ho	AUX:fin	avere	f AUX:fin VER:fin
ceduto	VER:ppast	cedere	f VER:ppast
e	CON	e	f CON
gli	CLI	gli	f ART CLI
ho	AUX:fin	avere	f AUX:fin VER:fin
detto	VER:ppast	dire	f VER:fin VER:ppast
di	PRE	di	f PRE
sì	ADV	sì	f ADV
.	SENT	.	f SENT
Abbiamo	AUX:fin	avere	f AUX:fin VER:fin
così	ADV	così	f ADV
dovuto	VER:ppast	dovere	f ADJ VER2:ppre VER:ppast
prendere	VER:infi	prendere	f VER:infi
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
occorrente	ADJ	occorrente	f ADJ VER:ppre
per	PRE	per	f PRE
giocare	VER:infi	giocare	f VER:infi
e	CON	e	f CON
poi	ADV	poi	f ADV
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
preparato	VER:ppast	preparare	f ADJ VER:ppast
il	ART	il	f ART
tavolo	NOUN	tavolo	f NOUN
.	SENT	.	f SENT
Abbiamo	AUX:fin	avere	f AUX:fin VER:fin
incominciato	VER:ppast	incominciare	f VER:ppast
a	PRE	a	f PRE
giocare	VER:infi	giocare	f VER:infi
e	CON	e	f CON
alla	ARTPRE	alla	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
ha	AUX:fin	avere	f AUX:fin VER:fin
vinto	VER:ppast	vincere	f ADJ VER:ppast
il	ART	il	f ART
mio	DET:poss	mio	f DET:poss PRO:poss
fratellino	NOUN	fratellino	f NOUN
minore	ADJ	minore	f ADJ
battendomi	VER:geru:cli	<unknown>	s ADJ NOUN VER:fin VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
21-15	NUM	@card@	f NUM
.	SENT	.	f SENT
allora	ADV	allora	f ADV
mi	CLI	mi	f CLI
toccava	VER:fin	toccare	f VER:fin
proprio	ADV	proprio	f ADJ ADV DET:poss PRO:poss
fargli	VER:infi:cli	fare	f VER2:infi:cli VER:infi:cli
tanti	DET:indef	tanto	f ADJ DET:indef PRO:indef
complimenti	NOUN	complimento	f NOUN VER:fin
.	SENT	.	f SENT
Dopo	CON	dopo	f ADJ ADV CON PRE
mi	CLI	mi	f CLI
aveva	AUX:fin	avere	f AUX:fin VER:fin
fatto	VER:ppast	fare	f NOUN VER2:ppast VER:ppast
anche	ADV	anche	f ADV
lui	PRO:pers	lui	f PRO:pers
i	ART	il	f ART
complimenti	NOUN	complimento	f NOUN VER:fin
ed	CON	ed	f CON
ero	VER:fin	essere	f AUX:fin AUX:geru AUX:ppre VER:fin
contenta	ADJ	contento	f ADJ VER:fin
di	PRE	di	f PRE
come	WH	come	f WH
avevo	AUX:fin	avere	f AUX:fin VER:fin
passato	VER:ppast	passare	f ADJ NOUN VER:ppast
la	ART	la	f ART CLI
giornata	NOUN	giornata	f NOUN
.	SENT	.	f SENT
Insegnamento	NOUN	insegnamento	f NOUN
:	PUN	:	f PUN
Da	PRE	da	s ADJ NOCAT NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
quel	DET:demo	quello	f DET:demo
momento	NOUN	momento	f NOUN
ho	AUX:fin	avere	f AUX:fin VER:fin
imparato	VER:ppast	imparare	f VER:ppast
che	CHE	che	f CHE CON DET:wh
non	NEG	non	f NEG
bisogna	VER:fin	bisognare	f VER2:fin VER:fin
mai	ADV	mai	f ADV
sottovalutare	VER:infi	sottovalutare	f VER:infi
le	ART	la	f ART CLI
persone	NOUN	persona	f NOUN
perché	WH	perché	f WH
possono	VER2:fin	potere	f VER2:fin VER:fin
avere	VER:infi	avere	f AUX:infi NOUN VER:infi
delle	ARTPRE	della	f ARTPRE
capacità	NOUN	capacità	f NOUN
che	CHE	che	f CHE CON DET:wh
tu	PRO:pers	tu	f PRO:pers
non	NEG	non	f NEG
sai	NOUN	saio	f NOUN VER:fin
.	SENT	.	f SENT

Quella	DET:demo	quello	f DET:demo PRO:demo
volta	NOUN	volta	f ADJ NOUN VER:fin VER:ppast
che	CHE	che	f CHE CON DET:wh
a	PRE	a	f PRE
pattinare	VER:infi	pattinare	f VER:infi
…	NOCAT	<unknown>	s NOCAT
Quando	VER:geru	<unknown>	s NPR VER:geru
ero	AUX:ppre	essere	f AUX:fin AUX:geru AUX:ppre VER:fin
in	PRE	in	f PRE
4°	NOCAT	<unknown>	s NOCAT
elementare	ADJ	elementare	f ADJ
,	PUN	,	f PUN
tutti	PRO:indef	tutti	f ADJ DET:indef PRO:indef
pensavano	VER:fin	pensare	f VER:fin
che	CHE	che	f CHE CON DET:wh
visto	NOUN	visto	f NOUN VER:fin
che	CHE	che	f CHE CON DET:wh
ero	VER:fin	essere	f AUX:fin AUX:geru AUX:ppre VER:fin
magra	ADJ	magro	f ADJ NOUN
ero	VER:fin	essere	f AUX:fin AUX:geru AUX:ppre VER:fin
anche	ADV	anche	f ADV
debole	ADJ	debole	f ADJ NOUN
.	SENT	.	f SENT
Una	ART	una	f ART PRO:indef
volta	NOUN	volta	f ADJ NOUN VER:fin VER:ppast
che	CHE	che	f CHE CON DET:wh
eravamo	VER:fin	essere	f AUX:fin VER:fin
a	PRE	a	f PRE
pattinare	VER:infi	pattinare	f VER:infi
(	PUN	(	f PUN
visto	NOUN	visto	f NOUN VER:fin
che	CHE	che	f CHE CON DET:wh
pattino	NOUN	pattino	f NOUN VER:fin
veloce	ADJ	veloce	f ADJ
)	PUN	)	f PUN
ho	AUX:fin	avere	f AUX:fin VER:fin
voluto	VER:ppast	volere	f ADJ VER2:ppre VER:ppast
fare	VER:infi	fare	f VER2:infi VER:infi
una	ART	una	f ART PRO:indef
gara	NOUN	gara	f NOUN
.	SENT	.	f SENT
Mi	CLI	mi	f CLI
sono	AUX:fin	essere	f AUX:fin VER:fin
guardata	VER:ppast	guardare	f VER:ppast
in	PRE	in	f PRE
giro	NOUN	giro	f NOUN VER:fin
e	CON	e	f CON
,	PUN	,	f PUN
per	PRE	per	f PRE
sfidare	VER:infi	sfidare	f VER:infi
la	ART	la	f ART CLI
sorte	NOUN	sorte	f ADJ NOUN VER:ppast
,	PUN	,	f PUN
ho	AUX:fin	avere	f AUX:fin VER:fin
scelto	VER:ppast	scegliere	f ADJ VER:ppast
di	PRE	di	f PRE
sfidare	VER:infi	sfidare	f VER:infi
il	ART	il	f ART
Franco	NPR	Franco	f NPR
.	SENT	.	f SENT
Lui	PRO:pers	lui	f PRO:pers
diceva	VER:fin	dire	f VER:fin
che	CHE	che	f CHE CON DET:wh
era	VER:fin	essere	f AUX:fin NOUN VER:fin
velocissimo	ADJ	veloce	f ADJ
e	CON	e	f CON
che	CHE	che	f CHE CON DET:wh
non	NEG	non	f NEG
sarei	AUX:fin	essere	f AUX:fin VER:fin
riuscita	VER:ppast	riuscire	f VER:ppast
a	PRE	a	f PRE
batterlo	VER:infi:cli	battere	f VER:infi:cli
!	SENT	!	f SENT
Allora	ADV	allora	f ADV
ha	AUX:fin	avere	f AUX:fin VER:fin
accettato	VER:ppast	accettare	f VER:ppast
,	PUN	,	f PUN
giá	ADJ	<unknown>	s ADJ NOCAT VER:fin
sicuro	ADV	sicuro	f ADJ ADV
di	PRE	di	f PRE
vincere	VER:infi	vincere	f VER:infi
naturalmente	ADV:mente	naturalmente	f ADV:mente
.	SENT	.	f SENT
Ci	CLI	ci	f CLI
siamo	AUX:fin	essere	f AUX:fin VER:fin
posizionati	VER:ppast	posizionare	f VER:ppast
ai	ARTPRE	al	f ARTPRE
blocchi	NOUN	blocco	f NOUN VER:fin
di	PRE	di	f PRE
partenza	NOUN	partenza	f NOUN
e	CON	e	f CON
…	NOCAT	<unknown>	s NOCAT
PRONTI	NPR	<unknown>	s ADJ NOUN NPR VER:fin
,	PUN	,	f PUN
PARTENZA	NPR	<unknown>	s NOUN NPR VER:fin
,	PUN	,	f PUN
VIA	NPR	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
!	SENT	!	f SENT
!	SENT	!	f SENT
!	SENT	!	f SENT
Siamo	VER:fin	essere	f AUX:fin VER:fin
partiti	NOUN	partito	f ADJ NOUN VER:ppast
,	PUN	,	f PUN
all’	ADJ	<unknown>	s ADJ NOCAT VER:fin
inizio	NOUN	inizio	f NOUN VER:fin
era	VER:fin	essere	f AUX:fin NOUN VER:fin
il	ART	il	f ART
Franco	NPR	Franco	f NPR
in	PRE	in	f PRE
testa	NOUN	testa	f NOUN VER:fin
poi	ADV	poi	f ADV
pian	VER:fin	piare	f VER:fin
piano	NOUN	piano	f ADJ NOUN VER:fin
si	CLI	si	f CLI
é	AUX:fin	essere	f AUX:fin VER:fin
stancato	VER:ppast	stancare	f VER:ppast
.	SENT	.	f SENT
Allora	ADV	allora	f ADV
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
ho	AUX:fin	avere	f AUX:fin VER:fin
superato	VER:ppast	superare	f ADJ VER:ppast
,	PUN	,	f PUN
quando	WH	quando	f WH
lui	PRO:pers	lui	f PRO:pers
se	CON	se	f CLI CON
ne	CLI	ne	f CLI
è	AUX:fin	essere	f AUX:fin VER:fin
accorto	VER:ppast	accorgere	f ADJ VER:ppast
ha	AUX:fin	avere	f AUX:fin VER:fin
gasato	VER:ppast	gasare	f ADJ VER:ppast
.	SENT	.	f SENT
Io	PRO:pers	io	f PRO:pers
ce	CLI	ce	f CLI
lo	CLI	lo	f ART CLI
avevo	VER:fin	avere	f AUX:fin VER:fin
alle	ARTPRE	alla	f ARTPRE
calcagna	NOUN	calcagno	f NOUN
,	PUN	,	f PUN
peró	ADJ	<unknown>	s ADJ NOCAT VER:fin
non	NEG	non	f NEG
é	AUX:fin	essere	f AUX:fin VER:fin
piu	ADV	più	f ADV
riuscito	VER:ppast	riuscire	f VER:ppast
a	PRE	a	f PRE
superarmi	VER:infi:cli	superare	f VER:infi:cli
.	SENT	.	f SENT
E	CON	e	f CON
alla	ARTPRE	alla	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
HO	NPR	<unknown>	s NPR VER:fin
VINTO	NPR	<unknown>	s ADJ NPR VER:ppast
IO	NPR	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
!	SENT	!	f SENT
Questo	DET:demo	questo	f DET:demo PRO:demo
episodio	NOUN	episodio	f NOUN
rispecchia	VER:fin	rispecchiare	f VER:fin
perfettamente	ADV:mente	perfettamente	f ADV:mente
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
insegnamento	NOUN	insegnamento	f NOUN
della	ARTPRE	della	f ARTPRE
favola	NOUN	favola	f NOUN
di	PRE	di	f PRE
Esopo	NPR	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
“	NOCAT	<unknown>	s NOCAT
La	VER:fin	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
tartaruga	NOUN	tartaruga	f NOUN
e	CON	e	f CON
la	ART	la	f ART CLI
lepre	NOUN	lepre	f NOUN
”	NOCAT	<unknown>	s NOCAT
.	SENT	.	f SENT

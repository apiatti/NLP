Un	ART	un	f ART
anno	NOUN	anno	f NOUN
fa	ADV	fa	f ADV VER2:fin VER:fin
,	PUN	,	f PUN
stavamo	VER:fin	stare	f VER:fin
giocando	VER:geru	giocare	f VER:geru
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
ultima	ADJ	ultimo	f ADJ NOUN VER:fin
partita	NOUN	partita	f ADJ NOUN VER:ppast
di	PRE	di	f PRE
campionato	NOUN	campionato	f NOUN VER:ppast
,	PUN	,	f PUN
e	CON	e	f CON
stavamo	VER:fin	stare	f VER:fin
perdendo	VER:geru	perdere	f VER:geru
quattro	DET:num	quattro	f DET:num NOUN PRO:num
a	PRE	a	f PRE
due	DET:num	due	f DET:num PRO:num
.	SENT	.	f SENT
Mancavano	VER:fin	mancare	f VER:fin
15	NUM	@card@	f NUM
minuti	NOUN	minuto	f ADJ NOUN
alla	ARTPRE	alla	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
della	ARTPRE	della	f ARTPRE
partita	NOUN	partita	f ADJ NOUN VER:ppast
e	CON	e	f CON
noi	PRO:pers	noi	f PRO:pers
eravamo	VER:fin	essere	f AUX:fin VER:fin
stanchissimi	ADJ	stanco	f ADJ
,	PUN	,	f PUN
il	ART	il	f ART
nostro	DET:poss	nostro	f DET:poss PRO:poss
allenatore	NOUN	allenatore	f NOUN
stava	VER:fin	stare	f VER:fin
gridando	VER:geru	gridare	f VER:geru
tantissimo	ADJ	tanto	f ADJ
.	SENT	.	f SENT
Dopo	PRE	dopo	f ADJ ADV CON PRE
cinque	DET:num	cinque	f DET:num PRO:num
minuti	NOUN	minuto	f ADJ NOUN
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
fatto	VER:ppast	fare	f NOUN VER2:ppast VER:ppast
un	ART	un	f ART
goal	NOUN	goal	f NOUN
,	PUN	,	f PUN
e	CON	e	f CON
da	PRE	da	f PRE
lí	ADJ	<unknown>	s ADJ NOCAT VER:fin
ci	CLI	ci	f CLI
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
creduto	VER:ppast	credere	f VER:ppast
fino	PRE	fino	f ADJ PRE
alla	ARTPRE	alla	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
.	SENT	.	f SENT
Poi	ADV	poi	f ADV
un’	ADJ	<unknown>	s ADJ NOCAT VER:fin
altro	DET:indef	altro	f ADJ DET:indef PRO:indef
goal	NOUN	goal	f NOUN
e	CON	e	f CON
allora	ADV	allora	f ADV
divenne	VER:fin	divenire	f VER:fin
un	ART	un	f ART
pareggio	NOUN	pareggio	f NOUN VER:fin
.	SENT	.	f SENT
Mancavano	VER:fin	mancare	f VER:fin
due	DET:num	due	f DET:num PRO:num
minuti	NOUN	minuto	f ADJ NOUN
e	CON	e	f CON
stavamo	VER:fin	stare	f VER:fin
lottando	VER:geru	lottare	f VER:geru
per	PRE	per	f PRE
vincere	VER:infi	vincere	f VER:infi
,	PUN	,	f PUN
poi	ADV	poi	f ADV
proprio	ADV	proprio	f ADJ ADV DET:poss PRO:poss
quando	WH	quando	f WH
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
albitro	NOUN	<unknown>	s ADJ NOUN VER:fin
stava	VER:fin	stare	f VER:fin
per	PRE	per	f PRE
fischiare	VER:infi	fischiare	f VER:infi
la	ART	la	f ART CLI
fine	NOUN	fine	f ADJ NOUN
,	PUN	,	f PUN
io	PRO:pers	io	f PRO:pers
ho	AUX:fin	avere	f AUX:fin VER:fin
fatto	VER:ppast	fare	f NOUN VER2:ppast VER:ppast
il	ART	il	f ART
goal	NOUN	goal	f NOUN
decisivo	ADJ	decisivo	f ADJ
e	CON	e	f CON
quindi	ADV	quindi	f ADV
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
vinto	VER:ppast	vincere	f ADJ VER:ppast
la	ART	la	f ART CLI
partita	NOUN	partita	f ADJ NOUN VER:ppast
.	SENT	.	f SENT
Questa	DET:demo	questo	f DET:demo PRO:demo
esperienza	NOUN	esperienza	f NOUN
mi	CLI	mi	f CLI
ha	AUX:fin	avere	f AUX:fin VER:fin
insegnato	VER:ppast	insegnare	f VER:ppast
che	CHE	che	f CHE CON DET:wh
anche	ADV	anche	f ADV
se	CLI	se	f CLI CON
stai	VER:fin	stare	f VER:fin
perdendo	VER:geru	perdere	f VER:geru
la	ART	la	f ART CLI
partita	NOUN	partita	f ADJ NOUN VER:ppast
devi	VER2:fin	dovere	f VER2:fin VER:fin
comunque	WH	comunque	f WH
dare	VER:infi	dare	f VER:infi
il	ART	il	f ART
massimo	NOUN	massimo	f ADJ NOUN
e	CON	e	f CON
crederci	VER:infi:cli	credere	f VER:infi:cli
fino	PRE	fino	f ADJ PRE
alla	ARTPRE	alla	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
.	SENT	.	f SENT

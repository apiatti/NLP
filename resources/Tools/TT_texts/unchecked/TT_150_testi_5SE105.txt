Torneo	NOUN	torneo	f NOUN VER:fin
calcio	NOUN	calcio	f NOUN VER:fin
scolari	ADJ	scolare	f ADJ NOUN
sapete	VER:fin	sapere	f VER:fin
tutti	PRO:indef	tutti	f ADJ DET:indef PRO:indef
cos’é	VER:fin	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
il	ART	il	f ART
torneo	NOUN	torneo	f NOUN VER:fin
calcio	NOUN	calcio	f NOUN VER:fin
scolari	ADJ	scolare	f ADJ NOUN
(	PUN	(	f PUN
torneo	NOUN	torneo	f NOUN VER:fin
calcio	NOUN	calcio	f NOUN VER:fin
scolari	ADJ	scolare	f ADJ NOUN
e	CON	e	f CON
un	ART	un	f ART
torneo	NOUN	torneo	f NOUN VER:fin
di	PRE	di	f PRE
calcio	NOUN	calcio	f NOUN VER:fin
per	PRE	per	f PRE
i	ART	il	f ART
bambini	NOUN	bambino	f NOUN
delle	ARTPRE	della	f ARTPRE
classi	NOUN	classe	f NOUN
che	CHE	che	f CHE CON DET:wh
si	CLI	si	f CLI
offrono	VER:fin	offrire	f VER:fin
volontari	NOUN	volontario	f ADJ NOUN
)	PUN	)	f PUN
.	SENT	.	f SENT
stavamo	VER:fin	stare	f VER:fin
partecipando	VER:geru	partecipare	f VER:geru
propio	VER:fin	<unknown>	s ADJ NOUN VER:fin
a	PRE	a	f PRE
quel	DET:demo	quello	f DET:demo
torneo	NOUN	torneo	f NOUN VER:fin
.	SENT	.	f SENT
Eravamo	VER:fin	essere	f AUX:fin VER:fin
ad	PRE	ad	f PRE
allenarci	VER:infi:cli	allenare	f VER:infi:cli
per	PRE	per	f PRE
la	ART	la	f ART CLI
prima	ADJ	primo	f ADJ ADV
partita	NOUN	partita	f ADJ NOUN VER:ppast
,	PUN	,	f PUN
entriamo	VER:fin	entrare	f VER:fin
in	PRE	in	f PRE
campo	NOUN	campo	f NOUN VER:fin
e	CON	e	f CON
gli	ART	il	f ART CLI
avversari	NOUN	avversario	f NOUN
erano	VER:fin	essere	f AUX:fin VER:fin
,	PUN	,	f PUN
anzi	ADV	anzi	f ADV
sembravano	VER:fin	sembrare	f VER:fin
,	PUN	,	f PUN
un	ART	un	f ART
po	NOUN	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
deboluci	ADJ	<unknown>	s ADJ NOUN VER:fin
ma	CON	ma	f CON
non	NEG	non	f NEG
era	VER:fin	essere	f AUX:fin NOUN VER:fin
così	ADV	così	f ADV
.	SENT	.	f SENT
Alla	ARTPRE	alla	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
della	ARTPRE	della	f ARTPRE
partita	NOUN	partita	f ADJ NOUN VER:ppast
il	ART	il	f ART
risultato	NOUN	risultato	f NOUN VER:ppast
era	VER:fin	essere	f AUX:fin NOUN VER:fin
3-0	NUM	@card@	f NUM
per	PRE	per	f PRE
loro	PRO:pers	loro	f DET:poss PRO:pers PRO:poss
,	PUN	,	f PUN
mica	ADV	mica	f ADV
per	PRE	per	f PRE
noi	PRO:pers	noi	f PRO:pers
,	PUN	,	f PUN
e	CON	e	f CON
lì	ADV	lì	f ADV
ci	CLI	ci	f CLI
sembrava	VER:fin	sembrare	f VER:fin
di	PRE	di	f PRE
essere	VER:infi	essere	f AUX:infi NOUN VER:infi
un	ART	un	f ART
po	NOUN	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
’	NOCAT	<unknown>	s NOCAT
deboluci	ADJ	<unknown>	s ADJ NOUN VER:fin
noi	PRO:pers	noi	f PRO:pers
.	SENT	.	f SENT
Alla	ARTPRE	alla	f ARTPRE
seconda	ADJ	secondo	f ADJ NOUN VER:fin
partita	NOUN	partita	f ADJ NOUN VER:ppast
5-0	NUM	@card@	f NUM
che	CHE	che	f CHE CON DET:wh
vergogna	NOUN	vergogna	f NOUN VER:fin
ma	CON	ma	f CON
poi	ADV	poi	f ADV
arrivò	VER:fin	arrivare	f VER:fin
la	ART	la	f ART CLI
maestra	NOUN	maestra	f ADJ NOUN
e	CON	e	f CON
ci	CLI	ci	f CLI
dà	VER:fin	dare	f VER:fin
un	ART	un	f ART
gelato	NOUN	gelato	f ADJ NOUN VER:ppast
,	PUN	,	f PUN
a	PRE	a	f PRE
mi	CLI	mi	f CLI
sono	AUX:fin	essere	f AUX:fin VER:fin
dimenticato	VER:ppast	dimenticare	f ADJ VER:ppast
avvevamo	VER:fin	<unknown>	s VER:fin
fatto	VER:ppast	fare	f NOUN VER2:ppast VER:ppast
pranzo	NOUN	pranzo	f NOUN VER:fin
.	SENT	.	f SENT
E	CON	e	f CON
a	PRE	a	f PRE
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
ultima	ADJ	ultimo	f ADJ NOUN VER:fin
partita	NOUN	partita	f ADJ NOUN VER:ppast
con	PRE	con	f PRE
dei	ARTPRE	del	f ARTPRE NOUN
piccoletti	NOUN	piccoletto	f NOUN
ma	CON	ma	f CON
la	ART	la	f ART CLI
loro	DET:poss	loro	f DET:poss PRO:pers PRO:poss
forza	NOUN	forza	f NOUN VER:fin
del	ARTPRE	del	f ARTPRE
calcio	NOUN	calcio	f NOUN VER:fin
era	VER:fin	essere	f AUX:fin NOUN VER:fin
gigantesca	ADJ	gigantesco	f ADJ
e	CON	e	f CON
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
perso	VER:ppast	perdere	f ADJ VER:ppast
9-0	NUM	@card@	f NUM
qua	ADV	qua	f ADV
in	PRE	in	f PRE
questa	DET:demo	questo	f DET:demo PRO:demo
partita	NOUN	partita	f ADJ NOUN VER:ppast
altro	PRO:indef	altro	f ADJ DET:indef PRO:indef
che	CHE	che	f CHE CON DET:wh
tristezza	NOUN	tristezza	f NOUN
come	WH	come	f WH
le	ART	la	f ART CLI
altre	PRO:indef	altre	f ADJ DET:indef PRO:indef
e	CON	e	f CON
certi	ADJ	certo	f ADJ
si	CLI	si	f CLI
sono	AUX:fin	essere	f AUX:fin VER:fin
messi	VER:ppast	mettere	f ADJ NOUN VER:ppast
a	PRE	a	f PRE
piangere	VER:infi	piangere	f VER:infi
.	SENT	.	f SENT
Ma	CON	ma	f CON
sembravano	VER:fin	sembrare	f VER:fin
così	ADV	così	f ADV
debolucci	ADJ	<unknown>	s ADJ NOUN VER:fin
.	SENT	.	f SENT
Ma	CON	ma	f CON
perdere	VER:infi	perdere	f VER:infi
9-0	NUM	@card@	f NUM
,	PUN	,	f PUN
non	NEG	non	f NEG
melavevo	VER:fin	<unknown>	s ADJ NOUN VER:fin
mai	ADV	mai	f ADV
immaginato	VER:ppast	immaginare	f VER:ppast
di	PRE	di	f PRE
perdere	VER:infi	perdere	f VER:infi
così	ADV	così	f ADV
tanto	ADV	tanto	f ADJ ADV DET:indef
.	SENT	.	f SENT
Quel	DET:demo	quello	f DET:demo
giorno	NOUN	giorno	f NOUN
ho	AUX:fin	avere	f AUX:fin VER:fin
scoperto	VER:ppast	scoprire	f ADJ VER:ppast
che	CHE	che	f CHE CON DET:wh
non	NEG	non	f NEG
si	CLI	si	f CLI
deve	VER2:fin	dovere	f VER2:fin VER:fin
mai	ADV	mai	f ADV
sottovalutare	VER:infi	sottovalutare	f VER:infi
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
avversario	NOUN	avversario	f NOUN
se	CON	se	f CLI CON
non	NEG	non	f NEG
si	CLI	si	f CLI
sa	VER:fin	sapere	f VER:fin
la	ART	la	f ART CLI
sua	DET:poss	suo	f DET:poss PRO:poss
forza	NOUN	forza	f NOUN VER:fin
.	SENT	.	f SENT

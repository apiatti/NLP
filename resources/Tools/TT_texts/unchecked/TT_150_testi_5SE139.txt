La	ART	la	f ART CLI
pioggia	NOUN	pioggia	f NOUN
Un	VER:fin	<unknown>	s NPR VER:fin
pomeriggio	NOUN	pomeriggio	f NOUN
stava	VER:fin	stare	f VER:fin
diluviando	VER:geru	diluviare	f VER:geru
,	PUN	,	f PUN
e	CON	e	f CON
la	ART	la	f ART CLI
mamma	NOUN	mamma	f NOUN
mi	CLI	mi	f CLI
aveva	AUX:fin	avere	f AUX:fin VER:fin
detto	VER:ppast	dire	f VER:fin VER:ppast
di	PRE	di	f PRE
mettere	VER:infi	mettere	f VER:infi
la	ART	la	f ART CLI
mantellina	NOUN	mantellina	f NOUN
e	CON	e	f CON
prendere	VER:infi	prendere	f VER:infi
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
ombrello	NOUN	ombrello	f NOUN
,	PUN	,	f PUN
ma	CON	ma	f CON
io	PRO:pers	io	f PRO:pers
volendo	VER2:geru	volere	f VER2:geru VER:geru
mettere	VER:infi	mettere	f VER:infi
solo	ADV	solo	f ADJ ADV VER:fin
la	ART	la	f ART CLI
mantellina	NOUN	mantellina	f NOUN
,	PUN	,	f PUN
sono	AUX:fin	essere	f AUX:fin VER:fin
andata	VER:ppast	andare	f ADJ VER:ppast
a	PRE	a	f PRE
prendere	VER:infi	prendere	f VER:infi
il	ART	il	f ART
bus	NOUN	bus	f NOUN
senza	PRE	senza	f CON PRE
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
ombrello	NOUN	ombrello	f NOUN
.	SENT	.	f SENT
Così	ADV	così	f ADV
appena	ADV	appena	f ADV
uscií	ADJ	<unknown>	s ADJ NOCAT VER:fin
di	PRE	di	f PRE
casa	NOUN	casa	f NOUN
ero	AUX:fin	essere	f AUX:fin AUX:geru AUX:ppre VER:fin
bagnata	VER:ppast	bagnare	f ADJ VER:ppast
dalla	ARTPRE	dalla	f ARTPRE
punta	NOUN	punta	f ADJ NOUN VER:fin VER:ppast
dei	ARTPRE	del	f ARTPRE NOUN
piedi	NOUN	piede	f NOUN
alla	ARTPRE	alla	f ARTPRE
punta	NOUN	punta	f ADJ NOUN VER:fin VER:ppast
della	ARTPRE	della	f ARTPRE
testa	NOUN	testa	f NOUN VER:fin
.	SENT	.	f SENT
Sono	AUX:fin	essere	f AUX:fin VER:fin
arrivata	VER:ppast	arrivare	f ADJ VER:ppast
a	PRE	a	f PRE
scuola	NOUN	scuola	f NOUN
zuppa	ADJ	zuppo	f ADJ NOUN
e	CON	e	f CON
avevo	VER:fin	avere	f AUX:fin VER:fin
freddo	ADJ	freddo	f ADJ NOUN VER:fin
.	SENT	.	f SENT
Quindi	ADV	quindi	f ADV
ho	AUX:fin	avere	f AUX:fin VER:fin
imparato	VER:ppast	imparare	f VER:ppast
che	CHE	che	f CHE CON DET:wh
quando	WH	quando	f WH
la	ART	la	f ART CLI
mamma	NOUN	mamma	f NOUN
o	CON	o	f CON
il	ART	il	f ART
papá	ADJ	<unknown>	s ADJ NOCAT VER:fin
ti	CLI	ti	f CLI
dicono	VER:fin	dire	f VER:fin
qualcosa	PRO:indef	qualcosa	f PRO:indef
devi	VER2:fin	dovere	f VER2:fin VER:fin
ascoltarli	VER:infi:cli	ascoltare	f VER:infi:cli
,	PUN	,	f PUN
perché	WH	perché	f WH
é	VER:fin	essere	f AUX:fin VER:fin
per	PRE	per	f PRE
il	ART	il	f ART
tuo	DET:poss	tuo	f ADJ DET:poss PRO:poss
bene	NOUN	bene	f ADV NOUN
che	CHE	che	f CHE CON DET:wh
te	PRO:pers	te	f CLI PRO:pers
lo	CLI	lo	f ART CLI
dicono	VER:fin	dire	f VER:fin
.	SENT	.	f SENT
Sono	AUX:fin	essere	f AUX:fin VER:fin
fatti	VER:ppast	fare	f NOUN VER2:ppast VER:ppast
per	PRE	per	f PRE
quello	PRO:demo	quello	f DET:demo PRO:demo
:	PUN	:	f PUN
per	PRE	per	f PRE
volerti	VER:infi:cli	volere	f VER2:infi:cli VER:infi:cli
bene	ADV	bene	f ADV NOUN
,	PUN	,	f PUN
per	PRE	per	f PRE
stare	VER:infi	stare	f ADV VER:infi
insieme	ADV	insieme	f ADV NOUN PRE
,	PUN	,	f PUN
per	PRE	per	f PRE
proteggerti	VER:infi:cli	<unknown>	s ADJ NOUN VER:fin VER:infi:cli VER:ppast
e	CON	e	f CON
per	PRE	per	f PRE
altre	DET:indef	altro	f ADJ DET:indef PRO:indef
tantissimissime	ADJ	<unknown>	s ADJ
cose	NOUN	cosa	f NOUN
.	SENT	.	f SENT
Quindi	ADV	quindi	f ADV
é	VER:fin	essere	f AUX:fin VER:fin
bello	ADJ	bello	f ADJ
ascoltarli	VER:infi:cli	ascoltare	f VER:infi:cli
,	PUN	,	f PUN
perché	WH	perché	f WH
fai	VER:fin	fare	f VER2:fin VER:fin
la	ART	la	f ART CLI
cosa	NOUN	cosa	f NOUN WH
giusta	ADJ	giusto	f ADJ
,	PUN	,	f PUN
anche	ADV	anche	f ADV
se	CON	se	f CLI CON
certe	ADJ	certo	f ADJ
volte	NOUN	volta	f ADJ NOUN VER:ppast
pensi	VER:fin	pensare	f VER:fin
il	ART	il	f ART
contrario	NOUN	contrario	f ADJ NOUN VER:fin
.	SENT	.	f SENT

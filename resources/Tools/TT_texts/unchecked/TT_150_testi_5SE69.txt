Campionessa	NOUN	campionessa	f NOUN
di	PRE	di	f PRE
Wibit	NPR	<unknown>	s NOUN NPR
Un	VER:fin	<unknown>	s NPR VER:fin
giorno	NOUN	giorno	f NOUN
,	PUN	,	f PUN
quando	WH	quando	f WH
ero	VER:fin	essere	f AUX:fin AUX:geru AUX:ppre VER:fin
da	PRE	da	f PRE
mia	DET:poss	mio	f DET:poss PRO:poss
zia	NOUN	zia	f NOUN
siamo	AUX:fin	essere	f AUX:fin VER:fin
andate	VER:ppast	andare	f ADJ VER:fin VER:ppast
al	ARTPRE	al	f ARTPRE
California	NPR	California	f NPR
,	PUN	,	f PUN
un	ART	un	f ART
centro	NOUN	centro	f NOUN VER:fin
balneare	ADJ	balneare	f ADJ
situato	VER:ppast	situare	f VER:ppast
a	PRE	a	f PRE
Balerna	NPR	<unknown>	s ADJ NPR VER:fin
.	SENT	.	f SENT
Con	PRE	con	f PRE
noi	PRO:pers	noi	f PRO:pers
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
erano	VER:fin	essere	f AUX:fin VER:fin
mia	DET:poss	mio	f DET:poss PRO:poss
sorella	NOUN	sorella	f NOUN
e	CON	e	f CON
mia	DET:poss	mio	f DET:poss PRO:poss
cugina	NOUN	cugina	f NOUN
.	SENT	.	f SENT
Al	ARTPRE	al	f ARTPRE NPR
California	NPR	California	f NPR
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
era	VER:fin	essere	f AUX:fin NOUN VER:fin
il	ART	il	f ART
Wibit	NPR	<unknown>	s NOUN NPR
,	PUN	,	f PUN
un	ART	un	f ART
percorso	NOUN	percorso	f ADJ NOUN VER:ppast
gonfiabile	ADJ	<unknown>	s ADJ VER:fin
che	CHE	che	f CHE CON DET:wh
attraversava	VER:fin	attraversare	f VER:fin
la	ART	la	f ART CLI
piscina	NOUN	piscina	f NOUN
.	SENT	.	f SENT
Dopo	PRE	dopo	f ADJ ADV CON PRE
qualche	DET:indef	qualche	f ADJ DET:indef
minuto	NOUN	minuto	f ADJ NOUN
,	PUN	,	f PUN
agli	ARTPRE	al	f ARTPRE
autoparlanti	VER:ppre	<unknown>	s ADJ NOUN VER:fin VER:ppast VER:ppre
hanno	AUX:fin	avere	f AUX:fin VER:fin
annunciato	VER:ppast	annunciare	f VER:ppast
che	CHE	che	f CHE CON DET:wh
nel	ARTPRE	nel	f ARTPRE
tardo	ADJ	tardo	f ADJ ADV VER:fin
pomeriggio	NOUN	pomeriggio	f NOUN
si	CLI	si	f CLI
sarebbe	AUX:fin	essere	f AUX:fin VER:fin
svolta	VER:ppast	svolgere	f ADJ NOUN VER:fin VER:ppast
una	ART	una	f ART PRO:indef
gara	NOUN	gara	f NOUN
.	SENT	.	f SENT
La	ART	la	f ART CLI
gara	NOUN	gara	f NOUN
consisteva	VER:fin	consistere	f VER:fin
nel	ARTPRE	nel	f ARTPRE
svolgere	VER:infi	svolgere	f VER:infi
il	ART	il	f ART
percorso	NOUN	percorso	f ADJ NOUN VER:ppast
senza	PRE	senza	f CON PRE
cadere	VER:infi	cadere	f VER:infi
in	PRE	in	f PRE
acqua	NOUN	acqua	f NOUN
e	CON	e	f CON
metterci	VER:infi:cli	mettere	f VER:infi:cli
il	ART	il	f ART
minor	ADJ	minore	f ADJ
tempo	NOUN	tempo	f NOUN
possibile	ADJ	possibile	f ADJ
ad	PRE	ad	f PRE
arrivare	VER:infi	arrivare	f VER:infi
alla	ARTPRE	alla	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
.	SENT	.	f SENT
Il	ART	il	f ART
percorso	NOUN	percorso	f ADJ NOUN VER:ppast
era	AUX:fin	essere	f AUX:fin NOUN VER:fin
formato	VER:ppast	formare	f ADJ NOUN VER:ppast
da	PRE	da	f PRE
una	ART	una	f ART PRO:indef
parte	NOUN	parte	f NOUN VER:fin
in	PRE	in	f PRE
cui	WH	cui	f WH
bisogna	VER:fin	bisognare	f VER2:fin VER:fin
attaccarsi	VER:infi:cli	attaccare	f VER:infi:cli
a	PRE	a	f PRE
delle	ARTPRE	della	f ARTPRE
maniglie	NOUN	maniglia	f NOUN
e	CON	e	f CON
spostarsi	VER:infi:cli	spostare	f VER:infi:cli
,	PUN	,	f PUN
una	ART	una	f ART PRO:indef
dove	WH	dove	f WH
si	CLI	si	f CLI
deve	VER2:fin	dovere	f VER2:fin VER:fin
stare	VER:infi	stare	f ADV VER:infi
in	PRE	in	f PRE
equilibrio	NOUN	equilibrio	f NOUN
e	CON	e	f CON
caminare	VER:infi	<unknown>	s ADJ NOUN VER:infi
sopra	ADV	sopra	f ADJ ADV PRE
a	PRE	a	f PRE
delle	ARTPRE	della	f ARTPRE
parti	NOUN	parte|parto	f NOUN VER:fin
gonfiabili	ADJ	<unknown>	s ADJ NOUN VER:fin
,	PUN	,	f PUN
una	ART	una	f ART PRO:indef
dove	WH	dove	f WH
si	CLI	si	f CLI
deve	VER2:fin	dovere	f VER2:fin VER:fin
scavalcare	VER:infi	scavalcare	f VER:infi
una	ART	una	f ART PRO:indef
specie	NOUN	specie	f ADV NOUN
di	PRE	di	f PRE
gabbia	NOUN	gabbia	f NOUN
e	CON	e	f CON
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
ultima	ADJ	ultimo	f ADJ NOUN VER:fin
si	CLI	si	f CLI
deve	VER2:fin	dovere	f VER2:fin VER:fin
fare	VER:infi	fare	f VER2:infi VER:infi
uno	ART	un	f ART NOUN PRO:indef
scivolo	NOUN	scivolo	f NOUN VER:fin
.	SENT	.	f SENT
Io	PRO:pers	io	f PRO:pers
mi	CLI	mi	f CLI
allenai	VER:fin	allenare	f VER:fin
molto	ADV	molto	f ADJ ADV DET:indef
e	CON	e	f CON
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
era	VER:fin	essere	f AUX:fin NOUN VER:fin
una	ART	una	f ART PRO:indef
bambina	NOUN	bambina	f NOUN
che	CHE	che	f CHE CON DET:wh
mi	CLI	mi	f CLI
fissava	VER:fin	fissare	f VER:fin
sicura	ADJ	sicuro	f ADJ
che	CHE	che	f CHE CON DET:wh
avrebbe	AUX:fin	avere	f AUX:fin VER:fin
vinto	VER:ppast	vincere	f ADJ VER:ppast
.	SENT	.	f SENT
Poi	ADV	poi	f ADV
arrivò	VER:fin	arrivare	f VER:fin
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
ora	ADV	ora	f ADV NOUN VER:fin
della	ARTPRE	della	f ARTPRE
gara	NOUN	gara	f NOUN
la	ART	la	f ART CLI
bambina	NOUN	bambina	f NOUN
partí	ADJ	<unknown>	s ADJ NOCAT VER:fin
per	PRE	per	f PRE
prima	ADV	prima	f ADJ ADV
ma	CON	ma	f CON
alla	ARTPRE	alla	f ARTPRE
seconda	ADJ	secondo	f ADJ NOUN VER:fin
parte	NOUN	parte	f NOUN VER:fin
del	ARTPRE	del	f ARTPRE
percorso	NOUN	percorso	f ADJ NOUN VER:ppast
cadde	VER:fin	cadere	f VER:fin
in	PRE	in	f PRE
acqua	NOUN	acqua	f NOUN
.	SENT	.	f SENT
Dopo	PRE	dopo	f ADJ ADV CON PRE
mia	DET:poss	mio	f DET:poss PRO:poss
cugina	NOUN	cugina	f NOUN
,	PUN	,	f PUN
andai	VER:fin	andare	f VER:fin
io	PRO:pers	io	f PRO:pers
.	SENT	.	f SENT
Non	NEG	non	f NEG
feci	VER:fin	fare	f NOUN VER2:fin VER:fin
neanche	ADV	neanche	f ADV
uno	ART	un	f ART NOUN PRO:indef
sbaglio	NOUN	sbaglio	f NOUN VER:fin
,	PUN	,	f PUN
e	CON	e	f CON
arrivai	VER:fin	arrivare	f VER:fin
2°	NOCAT	<unknown>	s NOCAT
,	PUN	,	f PUN
1°	NOCAT	<unknown>	s NOCAT
arrivò	VER:fin	arrivare	f VER:fin
mia	DET:poss	mio	f DET:poss PRO:poss
cugina	NOUN	cugina	f NOUN
e	CON	e	f CON
per	PRE	per	f PRE
ultima	NOUN	ultima	f ADJ NOUN VER:fin
quella	PRO:demo	quella	f DET:demo PRO:demo
vanitosa	ADJ	vanitoso	f ADJ
di	PRE	di	f PRE
una	ART	una	f ART PRO:indef
bambina	NOUN	bambina	f NOUN
.	SENT	.	f SENT
Io	PRO:pers	io	f PRO:pers
vinsi	VER:fin	vincere	f VER:fin
1	NUM	@card@	f NUM
buono	NOUN	buono	f ADJ NOUN
entrata	VER:ppast	entrare	f NOUN VER:ppast
per	PRE	per	f PRE
la	ART	la	f ART CLI
piscina	NOUN	piscina	f NOUN
.	SENT	.	f SENT
Se	CON	se	f CLI CON
non	NEG	non	f NEG
ci	CLI	ci	f CLI
si	CLI	si	f CLI
impegna	VER:fin	impegnare	f VER:fin
,	PUN	,	f PUN
non	NEG	non	f NEG
si	CLI	si	f CLI
ottiene	VER:fin	ottenere	f VER:fin
nulla	PRO:indef	nulla	f ADJ PRO:indef
.	SENT	.	f SENT

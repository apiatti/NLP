IL	ART	il	f ART
TORNEO	NPR	<unknown>	s ADJ NOUN NPR VER:fin
Una	VER:fin	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
domenica	NOUN	domenica	f NOUN
(	PUN	(	f PUN
un	ART	un	f ART
pò	PRO:indef	po'	f PRO:indef
di	PRE	di	f PRE
mesi	NOUN	mese	f NOUN
fa	ADV	fa	f ADV VER2:fin VER:fin
)	PUN	)	f PUN
io	PRO:pers	io	f PRO:pers
e	CON	e	f CON
la	ART	la	f ART CLI
mia	DET:poss	mio	f DET:poss PRO:poss
squadra	NOUN	squadra	f NOUN VER:fin
di	PRE	di	f PRE
calcio	NOUN	calcio	f NOUN VER:fin
(	PUN	(	f PUN
Joana	NPR	<unknown>	s ADJ NOUN NPR VER:fin
,	PUN	,	f PUN
io	PRO:pers	io	f PRO:pers
Letizia	NPR	Letizia	f NPR
,	PUN	,	f PUN
Chiara	NPR	Chiara	f NPR
,	PUN	,	f PUN
Natali	NPR	<unknown>	s ADJ NOUN NPR VER:fin:cli
,	PUN	,	f PUN
Sara	NPR	Sara	f NPR
,	PUN	,	f PUN
Altea	NPR	<unknown>	s ADJ NOUN NPR
)	PUN	)	f PUN
ci	CLI	ci	f CLI
stavamo	VER:fin	stare	f VER:fin
dirigendo	VER:geru	dirigere	f VER:geru
verso	PRE	verso	f ADJ NOUN PRE VER:fin
Ascona	NPR	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
,	PUN	,	f PUN
dove	WH	dove	f WH
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
era	VER:fin	essere	f AUX:fin NOUN VER:fin
un	ART	un	f ART
torneo	NOUN	torneo	f NOUN VER:fin
.	SENT	.	f SENT
Arrivate	VER:ppast	arrivare	f ADJ VER:fin VER:ppast
ad	PRE	ad	f PRE
Ascona	NPR	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
,	PUN	,	f PUN
in	PRE	in	f PRE
spogliatoio	NOUN	spogliatoio	f NOUN
,	PUN	,	f PUN
ci	CLI	ci	f CLI
siamo	AUX:fin	essere	f AUX:fin VER:fin
cambiate	VER:ppast	cambiare	f VER:fin VER:ppast
(	PUN	(	f PUN
io	PRO:pers	io	f PRO:pers
avevo	VER:fin	avere	f AUX:fin VER:fin
il	ART	il	f ART
numero	NOUN	numero	f NOUN VER:fin
13	NUM	@card@	f NUM
per	PRE	per	f PRE
giocare	VER:infi	giocare	f VER:infi
in	PRE	in	f PRE
difesa	NOUN	difesa	f ADJ NOUN VER:ppast
e	CON	e	f CON
il	ART	il	f ART
no.	ADJ	<unknown>	s ADJ NOCAT NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
1	NUM	@card@	f NUM
per	PRE	per	f PRE
giocare	VER:infi	giocare	f VER:infi
in	PRE	in	f PRE
porta	NOUN	porta	f ADJ NOUN VER:fin VER:ppast
,	PUN	,	f PUN
visto	NOUN	visto	f NOUN VER:fin
che	CHE	che	f CHE CON DET:wh
in	PRE	in	f PRE
squadra	NOUN	squadra	f NOUN VER:fin
ci	CLI	ci	f CLI
scambiamo	VER:fin	scambiare	f VER:fin
i	ART	il	f ART
ruoli	NOUN	ruolo	f NOUN
)	PUN	)	f PUN
,	PUN	,	f PUN
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
iniziato	VER:ppast	iniziare	f ADJ VER:ppast
la	ART	la	f ART CLI
partita	NOUN	partita	f ADJ NOUN VER:ppast
con	PRE	con	f PRE
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
Ascona	VER:fin	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
io	PRO:pers	io	f PRO:pers
ero	VER:fin	essere	f AUX:fin AUX:geru AUX:ppre VER:fin
in	PRE	in	f PRE
difesa	NOUN	difesa	f ADJ NOUN VER:ppast
,	PUN	,	f PUN
Joana	NPR	<unknown>	s ADJ NOUN NPR VER:fin
in	PRE	in	f PRE
porta	NOUN	porta	f ADJ NOUN VER:fin VER:ppast
,	PUN	,	f PUN
Letizia	NPR	Letizia	f NPR
in	PRE	in	f PRE
attacco	NOUN	attacco	f NOUN VER:fin
,	PUN	,	f PUN
Sara	NPR	Sara	f NPR
in	PRE	in	f PRE
attacco	NOUN	attacco	f NOUN VER:fin
e	CON	e	f CON
Altea	NPR	<unknown>	s ADJ NOUN NPR
in	PRE	in	f PRE
difesa	NOUN	difesa	f ADJ NOUN VER:ppast
.	SENT	.	f SENT
Noi	PRO:pers	noi	f PRO:pers
eravamo	AUX:fin	essere	f AUX:fin VER:fin
convinte	VER:ppast	convincere	f ADJ VER:ppast
di	PRE	di	f PRE
vincere	VER:infi	vincere	f VER:infi
,	PUN	,	f PUN
ma	CON	ma	f CON
il	ART	il	f ART
Dome	NPR	<unknown>	s ADJ NOUN NPR
(	PUN	(	f PUN
il	ART	il	f ART
nostro	DET:poss	nostro	f DET:poss PRO:poss
allenatore	NOUN	allenatore	f NOUN
)	PUN	)	f PUN
diceva	VER:fin	dire	f VER:fin
di	PRE	di	f PRE
giocare	VER:infi	giocare	f VER:infi
allo	ARTPRE	al	f ARTPRE
stesso	ADJ	stesso	f ADJ NOUN
bene	NOUN	bene	f ADV NOUN
.	SENT	.	f SENT
Dopo	PRE	dopo	f ADJ ADV CON PRE
5	NUM	@card@	f NUM
min	VER:fin	<unknown>	s VER:fin
.	SENT	.	f SENT
Ascona	NPR	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
ci	CLI	ci	f CLI
fa	VER:fin	fare	f ADV VER2:fin VER:fin
un	ART	un	f ART
goal	NOUN	goal	f NOUN
:(	NOCAT	:(	f NOCAT
noi	PRO:pers	noi	f PRO:pers
pensavamo	VER:fin	pensare	f VER:fin
che	CHE	che	f CHE CON DET:wh
avremmo	AUX:fin	avere	f AUX:fin VER:fin
subito	ADV	subito	f ADJ ADV VER:ppast
recuperato	VER:ppast	recuperare	f VER:ppast
ma	CON	ma	f CON
non	NEG	non	f NEG
è	AUX:fin	essere	f AUX:fin VER:fin
stato	VER:ppast	essere|stare	f AUX:ppast NOUN VER:ppast
così	ADV	così	f ADV
e	CON	e	f CON
quindi	ADV	quindi	f ADV
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
perso	VER:ppast	perdere	f ADJ VER:ppast
.	SENT	.	f SENT
Questa	DET:demo	questo	f DET:demo PRO:demo
storia	NOUN	storia	f NOUN
insenia	ADJ	<unknown>	s ADJ NOUN VER:fin
che	CHE	che	f CHE CON DET:wh
si	CLI	si	f CLI
deve	VER2:fin	dovere	f VER2:fin VER:fin
sempre	ADV	sempre	f ADV
ascoltare	VER:infi	ascoltare	f VER:infi
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
allenatore	NOUN	allenatore	f NOUN
.	SENT	.	f SENT

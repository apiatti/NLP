Due	DET:num	due	f DET:num PRO:num
anni	NOUN	anno	f NOUN
fa	ADV	fa	f ADV VER2:fin VER:fin
ero	VER:fin	essere	f AUX:fin AUX:geru AUX:ppre VER:fin
tanto	ADV	tanto	f ADJ ADV DET:indef
amica	ADJ	amico	f ADJ NOUN VER:fin
di	PRE	di	f PRE
una	ART	una	f ART PRO:indef
mia	DET:poss	mio	f DET:poss PRO:poss
compagnia	NOUN	compagnia	f NOUN
di	PRE	di	f PRE
classe	NOUN	classe	f NOUN
,	PUN	,	f PUN
di	PRE	di	f PRE
nome	NOUN	nome	f NOUN
Valentina	NPR	Valentina	f NPR
.	SENT	.	f SENT
Io	PRO:pers	io	f PRO:pers
e	CON	e	f CON
Valentina	NPR	Valentina	f NPR
eravano	VER:fin	<unknown>	s VER:fin
molto	ADV	molto	f ADJ ADV DET:indef
amiche	ADJ	amico	f ADJ NOUN
,	PUN	,	f PUN
io	PRO:pers	io	f PRO:pers
invitavo	VER:fin	invitare	f VER:fin
lei	PRO:pers	lei	f PRO:pers
a	PRE	a	f PRE
pranzo	NOUN	pranzo	f NOUN VER:fin
e	CON	e	f CON
lei	PRO:pers	lei	f PRO:pers
invitava	VER:fin	invitare	f VER:fin
mè	NOUN	<unknown>	s NOUN VER:fin
.	SENT	.	f SENT
Un	ART	un	f ART
giorno	NOUN	giorno	f NOUN
mentre	CON	mentre	f CON
ero	VER:fin	essere	f AUX:fin AUX:geru AUX:ppre VER:fin
a	PRE	a	f PRE
scuola	NOUN	scuola	f NOUN
uscimmo	VER:fin	uscire	f VER:fin
a	PRE	a	f PRE
ricreazione	NOUN	ricreazione	f NOUN
,	PUN	,	f PUN
poi	ADV	poi	f ADV
ci	CLI	ci	f CLI
eravamo	AUX:fin	essere	f AUX:fin VER:fin
messe	VER:ppast	mettere	f ADJ NOUN VER:ppast
a	PRE	a	f PRE
litigare	VER:infi	litigare	f VER:infi
per	PRE	per	f PRE
un	ART	un	f ART
gioco	NOUN	gioco	f NOUN VER:fin
che	CHE	che	f CHE CON DET:wh
non	NEG	non	f NEG
gli	CLI	gli	f ART CLI
andava	VER:fin	andare	f VER:fin
bene	ADV	bene	f ADV NOUN
.	SENT	.	f SENT
Il	ART	il	f ART
giorno	NOUN	giorno	f NOUN
dopo	PRE	dopo	f ADJ ADV CON PRE
,	PUN	,	f PUN
io	PRO:pers	io	f PRO:pers
dalla	ARTPRE	dalla	f ARTPRE
rabbia	NOUN	rabbia	f NOUN
che	CHE	che	f CHE CON DET:wh
avevamo	AUX:fin	avere	f AUX:fin VER:fin
litigato	VER:ppast	litigare	f VER:ppast
,	PUN	,	f PUN
io	PRO:pers	io	f PRO:pers
presi	VER:ppast	prendere	f ADJ VER:fin VER:ppast
un	ART	un	f ART
foglio	NOUN	foglio	f NOUN VER:fin
.	SENT	.	f SENT
Con	PRE	con	f PRE
quel	DET:demo	quello	f DET:demo
fogli	NOUN	foglio	f NOUN VER:fin
lí	ADJ	<unknown>	s ADJ NOCAT VER:fin
,	PUN	,	f PUN
avevo	AUX:fin	avere	f AUX:fin VER:fin
chiesto	VER:ppast	chiedere	f ADJ VER:ppast
a	PRE	a	f PRE
tutti	DET:indef	tutto	f ADJ DET:indef PRO:indef
i	ART	il	f ART
miei	DET:poss	mio	f DET:poss PRO:poss
compagni	NOUN	compagno	f NOUN
,	PUN	,	f PUN
chi	WH	chi	f WH
odiava	VER:fin	odiare	f VER:fin
la	ART	la	f ART CLI
Valentina	NPR	Valentina	f NPR
doveva	VER2:fin	dovere	f VER2:fin VER:fin
firmare	VER:infi	firmare	f VER:infi
.	SENT	.	f SENT
Lei	PRO:pers	lei	f PRO:pers
vide	VER:fin	vedere	f VER:fin
il	ART	il	f ART
foglietto	NOUN	foglietto	f NOUN
,	PUN	,	f PUN
e	CON	e	f CON
andò	VER:fin	andare	f VER:fin
a	PRE	a	f PRE
dirlo	VER:infi:cli	dire	f VER:infi:cli
alla	ARTPRE	alla	f ARTPRE
maestra	NOUN	maestra	f ADJ NOUN
Mariella	NPR	<unknown>	s NOUN NPR VER:fin
.	SENT	.	f SENT
Mariella	NPR	<unknown>	s ADJ NOUN NPR VER:fin
mi	CLI	mi	f CLI
sgridò	VER:fin	sgridare	f VER:fin
,	PUN	,	f PUN
e	CON	e	f CON
io	PRO:pers	io	f PRO:pers
chiesi	VER:fin	chiedere	f VER:fin
scusa	NOUN	scusa	f NOUN VER:fin
a	PRE	a	f PRE
Valentina	NPR	Valentina	f NPR
,	PUN	,	f PUN
ma	CON	ma	f CON
lei	PRO:pers	lei	f PRO:pers
da	PRE	da	f PRE
quel	DET:demo	quello	f DET:demo
giorno	NOUN	giorno	f NOUN
non	NEG	non	f NEG
mi	CLI	mi	f CLI
rispondeva	VER:fin	rispondere	f VER:fin
più	ADV	più	f ADV
alle	ARTPRE	alla	f ARTPRE
chiamate	NOUN	chiamata	f NOUN VER:fin VER:ppast
che	CHE	che	f CHE CON DET:wh
facevo	VER:fin	fare	f VER2:fin VER:fin
.	SENT	.	f SENT
Io	PRO:pers	io	f PRO:pers
da	PRE	da	f PRE
quel	DET:demo	quello	f DET:demo
giorno	NOUN	giorno	f NOUN
mi	CLI	mi	f CLI
sono	AUX:fin	essere	f AUX:fin VER:fin
pentita	VER:ppast	pentire	f ADJ NOUN VER:ppast
di	PRE	di	f PRE
quello	PRO:demo	quello	f DET:demo PRO:demo
che	CHE	che	f CHE CON DET:wh
avevo	AUX:fin	avere	f AUX:fin VER:fin
fatto	VER:ppast	fare	f NOUN VER2:ppast VER:ppast
.	SENT	.	f SENT
La	ART	la	f ART CLI
morale	NOUN	morale	f ADJ NOUN
di	PRE	di	f PRE
questa	DET:demo	questo	f DET:demo PRO:demo
storia	NOUN	storia	f NOUN
è	VER:fin	essere	f AUX:fin VER:fin
se	CON	se	f CLI CON
hai	VER:fin	avere	f AUX:fin VER:fin
un	ART	un	f ART
amico	NOUN	amico	f ADJ NOUN VER:fin
o	CON	o	f CON
una	ART	una	f ART PRO:indef
amica	NOUN	amica	f ADJ NOUN VER:fin
,	PUN	,	f PUN
anche	ADV	anche	f ADV
se	CLI	se	f CLI CON
litighi	VER:fin	litigare	f VER:fin
con	PRE	con	f PRE
lei	PRO:pers	lei	f PRO:pers
,	PUN	,	f PUN
non	NEG	non	f NEG
vendicatevi	VER:fin:cli	<unknown>	s ADJ NOUN VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
mai	ADV	mai	f ADV
,	PUN	,	f PUN
perché	WH	perché	f WH
puoi	VER2:fin	potere	f VER2:fin VER:fin
perdere	VER:infi	perdere	f VER:infi
il	ART	il	f ART
tuo	DET:poss	tuo	f ADJ DET:poss PRO:poss
amico	NOUN	amico	f ADJ NOUN VER:fin
.	SENT	.	f SENT

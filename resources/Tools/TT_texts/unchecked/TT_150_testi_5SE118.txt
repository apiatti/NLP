Un	ART	un	f ART
giorno	NOUN	giorno	f NOUN
,	PUN	,	f PUN
in	PRE	in	f PRE
estate	NOUN	estate	f NOUN
,	PUN	,	f PUN
eravamo	AUX:fin	essere	f AUX:fin VER:fin
andati	VER:ppast	andare	f ADJ VER:ppast
io	PRO:pers	io	f PRO:pers
,	PUN	,	f PUN
mio	DET:poss	mio	f DET:poss PRO:poss
fratello	NOUN	fratello	f NOUN
e	CON	e	f CON
mio	DET:poss	mio	f DET:poss PRO:poss
nonno	NOUN	nonno	f NOUN
in	PRE	in	f PRE
campagna	NOUN	campagna	f NOUN
avevamo	AUX:fin	avere	f AUX:fin VER:fin
coltivato	VER:ppast	coltivare	f ADJ VER:ppast
tante	DET:indef	tanto	f ADJ DET:indef PRO:indef
cose	NOUN	cosa	f NOUN
,	PUN	,	f PUN
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
era	VER:fin	essere	f AUX:fin NOUN VER:fin
anche	ADV	anche	f ADV
un	ART	un	f ART
po	NOUN	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
’	NOCAT	<unknown>	s NOCAT
di	PRE	di	f PRE
terra	NOUN	terra	f NOUN
,	PUN	,	f PUN
rami	NOUN	ramo	f NOUN VER:fin
,	PUN	,	f PUN
foglie	NOUN	foglia	f NOUN
ecc	ADJ	<unknown>	s ADJ NOUN NPR VER:fin
.	SENT	.	f SENT
lí	ADJ	<unknown>	s ADJ NOCAT VER:fin
avevamo	AUX:fin	avere	f AUX:fin VER:fin
messo	VER:ppast	mettere	f ADJ VER:ppast
una	ART	una	f ART PRO:indef
piscina	NOUN	piscina	f NOUN
e	CON	e	f CON
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
fatto	VER:ppast	fare	f NOUN VER2:ppast VER:ppast
il	ART	il	f ART
bagno	NOUN	bagno	f NOUN VER:fin
,	PUN	,	f PUN
ci	CLI	ci	f CLI
divertivamo	VER:fin	divertire	f VER:fin
come	WH	come	f WH
matti	NOUN	matto	f ADJ NOUN
.	SENT	.	f SENT
Mio	DET:poss	mio	f DET:poss PRO:poss
nonno	NOUN	nonno	f NOUN
mì	NOUN	<unknown>	s ADJ NOUN VER:fin
disse	VER:fin	dire	f VER:fin
di	PRE	di	f PRE
uscire	VER:infi	uscire	f VER:infi
dalla	ARTPRE	dalla	f ARTPRE
piscina	NOUN	piscina	f NOUN
io	PRO:pers	io	f PRO:pers
non	NEG	non	f NEG
sono	AUX:fin	essere	f AUX:fin VER:fin
uscita	VER:ppast	uscire	f NOUN VER:ppast
perché	WH	perché	f WH
volevo	VER2:fin	volere	f VER2:fin VER:fin
restare	VER:infi	restare	f VER:infi
ancora	ADV	ancora	f ADV VER:fin
un	ART	un	f ART
po	NOUN	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
’	NOCAT	<unknown>	s NOCAT
,	PUN	,	f PUN
non	NEG	non	f NEG
per	PRE	per	f PRE
molto	ADV	molto	f ADJ ADV DET:indef
,	PUN	,	f PUN
alla	ARTPRE	alla	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
uscii	VER:fin	uscire	f VER:fin
anche	ADV	anche	f ADV
ìo	NOUN	<unknown>	s NOUN
,	PUN	,	f PUN
ho	AUX:fin	avere	f AUX:fin VER:fin
gia	VER:fin	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
detto	VER:ppast	dire	f VER:fin VER:ppast
all’	ADJ	<unknown>	s ADJ NOCAT VER:fin
inizio	NOUN	inizio	f NOUN VER:fin
che	CHE	che	f CHE CON DET:wh
vicino	ADJ	vicino	f ADJ NOUN
alla	ARTPRE	alla	f ARTPRE
piscina	NOUN	piscina	f NOUN
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
erano	VER:fin	essere	f AUX:fin VER:fin
rami	NOUN	ramo	f NOUN VER:fin
,	PUN	,	f PUN
foglie	NOUN	foglia	f NOUN
ecc	ADJ	<unknown>	s ADJ NOUN NPR VER:fin
.	SENT	.	f SENT
Io	PRO:pers	io	f PRO:pers
non	NEG	non	f NEG
misi	VER:fin	mettere	f VER:fin
le	ART	la	f ART CLI
ciabatte	NOUN	ciabatta	f NOUN
quindi	ADV	quindi	f ADV
andai	VER:fin	andare	f VER:fin
a	PRE	a	f PRE
piedi	NOUN	piede	f NOUN
nudi	ADJ	nudo	f ADJ NOUN
sull	NOUN	<unknown>	s ADJ NOUN NPR VER:fin
’	NOCAT	<unknown>	s NOCAT
asciugamano	NOUN	asciugamano	f NOUN
,	PUN	,	f PUN
stavo	VER:fin	stare	f VER:fin
camminando	VER:geru	camminare	f VER:geru
e	CON	e	f CON
mi	CLI	mi	f CLI
sentii	VER:fin	sentire	f VER:fin
un	ART	un	f ART
chiodo	NOUN	chiodo	f NOUN VER:fin
arrugginito	VER:ppast	arrugginire	f VER:ppast
,	PUN	,	f PUN
lungo	ADJ	lungo	f ADJ PRE
,	PUN	,	f PUN
sporco	VER:fin	sporcare	f ADJ NOUN VER:fin
dentro	ADV	dentro	f ADV PRE
al	ARTPRE	al	f ARTPRE
piede	NOUN	piede	f NOUN
,	PUN	,	f PUN
alla	ARTPRE	alla	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
mio	DET:poss	mio	f DET:poss PRO:poss
nonno	NOUN	nonno	f NOUN
melo	NOUN	melo	f NOUN
tolse	VER:fin	togliere	f VER:fin
.	SENT	.	f SENT
Mi	CLI	mi	f CLI
fece	VER:fin	fare	f NOUN VER2:fin VER:fin
una	ART	una	f ART PRO:indef
grande	ADJ	grande	f ADJ NOUN
infezzione	NOUN	<unknown>	s NOUN
ma	CON	ma	f CON
alla	ARTPRE	alla	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
andò	VER:fin	andare	f VER:fin
tutto	PRO:indef	tutto	f ADJ DET:indef PRO:indef
bene	ADV	bene	f ADV NOUN
,	PUN	,	f PUN
tutti	PRO:indef	tutti	f ADJ DET:indef PRO:indef
si	CLI	si	f CLI
erano	AUX:fin	essere	f AUX:fin VER:fin
spaventati	VER:ppast	spaventare	f ADJ VER:ppast
compresa	ADJ	compreso	f ADJ VER:ppast
io	PRO:pers	io	f PRO:pers
anche	ADV	anche	f ADV
mia	DET:poss	mio	f DET:poss PRO:poss
mamma	NOUN	mamma	f NOUN
si	CLI	si	f CLI
era	AUX:fin	essere	f AUX:fin NOUN VER:fin
spaventata	VER:ppast	spaventare	f ADJ VER:ppast
.	SENT	.	f SENT
Io	PRO:pers	io	f PRO:pers
da	PRE	da	f PRE
questa	DET:demo	questo	f DET:demo PRO:demo
esperienza	NOUN	esperienza	f NOUN
ho	AUX:fin	avere	f AUX:fin VER:fin
capito	VER:ppast	capire	f VER:fin VER:ppast
che	CHE	che	f CHE CON DET:wh
anche	ADV	anche	f ADV
se	CON	se	f CLI CON
le	ART	la	f ART CLI
ciabatte	NOUN	ciabatta	f NOUN
sono	VER:fin	essere	f AUX:fin VER:fin
nuove	ADJ	nuovo	f ADJ
o	CON	o	f CON
vecchie	ADJ	vecchio	f ADJ NOUN
devo	VER2:fin	dovere	f VER2:fin VER:fin
sempre	ADV	sempre	f ADV
metterle	VER:infi:cli	mettere	f VER:infi:cli
perché	WH	perché	f WH
senó	ADJ	<unknown>	s ADJ NOCAT VER:fin
mi	CLI	mi	f CLI
succede	VER:fin	succedere	f VER:fin
questo	PRO:demo	questo	f DET:demo PRO:demo
.	SENT	.	f SENT

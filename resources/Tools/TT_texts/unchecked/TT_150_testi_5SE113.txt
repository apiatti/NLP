LA	ART	la	f ART CLI
GARA	NPR	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
DI	NPR	<unknown>	s ADJ NOCAT NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
ARRAMPICATA	NPR	<unknown>	s ADJ NOUN NPR VER:ppast
Quando	NPR	<unknown>	s NPR VER:geru
ero	VER:fin	essere	f AUX:fin AUX:geru AUX:ppre VER:fin
più	ADV	più	f ADV
piccola	ADJ	piccolo	f ADJ
al	ARTPRE	al	f ARTPRE
corso	NOUN	corso	f ADJ NOUN VER:ppast
di	PRE	di	f PRE
arrampicata	NOUN	arrampicata	f NOUN VER:ppast
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
erano	AUX:fin	essere	f AUX:fin VER:fin
Molti	VER:ppast	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
bambini	NOUN	bambino	f NOUN
che	CHE	che	f CHE CON DET:wh
erano	VER:fin	essere	f AUX:fin VER:fin
bravi	ADJ	bravo	f ADJ
e	CON	e	f CON
anche	ADV	anche	f ADV
io	PRO:pers	io	f PRO:pers
ero	AUX:ppre	essere	f AUX:fin AUX:geru AUX:ppre VER:fin
tra	PRE	tra	f CON PRE
quelli	PRO:demo	quelli	f DET:demo PRO:demo
bravi	ADJ	bravo	f ADJ
.	SENT	.	f SENT
Ma	CON	ma	f CON
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
era	VER:fin	essere	f AUX:fin NOUN VER:fin
uno	PRO:indef	uno	f ART NOUN PRO:indef
,	PUN	,	f PUN
di	PRE	di	f PRE
nome	NOUN	nome	f NOUN
Michele	NPR	Michele	f NPR
,	PUN	,	f PUN
che	CHE	che	f CHE CON DET:wh
era	AUX:fin	essere	f AUX:fin NOUN VER:fin
anche	ADV	anche	f ADV
dotato	VER:ppast	dotare	f ADJ VER:ppast
solo	ADV	solo	f ADJ ADV VER:fin
che	CHE	che	f CHE CON DET:wh
era	VER:fin	essere	f AUX:fin NOUN VER:fin
molto	ADV	molto	f ADJ ADV DET:indef
vanitoso	ADJ	vanitoso	f ADJ
.	SENT	.	f SENT
Durante	PRE	durante	f PRE VER:ppre
gli	ART	il	f ART CLI
allenamenti	NOUN	allenamento	f NOUN
preferiva	VER:fin	preferire	f VER:fin
andare	VER:infi	andare	f VER:infi
in	PRE	in	f PRE
giro	NOUN	giro	f NOUN VER:fin
a	PRE	a	f PRE
criticare	VER:infi	criticare	f VER:infi
gli	ART	il	f ART CLI
altri	PRO:indef	altri	f ADJ DET:indef PRO:indef
,	PUN	,	f PUN
ad	PRE	ad	f PRE
esempio	NOUN	esempio	f NOUN
:	PUN	:	f PUN
–	NOCAT	<unknown>	s NOCAT
Tu	NPR	<unknown>	s NPR
non	NEG	non	f NEG
devi	VER2:fin	dovere	f VER2:fin VER:fin
tenere	VER:infi	tenere	f ADJ NOUN VER:infi
questa	DET:demo	questo	f DET:demo PRO:demo
presa	NOUN	presa	f ADJ NOUN VER:ppast
così	ADV	così	f ADV
ma	CON	ma	f CON
così	ADV	così	f ADV
,	PUN	,	f PUN
io	PRO:pers	io	f PRO:pers
lo	CLI	lo	f ART CLI
so	VER:fin	sapere	f VER:fin
perche	WH	perché	f WH
sono	VER:fin	essere	f AUX:fin VER:fin
molto	ADV	molto	f ADJ ADV DET:indef
più	ADV	più	f ADV
abile	ADJ	abile	f ADJ
e	CON	e	f CON
capace	ADJ	capace	f ADJ
di	PRE	di	f PRE
te	PRO:pers	te	f CLI PRO:pers
!	SENT	!	f SENT
–	NOCAT	<unknown>	s NOCAT
e	CON	e	f CON
cosí	ADJ	<unknown>	s ADJ NOCAT VER:fin
passavano	VER:fin	passare	f VER:fin
le	ART	la	f ART CLI
lezioni	NOUN	lezione	f NOUN
e	CON	e	f CON
lui	PRO:pers	lui	f PRO:pers
non	NEG	non	f NEG
faceva	VER:fin	fare	f VER2:fin VER:fin
il	ART	il	f ART
minimo	ADJ	minimo	f ADJ NOUN
sforzo	NOUN	sforzo	f NOUN VER:fin
per	PRE	per	f PRE
migliorare	VER:infi	migliorare	f VER:infi
.	SENT	.	f SENT
alla	ARTPRE	alla	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
dell’	ADJ	<unknown>	s ADJ NOCAT VER:fin
anno	NOUN	anno	f NOUN
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
era	VER:fin	essere	f AUX:fin NOUN VER:fin
sempre	ADV	sempre	f ADV
una	ART	una	f ART PRO:indef
grande	ADJ	grande	f ADJ NOUN
gara	NOUN	gara	f NOUN
molto	ADV	molto	f ADJ ADV DET:indef
attesa	NOUN	attesa	f ADJ NOUN VER:ppast
da	PRE	da	f PRE
tutti	PRO:indef	tutti	f ADJ DET:indef PRO:indef
;	PUN	;	f PUN
in	PRE	in	f PRE
quella	DET:demo	quello	f DET:demo PRO:demo
gara	NOUN	gara	f NOUN
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
era	VER:fin	essere	f AUX:fin NOUN VER:fin
la	ART	la	f ART CLI
sfida	NOUN	sfida	f NOUN VER:fin
delle	ARTPRE	della	f ARTPRE
fessioni	NOUN	<unknown>	s ADJ NOUN VER:fin
,	PUN	,	f PUN
la	ART	la	f ART CLI
gara	NOUN	gara	f NOUN
di	PRE	di	f PRE
velocità	NOUN	velocità	f NOUN
,	PUN	,	f PUN
le	ART	la	f ART CLI
staffette	NOUN	staffetta	f NOUN
,	PUN	,	f PUN
insomma	ADV	insomma	f ADV
tutti	DET:indef	tutto	f ADJ DET:indef PRO:indef
i	ART	il	f ART
tipi	NOUN	tipo	f ADJ NOUN
di	PRE	di	f PRE
gare	NOUN	gara	f NOUN
possibili	ADJ	possibile	f ADJ
,	PUN	,	f PUN
nel	ARTPRE	nel	f ARTPRE
arrampicata	NOUN	arrampicata	f NOUN VER:ppast
.	SENT	.	f SENT
Quando	WH	quando	f WH
giunse	VER:fin	giungere	f VER:fin
quel	DET:demo	quello	f DET:demo
giorno	NOUN	giorno	f NOUN
tutti	PRO:indef	tutti	f ADJ DET:indef PRO:indef
erano	AUX:fin	essere	f AUX:fin VER:fin
molto	ADV	molto	f ADJ ADV DET:indef
emozionati	VER:ppast	emozionare	f VER:ppast
e	CON	e	f CON
agitati	VER:ppast	agitare	f ADJ VER:ppast
a	PRE	a	f PRE
parte	NOUN	parte	f NOUN VER:fin
Michele	NPR	Michele	f NPR
,	PUN	,	f PUN
lui	PRO:pers	lui	f PRO:pers
era	VER:fin	essere	f AUX:fin NOUN VER:fin
sicuro	ADV	sicuro	f ADJ ADV
di	PRE	di	f PRE
vincere	VER:infi	vincere	f VER:infi
la	ART	la	f ART CLI
medaglia	NOUN	medaglia	f NOUN
d’	ADJ	<unknown>	s ADJ NOCAT VER:fin
oro	NOUN	oro	f NOUN VER:fin
!	SENT	!	f SENT
Ma	CON	ma	f CON
non	NEG	non	f NEG
era	AUX:fin	essere	f AUX:fin NOUN VER:fin
allenato	VER:ppast	allenare	f VER:ppast
e	CON	e	f CON
così	ADV	così	f ADV
andò	VER:fin	andare	f VER:fin
a	PRE	a	f PRE
finire	VER:infi	finire	f NOUN VER:infi
che	CHE	che	f CHE CON DET:wh
lui	PRO:pers	lui	f PRO:pers
arrivò	VER:fin	arrivare	f VER:fin
all’	ADJ	<unknown>	s ADJ NOCAT VER:fin
ultimo	ADJ	ultimo	f ADJ NOUN VER:fin
posto	NOUN	posto	f ADJ NOUN VER:fin VER:ppast
.	SENT	.	f SENT
Michele	NPR	Michele	f NPR
si	CLI	si	f CLI
vergognava	VER:fin	vergognare	f VER:fin
parecchio	ADV	parecchio	f ADJ ADV DET:indef
.	SENT	.	f SENT
Tutti	PRO:indef	tutti	f ADJ DET:indef PRO:indef
lo	CLI	lo	f ART CLI
prendevano	VER:fin	prendere	f VER:fin
in	PRE	in	f PRE
giro	NOUN	giro	f NOUN VER:fin
e	CON	e	f CON
lo	CLI	lo	f ART CLI
scherzarono	VER:fin	scherzare	f VER:fin
.	SENT	.	f SENT
Così	ADV	così	f ADV
lui	PRO:pers	lui	f PRO:pers
non	NEG	non	f NEG
si	CLI	si	f CLI
fece	VER:fin	fare	f NOUN VER2:fin VER:fin
più	ADV	più	f ADV
vedere	VER:infi	vedere	f VER:infi
in	PRE	in	f PRE
quel	DET:demo	quello	f DET:demo
corso	NOUN	corso	f ADJ NOUN VER:ppast
.	SENT	.	f SENT
Questa	DET:demo	questo	f DET:demo PRO:demo
scena	NOUN	scena	f NOUN
mi	CLI	mi	f CLI
ha	AUX:fin	avere	f AUX:fin VER:fin
insegnato	VER:ppast	insegnare	f VER:ppast
ad	PRE	ad	f PRE
allenarmi	VER:infi:cli	allenare	f VER:infi:cli
sempre	ADV	sempre	f ADV
e	CON	e	f CON
a	PRE	a	f PRE
non	NEG	non	f NEG
parlare	VER:infi	parlare	f VER:infi
o	CON	o	f CON
vantarmi	VER:infi:cli	vantare	f VER:infi:cli
.	SENT	.	f SENT

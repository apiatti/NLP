Il	ART	il	f ART
più	ADV	più	f ADV
forte	ADJ	forte	f ADJ NOUN
e	CON	e	f CON
il	ART	il	f ART
più	ADV	più	f ADV
debole	ADJ	debole	f ADJ NOUN
Un	VER:fin	<unknown>	s NPR VER:fin
giorno	NOUN	giorno	f NOUN
ero	VER:fin	essere	f AUX:fin AUX:geru AUX:ppre VER:fin
ha	VER:fin	avere	f AUX:fin VER:fin
una	ART	una	f ART PRO:indef
partita	NOUN	partita	f ADJ NOUN VER:ppast
di	PRE	di	f PRE
hockey	NOUN	<unknown>	s ADJ NOUN NPR
e	CON	e	f CON
dopo	PRE	dopo	f ADJ ADV CON PRE
un	ART	un	f ART
attimo	NOUN	attimo	f NOUN
ero	VER:fin	essere	f AUX:fin AUX:geru AUX:ppre VER:fin
davanti	PRE	davanti	f ADJ ADV PRE
tutto	PRO:indef	tutto	f ADJ DET:indef PRO:indef
solo	ADV	solo	f ADJ ADV VER:fin
contro	PRE	contro	f ADV PRE
il	ART	il	f ART
portiere	NOUN	portiera|portiere	f NOUN
della	ARTPRE	della	f ARTPRE
squadra	NOUN	squadra	f NOUN VER:fin
aversaria	ADJ	<unknown>	s ADJ NOUN NPR VER:fin VER:infi:cli
era	VER:fin	essere	f AUX:fin NOUN VER:fin
il	ART	il	f ART
portiere	NOUN	portiera|portiere	f NOUN
più	ADV	più	f ADV
forte	ADJ	forte	f ADJ NOUN
del	ARTPRE	del	f ARTPRE
mondo	NOUN	mondo	f ADJ NOUN VER:fin
e	CON	e	f CON
sotto	PRE	sotto	f ADJ ADV PRE
voce	NOUN	voce	f NOUN
diceva	VER:fin	dire	f VER:fin
che	CHE	che	f CHE CON DET:wh
era	VER:fin	essere	f AUX:fin NOUN VER:fin
un	ART	un	f ART
gioco	NOUN	gioco	f NOUN VER:fin
da	PRE	da	f PRE
ragazzi	NOUN	ragazzo	f NOUN
parare	VER:infi	parare	f VER:infi
quel	DET:demo	quello	f DET:demo
tiro	NOUN	tiro	f NOUN VER:fin
.	SENT	.	f SENT
Dopo	CON	dopo	f ADJ ADV CON PRE
avanzai	VER:fin	avanzare	f VER:fin
velocemente	ADV:mente	velocemente	f ADV:mente
dopo	ADV	dopo	f ADJ ADV CON PRE
li	CLI	li	f CLI
feci	VER:fin	fare	f NOUN VER2:fin VER:fin
una	ART	una	f ART PRO:indef
finta	NOUN	finta	f ADJ NOUN VER:fin VER:ppast
e	CON	e	f CON
goal	NOUN	goal	f NOUN
!	SENT	!	f SENT
!	SENT	!	f SENT
!	SENT	!	f SENT
Il	ART	il	f ART
portiero	NOUN	<unknown>	s ADJ NOUN VER:fin
non	NEG	non	f NEG
credeva	VER:fin	credere	f VER:fin
ai	ARTPRE	al	f ARTPRE
suoi	DET:poss	suo	f DET:poss PRO:poss
occhi	NOUN	occhio	f NOUN
pero	NOUN	pero	f NOUN
dopo	CON	dopo	f ADJ ADV CON PRE
mi	CLI	mi	f CLI
disse	VER:fin	dire	f VER:fin
che	CHE	che	f CHE CON DET:wh
fui	VER:fin	essere	f AUX:fin VER:fin
il	ART	il	f ART
primo	NOUN	primo	f ADJ NOUN
a	PRE	a	f PRE
segnare	VER:infi	segnare	f VER:infi
contro	ADV	contro	f ADV PRE
di	PRE	di	f PRE
lui	PRO:pers	lui	f PRO:pers
.	SENT	.	f SENT
Io	PRO:pers	io	f PRO:pers
o	CON	o	f CON
imparato	VER:ppast	imparare	f VER:ppast
che	CHE	che	f CHE CON DET:wh
quando	WH	quando	f WH
non	NEG	non	f NEG
ai	ARTPRE	al	f ARTPRE
voglia	NOUN	voglia	f NOUN VER2:fin VER:fin
di	PRE	di	f PRE
fare	VER:infi	fare	f VER2:infi VER:infi
qualcosa	PRO:indef	qualcosa	f PRO:indef
e	CON	e	f CON
ti	CLI	ti	f CLI
vanti	VER:fin	vantare	f NOUN VER:fin
e	CON	e	f CON
non	NEG	non	f NEG
ci	CLI	ci	f CLI
metti	VER:fin	mettere	f VER:fin
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
impegno	NOUN	impegno	f NOUN VER:fin
sara	VER:fin	<unknown>	s ADJ NOUN VER:fin
quasi	ADV	quasi	f ADV
impossibile	ADJ	impossibile	f ADJ
parare	VER:infi	parare	f VER:infi
quel	DET:demo	quello	f DET:demo
tiro	NOUN	tiro	f NOUN VER:fin
anche	ADV	anche	f ADV
se	CLI	se	f CLI CON
è	VER:fin	essere	f AUX:fin VER:fin
il	ART	il	f ART
portiere	NOUN	portiera|portiere	f NOUN
più	ADV	più	f ADV
forte	ADJ	forte	f ADJ NOUN
del	ARTPRE	del	f ARTPRE
mondo	NOUN	mondo	f ADJ NOUN VER:fin
.	SENT	.	f SENT
Quindi	ADV	quindi	f ADV
chi	WH	chi	f WH
sembra	VER:fin	sembrare	f VER:fin
forte	ADJ	forte	f ADJ NOUN
e	CON	e	f CON
non	NEG	non	f NEG
a	PRE	a	f PRE
voglia	NOUN	voglia	f NOUN VER2:fin VER:fin
non	NEG	non	f NEG
fa	VER:fin	fare	f ADV VER2:fin VER:fin
niente	PRO:indef	niente	f ADJ ADV PRO:indef
,	PUN	,	f PUN
o	CON	o	f CON
imparato	VER:ppast	imparare	f VER:ppast
quindi	ADV	quindi	f ADV
una	ART	una	f ART PRO:indef
cosa	NOUN	cosa	f NOUN WH
il	ART	il	f ART
più	ADV	più	f ADV
forte	ADJ	forte	f ADJ NOUN
se	CON	se	f CLI CON
non	NEG	non	f NEG
si	CLI	si	f CLI
impegna	VER:fin	impegnare	f VER:fin
non	NEG	non	f NEG
vincera	VER:fin	<unknown>	s ADJ NOUN VER:fin
mai	ADV	mai	f ADV
.	SENT	.	f SENT

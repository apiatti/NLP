Lo	ART	il	f ART CLI
scivolo	NOUN	scivolo	f NOUN VER:fin
d’	ADJ	<unknown>	s ADJ NOCAT VER:fin
acqua	NOUN	acqua	f NOUN
Tanti	VER:ppre	<unknown>	s ADJ NOUN NPR VER:ppre
anni	NOUN	anno	f NOUN
fa	ADV	fa	f ADV VER2:fin VER:fin
sono	AUX:fin	essere	f AUX:fin VER:fin
andata	VER:ppast	andare	f ADJ VER:ppast
a	PRE	a	f PRE
un	ART	un	f ART
compleanno	NOUN	compleanno	f NOUN
di	PRE	di	f PRE
una	ART	una	f ART PRO:indef
mia	DET:poss	mio	f DET:poss PRO:poss
amica	NOUN	amica	f ADJ NOUN VER:fin
di	PRE	di	f PRE
classe	NOUN	classe	f NOUN
.	SENT	.	f SENT
Il	ART	il	f ART
compleanno	NOUN	compleanno	f NOUN
si	CLI	si	f CLI
faceva	VER:fin	fare	f VER2:fin VER:fin
vicino	ADJ	vicino	f ADJ NOUN
al	ARTPRE	al	f ARTPRE
lago	NOUN	lago	f NOUN
dove	WH	dove	f WH
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
era	VER:fin	essere	f AUX:fin NOUN VER:fin
uno	ART	un	f ART NOUN PRO:indef
scivolo	NOUN	scivolo	f NOUN VER:fin
d’	ADJ	<unknown>	s ADJ NOCAT VER:fin
acqua	NOUN	acqua	f NOUN
.	SENT	.	f SENT
Allora	ADV	allora	f ADV
prima	ADV	prima	f ADJ ADV
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
giocato	VER:ppast	giocare	f VER:ppast
,	PUN	,	f PUN
mangiato	VER:ppast	mangiare	f VER:ppast
poi	ADV	poi	f ADV
siamo	AUX:fin	essere	f AUX:fin VER:fin
andati	VER:ppast	andare	f ADJ VER:ppast
nel	ARTPRE	nel	f ARTPRE
lago	NOUN	lago	f NOUN
a	PRE	a	f PRE
fare	VER:infi	fare	f VER2:infi VER:infi
il	ART	il	f ART
bagno	NOUN	bagno	f NOUN VER:fin
.	SENT	.	f SENT
Siamo	AUX:fin	essere	f AUX:fin VER:fin
stati	VER:ppast	essere|stare	f AUX:ppast NOUN VER:ppast
tanto	DET:indef	tanto	f ADJ ADV DET:indef
tempo	NOUN	tempo	f NOUN
dentro	PRE	dentro	f ADV PRE
il	ART	il	f ART
lago	NOUN	lago	f NOUN
.	SENT	.	f SENT
Dopo	ADV	dopo	f ADJ ADV CON PRE
siamo	AUX:fin	essere	f AUX:fin VER:fin
uscite	VER:ppast	uscire	f NOUN VER:fin VER:ppast
dall’	ADJ	<unknown>	s ADJ NOCAT
acqua	NOUN	acqua	f NOUN
e	CON	e	f CON
la	ART	la	f ART CLI
mia	DET:poss	mio	f DET:poss PRO:poss
amica	NOUN	amica	f ADJ NOUN VER:fin
aveva	AUX:fin	avere	f AUX:fin VER:fin
visto	NOUN	visto	f NOUN VER:fin
lo	ART	il	f ART CLI
scivolo	NOUN	scivolo	f NOUN VER:fin
d’	ADJ	<unknown>	s ADJ NOCAT VER:fin
acqua	NOUN	acqua	f NOUN
Allora	NPR	<unknown>	s NPR VER:fin
decide	VER:fin	decidere	f VER:fin
di	PRE	di	f PRE
andarci	VER:infi:cli	andare	f VER:infi:cli
.	SENT	.	f SENT
siamo	AUX:fin	essere	f AUX:fin VER:fin
arrivati	VER:ppast	arrivare	f ADJ VER:ppast
allo	ARTPRE	al	f ARTPRE
scivolo	NOUN	scivolo	f NOUN VER:fin
e	CON	e	f CON
erano	VER:fin	essere	f AUX:fin VER:fin
tutti	PRO:indef	tutti	f ADJ DET:indef PRO:indef
cosi	ADV	così	f ADV
contenti	ADJ	contento	f ADJ NOUN VER:fin
allora	ADV	allora	f ADV
ci	CLI	ci	f CLI
sono	AUX:fin	essere	f AUX:fin VER:fin
andati	VER:ppast	andare	f ADJ VER:ppast
.	SENT	.	f SENT
Io	PRO:pers	io	f PRO:pers
non	NEG	non	f NEG
volevo	VER2:fin	volere	f VER2:fin VER:fin
andarci	VER:infi:cli	andare	f VER:infi:cli
perché	WH	perché	f WH
avevo	VER:fin	avere	f AUX:fin VER:fin
paura	NOUN	paura	f NOUN
però	ADV	però	f ADV
io	PRO:pers	io	f PRO:pers
mi	CLI	mi	f CLI
sentivo	VER:fin	sentire	f VER:fin
una	ART	una	f ART PRO:indef
frana	NOUN	frana	f NOUN VER:fin
.	SENT	.	f SENT
E	CON	e	f CON
la	ART	la	f ART CLI
mia	DET:poss	mio	f DET:poss PRO:poss
amica	NOUN	amica	f ADJ NOUN VER:fin
mi	CLI	mi	f CLI
chiedeva	VER:fin	chiedere	f VER:fin
sempre	ADV	sempre	f ADV
vieni	AUX:fin	venire	f AUX:fin VER:fin
vieni	VER:fin	venire	f AUX:fin VER:fin
.	SENT	.	f SENT
Guardo	VER:fin	guardare	f VER:fin
che	CHE	che	f CHE CON DET:wh
in	PRE	in	f PRE
due	DET:num	due	f DET:num PRO:num
si	CLI	si	f CLI
andava	VER:fin	andare	f VER:fin
piú	ADV	più	f ADV
veloce	ADJ	veloce	f ADJ
nello	ARTPRE	nel	f ARTPRE
scivolo	NOUN	scivolo	f NOUN VER:fin
.	SENT	.	f SENT
Cosi	ADV	così	f ADV
ho	AUX:fin	avere	f AUX:fin VER:fin
chiesto	VER:ppast	chiedere	f ADJ VER:ppast
alla	ARTPRE	alla	f ARTPRE
mia	DET:poss	mio	f DET:poss PRO:poss
amica	NOUN	amica	f ADJ NOUN VER:fin
se	CLI	se	f CLI CON
voleva	VER2:fin	volere	f VER2:fin VER:fin
andare	VER:infi	andare	f VER:infi
con	PRE	con	f PRE
me	PRO:pers	me	f CLI PRO:pers
allo	ARTPRE	al	f ARTPRE
scivolo	NOUN	scivolo	f NOUN VER:fin
perché	WH	perché	f WH
dasola	VER:fin	<unknown>	s NOUN VER:fin
avevo	VER:fin	avere	f AUX:fin VER:fin
paura	NOUN	paura	f NOUN
di	PRE	di	f PRE
andare	VER:infi	andare	f VER:infi
.	SENT	.	f SENT
Allora	ADV	allora	f ADV
siamo	AUX:fin	essere	f AUX:fin VER:fin
partite	VER:ppast	partire	f ADJ NOUN VER:fin VER:ppast
e	CON	e	f CON
guando	NOUN	<unknown>	s NOUN VER:geru
siamo	AUX:fin	essere	f AUX:fin VER:fin
arrivate	VER:ppast	arrivare	f ADJ VER:fin VER:ppast
giú	ADJ	<unknown>	s ADJ NOCAT VER:fin
io	PRO:pers	io	f PRO:pers
mi	CLI	mi	f CLI
sono	AUX:fin	essere	f AUX:fin VER:fin
fatta	VER:ppast	fare	f VER2:ppast VER:ppast
male	ADV	male	f ADJ ADV NOUN
.	SENT	.	f SENT
Da	PRE	da	f PRE
quel	DET:demo	quello	f DET:demo
momento	NOUN	momento	f NOUN
ho	AUX:fin	avere	f AUX:fin VER:fin
capito	VER:ppast	capire	f VER:fin VER:ppast
che	CHE	che	f CHE CON DET:wh
non	NEG	non	f NEG
devi	VER2:fin	dovere	f VER2:fin VER:fin
guardare	VER:infi	guardare	f VER:infi
quello	PRO:demo	quello	f DET:demo PRO:demo
che	CHE	che	f CHE CON DET:wh
fanno	VER:fin	fare	f VER2:fin VER:fin
gli	ART	il	f ART CLI
altri	PRO:indef	altri	f ADJ DET:indef PRO:indef
pensando	VER:geru	pensare	f VER:geru
che	CHE	che	f CHE CON DET:wh
loro	PRO:pers	loro	f DET:poss PRO:pers PRO:poss
sono	VER:fin	essere	f AUX:fin VER:fin
migliori	ADJ	migliore	f ADJ VER:fin
di	PRE	di	f PRE
te	PRO:pers	te	f CLI PRO:pers
ma	CON	ma	f CON
ognuno	PRO:indef	ognuno	f PRO:indef
deve	VER2:fin	dovere	f VER2:fin VER:fin
fare	VER:infi	fare	f VER2:infi VER:infi
cio	VER:fin	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
che	CHE	che	f CHE CON DET:wh
sente	VER:fin	sentire	f VER:fin
seguendo	VER:geru	seguire	f VER:geru
il	ART	il	f ART
proprio	DET:poss	proprio	f ADJ ADV DET:poss PRO:poss
cuore	NOUN	cuore	f NOUN
.	SENT	.	f SENT

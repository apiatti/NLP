Circa	ADV	circa	f ADV PRE
un	ART	un	f ART
mese	NOUN	mese	f NOUN
fa	VER2:fin	fare	f ADV VER2:fin VER:fin
ero	AUX:ppre	essere	f AUX:fin AUX:geru AUX:ppre VER:fin
con	PRE	con	f PRE
una	ART	una	f ART PRO:indef
mia	DET:poss	mio	f DET:poss PRO:poss
amica	NOUN	amica	f ADJ NOUN VER:fin
che	CHE	che	f CHE CON DET:wh
però	ADV	però	f ADV
non	NEG	non	f NEG
viene	VER:fin	venire	f AUX:fin VER:fin
a	PRE	a	f PRE
scuola	NOUN	scuola	f NOUN
qui	ADV	qui	f ADV
ad	PRE	ad	f PRE
Aascona	NPR	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
,	PUN	,	f PUN
eravamo	VER:fin	essere	f AUX:fin VER:fin
giu	ADJ	<unknown>	s ADJ NPR VER:fin
di	PRE	di	f PRE
sotto	ADV	sotto	f ADJ ADV PRE
dove	WH	dove	f WH
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
era	VER:fin	essere	f AUX:fin NOUN VER:fin
il	ART	il	f ART
tapisrouland	NPR	<unknown>	s ADJ NOUN NPR
lei	PRO:pers	lei	f PRO:pers
ci	CLI	ci	f CLI
era	AUX:fin	essere	f AUX:fin NOUN VER:fin
salita	VER:ppast	salire	f NOUN VER:ppast
,	PUN	,	f PUN
e	CON	e	f CON
poi	ADV	poi	f ADV
io	PRO:pers	io	f PRO:pers
ho	AUX:fin	avere	f AUX:fin VER:fin
detto	VER:ppast	dire	f VER:fin VER:ppast
:	PUN	:	f PUN
ma	CON	ma	f CON
sara	VER:fin	<unknown>	s ADJ NOUN VER:fin
facilissimo	ADJ	facile	f ADJ
,	PUN	,	f PUN
però	ADV	però	f ADV
non	NEG	non	f NEG
dovevo	VER2:fin	dovere	f VER2:fin VER:fin
giudicare	VER:infi	giudicare	f VER:infi
prima	ADV	prima	f ADJ ADV
di	PRE	di	f PRE
fare	VER:infi	fare	f VER2:infi VER:infi
.	SENT	.	f SENT
Quando	WH	quando	f WH
ci	CLI	ci	f CLI
sono	AUX:fin	essere	f AUX:fin VER:fin
salita	VER:ppast	salire	f NOUN VER:ppast
però	ADV	però	f ADV
,	PUN	,	f PUN
mi	CLI	mi	f CLI
sono	AUX:fin	essere	f AUX:fin VER:fin
accorta	VER:ppast	accorgere	f ADJ NOUN VER:ppast
che	CHE	che	f CHE CON DET:wh
non	NEG	non	f NEG
era	VER:fin	essere	f AUX:fin NOUN VER:fin
così	ADV	così	f ADV
facile	ADJ	facile	f ADJ
,	PUN	,	f PUN
e	CON	e	f CON
da	PRE	da	f PRE
lì	ADV	lì	f ADV
ho	AUX:fin	avere	f AUX:fin VER:fin
capito	VER:ppast	capire	f VER:fin VER:ppast
che	CHE	che	f CHE CON DET:wh
prima	ADV	prima	f ADJ ADV
si	CLI	si	f CLI
devono	VER2:fin	dovere	f VER2:fin VER:fin
provare	VER:infi	provare	f VER:infi
ha	VER:fin	avere	f AUX:fin VER:fin
fare	VER:infi	fare	f VER2:infi VER:infi
le	ART	la	f ART CLI
cose	NOUN	cosa	f NOUN
e	CON	e	f CON
poi	ADV	poi	f ADV
si	CLI	si	f CLI
giudica	VER:fin	giudicare	f VER:fin
.	SENT	.	f SENT
Quando	WH	quando	f WH
eravamo	VER:fin	essere	f AUX:fin VER:fin
a	PRE	a	f PRE
ginna	NOUN	<unknown>	s ADJ NOUN VER:fin
e	CON	e	f CON
abbiamo	AUX:fin	avere	f AUX:fin VER:fin
fatto	VER:ppast	fare	f NOUN VER2:ppast VER:ppast
la	ART	la	f ART CLI
corsa	NOUN	corsa	f ADJ NOUN VER:ppast
pensavo	VER:fin	pensare	f VER:fin
che	CHE	che	f CHE CON DET:wh
non	NEG	non	f NEG
sarei	AUX:fin	essere	f AUX:fin VER:fin
riuscita	VER:ppast	riuscire	f VER:ppast
ad	PRE	ad	f PRE
arrivare	VER:infi	arrivare	f VER:infi
prima	ADV	prima	f ADJ ADV
della	ARTPRE	della	f ARTPRE
Samira	NPR	<unknown>	s NPR VER:fin
quando	WH	quando	f WH
siamo	AUX:fin	essere	f AUX:fin VER:fin
partite	VER:ppast	partire	f ADJ NOUN VER:fin VER:ppast
io	PRO:pers	io	f PRO:pers
non	NEG	non	f NEG
ci	CLI	ci	f CLI
avevo	AUX:fin	avere	f AUX:fin VER:fin
messo	VER:ppast	mettere	f ADJ VER:ppast
molto	ADV	molto	f ADJ ADV DET:indef
impegno	NOUN	impegno	f NOUN VER:fin
,	PUN	,	f PUN
alla	ARTPRE	alla	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
siamo	AUX:fin	essere	f AUX:fin VER:fin
arrivate	VER:ppast	arrivare	f ADJ VER:fin VER:ppast
pari	ADJ	pario	f ADJ VER:fin
,	PUN	,	f PUN
però	ADV	però	f ADV
se	CON	se	f CLI CON
ci	CLI	ci	f CLI
avessi	AUX:fin	avere	f AUX:fin VER:fin
messo	VER:ppast	mettere	f ADJ VER:ppast
un	ART	un	f ART
po	NOUN	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
’	NOCAT	<unknown>	s NOCAT
più	ADV	più	f ADV
di	PRE	di	f PRE
impegno	NOUN	impegno	f NOUN VER:fin
forse	ADV	forse	f ADV
c’	ADJ	<unknown>	s ADJ NOCAT VER:fin
e	CON	e	f CON
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
avrei	AUX:fin	avere	f AUX:fin VER:fin
fatta	VER:ppast	fare	f VER2:ppast VER:ppast
.	SENT	.	f SENT
Quindi	ADV	quindi	f ADV
ho	AUX:fin	avere	f AUX:fin VER:fin
capito	VER:ppast	capire	f VER:fin VER:ppast
che	CHE	che	f CHE CON DET:wh
se	CLI	se	f CLI CON
ci	CLI	ci	f CLI
si	CLI	si	f CLI
mette	VER:fin	mettere	f VER:fin
un	ART	un	f ART
po	NOUN	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
’	NOCAT	<unknown>	s NOCAT
più	ADV	più	f ADV
impegno	NOUN	impegno	f NOUN VER:fin
si	CLI	si	f CLI
puo	VER:fin	<unknown>	s ADJ NOUN VER:fin
farcela	VER:infi:cli	fare	f VER2:infi:cli VER:infi:cli
.	SENT	.	f SENT

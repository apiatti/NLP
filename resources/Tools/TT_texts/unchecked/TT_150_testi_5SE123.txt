Un	ART	un	f ART
giorno	NOUN	giorno	f NOUN
(	PUN	(	f PUN
mercoledì	NOUN	mercoledì	f NOUN
)	PUN	)	f PUN
alle	ARTPRE	alla	f ARTPRE
4.15	NUM	@card@	f NUM
eravamo	VER:fin	essere	f AUX:fin VER:fin
all’	ADJ	<unknown>	s ADJ NOCAT VER:fin
allenamento	NOUN	allenamento	f NOUN
di	PRE	di	f PRE
basket	NOUN	basket	f NOUN
quando	WH	quando	f WH
e	CON	e	f CON
arrivata	VER:ppast	arrivare	f ADJ VER:ppast
la	ART	la	f ART CLI
signora	NOUN	signora	f NOUN
di	PRE	di	f PRE
STARTI	NPR	<unknown>	s NPR VER:infi:cli
(	PUN	(	f PUN
un	ART	un	f ART
gruppo	NOUN	gruppo	f NOUN
che	CHE	che	f CHE CON DET:wh
insegna	VER:fin	insegnare	f NOUN VER:fin
a	PRE	a	f PRE
giocare	VER:infi	giocare	f VER:infi
lealmente	ADV:mente	lealmente	f ADV:mente
,	PUN	,	f PUN
insieme	ADV	insieme	f ADV NOUN PRE
e	CON	e	f CON
uniti	ADJ	unito	f ADJ VER:ppast
)	PUN	)	f PUN
.	SENT	.	f SENT
Ci	CLI	ci	f CLI
siamo	AUX:fin	essere	f AUX:fin VER:fin
seduti	VER:ppast	sedere	f VER:ppast
in	PRE	in	f PRE
cerchio	NOUN	cerchio	f NOUN VER:fin
intorno	PRE	intorno	f ADV PRE
a	PRE	a	f PRE
lei	PRO:pers	lei	f PRO:pers
ma	CON	ma	f CON
nel	ARTPRE	nel	f ARTPRE
frattempo	NOUN	frattempo	f NOUN
aveva	AUX:fin	avere	f AUX:fin VER:fin
tirato	VER:ppast	tirare	f ADJ VER:ppast
fuori	ADV	fuori	f ADV PRE
dalla	ARTPRE	dalla	f ARTPRE
borsa	NOUN	borsa	f NOUN
un	ART	un	f ART
mazzo	NOUN	mazzo	f NOUN
di	PRE	di	f PRE
legnetti	VER:fin	<unknown>	s NOUN VER:fin
,	PUN	,	f PUN
tutti	PRO:indef	tutti	f ADJ DET:indef PRO:indef
ci	CLI	ci	f CLI
guardammo	VER:fin	guardare	f VER:fin
intorno	PRE	intorno	f ADV PRE
non	NEG	non	f NEG
sapevamo	VER:fin	sapere	f VER:fin
cosa	WH	cosa	f NOUN WH
fare	VER:infi	fare	f VER2:infi VER:infi
con	PRE	con	f PRE
un	ART	un	f ART
mazzo	NOUN	mazzo	f NOUN
di	PRE	di	f PRE
legnetti	VER:fin	<unknown>	s NOUN VER:fin
,	PUN	,	f PUN
lei	PRO:pers	lei	f PRO:pers
disse	VER:fin	dire	f VER:fin
:	PUN	:	f PUN
–	NOCAT	<unknown>	s NOCAT
Chi	NPR	<unknown>	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
vuole	VER2:fin	volere	f VER2:fin VER:fin
provare	VER:infi	provare	f VER:infi
a	PRE	a	f PRE
spezzare	VER:infi	spezzare	f VER:infi
i	ART	il	f ART
legnetti	VER:fin	<unknown>	s NOUN VER:fin
tutti	PRO:indef	tutti	f ADJ DET:indef PRO:indef
assieme	PRE	assieme	f ADV PRE
–	NOCAT	<unknown>	s NOCAT
.	SENT	.	f SENT
Tutti	DET:indef	tutto	f ADJ DET:indef PRO:indef
noi	PRO:pers	noi	f PRO:pers
alzammo	VER:fin	alzare	f VER:fin
la	ART	la	f ART CLI
mano	NOUN	mano	f NOUN
allora	ADV	allora	f ADV
ci	CLI	ci	f CLI
fece	VER2:fin	fare	f NOUN VER2:fin VER:fin
provare	VER:infi	provare	f VER:infi
a	PRE	a	f PRE
tutti	DET:indef	tutto	f ADJ DET:indef PRO:indef
ma	CON	ma	f CON
nessuno	PRO:indef	nessuno	f ADJ PRO:indef
di	PRE	di	f PRE
noi	PRO:pers	noi	f PRO:pers
era	AUX:fin	essere	f AUX:fin NOUN VER:fin
riuscito	VER:ppast	riuscire	f VER:ppast
a	PRE	a	f PRE
spezzare	VER:infi	spezzare	f VER:infi
quel	DET:demo	quello	f DET:demo
mazzo	NOUN	mazzo	f NOUN
di	PRE	di	f PRE
legnetti	NOUN	<unknown>	s NOUN VER:fin
.	SENT	.	f SENT
Allora	ADV	allora	f ADV
le	ART	la	f ART CLI
signora	NOUN	signora	f NOUN
di	PRE	di	f PRE
STARTI	VER:infi:cli	<unknown>	s NPR VER:infi:cli
disse	VER:fin	dire	f VER:fin
:	PUN	:	f PUN
–	NOCAT	<unknown>	s NOCAT
Ricordatevi	VER:fin:cli	ricordare	s NPR VER:fin:cli
questa	DET:demo	questo	f DET:demo PRO:demo
situazione	NOUN	situazione	f NOUN
,	PUN	,	f PUN
cioè	ADV	cioè	f ADV CON
che	CHE	che	f CHE CON DET:wh
nessuno	PRO:indef	nessuno	f ADJ PRO:indef
di	PRE	di	f PRE
voi	PRO:pers	voi	f NOUN PRO:pers
è	AUX:fin	essere	f AUX:fin VER:fin
riuscito	VER:ppast	riuscire	f VER:ppast
a	PRE	a	f PRE
spezzare	VER:infi	spezzare	f VER:infi
i	ART	il	f ART
legnetti	VER:fin	<unknown>	s NOUN VER:fin
tutti	PRO:indef	tutti	f ADJ DET:indef PRO:indef
assieme	PRE	assieme	f ADV PRE
–	NOCAT	<unknown>	s NOCAT
.	SENT	.	f SENT
Alla	ARTPRE	alla	f ARTPRE
fine	NOUN	fine	f ADJ NOUN
slego	VER:fin	slegare	f VER:fin
i	ART	il	f ART
legnetti	VER:fin	<unknown>	s NOUN VER:fin
e	CON	e	f CON
ce	CLI	ce	f CLI
ne	CLI	ne	f CLI
diede	VER:fin	dare	f VER:fin
uno	PRO:indef	uno	f ART NOUN PRO:indef
per	PRE	per	f PRE
uno	PRO:indef	uno	f ART NOUN PRO:indef
.	SENT	.	f SENT
La	ART	la	f ART CLI
signora	NOUN	signora	f NOUN
disse	VER:fin	dire	f VER:fin
:	PUN	:	f PUN
–	NOCAT	<unknown>	s NOCAT
Ora	VER:fin	orare	s ADJ NOUN NPR VER:fin VER:fin:cli VER:geru VER:geru:cli VER:infi VER:infi:cli VER:ppast VER:ppre
provate	ADJ	provato	f ADJ VER:fin VER:ppast
a	PRE	a	f PRE
spezzarli	VER:infi:cli	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
–	NOCAT	<unknown>	s NOCAT
,	PUN	,	f PUN
e	CON	e	f CON
,	PUN	,	f PUN
tutti	DET:indef	tutto	f ADJ DET:indef PRO:indef
noi	PRO:pers	noi	f PRO:pers
riuscimmo	VER:fin	riuscire	f VER:fin
a	PRE	a	f PRE
spezzarli	VER:infi:cli	<unknown>	s ADJ NOUN VER:fin VER:infi VER:infi:cli VER:ppast VER:ppre
con	PRE	con	f PRE
facilità	NOUN	facilità	f NOUN
.	SENT	.	f SENT
Questa	DET:demo	questo	f DET:demo PRO:demo
storia	NOUN	storia	f NOUN
mi	CLI	mi	f CLI
ha	AUX:fin	avere	f AUX:fin VER:fin
insegnato	VER:ppast	insegnare	f VER:ppast
che	CHE	che	f CHE CON DET:wh
dobbiamo	VER2:fin	dovere	f VER2:fin VER:fin
stare	VER:infi	stare	f ADV VER:infi
uniti	ADJ	unito	f ADJ VER:ppast
e	CON	e	f CON
non	NEG	non	f NEG
separarci	VER:infi:cli	separare	f VER:infi:cli
mai	ADV	mai	f ADV
(	PUN	(	f PUN
seno	NOUN	seno	f NOUN
facciamo	VER:fin	fare	f VER2:fin VER:fin
la	ART	la	f ART CLI
fine	NOUN	fine	f ADJ NOUN
dei	ARTPRE	del	f ARTPRE NOUN
legnetti	NOUN	<unknown>	s NOUN VER:fin
)	PUN	)	f PUN
.	SENT	.	f SENT
Morale	ADJ	morale	f ADJ NOUN
:	PUN	:	f PUN
l’	ADJ	<unknown>	s ADJ NOCAT VER:fin
unione	NOUN	unione	f NOUN
fa	VER:fin	fare	f ADV VER2:fin VER:fin
la	ART	la	f ART CLI
forza	NOUN	forza	f NOUN VER:fin
.	SENT	.	f SENT

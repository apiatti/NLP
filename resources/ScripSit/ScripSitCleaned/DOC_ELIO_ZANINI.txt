Con lo scopo di comprendere e migliorare una particolare tipologia di sensore d’umidità esistente, questo progetto copre svariati argomenti.
Per mezzo di simulazioni preliminari si è potuto capire il funzionamento del risonatore ed il principio alla base della misura d’umidità.
Tramite opportune manipolazioni dei dati è stato poi possibile comprendere le relazioni esistenti fra geometrie strutturali e caratteristiche frequenziali.
Le suddette nozioni hanno successivamente permesso il dimensionamento e la progettazione di un dispositivo migliorato.
Negli ultimi anni si è prestata particolare attenzione all’igiene e alla sanità in ambito alimentare, e questo particolare riguardo continuerà a crescere in futuro.
La tecnologia, e nello speciﬁco il campo della sensorica adibita all’automazione industriale, con l’aumento delle norme igieniche richieste si è evoluta a sua volta.
Bühler è attualmente leader mondiale negli impianti per lo stoccaggio e la trasformazione dei prodotti agroalimentari, ed in particolare nell’ambito dei cereali.
Il lavoro di diploma verte sulla comprensione ed ottimizzazione di un particolare sensore d’umidità attualmente presente sul mercato.
Il dispositivo è sviluppato dalla ditta Tews ed è stato costruito sulla base di un risonatore a cavità che purtroppo possiede alcuni limiti funzionali come per esempio il range d’umidità misurabile.
Il sensore rivisitato non dovrà incorrere in effetti non lineari e dovrà inoltre avere un range di misura esteso, che possa dunque rilevare umidità superiori al 20%.
Il progetto consiste inizialmente nello studio dello stato d’arte attuale, per poi procedere alla comprensione dei limiti reali del sensore.
Compiti • Studio dello stato d’arte dei sensori d’umidità in RF e comprensione dei modelli ﬁsici (curve caratteristiche di risonanza, sensibilità e frequenza)
Questa comunicazione ha permesso uno scambio d’informazioni e soprattutto d’opinioni.
Per quanto riguarda la divisione temporale dei compiti, è stato redatto il seguente diagramma Gantt, che illustra la procedura adottata durante il corso del progetto.
In questo capitolo mi sono concetrato sullo studio attuale della tecnologia, facendo riferimento ad alcuni di libri, alcune pubblicazioni ed alcuni brevetti ([3][4][5][6][7]), ho cercato di comprendere meglio il metodo impiegato da Bühler per la misura dell’umidità, in particolare quello basato su sensori progettati con un risonatore a microonde.
Naturalmente a livello industriale non vi è solo un metodo per misurare l’umidità di un certo materiale, nel caso di Bühler, si fa afﬁdamento ad una superﬁcie inclinata sulla quale scorre il prodotto da misurare.
Il funzionamento in riﬂessione comporta alcuni vantaggi, come la minor quantità di componenti, quindi un minor costo ed una minore manutenzione, ed inﬁne una maggiore risoluzione di misura.
Il campo verrà poi captato all’altro estremo del risonatore e a causa delle sue differenze con l’originale, sarà possibile estrapolare informazioni riguardanti il materiale sotto analisi.
Questo processo è spiegato con maggiore scrupolo nelle prossime pagine, al capitolo Analisi del sensore.
Normalmente se si lavora con campi elettromagnetici e si ha bisogno di informazioni riguardanti un certo materiale, ci sono due metodologie utilizzabili, il metodo diretto e quello indiretto.
Metodi di caratterizzazione Inizio chiarendo alcune sigle, fr ed r0 sono la frequenza di risonanza e la resistenza d’entrata normalizzata, mc e ρd corrispondono all’umidità (moisture content) e alla densità parziale asciutta.
Entrambe le metodologie comportano una misura che compensa la densità del materiale, caratteristica essenziale per Bühler in quanto il materiale biologico da controllare non sempre ha una densità costante.
Inoltre ogni qualvolta il sensore viene calibrato, è necessario rieseguirne la caratterizzazione.[3] 4.2.1.2 Caratterizzazione indiretta Anche questo metodo è empirico dunque richiede
una certa esperienza, ma questa volta per ottenere i fattori mc e ρd si deve passare dai parametri dielettrici, ovvero la costante dielettrica ε ed il fattore di perdita ε . Il vero vantaggio del metodo indiretto è il fatto che, ad una frequenza ﬁssa, l’empiricità del sistema è legata solo al materiale che si vuole testare, è dunque indipendente dal sensore, ciò signiﬁca che il materiale necessita una sola caratterizzazione.[3]
In questo capitolo sono racchiuse brevemente alcune nozioni teoriche che permettono di introdurre il lettore ai concetti di risonanza, onde elettromagnetiche, microonde, ed al principio di funzionamento del sensore TEWS . . .
Se ammettiamo di avere un risonatore ideale, questa particolare oscillazione ad una determinata frequenza (frequenza di risonanza) tenderà a divergere verso ∞, in altre parole il sistema riesce a trasferire energia senza perdite causando un aumento d’ampiezza di grosse proporzioni dell’oscillazione.
Un risonatore è dunque un dispositivo che offre le caratteristiche ottimali al ﬁne di ottenere una risonanza, queste caratteristiche sono spesso e volentieri dettate dalle dimensioni ﬁsiche dell’apparecchio e dai materiali con cui è stato costruito.
Data la grande quantità di teoria che giace dietro alle onde elettromagnetiche, inizio a spiegare (per semplicità) dalle equazioni di Maxwell[8].
si modiﬁcano, le due derivate parziali si annullano poichè non c’è variazione di tempo.
Equazioni di Maxwell - caso statico Possiamo dunque affermare che se siamo in una situazione statica, i campi magnetici e/o elettrici, possono esistere anche singolarmente, la presenza di uno dei due dunque non impone la presenza dell’altro. 5.3.1.3 Nel vuoto Il caso nel vuoto è probabilmente il più interessante a causa della
Se assumiamo di analizzare queste equazioni in uno spazio d’osservazione come il vuoto allora appare evidente che bisogna fare due piccole correzioni.
Equazioni di Maxwell - caso nel vuoto Essendo nel vuoto non ci possono essere materiali nei quali scorre una corrente I , come pure non ci possono essere cariche elettriche Q. A questo punto modiﬁchiamo leggermente le prime due equazioni per ottenere una forma che sarà più utile nel prossimo paragrafo.
Riprendendo le equazioni soprastanti, vediamo che entrambe sono in funzione di E ed H , possiamo dunque manipolarle per ottenere due equazioni (differenziali) espresse esclusivamente in una delle due variabili (con r corrispondente allo spazio percorso in 3D).
Questo tipo d’equazione soddisfa i requisiti per avere una soluzione che corrisponde ad un’onda, la cui forma è
Le microonde sono un tipo di onda elettromagnetica, che può trovarsi fra le frequenze
Per capire al meglio il funzionamento del sensore, ed in particolare del risonatore al suo interno, mi sono documentato a riguardo diversi tipi di risonatori utilizzati nel campo della misura d’umidità, cercando a grandi linee le loro caratteristiche principali.
Ho dunque deciso di concentrarmi inizialmente su questi due tipi di risonatore al ﬁne di capirne meglio il funzionamento e di avere dei modelli inseriti nel simulatore pronti ad essere utilizzati all’occorrenza.
Usando delle variabili al posto dei parametri nel modello, quando vi sarà la necessità potro sfruttare alcune funzionalità del software che mi permetteranno di variarne automaticamente le dimensioni, risparmiando tempo e ottenendo risultati già predisposti al confronto.
Essendo le prime simulazioni, non ho considerato l’idea di porre vincoli troppo severi al software, per tutte le simulazioni preliminari ho optato quindi per uno sweep in frequenza fra 2[GHz ] e 6[GHz ] forchetta nella quale, secondo la documentazione ed i brevetti, vi dovrebbero essere parecchie risonanze.
Su Ansys non potendo utilizzare la funzione Sweep con componenti composti, ho dovuto modellarlo tramite alcuni dischi e cilindri.
Il risultato è comunque soddisfacente e avendolo disegnato in maniera parametrica posso cambiare a piacere le dimensioni senza alcuna difﬁcoltà.
Campo fuoriuscente dal risonatore In questa prima simulazione si vedono bene i nodi del campo elettromagnetico che si formano sulla superﬁcie del risonatore nel caso in cui sia in risonanza.
La traccia rossa rappresenta il parametro S11 mentre l’altra il parametro S21
Analoga alla precedente, questa simulazione si differenzia dalla prima solo per la dimensione della fascia centrale in rame costruita nel modello.
Parametri S11 e S21 Si nota una minore quantità di campo fuoriuscente, causata probabilmente dalla nuova e più piccola dimensione della cavità.
Nel graﬁco si vede che con una minore fuoriuscita di campo, si ha un peggioramento delle risonanze e di conseguenza vi sarà anche un peggioramento della misura.
Come il risonatore precedente anche questo è stato realizzato in maniera parametrica.
Il risonatore a guida d’onda rettangolare è composto da una guida d’onda aperta su uno dei suoi lati e distribuita circolarmente.
Sapendo che una variazione della fessura comporta una variazione sulla fuoriuscita del campo elettromagnetico, stavolta decido di dimezzare lo spessore del risonatore, portandolo da 3[mm] a 1.5[mm].
Campo fuoriuscente dal risonatore La differenza fra questo caso ed il precedente è lampante, la profondità di fuoriuscita del campo all’esterno del risonatore è di gran lunga maggiore, inoltre si vede che specialmente nelle zone dove sono poste le antenne, il campo prende una forma particolare.
Come accennato prima, in questa variante della Simulazione 1 dimezzo lo spessore del modello usato nella prima simulazione del risonatore Microstrip.
Come accennato in precedenza, Bühler AG lavora con prodotti organici e nello speciﬁco cereali.
L’azienda mi ha fornito alcuni campioni di materiali che i loro macchinari elaborano quotidianamente.
I materiali sono quattro, trattasi di mais, mais macinato, pellet e grano per mangime, ognuno di questi è in tre stati d’umidità diversi, ovvero secco, appena raccolto (senza interventi) ed inumidito.
Lo scopo di questa misura è ottenere i parametri dielettrici dei materiali, in questo modo potrò utilizzarli durante le simulazioni e la progettazione del nuovo risonatore.
Questi parametri sono essenzialmente gli elementi dell’omonima matrice, che permette di capire in maniera relativamente semplice la relazione esistente fra due o più componenti.
Schema sempliﬁcato di un dispositivo a due porte [1] Separando riﬂessione e trasmissione, possiamo ottenere per ognuna delle due porte un informazione che esprime la quantità di segnale ricevuto o riﬂesso da parte di una qualsiasi delle due porte.
Dove S11 e S22 sono i coefﬁcienti di riﬂessione rispettivamente delle porte 1 e 2, S12 e S21 rappresentano invece la quantità di segnale trasmessa alla porta 1 dalla 2 e viceversa.
Per una maggiore comprensione, se si osservano i graﬁci del capitolo Simulazioni preliminari (ﬁgure 11 13 18 20 e 22) si vede che i dati rappresentati dalla linea rossa corrispondono al parametro S11 quindi è la quantità di segnale inviato dalla prima porta che ritorna sempre sulla stessa, a causa di riﬂessioni.
La seconda linea invece sta ad indicare il parametro S21 , che come spiegato un attimo fa, indica il segnale ricevuto alla seconda porta dalla prima.
Normalmente tutti i parametri della matrice hanno la loro importanza, ma in questo caso, possiamo concentrarci maggiormente solo sul S21 .
Il sensore ha il grosso vantaggio di avere già integrati i connettori tramite i quali è possibile collegare un network analyzer, si possono dunque ricavare i parametri S direttamente.
Lo strumento permette l’esportazione dei dati misurati (matrice S ) in diversi formati, fra i quali, il formato Touchstone (.s2p) spesso utilizzato in questo campo.
Senza particolari difﬁcoltà, passando per il programma Excel [11], riesco a importare tutti i dati in Matlab [12], dove posso analizzarli ed arrangiarli a mio piacimento.
Dal momento che le misure non presentano differenze importanti, per questioni di leggibilità nelle pagine seguenti, sono commentati i tre tipi di graﬁci riguardanti solo uno dei materiali misurati.
In tutti e tre i graﬁci sono rappresentate quattro tracce, che corrispondono al parametro S21 rilevato dal sensore per i seguenti casi:
Segue lo schema del setup di misura utilizzato.
Valori S21 - Mais macinato Questo primo tipo di graﬁco rappresenta la risposta frequenziale del sensore disposta su scala lineare.
L’asse delle ordinate non ha unità di misura in quanto i dati sono normalizzati, in questo modo un valore pari all’unità, sta ad indicare che il parametro S21 , ovvero la quantità di segnale in ricezione è massima.
Come è possibile osservare nei graﬁci da ﬁgura 72 a ﬁgura 82 (vedi Allegati ), si ha sempre un comportamento analogo in tutti i casi, ci sono generalmente 7 picchi di risonanza distanziati in frequenza regolarmente e di ampiezza crescente.
Si nota dunque una certa regolarità nelle risonanze al di sotto dei 3.70[GHz ], mentre al di sopra di questa soglia, il tutto diventa più imprevedibile.
Ha senso dunque concentrare gli studi successivi sulla zona frequenziale compresa fra 2[GHz ] e 4[GHz ].
Osservando gli ingrandimenti dell’ultimo picco frequenziale, ovvero le ﬁgure 74, 77, 80 e 83 (vedi Allegati ) si può notare una leggera discrepanza fra i diversi materiali, fenomeno atteso in quanto ogni materiale possiede una permittività dielettrica differente.
Quindi i materiali inumiditi sono caratterizzati dalla curva d’ampiezza minore (ﬁgura 27), in quanto posseggono il valore più alto d’attenuazione, a seguire vi sono i campioni appena raccolti ed inﬁne quelli asciutti.
Qui di seguito un graﬁco che rappresenta la variazione del fattore di qualità Q in funzione dei picchi di risonanza.
Fattore di qualità Q Proprio secondo le aspettative, le risonanze ottenute senza MUT hanno un fattore di qualità superiore, e in scala seguono i materiali asciutti, quelli appena raccolti ed inﬁne i campioni inumiditi.
Di seguito la tabella riassuntiva contenente i dati rilevati tramite lo strumento del laboratorio.
A causa di una certa imprecisione da parte della misura ogni campione è stato misurato 5 volte, questo per avere un valore medio della permittività dielettrica più afﬁdabile, afﬁancato poi al valore della deviazione standard σ .
Misure ε dei campioni Naturalmente la costante dielettrica ε aumenta con l’aumentare dell’umidità nel materiale.
Intuitivamente si può comprendere questa variazione osservando il valore dielettrico dell’acqua che (a temperatura ambiente) essendo pari a ε ∼ = 80.1 è molto superiore ai valori dei materiali secchi e che dunque va ad aumentarne la media complessiva.
Inizialmente si pensava che la misura della permittività dielettrica fosse cruciale al ﬁne di determinare il contenuto d’acqua del materiale, poi invece si è notato che con la procedura matematica adottata2 , era un elemento superﬁciale.
Consultando i documenti [7] [13] [2] [14] in parallelo alle simulazioni preliminari (vedi immagini 10 e 19), noto che il risonatore a guida d’onda rettangolare (Rectangular waveguide resonator ) ha un numero maggiore di similitudini con i brevetti, inoltre i risultati delle simulazioni (per questo risonatore in particolare) promettono bene.
Il risonatore in questione è costituito da un materiale dielettrico, nel caso delle simulazioni ho optato per un particolare tipo di ceramica (Rogers TMM10 R ), uno strato di rame inferiore, uno secondo strato di rame superiore cavo in punti determinati, degli anelli di rame che sezionano il dielettrico ed inﬁne i due accoppiatori il quale scopo è interfacciare la coppia di cavi coassiali.
Le parti in rame rafﬁgurate nelle quattro immagini seguenti servono a limitare la propagazione del campo elettromagnetico, indirizzandolo dunque quasi solo all’interno del risonatore, il fatto che la parte superiore di rame presenti delle cavità, è dovuto al fatto che una parte del campo deve per forza fuoriuscire in modo da poter penetrare nel materiale da misurare.
Il dielettrico invece permette la costruzione del risonatore con dimensioni estremamente ridotte, a causa del suo effetto di riduzione della velocità di propagazione elettromagnetica.
Di seguito un paio d’immagini che rafﬁgurano il fenomeno appena descritto.
Nodi formati sul Rectangular Waveguide Resonator visti dal ﬁanco
Come visto in precedenza nel capitolo Onde elettromagnetiche, la velocità di propagazione di un’onda elettromagnetica, dipende dal materiale che sta attraversando ed in particolare dalle sue caratteristiche dielettriche e magnetiche (ε0 , εr , µ0 e µr ).
I due materiali in questione (aria e MUT ) avranno dunque intaccato la velocità del segnale in maniera differente e potremo quindi determinare la costante dielettrica del MUT. A questo punto potremmo eseguire questo procedimento a qualsiasi frequenza compresa nel range delle microonde, purtroppo (o fortunatamente) i risultati migliorano drasticamente solo quando operiamo su una frequenza di risonanza, è dunque opportuno lavorare in questi range di frequenza relativamente stretti.
Si vede dunque che con l’aumentare dell’umidità le tracce tendono a diminuire sia d’ampiezza sia di frequenza.
Si nota inoltre che la differenza fra le curve è molto marcata sui picchi e non sugli avvallamenti.
K è il fattore dipendente dall’orientamento dell’oggetto 0 < K è la variazione d’ampiezza è esprimibile con ∆T = 10 3
Concludendo, non si possono utilizzare le due equazioni in questa forma, è dunque necessario trovare un modo per ridurre la quantità d’incognite presente.
Dato che entrambe le equazioni hanno la dipendenza del volume espressa da vs v0 ,
un’equazione con l’altra, otteniamo una nuova formula indipendente dal volume della cavità, dal volume del materiale e inoltre indipendente ad eventuali variazioni di densità.
Come accennato brevemente nel capitolo Caratterizzazione dei materiali la misura della costante dielettrica si è rivelata d’importanza trascurabile, in quanto avendo il fattore X , il quale è composto da ∆F ∆T ,
si possono ignorare i valori che assumono ε e ε
Nonostante il parametro K non sia d’importanza cruciale per la risoluzione del calcolo, resta comunque il fatto che sia ∆F sia ∆T dipendano da esso.
può essere separata nella seguente con annessa la correzione del fattore K
Dal libro [3] si legge che un fattore K = 1 è utilizzato quando si sta misurando un oggetto simile ad un cilindro molto ﬁne e parallelo al campo elettrico, arbitrariamente per i calcoli è stato utilizzato un valore K = 0.4. Ora con l’equazione generale (anche questa indipendente alla densità1) che permette di ottenere il quantitativo d’acqua ([3] pagina 183), si può determinare l’umidità del materiale in questione a X
A sostegno della teoria, segue la comparazione fra i valori d’umidità dei campioni forniti da Bühler e quelli calcolati tramite il metodo appena descritto.
Qui sotto invece sono rappresentati in funzione della frequenza i valori ricavati dai calcoli (ricordo che non avendo il parametro K , è stato arbitrariamente ﬁssato a K = 0.4, di conseguenza il calcolo non è afﬁdabile al 100%).
Si vede che nonostante l’imprecisione del calcolo, i valori risultanti sono comunque vicini all’umidità reale.
Una volta capito il principio di funzionamento, si può passare alla parte di simulazione, che ha lo scopo primario di investigare il comportamento del sensore in funzione delle variazioni geometriche.
Di seguito un’immagine del modello utilizzato, con rappresentate le grandezze prese in considerazione.
REXT è la grandezza del raggio esterno della cavità, mantenendo dunque una larghezza d’anello ﬁssa a 15[mm] è possibile far variare la posizione della cavità.
Inoltre, si è pensato che capire il comportamento del sensore in funzione delle caratteristiche del materiale esterno sarebbe stato interessante, dunque anche il valore ε del MUT è stato fatto variare.
Per questioni di tempo, le variazioni geometriche concernenti le variabili elencate qui sopra ed a ε sono state simulate tenendo in considerazione una variabile alla volta (quindi con le altre ﬁsse ad un valore).
Una simulazione completa avrebbe richiesto un ammontare di tempo molto maggiore di quello a disposizione.
Non possiamo però sapere la direzione degli spostamenti, l’intensità delle variazioni di banda, e soprattutto se ci sono delle regolarità, ovvero se per esempio diminuendo le dimensioni generali del risonatore, tutte le risonanze aumenteranno d’intensità anzichè solo alcune.
Nel capitolo Simulazioni preliminari vi è scritto che il dominio frequenziale nei test è stato conﬁnato fra 2[GHz ] e 6[GHz ], accorgimento preso per non sprecare tempo nella simulazione dei modelli.
Risposta frequenziale a vuoto del sensore Tews La ﬁgura qui sopra mostra che oltre i 4[GHz ] il tutto diventa abbastanza irregolare a causa di risonanze armoniche e modi superiori, i quali non sono evidenti da interpretare.
Dal software è possibile ricavare tante tipologie di graﬁci in quanto i parametri S sono relativamente versatili e possono essere trattati per ottenere parecchie informazioni.
In questo capitolo verranno inizialmente riportati una prima volta i graﬁci rafﬁguranti i parametri S11 ,
Successivamente vista la complessità di lettura, verranno riassunti in cinque graﬁci 3D per una maggiore comprensione ed un’analisi sempliﬁcata. 8.3.2.1 Simulazioni parametriche - 2D
Questa prima immagine rappresenta l’effetto che la variazione dello spessore del risonatore ha sul parametro S21 .
Osservando il graﬁco si vede bene che ci sono delle variazioni evidenti fra i picchi di risonanza, inoltre si si osserva con maggiore scrupolo si nota anche una certa regolarità.
Dati S11 per H parametrizzato Simile al caso S11 l’immagine seguente mostra la variazione delle risonanze, anche se questa volta si osserva il parametro S21 .
Confrontando le due immagini si vede che apparte per la forma non ci sono differenze fra l’ordine delle curve, di conseguenza d’ora in avanti, per comprensione, saranno rafﬁgurati solo i graﬁci che concernono il parametro S21 .
Dati S21 per H parametrizzato Ora è necessario capire l’andamento di questa variazione, nel software isolo dunque solo alcune delle curve disponibili e applico dei cursori che permettono di vedere in maniera esatta come si spostano le risonanze.
Parte di dati S21 per H parametrizzato con cursori Con i cursori è quindi possibile estrarre due informazioni, il fatto che con l’aumentare della frequenza la distanza fra le risonanze aumenta, ed inoltre che con l’aumentare del valore di
Anche in questo caso, per motivi di qualità di lettura, per i prossimi casi non saranno rappresentati i graﬁci con i cursori in quanto successivamente tutte le informazioni sono facilmente reperibili dai graﬁci 3D.
Ovviamente qualora si volesse lo stesso consultare i graﬁci, è possibile trovarli negli allegati.
Dati S21 per RIN T parametrizzato Il graﬁco soprastante permette di estrapolare parecchie informazioni.
Ad esempio nel caso della variazione di H si vede che le risonanze si spostano leggermente, e inoltre è possibile notare un paio di curve che probabilmente corrispondono a modi di trasmissione non ottimali.
Dati S21 per REXT parametrizzato La lettura in 2D di questo graﬁco è difﬁcile, a questo proposito si analizzerà meglio il tutto successivamente nelle immagini 3D. 8.3.2.1.5 Simulazione parametrica - εM U T
Dati S21 per εM U T parametrizzato Come nel caso precedente la lettura in 3D porterà maggior chiarezza nella simulazione.
sione dei graﬁci rafﬁgurati nelle pagine precedenti risulti abbastanza tediosa, specialmente quando si tratta di capire gli spostamenti delle risonanze tramite i cursori, è dunque opportuno arrangiarli in modo da avere una migliore interpretazione.
Importando i dati in Matlab [12], è possibile utilizzare graﬁci tridimensionali, i quali comportano i seguenti risultati. 8.3.2.2.1 Simulazione parametrica - H
Dati S21 per H parametrizzato - 3D In questo graﬁco si può vedere bene come le risonanze si spostano con il variare delle dimensioni, si nota che la distanza fra le risonanze aumenta con l’aumentare della frequenza e con l’aumentare dello spessore H .
In aggiunta si vede che ai valori H
Questo fenomeno è probabilmente dovuto al fatto che per alcune speciﬁche dimensioni geometriche, le equazioni di Maxwell non vengano soddisfatte, causando così un modo di propagazione erroneo, che in questo caso si traduce in un’assenza di risonanze.
non è molto pronunciata, ciò è una buona notizia perchè signiﬁca che durante il design del risonatore se ne potrà ignorare lo spessore (a condizione di evitare le dimensioni che causano modi erronei), oppure lasciare questo parametro per ultimo in modo da usarlo come ﬁne-tuning.
Dati S21 per d parametrizzato - 3D Come previsto nel caso bidimensionale le distanze fra i picchi di risonanza sono più evidenti, anche qui con l’aumentare della frequenza le risonanze si distanziano fra loro.
Interessante è il fatto che con il diminuire delle dimensioni generali del risonatore, i picchi si allarghino e si spostino leggermente più in alto in frequenza.
Dati S21 per RIN T parametrizzato 3D La simulazione di RIN T consolida il fatto che aumentando la frequenza aumenta la distanza fra i picchi di risonanza.
Inoltre mostra che variando RIN T (che ricordo corrisponde alla larghezza della cavità a forma d’anello) non si ottengono dei cambiamenti rilevanti, fenomeno osservabile bene a f
come nei casi precedenti, sono presenti dei modi trasmissivi non ottimali nello speciﬁco per
Dati S21 per REXT parametrizzato - 3D La simulazione parametrica di REXT è uno dei casi 2D che sono stati tralasciati a causa della loro complessità di lettura, qui si capisce il motivo per il quale è stata accantonata.
La maggior parte dei risultati corrispondono a modi di trasmissione non corretti, solo i casi per REXT
porta a riﬂettere sulla relazione delle due variabili RIN T e REXT , nella simulazione precedente si è visto che RIN T non ha un grande inﬂusso sulle risonanze, ma che la differenza più grande la fa l’aumento di frequenza.
In questo caso invece si vede che la posizione della cavità è importante poichè dal graﬁco si vede che solo pochi valori sono accettabili, concludendo si può dire che a discapito delle dimensioni della cavita, la sua posizione deve essere ben studiata e possibilmente deve portare l’anello cavo il più vicino possibile al limite esterno del risonatore stesso.
Dati S21 per εM U T parametrizzato - 3D Modiﬁcanto il valore della costante dielettrica, è stato possibile capire il comportamento del risonatore quando viene esposto a materiali con diversi valori d’umidità.
Si nota innazitutto l’assenza di risonanze ben deﬁnite quando il materiale in questione possiede un ε alto, inoltre si vede che scendendo con la frequenza anche i materiali con costante dielettrica pari o vicina all’unità hanno un inﬂusso negativo sul risonatore.
Si può dunque dedurre che aumentando la frequenza di lavoro e usando materiali con ε pari a valori bassi (quindi
Dati S21 per εdielettrico parametrizzato - 3D Inizialmente non prevista, questa simulazione è stata realizzata più tardi nel corso del progetto.
Vista la relazione della permittività dielettrica con le risonanze osservata nella simulazione precedente, si è pensato che fosse importante poter avere un’idea di come il materiale dielettrico con cui è stato fabbricato il risonatore inﬂuisce sulle risonanze.
All’interno del simulatore sono già presenti alcuni materiali che spesso vengono utilizzati in ambito professionale, e dato che il risonatore è stato costruito con un dielettrico della linea TMM R
, si è pensato di provare con altri materiali della medesima linea.
εT M M 10i = 9.8 si può avere un’idea di come la permittività dielettrica del materiale utilizzato inﬂuisca sulle risonanze.
Il tutto è confermato dalla simulazione, che mostra l’andamento appena descritto.
La seguente tabella riassume i comportamenti del risonatore in funzione delle variabili prese in considerazione nelle precedenti simulazioni.
Comportamento del risonatore Osservazione Posizione delle risonanze Distanza fra le risonanze Aumento di variabile
intuitivamente si può capire che le frecce crescenti indicano un aumento della
distanza frequenziale presente fra le risonanze, e al contrario quella decrescente sta ad indicare un avvicinamento dei picchi.
Durante le ultime settimane si è presentata la possibilità di fare delle scansioni a raggi x al sensore Tews presso una ditta esterna.
Questa possibilità è estremamente importante in quanto confermerebbe, nel caso in cui le geometrie siano simili, la validità del modello sviluppato su Ansys.
Immagine brevetto [2] Qui sopra ci sono due ﬁgure che mostrano il modello usato per le simulazioni (visto dal ﬁanco), ed il disegno del brevetto [2].
Nel CD sono comunque riportate tutte le scansioni qualora si volesse approfondire.
La scansione di sinistra è stata fatta sulla parte superiore del dispositivo, si vedono bene alcuni componenti, i cavi dei collegamenti, un anello che potrebbe essere la cavita dove fuoriesce il campo elettromagnetico, e le due porte di trasmissione disposte nelle parti superiore ed inferiore dell’immagine.
La ﬁgura di destra è invece un ingrandimento dell’immagine precedente, nello speciﬁco è rafﬁgurata la parte destra centrale.
In questa coppia di ﬁgure una delle due porte di trasmissione è rappresentata con colorazione e angolazione differente, le immagini mostrano non solo la posizione e il tipo di porte utilizzate, ma anche il metodo con cui il campo elettromagnetico viene generato (se partendo dal campo E o H ) in questo caso, tramite il campo E .
Ovviamente nella pagina precedente le immagini delle scansioni a raggi x non sono in scala, nelle versioni del capitolo Allegati però hanno afﬁancate le dimensioni, le quali mi hanno permesso di stabilire l’esatta geometria del risonatore:
Già dalla prima scansione si vedono alcune somiglianze con il modello e il disegno del brevetto, questa è un ottima notizia in quanto molto probabilmente il modello con cui sono state fatte le simulazioni è abbastanza fedele alla realtà dei fatti, di conseguenza le considerazioni del capitolo Analisi del sensore possono essere considerate buone.
Salta subito all’occhio che le porte di trasmissione sono poste in maniera differente, nella scansione si vede che sono localizzate all’interno di quello che sembra essere l’anello cavo dove fuoriesce il campo elettromagnetico, mentre nel modello si vede che sono poste sulla parte più esterna.
Le dimensioni generali sono relativamente vicine a quanto simulato, dalle scansioni emerge che il diametro totale corrisponde circa 7.9cm mentre considerando le variazioni eseguite durante le simulazioni parametriche, si è arrivati ad un diametro di circa 8.0cm - 8.5cm quindi il risonatore reale è leggermente più piccolo di quanto previsto.
Anche la cavità a forma d’anello è più contenuta rispetto alle aspettative, durante le simulazioni lo spessore della fessura variava da 1mm a 20mm circa, mentre da quanto si vede dalle scansioni, apparentemente la fessura misura 2mm.
Questo capitolo contiene i passaggi effettuati durante la progettazione del nuovo risonatore.
Il dispositivo è stato studiato tenendo conto di tutte le simulazioni effettuate ﬁn’ora e naturalmente mantenendo in considerazione la richiesta iniziale di Bühler.
L’obiettivo è dunque quello di progettare un risonatore che una volta integrato in un sistema di misura, possa avere delle performance superiori a quelle del dispositivo Tews.
Molti sono i parametri da tenere in considerazione, il seguente elenco ne fornisce un riassunto.
∗ Posizione ∗ Spessore • Caratteristiche frequenziali – Numero di risonanze – Modi di trasmissione Come prima cosa è opportuno ﬁssare degli obiettivi:
Il risonatore dovrà avere un minimo di ﬂessibilità nella geometria per evitare questi fenomeni sgraditi
Visto l’esito delle simulazioni e la notevole somiglianza con le scansioni a raggi x, scelgo di procedere con un risonatore di tipo Rectangular waveguide.
Senza particolari preoccupazioni decido di usare del Rame per il nuovo risonatore, le motivazioni di questa scelta sono legate puramente alle simulazioni, nelle quali ogni volta è stato adoperato il suddetto materiale senza particolari problemi.
Un raggio di queste dimensioni comporta un diametro compreso fra i 7.5cm e i 9.5cm range in cui il modo di trasmissione indesiderato, rafﬁgurato nella ﬁgura 50 non è presente e non si trova nemmeno nelle vicinanze, quindi sia l’obiettivo 3 sia il 4 sono in linea con questa scelta.
Durante la simulazione parametrica concernente lo spessore generale del risonatore (Simulazione parametrica - H ), si è notato che la variazione di questo parametro non comporta grossi cambiamenti nelle risonanze, e che è quindi possibile non tenerne particolare conto ed utilizzarlo poi in una fase di ﬁne-tuning.
Non avendo stabilito una dimensione precisa, mi limito a mantenere la fessura ad una distanza massima di 10mm dal bordo esterno.
Lo spessore della fessura è determinante per quanto riguarda la sensibilità del sensore che ospiterà il risonatore in progettazione.
Analogamente se la cavità è troppo stretta, il campo elettromagnetico si propagherà a fatica, risultando in un poderoso segnale ricevuto all’accoppiatore ma con scarsa quantità d’informazioni utili.
Fatti questi ragionamenti, lo spessore della fessura sarà sicuramente oggetto di ulteriori modiﬁche, pertando è compreso fra 2mm e 10mm.
Considerando che i seguenti parametri dipendono fortemente dai precedenti, mi limiterò per ora a ﬁssare dei vincoli relativamente generosi, che con il proseguimento della progettazione diverranno più severi. 10.2.3.4.1 Numero di risonanze
Tenendo conto delle simulazioni effettuate, un numero di risonanze nel range fra 2[GHz ] e
Ripetuto molteplici volte ormai, la presenza di modi trasmissivi superiori è scomoda, di conseguenza il risonatore dovrà avere geometricamente una buona protezione da questi fenomeni.
Tutto questo lo si effettuerà mantenendo dimensioni che, secondo le simulazioni non sono a rischio di modi erronei.
Di seguito una tabella con sintetizzate le scelte e i valori scritti nei paragraﬁ precedenti.
Linee guida del nuovo risonatore Parametro Tipo Materiale Conduttivo Dielettrico Raggio generale Dimensioni Spessore Fessura Caratteristiche frequenziali Numero risonanze Modi trasmissivi Tabella 10:
Modello del nuovo risonatore Molto simile ai precedenti, questa variante di modello si contraddistingue solo per le dimensioni.
Essendo il punto di partenza, tutti gli altri parametri sono rimasti invariati.
Le parti arancioni sono costituite da Rame, il verde chiaro rappresenta il dielettrico, e sulla parte inferiore è possibile vedere gli accoppiatori coassiali.
Vista la discreta quantità di parametri da tenere in considerazione (come si vede dalla tabella 10) è opportuno iniziare subito a ridurre la mole di lavoro.
Ricordo che la propagazione attraverso il materiale sotto test è uno dei parametri importanti visti in precedenza.
Seguono dunque due graﬁci che mettono in relazione la percentuale di campo elettromagnetico fuoriuscente con la distanza per entrambi i materiali dielettrici.
Campo E.M in funzione della distanza - TMM10 R La simulazione è stata eseguita radialmente, di conseguenza a causa degli svariati angoli presi in considerazione le curve rappresentate sono molteplici.
Le etichette invece sono state messe per tutte le tracce al valore ﬁsso di 0.36[−] che corrisponde approssimativamente al fattore 1 e . Questo parametro aiuta a stabilire quanto il campo elettromagnetico sia in grado di propagarsi nel materiale sotto test.
In questo caso vediamo che al raggiungimento del
Il materiale TMM10i R sarà dunque utilizzato nel modello e nelle simulazioni successive.
Ripensando alla tabella stilata in precedenza, si può già notare che il numero di risonanze volute è al limite inferiore di quanto stabilito, ovvero otto.
Le due immagini seguenti rappresentano invece il campo elettromagnetico fuoriuscente dal modello.
Campo E.M fuoriuscente dal risonatore - Trasparente In queste due ﬁgure la scala è stata modiﬁcata in quanto la visualizzazione per mezzo della precedente risultava non ottimale.
Notare che il numero di nodi si è ridotto, questo a causa delle dimensioni più contenute di questo modello.
A questo punto bisogna scegliere dei valori precisi per lo spessore della fessura e per il raggio generale.
In proposito, nelle prossime due pagine vi sono due simulazioni mi guidano alla scelta da prendere, di seguito i commenti.
Tenendo in considerazione il fatto di dover evitare modi di trasmissione superiori e di mantenere un numero minimo di risonanze, per il raggio generale la scelta cade dunque su un valore pari a Rgen = 47[mm], a causa dei modi superiori presenti con le dimensioni minori.
Decido dunque che la fessura avrà uno spessore di 2[mm], scelta giustiﬁcata dalla presenza di un modo superiore nelle vicinanze dei 3[mm], ed in linea con il terzo obiettivo stilato in precedenza.
Le dimensioni del risonatore sono dunque le seguenti.
Dimensioni ﬁnali Parametro Tipo Materiale Conduttivo Dielettrico Raggio generale Dimensioni Spessore Fessura Posizione Spessore Valore Rectangular Waveguide Rame TMM10i R
risonatore, seguono due immagini che rappresentano lo strato superiore ed inferiore del
Seppure l’obiettivo ﬁnale di questo progetto fosse quello di migliorare un prodotto esistente, questo lavoro è stato principalmente orientato alla comprensione delle relazioni esistenti fra geometrie e caratteristiche frequenziali di un risonatore a microonde.
Nonostante questa focalizzazione mirata, gli obiettivi stilati all’accettazione sono stati rispettati.
Purtoppo dati rilevanti come le scansioni a raggi x sono giunte a disposizione nelle ultime settimane, quindi l’effettiva realizzazione del prototipo è stata accantonata, anche a causa d’assenza di materiali disponibili in sede.
In qualsiasi caso però tutta la progettazione è stata eseguita ed i ﬁles corrispondenti sono reperibili per mezzo del CD allegato.
Rimangono da svolgere alcune migliorie che porterebbero questo studio ad una maggiore completezza.
Alcune di queste sono la realizzazione di un prototipo ed il confronto con il sensore fornito da Bühler.

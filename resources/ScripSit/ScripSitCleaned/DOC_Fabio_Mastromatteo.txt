Il progetto mira all’ottimizzazione della supply chain di società operanti nel settore tessile in modo da ridurre l’impatto ambientale complessivo dei propri prodotti e poterlo certificare.
Tale ottimizzazione deve tenere conto della forte incertezza che caratterizza tempi costi ed impatti di trasporti e produzioni.
Si è partiti dall’analisi delle necessità del committente (Hugo Boss) e dal progetto EcoLogTex.
Sono stati analizzati i tipi di ottimizzazione, gli algoritmi conosciuti e in base al problema reale si è deciso di implementare l’ottimizzazione robusta.
Sono stati analizzati quindi i problemi già risolti da studi precedenti e sono state raccolte informazioni e metodologie per formulare le teorie per risolvere questo nuovo problema, che non era stato risolto in precedenza.
In particolare K rappresenta il numero di percorsi analizzati dall’algoritmo.
Nei casi in cui non venga trovata una soluzione ottimale, l’ordine in cui sono generati i percorsi determina una forte diminuzione della probabilità di trovare cammini migliori nelle iterazioni successive, questo ci consente di utilizzare valori bassi di K e di trovare comunque una soluzione euristica di alta qualità.
I risultati computazionali confermano l’efficacia dell’approccio proposto e corroborano le intuizioni su cui il progetto è basato.
L’algoritmo è in grado di garantire soluzioni ottimali per la maggior parte dei problemi di tre delle quattro famiglie di grafi studiati ed è in grado di garantire soluzioni euristiche di alta qualità per le rimanenti.
I tempi in cui vengono trovate le soluzioni sono molto bassi e rendono il progetto applicabile al problema di ottimizzazione di una supply chain nel settore tessile, in cui il decision maker si trova a dover confrontare i migliori percorsi su grafi direzionali aciclici a livelli simili a quelli utilizzati in Hugo Boss, che vengono risolti in qualche decimo di secondo utilizzando un normale PC. I test effettuati con altre famiglie di grafi mostrano come la soluzione sviluppata in questo progetto sia applicabile anche a supply chain più complesse ed a problemi diversi.
La sua valenza scientifica corroborata da nuovi risultati teorici, ha permesso la preparazione di un articolo candidato alla pubblicazione sul Journal Of Applied Operational Research (JAOR), Special Issue on Optimization Methods in Logistics.
Then we analyzed the problems already solved by previous studies and we gathered information to formulate theories and methodologies to solve this new problem, which had not been solved before.
il progetto concerne lo sviluppo di algoritmi robusti per l'ottimizzazione della catena logistica nel settore tessile.
I parametri di ottimizzazione considerati coprono i costi, le stime dei tempi di produzione e consegna, e le stime dell'impatto ambientale.
In questo progetto nuovi algoritmi robusti verranno sviluppati e implementati per tenere conto di tale incertezza.
Il punto di partenza sono i classici algoritmi deterministici che non considerano incertezza, e che sono comunemente usati.
Obbiettivi Progettare e implementare nuovi algoritmi di ottimizzazione robusta (correntemente non noti nel ramo di ricerca associato) Tecnologie C, C++, STL.
L’algoritmo si propone di essere un tool strategico e di primaria importanza, dovrà fornire H migliori soluzioni, in tempi rapidi, utilizzando dati per natura soggetti a forte incertezza. Al decision maker verranno proposte le opzioni e sarà lui a prendere la decisione finale, incorporando nella scelta strategie aziendali globali non parametrizzabili.
Per sviluppare questa soluzione sono stati studiati e sviluppati nuovi algoritmi di ottimizzazione robusta discreta H-shortest path con interval data basati su solutions ranking.
Verrà prima fornita una descrizione del problema, che illustrerà la piattaforma e le motivazioni che hanno portato alla decisione di sviluppare questo progetto.
Successivamente verranno spiegati i concetti alla base dell’ottimizzazione robusta, indicando i motivi per cui è stata scelta.
Dopodiché avremo un capitolo riguardante lo studio della soluzione, che illustrerà come il problema è stato affrontato, quali tecniche di ottimizzazione sono state utilizzate e quali nuove sono state implementate.
Avremo quindi la descrizione dell’algoritmo, delle condizioni di uscita, dei miglioramenti e dell’implementazione.
Il documento proseguirà con l’illustrazione dei risultati computazionali, la comparazione con le ipotesi e presenterà le conclusioni.
Le motivazioni che hanno portato allo sviluppo del progetto EcoLogTex riguardano principalmente la gestione dell’impatto ambientale e della complessità delle supply chain:
Benchmarker Fornirà un’interfaccia che permetterà agli stakeholder di inserire i propri parametri, confrontarsi in modo anonimo con la concorrenza e con le ricerche di mercato, calcolare gli impatti e capire quali fattori hanno i pesi maggiori.
Nella figura 2.1 vediamo un esempio di parametri forniti da un’indagine di mercato
Supply Chain Designer È il componente principale del progetto, consentirà di disegnare la supply chain (figura 2.2) e di fornire percorsi alternativi ai decision maker prendendo in considerazione fattori come costi, tempi ed impatto ambientale.
Più in dettaglio al decision maker verrà chiesto di regolare i pesi dei differenti fattori, che verranno utilizzati per ottimizzare la catena logistica e fornire i percorsi migliori.
In questo modo si potrà ottenere un ventaglio di soluzioni ed ottimizzare la catena logistica in base alle strategie aziendali.
Questo processo sarà tipicamente interattivo, in cui il manager vedrà alcune proposte sullo schermo e modificherà i pesi dei differenti fattori in base alle aspettative ed alle preferenze aziendali fino a quando non sarà soddisfatto del risultato.
Gli input per il processo di ottimizzazione vengono dai contratti firmati con i fornitori, principalmente riguardo costi e tempi di produzione e di trasporto, e da fonti di informazione più generiche come database specialistici (es.
Nel nostro caso le informazioni vengono arricchite dai dati collezionati tramite il Benchmarker.
L’output del Supply chain designer sarà la fonte di informazioni principale usata del Reporter.
Reporter Basandosi sulle informazioni inserite tramite il Benchmarker e le decisioni prese tramite il Supply chain designer, fornirà la possibilità di ottenere per qualsiasi prodotto la tracciabilità e la stima dell’impatto ambientale.
Queste informazioni potranno essere usate per la creazione di una certificazione sull’impatto ambientale dei prodotti (figura 2.3), utilizzabile come strategia di marketing.
Riguardanti ogni elemento della supply chain in modo da consentire al decision maker di prendere decisioni strategiche globali su aspetti come i luoghi di approvvigionamento delle materie prime, di produzione dei prodotti semilavorati, i fornitori, le logistiche e tutto ciò che riguarda la supply chain, arrivando fino alla vendita ai consumatori.
La piattaforma dovrà comparare fra di loro i tre aspetti utilizzando degli appositi pesi e dovrà fornire al decision maker le H soluzioni che presentano le migliori performance, in modo che possa ottenere un ventaglio delle migliori soluzioni per prendere poi la decisione finale.
La piattaforma dovrà dare la possibilità ai fornitori di inserire autonomamente le proprie caratteristiche ed offerte nel sistema.
La parte del progetto analizzata in questa ricerca riguarda l’algoritmo di ottimizzazione che dovrà elaborare le informazioni e fornire le H migliori soluzioni.
a cosa serve, come funziona e perché è stata scelta.
Sono molte le decisioni che si trovano a doversi confrontare con dati non certi.
Incertezza nei costi di produzione, nei prezzi, nei costi del lavoro e nella reperibilità delle materie prime rendono complicato pianificare la produzione.
L’incertezza nei cash flow complica le decisioni riguardanti gli investimenti a lungo termine.
Lo sviluppo di nuovi prodotti è soggetto ad incertezza legata all’uso di materiali o di processi nuovi, alla risposta del mercato, al cambio di preferenza dei consumatori, alla reazione dei concorrenti, ad innovazioni tecnologiche che potrebbero rendere il prodotto obsoleto, così come a condizioni macroeconomiche.
L’incertezza non è un evento occasionale e temporaneo che devia dai piani prestabiliti, bensì si tratta di una condizione strutturale.
Il modo migliore per gestirla è di accettarla, strutturarla ed includerla nel processo decisionale.
L’approccio robusto è costruito su queste premesse ed è uno strumento efficace per strutturare l’incertezza e prendere decisioni in sua presenza.
Esistono molti tipi di problemi incerti (ai quali non è possibile attribuire una probabilità fra la decisione e il risultato), è possibile trasformarne alcuni in problemi di rischio, attribuendo una stima soggettiva delle probabilità, è possibile inoltre trasformarli in problemi certi, ad esempio assegnando una stima dei valori che si pensa si presenteranno in uno scenario futuro.
Però a volte aspetti futuri sono impossibili da determinare con una precisione accettabile e in ogni caso molto probabilmente la decisione sarà non ottimale.
L’approccio deterministico utilizza dati presunti certi per trovare la soluzione.
In questo modo viene ignorata completamente l’incertezza, oppure vengono usati dati storici per predire il futuro.
Ad esempio se il decision maker dovrà scegliere se affidare una spedizione ad un camion piuttosto che ad un treno, non potrà tenere conto che il camion può essere soggetto più facilmente a ritardi imprevisti o che i treni in alcune nazioni possono essere soggetti più facilmente a scioperi.
Questo approccio, in situazioni che presentano una incertezza significativa, porta inevitabilmente ad ottenere frequenti risultati non accettabili.
L’approccio stocastico tiene conto della variabilità dei dati che potrebbe presentarsi in futuro, però è necessario fornire all’algoritmo le probabilità di tutti i valori.
Generalmente per decisioni complesse che richiedono l’analisi di molte informazioni, viene assunto che tali dati siano indipendenti fra di loro.
Esistono però critiche su molti livelli per quanto riguarda l’ottimizzazione stocastica in presenza di significativa incertezza.
In primo luogo l’approccio forza i decision makers ad assegnare probabilità agli elementi degli scenari futuri.
Assegnare probabilità e distribuzioni gaussiane non è semplice, soprattutto per decisioni che presentano fattori multipli interdipendenti, magari con elementi che dipendono da società esterne.
Un’altra critica riguarda l’utilizzo di questo approccio in situazioni in cui pochi elementi determinano l’incertezza di molti altri.
La non correlazione fra gli elementi è la ragione più importante del fallimento dei modelli deterministici e stocastici in condizioni di significativa incertezza.
Questo è particolarmente importante in caso di decisioni che si devono prendere una volta sola.
In questo caso la Long Run Optimization è irrilevante perché non abbiamo e non avremo uno storico di risultati a cui affidarci.
Infine i decision maker si trovano spesso ad essere giudicati a posteriori, come se lo scenario che si è verificato fosse già noto.
In questi casi il decision maker oltre a preoccuparsi di trovare la soluzioni migliore fra gli scenari a disposizione, deve preoccuparsi che la soluzione scelta sia valida anche in relazione agli eventi verificatisi successivamente.
Le ottimizzazioni deterministiche e stocastiche non si occupano di questo aspetto, mentre vedremo che l’approccio robusto è basato su di esso.
Analizzando le caratteristiche e le critiche che vengono fatte agli approcci tradizionali in situazioni di incertezza, possiamo ora delineare il profilo a cui meglio si addice l’approccio robusto:
il decision maker vuole evitare il rischio, viene valutato a posteriori in base allo scenario che si è realizzato e molte delle decisioni devono essere prese in situazioni uniche, non ripetibili.
Pertanto non viene ricercata la soluzione migliore per uno specifico scenario o quella per lo scenario più probabile, neanche quella migliore “Long Run”.
Il decision maker vuole una decisione robusta, ovvero una decisione che si comporti bene in ogni scenario e che eviti le soluzioni peggiori.
L’approccio robusto utilizza degli scenari per rappresentare l’incertezza del nostro modello di dati.
Ogni scenario rappresenta una possibile realizzazione dei parametri fondamentali del nostro modello.
In questo modo il decision maker può descrivere la relazione fra i maggiori elementi incerti e i tanti altri, definendo pertanto le relazioni che legano gli elementi fondamentali.
La generazione degli scenari è la parte più importante del processo.
Una volta delineati gli scenari, abbiamo a disposizione tre tipi di ottimizzazione robusta.
L’absolute robust decision minimizza il costo massimo, fra tutte le decisioni realizzabili in tutti gli scenari [4].
Questa decisione è la più conservativa e si basa sul concetto di evitare il peggio.
La robust deviation decision trova la migliore peggiore deviazione dalla soluzione ottima, fra tutte le decisioni realizzabili in tutti gli scenari [4].
La relative robust decision trova la migliore peggiore deviazione in percentuale dalla soluzione ottima, fra tutte le decisioni realizzabili in tutti gli scenari [4].
Una volta definiti questi tre elementi, il decision maker potrà utilizzare tool di programmazione standard per generare la decisione robusta.
Criterio di robustezza 3. Formulazione di un modello di ottimizzazione che generi la decisione robusta
Ad esempio un trasporto su gomma sarà soggetto a variazioni che riguardano il traffico, un trasporto via nave sarà soggetto al mal tempo o alla congestione dei porti.
Un fornitore se dovrà fornire una stima certa sui tempi, sceglierà un valore che gli consenta di effettuare la vendita, ma anche di non pagare penali.
Alcune nazioni a differenza di altre per cultura e per fattori congiunturali sono più soggette a possibili ritardi.
Non possiamo stimare dei valori probabilistici in quanto vengono utilizzate spesso strutture su cui non abbiamo uno storico, quindi niente Long Run Optimization, così come sono i fornitori stessi ad inserire una buona parte delle informazioni, sarebbe troppo complesso chiedere di fornire le stime sotto forma di Gaussiane;
Soprattutto la certificazione dell’impatto ambientale, ma anche i costi ed i tempi richiedono di essere certi di non superare determinati target.
Nel nostro approccio quindi la strutturazione dell’incertezza tramite scenari avviene tramite gli intervalli, la metodologia di applicazione è spiegata nel paragrafo 4.2.2. Il criterio di robustezza applicato è il Robust Deviation.
Studio dell’approccio risolutivo 4.1 Semplificazione dei dati e attribuzione dei pesi
Alcuni presentano un valore certo, altri dei range con valori minimi e massimi.
Ponendo i trasporti sugli archi e le lavorazioni sui nodi, una rappresentazione molto semplificata e verbosa di una soluzione utilizzando un grafo direzionale è la seguente (figura 4.1):
A questo punto abbiamo tre trasformazioni da effettuare ai dati in modo da poter utilizzare i tool di ottimizzazione standard per i grafi: 1.
Spostare tutti gli impatti sugli archi 2.
Applicare i pesi forniti dal decision maker 3. Trasformare i tre tipi di impatto in un intervallo unico Nel dettaglio:
Dato che abbiamo a disposizione un grafo orientato a livelli, per spostare gli impatti sugli archi si è deciso di sommare i valori dell’arco con i valori del nodo a cui punta.
Questo è utile anche per i casi in cui ci siano diversi tipi di trasporto a disposizione, in questo modo avremmo diversi archi che puntano allo stesso nodo e che espongono impatti diversi.
A questo punto moltiplichiamo i range per i pesi stabiliti dal decision maker, in questo esempio prenderemo 0.25, 0.25, 0.5 per semplicità.
Trasformiamo tutti i valori in costi (i costi li abbiamo, per i tempi chiediamo al decision maker il costo orario, per gli impatti stimiamo dei costi come ad esempio i carbon credit).
Compariamo tramite algoritmo (ad esempio calcolando i valori dello shortest path per ogni dimensione e moltiplicando gli impatti per la proporzione dei risultati ottenuti, oppure usando la Pareto distribution).
Normalizziamo i dati prendendo il valore massimo per ogni dimensione e gli attribuiamo il valore uno, prendiamo il valore minimo e gli attribuiamo il valore 0 e modifichiamo tutti i valori intermedi in proporzione.
Per semplicità nel nostro esempio useremo la prima opzione, ovvero moltiplicheremo solo per i range stabiliti dal decision maker.
La soluzione che verrà adottata in produzione sarà probabilmente la normalizzazione.
Si è deciso inoltre di scartare archi i cui impatti presentano valori eccessivamente alti o bassi, attribuendoli a errori di inserimento dei dati.
Il problema che dobbiamo risolvere si presenta come uno shortest path, a cui dobbiamo applicare un approccio Robust Deviation, i dati si presentano sotto forma di intervalli e dobbiamo fornire le H migliori soluzioni.
Non esiste un algoritmo che risolva tutte queste necessità, ma sono già state studiate diverse soluzioni che possiamo integrare ed ampliare.
Nel nostro caso, avendo a disposizione un grafo orientato con i pesi sugli archi, lo shortest consiste nel trovare il percorso che, rispettando l’orientamento degli archi, abbia la somma minore dei costi.
Gli incroci costituiscono i nodi e le strade gli archi.
Sugli archi potremmo mettere le distanze, oppure i tempi di percorrenza, oppure i costi.
Nel nostro caso dobbiamo fare in modo che la supply chain passi per ogni fase della lavorazione (proprietà garantita dall’orientamento degli archi) e dobbiamo trovare il costo minore.
La soluzione trovata è ottimale, la complessità computazionale è O(n2) e richiede valori non negativi sugli archi.
L’algoritmo inizia con il salvare il costo dei tragitti per i nodi connessi direttamente al nodo di partenza, successivamente si sposta sul nodo il cui tragitto ha il peso minore ed aggiunge il peso di tutti i tragitti che hanno origine da tale nodo.
Nei passaggi successivi l’algoritmo sceglierà sempre il nodo che ha il tragitto con peso minore dall’origine, fra tutti quelli salvati in precedenza e non già visitati e salverà il peso dei percorsi che hanno origine in tale nodo, se minori di quelli trovati in precedenza.
Si fermerà quando il nodo visitato sarà quello di destinazione.
Riprendendo l’esempio di sopra, i passaggi possono essere visualizzati tramite la seguente tabella.
Nei campi vengono inserite le distanze da start, in grassetto sottolineato i nodi già visitati (Tabella 4.1):
Per ogni percorso possibile calcoliamo il costo nello scenario UB, dopodiché creiamo uno scenario indotto assegnando agli archi che fanno parte del percorso UB con costo minore il peso massimo e a tutti gli altri il peso minimo.
Calcoliamo il costo dello SP dello scenario indotto e la differenza fra i già calcolati costi UB e lo scenario indotto ci darà il costo di robustezza.
In questo modo minimizziamo la differenza fra il costo massimo del percorso che abbiamo scelto e la sua alternativa migliore.
Il percorso di Robust Deviation è massimizzato nello scenario in cui il costo degli archi del percorso sono all’upper bound e il costo di tutti gli altri archi è al lower bound.
Sia lo scenario in cui il costo degli archi di siano al proprio upper bound e il costo degli altri archi sia al proprio lower bound.
Questa proposition implica che dobbiamo considerare un numero finito di scenari, corrispondente al numero di percorsi del grafo.
Tale numero però può crescere esponenzialmente in relazione al numero dei nodi del grafo.
Nell’algoritmo sono stati implementati dei miglioramenti che permettono di non calcolare tutti i percorsi, li vedremo nei prossimi paragrafi.
Abbiamo scelto la Robust Deviation perché garantisce soluzioni robuste nei confronti dell’incertezza con complessità computazionale accettabile.
La prima non presenta restrizioni sulla serie dei percorsi, la seconda invece presenta dei vincoli di non ripetizione degli archi o dei nodi già visitati nelle soluzioni precedenti.
Seguendo il percorso p1, l’algoritmo ripete le stesse operazioni sui nodi 4 e 7. Al termine il grafo rooted sarà diventato così (figura 4.8):
L’operazione viene ripetuta per tutti i nodi successivi del percorso p2, aggiungendo 4’’ e 7’’ ed ottenendo quindi il seguente grafo (figura 4.9):
Questa versione dell’algoritmo, paragonata alla precedente di Eppstein, permette un discreto risparmio di spazio, permettendo quindi di risolvere problemi più grandi ed in minore tempo.
Questo algoritmo ha complessità O(Km), dove K è il numero dei percorsi che vogliamo trovare e m è il numero degli archi, da aggiungere alla complessità necessaria per trovare p1, che però utilizziamo anche per altri scopi.
Per la sua realizzazione ci si è basati sul lavoro “An exact algorithm for the robust shortest path problem with interval dat a” di Montemanni e Gambardella [3], che è stato modificato ed ampliato includendo le nuove funzionalità.
Dato H > 0 che rappresenta il numero di soluzioni da presentare al decision maker e dato K ≥ H che rappresenta il numero massimo di iterazioni che dovrà eseguire l’algoritmo.
Definito come costo di robustezza il costo di robust deviation descritto al paragrafo 4.2.2 in cui lo scenario r vede gli archi visitati dal percorso p con associato il costo uij e tutti gli altri archi con associato il costo lij.
Ad ogni iterazione calcola il costo di robustezza dello scenario corrispondente del grafo G e lo invia ad un archivio.
Tale archivio controlla se il costo di robustezza massimo degli H percorsi salvati in precedenza è maggiore, lo salva, altrimenti lo ignora.
Le iterazioni continuano fino a che non sia raggiunta una condizione di uscita.
La decisione di analizzare i percorsi partendo dal ranking UB con costo minore è stata presa perché ad ogni iterazione, diminuisce la probabilità di trovare un percorso con costo di robustezza maggiore.
Quando ha esaurito i percorsi Ovvero quando non esistono più nuovi percorsi, in questo caso la nostra soluzione sarà ottimale.
Questa condizione di solito si verifica soltanto su istanze piccole.
Quando ha raggiunto un valore limite di percorsi calcolabili L’algoritmo contiene una variabile K che indica come condizione di uscita il numero massimo di percorsi calcolabili.
Quando trova una soluzione ottimale Questa è la condizione più interessante e complessa, fornisce una soluzione ottimale.
Questa regola è presa dal lavoro di Montemanni e Gambardella [3], sez. 3.2 e sarà ampliata per supportare gli H migliori percorsi.
Analizzando la congettura che uno shortest path ranking nello scenario upper bound è anche un buon ranking in termini di Robust Deviation, è possibile scoprire che esaminando i percorsi nello stesso ordine, ad ogni iterazione abbiamo la possibilità di ottenere una stima per difetto del costo di robustezza, utilizzabile come regola di arresto.
Il risultato del lemma 1 è usato nel seguente teorema, il quale, esaminando i percorsi in ordine non decrescente di UC(p), fornisce un lower bound del costo di robustezza dei percorsi non ancora esaminati.
Il risultato del teorema 1 viene utilizzato nella nuova proposition 1, che fornisce un criterio di uscita che ci consente di ottenere una soluzione ottimale.
In pratica: esaminando i percorsi in ordine di costo UC(p) non decrescente ed esaminando la differenza di costo fra UC(p) e UC(p1), quando questo valore supera il costo di robustezza massimo delle nostre migliori H soluzioni, siamo certi che non esisteranno soluzioni migliori rispetto a quelle già trovate.
Facciamo un esempio, nella tabella 4.2 indicheremo i risultati ottenuti con un ipotetico grafo di esempio (H=3):
Note Soluzione valida 1 Soluzione valida 2 Soluzione valida 3 Soluzione valida 4, esce la 3 Prop. 2 cost ≥ Robust cost massimo
A questo punto abbiamo un numero di percorsi uguale a quelli che vogliamo trovare, possiamo quindi iniziare a considerare la condizione di arresto.
Il proposition 2 cost (4) è minore del costo di robustezza massimo delle soluzioni trovate precedentemente, pertanto la condizione di arresto non è soddisfatta.
Il costo di robustezza è minore della soluzione p3, pertanto la elimino dalla lista e salvo p4. Quinto passaggio:
Condizione di arresto soddisfatta, ci fermiamo con la soluzione esatta.
Dopodiché inizializza alcune variabili e istanzia l’archivio dei percorsi.
A questo punto entra in un loop in cui trova il percorso con costo nello scenario U minore e non trovato in precedenza.
Se si verifica la Proposition 2, ovvero se il costo del percorso corrente nello scenario U – il costo di p1 nello scenario U è maggiore o uguale al costo di robustezza maggiore fra i percorsi salvati nell’archivio e l’archivio è pieno, allora abbiamo trovato la soluzione certa, perché i percorsi successivi potranno solo essere peggiori.
Se non esistono altri percorsi non trovati in precedenza, usciamo.
Se abbiamo raggiunto il numero massimo di percorsi considerabili, usciamo.
È sufficiente chiamare il metodo Store e PathsArchive deciderà se salvare il nuovo percorso se il costo di robustezza è minore del valore massimo salvato, la posizione in cui metterlo nella lista e se rimuovere percorsi precedenti nel caso il numero di soluzioni desiderate sia stato raggiunto.
Ci troviamo pertanto di fronte ad un problema con complessità non polinomiale.
Nel nostro caso siamo riusciti a ridurre la complessità grazie alle condizioni di uscita.
Come vedremo nei risultati computazionali, l’algoritmo riesce a ottenere risultati certi o comunque di alta qualità entro la K-esima iterazione, dove K viene limitato ad un valore massimo.
In dettaglio, il nostro algoritmo mitiga la complessità pratica sfruttando la Proposition 2, che ci permette di stabilire in modo molto efficiente quando abbiamo trovato i percorsi migliori senza doverli esaminare tutti ed utilizzando la caratteristica che ordinando la ricerca per costo nello scenario U non decrescente, i percorsi trovati in precedenza avranno una maggiore probabilità di essere migliori dei successivi.
Nella sezione 5.1 verranno descritti i tipi di grafi utilizzati, nella sezione 5.2 verrà presentata la relazione fra la lunghezza dei percorsi e le performance dell’algoritmo, nella sezione 5.3 verrà presentata la relazione fra il numero di migliori soluzioni ricercate e le performance dell’algoritmo, nella sezione 5.4 verranno discussi i risultati.
L’algoritmo è stato testato con grafi orientati con intervalli di peso sugli archi e divisi in tre famiglie.
Gli intervalli di costo sugli archi sono stati inseriti in modo da simulare le differenze tipiche di condizione stradale durante i diversi orari della giornata.
Per il momento Hugo Boss ci ha fornito solo le caratteristiche, ma non dati reali su cui lavorare.
Questa soluzione è utile per testare l’efficacia dell’algoritmo anche in situazioni senza vincoli particolari, quindi diversi da cartine stradali o da grafi aciclici ed a livelli.
Correlazione fra la lunghezza dei percorsi e le performance dell’algoritmo
Per i tipi di grafo Sottoceneri e Casuale, la lunghezza del percorso robust deviation non è nota in partenza (in termini di numero di archi).
In questa sezione verrà mostrata la correlazione fra la lunghezza dei percorsi e le performance dell’algoritmo.
Sono stati poi analizzati i tempi medi di risoluzione in relazione alla lunghezza del percorso risultante e riportati nel seguente grafico (figura 5.1).
Su 1.000 problemi, l’algoritmo ha raggiunto una soluzione certa o ottimale 995 volte.
Solo 5 volte è arrivato alla soluzione euristica (impostata a K=2.000 iterazioni), in occasione delle seguenti lunghezze di percorso:
Nel grafico sono stati inseriti i risultati solo fino ai 24 vertici perché per le lunghezze superiori le occorrenze erano troppo poche.
Includendo le soluzioni euristiche, il tempo medio di risoluzione del problema è stato di 8.10 secondi.
Quando il percorso da s a t è lungo, anche i percorsi alternativi da s a t tenderanno ad essere lunghi, pertanto l’algoritmo per il calcolo dello shortest path impiegherà più tempo (Dijkstra [2]);
Quando il percorso da s a t è lungo, tenderanno ad esistere più percorsi alternativi, quindi l’algoritmo avrà bisogno di più iterazioni per convergere.
Questo risultato conferma le aspettative e l’incremento di tempo nel nostro caso viene compensato dalla certezza della lunghezza del percorso nell’applicazione con la catena logistica di Hugo Boss.
Correlazione fra il numero di migliori soluzioni ricercate e le performance dell’algoritmo
In questa sezione verrà mostrata la relazione fra il numero di migliori soluzioni ricercate e le performance dell’algoritmo.
L’algoritmo ha eseguito 50 problemi diversi di tipo R-7000-1000.0001 (figura 5.3) e altri 50 di tipo Hugo Boss (figura 5.4) per ogni numero di migliori soluzioni da trovare (H) nell’intervallo da 1 a 15. I risultati vengono mostrati nei seguenti due grafici:
Possiamo notare che con entrambi i problemi affrontati, al variare di H abbiamo un incremento di tempo medio lineare, il che è molto positivo perché garantisce la possibilità di utilizzare l’algoritmo in modo efficiente anche per eventuali problemi futuri che richiedessero un maggiore valore di H e garantisce pieno controllo sui tempi di calcolo al crescere di H.
Per i problemi random e Sottoceneri, i nodi s e t vengono scelti casualmente.
interazione massima in cui l’algoritmo ha trovato un miglioramento della soluzione euristica.
Dalla tabella possiamo notare come la percentuale in cui otteniamo soluzioni ottimali è molto alta nei problemi basati sui grafi Random, supera il 50% con i grafi Sottoceneri, mentre è bassa nei problemi Karaşan.
Il grafo di tipo Hugo Boss nei test ha raggiunto sempre l’ottimalità con tempi di esecuzione estremamente bassi.
La percentuale di scostamento fra LB ed UB è strettamente legata e mostra lo stesso andamento.
Otteniamo una conferma della bontà delle soluzioni euristiche guardando il numero di iterazione in cui l’algoritmo trova un miglioramento della soluzione euristica.
Per i grafi Sottoceneri e Random abbiamo valori compresi fra le 4.56 e le 7.12 iterazioni, con una punta massima di 114. Questo significa che la probabilità di ottenere miglioramenti aumentando il numero di iterazioni è estremamente bassa.
I problemi basati sui grafi Karaşan invece presentano miglioramenti anche con numeri di iterazione più elevati, ma sempre con medie estremamente inferiori rispetto al valore di K.
Questi dati confermano l’intuizione su cui l’algoritmo è basato, ovvero che il ranking dei percorsi nello scenario upper bound è anche un buon ranking in termini di robust deviation.
Osservando i tempi medi di esecuzione notiamo come l’algoritmo sia estremamente veloce.
Nonostante la complessità dei problemi affrontati ed i valori di K molto più elevati di quanto si sceglierebbe per un ambiente di produzione, notiamo che i tempi medi di risoluzione vanno dai 17.39 ai 104.35 secondi e che in particolare i tempi medi per l’ultimo miglioramento vanno dagli 0.0036 ai 1.4196 secondi.
Rilevanza dei risultati con il problema pratico EcoLogTex – Hugo Boss
Dall’analisi della supply chain di Hugo Boss, abbiamo appreso che un prodotto tipico passa attraverso un range di 8-15 fasi di produzione, con un numero di alternative per ogni fasi di 2-9 fabbriche/trasporti.
Al decision maker verranno presentati tipicamente i migliori 10-15 percorsi in base ai pesi che ha inserito, in modo da permettere di confrontare le alternative e di prendere la decisione finale, oppure di modificare i pesi ed ottenere nuove soluzioni.
In questo paragrafo verranno proposti i risultati computazionali ottenuti con un grafo Hugo Boss che indica le condizioni teoriche massime di stress a cui può essere sottoposto l’algoritmo.
In realtà, date le specifiche di Hugo Boss, queste condizioni sono irrealizzabili, ma per completezza della documentazione si sono volute testare ugualmente.
Possiamo notare come nelle condizioni di stress, con il grafo Hugo Boss Hard like, otteniamo risultati assolutamente soddisfacenti:
il test ha presentato sempre soluzioni ottimali, con un tempo medio di esecuzione di soli 10.65 secondi.
Ricordo che queste sono condizioni estreme difficilmente realizzabili, il grafo generato prevede 15 fasi di produzione, 9 alternative per ogni fase, una densità di 0.5 (si prevede che sia 0.2 nella realtà), H=15 e K=300’000.
È stato ideato, sviluppato e testato un nuovo algoritmo in grado di produrre soluzioni euristiche di alta qualità e spesso soluzioni certe certificate in tempi rapidi e con complessità computazionale polinomiale O(Kn3), dove K è un parametro che permette di regolare un trade-off tra qualità della soluzione e tempi di calcolo.
I risultati computazionali confermano l’efficacia dell’approccio proposto e corroborano le intuizioni su cui il progetto è basato.
L’algoritmo è in grado di garantire soluzioni ottimali per la maggior parte dei problemi di tre delle quattro famiglie di grafi studiati ed è in grado di garantire soluzioni euristiche di alta qualità per le rimanenti.
I tempi in cui vengono trovate le soluzioni sono molto bassi e rendono il progetto applicabile al problema di ottimizzazione di una supply chain nel settore tessile, in cui il decision maker si trova a dover confrontare i migliori percorsi su grafi direzionali aciclici a livelli simili a quelli utilizzati in Hugo Boss, che vengono risolti in qualche decimo di secondo utilizzando un normale PC. I test effettuati con le altre famiglie di grafi mostrano come il progetto sia applicabile anche a supply chain più complesse ed a problemi diversi.
La sua valenza scientifica corroborata da nuovi risultati teorici, ha permesso la preparazione di un articolo candidato alla pubblicazione sul Journal Of Applied Operational Research (JAOR), Special Issue on Optimization Methods in Logistics [10].

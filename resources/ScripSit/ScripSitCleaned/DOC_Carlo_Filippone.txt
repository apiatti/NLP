Ringrazio i miei genitori, Carlo e Renata, per avermi sostenuto moralmente e finanziariamente, mostrandomi sempre di essere dalla mia parte, anche nei momenti più impegnativi.
Introduzione Il committente di questo progetto è l’ufficio della cooperazione dello sviluppo della Conferenza dei rettori delle SUP.
Esso si occupa di coordinare tutte le attività di cooperazione e sviluppo svolte presso le SUP e le ASP svizzere, nell’ambito dei quattro mandati principali della legge sulle scuole universitarie professionali svizzere (LSUP):
I file relativi al sito web devono essere memorizzati nella directory docs.
La directory conf contiene invece parametri di configurazione e le credenziali d’ accesso al server MySQL.
Per una descrizione dettagliata dell’organizzazione delle directory è possibile consultare la documentazione tiedu al seguente indirizzo. http://www4.ti-edu.ch/gestione_spazio_web_it.pdf Zend framework 2 Per l’implementazione del progetto è stato scelto di appoggiarsi alle funzionalità del framework Zend 2.Questo a fronte di qualità quali la stabilità, le molteplici librerie a disposizione e la semplicità di installazione su di una piattaforma esistente.
Per permettere la corretta visualizzazione è inoltre necessario specificare la seguente opzione all’interno del file .htaccess nella cartella /docs/public, ove dove risiede il file index.php. + FollowSymlinks
Struttura modulare Il Framework Zend offre totale libertà di scelta allo sviluppatore per quanto riguarda l’organizzazione del codice.
Tuttavia, esso è predisposto per essere strutturato come rappresentato nello schema seguente.
Config In questa cartella devono essere specificate le informazioni di configurazione dell’applicazione.
Vengono ad esempio registrati i moduli presenti, i parametri di accesso ad un eventuale banca dati oltre all’ubicazione dei moduli rispetto alla root directory.
Public Questa cartella ha lo scopo di contenere tutti i documenti pubblicamente accessibili da parte degli utenti, quali script js, fogli di stile css, ed immagini.
In questa directory è infatti presente l’unico file PHP raggiungibile dall’utente, dal quale vengono poi inclusi i componenti necessari quali le librerie Zend e i dispatcher delle richieste.
Il webserver va idealmente configurato per puntare alla suddetta cartella.
Module È la directory contenente i moduli dell’applicazione, nella quale inserire i diversi componenti della stessa.
All’interno di questo direttorio è necessario creare il modulo Application, nel quale si vanno a definire il layout delle pagine e le funzioni condivise tra i vari moduli.
Nell’ambito di questo progetto, ciascun modulo contiene un modello al quale è abbinato un controller e le rispettive view.
Seguendo le specifiche del framework, i singoli moduli possono essere specificati, nel pieno rispetto dell’architettura MVC, come riportato nello schema seguente.
Module Conﬁg Src Model Form Controller Di seguito sono elencate le specifiche della struttura, le quali sono rispettate rigidamente per tutti i moduli presenti nell’applicazione e che dovranno essere rispettate per eventuali moduli sviluppati in futuro.
Config In questa directory è contenuto il file module.config.php, il quale specifica i dettagli del routing per poter chiamare le rispettive azioni del controller.
Le informazioni di configurazione sono specificate rispettando l’esempio riportato qui sotto.
Come è possibile capire dal file di configurazione, il controller si chiamerà NomeModuloController e risiederà nella sottodirectory /controller mentre le view relative alle azioni del controller risiederanno nella sottodirectory /view.
Nel caso non venisse specificata, l’azione di default è index.
Model In questa directory è ubicato il modello dati del rispettivo modulo.
Come detto in precedenza, nello sviluppo di questa applicazione, ogni modulo rappresenta un modello, con relativi controller e views.
La cartella contiene due files, i quali saranno chiamati NomeModulo.php e NomeModuloTable.php. NomeModulo.php è la classe che definisce l’oggetto del quale si vogliono creare istanze, modificarle ed eliminarle.
Ha una struttura di base che si basa sul seguente template.
} NomeModuloTable.php è invece la classe che si occupa di interagire con la banca dati per gestire le operazioni CRUD delle istanze del modello.
In questa classe sono dunque specificati di base un medodo per elencare tutti gli oggetti, uno per estrarne un istanza specifica, uno per creare/modificare un istanza ed uno per eliminare un’istanza del modello.
L’implementazione sfrutta le funzionalità della libreria Zend TableGateway.
} Questo codice di base è stato in seguito adattato per soddisfare le necessità specifiche dei singoli moduli dell’applicazione.
Controller In questa directory risiede il controller del modulo, il quale è specificato nel file NomeModuloController.php. In esso sono specificati i metodi che verranno attivati alla relativa chiamata HTTP, e che contengono a tutti gli effetti la logica dell’applicazione.
Tutti i controller sono basati sul seguente template.
Queste azioni, non specificando una view nei parametri di ritorno, si aspettano che tale sia presente nella directory delle views, con un nome identico a quello dell’azione (senza il suffisso action).
Per fare un esempio, all’azione “deleteAction” sarà dunque associata la view delete.phtml.
Si può notare inoltre l’utilizzo di un componente chiamato nomemoduloForm.
Form Nella directory form sono contenuti i differenti files i quali descrivono in che modo sia possibile effettuare l’inserimento dei dati da parte dell’utente.
In questo progetto ogni modulo ha tipicamente un form, ma niente impedisce lo sviluppatore a realizzarne molteplici in base alle sue esigenze.
Nell’esempio è indicato un input type di tipo hidden per l’ID, oltre as un esempio di textbox e ad un bottone di invio.
Il form può in seguito essere costruito nella view, se non ci sono esigenze particolari di presentazione, richiamando i seguenti metodi.
Dalla view è possibile accedere ai valori ritornati nel controller. Un controller con il valore di ritorno seguente:
permette l’accesso al valore di ‘nomeelemento’ all’interno della view nella variabile $nomeelemento.
Essa può essere utilizzata per mostrare i valori a schermo, accedendo agli attributi del modello nel modo seguente: $nomelemento->nomeattributo.
Architettura dell’applicazione Le necessità del progetto sono di permettere una consultazione da parte dei visitatori del sito, di varie informazioni concernenti gli utenti e alle loro attività.
Gli utenti stessi potranno inoltre modificare parti del loro profilo, aggiungere informazioni e contenuti multimediali.
D’altra parte, sono richieste anche delle operazioni di amministrazione, con funzionalità molto più estese, che permettano la modifica di parametri e informazioni che andranno a influenzare l’intero portale e i suoi contenuti.
È per questo motivo che lo sviluppo del progetto è stato concepito dall’inizio come l’integrazione di due applicazioni totalmente indipendenti ma con alcuni punti in comune:
Struttura della banca dati Il modello dati, sul quale si appoggia l’applicazione, è definito seguendo il diagramma seguente.
Le chiavi primarie sono indicate con una chiave gialla, mentre le chiavi esterne si distinguono per il simbolo di colore rosso.
Applicazione Administration Introduzione L’applicazione Administrator ha l’obiettivo di consentire all’utente amministratore un controllo estensivo della banca dati, permettendo la supervisione di tutte le modifiche effettuate dagli utenti, e offrendo alcune funzionalità d’esportazione dei dati.
Tra i suoi scopi principali ci sono anche la gestione dei diritti dell’utenza e l’approvazione dei nuovi iscritti al portale.
Struttura L’applicazione rispetta le premesse del sistema a blocchi presentato nel capitolo precedente e si compone dei moduli illustrati nello schema seguente.
Configurazione Parametri mySQL La connessione al database si effettua basandosi sui parametri seguenti, memorizzati nella variabile $db sotto forma di array e utilizzati in seguito dall’oggetto TableGateway per l’interazione con la banca dati.
Il nome utente e la password, rispettando le best practices della filosofia Zend, sono specificati in un secondo file, il quale, nel caso di un deployment dell’applicazione su un server Zend, verrebbe escluso dal pacchetto per evitare la perdita di informazioni riservati.
Layout Le esigenze a livello di layout da parte del committente sono minime, considerato anche il fatto che se le prerogative del progetto saranno rispettate, il layout stesso verrà in seguito sottoposto a importanti modifiche da parte di terzi.
È stata quindi introdotta una versione leggermente modificata di Twitter bootstrap, che ha portato a ottenere la presentazione grafica riportata di seguito.
Questo layout, oltre alle informazioni prettamente legate alla presentazione, contiene la seguente riga di codice, la quale permette di richiamare il contenuto delle singole pagine in modo da mostrarle integrate nel layout stesso.
È per questo motivo che in questa directory sono presenti degli esempi di files presi direttamente dal portale di supporto di Zend Framework, i quali fungono da esempio per un eventuale futura traduzione del sito web in altre lingue.
Mandato Il mandato è un’informazione necessaria a definire un oggetto di tipo attività.
Paese Il paese serve a definire il luogo nel quale un’attività è stata svolta.
Esso viene in seguito utilizzato per rappresentare la concentrazione delle stesse nei differenti paesi in via di sviluppo.
Partner La tabella partner serve ad aggiungere informazioni relative alla collaborazione, durante un’attività, con un istituto o ente del luogo in cui essa è stata svolta.
Tipologia Istituto L’oggetto tipologia istituto serve quale tabella d’appoggio per categorizzare gli istituti di appartenenza dei profili.
Al momento solo due tipologie sono considerate all’interno dell’applicazione, ma essa è concepita per supportarne altre qualora fosse necessario.
Documento Il modulo documento permette l’inserimento di files allegati ai profili degli utenti.
Esso ha lo scopo di consentire agli utenti la condivisione di files concernenti le loro attività di sviluppo e cooperazione.
Al filename viene aggiunto un prefisso con l’id del proprietario, al fine di evitare conflitti.
Se l’azione è POST, utilizza i valori ricevuti dal form salva il file su disco e crea un nuovo documento tramite saveDocumento() Utilizza deleteDocumento() per eliminare il documento corrispondente ed elimina il file dal server
Immagine Il modulo immagine permette l’inserimento di fotografie e documenti con estensioni generalmente associate a questi tipi di files.
Esso ha lo scopo di consentire agli utenti l’aggiunta di fotografie concernenti le loro attività di sviluppo e cooperazione.
Al filename viene aggiunto un prefisso con l’id del proprietario, al fine di evitare conflitti.
Attività Il modulo attività permette di definire elementi che descrivano l’operato degli utenti nell’ambito di progetti di sviluppo e cooperazione.
Esso ha lo scopo di aggiungere informazioni ai profili degli utenti, in modo da costituire una sorta di curriculum vitae online.
Le informazioni fornite da questo modulo sono utilizzate per cercare utenti con particolari caratteristiche oltre a comporre la cartina geografica come descritto nel capitolo “Geo”.
Profilo L’elemento profilo, punto centrale dell’applicazione, rappresenta un utente del sistema e ne mostra le sue informazioni personali.
A esso sono associati alcuni dei moduli visti in precedenza, quali Attività, Documento, Immagine e Istituto.
Chiave esterna per l’istituto di appartenenza Le tabelle Immagine, Documento e Attività referenziano profilo tramite la chiave esterna “profilo”.
Effettua una JOIN con istituto per ottenerne il nome Salva il profilo(inserisce o modifica se esistente) Elimina il profilo con l’id corrispondente
La view riassuntiva del modulo profilo è di fatto un aggregazione delle view dei moduli che lo compongono, in modo da poter mostrare una visione di insieme dello stesso.
Le altre views si comportano come quelle viste in precedenza, basandosi sul form relativo al Profilo per la modifica degli attributi.
Linkedin plugin L’attributo “linkedin” dell’elemento profilo viene sfruttato in congiunzione con un apposito plugin [1], per mostrare le informazioni relative al profilo linkedin dell’utente visualizzato.
Lo stesso logo è visibile solo se l’informazione è stata fornita dall’utente stesso.
Applicazione Front Introduzione Questa applicazione rappresenta la parte pubblica del progetto ed è accessibile agli utenti Internet e agli utenti registrati.
Essa ha lo scopo di rendere visibili i profili degli utenti coinvolti nei progetti di sviluppo e cooperazione.
Inoltre, deve poter permettere di visualizzare informazioni relative alla distribuzione delle attività dei membri con una rappresentazione geografica.
Struttura Anche questa applicazione rispetta le premesse del framework Zend ed è organizzata in modo modulare secondo lo schema seguente.
Nonostante la struttura praticamente identica all’applicazione Administrator, le funzionalità dell’applicazione Front sono nella maggior parte dei casi differenti o più limitate.
Ad esempio, per i moduli Istituto, Mandato e Tipologia Istituto, nessun inserimento di dati è possibile da parte dell’utente e gli stessi moduli sono presenti solamente quale supporto ad altri moduli più complessi quali Profilo e Attività.
È per questo motivo che, nella seguente descrizione dei moduli, saranno indicate solamente le caratteristiche che li distinguono dalla loro controparte presente nell’applicazione Administrator.
Configurazione Parametri mySQL La connessione al database si effettua basandosi sui parametri seguenti, memorizzati nella variabile $db sotto forma di array e utilizzati in seguito dall’oggetto TableGateway per l’interazione con la banca dati.
Il nome utente e la password, rispettando le best practices della filosofia Zend, sono specificati in un secondo file, il quale, nel caso di un deployment dell’applicazione su un server Zend, verrebbe escluso dal pacchetto per evitare la perdita di informazioni riservati.
Layout Il layout di quest’applicazione esegue alcuni controlli sui dati per determinare cosa mostrare all’utenza.
Esso è infatti condiviso tra utenza registrata e utenza anonima.
Deve dunque mostrare solamente le informazioni adeguate al tipo di utente che sta navigando nel sito.
Controllo sulle operazioni CRUD Il moduli dell’applicazione si propongono con funzionalità molto simili alle loro controparti di amministrazione.
Una differenza sostanziale riguarda però la limitazione che impedisce ad un utente di effettuare modifiche ad informazioni che non gli appartengono.
} Questa azione estrapola l’identificativo del profilo dalle variabili di sessione, nel quale è contenuto l’array UserDetails, costituito da variabili relative all’utente loggato.
Geo Questo modulo si appoggia ad Attività per ottenere i dati aggregati che rappresentano il numero di attività svolte in un determinato paese.
Controller Il controller si compone di una sola azione fondamentale, il cui comportamento è rappresentato dal diagramma seguente.
View La view Index si occupa dunque di sfruttare le informazioni fornite dal controller al fine di generare una mappa geografica con un layer che indichi il numero di attività svolte in tutti i paesi, sotto forma di cerchi più o meno grandi in base al numero di attività.
Ciò è possibile grazie all’API Google Maps JavaScript V3, il cui dettagli di funzionamento sono illustrati nell’approfondimento.
Profilo Questo componente ricalca in modo quasi identico le funzionalità della sua controparte di amministrazione, applicando però una limitazione a livello di view per non dare accesso all’utente ad operazioni del controller delle quali non avrebbe i diritti di esecuzione.
Sono inoltre presenti nel controller le azioni di login e logout, necessarie per l’autenticazione.
Controller Per verificare se l’utente corrente è proprietario del profilo visualizzato, viene effettuato un test a livello controller, il cui risultato booleano viene assegnato alla variabile isOwner.
Essa viene in seguito utilizzata nella View per visualizzare o meno alcuni controlli.
Nome registerAction() Parametri richiesti Descrizione Verifica se l’utente SWITCH AAI dispone già di un profilo e in caso negativo ne permette la sua registrazione Utilizza alcune informazioni provenienti dalla banca dati per compilare le variabili di sessione “userDetails” le quali descrivono id, nome, cognome e ruolo dell’utente corrente.
Elimina le variabili di sessione “userDetails” effettuando di fatto il logout dell’utente Ritorna tutte le informazioni sul profilo e le tabelle correlate
Approfondimenti In questa sezione si vogliono trattare in dettaglio alcuni meccanismi che sono parte dell’applicazione stessa, al fine di facilitarne la manutenzione e il miglioramento in futuro.
Access Control Lists Per limitare l’accesso ad alcune parti dell’applicazione si è deciso di introdurre un sistema ACL.
Basandomi sulla documentazione di Zend [1] ho quindi realizzato la seguente struttura per l’applicazione Administrator.
In questa lista sono presenti tutti i moduli che per essere eseguiti necessitano diritti di amministrazione.
Si tratta di tutti i moduli possibili, ma è possibile in futuro che altri ruoli verranno introdotti.
Nel file Module dell modulo Application, sono in seguito effettuate le seguenti azioni.
Il metodo initAcl si occupa dunque di estrarre la configurazione dal file Module.acl.roles.php , crea un oggetto di tipo Acl e lo popola con i differenti ruoli e le risorse presenti.
Questa funzionalità [2], utilizzata in modo molto simile nei moduli Immagine e Documento, permette il caricamento di files tramite form, la loro memorizzazione sul filesystem del server e l’inserimento delle informazioni corrispondenti nella banca dati.
Il comportamento dell’applicazione è simile a quello già visto per gli altri moduli, salvo alcune particolarità.
Controller A livello di controller è possibile catturare il file allegato con la seguente istruzione.
A partire dalla versione 2.1 del framework zend è inoltre necessario effettuare un merge tra la componente file e quella non-file del POST.
Le seguenti istruzioni servono invece per specificare una destinazione per il file e per effettuarne il salvataggio.
Ciò è possibile grazie alla Google Maps JavaScript API V3 [5], la quale permette la creazione a runtime di una mappa dinamica grazie a javascript.
Per rappresentare dei cerchi di dimensione proporzionata al numero di attività per paese, viene dapprima costruito un array contenente le coordinate e popolazione(numero di attività).
In seguito, al momento della generazione della mappa, gli elementi dell’array vengono usati per disegnare i cerchi in sovraimpressione rispetto all’immagine del terreno.
A questo proposito è dunque necessario, partendo da un’informazione come il nome di una nazione, poter ottenere le sue coordinate.
Il risultato viene dunque abbinato ad ogni nazione per la quale sono presenti attività, in modo da poter rappresentare le informazioni stesse come spiegato nell’approfondimento seguente.
Analytics L’applicazione Front è stata dotata di un sistema di analisi delle visite, in modo da poter elaborare i dati relativi al flusso percorso da ciascun visitatore all’interno del sito.
In questo modo, oltre a poter visualizzare il numero di visite giornaliere, è possibile delineare il comportamento dei singoli utenti, capire quali sono i punti d’uscita dal sito e renderli più accattivanti.
Per fare ciò è stato registrato un account Google Analytics, il quale esegue il monitor delle pagine visualizzate.
I contenuti statici delle differenti view, nonostante siano al momento solo disponibili in lingua inglese, possono essere facilmente tradotti in altre lingue senza intervenire a livello di programmazione.
Ogni stringa è infatti mostrata passando tramite la seguente funzione.
Cosi facendo, viene interrogato il file di configurazione corrispondente alla lingua impostata e si applica la sostituzione della stringa con la controparte nella lingua desiderata, se disponibile.
Il file di configurazione si compone come di seguito ed è modificabile senza conoscenze di programmazione.
Nel file sono indicate le pagine interessate alla traduzione, il testo originale e il testo sostitutivo nella lingua corrispondente.
Punti aperti Il progetto non può considerarsi terminato nonostante il suo stato attuale presenti una versione stabile e coerente del software.
Sono infatti già state definite alcune esigenze, nel corso degli incontri con il committente, le quali necessitano una realizzazione al fine di rendere fruibile quanto fatto fino ad ora.
Documento allegato ad attività Le attività svolte dagli utenti sono accompagnate comunemente da una scheda descrittiva del progetto.
In accordo con il committente è stata dunque accertata la volontà di permettere agli utenti di allegare la scheda descrittiva in aggiunta alle informazioni già presenti sull’attività.
Didascalia immagini Le immagini della galleria personale hanno alcune informazioni quali il nome dell’autore, l’anno e il paese di provenienza dello scatto, memorizzate all’interno della banca dati.
Questi dati non sono però al momento visualizzati.
La galleria immagini offre la possibilità di far apparire del testo in sovraimpressione nella visualizzazione principale.
Si vuole utilizzare questo spazio per mostrare una didascalia contenente le informazioni legate all’immagine.
Si può però prevedere una crescita nei prossimi mesi fino a raggiungere un minimo di 500 profili registrati.
È per questo motivo che è necessario permettere di filtrare la lista dei profili al fine di raggiungere velocemente le informazioni desiderate.
Migliorie cartina geografica Si è riscontrato come l’attuale visualizzazione geografica non soddisfi le esigenze dell’utente e si dimostri poco pratica.
Esso dovrà essere dinamico e permettere l’apertura di un fumetto contenente dei collegamenti alle pagine degli utenti con attività nel paese selezionato.
Estrazione indirizzi email in Amministrazione Gli utenti con diritti di amministrazione devono poter estrarre delle liste d’indirizzi e-mail al fine di contattare cerchie di utenti.
Conclusioni Il progetto è stato portato avanti adeguandosi alle richieste e alle necessità sopraggiunte in corso d’opera e sempre in accordo tra le parti coinvolte.
Il codice stesso è stato organizzato nel rispetto del paradigma MVC e con commenti in inglese, qualora si sia ritenuta necessaria una spiegazione o per facilitare la lettura del codice stesso.
Design pattern sono stati applicati quando possibile ai fini di ottenere il comportamento desiderato.
Allo stato attuale l’architettura dell’applicazione offre una solida base da cui partire per ampliarne le funzionalità a seconda delle esigenze che insorgeranno col tempo.
L’isolamento modulare delle varie componenti permette l’apporto di modifiche basandosi sullo standard del framework Zend 2 e senza conoscere nel dettaglio l’intera logica dell’applicativo.
Prendendo come esempio qualunque dei moduli implementati è possibile dunque realizzarne di nuovi, i quali si integreranno nell’applicazione senza dover effettuare modifiche al codice già esistente.
Dal punto di vista organizzativo e dell’ingegneria del software, si è voluta seguire la metodologia AGILE con Scrum, la quale è chiaramente più efficace all’interno di un Team di sviluppo, ma che ha permesso di mantenere sempre chiara una visione dell’evoluzione dei lavori, permettendo di rispettare le tempistiche e di tenere traccia delle nuove richieste formulate dal committente.
Ho potuto inoltre confermare come la comunicazione sia fondamentale nella gestione di un progetto e ho appreso quanto sia necessario non sottovalutare i tempi di apprendimento nel caso in cui si sia confrontati con argomenti nuovi.
Nel caso specifico, la volontà di utilizzare il Framework Zend ha pagato in quanto è ora possibile modificare in modo agile l’applicativo, ma l’apprendere a sviluppare sul framework stesso ha richiesto svariate ore di lavoro le quali non erano state adeguatamente previste.
Piani di Lavoro Le diverse fasi del progetto sono state pianificate inizialmente fissandosi come termine la prima settimana di luglio.
L’apprendimento del framework e lo sviluppo di una struttura adeguata ad ospitare le varie funzionalità hanno nettamente influito facendo quindi slittare di qualche settimana tutte le tempistiche.
Durante lo svolgimento del progetto ci si è invece affidati al tool Youtrack che permette la gestione di un progetto con metodologia AGILE.

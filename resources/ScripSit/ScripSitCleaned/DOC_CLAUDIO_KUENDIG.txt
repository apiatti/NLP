La scheda è in grado di elaborare varie modalità di utilizzo e di misurarne le caratteristiche per ogni motore elettrico.
La scheda da la possibilità di osservare tutti i processi intermedi dal microcontrollore fino al motore.
Inserimento di un carattere al posto di un valore Monofase PWM e filtrato Motore monofase Motore trifase Motore a corrente continua Wave Drive PWM Segnale PWM Wave Drive Microstepping filtrato
Modello a V Per la realizzazione del progetto si è utilizzato il Modello a V (Figura 2-1).
Questo modello si integra molto bene con il progetto assegnato, pertanto si è scelto di organizzare la struttura dei capitoli in maniera tale da poter rappresentare tutti i passaggi effettuati.
La tabella definisce gli obbiettivi obbigatori e non obbigatori per gli argomenti dicotomici e dimensionali.
Dicotomico Realizzare un sistema di gestione per ogni singolo motore Padronanza della piattaforma Dimensionale Tensione di utilizzo Potenze in esercizio Norme di sicurezza Alimentazione singola
Avere un solo sistema di gestione per tutti i vari tipi di motori
Il sistema è rappresentato dallo schema di principio seguente.
convertitore AC/DC isolato Separazione galvanica tra scheda di controllo e circuiti di potenza Due entrate analogiche (velocità, corrente,…) Due entrate digitali Due uscite digitali con led di segnaleazione
Separazione galvanica Stadio d’entrata Tensione d’ingesso Potenza nominale HV Potenza nominale LV Tensione DC link Tensione d’uscita AC HV Frequenza d’uscita AC HV Tensione d’uscita DC HV Stadio d’uscita HV Stadio d’uscita LV Protocollo per gestione motori Frequenza PWM Tensione d’ingresso LV Tensione d’uscita AC LV Frequenza d’uscita AC LV Tensione d’uscita DC LV Transistor HV Transistor LV Uscite/TestPoint HV Uscite/TestPoint LV TestPoint controllo Boccole HV Boccole LV Connessione TestPoint controllo Boccole segnali micro
Tre fasi d’uscita DC link e punto tra condensatori (Graetz raddoppiatore) 4 segnali d’uscita 4 PWM parte alta (entrata driver) 4 PWM parte bassa (entrata driver) Boccole di sicurezza 4mm Boccole standard 4mm Gancetti metallici per connettere le sonde Boccole standard 2mm differenziate per colore
Lo schema (Figura 3-3) rappresenta la scheda divisa in blocchi con le loro funzioni e con le interazioni delle varie parti.
La linea tratteggiata rossa rappresenta la separazione galvanica che divide la parte alta tensione da quella di controllo e in bassa tensione.
Alimentazione Interruttore Segnale PWM en A lim i taz e on
Il convertitore deve essere isolato galvanicamente in modo da separare la parte bassa tensione da quella alta tensione (3.1.1).
La scelta ricade su un circuito integrato avente una certificazione per la separazione galvanica con una sufficente potenza di trasferimento.
Questo protegge il microcontrollore da eventuali sovratensioni che lo potrebbero danneggiare.
Permette di disinserire la parte alta tensione in caso di problemi.
Dato che la scheda ha un uso didattico, si è deciso di inserie dei punti di test per eseguire misure sui segnali intermedi.
Per la parte alta tensione, si ha la possibilità di misurare le tre fasi d’uscita la tensione del DC link e il livello tra i due condensatori del raddrizzatore raddoppiatore.
Per queste misurazioni si utilizzano delle boccole di sicurezza onde evitare il contatto con tensioni elevate.
Per la parte bassa tensione, si misurano 4 segnali all’uscita dei due ponti H e la tensione del DC link.
Per la misurazione, si utilizzano delle boccole convenzionali poichè non si hanno tensioni pericolose.
Si ha quindi una distinzione visiva tra bassa e alta tensione.
Si misurano inoltre i segnali PWM prodotti dal microcontrollore.
A questo scopo si utilizzano dei gancetti in metallo per il collegamento delle sonde dell’oscilloscopio.
Disposizione componenti Legenda: 1. 2. 3. 4. 5. 6. 7. 8. 9. 10. 11. Raddrizzatore con raddoppio 230V AC a 650V DC Convertitore 230v a 12V DC XMC1300 Digital Power Control Card Interruttore alta tensione Separazione galvanica Doppio ponte H bassa tensione Uscita bassa tensione Ponte trifase Uscita trifase alta tensione Boccole interfaccia esterna e punti di misura segnali PWM Boccole DC-link e punto centrale alta tensione
In questo sottocapitolo si procede alla scelta dei vari componenti, con l’analisi di possibili soluzioni a confronto.
Si potrà pertanto creare la scheda prototipo con tecnologia THT, utilizzando degli zoccoli per IC.
Per la realizzazione della sceda definitva con ottimizzazione degli spazi si utilizzerà pertanto la tecnologia SMD.
Sono stati valutati tre diversi componenti tutti dello stesso produttore, con caratteristiche differenti, ma tutti utilizzabili:
Potenza in funzione della corrente Dal grafico (Figura 4-8), si nota la caratteristica della potenza rispetto alla corrente di ogni singolo componente.
questo è dovuto alla resistenza del canale tra drain e sorce, in conduzione, che ha una resistenza costante.
Questo non avviene negli IGBT, in quanto hanno una tensione costante ai loro capi, malgrado la corrente possa variare.
Per queste ragioni, nei sistemi di gestione di motori ad alta potenza si utilizzano normalmente IGBT, in quanto sopportano meglio gli spunti di corrente all’avvio dei motori.
Per la nostra applicazione, si è optato per l’utilizzo dei coolMOS in quanto permettono di gestire motori con perdita relativamente bassa.
Gli spunti di corrente iniziali dei motori creano delle potenze di perdita sui coolMOSFET che rimangono sempre al di sotto di quelle sugli IGBT.
La prima utilzza un IC con isolazione galvanica tra i due gate, la seconda utilizza un IC senza isolazione tra i due gate.
Questa soluzione ha però un difetto, in quanto può avere dei picchi di corrente, causati della carica del condensatore che si scarica per pilotare il transistor.
D’altro canto la prima soluzione è molto più costosa ma è anche quella più robusta e affidabile.
Dalle ricerche effettuate si sono tovate due tipologie di circuito che possono fare al caso.
La tecnologia mediante la RF è stata scartata in partenza, in quanto non si trovano componenti con uscita maggiore di 5V.
Si è deciso di utlizzare un componente che abbia al suo interno, nella parte di uscita, un trigger di Schmit per identificare i Driver versatili per motori elettrici
segnali digitali con frequenze di almeno 2MHz. Rispetto al tradizionale fotoaccopiatore, con all’uscita ha un fototransistor, che non raggiunge quelle frequenze, si possono avere problemi di comunicazione tra la parte di controllo e quella alta tensione.
Questo sistema può essere utilizzato dove non c’è la trifase.
Per il calcolo della bobina si è deciso di optare per la frequenza di taglio inferiore ai 50Hz in quanto se superiore non avrebbe nessun effetto di smorzamento della corrente e se troppo inferiore non riuscirebbe a caricare del tutto i condensatori.
Dalla simulazione transiente in TINA della Figura 4-11, inserendo in ingresso una funzione sinusoidale a 50Hz, si ottiene una piccola risonanza sui due condesatori (segnale rosso e segnale blu) che viene subito attenuata dal circuito.
Questo permette di caricare entrambi i condensatori al livello massimo e di mantenere una tensione costante.
Per evitare che i MOSFET si danneggino durante il loro funzionamento, si è calcolato la loro temperatura interna.
Questa deve rimanere al di sotto di quella consentita.
Dall’equazione (4-10) che calcola la temperatua interna del transistor, con una resistenza termica junction-case di 0.25°C/W, della pasta termica di 0.5°C/W e del dissipatore di 1°C/W, si ottiene una temperatura al suo interno minore dei 260°C dati dal fabbricante.
Per la realizzazione della scheda si è usato il tool di sviluppo Altium.
Per la realizzazione della scheda definitiva si utilizzernno componenti SMD per minimizzare gli spazi.
Sono state aggiunte delle resistenze (R203, R204) di pull down per avere un riferimento del gate verso source.
L’interruttore termico posto sul motore è normalmente in conduzione ma se si supera la temperatura di sicurezza questo viene interdetto.
L’interruttore viene posto nella parte di controllo, alimentato dalla bassa tensione, che in mancanza di corrente o di spegnimento, non deve trasmettere il segnale al fotocopiatore.
Per la progettazione della parte alta tensione (Figura 4-15), oltre alle direttive date dal produttore dell’IR2213 nell’aplication note, che prevedevano l’inserimento di una resistenza sul gate (R101, R102) e il diodo (D101) per la carica del condensatore (C101), sono stati aggiunti dei diodi Zenner allo scopo di proteggere i MOSFET e l’integrato da sovratensioni.
Durante i test unitari, sono stati aggiunti successivamente i diodi (D116, D117) posti in parallelo alle resistenze sul gate per velocizzare il tempo di spegnimento dei MOSFET.
Si è in oltre aggiunto un altro connettore con la misura standard, ossia 2x20 con passo da 2.54mm come possibilità di rendere i vari collegamenti della scheda accessibili.
Dal valore calcolato, si sono trovate le misure del dissipatore che corrispondono a 100 x 135 mm.
Per il montaggio sulla scheda si è optato per il posizionamento parallelo alla scheda stessa.
Il dissipatore è stato collegato, oltre ai sei MOSFET, anche il ponte di Greatz.
Per il montaggio dei componenti sono stati previsti dei buchi nella scheda in modo da agevolare il montaggio dei componenti.
Il dissipatore è stato messo a terra per proteggere da eventuali sovratensioni, qualora venisse involontariamente toccato.
Per lo sviluppo si usa la piattaforma DAVE, prodotto dalla stessa Infineon, che oltre alla programmazione in C permette una programmazione grafica basata su moduli preprogrammati.
Per il progetto servono otto uscite PWM provenienti dalla periferica CCU80 del microcontrollore per gestire i ponti H, e la periferica USIC, per comunicare con il PC in modo seriale.
Si evita in questo modo di progettare cablaggi e collegamenti supplementari per la configurazione.
Si utilizza l’APP TIMER che sfrutta il timer 4 per ottenere un calcolo preciso dei tempi in modo da gestire le sinusoidi.
Scrivi su terminale menu di selezione È stato premuto un tasto?
La funzione accetta l’inserimento di un valore tra 1 e 99. Se l’inserimento è corretto ritorna il valore inserito altrimenti ritorna “0”.
Scrivi su terminale menu di selezione È stato premuto un tasto?
Sì Ritorna valore Ritorna 0 È stato premuto carattere?
Se l’inserimento dei vari dati non è corretto, la funzione ritorna il valore “0”.
Se tutti i parametri sono corretti, viene calcolato il valore della tensione per il motore specifico, dalle equazioni per calcolare i motori alternati (Formula 5-11), per calcolare i motori a corrente continua (Formula 5-12) e per calcolare i motori passo-passo (Formula 5-13).
Per la scelta della frequenza, la funzione non richiede il passaggio di nessun valore ma l’inserimento di una frequenza da 1 a 200Hz. Se l’inserimento dei valori non è corretto ritorna “0”.
Scrivi su terminale menu di selezione È stato premuto un tasto?
Sì Ritorna valore Ritorna 0 È stato premuto carattere?
Se è stato inserito il dato di un altro motore ritorna “0”.
Questa funzione inizializza e attiva le varie strutture delle APP del motore in uso (Tabella 5-5).
Diagramma di flusso per la configurazione delle APP
Questa funzione imposta le PWM al duty cycle al 0% o ritorna allo stato attuale del motore.
Diagramma di flusso per la gestione della trifase
La funzione per i motori monofase richiede il passaggio dei parametri di tensione e frequenza.
Questa funzione gestisce un ponte H dove i valori di ogni mezzo ponte sono complementari tra loro.
Diagramma di flusso per la gestione della monofase Driver versatili per motori elettrici
La funzione per i motori a corrente continua richiede il passaggio dei parametri di tensione e il senso di rotazione.
La funzione passa il valore della tensione direttamente al duty cycle.
Questa funzione gestisce un ponte H dove i valori di ogni mezzo ponte sono complementari tra loro.
In caso di inversione di rotazione, vengono invertiti i valori di ogni mezzo ponte.
Questa scelta di azzerare il timer è stata fatta poiché diventerebbe molto complesso creare una funzione per segnali quadrati periodici che possa variare frequenza e ampiezza usando poche risorse.
Per la gestione dei transistor, si è implementato tramite software un tempo morto per garantire che i segnali PWM dei transistor complemntari durante la commutazione rimangano per un breve tempo tutti e due a zero: questo per dare la possibilità al MOSFET in fase di spegnimento di spengersi prima di accendere l’altro.
Se non ci fosse il dead time si arrischia che, al disinserimento del primo MOSFET, questo non abbia il tempo per spegnersi completamente prima dell’accensione di quello opposto, causando un cortocircuito che danneggerebbe i componenti.
Nei parametri del microcntrollore si è imposto un valore di 300ns per essere sicuri che il MOSFET abbia il tempo necessario per essere completamente spento.
Per l’accensione del MOSFET alta tensione superiore, nell’oscillogramma Figura 6-39, si nota una rampa meno accentuata rispetto allo spegnimento.
Per il pilotaggio del gate, si utilizza una resistenza per la carica, mentre per la scarica viene sfruttato il diodo posto in parallelo.
Questa soluzione è stata adottata anche per il MOSFET inferiore.
Comando di inserimento e disinserimento MOSFET superiore alta tensione Durante l’inserimento del MOSFET inferiore si notano delle oscillazioni.
Si nota un ritardo nel tempo di accensione di 86.5ns, questo è dovuto al tempo di reazione del IR2011 e del MOSFET.
Nella parte di spegnimento, raffigurata con un cerchio rosso, ci troviamo nella situazione Dead Time.
Per l’interfaccia grafica si utilizza un normale terminale da PC. Come primo menu (Figura 7-43) appare la richiesta di scegliere il tipo di motore da testare.
Menu principale Dopo la scelta del tipo di motore, viene richiesta la percentuale di alimentazione che si vuole dare al motore rispetto alla tensione nominale del DC link.
Menu motore trifase Dopo aver configurato la tensione di alimentazione verrà richiesto (non per il motore a corrente continua) (differenza tra Figura 7-44 e Figura 7-45), di inserire la frequenza di rotazione del motore.
Dopo aver sostituito i valori richiesti, il microcontrollore manda un messaggio che i parametri inseriti sono conformi e prosegue con la configurazione.
Se i parametri non sono idonei, il microcontrollore reimposta il valore precedente.
Se durante la configurazione venissero (Figura 7-47) inseriti dei valori fuori dal range di utilizzo, viene ritornato un messaggio di errore che impone il reinserimento del parametro.
Inserimento di un carattere al posto di un valore
Per verificare che sul motore si trovi realmente la sinusoide desiderata, si utilizza l’oscilloscopio con la funzione matematica “sottrazione”:
ciò significa che viene misurata la differenza tra i due segnali.
Si ottiene il segnale rosa, che rappresenta perfettamente la sinusoide.
Vista la regolarità della sinusoide rosa e dalla misura eseguita di due segnali sfasati di 180°, si deduce che il sistema funziona correttamente.
Nell’oscillogramma di Figura 8-50, vengono rappresentati le due sunusoidi complementari, mentre nel secondo oscillogramma vengono rappresentati i segnali sul motore monofase.
Per il test di validazione della trifase si sono utilizzati tre filtri con riferimento a massa, imponendo una frequenza di 10Hz otteniamo tre sinusoidi pulite con uno sfasamento reciproco di 120°.
Nell’oscillogramma di Figura 8-51, vengono rappresentati le tre sunusoidi filtrate, mentre nel secondo oscillogramma vengono rappresentati i segnali sul motore trifase.
Per verificare il corretto funzionamento della gestione dei motori a corrente continua si è scelto di visualizzare in dettaglio il segnale PWM, in quando filtrato otterremmo solo un segnale costante.
Questi valori sono corretti e, in conformità al capitolo 6.1 abbiamo imposto al microcontrollore un Dead Time di 300ns per evitare dei cortocircuiti sui MOSFET.
Nell’oscillogramma di Figura 8-52, vengono rappresentati i segnali sui MOSFET del ponte H. Nel secondo oscillogramma vengono rappresentati i segnali sul motore.
La gestione dei motori passo-passo impone dei segnali fissi al valore massimo per un determinato periodo.
questa situazione crea dei problemi con i driver che possono non funzionare correttamente.
Per evitare di avere un segnale non conforme è stato imposto un valore massimo del Duty Cylce di 99%:
Questo risulta difficile da rappresentare tramite un filtro passa basso, in quanto i cambiamenti di stato vengono “ammortizzati” dalla scarica del condensatore del filtro.
Questa modalità utilizza lo stesso principio della modalità Wave Drive ma al posto dei segnali quadrati si hanno delle mezze semi-onde.
Avendo a disposizione un microcontrollore con poca potenza di calcolo, durante la generazione di segnali a frequenze elevate, si ha un segnale non perfettamente pulito.
Questo ha comunque permesso di mostrare il funzionamento del sistema.
Nome attività Pianificazione delle attività Definizione delle specifiche Scelta componenti Realizzazione della scheda Montaggio scheda Studio piattaforma DAVE Progettazione del Firmware Test unitari Integrazione Test di validazione Documentazione
Nome attività Pianificazione delle attività Definizione delle specifiche Scelta componenti Realizzazione della scheda Montaggio scheda Studio piattaforma DAVE Progettazione del Firmware Test unitari Integrazione Test di validazione Documentazione

L'algoritmo I dati di input 1.7.1 1.7.2 1.7.3 1.7.4 Localizzazione delle strutture turistiche Il numero di veicoli Il punto di stoccaggio Peso relativo per il tempo totale di percorrenza
L'iniziativa è tesa alla salvaguardia del mare indonesiano che, a causa dello sviluppo di numerose strutture turistiche, è sempre più soggetto ad inquinamento dovuto agli scarti della ristorazione.
Tale inquinamento minaccia direttamente, non solo la vita della popolazione locale, ma partecipa al pericoloso cambiamento climatico che sta coinvolgendo l'intero pianeta negli ultimi decenni.
In particolare verrà migliorato l'algoritmo di instradamento alla base del sistema, che potrà produrre una migliore distribuzione dei periodi di recupero dell'olio, massimizzandone la quantità raccolta di volta in volta.
Poiché l'algoritmo fornice una soluzione minimizzando solo la durata totale dei viaggi e la deviazione standard delle durate, esiste la possibilità che la raccolta venga effettuata in più giorni consecutivi per lo stesso cliente, pianificando dei viaggi quasi a vuoto pervia della ridotta quantità di olio prodotta nel frattempo.
Infine verrà introdotta la possibilità per le varie attività di ristorazione, di indicare una preferenza sul giorno in cui tale raccolta dovrà avvenire e ciò implicherà l'aggiunta di un nuovo vincolo e la revisione di parte dell'algoritmo.
A prova del miglioramento sono riportati i dati sulle caratteristiche della soluzione a confronto tra la vecchia e la nuova versione dell'algoritmo.
Il ruolo dello studente sarà di migliorare i correnti algoritmi, in modo da renderli più realistici e reattivi alle mutevoli condizioni di traffico sull’isola, con l’introduzione di concetti stocastici.
Compiti Lo studente dovrà ideare e sviluppare nuove funzionalità per migliorare la pianificazione prodotta dall'algoritmo rendendo le soluzioni prodotte, più adatte al contesto lavorativo reale in cui devono essere sfruttate.
Introduzione Nel 2010 Caritas Suisse, in collaborazione con myclimate, Kuoni Group e IDSIA, ha iniziato un progetto di collaborazione per far fronte all'inquinamento creato dalle numerose strutture turistiche dell'isola di Bali, avviando un'attività di recupero e riciclaggio di olio esausto proveniente dalle cucine di queste strutture.
Al momento hanno aderito 308 impianti turistici, tra hotel e ristoranti, sparsi perlopiù nell'area urbana della capitale Denpasar.
Per effettuare la raccolta dell'olio sono stati messi a disposizione diversi veicoli che potranno trasportare una quantità relativamente limitata di olio ognuno.
Inoltre è stato scelto un punto di stoccaggio in posizione strategica, da cui partirà ogni veicolo e vi ritornerà per depositare il materiale raccolto.
Per organizzare tale attività è stato messo a punto un software dedicato alla pianificazione della raccolta dell'olio.
Inoltre vuole migliorare la vita della popolazione locale, svantaggiata, creando una forma di impiego, e soprattutto impedendo quanto possibile l'inquinamento dei mari indonesiani.
Questo progetto prevede due fasi di elaborazione, una prima analisi approfondita dell'algoritmo esistente, sviluppato dai ricercatori ISDIA, così da capirne il funzionamento nei particolari e individuarne i punti su cui intervenire successivamente con le modifiche.
Saranno analizzati i dati prodotti dalla soluzione iniziale e i risultati verranno confrontati con quelli della nuova soluzione, inizialmente per valutarne i punti deboli, successivamente per valutarne i miglioramenti.
Le attività locali aderenti al progetto umanitario/ambientale, sono al momento 308, tra ristoranti e hotel.
Ognuna di queste ha dato indicazioni sulla quantità media di olio esausto accumulato ogni settimana e di ognuna si conosce l'ubicazione esatta in termini di coordinate geografiche.
Tra tre punti di stoccaggio inizialmente valutati ne è stato individuato uno, con posizione strategica, dove l'olio esausto raccolto verrà depositato e lavorato.
La raccolta viene effettuata porta a porta per mezzo di veicoli con la capacità di 18 contenitori da 25 litri, per un totale di 450 litri.
Dato che verrà tenuto conto solo dei giorni lavorativi, di due settimane, la pianificazione occuperà solo dieci giorni.
In inglese Traveling Salesman Problem (TSP), il problema del commesso viaggiatore è alla base della maggior parte dei modelli di instradamento di veicoli.
Va tenuto conto della capacità dei veicoli e della loro numerosità, oltre al fatto che la raccolta non avviene in un solo giorno, ma distribuita su finestre temporali nell'arco di 10 giorni lavorativi.
Questo modello rappresenta in gran parte il problema.
Nel modello descritto da Blaż Zmazek, vengono considerate finestre temporali di alcune ore, parte in un singolo giorno, durante ognuna delle quali un nodo deve essere rigorosamente servito.
Si tratta di finestre temporali hard o rigide, infatti se il nodo viene raggiunto in anticipo rispetto all'inizio della finestra temporale a lui dedicata, l'operazione di raccolta non può avere subito inizio e viene introdotta un'attesa per rispettarne i limiti.
Nel caso in cui il nodo venga raggiunto oltre la fine della finestra temporale, la soluzione viene considerata non accettabile.
Inoltre nel modello proposto da Blaż Zmazek viene considerato un solo giorno, mentre nel nostro modello si dovrà ovviare alla pianificazione di più giorni lavorativi, durante i quale dovrà avvenire la visita di tutti i nodi.
Ora che è stato definito il problema nel dettaglio, va individuata la funzione obiettivo che ha il compito di pilotare la soluzione secondo determinati fattori di qualità.
la distanza totale percorsa da tutti i veicoli nell'intero orizzonte di pianificazione deve essere minore possibile i viaggi devono essere di lunghezza omogenea per distribuire equamente il carico di lavoro tra gli autisti, vicolo richiesto da Caritas
La soluzione è dettata dalla combinazione di entrambe i fattori.
i pesi relativi assegnati rispettivamente alla distanza totale percorsa e a
Il primo livello prevede l'utilizzo della Ricerca Locale come euristica di assegnazione ottimale dei clienti ai veicoli e ai giorni lavorativi.
Il secondo livello invece è utilizzato un altro tipo di meccanismo per ottimizzare i percorsi di differenti veicoli e giorni.
In questo caso è stato utilizzato il cosiddetto Iterative Improvement che consiste nel reiterare la ricerca sulla soluzione corrente, solo se questa ha migliorato la funzione obiettivo.
In altre parole viene reiterata la Ricerca Locale in successione su ogni soluzione vicina che migliora la funzione obiettivo, finché non ne viene incontrata una non migliorante.
Ad ogni iterazione della Ricerca Locale viene effettuata l'ottimizzazione dei singoli percorsi di ogni veicolo per ogni giorno lavorativo.
Tra i parametri di input del software, l'utente può indicare la profondità della ricerca esaustiva.
Ossia la lunghezza massima, in numero di tratte, del percorso di un veicolo, entro la quale è possibile utilizzare la ricerca esaustiva per ottimizzarlo.
Superata questa soglia, entrerà in gioco l'euristica costruttiva, più rapida nella ricerca della soluzione.
Risolvendo il sottoproblema di instradamento viene rilassato il vincolo della capacità dei veicoli, poiché il percorso, se necessario, viene spezzato in più sottopercorsi con partenza e arrivo sempre allo
In pratica al veicolo viene assegnato un percorso attraverso un certo numero di nodi.
Se durante il tragitto il veicolo viene riempito raggiungendo la sua capacità massima, questo deve rientrare al punto di stoccaggio per essere svuotato, per poi riprendere il tragitto da dove era stato interrotto.
Un esempio è riportato in modo schematico in Figura 5.
La soluzione di partenza per questo progetto è già in grado di fornire un buon risultato in termini di percorsi, partendo da dati che descrivono i vari punti di raccolta e di smaltimento, in particolare le loro coordinate geografiche e la quantità di olio esausto accumulato settimanalmente.
Tali dati vengono elaborati insieme ai parametri di input, per ottenere tramite un'euristica una buona distribuzione del traffico.
I dati di input Localizzazione delle strutture turistiche
I dati relativi alle varie strutture turistiche sono stati raccolti da Caritas e Kuoni, per consentire lo sviluppo di questo algoritmo.
locations.txt distances.txt Nel file locations.txt sono riportate le coordinate geografiche, latitudine e longitudine, e la quantità di olio prodotta settimanalmente, oltre ad altri dati non utilizzati dall'algoritmo, come il codice identificativo ID, il nome dell'attività, il tipo di attività, il numero di camere messe a disposizione, il numero di impiegati e il numero i ristoranti.
Nel file distances.txt sono riportate invece le distanze reali, su strada, tra le varie attività.
L'algoritmo provvede al calcolo di tale distanza se non dovesse essere indicata, sfruttando le coordinare geografiche.
Per garantirne la durabilità è prevista la possibilità di non usarli sempre tutti, in modo da poter effettuare una rotazione e garantirne una maggiore durabilità.
Si ha quindi la possibilità di valutare la pianificazione considerando da 4 a 7 veicoli.
Inizialmente erano previsti tre punti di stoccaggio, ma nel corso del progetto precedente che ha dato alla luce questa soluzione di partenza, ne è stato selezionato uno, con posizione strategica, che permette di ottimizzare al meglio la distanza totale percorsa.
Il valore di tale parametro non ha quindi effetto sull'algoritmo, ma non è stato rimosso per dare spazio ad eventuali sviluppi futuri.
Se il fattore è uguale a 1 si darà peso massimo al tempo totale di percorrenza, mentre se si avvicina a 0 si darà più importanza alla deviazione standard della lunghezza dei vari viaggi.
Durante la fase preliminare di analisi del progetto iniziale, sono state individuate due possibili migliorie, di grande impatto sull'utilizzo pratico dell'algoritmo trattato.
La prima consiste nel dare la possibilità ai gestori delle strutture turistiche di indicare uno o più giorni della settimana lavorativa, durante i quali effettuare il prelievo dell'olio esausto.
La seconda riguarda l'ottimizzazione dell'efficacia della raccolta, in modo da massimizzare la quantità di olio raccoglibile ad ogni visita, per evitare che questo rimanga troppo a lungo depositato nei locali delle attività di ristorazione.
L'idea alla base di questo sviluppo è che il cliente finale possa dare disposizione di effettuare la presa dell'olio esausto durante un determinato giorno della settimana.
Tale vincolo invece implicherà l'assegnazione prioritaria ai giorni di preferenza, e quando questi saranno occupati, se necessario ne verranno scelti liberamente altri.
L'algoritmo dovrà continuare ad impedire che più visite vengano effettuate nello stesso giorno per lo stesso cliente.
Il dato è stato serializzato nel file locations.txt, come una lista di giorni riportati in formato numerico.
Criteri di assegnazione del giorno di raccolta
Al fine di massimizzare l'efficacia dell'assegnazione in relazione alla qualità della soluzione, sono stati in primo luogo individuati i criteri necessari alla scelta del giorno.
Se il cliente non ha dato disposizioni a riguardo, il giorno di presa viene scelto secondo altri criteri.
Altrimenti la funzione tenterà di assegnarlo secondo le disposizioni.
La priorità nell'assegnazione viene data ai casi in cui il giorno di preferenza è indicato.
Inoltre se il cliente ha indicato un numero di giorni di preferenza maggiore di quello strettamente necessario per la raccolta pianificata, ne devono essere scelti solo alcuni, mentre quelli in più rimarranno liberi.
Se la quantità supera un certo limite, sarà necessario effettuare più viaggi nell'arco delle due settimane pianificate.
Infine il fatto di dover interrompere le proprie attività più volte nell'arco della giornata può risultare fastidioso per il cliente.
Per questo motivo la raccolta dell'olio non può essere effettuata più volte nello stesso giorno per lo stesso cliente.
Perché i giorni di preferenza vengano trattati con priorità maggiore rispetto agli altri, devono essere già assegnati durante l'inizializzazione della soluzione di partenza. Successivamente un vincolo farà in modo che questi non possano essere sostituiti se non con altri indicati come preferiti, durante l'ottimizzazione della soluzione.
Di seguito è riportato la funzione con che effettua l'assegnazione dei clienti ai giorni di raccolta, oltre ai veicoli necessari e quindi ai percorsi.
La funzione effettua in generale l'assegnamento di tutti i clienti ai giorni dell'orizzonte di pianificazione necessari, definendo una prima forma per tutti i viaggi, con i rispettivi veicoli.
In particolare la funzione effettua diverse iterazioni sull'intero orizzonte di pianificazione, assegnando man mano i vari clienti e i veicoli ai vari giorni, finché non viene coperto il recupero di tutto l'olio.
Se la preferenza è indicata, la funzione fa si che le visite vengano effettuate in tali giorni, fino ad esaurimento.
Se i giorni preferiti dovessero essere già coperti per il cliente, l'assegnazione continua ad altri giorni non preferiti.
Se non è indicata alcuna preferenza, viene individuato il giorno migliore per la visita, in base alla produzione giornaliera di olio in relazione ai giorni di visita già assegnati.
Durante l'esecuzione della ricerca locale, l'algoritmo definisce tutti i riassegnamenti, o mosse, possibili per clienti, da un veicolo ad un altro o da un giorno ad un altro.
Per fare in modo che non venga persa la priorità dell'assegnazione del giorno di preferenza, è stato introdotto un vincolo che impedisce lo spostamento da un giorno preferito ad uno non preferito.
La nuova forma è riportata di seguito in pseudo codice.
Un'altra caratteristica importante del problema trattato in questo progetto è che lo scopo della visita dei vari nodi non è la consegna ma il recupero di materiale e che la quantità di materiale recuperabile da ogni nodo cresce progressivamente col tempo.
Ad esempio nel nostro caso, i vari clienti hanno dato indicazioni sulla quantità media di olio esausto accumulato ogni settimana.
Ipotizzando che la capacità massima del veicolo sia di 200 litri, si dovranno effettuare due visite, poiché in due settimane la quantità totale raggiunge i 280 litri.
Poiché però la ricerca locale non permette lo spostamento in blocco dei giorni assegnati, ma solamente uno alla volta, si deve ricorrere ad una strategia che permetta l'assegnazione ottimale anche in modo sequenziale.
La soluzione migliore è stata individuata nella rielaborazione della funzione obiettivo, introducendo un indice di qualità della distribuzione ottimale delle visite per ogni cliente.
Fino ad ora per la valutazione erano considerati solo il tempo totale di percorrenza di tutti i viaggi nella pianificazione e la deviazione standard della durata dei viaggi.
Si vuole quindi introdurre anche l'indice di qualità relativo alla distribuzione dei giorni di raccolta per ogni cliente.
Per il calcolo della qualità della distribuzione si è deciso di utilizzare una strategia con punti di penalità, che verranno assegnati ai giorni pianificati in modo non ottimale.
Migliore sarà la distribuzione delle visite, minore sarà il punteggio finale.
Al contrario, se ad esempio alcune visite pur potendo essere distanti tra loro di più giorni e invece sono schedulate in giorni adiacenti, la distribuzione sarà peggiore, quindi il punteggio sarà più alto.
Il punteggio da assegnare al giorno da valutare è stato ponderato in modo tale da avere un valore molto alto se i giorni adiacenti sono assegnati e via via minore per i giorni assegnati più distanti.
In Figura 7 è rappresentato come effettivamente viene distribuito il punteggio, in questo caso per il venerdì della prima settimana.
Ipotizzando che la pianificazione delle visite per un cliente sia come rappresentata in Figura 8, dove il valore 1 corrisponde all'assegnamento, mentre 0 indica che il giorno è libero, il calcolo del punteggio per il venerdì della prima settimana avviene come segue.
Sommando le penalità di tutti e tre i giorni assegnati nell'esempio di Figura 9, si ottiene la qualità della pianificazione della raccolta dell'olio per il cliente interessato.
La somma delle penalità di ogni pianificazione per ogni cliente indicherà la qualità dell'intera soluzione.
La qualità migliore possibile è indicata ovviamente dalla penalità nulla.
Analisi di sensitività dei pesi relativi della funzione obiettivo
Per poter integrare correttamente la nuova funzione obiettivo per la distribuzione ottimale dei giorni di visita, alla funzione obiettivo originale, è stato svolto uno studio di sensitività dei pesi relativi di quest'ultima.
Attraverso diverse prove significative si è individuato il fattore di moltiplicazione per la nuova funzione obiettivo, che permette ad essa di dare il proprio contributo nella funzione obiettivo finale, senza annullare i pesi di quella originale.
Moltiplicando il risultato della nuova funzione obiettivo sarà quindi possibile integrare quest'ultima con quella originale, sfruttando lo stesso principio di variazione di peso utilizzato con il tempo totale di percorrenza.
i pesi relativi assegnati rispettivamente alla distanza totale percorsa, alla e alla qualità di distribuzione dei giorni di visita
Tutti i test sono stati eseguiti con le stesse condizioni di ambiente, ossia utilizzando lo stesso set di dati, lo stesso valore di inizializzazione delle funzioni di randomizzazione e le stesse condizioni della macchina.
Il set di dati proviene dal contesto reale del progetto e comprende le informazioni relative alle varie strutture turistiche, tra cui l'ubicazione in termini di coordinate geografiche, la distanza reale (stradale) tra di esse e l'indicazione del tasso di produzione di olio esausto di ognuna.
I test effettuati vogliono definire il comportamento dell'algoritmo al variare di ogni possibile parametro di input, oltre all'incidenza della funzione obiettivo nella definizione della soluzione.
Ove possibile, sono messe a confronto la soluzione iniziale e quella risultante da questo progetto.
Per alcuni test è stato possibile effettuare confronto tra i risultati ottenuti tramite l'algoritmo originale e quelli ottenuti tramite il nuovo algoritmo.
Si vuole infine fa notare che per rendere più leggibili i grafici riportati di seguito, sono state utilizzate due diverse scale per l'asse delle ordinate, visibili ai due lati dell'area del grafico.
Il peso relativo del tempo di percorrenza totale è rappresentato da nella funzione obiettivo.
Il parametro è fatto variare dal massimo valore 1 a valori tendenti allo 0, riducendo quindi man mano il peso della variabile nella funzione obiettivo.
Con questo test si vuole verificare l'efficacia della ricerca esaustiva oltre all'effetto ha la variazione della sua profondità nella qualità della soluzione trovata.
La profondità, che rappresenta di fatto la lunghezza in tratte di un percorso, è stata variata da 5 a 9. Si voglia ricordare che la ricerca esaustiva viene utilizzata per ottimizzare i singoli percorsi, e che oltre il limite di profondità, l'algoritmo utilizza un'euristica costruttiva al suo posto.
Dai risultati di questo testo si può osservare come entrambe le soluzione trovate dalle due versioni dell'algoritmo abbiamo un buon andamento della qualità, crescente all'aumentare della profondità della ricerca esaustiva.
Dai risultati di questo test è subito visibile sia per i dati raccolti col vecchio algoritmo che con quelli raccolti con il nuovo, come l'aumentare del numero di veicoli porti al decremento della lunghezza dei singoli viaggi, osservabile dalla diminuzione della distanza massima e minima percorsa.
Va notata però una differenza tra i dati raccolti con i due algoritmi, in particolare riguardo alla distanza minima.
Dal grafico in Figura 13, relativo al vecchio algoritmo, si può osservare l'andamento altalenante della distanza minima, che si annulla in due punti distinti, indicando l'esistenza di un viaggio con lunghezza 0 nella soluzione.
Anche nella Figura 14 è osservabile lo stesso fenomeno, ma in questo caso l'annullamento della distanza minima avviene solo con un alto numero di veicoli.
Con questo test si vuole verificare l'effetto della variazione del peso della qualità di distribuzione dei giorni di raccolta.
Tale peso è rappresentato nella funzione obiettivo tramite la variabile .
Il parametro è fatto variare dal massimo valore 1 a valori tendenti allo 0, riducendo quindi man mano il peso della variabile nella funzione obiettivo.
Va notato invece la diminuzione della distanza minima, l'aumento della distanza massima e la diminuzione del tempo totale.
Tale variazione è dovuta all'accrescimento dell'importanza degli altri parametri della funzione obiettivo, al diminuire di , ed è perfettamente in linea con le aspettative.
Importante notare il valore aggiunto dato alla qualità delle soluzioni prodotte, osservabile confrontando la qualità di distribuzione della soluzione, ottenuta da entrambe le soluzioni utilizzando i pesi massimi per durata totale dei viaggi, deviazione standard delle durate e qualità della distribuzione.
Ovviamente con l'algoritmo originale non è dato peso alla distribuzione ottimale dei giorni di raccolta, non essendo parte della funzione obiettivo.
Con questo test si vuole valutare l'impatto che ha sulla definizione della soluzione, l'indicazione di uno o più giorni di preferenza per la raccolta dell'olio da parte delle strutture turistiche.
Dai dati estratti è immediatamente visibile il progressivo calo della qualità generale della soluzione, all'aumentare del numero di clienti che indicano il giorno di preferenza.
Il risultato è in linea con l'aspettativa, poiché il giorno di preferenza rappresenta un vincolo per l'algoritmo e di conseguenza introduce un limite alla qualità della soluzione.
Gli indici mantengono comunque un buon valore garantendo una buona qualità alle soluzioni trovate.
L'intero lavoro è stato svolto utilizzando una macchina con sistema operativo Microsoft Windows 7 Come ambiente di sviluppo è stato utilizzato Eclipse IDE for C/C++ Developers nella versione Luna Release (4.4.0).
Eclipse permette di utilizzare il compilatore gcc, che supporta la creazione di array utilizzando variabili non costanti per definirne la dimensione, proprietà di C99. Ciò ha determinato la scelta, poiché nella soluzione del progetto è fatto uso di questo metodo di creazione degli array.
Con lo svolgimento di questo progetto sono stati raggiunti gli obiettivi preposti, introducendo con successo due nuove importanti funzionalità, quali, la possibilità per le attrezzature turistiche aderenti, di poter dare disposizioni riguardo la pianificazione della raccolta dell'olio indicando dei giorni di preferenza, e l'ottimizzazione della distribuzione delle visite, per uniformare il flusso di olio esausto verso il punto di stoccaggio, massimizzandone la quantità raccolta di volta in volta.
Lo studio approfondito e la successiva ottimizzazione dell'algoritmo trattato con questo progetto ha portato ad un sensibile miglioramento della qualità delle soluzioni definibili da esso, soprattutto in termini di praticità nella loro applicazione al contesto reale per cui sono preposte.

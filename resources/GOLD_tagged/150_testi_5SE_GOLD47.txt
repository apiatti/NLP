****5testo48	*SCUOLA=SE	*ANNOSCOLASTICO=5	*CLASSE=BI51	*TERRITORIO=BI	*LINGUA=AL	*GENERE=M
James	NPR	James
e	CON	e
Joy	NPR	Joy
Un	giorno	ADV	un	giorno	pl1
io	PRO:pers	io
e	CON	e
mio	DET:poss	mio
stavamo	VER:fin	stare
giocando	VER:geru	giocare
a	PRE	a
pichiarci	VER:infi:cli	picchiarsi	e1
,	PUN	,
alinizio	ADV	all’inizio	e1
mi	CLI	mi
bateva	VER:fin	battere	e1
sempre	ADV	sempre
e	CON	e
io	PRO:pers	io
credevo	VER:fin	credere
che	CHE:con	che
Joy	NPR	Joy
fosse	VER:fin	essere
piú	ADV	più
forte	ADJ	forte
di	PRE	di
mé	PRO:pers	me	e1
comunque	ADV	comunque
continuavo	VER:fin	continuare
a	PRE	a
batermi	VER:infi:cli	battersi	e1
contro	di	PRE	contro	di	pl1
lui	PRO:pers	lui
anche	se	CON	anche	se	pl1	
perdevo	VER:fin	perdere
sempre	ADV	sempre
mi	CLI	mi
divertivo	VER:fin	divertirsi
giocare	VER:infi	giocare
con	PRE	con
lui	PRO:pers	lui
,	PUN	,
perche	CON	perché	e1
io	PRO:pers	io
sapevo	VER:fin	sapere
che	CHE:con	che
Joy	NPR	Joy
essendo	AUX:geru	essere
piú	ADV	più
grande	ADJ	grande
di	PRE	di
mé	PRO:pers	me	e1
mi	CLI	mi
avrebbe	AUX:fin	avere
battuto	VER:ppast	battere
,	PUN	,
finche	CON	finché	e1
non	NEG	non
sarei	AUX:fin	essere
diventato	VER:ppast	diventare
piú	ADV	più
grande	ADJ	grande
.	SENT	.
Ma	CON	ma
grazie	a	PRE	grazie	a	pl1
mio	DET:poss	mio
fratello	NOUN	fratello
che	CHE:rel	che
un	giorno	ADV	un	giorno	pl1
mi	CLI	mi
disse	VER:fin	dire
:	PUN	:
?	SENT	?
James	NPR	James
una	ART	una
cosa	NOUN	cosa
che	CHE:rel	che
mi	CLI	mi
piace	VER:fin	piacere
di	PRE	di
té	PRO:pers	te	e1
é	VER:fin	essere
che	CHE:con	che
non	NEG	non
ti	CLI	ti
arendi	VER:fin	arrendersi	e1
mai	ADV	mai
e	CON	e
per	PRE	per
quello	PRO:demo	quello
che	CHE:con	che
un	giorno	ADV	un	giorno	pl1
mi	CLI	mi
baterai	VER:fin	battere	e1
.	SENT	.
Grazie	a	PRE	grazie	a	pl1
quelle	DET:demo	quello
parole	NOUN	parole
mi	CLI	mi
dette_(diede)	VER:fin	dare	e2
ancora	ADV	ancora
piú	ADV	più
forza	NOUN	forza
,	PUN	,
e	CON	e
da	PRE	da
li	ADV	lì	e1
in	poi	ADV	in	poi	pl1
iniziai	VER:fin	iniziare
ad	PRE	a
alenarmi	VER:infi:cli	allenarsi	e1
contro	PRE	contro
i	ART	il
mie_(miei)	DET:poss	mio	e2
pupazzi	NOUN	pupazzo
.	SENT	.
Finche	CON	finché	e1
un	giorno	ADV	un	giorno	pl1
gli	CLI	gli
tirai	VER:fin	tirare
un	ART	un
pugno	NOUN	pugno
ben	ADV	ben
sesto	ADJ	sesto
(	PUN	(
glilo	PRO:pers:cli	glielo	e1
tirai	VER:fin	tirare
perché	CON	perché
mi	CLI	mi
disse	VER:fin	dire
che	CHE:con	che
essendo	AUX:geru	essere
piccolo	ADJ	piccolo
non	NEG	non
lavrei_(l’avrei)	AUX:fin	avere	e1
mai	ADV	mai
batutto	VER:ppast	battere	e1
)	PUN	)
e	CON	e
lui	PRO:pers	lui
cadde	VER:fin	cadere
giú	ADV	giù
per	terra	ADV	per	terra	pl1
.	SENT	.
La	ART	la
lezione	NOUN	lezione
che	CHE:rel	che
ho	AUX:fin	avere
imparato	VER:ppast	imparare
é	AUX:fin	essere
stata	VER:ppast	stata
la	ART	la
seguente	ADJ	seguente
:	PUN	:
-	1	NUM	1
Mai	ADV	mai
arendersi	VER:fin	arrendersi	e1
anche	se	CON	anche	se	pl1
l’	ART	lo
aversario	NOUN	avversario	e1
ti	CLI	ti
butta	VER:fin	buttare
giú	di	morale	ADJ	giù	di	morale	pl1
.	SENT	.
−	PUN	−
-	2	NUM	2
Non	NEG	non
Sempre	ADV	sempre	e1
chi	PRO:indef	chi
è	VER:fin	essere
piú	ADV	più
grande	ADJ	grande
vince	VER:fin	vincere
ma	CON	ma
avolte	ADV	a	volte	e1	pl1
i	ART	il
piú	ADV	più
piccoli	ADJ	piccolo
possono	VER2:fin	potere
vincere	VER:infi	vincere
.	SENT	.
Mtendo	VER:geru	mettere	e1
tutto	PRO:indef	tutto
asieme	ADV	assieme	e1
anche	ADV	anche
i	ART	il
piú	ADV	più
piccoli	ADJ	piccolo
posono	VER:2fin	potere	e1
vincere	VER:infi	vincere
(	PUN	(
anche	se	CON	anche	se	pl1
Joy	NPR	Joy
ha	VER:fin	avere
16	DET:num	16
anni	NOUN	anno
e	CON	e
mezzo	NOUN	mezzo
)	PUN	)
.	SENT	.
Gemima	NPR	Gemima
e	CON	e
Joy	NPR	Joy
sono	VER:fin	essere
le	ART	la
7	ORA	7
di	PRE	di
sera	NOUN	sera
Joy	NPR	Joy
vuole	VER2:fin	volere
uscire	VER:infi	uscire
urla	NOUN	urlo
:	PUN	:
−	PUN	−
torno	NOUN	torno
alle	ARTPRE	alla
11	ORA	11
la	ART	la
Gemima	NPR	Gemima
dice	VER:fin	dire
:	PUN	:
hokei	INTER	okay	e1	f1
alle	ARTPRE	alla
11	ORA	11
a	PRE	a
casa	NOUN	casa
.	SENT	.
Joy	NPR	Joy
esce	VER:fin	uscire
va	VER:fin	andare
a	PRE	a
carnevale	NOUN	carnevale
torna	VER:fin	tornare
a	PRE	a
mezzanotte	ORA	mezzanotte
la	ART	la
mamma	NOUN	mamma
lo	CLI	lo
aspetta	VER:fin	aspettare
lo	CLI	lo
vede	VER:fin	vedere
entrare	VER:infi	entrare
mezzo	ADJ	mezzo
cioco	ADJ	ciucco	e1
Joy	NPR	Joy
inizia	VER:fin	iniziare
a	PRE	a
vomitare	VER:infi	vomitare
.	SENT	.
(	PUN	(
che	CHE:adj	che
fortuna	NOUN	fortuna
che	CHE:pol	che
sono	VER:fin	essere
a	letto	ADV	a	letto	pl1
)	PUN	)
,	PUN	,
la	ART	la
mamma	NOUN	mamma
lo	CLI	lo
mette	VER:fin	mettere
a	letto	ADV	a	letto	pl1
e	CON	e
smette	VER:fin	smettere
di	PRE	di
vomitare	VER:infi	vomitare
,	PUN	,
per	fortuna	ADV	per	fortuna	pl1
.	SENT	.
Il	ART	il
giorno	NOUN	giorno
seguente	ADJ	seguente
la	ART	la
mamma	NOUN	mamma
scopre	VER:fin	scoprire
che	CHE:con	che
si	CLI	si
era	AUX:fin	essere
ubriacato	VER:ppast	ubriacarsi
e	CON	e
lo	CLI	lo
mette	in	castigo	VER:fin	mettere	in	castigo	pl1
.	SENT	.
Insegnamento	NOUN	insegnamento
della	ARTPRE	della
storia	NOUN	storia
.	SENT	.
-	1	NUM	1
bisognia	VER:fin	bisognare	e1
sempre	ADV	sempre
ascoltare	VER:infi	ascoltare
gli	ART	lo
orari	NOUN	orario
a	PRE	a
qui	PRO:rel	cui	e1
entrare	VER:infi	entrare
.	SENT	.
E	CON	e
che	CHE:con	che
i	ART	il
consigli	NOUN	consiglio
della	ARTPRE	della
mamma	NOUN	mamma
vanno	VER:fin	andare
ascoltati	VER:ppast	ascoltare



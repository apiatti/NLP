****5testo7	*SCUOLA=SE	*ANNOSCOLASTICO=5	*CLASSE=AG51	*TERRITORIO=AG	*LINGUA=AL	*GENERE=F
Allo	ARTPRE	allo
spettacolo	NOUN	spettacolo
di	PRE	di
pattinaggio	NOUN	pattinaggio
Un	giorno	ADV	un	giorno	pl1
sono	AUX:fin	essere
andata	VER:ppast	andare
a	PRE	a
Lugano	NPR	Lugano
,	PUN	,
per	PRE	per
partecipare	VER:infi	partecipare
allo	ARTPRE	allo
spettacolo	NOUN	spettacolo
di	PRE	di
pattinaggio	NOUN	pattinaggio
.	SENT	.
C’era	VER:fin	esserci
una	ART	una
ragazza	NOUN	ragazzo
molto	ADV	molto
presuntuosa	ADJ	presuntuoso
,	PUN	,
che	CHE:rel	che
aveva	AUX:fin	avere
fatto	VER:ppast	fare
il	ART	il
suo	DET:poss	suo
spettacolo	NOUN	spettacolo
di	fretta	ADV	di	fretta	pl1
,	PUN	,
per	PRE	per
vantarsi	VER:infi	vantarsi
,	PUN	,
ma	CON	ma
alla	fine	ADV	alla	fine	pl1
aveva	AUX:fin	avere
perso	VER:ppast	perdere
.	SENT	.
Io	PRO:pers	io
,	PUN	,
dato	che	CON	dato	che	pl1
avevo	AUX:fin	avere
visto	VER:ppast	vedere
che	CHE:con	che
farlo	VER:infi:cli	fare
veloce	ADV		veloce
non	NEG	non
andava	bene	VER:fin	andare	bene	pl1
,	PUN	,
l’	CLI	lo
ho	AUX:fin	avere
fatto	VER:ppast	fare
con	calma	ADV	con	calma	pl1
e	CON	e
ho	AUX:fin	avere
vinto	VER:ppast	vincere
il	ART	il
primo	ADJ	primo
posto	NOUN	posto
.	SENT	.
MORALE	NOUN	morale
:	PUN	:
La	ART	la
morale	NOUN	morale
è	VER:fin	essere
che	CHE:con	che
l’	ART	lo
egoismo	NOUN	egoismo
e	CON	e
la	ART	la
vanità	NOUN	vanità
non	NEG	non
portano	VER:fin	portare
niente	PRO:indef	niente
.	SENT	.
Quindi	CON	quindi
chi	PRO:indef	chi
va	VER:fin	andare
piano	ADV	piano
va	VER:fin	andare
sano	ADJ	sano
e	CON	e
lontano	ADV	lontano
!	SENT	!
THE	END	ADV	The	end	pl1	f1
Poi	ADV	poi
fare	VER:infi	fare
gli	ART	lo
esercizzi	NOUN	esercizio	e1
con	calma	ADV	con	calma	pl1
mi	CLI	mi
è	AUX:fin	essere
servito	VER:ppast	servire
in	PRE	in
altre	DET:indef	altro
occasioni	NOUN	occasione
per	esempio	ADV	per	esempio	pl1
quando	CON	quando
mi	CLI	mi
stavo	VER:fin	stare
allenando	VER:geru	allenarsi
per	PRE	per
dare	VER:infi	dare
il	ART	il
meglio	NOUN	meglio
di	PRE	di
me	PRO:pers	me
.	SENT	.
Dando	VER:geru	dare
il	ART	il
meglio	NOUN	meglio
miglioro	VER:fin	migliorare
.	SENT	.


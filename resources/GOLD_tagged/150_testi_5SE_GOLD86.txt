****5testo87	*SCUOLA=SE	*ANNOSCOLASTICO=5	*CLASSE=BS51	*TERRITORIO=BS	*LINGUA=AU	*GENERE=M
Mai	ADV	mai
sottovalutare	VER:infi	sottovalutare
gli	ART	lo
avversari	NOUN	avversario
Ero	VER:fin	essere
in	PRE	in
partenza	NOUN	partenza
per	PRE	per
un	ART	un
torneo	NOUN	torneo
a	PRE	a
Chiasso	NPR	Chiasso
.	SENT	.
Arrivati	VER:ppast	arrivare
sono	AUX:fin	essere
andato	VER:ppast	andare
negli	ARTPRE	nel
spogliatoi	NOUN	spogliatoio
e	CON	e
c’erano	VER:fin	esserci
tutti	DET:indef	tutto
i	ART	il
miei	DET:poss	mio
compagni	NOUN	compagno
di	PRE	di
hockei	NOUN	hockey	e1	f1
.	SENT	.
Siamo	AUX:fin	essere
andati	VER:ppast	andare
a	PRE	a
fare	VER:infi	fare
riscaldamento	NOUN	riscaldamento
di	fuori	ADV	di	fuori	pl1
sul	ARTPRE	sul
piazzale	NOUN	piazzale
.	SENT	.
La	ART	la
prima	ADJ	prima
partita	NOUN	partita
era	VER:fin	essere
contro	PRE	contro
il	ART	il
Chiasso	NPR	Chiasso
e	CON	e
abbiamo	AUX:fin	avere
vinto	VER:ppast	vincere
cinque	DET:num	cinque
a	PRE	a
tre	DET:num	tre
.	SENT	.
La	ART	la
seconda	ADJ	secondo
partita	NOUN	partita
era	VER:fin	essere
contro	PRE	contro
L’	ART	la	e1
ascona	NPR	Ascona	e1
,	PUN	,
finita	VER:ppast	finire
la	ART	la
partita	NOUN	partita
ne	CLI	ne
iniziava	VER:fin	iniziare
un’	ART	una
altra	PRO:indef	altro
contro	PRE	contro
il	ART	il
Lugano	NPR	Lugano
un	ART	un
derbi	ADJ	derby	e1	f1
.	SENT	.
Alla	fine	ADV	alla	fine	pl1
e	AUX:fin	essere	e1
finita	VER:ppast	finire
sette	DET:num	sette
ha	PRE	a	e1
due	DET:num	due
a	AUX:fin	avere	e1
vinto	VER:ppast	vincere
il	ART	il
Lugano	NPR	Lugano
,	PUN	,
siamo	AUX:fin	essere
arrivati	VER:ppast	arrivare
secondi	ADJ	secondo
adesso	ADV	adesso
mi	rendo	conto	VER:fin	rendersi	conto	pl1
che	CHE:con	che
Mai	ADV	mai	e1
sottovalutare	VER:infi	sottovalutare
gli	ART	lo
avversari	NOUN	avversario
.	SENT	.


****5testo9	*SCUOLA=SE	*ANNOSCOLASTICO=5	*CLASSE=AG51	*TERRITORIO=AG	*LINGUA=AL	*GENERE=M
Una	volta	ADV	una	volta	pl1
un	ART	un
bambino	NOUN	bambino
mi	CLI	mi
ha	AUX:fin	avere
chiesto	VER:ppast	chiedere
di	PRE	di
fare	VER:infi	fare
un	ART	un
gioco	NOUN	gioco
,	PUN	,
lui	PRO:pers	lui
penzava	VER:fin	pensare	t1
di	PRE	di
essere	AUX:infi	essere
più	ADV	più
bravo	ADJ	bravo
di	PRE	di
mé	PRO:pers	me	e1
ma	CON	ma
non	NEG	non
era	VER:fin	essere
così	ADV	così
.	SENT	.
Il	ART	il
giorno	NOUN	giorno
dopo	ADJ	dopo
abbiamo	AUX:fin	avere
giocato	VER:ppast	giocare
a	PRE	a
quel	DET:demo	quello
gioco	NOUN	gioco
lui	PRO:pers	lui
era	VER:fin	essere
molto	ADJ	molto
avanti	ADV	avanti
ma	CON	ma
io	PRO:pers	io
dopo	CON	dopo
lo_(l’ho)	AUX:fin	avere	e1
superato	VER:ppast	superare
e	CON	e
ho	AUX:fin	avere
vinto	VER:ppast	vincere
il	ART	il
gioco	NOUN	gioco
.	SENT	.
Mi	CLI	mi
ha	AUX:fin	avere
insegnato	VER:ppast	insegnare
che	CHE:con	che
si	CLI	si
deve	VER2:fin	dovere
essere	VER:infi	essere
bravi	ADJ	bravo
in	PRE	in
quel	DET:demo	quello
gioco	NOUN	gioco
e	CON	e
non	NEG	non
dire	VER:infi	dire
che	CHE:con	che
si	CLI	si
e_(è)	VER:fin	essere	e1
sicuri	ADJ	sicuro
divincere_(divincere)	VER:infi	vincere	e1
il	ART	il
gioco	NOUN	gioco
.	SENT	.
Lui	PRO:pers	lui
che	CHE:rel	che
penzava	VER:fin	pensare	t1
di	PRE	di
vìncere	VER:infi	vincere	e1
,	PUN	,
ma	CON	ma
quando	CON	quando
io	PRO:pers	io
l’	CLI	lo
avevo	AUX:fin	avere
battuto	VER:ppast	battere
si	CLI	si
era	AUX:fin	essere
arrabbiato	VER:ppast	arrabbiarsi
perciò	CON	perciò
voleva	VER2:fin	volere
fare	VER:infi	fare
la	ART	la
rivincita	NOUN	rivincita
,	PUN	,
ma	CON	ma
stavolto	ADV	stavolta	e1
a	PRE	a
un	ART	un
altro	DET:indef	altro
gioco	NOUN	gioco
.	SENT	.
Ma	CON	ma
stavolto	ADV	stavolta	e1
ero	VER:fin	essere
in	PRE	in
vantaggio	NOUN	vantaggio
io	PRO:pers	io
ma	CON	ma
al_(all’)		ARTPRE	allo	e1
ultimo	ADJ	ultimo
momento	NOUN	momento
mi	CLI	mi
supero_(superò)	VER:fin	superare	e1
e	CON	e
vinse	VER:fin	vincere
lui	PRO:pers	lui
il	ART	il
gioco	NOUN	gioco
,	PUN	,
cosi	ADV	così	e1
stavolto	ADV	stavolta	e1
era	VER:fin	essere
contento	ADJ	contento
.	SENT	.
Ho	AUX:fin	avere
imparato	VER:ppast	imparare
che	CHE:con	che
non	NEG	non
si	CLI	si
deve	VER2:fin	dovere
fare	VER:infi	fare
la	ART	la
battalia	NOUN		battaglia	e1
di	PRE	di
un	ART	un
gioco	NOUN	gioco
ma	CON	ma
la	ART	la
miglior	ADJ	migliore
coso	NOUN	cosa	e2
in	PRE	in
un	ART	un
gioco	NOUN	gioco
si	CLI	si
dive	VER:2fin	dovere	e1
divertirsi	VER:infi	divertirsi
,	PUN	,


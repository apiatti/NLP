****5testo23	*SCUOLA=SE	*ANNOSCOLASTICO=5	*CLASSE=AS51	*TERRITORIO=AS	*LINGUA=AL	*GENERE=M
L’	ART	lo
anno	NOUN	anno
scorso	ADJ	scorso
nel	ARTPRE	nel
campionato	NOUN	campionato
di	PRE	di
calcio	NOUN	calcio
abbiamo	AUX:fin	avere
giocato	VER:ppast	giocare
contro	PRE	contro
il	ART	il
sassariente	piano	NPR	Sassariente	Piano	e1	pl1	
8	NUM	8	
in	PRE	in
spogliatoio	NOUN	spogliatoio
avevo	AUX:fin	avere
sotto	valutato	VER:ppast	sottovalutare	e1
il_(l’)	ART	lo	e2
avversario	NOUN	avversario
in	PRE	in
partita	NOUN	partita
ho	AUX:fin	avere
visto	VER:ppast	vedere
che	CHE:con	visto	che
ci	CLI	ci
stavano	VER:fin	stare
pattendo	VER:geru	battere	e1
2:0	NUM	2:0
peró	CON	però
nel	ARTPRE	nel
secondo	ADJ	secondo
tempo	NOUN	tempo
abbiamo	AUX:fin	avere
ricuperato	VER:ppast	ricuperare
fino	a	PRE	fino	a	pl1
2:2	NUM	2:2
quando	CON	quando
ci	CLI	ci
mancavano	VER:fin	mancare
due	DET:num	due
tre	DET:num	tre
minuti	NOUN	minuto
eravamo	VER:fin	essere
un	po’	ADV	un	po’	pl1
emozionati	ADJ	emozionato
peró	CON	però
nei_(negli)	ARTPRE	nel	e2
ultimi	ADJ	ultimo
50	DET:num	50
secondi	NOUN	secondo
i_(gli)	ART	il	e2
avversari	NOUN	avversario
anno	AUX:fin	avere	e1
ancora	ADV	ancora
segnato	VER:ppast	segnare
un	ART	un
Gol	NOUN	goal	e1	
per	quello	CON	per	quello	pl1
abbiamo	AUX:fin	avere
perso	VER:ppast	perdere
la	ART	la
partita	NOUN	partita
.	SENT	.
In	PRE	in
spogliatoi	NOUN	spogliatoio	e1
il_(l’)	ART	lo	e2
allenatore	NOUN	allenatore
era	VER:fin	essere
abbastanza	ADV	abbastanza
contento	ADJ	contento
di	PRE	di
come	ADV	come
abbiamo	AUX:fin	avere
giocato	VER:ppast	giocare
.	SENT	.
Nei_(negli)	ARTPRE	nel	e2
allenamenti	NOUN	allenamento
il_(l’)	ART	lo	e2
allenatore	NOUN	allenatore
aveva	AUX:fin	avere
detto	VER:ppast	dire
di	PRE	di
non	NEG	non
sotto	valutare	VER:infi	sottovalutare	e1
gli	ART	lo
aversari	NOUN	avversario	e1
e	CON	e
aveva	AUX:fin	avere
anche	ADV	anche
detto	VER:ppast	dire
di	PRE	di
correre	VER:infi	correre
perché	CON	perché
se	CON	se
non	NEG	non
cori	VER:infi	correre	e1
non	NEG	non
piuoi	VER2:fin	potere	e1
segnare	VER:infi	segnare
.	SENT	.
L’	ART	lo
insegnamento	NOUN	insegnamento
che	CHE:rel	che
ho	AUX:fin	avere
ricevuto	VER:ppast	ricevere
é	VER:fin	essere
di	PRE	di
non	NEG	non
sotto	valutare	VER:infi	sottovalutare
i_(gli)	ART	il	e2
avversari	NOUN	avversario
e	CON	e
mi	CLI	mi
sono	AUX:fin	essere
vantato	VER:ppast	vantare
troppo	ADV	troppo
e	CON	e
non	NEG	non
ho	AUX:fin	avere
corso	VER:ppast	correre
.	SENT	.


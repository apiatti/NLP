****5testo67	*SCUOLA=SE	*ANNOSCOLASTICO=5	*CLASSE=BI51	*TERRITORIO=BI	*LINGUA=AU	*GENERE=F
Il	ART	il
signore	NOUN	signore
del	ARTPRE	del
bar	NOUN	bar
e	CON	e
la	ART	la
signora	NOUN	signore
Una	ART	una
giornata	NOUN	giornata
,	PUN	,
avevo	VER:fin	avere
8	DET:num	8
anni	NOUN	anno
,	PUN	,
io	PRO:pers	io
mia	DET:poss	mio
mamma	NOUN	mamma
e	CON	e
papá	NOUN	papà
e	CON	e
mio	DET:poss	mio
fratellino	NOUN	fratello
Giona	NPR	Giona
eravamo	VER:fin	essere
in	PRE	in
Liguria	NPR	Liguria
,	PUN	,
siamo	AUX:fin	essere
andati	VER:ppast	andare
in	PRE	in
spiaggia	NOUN	spiaggia
,	PUN	,
eravamo	VER:fin	essere
al	ARTPRE	al
piccolo	ADJ	piccolo
bar	NOUN	bar
che	CHE:rel	che
c’era	VER:fin	esserci
,	PUN	,
stavevamo	VER:fin	stare	e1
mangiando	VER:geru	mangiare
tranquilli	ADJ	tranquillo
e	CON	e
tutto	ad	un	tratto	ADV	tutto	a	un	tratto	pl1
una	ART	una
signora	NOUN	signore
stava	VER:fin	stare
litigando	VER:geru	litigare
con	PRE	con
un	ART	un
signore	NOUN	signore
che	CHE:rel	che
lavora	VER:fin	lavorare
al	ARTPRE	al
bar	NOUN	bar
stavano	VER:fin	stare
litigando	VER:geru	litigare
,	PUN	,
non	NEG	non
so	VER:fin	sapere
per	PRE	per
cosa	NOUN	cosa
ma	CON	ma
la	ART	la
signora	NOUN	signore
continuava	VER:fin	continuare
a	PRE	a
urlare	VER:infi	urlare
,	PUN	,
e	CON	e
sene	ADV	se	ne	e1	pl1
andó	VER:fin	andarsene
,	PUN	,
e	CON	e
tutto	ad	un	tratto	ADV	tutto	a	un	tratto	pl1
si	CLI	si
é	AUX:fin	essere
rotta	VER:ppast	rompersi
la	ART	la
vetrina	NOUN	vetrina
,	PUN	,
non	NEG	non
si	CLI	si
sa	VER:fin	sapere
come	ADV	come
era	VER:fin	essere
quella	PRO:demo	quella
dei	ARTPRE	del
vini	NOUN	vino
per	fortuna	ADV	per	fortuna	pl1
il	ART	il
signore	NOUN	signore
si	CLI	si
é	AUX:fin	essere
salvato	VER:ppast	salvarsi
mio	DET:poss	mio
papá	NOUN	papà
é	AUX:fin	essere
andato	VER:ppast	andare
ad	PRE	a
aiutare	VER:infi	aiutare
,	PUN	,
ed	CON	ed
é	AUX:fin	essere
arrivata	VER:ppast	arrivare
ancora	ADV	ancora
la	ART	la
signora	NOUN	signore
e	CON	e
li	CLI	gli	e2
ha	AUX:fin	avere
detto	VER:ppast	dire
ecco	ADV	ecco
questa	PRO:demo	questo
é	VER:fin	essere
la	ART	la
giusta	ADJ	giusto
punizione	NOUN	punizione
e	CON	e
sene	ADV	se	ne	pl1	e1
andó	VER:fin	andarsene
dinuovo	ADV	di	nuovo	e1	pl1
,	PUN	,
potrebbe	VER2:fin	potere
essere	AUX:infi	essere
stata	VER:ppast	stata
lei	PRO:pers	lei
avrá	AUX:fin	avere
lanciato	VER:ppast	lanciare
qualcosa	PRO:indef	qualcosa
non	NEG	non
puó	VER:2fin	potere
essersi	AUX:infi	essere
rotta	VER:ppast	rompersi
da	sola	ADV	da	sola	pl1
la	ART	la
vitrina	NOUN	vetrina	e1
,	PUN	,
ho	AUX:fin	avere
imparato	VER:ppast	imparare
che	CHE:con	che
non	NEG	non
dobbiamo	VER2:fin	dovere
prendercela	VER:infi:cli	prendersela
cosí	ADV	così
tanto	ADV	tanto
,	PUN	,
forse	ADV	forse
sí	ADV	sì
ma	CON	ma
offrire	VER:infi	offrire
l’	ART	lo
aiuto	NOUN	aiuto
come	ADV	come
ha	AUX:fin	avere
fatto	VER:ppast	fare
mio	DET:poss	mio
papá	NOUN	papà
anche	se	CON	anche	se	pl1
non	NEG	non
era	VER:fin	essere
arrabbiato	ADJ	arrabbiato
.	SENT	.


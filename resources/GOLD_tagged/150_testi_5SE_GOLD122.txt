****5testo123	*SCUOLA=SE	*ANNOSCOLASTICO=5	*CLASSE=CH51	*TERRITORIO=CH	*LINGUA=AU	*GENERE=F
Quando	CON	quando
facevo	VER:fin	fare
il	ART	il
pattinaggio	NOUN	pattinaggio
ero	VER:fin	essere
sempre	ADV	sempre
la	ART	la
più	ADV	più
brava	ADJ	bravo
nel	ARTPRE	nel
fare	VER:infi	fare
gli	ART	lo
esercizi	NOUN	esercizio
e	CON	e
per	questo	CON	per	questo	pl1
che	CHE:con	che
mi	CLI	mi
l’asciavano	VER:fin	lasciare	e1
sempre	ADV	sempre
in	disparte	ADV	in	disparte	pl1
e	CON	e
gli	CLI	gli
dava	VER:fin	dare
molto	ADV	molto
fastidio	NOUN	fastidio
alle	ARTPRE	alla
mi_(mie)	DET:poss	mia	e1	
compagne	NOUN	compagna
che	CHE:rel	che
mi	CLI	mi
chiamavano	VER:fin	chiamare
sempre	ADV	sempre
per	PRE	per
far	VER2:infi	fare
vedere	VER:infi	vedere
come	ADV	come
si	CLI	si
faceva	VER:fin	fare
un	ART	un
esercizio	NOUN	esercizio
.	SENT	.
Mi	CLI	mi
dicevano	VER:fin	dire
cose	NOUN	cosa
sgradevoli	ADJ	sgradevole
e	CON	e
dicevano	VER:fin	dire
bugie	NOUN	bugia
sul	ARTPRE	sul
mio	DET:poss	mio
conto	NOUN	conto
per	esempio	ADV	per	esempio	pl1
:	PUN	:
quando	CON	quando
avevo	AUX:fin	avere
smesso	VER:ppast	smettere
di	PRE	di
pattinare	VER:infi	pattinare
me	CLI	me	
dicevano	VER:fin	dirne	e2	pl1
di	tutti	i	colori	ADJ	di	tutti	i	colori	pl1
:	PUN	:
dicevanono	VER:fin	dire	e1
che	CHE:con	che
avevo	AUX:fin	avere
smesso	VER:ppast	smettere
perché	CON	perché
mi	CLI	mi
facevano	male	VER:fin	fare	male	e2
le	ART	la
ossa	NOUN	osso
,	PUN	,
o	CON	o
dicevano	VER:fin	dire
che	CHE:con	che
mia	DET:poss	mio
mamma	NOUN	mamma
non	NEG	non
poteva	VER2:fin	potere
piú	ADV	più
pagare	VER:infi	pagare
il	ART	il
CPC	NPR	CPC
e	CON	e
la	ART	la
cosa	NOUN	cosa
che	CHE:rel	che
mi	CLI	mi
ha	AUX:fin	avere
fatto	VER:ppast	fare
piú	ADV	più
arrabbiare	VER:infi	arrabbiare
è	AUX:fin	essere
stata	VER:ppast	stata
quella	PRO:demo	quello
che	CHE:rel	che
io	PRO:pers	io
non	NEG	non
potevo	VER2:fin	potere
chiamare	VER:infi	chiamare
mami	NOUN	mamma
la	CLI	la
mia	DET:poss	mio
madre	NOUN	madre
adottiva	ADJ	adottivo
perché	CON	perché
non	NEG	non
era	VER:fin	essere
la	CLI	la
mia	DET:poss	mio
vera	ADJ	vero
mamma	NOUN	mamma
.	SENT	.
Peró	CON	però
avevo	VER:fin	avere
qualche	DET:indef	qualche
amica	NOUN	amica
come	ADV	come
la	CLI	la
Sara	NPR	Sara
,	PUN	,
l’	ART	lo
Arhuch	NPR	Arhuch
e	CON	e
il	ART	il
Dario	NPR	Dario
;	PUN	;
con	PRE	con
la	CLI	la
Sara	NPR	Sara
non	NEG	non
parlavo	VER:fin	parlare
in	PRE	in
pista	NOUN	pista
perché	CON	perché
era	VER:fin	essere
in	PRE	in
un	ART	un
altro	DET:indef	altro
gruppo	NOUN	gruppo
piú	ADV	più
in	avanti	ADV	in	avanti	pl1
(	PUN	(
ma	CON	ma
non	NEG	non
molto	ADV	molto
secondo	PRE	secondo
me	PRO:pers	me
,	PUN	,
erano	VER:fin	essere
solo	ADV	solo
in	avanti	ADV	in	avanti	pl1
di	PRE	di
categoria	NOUN	categoria
)	PUN	)
.	SENT	.
Le	ART	la
mie	DET:poss	mio
più	ADV	più
atroci	ADJ	atroce
nemiche	NOUN	nemica
erano	VER:fin	essere
la	ART	la
Fancesca	NPR	Francesca	e1
e	CON	e
la	ART	la
Giorgia	NPR	Giorgia
Muscionico	NPR	Muscionico
:	PUN	:
la	ART	la
Giorgia	NPR	Giorgia
era	VER:fin	essere
una	ART	una
serpe	NOUN	serpe
voleva	VER2:fin	volere
sempre	ADV	sempre
essere	VER:infi	essere
nel	ARTPRE	nel
centro	NOUN	centro
dell’	ARTPRE	della
attenzione	NOUN	attenzione
era	VER:fin	essere
una	ART	una
attrice	NOUN	attore
nata	VER:ppast	nascere
,	PUN	,
una	volta	ADV	una	volta	pl1
ha	AUX:fin	avere
fatto	VER2:ppast	fare
cadere	VER:infi	cadere
la	ART	la
Sara	NPR	Sara
dalle	ARTPRE	dalla
scale	NOUN	scala
,	PUN	,
la	ART	la
Sara	NPR	Sara
e	AUX:fin	essere	e1
andata	VER:ppast	andare
diglielo	VER:infi:cli	dire	e1
alle	ARTPRE	alla
maestre	NOUN	maestra
e	CON	e
solo	che	CON	solo	che	pl1
la	ART	la
Giorgia	NPR	Giorgia
si	CLI	si
é	AUX:fin	essere
messa	VER:ppast	mettersi
a	PRE	a
piangere	VER:infi	piangere
e	CON	e
dire	VER:infi	dire
che	CHE:con	che
non	NEG	non
era	VER:fin	essere
vero	ADJ	vero
e	CON	e
alla	ARTPRE	alla
fine	NOUN	fine
anno	AUX:fin	avere	e1
creduto	VER:ppast	credere
a	PRE	a
quella	DET:demo	quello
vipera	NOUN	vipera
.	SENT	.
Bè	INTER	be’	e1
devo	VER2:fin	dovere
direre	VER:infi	dire	e1
in	PRE	in
quei	DET:demo	quello
cinque	DET:num	cinque
anni	NOUN	anno
ho	AUX:fin	avere
rafforzato	VER:ppast	rafforzare
il	ART	il
mio	DET:poss	mio
carattere	NOUN	carattere
.	SENT	.
La	ART	la
morale	NOUN	morale
è	VER:fin	essere
che	CHE:con	che
se	CON	se
qualcuno	PRO:indef	qualcuno
ti	CLI	ti
fa	VER2:fin	fare
star	VER:infi	stare
male	ADV	male
devi	VER2:fin	dovere
avanti	ADV	avanti
e	CON	e
non	NEG	non
fermarti	VER:infi:cli	fermarsi
.	SENT	.


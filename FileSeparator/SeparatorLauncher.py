from FileWriter import FileWriter
from ReaderParser import ReaderParser

PATHTOTEXTS = "../resources/150_testi_5SE_GOLD.txt"
readerParser = ReaderParser(PATHTOTEXTS)
readerParser.read_file()
readerParser.print_files()


fileWriter = FileWriter(PATHTOTEXTS, readerParser)
fileWriter.write_files()

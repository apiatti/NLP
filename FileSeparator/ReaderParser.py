import codecs


class ReaderParser:
    def __init__(self, path):
        self.path = path
        self.lines = []
        self.files = []
        pass

    def read_file(self):
        '''
        Read the files and populate the lines list object
        @return:
        '''
        with codecs.open(self.path, "r+") as f:
            counter = 0
            for line in f:
                if "GOLD" in self.path:
                    if "****" in line and counter != 0:
                        self.lines.append("\n")
                        self.lines.append(line)
                    else:
                        self.lines.append(line)

                    counter += 1
                else:
                    self.lines.append(line)
            counter += 1
        self.split_in_files()
        return self.lines

    def split_in_files(self):
        '''
        Create files from read lines
        @return:
        '''
        start = -1
        end = -1
        if "GOLD" not in self.path:
            pass
        for i in range(len(self.lines)):
            if self.lines[i].startswith("****"):
                start = i
            elif (self.lines[i] == "\n") or (self.lines[i] == "\r\n"):
                end = i
            self.files.append(self.lines[start:end + 1])

    def print_files(self):
        '''
        Print the 'files' to the console
        @return:
        '''
        for i in range(len(self.files)):
            if self.files[i]:
                print self.files[i]

    def print_lines(self):
        '''
        Print read lines to the console
        @return:
        '''
        for i in range(len(self.lines)):
            if self.lines[i]:
                print self.lines[i],

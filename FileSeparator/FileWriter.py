import os

GOLD_DIR = "/GOLD_tagged/"


class FileWriter:
    def __init__(self, filepath, reader):
        self.filepath = filepath
        self.reader = reader
        pass

    def write_files(self):
        '''
        Write separated files to the constructed path
        @return: None
        '''
        counter = 0
        for i in range(len(self.reader.files)):
            fpath = self.get_file_infos()[0]
            fname = self.get_file_infos()[1]
            fext = self.get_file_infos()[2]
            if self.reader.files[i]:
                counter += 1
                with open(self.construct_path(fpath, GOLD_DIR, fname, fext, counter), "a") as f:
                    for j in range(len(self.reader.files[i])):
                        if "GOLD" not in self.filepath:
                            if not str(self.reader.files[i][j]).startswith("****"):
                                f.write(self.reader.files[i][j])
                        else:
                                f.write(self.reader.files[i][j])

    def get_file_infos(self):
        '''

        @return: list object containing file infos such as path, name and extension
        '''
        fext = os.path.splitext(self.filepath)[1]
        fname = os.path.splitext(os.path.split(self.filepath)[1])[0]
        fpath = os.path.dirname(self.filepath)
        finfos = [fpath, fname, fext]
        return finfos

    def construct_path(self, path, dir, name, fext, i):
        '''
        @param path: current path
        @param dir: directory
        @param name: file name
        @param fext: file extension
        @param i: file number
        @return: the constructed path
        '''
        return path + dir + name + str(i) + fext
